using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Gather.Reflection.IL
{
    /// <summary>IL指令</summary>
    public class ILInstruction
    {
        #region 属性

        /// <summary>指令</summary>
        public OpCode Code { get; set; }

        /// <summary>操作</summary>
        public object Operand { get; set; }

        /// <summary>操作数据</summary>
        public byte[] OperandData { get; set; }

        /// <summary>偏移</summary>
        public int Offset { get; set; }

        #endregion

        /// <summary>已重载。返回指令的字符串形式</summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder(32);

            sb.AppendFormat("L_{0:0000}: {1}", Offset, Code);
            if (Operand == null) return sb.ToString();

            switch (Code.OperandType)
            {
                case OperandType.InlineField:
                    var fOperand = ((FieldInfo)Operand);
                    sb.Append(" " + FixType(fOperand.FieldType) + " " + FixType(fOperand.ReflectedType) + "::" + fOperand.Name + "");
                    break;
                case OperandType.InlineMethod:
                    try
                    {
                        var mOperand = (MethodInfo)Operand;
                        sb.Append(" ");
                        if (!mOperand.IsStatic) sb.Append("instance ");
                        sb.Append(FixType(mOperand.ReturnType) + " " + FixType(mOperand.ReflectedType) + "::" + mOperand.Name + "()");
                    }
                    catch
                    {
                        var mOperand = (ConstructorInfo)Operand;
                        sb.Append(" ");
                        if (!mOperand.IsStatic) sb.Append("instance ");
                        sb.Append("void " + FixType(mOperand.ReflectedType) + "::" + mOperand.Name + "()");
                    }
                    break;
                case OperandType.ShortInlineBrTarget:
                    sb.Append(" L_" + ((int)Operand).ToString("0000"));
                    break;
                case OperandType.InlineType:
                    sb.Append(" " + FixType((Type)Operand));
                    break;
                case OperandType.InlineString:
                    if (Operand.ToString() == "\r\n")
                        sb.Append(" \"\\r\\n\"");
                    else
                        sb.Append(" \"" + Operand + "\"");
                    break;
                default:
                    sb.Append("不支持");
                    break;
            }

            return sb.ToString();

        }

        /// <summary>取得类型的友好名</summary>
        /// <param name="type">类型</param>
        /// <returns></returns>
        static string FixType(Type type)
        {
            string result = type.ToString();
            switch (result)
            {
                case "System.string":
                case "System.String":
                case "String":
                    result = "string"; break;
                case "System.Int32":
                case "Int":
                case "Int32":
                    result = "int"; break;
            }
            return result;
        }
    }
}