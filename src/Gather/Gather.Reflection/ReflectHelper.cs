﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewLife.Reflection
{
    /// <summary>反射助手。全部为扩展方法，类名可能改变</summary>
    public static class ReflectHelper
    {
        ///// <summary>是否继承自指定类或接口</summary>
        ///// <param name="type"></param>
        ///// <param name="t"></param>
        ///// <returns></returns>
        //public static Boolean IsDerive(this Type type, Type t) { return t.IsAssignableFrom(type); }

        ///// <summary>是否继承自指定类或接口</summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="type"></param>
        ///// <returns></returns>
        //public static Boolean IsDerive<T>(this Type type) { return typeof(T).IsAssignableFrom(type); }
    }
}