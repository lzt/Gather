﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.IO;
using System.Text;
using System.Xml;
using Operate.ExtensionMethods;
using Operate.IO.Serializers;
using Operate.IO.Serializers.Interfaces;

#endregion

namespace Operate.IO.ExtensionMethods
{
    /// <summary>
    /// Serialization extensions
    /// </summary>
    public static class SerializationExtensions
    {
        #region Extension Methods
        
        #region Serialize

        /// <summary>
        /// Serializes the object using the specified serializer
        /// </summary>
        /// <param name="Object">Object to serialize</param>
        /// <param name="serializer">Serializer to use (defaults to JSONSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)</param>
        /// <param name="fileLocation">File location to save to</param>
        /// <returns>The serialized object</returns>
        public static string Serialize(this object Object, ISerializer<string> serializer = null, Encoding encodingUsing = null, string fileLocation = "")
        {
            if (Object.IsNull()) { throw new ArgumentNullException("Object"); }
            string data = serializer.Check(()=>new JsonSerializer(encodingUsing)).Serialize(Object);
            if (!string.IsNullOrEmpty(fileLocation))
                fileLocation.Save(data);
            return data;
        }

        #endregion

        #region SerializeBinary

        /// <summary>
        /// Serializes the object using the specified serializer
        /// </summary>
        /// <param name="Object">Object to serialize</param>
        /// <param name="serializer">Serializer to use (defaults to BinarySerializer)</param>
        /// <param name="fileLocation">File location to save to</param>
        /// <returns>The serialized object</returns>
        public static byte[] SerializeBinary(this object Object, ISerializer<byte[]> serializer = null, string fileLocation = "")
        {
              if (Object.IsNull()) { throw new ArgumentNullException("Object"); }
            byte[] data = serializer.Check(()=>new BinarySerializer()).Serialize(Object);
            if (!string.IsNullOrEmpty(fileLocation))
                fileLocation.Save(data);
            return data;
        }

        #endregion

        #region Deserialize

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="TR">Object type</typeparam>
        /// <param name="data">Data to deserialize</param>
        /// <param name="serializer">Serializer to use (defaults to JSONSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)></param>
        /// <returns>The deserialized object</returns>
        public static TR Deserialize<TR>(this string data, ISerializer<string> serializer = null, Encoding encodingUsing = null)
        {
            return string.IsNullOrEmpty(data) ? default(TR) : (TR)data.Deserialize(typeof(TR), serializer, encodingUsing);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="TR">Object type</typeparam>
        /// <param name="data">Data to deserialize</param>
        /// <param name="serializer">Serializer to use (defaults to XMLSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)></param>
        /// <returns>The deserialized object</returns>
        
        public static TR Deserialize<TR>(this XmlDocument data, ISerializer<string> serializer = null, Encoding encodingUsing = null)
        {
            return (data == null) ? default(TR) : (TR)data.InnerXml.Deserialize(typeof(TR), serializer.Check(()=>new XMLSerializer(encodingUsing)), encodingUsing);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="TR">Object type</typeparam>
        /// <param name="data">Data to deserialize</param>
        /// <param name="serializer">Serializer to use (defaults to BinarySerializer)</param>
        /// <returns>The deserialized object</returns>
        public static TR Deserialize<TR>(this byte[] data, ISerializer<byte[]> serializer = null)
        {
            return (data==null) ? default(TR) : (TR)data.Deserialize(typeof(TR), serializer);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="TR">Object type</typeparam>
        /// <param name="data">Data to deserialize</param>
        /// <param name="serializer">Serializer to use (defaults to JSONSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)></param>
        /// <returns>The deserialized object</returns>
        public static TR Deserialize<TR>(this FileInfo data, ISerializer<string> serializer = null, Encoding encodingUsing = null)
        {
            return (data==null || !data.Exists) ? default(TR) : (TR)data.Read().Deserialize(typeof(TR), serializer, encodingUsing);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="data">Data to deserialize</param>
        /// <param name="serializer">Serializer to use (defaults to JSONSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)></param>
        /// <param name="objectType">Object type</param>
        /// <returns>The deserialized object</returns>
        public static object Deserialize(this FileInfo data,Type objectType, ISerializer<string> serializer = null, Encoding encodingUsing = null)
        {
            return (data==null || !data.Exists) ? null : data.Read().Deserialize(objectType, serializer, encodingUsing);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="data">Data to deserialize</param>
        /// <param name="objectType">Object type</param>
        /// <param name="serializer">Serializer to use (defaults to JSONSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)></param>
        /// <returns>The deserialized object</returns>
        public static object Deserialize(this string data,Type objectType, ISerializer<string> serializer = null, Encoding encodingUsing = null)
        {
            return serializer.Check(()=>new JsonSerializer(encodingUsing)).Deserialize(data, objectType);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="data">Data to deserialize</param>
        /// <param name="objectType">Object type</param>
        /// <param name="serializer">Serializer to use (defaults to BinarySerializer)</param>
        /// <returns>The deserialized object</returns>
        public static object Deserialize(this byte[] data, Type objectType, ISerializer<byte[]> serializer = null)
        {
            return serializer.Check(()=>new BinarySerializer()).Deserialize(data, objectType);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="data">Data to deserialize</param>
        /// <param name="objectType">Object type</param>
        /// <param name="serializer">Serializer to use (defaults to XMLSerializer)</param>
        /// <param name="encodingUsing">Encoding to use (defaults to ASCII)></param>
        /// <returns>The deserialized object</returns>
        
        public static object Deserialize(this XmlDocument data, Type objectType, ISerializer<string> serializer = null, Encoding encodingUsing = null)
        {
            return (data == null) ? null : data.InnerXml.Deserialize(objectType, serializer.Check(()=>new XMLSerializer(encodingUsing)), encodingUsing);
        }

        #endregion

        #region DeserializeBinary

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="data">Data to deserialize</param>
        /// <param name="objectType">Object type</param>
        /// <param name="serializer">Serializer to use (defaults to BinarySerializer)</param>
        /// <returns>The deserialized object</returns>
        public static object DeserializeBinary(this FileInfo data, Type objectType, ISerializer<byte[]> serializer = null)
        {
            return (data==null || !data.Exists) ? null : data.ReadBinary().Deserialize(objectType, serializer);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="TR">Object type</typeparam>
        /// <param name="data">Data to deserialize</param>
        /// <param name="serializer">Serializer to use (defaults to BinarySerializer)</param>
        /// <returns>The deserialized object</returns>
        public static TR DeserializeBinary<TR>(this FileInfo data, ISerializer<byte[]> serializer = null)
        {
            return (data==null || !data.Exists) ? default(TR) : (TR)data.ReadBinary().Deserialize(typeof(TR), serializer);
        }

        #endregion

        #endregion
    }
}
