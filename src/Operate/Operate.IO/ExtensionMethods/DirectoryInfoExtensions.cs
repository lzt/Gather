﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods.Enums;

#endregion

namespace Operate.IO.ExtensionMethods
{
    /// <summary>
    /// Extension methods for <see cref="System.IO.DirectoryInfo"/>
    /// </summary>
    public static class DirectoryInfoExtensions
    {
        #region Extension Methods

        #region Creator

        /// <summary>
        /// 创建目录
        /// </summary>
        /// <param name="dir">要创建的目录路径包括目录名</param>
        public static void CreateDir(this string dir)
        {
            if (dir.Length == 0) return;
            if (dir.ExistDir()) return;
            Directory.CreateDirectory(dir);
        }

        #endregion

        #region  Verifiaction

        /// <summary>
        /// 检测补录是否存在
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool ExistDir(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }
            return Directory.Exists(input);
        }

        /// <summary>
        /// 检测指定目录是否为空
        /// </summary>
        /// <param name="directoryPath">指定目录的绝对路径</param>        
        public static bool IsEmptyDirectory(this string directoryPath)
        {
            //判断是否存在文件
            string[] fileNames = directoryPath.GetFileNames();
            if (fileNames.Length > 0)
            {
                return false;
            }

            //判断是否存在文件夹
            string[] directoryNames = directoryPath.GetDirectories();
            if (directoryNames.Length > 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 检测指定目录中是否存在指定的文件
        /// </summary>
        /// <param name="directoryPath">指定目录的绝对路径</param>
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param> 
        /// <param name="isSearchChild">是否搜索子目录</param>
        public static bool Contains(string directoryPath, string searchPattern, bool isSearchChild = false)
        {
            //获取指定的文件列表
            string[] fileNames = GetFileNames(directoryPath, searchPattern, true);

            //判断指定文件是否存在
            if (fileNames.Length == 0)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region CopyTo

        /// <summary>
        /// Copies a directory to another location
        /// </summary>
        /// <param name="source">Source directory</param>
        /// <param name="destination">Destination directory</param>
        /// <param name="recursive">Should the copy be recursive</param>
        /// <param name="options">Options used in copying</param>
        /// <returns>The DirectoryInfo for the destination info</returns>
        public static DirectoryInfo CopyTo(this DirectoryInfo source, string destination, bool recursive = true, CopyOptions options = CopyOptions.CopyAlways)
        {
            if (source.IsNull()) { throw new ArgumentNullException("source"); }
            if (source.Exists.IsNull()) { throw new DirectoryNotFoundException("Source directory not found."); }
            if (destination.IsNullOrEmpty()) { throw new ArgumentNullException("destination"); }
            var destinationInfo = new DirectoryInfo(destination);
            destinationInfo.Create();
            foreach (FileInfo tempFile in source.EnumerateFiles())
            {
                if (options == CopyOptions.CopyAlways)
                {
                    tempFile.CopyTo(Path.Combine(destinationInfo.FullName, tempFile.Name), true);
                }
                else if (options == CopyOptions.CopyIfNewer)
                {
                    if (File.Exists(Path.Combine(destinationInfo.FullName, tempFile.Name)))
                    {
                        var fileInfo = new FileInfo(Path.Combine(destinationInfo.FullName, tempFile.Name));
                        if (fileInfo.LastWriteTime.CompareTo(tempFile.LastWriteTime) < 0)
                            tempFile.CopyTo(Path.Combine(destinationInfo.FullName, tempFile.Name), true);
                    }
                    else
                    {
                        tempFile.CopyTo(Path.Combine(destinationInfo.FullName, tempFile.Name), true);
                    }
                }
                else if (options == CopyOptions.DoNotOverwrite)
                {
                    tempFile.CopyTo(Path.Combine(destinationInfo.FullName, tempFile.Name), false);
                }
            }
            if (recursive)
            {
                foreach (var subDirectory in source.EnumerateDirectories())
                    // ReSharper disable ConditionIsAlwaysTrueOrFalse
                    subDirectory.CopyTo(Path.Combine(destinationInfo.FullName, subDirectory.Name), recursive, options);
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
            }
            return new DirectoryInfo(destination);
        }

        #endregion

        #region Delete

        /// <summary>
        /// 删除目录
        /// </summary>
        /// <param name="dir"></param>
        public static void DeleteDirectory(this string dir)
        {
            if (dir.ExistDir())
            {
                var dirInfo = new DirectoryInfo(dir);
                dirInfo.DeleteDirectory();
            }
        }

        /// <summary>
        /// Deletes directory and all content found within it
        /// </summary>
        /// <param name="info">Directory info object</param>
        public static void DeleteDirectory(this DirectoryInfo info)
        {
            if (info.IsNull()) { throw new ArgumentNullException("info"); }
            if (!info.Exists)
                return;
            info.DeleteFiles();
            info.EnumerateDirectories().ForEach(x => x.DeleteDirectory());
            info.Delete(true);
        }

        #endregion

        #region DeleteDirectoriesNewerThan

        /// <summary>
        /// Deletes directories newer than the specified date
        /// </summary>
        /// <param name="directory">Directory to look within</param>
        /// <param name="compareDate">The date to compare to</param>
        /// <param name="recursive">Is this a recursive call</param>
        /// <returns>Returns the directory object</returns>
        public static DirectoryInfo DeleteDirectoriesNewerThan(this DirectoryInfo directory, DateTime compareDate, bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
            if (directory.Exists) { throw new DirectoryNotFoundException("directory"); }
            directory.EnumerateDirectories("*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                     .Where(x => x.LastWriteTime > compareDate)
                     .ForEach(x => x.DeleteDirectory());
            return directory;
        }

        #endregion

        #region DeleteDirectoriesOlderThan

        /// <summary>
        /// Deletes directories newer than the specified date
        /// </summary>
        /// <param name="directory">Directory to look within</param>
        /// <param name="compareDate">The date to compare to</param>
        /// <param name="recursive">Is this a recursive call</param>
        /// <returns>Returns the directory object</returns>
        public static DirectoryInfo DeleteDirectoriesOlderThan(this DirectoryInfo directory, DateTime compareDate, bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
           if (directory.Exists) { throw new DirectoryNotFoundException("directory"); }
            directory.EnumerateDirectories("*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                     .Where(x => x.LastWriteTime < compareDate)
                     .ForEach(x => x.DeleteDirectory());
            return directory;
        }

        #endregion

        #region DeleteFiles

        /// <summary>
        /// Deletes files from a directory
        /// </summary>
        /// <param name="directory">Directory to delete the files from</param>
        /// <param name="recursive">Should this be recursive?</param>
        /// <returns>The directory that is sent in</returns>
        public static DirectoryInfo DeleteFiles(this DirectoryInfo directory, bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
           if (!directory.Exists) { throw new DirectoryNotFoundException("directory"); }
            directory.EnumerateFiles("*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                     .ForEach(x => x.Delete());
            return directory;
        }

        #endregion

        #region DeleteFilesNewerThan

        /// <summary>
        /// Deletes files newer than the specified date
        /// </summary>
        /// <param name="directory">Directory to look within</param>
        /// <param name="compareDate">The date to compare to</param>
        /// <param name="recursive">Is this a recursive call</param>
        /// <returns>Returns the directory object</returns>
        public static DirectoryInfo DeleteFilesNewerThan(this DirectoryInfo directory, DateTime compareDate, bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
           if (directory.Exists) { throw new DirectoryNotFoundException("directory"); }
            directory.EnumerateFiles("*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                     .Where(x => x.LastWriteTime > compareDate)
                     .ForEach(x => x.Delete());
            return directory;
        }

        #endregion

        #region DeleteFilesOlderThan

        /// <summary>
        /// Deletes files older than the specified date
        /// </summary>
        /// <param name="directory">Directory to look within</param>
        /// <param name="compareDate">The date to compare to</param>
        /// <param name="recursive">Is this a recursive call</param>
        /// <returns>Returns the directory object</returns>
        public static DirectoryInfo DeleteFilesOlderThan(this DirectoryInfo directory, DateTime compareDate, bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
           if (directory.Exists) { throw new DirectoryNotFoundException("directory"); }
            directory.EnumerateFiles("*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                     .Where(x => x.LastWriteTime < compareDate)
                     .ForEach(x => x.Delete());
            return directory;
        }

        #endregion

        #region DriveInfo

        /// <summary>
        /// Gets the drive information for a directory
        /// </summary>
        /// <param name="directory">The directory to get the drive info of</param>
        /// <returns>The drive info connected to the directory</returns>
        public static DriveInfo DriveInfo(this DirectoryInfo directory)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
            return new DriveInfo(directory.Root.FullName);
        }

        #endregion

        #region EnumerateFiles

        /// <summary>
        /// Enumerates the files within a directory
        /// </summary>
        /// <param name="directory">Directory to search in</param>
        /// <param name="searchPatterns">Patterns to search for</param>
        /// <param name="options">Search options</param>
        /// <returns>The enumerated files from the directory</returns>
        public static IEnumerable<FileInfo> EnumerateFiles(this DirectoryInfo directory, IEnumerable<string> searchPatterns, SearchOption options)
        {
            var files = new List<FileInfo>();
            foreach (string searchPattern in searchPatterns)
                files.Add(directory.EnumerateFiles(searchPattern, options));
            return files;
        }

        #endregion

        #region Get

        /// <summary>
        /// 获取指定目录中所有文件列表
        /// </summary>
        /// <param name="directoryPath">指定目录的绝对路径</param>        
        public static string[] GetFileNames(this string directoryPath)
        {
            //如果目录不存在
            if (!directoryPath.ExistDir())
            {
                return new string[] { };
            }

            //获取文件列表
            return Directory.GetFiles(directoryPath);
        }

        /// <summary>
        /// 获取指定目录及子目录中所有文件列表
        /// </summary>
        /// <param name="directoryPath">指定目录的绝对路径</param>
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param>
        /// <param name="isSearchChild">是否搜索子目录</param>
        public static string[] GetFileNames(this string directoryPath, string searchPattern, bool isSearchChild = false)
        {
            //如果目录不存在
            if (!directoryPath.ExistDir())
            {
                return new string[] { };
            }

            if (isSearchChild)
            {
                return Directory.GetFiles(directoryPath, searchPattern, SearchOption.AllDirectories);
            }
            return Directory.GetFiles(directoryPath, searchPattern, SearchOption.TopDirectoryOnly);
        }

        /// <summary>
        /// 获取指定目录中所有子目录列表,若要搜索嵌套的子目录列表,请使用重载方法.
        /// </summary>
        /// <param name="directoryPath">指定目录的绝对路径</param>        
        public static string[] GetDirectories(this string directoryPath)
        {
            //如果目录不存在
            if (!directoryPath.ExistDir())
            {
                return new string[] { };
            }

            return Directory.GetDirectories(directoryPath);
        }

        #endregion

        #region Size

        /// <summary>
        /// Gets the size of all files within a directory
        /// </summary>
        /// <param name="directory">Directory</param>
        /// <param name="searchPattern">Search pattern used to tell what files to include (defaults to all)</param>
        /// <param name="recursive">determines if this is a recursive call or not</param>
        /// <returns>The directory size</returns>
        public static long Size(this DirectoryInfo directory, string searchPattern = "*", bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
            return directory.EnumerateFiles(searchPattern, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                            .Sum(x => x.Length);
        }

        #endregion

        #region SetAttributes

        /// <summary>
        /// Sets a directory's attributes
        /// </summary>
        /// <param name="directory">Directory</param>
        /// <param name="attributes">Attributes to set</param>
        /// <param name="recursive">Determines if this is a recursive call</param>
        /// <returns>The directory object</returns>
        public static DirectoryInfo SetAttributes(this DirectoryInfo directory, FileAttributes attributes, bool recursive = false)
        {
            if (directory.IsNull()) { throw new ArgumentNullException("directory"); }
            directory.EnumerateFiles()
                     .ForEach(x => x.SetAttributes(attributes));
            if (recursive)
                directory.EnumerateDirectories().ForEach(x => x.SetAttributes(attributes, true));
            return directory;
        }

        #endregion

        #endregion
    }
}