﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Text;
using System.Web;
using Operate.ExtensionMethods;

#endregion

namespace Operate.IO.ExtensionMethods
{
    /// <summary>
    /// Extension methods for <see cref="System.IO.FileInfo"/>
    /// </summary>
    public static class FileInfoExtensions
    {
        #region Extension Methods

        #region

        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="file"></param>
        public static void CreateFile(this string file)
        {
            if (file.ExistFile())
            {
                return;
            }
            if (!File.Exists(file))
            {
                if (file.IndexOf('\\') < 0)
                {
                    throw new Exception("该字符串{0}不是有效的路径".FormatValue(file));
                }

                var dir = file.Substring(0, file.LastIndexOf('\\'));
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
            using (var fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                var sw = new StreamWriter(fs);
                sw.Write("");
                sw.Close();
            }
        }

        #endregion

        #region Write

        /// <summary>
        /// 向文件中追加文本
        /// </summary>
        /// <param name="source"></param>
        /// <param name="text">写入的文本</param>
        /// <param name="writeNewLine"></param>
        public static void AppendFile(this string source, string text, bool writeNewLine = false)
        {
            if (!source.ExistFile())
            {
                source.CreateFile();
            }
            WriteFile(source, text, Encoding.UTF8, FileMode.Append, writeNewLine);
        }

        /// <summary>
        /// 向文件中追加文本
        /// </summary>
        /// <param name="source"></param>
        /// <param name="text">写入的文本</param>
        /// <param name="encoding">编码</param>
        /// <param name="writeNewLine"></param>
        public static void AppendFile(this string source, string text, Encoding encoding, bool writeNewLine = false)
        {
            if (!source.ExistFile())
            {
                source.CreateFile();
            }
            WriteFile(source, text, encoding, FileMode.Append, writeNewLine);
        }

        /// <summary>
        /// 向文件中追加文本
        /// </summary>
        /// <param name="source"></param>
        /// <param name="text">写入的文本</param>
        public static void WriteFile(this string source, string text)
        {
            if (!source.ExistFile())
            {
                source.CreateFile();
            }
            WriteFile(source, text, Encoding.UTF8, FileMode.Create);
        }

        /// <summary>
        /// 向文件中追加文本
        /// </summary>
        /// <param name="source"></param>
        /// <param name="text">写入的文本</param>
        /// <param name="encoding">编码</param>
        public static void WriteFile(this string source, string text, Encoding encoding)
        {
            if (!source.ExistFile())
            {
                source.CreateFile();
            }
            WriteFile(source, text, encoding, FileMode.Create);
        }

        /// <summary>
        /// 写入文件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="text"></param>
        /// <param name="encoding"></param>
        /// <param name="filemode"></param>
        /// <param name="writeNewLine"></param>
        public static void WriteFile(this string source, string text, Encoding encoding, FileMode filemode, bool writeNewLine = false)
        {
            using (var fs = new FileStream(source, filemode, FileAccess.Write, FileShare.Read))
            {
                var sw = new StreamWriter(fs, encoding);
                if (writeNewLine)
                {
                    sw.WriteLine(text);
                }
                else
                {
                    sw.Write(text);
                }
                sw.Close();
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteFile(this string file)
        {
            if (!file.ExistFile())
            {
                return;
            }
            File.Delete(file);
        }

        /// <summary>
        /// 移动文件(剪贴--粘贴)
        /// </summary>
        /// <param name="source">要移动的文件的路径及全名(包括后缀)</param>
        /// <param name="target">文件移动到新的位置,并指定新的文件名</param>
        public static void MoveFile(this string source, string target)
        {
            if (!source.ExistFile())
            {
                return;
            }
            File.Move(source, target);
        }

        /// <summary>
        /// 复制文件
        /// </summary>
        /// <param name="source">要复制的文件的路径已经全名(包括后缀)</param>
        /// <param name="target">目标位置,并指定新的文件名</param>
        /// <param name="cover"></param>
        public static void CopyFile(this string source, string target, bool cover = false)
        {
            if (!source.ExistFile())
            {
                return;
            }
            File.Copy(source, target, cover);
        }

        #endregion

        #region Verifiaction

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool ExistFile(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }
            return File.Exists(input);
        }

        /// <summary>
        /// 对比文件扩展名，忽略大小写区别
        /// </summary>
        /// <param name="file"></param>
        /// <param name="extensions">扩展名集合</param>
        /// <returns></returns>
        public static bool VerifiactionFileExtension(this FileInfo file, params string[] extensions)
        {
            return file.GetFileExtension().CompareIn(extensions);
        }

        #endregion

        #region Get

        /// <summary>
        /// 获取文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static FileInfo GetFileInfo(this string file)
        {
            if (!file.ExistFile())
            {
                return null;
            }

            return new FileInfo(file);
        }

        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string GetFileExtension(this FileInfo file)
        {
            if (file.IsNull())
            {
                return null;
            }
            var name = file.Name;

            var index = name.LastIndexOf('.');
            if (index > 0)
            {
                return name.Remove(0, index + 1);
            }
            return null;
        }

        #endregion

        #region CompareTo

        /// <summary>
        /// Compares two files against one another
        /// </summary>
        /// <param name="file1">First file</param>
        /// <param name="file2">Second file</param>
        /// <returns>True if the content is the same, false otherwise</returns>
        public static bool CompareTo(this FileInfo file1, FileInfo file2)
        {
            if (!(file1 != null && file1.Exists)) { throw new ArgumentNullException("file1"); }
            if (!(file2 != null && file2.Exists)) { throw new ArgumentNullException("file2"); }
            if (file1.Length != file2.Length)
                return false;
            return file1.Read().Equals(file2.Read());
        }

        #endregion

        #region DriveInfo

        /// <summary>
        /// Gets the drive information for a file
        /// </summary>
        /// <param name="file">The file to get the drive info of</param>
        /// <returns>The drive info connected to the file</returns>
        public static DriveInfo DriveInfo(this FileInfo file)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            return file.Directory.DriveInfo();
        }

        #endregion

        #region Read

        /// <summary>
        /// Reads a file to the end as a string
        /// </summary>
        /// <param name="file">File to read</param>
        /// <returns>A string containing the contents of the file</returns>
        public static string Read(this FileInfo file)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (!file.Exists)
                return "";
            using (StreamReader reader = file.OpenText())
            {
                string contents = reader.ReadToEnd();
                return contents;
            }
        }

        /// <summary>
        /// Reads a file to the end as a string
        /// </summary>
        /// <param name="location">File to read</param>
        /// <returns>A string containing the contents of the file</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public static string ReadFile(this string location)
        {
            if (location.StartsWith("~", StringComparison.InvariantCulture))
            {
                location = HttpContext.Current == null ? location.Replace("~", AppDomain.CurrentDomain.BaseDirectory) : HttpContext.Current.Server.MapPath(location);
            }
            return new FileInfo(location).Read();
        }

        #endregion

        #region ReadBinary

        /// <summary>
        /// Reads a file to the end and returns a binary array
        /// </summary>
        /// <param name="file">File to open</param>
        /// <returns>A binary array containing the contents of the file</returns>
        public static byte[] ReadBinary(this FileInfo file)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (!file.Exists)
                return new byte[0];
            using (FileStream reader = file.OpenRead())
            {
                byte[] output = reader.ReadAllBinary();
                return output;
            }
        }

        /// <summary>
        /// Reads a file to the end and returns a binary array
        /// </summary>
        /// <param name="location">File to open</param>
        /// <returns>A binary array containing the contents of the file</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public static byte[] ReadBinary(this string location)
        {
            if (location.StartsWith("~", StringComparison.InvariantCulture))
            {
                location = HttpContext.Current == null ? location.Replace("~", AppDomain.CurrentDomain.BaseDirectory) : HttpContext.Current.Server.MapPath(location);
            }
            return new FileInfo(location).ReadBinary();
        }

        /// <summary>
        /// 读取文件进入流中
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <returns></returns>
        public static MemoryStream ReadFileToStream(this string path)
        {
            if (!path.ExistFile())
            {
                return null;
            }
            var bytes = path.ReadBinary();
            return new MemoryStream(bytes);
        }

        #endregion

        #region Execute

        /// <summary>
        /// Executes the file
        /// </summary>
        /// <param name="file">File to execute</param>
        /// <param name="arguments">Arguments sent to the executable</param>
        /// <param name="domain">Domain of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="user">User to run the file as</param>
        /// <param name="windowStyle">Window style</param>
        /// <param name="workingDirectory">Working directory</param>
        /// <returns>The process object created when the executable is started</returns>
        public static Process Execute(this FileInfo file, string arguments = "",
            string domain = "", string user = "", string password = "",
            ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal, string workingDirectory = "")
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (!file.Exists) { throw new FileNotFoundException("file does not exist"); }
            var info = new ProcessStartInfo { Arguments = arguments, Domain = domain, Password = new SecureString() };
            foreach (char Char in password)
                info.Password.AppendChar(Char);
            info.UserName = user;
            info.WindowStyle = windowStyle;
            info.UseShellExecute = false;
            // ReSharper disable AssignNullToNotNullAttribute
            info.WorkingDirectory = string.IsNullOrEmpty(workingDirectory) ? file.DirectoryName : workingDirectory;
            // ReSharper restore AssignNullToNotNullAttribute
            return file.Execute(info);
        }

        /// <summary>
        /// Executes the file
        /// </summary>
        /// <param name="file">File to execute</param>
        /// <param name="arguments">Arguments sent to the executable</param>
        /// <param name="domain">Domain of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="user">User to run the file as</param>
        /// <param name="windowStyle">Window style</param>
        /// <param name="workingDirectory">Working directory</param>
        /// <returns>The process object created when the executable is started</returns>
        public static Process Execute(this string file, string arguments = "",
            string domain = "", string user = "", string password = "",
            ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal, string workingDirectory = "")
        {
            if (file.IsNullOrEmpty()) { throw new ArgumentNullException("file"); }
            var info = new ProcessStartInfo { Arguments = arguments, Domain = domain, Password = new SecureString() };
            foreach (char Char in password)
                info.Password.AppendChar(Char);
            info.UserName = user;
            info.WindowStyle = windowStyle;
            info.UseShellExecute = false;
            info.WorkingDirectory = workingDirectory;
            return file.Execute(info);
        }

        /// <summary>
        /// Executes the file
        /// </summary>
        /// <param name="file">File to execute</param>
        /// <param name="info">Info used to execute the file</param>
        /// <returns>The process object created when the executable is started</returns>
        public static Process Execute(this FileInfo file, ProcessStartInfo info)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (!file.Exists) { throw new FileNotFoundException("File not found"); }
            if (info.IsNull()) { throw new ArgumentNullException("info"); }
            info.FileName = file.FullName;
            return Process.Start(info);
        }

        /// <summary>
        /// Executes the file
        /// </summary>
        /// <param name="file">File to execute</param>
        /// <param name="info">Info used to execute the file</param>
        /// <returns>The process object created when the executable is started</returns>
        public static Process Execute(this string file, ProcessStartInfo info)
        {
            if (file.IsNullOrEmpty()) { throw new ArgumentNullException("file"); }
            if (info.IsNull()) { throw new ArgumentNullException("info"); }
            info.FileName = file;
            return Process.Start(info);
        }

        #endregion

        #region Save

        /// <summary>
        /// Saves a string to a file
        /// </summary>
        /// <param name="file">File to save to</param>
        /// <param name="content">Content to save to the file</param>
        /// <param name="encodingUsing">Encoding that the content is using (defaults to ASCII)</param>
        /// <param name="mode">Mode for saving the file (defaults to Create)</param>
        /// <returns>The FileInfo object</returns>
        public static FileInfo Save(this FileInfo file, string content, FileMode mode = FileMode.Create, Encoding encodingUsing = null)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            return file.Save(encodingUsing.Check(new ASCIIEncoding()).GetBytes(content), mode);
        }

        /// <summary>
        /// Saves a byte array to a file
        /// </summary>
        /// <param name="file">File to save to</param>
        /// <param name="content">Content to save to the file</param>
        /// <param name="mode">Mode for saving the file (defaults to Create)</param>
        /// <returns>The FileInfo object</returns>
        public static FileInfo Save(this FileInfo file, byte[] content, FileMode mode = FileMode.Create)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (file.DirectoryName != null) new DirectoryInfo(file.DirectoryName).Create();
            using (FileStream writer = file.Open(mode, FileAccess.Write))
            {
                writer.Write(content, 0, content.Length);
            }
            return file;
        }

        /// <summary>
        /// Saves the string to the specified file
        /// </summary>
        /// <param name="location">Location to save the content to</param>
        /// <param name="content">Content to save the the file</param>
        /// <param name="encodingUsing">Encoding that the content is using (defaults to ASCII)</param>
        /// <param name="mode">Mode for saving the file (defaults to Create)</param>
        /// <returns>The FileInfo object associated with the location</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public static FileInfo Save(this string location, string content, FileMode mode = FileMode.Create, Encoding encodingUsing = null)
        {
            if (location.StartsWith("~", StringComparison.InvariantCulture))
            {
                location = HttpContext.Current == null ? location.Replace("~", AppDomain.CurrentDomain.BaseDirectory) : HttpContext.Current.Server.MapPath(location);
            }
            return new FileInfo(location).Save(content, mode, encodingUsing);
        }

        /// <summary>
        /// Saves a byte array to a file
        /// </summary>
        /// <param name="location">File to save to</param>
        /// <param name="content">Content to save to the file</param>
        /// <param name="mode">Mode for saving the file (defaults to Create)</param>
        /// <returns>The FileInfo object associated with the location</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public static FileInfo Save(this string location, byte[] content, FileMode mode = FileMode.Create)
        {
            if (location.StartsWith("~", StringComparison.InvariantCulture))
            {
                location = HttpContext.Current == null ? location.Replace("~", AppDomain.CurrentDomain.BaseDirectory) : HttpContext.Current.Server.MapPath(location);
            }
            return new FileInfo(location).Save(content, mode);
        }

        #endregion

        #region SaveAsync

        /// <summary>
        /// Saves a string to a file (asynchronously)
        /// </summary>
        /// <param name="file">File to save to</param>
        /// <param name="content">Content to save to the file</param>
        /// <param name="callBack">Call back function</param>
        /// <param name="stateObject">State object</param>
        /// <param name="encodingUsing">Encoding that the content is using (defaults to ASCII)</param>
        /// <param name="mode">Mode for saving the file (defaults to Create)</param>
        /// <returns>The FileInfo object</returns>
        public static FileInfo SaveAsync(this FileInfo file, string content, AsyncCallback callBack,
            object stateObject, FileMode mode = FileMode.Create, Encoding encodingUsing = null)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            return file.SaveAsync(encodingUsing.Check(new ASCIIEncoding()).GetBytes(content), callBack, stateObject, mode);
        }

        /// <summary>
        /// Saves a byte array to a file (asynchronously)
        /// </summary>
        /// <param name="file">File to save to</param>
        /// <param name="content">Content to save to the file</param>
        /// <param name="callBack">Call back function</param>
        /// <param name="stateObject">State object</param>
        /// <param name="mode">Mode for saving the file (defaults to Create)</param>
        /// <returns>The FileInfo object</returns>
        public static FileInfo SaveAsync(this FileInfo file, byte[] content, AsyncCallback callBack,
            object stateObject, FileMode mode = FileMode.Create)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (file.DirectoryName != null) new DirectoryInfo(file.DirectoryName).Create();
            using (FileStream writer = file.Open(mode, FileAccess.Write))
            {
                writer.BeginWrite(content, 0, content.Length, callBack, stateObject);
            }
            return file;
        }

        #endregion

        #region SetAttributes

        /// <summary>
        /// Sets the attributes of a file
        /// </summary>
        /// <param name="file">File</param>
        /// <param name="attributes">Attributes to set</param>
        /// <returns>The file info</returns>
        public static FileInfo SetAttributes(this FileInfo file, FileAttributes attributes)
        {
            if (file.IsNull()) { throw new ArgumentNullException("file"); }
            if (!file.Exists) { throw new FileNotFoundException("file"); }
            File.SetAttributes(file.FullName, attributes);
            return file;
        }

        #endregion

        #endregion
    }
}