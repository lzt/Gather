﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Operate.IO
{
    /// <summary>
    /// INI操作类
    /// </summary>
    public class INIOperator
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="iniPath"></param>
        public INIOperator(string iniPath)
        {
            Path = iniPath;
        }

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string defVal, Byte[] retVal, int size, string filePath);


        /// <summary>
        /// 写INI文件
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void WriteValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, Path);
        }

        /// <summary>
        /// 读取INI文件
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string ReadValue(string section, string key)
        {
            var temp = new StringBuilder(255);
            GetPrivateProfileString(section, key, "", temp, 255, Path);
            return temp.ToString();
        }

        /// <summary>
        /// 读取
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public byte[] ReadValues(string section, string key)
        {
            var temp = new byte[255];
            GetPrivateProfileString(section, key, "", temp, 255, Path);
            return temp;
        }

        /// <summary>
        /// 删除ini文件下所有段落
        /// </summary>
        public void ClearAllSection()
        {
            WriteValue(null, null, null);
        }

        /// <summary>
        /// 删除ini文件下personal段落下的所有键
        /// </summary>
        /// <param name="section"></param>
        public void ClearSection(string section)
        {
            WriteValue(section, null, null);
        }

    }
}
