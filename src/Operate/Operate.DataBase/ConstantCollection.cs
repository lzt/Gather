﻿/*
* Code By 卢志涛
*/

namespace Operate.DataBase
{
    /// <summary>
    /// 常量集合类
    /// </summary>
    public class ConstantCollection : Operate.ConstantCollection
    {
        /// <summary>
        /// 查询是针对List的模式
        /// </summary>
        public enum ListMode
        {
            /// <summary>
            /// 包含
            /// </summary>
            Include = 1,

            /// <summary>
            /// 排除
            /// </summary>
            Exclude = 0,
        }
    }
}
