﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.DataBase.Model
{
    /// <summary>
    /// 分页信息
    /// </summary>
    public class PagerInfo
    {
        /// <summary>
        /// 当前页码
        /// </summary>
        public long CurrentPageIndex { get; set; }

        /// <summary>
        /// 总分页数
        /// </summary>
        public long TotalPages { get; set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        public long TotalRecords { get; set; }
    }
}
