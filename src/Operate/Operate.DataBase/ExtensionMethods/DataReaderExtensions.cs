﻿using System;
using System.Data;

namespace Operate.DataBase.ExtensionMethods
{
    /// <summary>
    /// 数据读取器扩展方法
    /// </summary>
    public static class DataReaderExtensions
    {
        /// <summary>
        /// 读取字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="defaultValue">默认值</param>
        public static string GetString(this IDataReader r, string field, string defaultValue = "")
        {
            if (r[field] is DBNull) return defaultValue;
            return r[field].ToString();
        }

        /// <summary>
        /// 读取日期时间过去的天数
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="defaultValue"></param>
        public static string GetDays(this IDataReader r, string field, string defaultValue = "")
        {
            if (r[field] is DBNull) return defaultValue;
            var timeSpan = DateTime.Now - (DateTime)r[field];
            if (timeSpan.TotalDays < 1) return "今天";
            if (timeSpan.TotalDays < 2) return "昨天";
            return string.Format("{0}天前", Math.Floor(timeSpan.TotalDays));
        }

        /// <summary>
        /// 读取友好的日期时间字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="format">默认格式</param>
        public static string GetFriendlyTime(this IDataReader r, string field, string format = "yyyy/MM/dd")
        {
            if (r[field] is DBNull) return string.Empty;
            var now = DateTime.Now;
            var t = (DateTime)r[field];

            var timeSpan = now - t;
            var totalMinutes = timeSpan.TotalMinutes;
            if (totalMinutes < 1) return string.Format("{0}秒前", Math.Floor(timeSpan.TotalSeconds));
            if (totalMinutes < 60) return string.Format("{0}分钟前", Math.Floor(totalMinutes));

            var totalDays = (now.Date - t.Date).TotalDays;
            if(totalDays < 1) return string.Format("今天 {0}", t.ToString("HH:mm"));
            if (totalDays < 2) return string.Format("昨天 {0}", t.ToString("HH:mm"));

            return t.ToString(format);
        }

        /// <summary>
        /// 读取日期时间的年月字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="format">默认格式</param>
        public static string GetMonth(this IDataReader r, string field, string format = "yyyy/M")
        {
            if (r[field] is DBNull) return string.Empty;
            return ((DateTime)r[field]).ToString(format);
        }

        /// <summary>
        /// 读取日期时间的日期字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="format">默认格式</param>
        public static string GetDate(this IDataReader r, string field, string format = "yyyy/MM/dd")
        {
            if (r[field] is DBNull) return string.Empty;
            return ((DateTime)r[field]).ToString(format);
        }

        /// <summary>
        /// 读取日期时间的时分字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="format">默认格式</param>
        public static string GetHourMinute(this IDataReader r, string field, string format = "H:mm")
        {
            if (r[field] is DBNull) return string.Empty;
            return ((DateTime)r[field]).ToString(format);
        }

        /// <summary>
        /// 读取日期时间的日期时分字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="format">默认格式</param>
        public static string GetDateMinute(this IDataReader r, string field, string format = "yyyy/MM/dd HH:mm")
        {
            if (r[field] is DBNull) return string.Empty;
            return ((DateTime)r[field]).ToString(format);
        }

        /// <summary>
        /// 读取日期时间的日期时分秒字符串
        /// </summary>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="format">默认格式</param>
        public static string GetDateSecond(this IDataReader r, string field, string format = "yyyy/MM/dd HH:mm:ss")
        {
            if (r[field] is DBNull) return string.Empty;
            return ((DateTime)r[field]).ToString(format);
        }

        /// <summary>
        /// 读取值类型
        /// </summary>
        /// <typeparam name="T">值类型</typeparam>
        /// <param name="r">IDataReader</param>
        /// <param name="field">字段名称</param>
        /// <param name="defaultValue">默认值</param>
        public static T GetValue<T>(this IDataReader r, string field, T defaultValue = default(T)) where T : struct
        {
            if (r[field] is DBNull) return defaultValue;
            return (T)Convert.ChangeType(r[field], typeof(T));
        }
    }
}