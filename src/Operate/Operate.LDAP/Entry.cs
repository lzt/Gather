﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.DirectoryServices;
using System.Text;

#endregion

namespace Operate.LDAP
{
    /// <summary>
    /// Directory entry class
    /// </summary>
    public class Entry : IDisposable
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="directoryEntry">Directory entry for the item</param>
        public Entry(DirectoryEntry directoryEntry)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            DirectoryEntry = directoryEntry;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Properties
        /// <summary>
        /// Actual base directory entry
        /// </summary>
        public virtual DirectoryEntry DirectoryEntry { get; set; }

        /// <summary>
        /// Email property for this entry
        /// </summary>
        public virtual string Email
        {
            get { return (string)GetValue("mail"); }
            set { SetValue("mail", value); }
        }

        /// <summary>
        /// distinguished name property for this entry
        /// </summary>
        public virtual string DistinguishedName
        {
            get { return (string)GetValue("distinguishedname"); }
            set { SetValue("distinguishedname", value); }
        }

        /// <summary>
        /// country code property for this entry
        /// </summary>
        public virtual string CountryCode
        {
            get { return (string)GetValue("countrycode"); }
            set { SetValue("countrycode", value); }
        }

        /// <summary>
        /// company property for this entry
        /// </summary>
        public virtual string Company
        {
            get { return (string)GetValue("company"); }
            set { SetValue("company", value); }
        }

        /// <summary>
        /// MemberOf property for this entry
        /// </summary>
        public virtual IEnumerable<string> MemberOf
        {
            get
            {
                var values = new List<string>();
                PropertyValueCollection collection = DirectoryEntry.Properties["memberof"];
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (object item in collection)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    values.Add((string)item);
                }
                return values;
            }
        }

        /// <summary>
        /// display name property for this entry
        /// </summary>
        public virtual string DisplayName
        {
            get { return (string)GetValue("displayname"); }
            set { SetValue("displayname", value); }
        }

        /// <summary>
        /// initials property for this entry
        /// </summary>
        public virtual string Initials
        {
            get { return (string)GetValue("initials"); }
            set { SetValue("initials", value); }
        }

        /// <summary>
        /// title property for this entry
        /// </summary>
        public virtual string Title
        {
            get { return (string)GetValue("title"); }
            set { SetValue("title", value); }
        }

        /// <summary>
        /// samaccountname property for this entry
        /// </summary>
        public virtual string SamAccountName
        {
            get { return (string)GetValue("samaccountname"); }
            set { SetValue("samaccountname", value); }
        }

        /// <summary>
        /// givenname property for this entry
        /// </summary>
        public virtual string GivenName
        {
            get { return (string)GetValue("givenname"); }
            set { SetValue("givenname", value); }
        }

        /// <summary>
        /// cn property for this entry
        /// </summary>
        public virtual string CN
        {
            get { return (string)GetValue("cn"); }
            set { SetValue("cn", value); }
        }

        /// <summary>
        /// name property for this entry
        /// </summary>
        public virtual string Name
        {
            get { return (string)GetValue("name"); }
            set { SetValue("name", value); }
        }

        /// <summary>
        /// office property for this entry
        /// </summary>
        public virtual string Office
        {
            get { return (string)GetValue("physicaldeliveryofficename"); }
            set { SetValue("physicaldeliveryofficename", value); }
        }

        /// <summary>
        /// telephone number property for this entry
        /// </summary>
        public virtual string TelephoneNumber
        {
            get { return (string)GetValue("telephonenumber"); }
            set { SetValue("telephonenumber", value); }
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Saves any changes that have been made
        /// </summary>
        public virtual void Save()
        {
            // ReSharper disable once NotResolvedInText
            if (DirectoryEntry.IsNull()) { throw new ArgumentNullException("DirectoryEntry"); }
            DirectoryEntry.CommitChanges();
        }

        /// <summary>
        /// Gets a value from the entry
        /// </summary>
        /// <param name="property">Property you want the information about</param>
        /// <returns>an object containing the property's information</returns>
        public virtual object GetValue(string property)
        {
            PropertyValueCollection collection = DirectoryEntry.Properties[property];
            return collection != null ? collection.Value : null;
        }

        /// <summary>
        /// Gets a value from the entry
        /// </summary>
        /// <param name="property">Property you want the information about</param>
        /// <param name="index">Index of the property to return</param>
        /// <returns>an object containing the property's information</returns>
        public virtual object GetValue(string property, int index)
        {
            PropertyValueCollection collection = DirectoryEntry.Properties[property];
            return collection != null ? collection[index] : null;
        }

        /// <summary>
        /// Sets a property of the entry to a specific value
        /// </summary>
        /// <param name="property">Property of the entry to set</param>
        /// <param name="value">Value to set the property to</param>
        public virtual void SetValue(string property, object value)
        {
            PropertyValueCollection collection = DirectoryEntry.Properties[property];
            if (collection != null)
                collection.Value = value;
        }

        /// <summary>
        /// Sets a property of the entry to a specific value
        /// </summary>
        /// <param name="property">Property of the entry to set</param>
        /// <param name="index">Index of the property to set</param>
        /// <param name="value">Value to set the property to</param>
        public virtual void SetValue(string property, int index, object value)
        {
            PropertyValueCollection collection = DirectoryEntry.Properties[property];
            if (collection != null)
                collection[index] = value;
        }

        /// <summary>
        /// Exports the entry as a string
        /// </summary>
        /// <returns>The entry as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (PropertyValueCollection property in DirectoryEntry.Properties)
            {
                builder.Append(property.PropertyName).Append(" = ").AppendLine(property.Value.ToString());
            }
            return builder.ToString();
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Disposes the object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of the objects
        /// </summary>
        /// <param name="disposing">True to dispose of all resources, false only disposes of native resources</param>
        protected virtual void Dispose(bool disposing)
        {
            if (DirectoryEntry != null)
            {
                DirectoryEntry.Dispose();
                DirectoryEntry = null;
            }
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~Entry()
        {
            Dispose(false);
        }

        #endregion
    }
}