﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.DirectoryServices;
using System.Globalization;
using System.Linq;
using Operate.ExtensionMethods;

#endregion

namespace Operate.LDAP
{
    /// <summary>
    /// Class for helping with AD
    /// </summary>
    public class Directory : IDisposable
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userName">User name used to log in</param>
        /// <param name="password">Password used to log in</param>
        /// <param name="path">Path of the LDAP server</param>
        /// <param name="query">Query to use in the search</param>
        public Directory(string query, string userName, string password, string path)
        {
            _entry = new DirectoryEntry(path, userName, password, AuthenticationTypes.Secure);
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Path = path;
            UserName = userName;
            Password = password;
            Query = query;
            _searcher = new DirectorySearcher(_entry) { Filter = query, PageSize = 1000 };
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Public Functions

        #region Authenticate

        /// <summary>
        /// Checks to see if the person was authenticated
        /// </summary>
        /// <returns>true if they were authenticated properly, false otherwise</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public virtual bool Authenticate()
        {
            try
            {
                return !string.IsNullOrEmpty(_entry.Guid.ToString().Trim());
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
            return false;
        }
        #endregion

        #region Close

        /// <summary>
        /// Closes the directory
        /// </summary>
        public virtual void Close()
        {
            _entry.Close();
        }

        #endregion

        #region FindActiveGroupMembers

        /// <summary>
        /// Returns a group's list of members who are active
        /// </summary>
        /// <param name="groupName">The group's name</param>
        /// <param name="recursive">Should sub groups' members be added instead of the sub group itself?</param>
        /// <returns>A list of the members</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "Entry"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public virtual ICollection<Entry> FindActiveGroupMembers(string groupName, bool recursive = false)
        {
            try
            {
                Entry group = FindGroup(groupName);
                ICollection<Entry> entries = FindActiveUsersAndGroups("memberOf=" + group.DistinguishedName);
                if (recursive)
                {
                    var returnValue = new List<Entry>();
                    foreach (Entry entry in entries)
                    {
                        Entry tempEntry = FindGroup(entry.CN);
                        if (tempEntry == null)
                            returnValue.Add(entry);
                        else
                            returnValue.Add(FindActiveGroupMembers(tempEntry.CN, true));
                    }
                    return returnValue;
                }
                return entries;
            }
            catch
            {
                return new List<Entry>();
            }
        }

        #endregion

        #region FindActiveGroups

        /// <summary>
        /// Finds all active groups
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all active groups' entries</returns>
        public virtual ICollection<Entry> FindActiveGroups(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&((userAccountControl:1.2.840.113556.1.4.803:=512)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(cn=*$)))({0}))", filter);
            return FindGroups(filter);
        }

        #endregion

        #region FindActiveUsers

        /// <summary>
        /// Finds all active users
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all active users' entries</returns>
        public virtual ICollection<Entry> FindActiveUsers(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&((userAccountControl:1.2.840.113556.1.4.803:=512)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(cn=*$)))({0}))", filter);
            return FindUsers(filter);
        }

        #endregion

        #region FindActiveUsersAndGroups

        /// <summary>
        /// Finds all active users and groups
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all active groups' entries</returns>
        public virtual ICollection<Entry> FindActiveUsersAndGroups(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&((userAccountControl:1.2.840.113556.1.4.803:=512)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(cn=*$)))({0}))", filter);
            return FindUsersAndGroups(filter);
        }

        #endregion

        #region FindAll

        /// <summary>
        /// Finds all entries that match the query
        /// </summary>
        /// <returns>A list of all entries that match the query</returns>
        public virtual ICollection<Entry> FindAll()
        {
            var returnedResults = new List<Entry>();
            using (SearchResultCollection results = _searcher.FindAll())
            {
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (SearchResult result in results)
                    // ReSharper restore LoopCanBeConvertedToQuery
                    returnedResults.Add(new Entry(result.GetDirectoryEntry()));
            }
            return returnedResults;
        }

        #endregion

        #region FindComputers

        /// <summary>
        /// Finds all computers
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all computers meeting the specified Filter</returns>
        public virtual ICollection<Entry> FindComputers(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&(objectClass=computer)({0}))", filter);
            _searcher.Filter = filter;
            return FindAll();
        }

        #endregion

        #region FindGroupMembers

        /// <summary>
        /// Returns a group's list of members
        /// </summary>
        /// <param name="groupName">The group's name</param>
        /// <param name="recursive">Should sub groups' members be added instead of the sub group itself?</param>
        /// <returns>A list of the members</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "Entry"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public virtual ICollection<Entry> FindGroupMembers(string groupName, bool recursive = false)
        {
            try
            {
                Entry group = FindGroup(groupName);
                ICollection<Entry> entries = FindUsersAndGroups("memberOf=" + group.DistinguishedName);
                if (recursive)
                {
                    var returnValue = new List<Entry>();
                    foreach (Entry entry in entries)
                    {
                        Entry tempEntry = FindGroup(entry.CN);
                        if (tempEntry == null)
                            returnValue.Add(entry);
                        else
                            returnValue.Add(FindActiveGroupMembers(tempEntry.CN, true));
                    }
                    return returnValue;
                }
                return entries;
            }
            catch
            {
                return new List<Entry>();
            }
        }

        #endregion

        #region FindGroup

        /// <summary>
        /// Finds a specific group
        /// </summary>
        /// <param name="groupName">Name of the group to find</param>
        /// <returns>The group specified</returns>
        public virtual Entry FindGroup(string groupName)
        {
            return FindGroups("cn=" + groupName).FirstOrDefault();
        }

        #endregion

        #region FindGroups

        /// <summary>
        /// Finds all groups
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all groups meeting the specified Filter</returns>
        public virtual ICollection<Entry> FindGroups(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&(objectClass=Group)(objectCategory=Group)({0}))", filter);
            _searcher.Filter = filter;
            return FindAll();
        }

        #endregion

        #region FindOne

        /// <summary>
        /// Finds one entry that matches the query
        /// </summary>
        /// <returns>A single entry matching the query</returns>
        public virtual Entry FindOne()
        {
            return new Entry(_searcher.FindOne().GetDirectoryEntry());
        }

        #endregion

        #region FindUsersAndGroups

        /// <summary>
        /// Finds all users and groups
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all users and groups meeting the specified Filter</returns>
        public virtual ICollection<Entry> FindUsersAndGroups(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&(|(&(objectClass=Group)(objectCategory=Group))(&(objectClass=User)(objectCategory=Person)))({0}))", filter);
            _searcher.Filter = filter;
            return FindAll();
        }

        #endregion

        #region FindUserByUserName

        /// <summary>
        /// Finds a user by his user name
        /// </summary>
        /// <param name="userName">User name to search by</param>
        /// <returns>The user's entry</returns>
        public virtual Entry FindUserByUserName(string userName)
        {
            if (userName.IsNullOrEmpty()) { throw new ArgumentNullException("userName"); }
            return FindUsers("samAccountName=" + userName).FirstOrDefault();
        }

        #endregion

        #region FindUsers

        /// <summary>
        /// Finds all users
        /// </summary>
        /// <param name="filter">Filter used to modify the query</param>
        /// <param name="args">Additional arguments (used in string formatting</param>
        /// <returns>A list of all users meeting the specified Filter</returns>
        public virtual ICollection<Entry> FindUsers(string filter, params object[] args)
        {
            filter = string.Format(CultureInfo.InvariantCulture, filter, args);
            filter = string.Format(CultureInfo.InvariantCulture, "(&(objectClass=User)(objectCategory=Person)({0}))", filter);
            _searcher.Filter = filter;
            return FindAll();
        }

        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// Path of the server
        /// </summary>
        public virtual string Path
        {
            get { return _path; }
            set
            {
                _path = value;
                if (_entry != null)
                {
                    _entry.Close();
                    _entry.Dispose();
                    _entry = null;
                }
                if (_searcher != null)
                {
                    _searcher.Dispose();
                    _searcher = null;
                }
                _entry = new DirectoryEntry(_path, _userName, _password, AuthenticationTypes.Secure);
                _searcher = new DirectorySearcher(_entry) { Filter = Query, PageSize = 1000 };
            }
        }

        /// <summary>
        /// User name used to log in
        /// </summary>
        public virtual string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                if (_entry != null)
                {
                    _entry.Close();
                    _entry.Dispose();
                    _entry = null;
                }
                if (_searcher != null)
                {
                    _searcher.Dispose();
                    _searcher = null;
                }
                _entry = new DirectoryEntry(_path, _userName, _password, AuthenticationTypes.Secure);
                _searcher = new DirectorySearcher(_entry) { Filter = Query, PageSize = 1000 };
            }
        }

        /// <summary>
        /// Password used to log in
        /// </summary>
        public virtual string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                if (_entry != null)
                {
                    _entry.Close();
                    _entry.Dispose();
                    _entry = null;
                }
                if (_searcher != null)
                {
                    _searcher.Dispose();
                    _searcher = null;
                }
                _entry = new DirectoryEntry(_path, _userName, _password, AuthenticationTypes.Secure);
                _searcher = new DirectorySearcher(_entry) { Filter = Query, PageSize = 1000 };
            }
        }

        /// <summary>
        /// The query that is being made
        /// </summary>
        public virtual string Query
        {
            get { return _query; }
            set
            {
                _query = value;
                _searcher.Filter = _query;
            }
        }

        /// <summary>
        /// Decides what to sort the information by
        /// </summary>
        public virtual string SortBy
        {
            get { return _sortBy; }
            set
            {
                _sortBy = value;
                _searcher.Sort.PropertyName = _sortBy;
                _searcher.Sort.Direction = SortDirection.Ascending;
            }
        }
        #endregion

        #region Private Variables
        private string _path = "";
        private string _userName = "";
        private string _password = "";
        private DirectoryEntry _entry;
        private string _query = "";
        private DirectorySearcher _searcher;
        private string _sortBy = "";
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Disposes of the directory object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of the objects
        /// </summary>
        /// <param name="disposing">True to dispose of all resources, false only disposes of native resources</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_entry != null)
            {
                _entry.Close();
                _entry.Dispose();
                _entry = null;
            }
            if (_searcher != null)
            {
                _searcher.Dispose();
                _searcher = null;
            }
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~Directory()
        {
            Dispose(false);
        }

        #endregion
    }
}