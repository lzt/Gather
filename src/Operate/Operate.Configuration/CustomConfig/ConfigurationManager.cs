﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Reflection;
using System.Text;
using Operate.Configuration.CustomConfig.Interfaces;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Configuration.CustomConfig
{
    /// <summary>
    /// Config manager
    /// </summary>
    public class ConfigurationManager
    {
        #region Constructor

        #endregion

        #region Public Functions

        #region RegisterConfigFile

        /// <summary>
        /// Registers a config file
        /// </summary>
        /// <typeparam name="TConfigType">The config object type to register</typeparam>
        public static void RegisterConfigFile<TConfigType>() where TConfigType : Config<TConfigType>, new()
        {
            RegisterConfigFile(new TConfigType());
        }

        /// <summary>
        /// Registers a config file
        /// </summary>
        /// <param name="configObject">Config object to register</param>
        public static void RegisterConfigFile(IConfig configObject)
        {
            if (configObject == null) throw new ArgumentNullException("configObject");
            if (ConfigFiles.ContainsKey(configObject.Name)) return;
            configObject.Load();
            ConfigFiles.Add(configObject.Name, configObject);
        }

        /// <summary>
        /// Registers a set of config file
        /// </summary>
        /// <param name="configObjects">Config objects to register</param>
        public static void RegisterConfigFile(IEnumerable<IConfig> configObjects)
        {
            if (configObjects == null) throw new ArgumentNullException("configObjects");
            foreach (IConfig configObject in configObjects)
                RegisterConfigFile(configObject);
        }

        /// <summary>
        /// Registers all config files in an assembly
        /// </summary>
        /// <param name="assemblyContainingConfig">Assembly to search</param>
        public static void RegisterConfigFile(Assembly assemblyContainingConfig)
        {
            if (assemblyContainingConfig == null) throw new ArgumentNullException("assemblyContainingConfig");
            RegisterConfigFile(assemblyContainingConfig.Objects<IConfig>());
        }

        #endregion

        #region GetConfigFile

        /// <summary>
        /// Gets a specified config file
        /// </summary>
        /// <typeparam name="T">Type of the config object</typeparam>
        /// <param name="name">Name of the config object</param>
        /// <returns>The config object specified</returns>
        public static T GetConfigFile<T>(string name)
        {
            if (name == null) throw new ArgumentException("The config object was not found or was not of the type specified.");
            return (T)ConfigFiles[name];
        }

        #endregion

        #region ContainsConfigFile

        /// <summary>
        /// Determines if a specified config file is registered
        /// </summary>
        /// <typeparam name="T">Type of the config object</typeparam>
        /// <param name="name">Name of the config object</param>
        /// <returns>The config object specified</returns>
        [Pure]
        public static bool ContainsConfigFile<T>(string name)
        {
            return ConfigFiles.ContainsKey(name) && ConfigFiles[name] is T;
        }

        #endregion

        #region ToString

        /// <summary>
        /// Outputs all of the configuration items as an HTML list
        /// </summary>
        /// <returns>All configs as a string list</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("<ul>").Append("<li>").Append(ConfigFiles.Count).Append("</li>");
            foreach (string name in ConfigFiles.Keys)
                builder.Append("<li>").Append(name).Append(":").Append(ConfigFiles[name].GetType().FullName).Append("</li>");
            builder.Append("</ul>");
            return builder.ToString();
        }

        #endregion

        #endregion

        #region Private fields

        private static readonly Dictionary<string, IConfig> ConfigFiles = new Dictionary<string, IConfig>();

        #endregion
    }
}