﻿using System.Collections.Generic;
using Operate.ExtensionMethods;

namespace Operate.Configuration.ConfigSet
{
    /// <summary>
    /// Config操作
    /// </summary>
    public class Config
    {
        #region Properties

        /// <summary>
        /// Config
        /// </summary>
        /// <param name="xpath"></param>
        public Config(string xpath)
        {
            Xpath = xpath;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// 配置Xpath
        /// </summary>
        public string Xpath { get; }

        #endregion

        #region Methods

        /// <summary>
        /// 根据配置中的Key获取值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string Get(string key)
        {
            var setting = ConfigManager.GetConfigCollection(Xpath);
            return setting[key];
        }

        /// <summary>
        /// 获取配置中所有Key
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllKey()
        {
            var setting = ConfigManager.GetConfigCollection(Xpath);
            return setting.AllKeys.ToList();
        }

        /// <summary>
        /// 获取配置中配置项的个数
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            var setting = ConfigManager.GetConfigCollection(Xpath);
            return setting.AllKeys.Length;
        }

        /// <summary>
        /// 获取配置中是否有配置项
        /// </summary>
        /// <returns></returns>
        public bool HasKey()
        {
            var setting = ConfigManager.GetConfigCollection(Xpath);
            return setting.HasKeys();
        }

        #endregion
    }
}
