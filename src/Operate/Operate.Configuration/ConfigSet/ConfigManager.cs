﻿using System;
using System.Collections.Specialized;
using System.Xml;
using Operate.Xml;

namespace Operate.Configuration.ConfigSet
{
    /// <summary>
    /// ConfigManager
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// GlobalConfig
        /// </summary>
        public static Config GlobalConfig;

        /// <summary>
        /// CacheConfig
        /// </summary>
        public static Config CacheConfig;

        /// <summary>
        /// TemplateConfig
        /// </summary>
        public static Config TemplateConfig;

        private static string _path;

        ///// <summary>
        ///// XML读取类
        ///// </summary>
        //private static XmlProcess _xmlFileOption;

        static ConfigManager()
        {
            Init();

            GlobalConfig = new Config("/configuration/global");
            CacheConfig = new Config("/configuration/cache");
            TemplateConfig = new Config("/configuration/template");
        }

        /// <summary>
        /// Init
        /// </summary>
        private static void Init()
        {
            _path = "ConfigSet.Config";
        }

        /// <summary>
        /// GetConfigCollection
        /// </summary>
        /// <param name="xpath"></param>
        /// <returns></returns>
        public static NameValueCollection GetConfigCollection(string xpath)
        {
            var xmlFileOption = new XmlProcess(_path);
            var settings = new NameValueCollection();
            if (!xmlFileOption.XmlFileExist)
            {
                throw new Exception("文件不存在");
            }

            var globalNode = xmlFileOption.GetDocument().SelectSingleNode(xpath);
            // ReSharper disable once UseNullPropagation
            if (globalNode != null)
            {
                var globalSet = globalNode.SelectNodes("add");
                if (globalSet != null)
                {
                    foreach (XmlNode node in globalSet)
                    {
                        if (node.Attributes != null)
                        {
                            if (node.Attributes["key"] != null && node.Attributes["value"] != null)
                            {
                                var k = node.Attributes["key"].Value;
                                var v = node.Attributes["value"].Value;
                                if (!string.IsNullOrEmpty(k))
                                {
                                    settings.Add(k, v);
                                }
                            }
                        }
                    }
                }
            }

            return settings;
        }
    }
}
