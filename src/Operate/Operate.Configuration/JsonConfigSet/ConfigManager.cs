﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Newtonsoft.Json;
using Operate.Xml;

namespace Operate.Configuration.JsonConfigSet
{
    /// <summary>
    /// ConfigurationManager
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// Path
        /// </summary>
        public static string Path;

        static ConfigManager()
        {
            Init();
        }

        /// <summary>
        /// Init
        /// </summary>
        private static void Init()
        {
            Path = "ConfigSet.JsonConfig";
        }

        /// <summary>
        /// GetConfigCollection
        /// </summary>
        /// <returns></returns>
        public static dynamic GetConfigCollection()
        {
            dynamic settings = null;
            string filePath = AppDomain.CurrentDomain.BaseDirectory + Path;
            var jsonString = File.ReadAllText(filePath);

            if (!string.IsNullOrWhiteSpace(jsonString))
            {
                var o = JsonConvert.DeserializeObject(jsonString);

                if (o!=null)
                {
                    settings= o;
                }
            }

            return settings;
        }
    }
}
