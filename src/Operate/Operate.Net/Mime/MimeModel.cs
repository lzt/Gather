﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.Net.Mime
{
    /// <summary>
    /// ContentType定义
    /// </summary>
    [Serializable]
    public class MimeModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// 内容类型
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// 内容类型别名
        /// </summary>
        public List<string> Alias { get; set; }
    }
}
