﻿using System.Collections.Generic;

namespace Operate.Net.Mime
{
    /// <summary>
    /// MimeCollection
    /// </summary>
    public class MimeCollection
    {
        /// <summary>
        /// Any mime：.*=application/octet-stream
        /// </summary>
        public static MimeModel Any
        {
            get
            {
                return new MimeModel
                {
                    Name = "Any",
                    Extension = ".*",
                    ContentType = "application/octet-stream",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Status001 mime：.001=application/x-001
        /// </summary>
        public static MimeModel Status001
        {
            get
            {
                return new MimeModel
                {
                    Name = "Status001",
                    Extension = ".001",
                    ContentType = "application/x-001",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Status301 mime：.301=application/x-301
        /// </summary>
        public static MimeModel Status301
        {
            get
            {
                return new MimeModel
                {
                    Name = "Status301",
                    Extension = ".301",
                    ContentType = "application/x-301",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Status323 mime：.323=text/h323
        /// </summary>
        public static MimeModel Status323
        {
            get
            {
                return new MimeModel
                {
                    Name = "Status323",
                    Extension = ".323",
                    ContentType = "text/h323",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Status906 mime：.906=application/x-906
        /// </summary>
        public static MimeModel Status906
        {
            get
            {
                return new MimeModel
                {
                    Name = "Status906",
                    Extension = ".906",
                    ContentType = "application/x-906",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Status907 mime：.907=drawing/907
        /// </summary>
        public static MimeModel Status907
        {
            get
            {
                return new MimeModel
                {
                    Name = "Status907",
                    Extension = ".907",
                    ContentType = "drawing/907",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// A11 mime：.a11=application/x-a11
        /// </summary>
        public static MimeModel A11
        {
            get
            {
                return new MimeModel
                {
                    Name = "A11",
                    Extension = ".a11",
                    ContentType = "application/x-a11",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Acp mime：.acp=audio/x-mei-aac
        /// </summary>
        public static MimeModel Acp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Acp",
                    Extension = ".acp",
                    ContentType = "audio/x-mei-aac",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ai mime：.ai=application/postscript
        /// </summary>
        public static MimeModel Ai
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ai",
                    Extension = ".ai",
                    ContentType = "application/postscript",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Aif mime：.aif=audio/aiff
        /// </summary>
        public static MimeModel Aif
        {
            get
            {
                return new MimeModel
                {
                    Name = "Aif",
                    Extension = ".aif",
                    ContentType = "audio/aiff",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Aifc mime：.aifc=audio/aiff
        /// </summary>
        public static MimeModel Aifc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Aifc",
                    Extension = ".aifc",
                    ContentType = "audio/aiff",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Aiff mime：.aiff=audio/aiff
        /// </summary>
        public static MimeModel Aiff
        {
            get
            {
                return new MimeModel
                {
                    Name = "Aiff",
                    Extension = ".aiff",
                    ContentType = "audio/aiff",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Anv mime：.anv=application/x-anv
        /// </summary>
        public static MimeModel Anv
        {
            get
            {
                return new MimeModel
                {
                    Name = "Anv",
                    Extension = ".anv",
                    ContentType = "application/x-anv",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Asa mime：.asa=text/asa
        /// </summary>
        public static MimeModel Asa
        {
            get
            {
                return new MimeModel
                {
                    Name = "Asa",
                    Extension = ".asa",
                    ContentType = "text/asa",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Asf mime：.asf=video/x-ms-asf
        /// </summary>
        public static MimeModel Asf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Asf",
                    Extension = ".asf",
                    ContentType = "video/x-ms-asf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Asp mime：.asp=text/asp
        /// </summary>
        public static MimeModel Asp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Asp",
                    Extension = ".asp",
                    ContentType = "text/asp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Asx mime：.asx=video/x-ms-asf
        /// </summary>
        public static MimeModel Asx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Asx",
                    Extension = ".asx",
                    ContentType = "video/x-ms-asf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Au mime：.au=audio/basic
        /// </summary>
        public static MimeModel Au
        {
            get
            {
                return new MimeModel
                {
                    Name = "Au",
                    Extension = ".au",
                    ContentType = "audio/basic",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Avi mime：.avi=video/avi
        /// </summary>
        public static MimeModel Avi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Avi",
                    Extension = ".avi",
                    ContentType = "video/avi",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Awf mime：.awf=application/vnd.adobe.workflow
        /// </summary>
        public static MimeModel Awf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Awf",
                    Extension = ".awf",
                    ContentType = "application/vnd.adobe.workflow",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Biz mime：.biz=text/xml
        /// </summary>
        public static MimeModel Biz
        {
            get
            {
                return new MimeModel
                {
                    Name = "Biz",
                    Extension = ".biz",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Bmp mime：.bmp=application/x-bmp
        /// </summary>
        public static MimeModel Bmp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Bmp",
                    Extension = ".bmp",
                    ContentType = "application/x-bmp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Bot mime：.bot=application/x-bot
        /// </summary>
        public static MimeModel Bot
        {
            get
            {
                return new MimeModel
                {
                    Name = "Bot",
                    Extension = ".bot",
                    ContentType = "application/x-bot",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// C4t mime：.c4t=application/x-c4t
        /// </summary>
        public static MimeModel C4T
        {
            get
            {
                return new MimeModel
                {
                    Name = "C4t",
                    Extension = ".c4t",
                    ContentType = "application/x-c4t",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// C90 mime：.c90=application/x-c90
        /// </summary>
        public static MimeModel C90
        {
            get
            {
                return new MimeModel
                {
                    Name = "C90",
                    Extension = ".c90",
                    ContentType = "application/x-c90",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cal mime：.cal=application/x-cals
        /// </summary>
        public static MimeModel Cal
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cal",
                    Extension = ".cal",
                    ContentType = "application/x-cals",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cat mime：.cat=application/s-pki.seccat
        /// </summary>
        public static MimeModel Cat
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cat",
                    Extension = ".cat",
                    ContentType = "application/s-pki.seccat",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cdf mime：.cdf=application/x-netcdf
        /// </summary>
        public static MimeModel Cdf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cdf",
                    Extension = ".cdf",
                    ContentType = "application/x-netcdf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cdr mime：.cdr=application/x-cdr
        /// </summary>
        public static MimeModel Cdr
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cdr",
                    Extension = ".cdr",
                    ContentType = "application/x-cdr",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cel mime：.cel=application/x-cel
        /// </summary>
        public static MimeModel Cel
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cel",
                    Extension = ".cel",
                    ContentType = "application/x-cel",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cer mime：.cer=application/x-x509-ca-cert
        /// </summary>
        public static MimeModel Cer
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cer",
                    Extension = ".cer",
                    ContentType = "application/x-x509-ca-cert",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cg4 mime：.cg4=application/x-g4
        /// </summary>
        public static MimeModel Cg4
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cg4",
                    Extension = ".cg4",
                    ContentType = "application/x-g4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cgm mime：.cgm=application/x-cgm
        /// </summary>
        public static MimeModel Cgm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cgm",
                    Extension = ".cgm",
                    ContentType = "application/x-cgm",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cit mime：.cit=application/x-cit
        /// </summary>
        public static MimeModel Cit
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cit",
                    Extension = ".cit",
                    ContentType = "application/x-cit",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Class mime：.class=java/*
        /// </summary>
        public static MimeModel Class
        {
            get
            {
                return new MimeModel
                {
                    Name = "Class",
                    Extension = ".class",
                    ContentType = "java/*",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cml mime：.cml=text/xml
        /// </summary>
        public static MimeModel Cml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cml",
                    Extension = ".cml",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cmp mime：.cmp=application/x-cmp
        /// </summary>
        public static MimeModel Cmp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cmp",
                    Extension = ".cmp",
                    ContentType = "application/x-cmp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cmx mime：.cmx=application/x-cmx
        /// </summary>
        public static MimeModel Cmx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cmx",
                    Extension = ".cmx",
                    ContentType = "application/x-cmx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cot mime：.cot=application/x-cot
        /// </summary>
        public static MimeModel Cot
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cot",
                    Extension = ".cot",
                    ContentType = "application/x-cot",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Crl mime：.crl=application/pkix-crl
        /// </summary>
        public static MimeModel Crl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Crl",
                    Extension = ".crl",
                    ContentType = "application/pkix-crl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Crt mime：.crt=application/x-x509-ca-cert
        /// </summary>
        public static MimeModel Crt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Crt",
                    Extension = ".crt",
                    ContentType = "application/x-x509-ca-cert",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Csi mime：.csi=application/x-csi
        /// </summary>
        public static MimeModel Csi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Csi",
                    Extension = ".csi",
                    ContentType = "application/x-csi",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Css mime：.css=text/css
        /// </summary>
        public static MimeModel Css
        {
            get
            {
                return new MimeModel
                {
                    Name = "Css",
                    Extension = ".css",
                    ContentType = "text/css",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Cut mime：.cut=application/x-cut
        /// </summary>
        public static MimeModel Cut
        {
            get
            {
                return new MimeModel
                {
                    Name = "Cut",
                    Extension = ".cut",
                    ContentType = "application/x-cut",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dbf mime：.dbf=application/x-dbf
        /// </summary>
        public static MimeModel Dbf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dbf",
                    Extension = ".dbf",
                    ContentType = "application/x-dbf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dbm mime：.dbm=application/x-dbm
        /// </summary>
        public static MimeModel Dbm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dbm",
                    Extension = ".dbm",
                    ContentType = "application/x-dbm",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dbx mime：.dbx=application/x-dbx
        /// </summary>
        public static MimeModel Dbx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dbx",
                    Extension = ".dbx",
                    ContentType = "application/x-dbx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dcd mime：.dcd=text/xml
        /// </summary>
        public static MimeModel Dcd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dcd",
                    Extension = ".dcd",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dcx mime：.dcx=application/x-dcx
        /// </summary>
        public static MimeModel Dcx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dcx",
                    Extension = ".dcx",
                    ContentType = "application/x-dcx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Der mime：.der=application/x-x509-ca-cert
        /// </summary>
        public static MimeModel Der
        {
            get
            {
                return new MimeModel
                {
                    Name = "Der",
                    Extension = ".der",
                    ContentType = "application/x-x509-ca-cert",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dgn mime：.dgn=application/x-dgn
        /// </summary>
        public static MimeModel Dgn
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dgn",
                    Extension = ".dgn",
                    ContentType = "application/x-dgn",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dib mime：.dib=application/x-dib
        /// </summary>
        public static MimeModel Dib
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dib",
                    Extension = ".dib",
                    ContentType = "application/x-dib",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dll mime：.dll=application/x-msdownload
        /// </summary>
        public static MimeModel Dll
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dll",
                    Extension = ".dll",
                    ContentType = "application/x-msdownload",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Doc mime：.doc=application/msword
        /// </summary>
        public static MimeModel Doc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Doc",
                    Extension = ".doc",
                    ContentType = "application/msword",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dot mime：.dot=application/msword
        /// </summary>
        public static MimeModel Dot
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dot",
                    Extension = ".dot",
                    ContentType = "application/msword",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Drw mime：.drw=application/x-drw
        /// </summary>
        public static MimeModel Drw
        {
            get
            {
                return new MimeModel
                {
                    Name = "Drw",
                    Extension = ".drw",
                    ContentType = "application/x-drw",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dtd mime：.dtd=text/xml
        /// </summary>
        public static MimeModel Dtd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dtd",
                    Extension = ".dtd",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dwf mime：.dwf=Model/vnd.dwf， .dwf=application/x-dwf
        /// </summary>
        public static MimeModel Dwf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dwf",
                    Extension = ".dwf",
                    ContentType = "Model/vnd.dwf",
                    Alias = new List<string>
                    {
                         "application/x-dwf",
                    },
                };
            }
        }

        /// <summary>
        /// Dwg mime：.dwg=application/x-dwg
        /// </summary>
        public static MimeModel Dwg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dwg",
                    Extension = ".dwg",
                    ContentType = "application/x-dwg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dxb mime：.dxb=application/x-dxb
        /// </summary>
        public static MimeModel Dxb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dxb",
                    Extension = ".dxb",
                    ContentType = "application/x-dxb",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Dxf mime：.dxf=application/x-dxf
        /// </summary>
        public static MimeModel Dxf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dxf",
                    Extension = ".dxf",
                    ContentType = "application/x-dxf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Edn mime：.edn=application/vnd.adobe.edn
        /// </summary>
        public static MimeModel Edn
        {
            get
            {
                return new MimeModel
                {
                    Name = "Edn",
                    Extension = ".edn",
                    ContentType = "application/vnd.adobe.edn",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Emf mime：.emf=application/x-emf
        /// </summary>
        public static MimeModel Emf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Emf",
                    Extension = ".emf",
                    ContentType = "application/x-emf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Eml mime：.eml=message/rfc822
        /// </summary>
        public static MimeModel Eml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Eml",
                    Extension = ".eml",
                    ContentType = "message/rfc822",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ent mime：.ent=text/xml
        /// </summary>
        public static MimeModel Ent
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ent",
                    Extension = ".ent",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Epi mime：.epi=application/x-epi
        /// </summary>
        public static MimeModel Epi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Epi",
                    Extension = ".epi",
                    ContentType = "application/x-epi",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Eps mime：.eps=application/x-ps，.eps=application/postscript
        /// </summary>
        public static MimeModel Eps
        {
            get
            {
                return new MimeModel
                {
                    Name = "Eps",
                    Extension = ".eps",
                    ContentType = "application/x-ps",
                    Alias = new List<string>
                    {
                        "application/postscript"
                    },
                };
            }
        }

        /// <summary>
        /// Etd mime：.etd=application/x-ebx
        /// </summary>
        public static MimeModel Etd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Etd",
                    Extension = ".etd",
                    ContentType = "application/x-ebx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Exe mime：.exe=application/x-msdownload
        /// </summary>
        public static MimeModel Exe
        {
            get
            {
                return new MimeModel
                {
                    Name = "Exe",
                    Extension = ".exe",
                    ContentType = "application/x-msdownload",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Fax mime：.fax=image/fax
        /// </summary>
        public static MimeModel Fax
        {
            get
            {
                return new MimeModel
                {
                    Name = "Fax",
                    Extension = ".fax",
                    ContentType = "image/fax",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Fdf mime：.fdf=application/vnd.fdf
        /// </summary>
        public static MimeModel Fdf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Fdf",
                    Extension = ".fdf",
                    ContentType = "application/vnd.fdf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Fif mime：.fif=application/fractals
        /// </summary>
        public static MimeModel Fif
        {
            get
            {
                return new MimeModel
                {
                    Name = "Fif",
                    Extension = ".fif",
                    ContentType = "application/fractals",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Fo mime：.fo=text/xml
        /// </summary>
        public static MimeModel Fo
        {
            get
            {
                return new MimeModel
                {
                    Name = "Fo",
                    Extension = ".fo",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Frm mime：.frm=application/x-frm
        /// </summary>
        public static MimeModel Frm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Frm",
                    Extension = ".frm",
                    ContentType = "application/x-frm",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Frm mime：application/x-www-form-urlencoded
        /// </summary>
        public static MimeModel FormUrlencoded
        {
            get
            {
                return new MimeModel
                {
                    Name = "FormUrlencoded",
                    Extension = "",
                    ContentType = "application/x-www-form-urlencoded",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Frm mime：multipart/form-data
        /// </summary>
        public static MimeModel FormData
        {
            get
            {
                return new MimeModel
                {
                    Name = "FormData",
                    Extension = "",
                    ContentType = "multipart/form-data",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// G4 mime：.g4=application/x-g4
        /// </summary>
        public static MimeModel G4
        {
            get
            {
                return new MimeModel
                {
                    Name = "G4",
                    Extension = ".g4",
                    ContentType = "application/x-g4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Gbr mime：.gbr=application/x-gbr
        /// </summary>
        public static MimeModel Gbr
        {
            get
            {
                return new MimeModel
                {
                    Name = "Gbr",
                    Extension = ".gbr",
                    ContentType = "application/x-gbr",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Gcd mime：.gcd=application/x-gcd
        /// </summary>
        public static MimeModel Gcd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Gcd",
                    Extension = ".gcd",
                    ContentType = "application/x-gcd",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Gif mime：.gif=image/gif
        /// </summary>
        public static MimeModel Gif
        {
            get
            {
                return new MimeModel
                {
                    Name = "Gif",
                    Extension = ".gif",
                    ContentType = "image/gif",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Gl2 mime：.gl2=application/x-gl2
        /// </summary>
        public static MimeModel Gl2
        {
            get
            {
                return new MimeModel
                {
                    Name = "Gl2",
                    Extension = ".gl2",
                    ContentType = "application/x-gl2",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Gp4 mime：.gp4=application/x-gp4
        /// </summary>
        public static MimeModel Gp4
        {
            get
            {
                return new MimeModel
                {
                    Name = "Gp4",
                    Extension = ".gp4",
                    ContentType = "application/x-gp4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hgl mime：.hgl=application/x-hgl
        /// </summary>
        public static MimeModel Hgl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hgl",
                    Extension = ".hgl",
                    ContentType = "application/x-hgl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hmr mime：.hmr=application/x-hmr
        /// </summary>
        public static MimeModel Hmr
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hmr",
                    Extension = ".hmr",
                    ContentType = "application/x-hmr",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hpg mime：.hpg=application/x-hpgl
        /// </summary>
        public static MimeModel Hpg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hpg",
                    Extension = ".hpg",
                    ContentType = "application/x-hpgl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hpl mime：.hpl=application/x-hpl
        /// </summary>
        public static MimeModel Hpl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hpl",
                    Extension = ".hpl",
                    ContentType = "application/x-hpl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hqx mime：.hqx=application/mac-binhex40
        /// </summary>
        public static MimeModel Hqx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hqx",
                    Extension = ".hqx",
                    ContentType = "application/mac-binhex40",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hrf mime：.hrf=application/x-hrf
        /// </summary>
        public static MimeModel Hrf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hrf",
                    Extension = ".hrf",
                    ContentType = "application/x-hrf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Hta mime：.hta=application/hta
        /// </summary>
        public static MimeModel Hta
        {
            get
            {
                return new MimeModel
                {
                    Name = "Hta",
                    Extension = ".hta",
                    ContentType = "application/hta",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Htc mime：.htc=text/x-component
        /// </summary>
        public static MimeModel Htc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Htc",
                    Extension = ".htc",
                    ContentType = "text/x-component",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Htm mime：.htm=text/html
        /// </summary>
        public static MimeModel Htm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Htm",
                    Extension = ".htm",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Html mime：.html=text/html
        /// </summary>
        public static MimeModel Html
        {
            get
            {
                return new MimeModel
                {
                    Name = "Html",
                    Extension = ".html",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Htt mime：.htt=text/webviewhtml
        /// </summary>
        public static MimeModel Htt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Htt",
                    Extension = ".htt",
                    ContentType = "text/webviewhtml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Htx mime：.htx=text/html
        /// </summary>
        public static MimeModel Htx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Htx",
                    Extension = ".htx",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Icb mime：.icb=application/x-icb
        /// </summary>
        public static MimeModel Icb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Icb",
                    Extension = ".icb",
                    ContentType = "application/x-icb",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ico mime：.ico=image/x-icon，.ico=application/x-ico
        /// </summary>
        public static MimeModel Ico
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ico",
                    Extension = ".ico",
                    ContentType = "image/x-icon",
                    Alias = new List<string>
                    {
                        "application/x-ico",
                    },
                };
            }
        }

        /// <summary>
        /// Iff mime：.iff=application/x-iff
        /// </summary>
        public static MimeModel Iff
        {
            get
            {
                return new MimeModel
                {
                    Name = "Iff",
                    Extension = ".iff",
                    ContentType = "application/x-iff",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ig4 mime：.ig4=application/x-g4
        /// </summary>
        public static MimeModel Ig4
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ig4",
                    Extension = ".ig4",
                    ContentType = "application/x-g4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Igs mime：.igs=application/x-igs
        /// </summary>
        public static MimeModel Igs
        {
            get
            {
                return new MimeModel
                {
                    Name = "Igs",
                    Extension = ".igs",
                    ContentType = "application/x-igs",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Iii mime：.iii=application/x-iphone
        /// </summary>
        public static MimeModel Iii
        {
            get
            {
                return new MimeModel
                {
                    Name = "Iii",
                    Extension = ".iii",
                    ContentType = "application/x-iphone",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Img mime：.img=application/x-img
        /// </summary>
        public static MimeModel Img
        {
            get
            {
                return new MimeModel
                {
                    Name = "Img",
                    Extension = ".img",
                    ContentType = "application/x-img",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ins mime：.ins=application/x-internet-signup
        /// </summary>
        public static MimeModel Ins
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ins",
                    Extension = ".ins",
                    ContentType = "application/x-internet-signup",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Isp mime：.isp=application/x-internet-signup
        /// </summary>
        public static MimeModel Isp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Isp",
                    Extension = ".isp",
                    ContentType = "application/x-internet-signup",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// IVF mime：.IVF=video/x-ivf
        /// </summary>
        public static MimeModel Ivf
        {
            get
            {
                return new MimeModel
                {
                    Name = "IVF",
                    Extension = ".IVF",
                    ContentType = "video/x-ivf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Java mime：.java=java/*
        /// </summary>
        public static MimeModel Java
        {
            get
            {
                return new MimeModel
                {
                    Name = "Java",
                    Extension = ".java",
                    ContentType = "java/*",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Jfif mime：.jfif=image/jpeg
        /// </summary>
        public static MimeModel Jfif
        {
            get
            {
                return new MimeModel
                {
                    Name = "Jfif",
                    Extension = ".jfif",
                    ContentType = "image/jpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Jpe mime：.jpe=image/jpeg，.jpe=application/x-jpe
        /// </summary>
        public static MimeModel Jpe
        {
            get
            {
                return new MimeModel
                {
                    Name = "Jpe",
                    Extension = ".jpe",
                    ContentType = "image/jpeg",
                    Alias = new List<string>
                    {
                         "application/x-jpe",
                    },
                };
            }
        }

        /// <summary>
        /// Jpeg mime：.jpeg=image/jpeg，.jpg=image/jpeg
        /// </summary>
        public static MimeModel Jpeg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Jpeg",
                    Extension = ".jpeg",
                    ContentType = "image/jpeg",
                    Alias = new List<string>
                    {
                         "image/jpeg",
                    },
                };
            }
        }

        /// <summary>
        /// Jpg mime：.jpg=application/x-jpg
        /// </summary>
        public static MimeModel Jpg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Jpg",
                    Extension = ".jpg",
                    ContentType = "application/x-jpg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Js mime：.js=application/x-javascript
        /// </summary>
        public static MimeModel Js
        {
            get
            {
                return new MimeModel
                {
                    Name = "Js",
                    Extension = ".js",
                    ContentType = "application/x-javascript",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Jsp mime：.jsp=text/html
        /// </summary>
        public static MimeModel Jsp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Jsp",
                    Extension = ".jsp",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Json mime：.js=application/json
        /// </summary>
        public static MimeModel Json
        {
            get
            {
                return new MimeModel
                {
                    Name = "Json",
                    Extension = ".js",
                    ContentType = "application/json",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// La1 mime：.la1=audio/x-liquid-file
        /// </summary>
        public static MimeModel La1
        {
            get
            {
                return new MimeModel
                {
                    Name = "La1",
                    Extension = ".la1",
                    ContentType = "audio/x-liquid-file",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Lar mime：.lar=application/x-laplayer-reg
        /// </summary>
        public static MimeModel Lar
        {
            get
            {
                return new MimeModel
                {
                    Name = "Lar",
                    Extension = ".lar",
                    ContentType = "application/x-laplayer-reg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Latex mime：.latex=application/x-latex
        /// </summary>
        public static MimeModel Latex
        {
            get
            {
                return new MimeModel
                {
                    Name = "Latex",
                    Extension = ".latex",
                    ContentType = "application/x-latex",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Lavs mime：.lavs=audio/x-liquid-secure
        /// </summary>
        public static MimeModel Lavs
        {
            get
            {
                return new MimeModel
                {
                    Name = "Lavs",
                    Extension = ".lavs",
                    ContentType = "audio/x-liquid-secure",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Lbm mime：.lbm=application/x-lbm
        /// </summary>
        public static MimeModel Lbm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Lbm",
                    Extension = ".lbm",
                    ContentType = "application/x-lbm",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Lmsff mime：.lmsff=audio/x-la-lms
        /// </summary>
        public static MimeModel Lmsff
        {
            get
            {
                return new MimeModel
                {
                    Name = "Lmsff",
                    Extension = ".lmsff",
                    ContentType = "audio/x-la-lms",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ls mime：.ls=application/x-javascript
        /// </summary>
        public static MimeModel Ls
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ls",
                    Extension = ".ls",
                    ContentType = "application/x-javascript",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ltr mime：.ltr=application/x-ltr
        /// </summary>
        public static MimeModel Ltr
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ltr",
                    Extension = ".ltr",
                    ContentType = "application/x-ltr",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// M1v mime：.m1v=video/x-mpeg
        /// </summary>
        public static MimeModel M1V
        {
            get
            {
                return new MimeModel
                {
                    Name = "M1v",
                    Extension = ".m1v",
                    ContentType = "video/x-mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// M2v mime：.m2v=video/x-mpeg
        /// </summary>
        public static MimeModel M2V
        {
            get
            {
                return new MimeModel
                {
                    Name = "M2v",
                    Extension = ".m2v",
                    ContentType = "video/x-mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// M3u mime：.m3u=audio/mpegurl
        /// </summary>
        public static MimeModel M3U
        {
            get
            {
                return new MimeModel
                {
                    Name = "M3u",
                    Extension = ".m3u",
                    ContentType = "audio/mpegurl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// M4e mime：.m4e=video/mpeg4
        /// </summary>
        public static MimeModel M4E
        {
            get
            {
                return new MimeModel
                {
                    Name = "M4e",
                    Extension = ".m4e",
                    ContentType = "video/mpeg4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mac mime：.mac=application/x-mac
        /// </summary>
        public static MimeModel Mac
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mac",
                    Extension = ".mac",
                    ContentType = "application/x-mac",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Man mime：.man=application/x-troff-man
        /// </summary>
        public static MimeModel Man
        {
            get
            {
                return new MimeModel
                {
                    Name = "Man",
                    Extension = ".man",
                    ContentType = "application/x-troff-man",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Math mime：.math=text/xml
        /// </summary>
        public static MimeModel Math
        {
            get
            {
                return new MimeModel
                {
                    Name = "Math",
                    Extension = ".math",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mdb mime：.mdb=application/msaccess，.mdb=application/x-mdb
        /// </summary>
        public static MimeModel Mdb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mdb",
                    Extension = ".mdb",
                    ContentType = "application/msaccess",
                    Alias = new List<string>
                    {
                         "application/x-mdb",
                    },
                };
            }
        }

        /// <summary>
        /// Mfp mime：.mfp=application/x-shockwave-flash
        /// </summary>
        public static MimeModel Mfp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mfp",
                    Extension = ".mfp",
                    ContentType = "application/x-shockwave-flash",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mht mime：.mht=message/rfc822
        /// </summary>
        public static MimeModel Mht
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mht",
                    Extension = ".mht",
                    ContentType = "message/rfc822",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mhtml mime：.mhtml=message/rfc822
        /// </summary>
        public static MimeModel Mhtml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mhtml",
                    Extension = ".mhtml",
                    ContentType = "message/rfc822",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mi mime：.mi=application/x-mi
        /// </summary>
        public static MimeModel Mi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mi",
                    Extension = ".mi",
                    ContentType = "application/x-mi",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mid mime：.mid=audio/mid
        /// </summary>
        public static MimeModel Mid
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mid",
                    Extension = ".mid",
                    ContentType = "audio/mid",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Midi mime：.midi=audio/mid
        /// </summary>
        public static MimeModel Midi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Midi",
                    Extension = ".midi",
                    ContentType = "audio/mid",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mil mime：.mil=application/x-mil
        /// </summary>
        public static MimeModel Mil
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mil",
                    Extension = ".mil",
                    ContentType = "application/x-mil",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mml mime：.mml=text/xml
        /// </summary>
        public static MimeModel Mml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mml",
                    Extension = ".mml",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mnd mime：.mnd=audio/x-musicnet-download
        /// </summary>
        public static MimeModel Mnd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mnd",
                    Extension = ".mnd",
                    ContentType = "audio/x-musicnet-download",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mns mime：.mns=audio/x-musicnet-stream
        /// </summary>
        public static MimeModel Mns
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mns",
                    Extension = ".mns",
                    ContentType = "audio/x-musicnet-stream",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mocha mime：.mocha=application/x-javascript
        /// </summary>
        public static MimeModel Mocha
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mocha",
                    Extension = ".mocha",
                    ContentType = "application/x-javascript",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Movie mime：.movie=video/x-sgi-movie
        /// </summary>
        public static MimeModel Movie
        {
            get
            {
                return new MimeModel
                {
                    Name = "Movie",
                    Extension = ".movie",
                    ContentType = "video/x-sgi-movie",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mp1 mime：.mp1=audio/mp1
        /// </summary>
        public static MimeModel Mp1
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mp1",
                    Extension = ".mp1",
                    ContentType = "audio/mp1",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mp2 mime：.mp2=audio/mp2
        /// </summary>
        public static MimeModel Mp2
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mp2",
                    Extension = ".mp2",
                    ContentType = "audio/mp2",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mp2v mime：.mp2v=video/mpeg
        /// </summary>
        public static MimeModel Mp2V
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mp2v",
                    Extension = ".mp2v",
                    ContentType = "video/mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mp3 mime：.mp3=audio/mp3
        /// </summary>
        public static MimeModel Mp3
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mp3",
                    Extension = ".mp3",
                    ContentType = "audio/mp3",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mp4 mime：.mp4=video/mp4
        /// </summary>
        public static MimeModel Mp4
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mp4",
                    Extension = ".mp4",
                    ContentType = "video/mp4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpa mime：.mpa=video/x-mpg
        /// </summary>
        public static MimeModel Mpa
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpa",
                    Extension = ".mpa",
                    ContentType = "video/x-mpg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpd mime：.mpd=application/-project
        /// </summary>
        public static MimeModel Mpd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpd",
                    Extension = ".mpd",
                    ContentType = "application/-project",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpe mime：.mpe=video/x-mpeg
        /// </summary>
        public static MimeModel Mpe
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpe",
                    Extension = ".mpe",
                    ContentType = "video/x-mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpeg mime：.mpeg=video/mpg
        /// </summary>
        public static MimeModel Mpeg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpeg",
                    Extension = ".mpeg",
                    ContentType = "video/mpg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpg mime：.mpg=video/mpg
        /// </summary>
        public static MimeModel Mpg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpg",
                    Extension = ".mpg",
                    ContentType = "video/mpg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpga mime：.mpga=audio/rn-mpeg
        /// </summary>
        public static MimeModel Mpga
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpga",
                    Extension = ".mpga",
                    ContentType = "audio/rn-mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpp mime：.mpp=application/-project
        /// </summary>
        public static MimeModel Mpp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpp",
                    Extension = ".mpp",
                    ContentType = "application/-project",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mps mime：.mps=video/x-mpeg
        /// </summary>
        public static MimeModel Mps
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mps",
                    Extension = ".mps",
                    ContentType = "video/x-mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpt mime：.mpt=application/-project
        /// </summary>
        public static MimeModel Mpt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpt",
                    Extension = ".mpt",
                    ContentType = "application/-project",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpv mime：.mpv=video/mpg
        /// </summary>
        public static MimeModel Mpv
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpv",
                    Extension = ".mpv",
                    ContentType = "video/mpg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpv2 mime：.mpv2=video/mpeg
        /// </summary>
        public static MimeModel Mpv2
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpv2",
                    Extension = ".mpv2",
                    ContentType = "video/mpeg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpw mime：.mpw=application/s-project
        /// </summary>
        public static MimeModel Mpw
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpw",
                    Extension = ".mpw",
                    ContentType = "application/s-project",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mpx mime：.mpx=application/-project
        /// </summary>
        public static MimeModel Mpx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mpx",
                    Extension = ".mpx",
                    ContentType = "application/-project",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mtx mime：.mtx=text/xml
        /// </summary>
        public static MimeModel Mtx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mtx",
                    Extension = ".mtx",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Mxp mime：.mxp=application/x-mmxp
        /// </summary>
        public static MimeModel Mxp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Mxp",
                    Extension = ".mxp",
                    ContentType = "application/x-mmxp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Net mime：.net=image/pnetvue
        /// </summary>
        public static MimeModel Net
        {
            get
            {
                return new MimeModel
                {
                    Name = "Net",
                    Extension = ".net",
                    ContentType = "image/pnetvue",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Nrf mime：.nrf=application/x-nrf
        /// </summary>
        public static MimeModel Nrf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Nrf",
                    Extension = ".nrf",
                    ContentType = "application/x-nrf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Nws mime：.nws=message/rfc822
        /// </summary>
        public static MimeModel Nws
        {
            get
            {
                return new MimeModel
                {
                    Name = "Nws",
                    Extension = ".nws",
                    ContentType = "message/rfc822",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Odc mime：.odc=text/x-ms-odc
        /// </summary>
        public static MimeModel Odc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Odc",
                    Extension = ".odc",
                    ContentType = "text/x-ms-odc",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Out mime：.out=application/x-out
        /// </summary>
        public static MimeModel Out
        {
            get
            {
                return new MimeModel
                {
                    Name = "Out",
                    Extension = ".out",
                    ContentType = "application/x-out",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P10 mime：.p10=application/pkcs10
        /// </summary>
        public static MimeModel P10
        {
            get
            {
                return new MimeModel
                {
                    Name = "P10",
                    Extension = ".p10",
                    ContentType = "application/pkcs10",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P12 mime：.p12=application/x-pkcs12
        /// </summary>
        public static MimeModel P12
        {
            get
            {
                return new MimeModel
                {
                    Name = "P12",
                    Extension = ".p12",
                    ContentType = "application/x-pkcs12",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P7b mime：.p7b=application/x-pkcs7-certificates
        /// </summary>
        public static MimeModel P7B
        {
            get
            {
                return new MimeModel
                {
                    Name = "P7B",
                    Extension = ".p7b",
                    ContentType = "application/x-pkcs7-certificates",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P7c mime：.p7c=application/pkcs7-mime
        /// </summary>
        public static MimeModel P7C
        {
            get
            {
                return new MimeModel
                {
                    Name = "P7C",
                    Extension = ".p7c",
                    ContentType = "application/pkcs7-mime",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P7m mime：.p7m=application/pkcs7-mime
        /// </summary>
        public static MimeModel P7M
        {
            get
            {
                return new MimeModel
                {
                    Name = "P7M",
                    Extension = ".p7m",
                    ContentType = "application/pkcs7-mime",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P7r mime：.p7r=application/x-pkcs7-certreqresp
        /// </summary>
        public static MimeModel P7R
        {
            get
            {
                return new MimeModel
                {
                    Name = "P7R",
                    Extension = ".p7r",
                    ContentType = "application/x-pkcs7-certreqresp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// P7s mime：.p7s=application/pkcs7-signature
        /// </summary>
        public static MimeModel P7S
        {
            get
            {
                return new MimeModel
                {
                    Name = "P7S",
                    Extension = ".p7s",
                    ContentType = "application/pkcs7-signature",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pc5 mime：.pc5=application/x-pc5
        /// </summary>
        public static MimeModel Pc5
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pc5",
                    Extension = ".pc5",
                    ContentType = "application/x-pc5",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pci mime：.pci=application/x-pci
        /// </summary>
        public static MimeModel Pci
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pci",
                    Extension = ".pci",
                    ContentType = "application/x-pci",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pcl mime：.pcl=application/x-pcl
        /// </summary>
        public static MimeModel Pcl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pcl",
                    Extension = ".pcl",
                    ContentType = "application/x-pcl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pcx mime：.pcx=application/x-pcx
        /// </summary>
        public static MimeModel Pcx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pcx",
                    Extension = ".pcx",
                    ContentType = "application/x-pcx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pdf mime：.pdf=application/pdf
        /// </summary>
        public static MimeModel Pdf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pdf",
                    Extension = ".pdf",
                    ContentType = "application/pdf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pdx mime：.pdx=application/vnd.adobe.pdx
        /// </summary>
        public static MimeModel Pdx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pdx",
                    Extension = ".pdx",
                    ContentType = "application/vnd.adobe.pdx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pfx mime：.pfx=application/x-pkcs12
        /// </summary>
        public static MimeModel Pfx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pfx",
                    Extension = ".pfx",
                    ContentType = "application/x-pkcs12",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pgl mime：.pgl=application/x-pgl
        /// </summary>
        public static MimeModel Pgl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pgl",
                    Extension = ".pgl",
                    ContentType = "application/x-pgl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pic mime：.pic=application/x-pic
        /// </summary>
        public static MimeModel Pic
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pic",
                    Extension = ".pic",
                    ContentType = "application/x-pic",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pko mime：.pko=application-pki.pko
        /// </summary>
        public static MimeModel Pko
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pko",
                    Extension = ".pko",
                    ContentType = "application-pki.pko",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pl mime：.pl=application/x-perl
        /// </summary>
        public static MimeModel Pl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pl",
                    Extension = ".pl",
                    ContentType = "application/x-perl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Plg mime：.plg=text/html
        /// </summary>
        public static MimeModel Plg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Plg",
                    Extension = ".plg",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pls mime：.pls=audio/scpls
        /// </summary>
        public static MimeModel Pls
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pls",
                    Extension = ".pls",
                    ContentType = "audio/scpls",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Plt mime：.plt=application/x-plt
        /// </summary>
        public static MimeModel Plt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Plt",
                    Extension = ".plt",
                    ContentType = "application/x-plt",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Png mime：.png=image/png，.png=application/x-png
        /// </summary>
        public static MimeModel Png
        {
            get
            {
                return new MimeModel
                {
                    Name = "Png",
                    Extension = ".png",
                    ContentType = "image/png",
                    Alias = new List<string>
                    {
                         "application/x-png",
                    },
                };
            }
        }

        /// <summary>
        /// Pot mime：.pot=applications-powerpoint
        /// </summary>
        public static MimeModel Pot
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pot",
                    Extension = ".pot",
                    ContentType = "applications-powerpoint",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ppa mime：.ppa=application/vs-powerpoint
        /// </summary>
        public static MimeModel Ppa
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ppa",
                    Extension = ".ppa",
                    ContentType = "application/vs-powerpoint",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ppm mime：.ppm=application/x-ppm
        /// </summary>
        public static MimeModel Ppm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ppm",
                    Extension = ".ppm",
                    ContentType = "application/x-ppm",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pps mime：.pps=application-powerpoint
        /// </summary>
        public static MimeModel Pps
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pps",
                    Extension = ".pps",
                    ContentType = "application-powerpoint",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ppt mime：.ppt=applications-powerpoint，.ppt=application/x-ppt
        /// </summary>
        public static MimeModel Ppt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ppt",
                    Extension = ".ppt",
                    ContentType = "applications-powerpoint",
                    Alias = new List<string>
                    {
                        "application/x-ppt",
                    },
                };
            }
        }

        /// <summary>
        /// Pr mime：.pr=application/x-pr
        /// </summary>
        public static MimeModel Pr
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pr",
                    Extension = ".pr",
                    ContentType = "application/x-pr",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Prf mime：.prf=application/pics-rules
        /// </summary>
        public static MimeModel Prf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Prf",
                    Extension = ".prf",
                    ContentType = "application/pics-rules",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Prn mime：.prn=application/x-prn
        /// </summary>
        public static MimeModel Prn
        {
            get
            {
                return new MimeModel
                {
                    Name = "Prn",
                    Extension = ".prn",
                    ContentType = "application/x-prn",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Prt mime：.prt=application/x-prt
        /// </summary>
        public static MimeModel Prt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Prt",
                    Extension = ".prt",
                    ContentType = "application/x-prt",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ps mime：.ps=application/x-ps，.ps=application/postscript
        /// </summary>
        public static MimeModel Ps
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ps",
                    Extension = ".ps",
                    ContentType = "application/x-ps",
                    Alias = new List<string>
                    {
                        "application/postscript",
                    },
                };
            }
        }

        /// <summary>
        /// Ptn mime：.ptn=application/x-ptn
        /// </summary>
        public static MimeModel Ptn
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ptn",
                    Extension = ".ptn",
                    ContentType = "application/x-ptn",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Pwz mime：.pwz=application/powerpoint
        /// </summary>
        public static MimeModel Pwz
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pwz",
                    Extension = ".pwz",
                    ContentType = "application/powerpoint",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// R3t mime：.r3t=text/vnd.rn-realtext3d
        /// </summary>
        public static MimeModel R3T
        {
            get
            {
                return new MimeModel
                {
                    Name = "R3T",
                    Extension = ".r3t",
                    ContentType = "text/vnd.rn-realtext3d",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ra mime：.ra=audio/vnd.rn-realaudio
        /// </summary>
        public static MimeModel Ra
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ra",
                    Extension = ".ra",
                    ContentType = "audio/vnd.rn-realaudio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ram mime：.ram=audio/x-pn-realaudio
        /// </summary>
        public static MimeModel Ram
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ram",
                    Extension = ".ram",
                    ContentType = "audio/x-pn-realaudio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ras mime：.ras=application/x-ras
        /// </summary>
        public static MimeModel Ras
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ras",
                    Extension = ".ras",
                    ContentType = "application/x-ras",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rat mime：.rat=application/rat-file
        /// </summary>
        public static MimeModel Rat
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rat",
                    Extension = ".rat",
                    ContentType = "application/rat-file",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rdf mime：.rdf=text/xml
        /// </summary>
        public static MimeModel Rdf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rdf",
                    Extension = ".rdf",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rec mime：.rec=application/vnd.rn-recording
        /// </summary>
        public static MimeModel Rec
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rec",
                    Extension = ".rec",
                    ContentType = "application/vnd.rn-recording",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Red mime：.red=application/x-red
        /// </summary>
        public static MimeModel Red
        {
            get
            {
                return new MimeModel
                {
                    Name = "Red",
                    Extension = ".red",
                    ContentType = "application/x-red",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rgb mime：.rgb=application/x-rgb
        /// </summary>
        public static MimeModel Rgb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rgb",
                    Extension = ".rgb",
                    ContentType = "application/x-rgb",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rjs mime：.rjs=application/vnd.rn-realsystem-rjs
        /// </summary>
        public static MimeModel Rjs
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rjs",
                    Extension = ".rjs",
                    ContentType = "application/vnd.rn-realsystem-rjs",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rjt mime：.rjt=application/vnd.rn-realsystem-rjt
        /// </summary>
        public static MimeModel Rjt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rjt",
                    Extension = ".rjt",
                    ContentType = "application/vnd.rn-realsystem-rjt",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rlc mime：.rlc=application/x-rlc
        /// </summary>
        public static MimeModel Rlc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rlc",
                    Extension = ".rlc",
                    ContentType = "application/x-rlc",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rle mime：.rle=application/x-rle
        /// </summary>
        public static MimeModel Rle
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rle",
                    Extension = ".rle",
                    ContentType = "application/x-rle",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rm mime：.rm=application/vnd.rn-realmedia
        /// </summary>
        public static MimeModel Rm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rm",
                    Extension = ".rm",
                    ContentType = "application/vnd.rn-realmedia",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmf mime：.rmf=application/vnd.adobe.rmf
        /// </summary>
        public static MimeModel Rmf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmf",
                    Extension = ".rmf",
                    ContentType = "application/vnd.adobe.rmf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmi mime：.rmi=audio/mid
        /// </summary>
        public static MimeModel Rmi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmi",
                    Extension = ".rmi",
                    ContentType = "audio/mid",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmj mime：.rmj=application/vnd.rn-realsystem-rmj
        /// </summary>
        public static MimeModel Rmj
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmj",
                    Extension = ".rmj",
                    ContentType = "application/vnd.rn-realsystem-rmj",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmm mime：.rmm=audio/x-pn-realaudio
        /// </summary>
        public static MimeModel Rmm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmm",
                    Extension = ".rmm",
                    ContentType = "audio/x-pn-realaudio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmp mime：.rmp=application/vnd.rn-rn_music_package
        /// </summary>
        public static MimeModel Rmp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmp",
                    Extension = ".rmp",
                    ContentType = "application/vnd.rn-rn_music_package",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rms mime：.rms=application/vnd.rn-realmedia-secure
        /// </summary>
        public static MimeModel Rms
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rms",
                    Extension = ".rms",
                    ContentType = "application/vnd.rn-realmedia-secure",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmvb mime：.rmvb=application/vnd.rn-realmedia-vbr
        /// </summary>
        public static MimeModel Rmvb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmvb",
                    Extension = ".rmvb",
                    ContentType = "application/vnd.rn-realmedia-vbr",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rmx mime：.rmx=application/vnd.rn-realsystem-rmx
        /// </summary>
        public static MimeModel Rmx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rmx",
                    Extension = ".rmx",
                    ContentType = "application/vnd.rn-realsystem-rmx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rnx mime：.rnx=application/vnd.rn-realplayer
        /// </summary>
        public static MimeModel Rnx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rnx",
                    Extension = ".rnx",
                    ContentType = "application/vnd.rn-realplayer",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rp mime：.rp=image/vnd.rn-realpix
        /// </summary>
        public static MimeModel Rp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rp",
                    Extension = ".rp",
                    ContentType = "image/vnd.rn-realpix",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rpm mime：.rpm=audio/x-pn-realaudio-plugin
        /// </summary>
        public static MimeModel Rpm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rpm",
                    Extension = ".rpm",
                    ContentType = "audio/x-pn-realaudio-plugin",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rsml mime：.rsml=application/vnd.rn-rsml
        /// </summary>
        public static MimeModel Rsml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rsml",
                    Extension = ".rsml",
                    ContentType = "application/vnd.rn-rsml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rt mime：.rt=text/vnd.rn-realtext
        /// </summary>
        public static MimeModel Rt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rt",
                    Extension = ".rt",
                    ContentType = "text/vnd.rn-realtext",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Rtf mime：.rtf=application/msword，.rtf=application/x-rtf
        /// </summary>
        public static MimeModel Rtf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rtf",
                    Extension = ".rtf",
                    ContentType = "application/msword",
                    Alias = new List<string>
                    {
                        "application/x-rtf",
                    },
                };
            }
        }

        /// <summary>
        /// Rv mime：.rv=video/vnd.rn-realvideo
        /// </summary>
        public static MimeModel Rv
        {
            get
            {
                return new MimeModel
                {
                    Name = "Rv",
                    Extension = ".rv",
                    ContentType = "video/vnd.rn-realvideo",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sam mime：.sam=application/x-sam
        /// </summary>
        public static MimeModel Sam
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sam",
                    Extension = ".sam",
                    ContentType = "application/x-sam",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sat mime：.sat=application/x-sat
        /// </summary>
        public static MimeModel Sat
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sat",
                    Extension = ".sat",
                    ContentType = "application/x-sat",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sdp mime：.sdp=application/sdp
        /// </summary>
        public static MimeModel Sdp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sdp",
                    Extension = ".sdp",
                    ContentType = "application/sdp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sdw mime：.sdw=application/x-sdw
        /// </summary>
        public static MimeModel Sdw
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sdw",
                    Extension = ".sdw",
                    ContentType = "application/x-sdw",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sit mime：.sit=application/x-stuffit
        /// </summary>
        public static MimeModel Sit
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sit",
                    Extension = ".sit",
                    ContentType = "application/x-stuffit",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Slb mime：.slb=application/x-slb
        /// </summary>
        public static MimeModel Slb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Slb",
                    Extension = ".slb",
                    ContentType = "application/x-slb",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sld mime：.sld=application/x-sld
        /// </summary>
        public static MimeModel Sld
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sld",
                    Extension = ".sld",
                    ContentType = "application/x-sld",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Slk mime：.slk=drawing/x-slk
        /// </summary>
        public static MimeModel Slk
        {
            get
            {
                return new MimeModel
                {
                    Name = "Slk",
                    Extension = ".slk",
                    ContentType = "drawing/x-slk",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Smi mime：.smi=application/smil
        /// </summary>
        public static MimeModel Smi
        {
            get
            {
                return new MimeModel
                {
                    Name = "Smi",
                    Extension = ".smi",
                    ContentType = "application/smil",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Smil mime：.smil=application/smil
        /// </summary>
        public static MimeModel Smil
        {
            get
            {
                return new MimeModel
                {
                    Name = "Smil",
                    Extension = ".smil",
                    ContentType = "application/smil",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Smk mime：.smk=application/x-smk
        /// </summary>
        public static MimeModel Smk
        {
            get
            {
                return new MimeModel
                {
                    Name = "Smk",
                    Extension = ".smk",
                    ContentType = "application/x-smk",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Snd mime：.snd=audio/basic
        /// </summary>
        public static MimeModel Snd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Snd",
                    Extension = ".snd",
                    ContentType = "audio/basic",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sol mime：.sol=text/plain
        /// </summary>
        public static MimeModel Sol
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sol",
                    Extension = ".sol",
                    ContentType = "text/plain",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sor mime：.sor=text/plain
        /// </summary>
        public static MimeModel Sor
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sor",
                    Extension = ".sor",
                    ContentType = "text/plain",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Spc mime：.spc=application/x-pkcs7-certificates
        /// </summary>
        public static MimeModel Spc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Spc",
                    Extension = ".spc",
                    ContentType = "application/x-pkcs7-certificates",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Spl mime：.spl=application/futuresplash
        /// </summary>
        public static MimeModel Spl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Spl",
                    Extension = ".spl",
                    ContentType = "application/futuresplash",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Spp mime：.spp=text/xml
        /// </summary>
        public static MimeModel Spp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Spp",
                    Extension = ".spp",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ssm mime：.ssm=application/streamingmedia
        /// </summary>
        public static MimeModel Ssm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ssm",
                    Extension = ".ssm",
                    ContentType = "application/streamingmedia",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sst mime：.sst=application-pki.certstore
        /// </summary>
        public static MimeModel Sst
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sst",
                    Extension = ".sst",
                    ContentType = "application-pki.certstore",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Stl mime：.stl=application/-pki.stl
        /// </summary>
        public static MimeModel Stl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Stl",
                    Extension = ".stl",
                    ContentType = "application/-pki.stl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Stm mime：.stm=text/html
        /// </summary>
        public static MimeModel Stm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Stm",
                    Extension = ".stm",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Sty mime：.sty=application/x-sty
        /// </summary>
        public static MimeModel Sty
        {
            get
            {
                return new MimeModel
                {
                    Name = "Sty",
                    Extension = ".sty",
                    ContentType = "application/x-sty",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Svg mime：.svg=text/xml
        /// </summary>
        public static MimeModel Svg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Svg",
                    Extension = ".svg",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Swf mime：.swf=application/x-shockwave-flash
        /// </summary>
        public static MimeModel Swf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Swf",
                    Extension = ".swf",
                    ContentType = "application/x-shockwave-flash",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Tdf mime：.tdf=application/x-tdf
        /// </summary>
        public static MimeModel Tdf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tdf",
                    Extension = ".tdf",
                    ContentType = "application/x-tdf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Tg4 mime：.tg4=application/x-tg4
        /// </summary>
        public static MimeModel Tg4
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tg4",
                    Extension = ".tg4",
                    ContentType = "application/x-tg4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Tga mime：.tga=application/x-tga
        /// </summary>
        public static MimeModel Tga
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tga",
                    Extension = ".tga",
                    ContentType = "application/x-tga",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Tif mime：.tif=image/tiff，.tif=application/x-tif
        /// </summary>
        public static MimeModel Tif
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tif",
                    Extension = ".tif",
                    ContentType = "image/tiff",
                    Alias = new List<string>
                    {
                         "application/x-tif",
                    },
                };
            }
        }

        /// <summary>
        /// Tiff mime：.tiff=image/tiff
        /// </summary>
        public static MimeModel Tiff
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tiff",
                    Extension = ".tiff",
                    ContentType = "image/tiff",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Tld mime：.tld=text/xml
        /// </summary>
        public static MimeModel Tld
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tld",
                    Extension = ".tld",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Top mime：.top=drawing/x-top
        /// </summary>
        public static MimeModel Top
        {
            get
            {
                return new MimeModel
                {
                    Name = "Top",
                    Extension = ".top",
                    ContentType = "drawing/x-top",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Torrent mime：.torrent=application/x-bittorrent
        /// </summary>
        public static MimeModel Torrent
        {
            get
            {
                return new MimeModel
                {
                    Name = "Torrent",
                    Extension = ".torrent",
                    ContentType = "application/x-bittorrent",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Tsd mime：.tsd=text/xml
        /// </summary>
        public static MimeModel Tsd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Tsd",
                    Extension = ".tsd",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Txt mime：.txt=text/plain
        /// </summary>
        public static MimeModel Txt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Txt",
                    Extension = ".txt",
                    ContentType = "text/plain",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Uin mime：.uin=application/x-icq
        /// </summary>
        public static MimeModel Uin
        {
            get
            {
                return new MimeModel
                {
                    Name = "Uin",
                    Extension = ".uin",
                    ContentType = "application/x-icq",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Uls mime：.uls=text/iuls
        /// </summary>
        public static MimeModel Uls
        {
            get
            {
                return new MimeModel
                {
                    Name = "Uls",
                    Extension = ".uls",
                    ContentType = "text/iuls",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vcf mime：.vcf=text/x-vcard
        /// </summary>
        public static MimeModel Vcf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vcf",
                    Extension = ".vcf",
                    ContentType = "text/x-vcard",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vda mime：.vda=application/x-vda
        /// </summary>
        public static MimeModel Vda
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vda",
                    Extension = ".vda",
                    ContentType = "application/x-vda",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vdx mime：.vdx=application/vnd.visio
        /// </summary>
        public static MimeModel Vdx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vdx",
                    Extension = ".vdx",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vml mime：.vml=text/xml
        /// </summary>
        public static MimeModel Vml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vml",
                    Extension = ".vml",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vpg mime：.vpg=application/x-vpeg005
        /// </summary>
        public static MimeModel Vpg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vpg",
                    Extension = ".vpg",
                    ContentType = "application/x-vpeg005",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vsd mime：.vsd=application/vnd.visio， .vsd=application/x-tif
        /// </summary>
        public static MimeModel Vsd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vsd",
                    Extension = ".vsd",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>
                    {
                        "application/x-vsd",
                    },
                };
            }
        }

        /// <summary>
        /// Vss mime：.vss=application/vnd.visio
        /// </summary>
        public static MimeModel Vss
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vss",
                    Extension = ".vss",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vst mime：.vst=application/vnd.visio，.vst=application/x-vst
        /// </summary>
        public static MimeModel Vst
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vst",
                    Extension = ".vst",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>
                    {
                         "application/x-vst",
                    },
                };
            }
        }

        /// <summary>
        /// Vsw mime：.vsw=application/vnd.visio
        /// </summary>
        public static MimeModel Vsw
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vsw",
                    Extension = ".vsw",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vsx mime：.vsx=application/vnd.visio
        /// </summary>
        public static MimeModel Vsx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vsx",
                    Extension = ".vsx",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vtx mime：.vtx=application/vnd.visio
        /// </summary>
        public static MimeModel Vtx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vtx",
                    Extension = ".vtx",
                    ContentType = "application/vnd.visio",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Vxml mime：.vxml=text/xml
        /// </summary>
        public static MimeModel Vxml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Vxml",
                    Extension = ".vxml",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wav mime：.wav=audio/wav
        /// </summary>
        public static MimeModel Wav
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wav",
                    Extension = ".wav",
                    ContentType = "audio/wav",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wax mime：.wax=audio/x-ms-wax
        /// </summary>
        public static MimeModel Wax
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wax",
                    Extension = ".wax",
                    ContentType = "audio/x-ms-wax",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wb1 mime：.wb1=application/x-wb1
        /// </summary>
        public static MimeModel Wb1
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wb1",
                    Extension = ".wb1",
                    ContentType = "application/x-wb1",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wb2 mime：.wb2=application/x-wb2
        /// </summary>
        public static MimeModel Wb2
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wb2",
                    Extension = ".wb2",
                    ContentType = "application/x-wb2",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wb3 mime：.wb3=application/x-wb3
        /// </summary>
        public static MimeModel Wb3
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wb3",
                    Extension = ".wb3",
                    ContentType = "application/x-wb3",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wbmp mime：.wbmp=image/vnd.wap.wbmp
        /// </summary>
        public static MimeModel Wbmp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wbmp",
                    Extension = ".wbmp",
                    ContentType = "image/vnd.wap.wbmp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wiz mime：.wiz=application/msword
        /// </summary>
        public static MimeModel Wiz
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wiz",
                    Extension = ".wiz",
                    ContentType = "application/msword",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wk3 mime：.wk3=application/x-wk3
        /// </summary>
        public static MimeModel Wk3
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wk3",
                    Extension = ".wk3",
                    ContentType = "application/x-wk3",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wk4 mime：.wk4=application/x-wk4
        /// </summary>
        public static MimeModel Wk4
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wk4",
                    Extension = ".wk4",
                    ContentType = "application/x-wk4",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wkq mime：.wkq=application/x-wkq
        /// </summary>
        public static MimeModel Wkq
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wkq",
                    Extension = ".wkq",
                    ContentType = "application/x-wkq",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wks mime：.wks=application/x-wks
        /// </summary>
        public static MimeModel Wks
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wks",
                    Extension = ".wks",
                    ContentType = "application/x-wks",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wm mime：.wm=video/x-ms-wm
        /// </summary>
        public static MimeModel Wm
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wm",
                    Extension = ".wm",
                    ContentType = "video/x-ms-wm",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wma mime：.wma=audio/x-ms-wma
        /// </summary>
        public static MimeModel Wma
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wma",
                    Extension = ".wma",
                    ContentType = "audio/x-ms-wma",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wmd mime：.wmd=application/x-ms-wmd
        /// </summary>
        public static MimeModel Wmd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wmd",
                    Extension = ".wmd",
                    ContentType = "application/x-ms-wmd",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wmf mime：.wmf=application/x-wmf
        /// </summary>
        public static MimeModel Wmf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wmf",
                    Extension = ".wmf",
                    ContentType = "application/x-wmf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wml mime：.wml=text/vnd.wap.wml
        /// </summary>
        public static MimeModel Wml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wml",
                    Extension = ".wml",
                    ContentType = "text/vnd.wap.wml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wmv mime：.wmv=video/x-ms-wmv
        /// </summary>
        public static MimeModel Wmv
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wmv",
                    Extension = ".wmv",
                    ContentType = "video/x-ms-wmv",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wmx mime：.wmx=video/x-ms-wmx
        /// </summary>
        public static MimeModel Wmx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wmx",
                    Extension = ".wmx",
                    ContentType = "video/x-ms-wmx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wmz mime：.wmz=application/x-ms-wmz
        /// </summary>
        public static MimeModel Wmz
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wmz",
                    Extension = ".wmz",
                    ContentType = "application/x-ms-wmz",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wp6 mime：.wp6=application/x-wp6
        /// </summary>
        public static MimeModel Wp6
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wp6",
                    Extension = ".wp6",
                    ContentType = "application/x-wp6",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wpd mime：.wpd=application/x-wpd
        /// </summary>
        public static MimeModel Wpd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wpd",
                    Extension = ".wpd",
                    ContentType = "application/x-wpd",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wpg mime：.wpg=application/x-wpg
        /// </summary>
        public static MimeModel Wpg
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wpg",
                    Extension = ".wpg",
                    ContentType = "application/x-wpg",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wpl mime：.wpl=application/-wpl
        /// </summary>
        public static MimeModel Wpl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wpl",
                    Extension = ".wpl",
                    ContentType = "application/-wpl",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wq1 mime：.wq1=application/x-wq1
        /// </summary>
        public static MimeModel Wq1
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wq1",
                    Extension = ".wq1",
                    ContentType = "application/x-wq1",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wr1 mime：.wr1=application/x-wr1
        /// </summary>
        public static MimeModel Wr1
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wr1",
                    Extension = ".wr1",
                    ContentType = "application/x-wr1",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wri mime：.wri=application/x-wri
        /// </summary>
        public static MimeModel Wri
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wri",
                    Extension = ".wri",
                    ContentType = "application/x-wri",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wrk mime：.wrk=application/x-wrk
        /// </summary>
        public static MimeModel Wrk
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wrk",
                    Extension = ".wrk",
                    ContentType = "application/x-wrk",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ws mime：.ws=application/x-ws
        /// </summary>
        public static MimeModel Ws
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ws",
                    Extension = ".ws",
                    ContentType = "application/x-ws",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Ws2 mime：.ws2=application/x-ws
        /// </summary>
        public static MimeModel Ws2
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ws2",
                    Extension = ".ws2",
                    ContentType = "application/x-ws",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wsc mime：.wsc=text/scriptlet
        /// </summary>
        public static MimeModel Wsc
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wsc",
                    Extension = ".wsc",
                    ContentType = "text/scriptlet",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wsdl mime：.wsdl=text/xml
        /// </summary>
        public static MimeModel Wsdl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wsdl",
                    Extension = ".wsdl",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Wvx mime：.wvx=video/x-ms-wvx
        /// </summary>
        public static MimeModel Wvx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Wvx",
                    Extension = ".wvx",
                    ContentType = "video/x-ms-wvx",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xdp mime：.xdp=application/vnd.adobe.xdp
        /// </summary>
        public static MimeModel Xdp
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xdp",
                    Extension = ".xdp",
                    ContentType = "application/vnd.adobe.xdp",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xdr mime：.xdr=text/xml
        /// </summary>
        public static MimeModel Xdr
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xdr",
                    Extension = ".xdr",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xfd mime：.xfd=application/vnd.adobe.xfd
        /// </summary>
        public static MimeModel Xfd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xfd",
                    Extension = ".xfd",
                    ContentType = "application/vnd.adobe.xfd",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xfdf mime：.xfdf=application/vnd.adobe.xfdf
        /// </summary>
        public static MimeModel Xfdf
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xfdf",
                    Extension = ".xfdf",
                    ContentType = "application/vnd.adobe.xfdf",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xhtml mime：.xhtml=text/html
        /// </summary>
        public static MimeModel Xhtml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xhtml",
                    Extension = ".xhtml",
                    ContentType = "text/html",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xls mime：.xls=application/-excel，.xls=application/x-xls
        /// </summary>
        public static MimeModel Xls
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xls",
                    Extension = ".xls",
                    ContentType = "application/-excel",
                    Alias = new List<string>
                    {
                         "application/x-xls",
                    },
                };
            }
        }

        /// <summary>
        /// Xlw mime：.xlw=application/x-xlw
        /// </summary>
        public static MimeModel Xlw
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xlw",
                    Extension = ".xlw",
                    ContentType = "application/x-xlw",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xml mime：.xml=text/xml
        /// </summary>
        public static MimeModel Xml
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xml",
                    Extension = ".xml",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xpl mime：.xpl=audio/scpls
        /// </summary>
        public static MimeModel Xpl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xpl",
                    Extension = ".xpl",
                    ContentType = "audio/scpls",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xq mime：.xq=text/xml
        /// </summary>
        public static MimeModel Xq
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xq",
                    Extension = ".xq",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xql mime：.xql=text/xml
        /// </summary>
        public static MimeModel Xql
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xql",
                    Extension = ".xql",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xquery mime：.xquery=text/xml
        /// </summary>
        public static MimeModel Xquery
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xquery",
                    Extension = ".xquery",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xsd mime：.xsd=text/xml
        /// </summary>
        public static MimeModel Xsd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xsd",
                    Extension = ".xsd",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xsl mime：.xsl=text/xml
        /// </summary>
        public static MimeModel Xsl
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xsl",
                    Extension = ".xsl",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xslt mime：.xslt=text/xml
        /// </summary>
        public static MimeModel Xslt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xslt",
                    Extension = ".xslt",
                    ContentType = "text/xml",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// Xwd mime：.xwd=application/x-xwd
        /// </summary>
        public static MimeModel Xwd
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xwd",
                    Extension = ".xwd",
                    ContentType = "application/x-xwd",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// X_b mime：.x_b=application/x-x_b
        /// </summary>
        public static MimeModel Xb
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xb",
                    Extension = ".x_b",
                    ContentType = "application/x-x_b",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// X_t mime：.x_t=application/x-x_t
        /// </summary>
        public static MimeModel Xt
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xt",
                    Extension = ".x_t",
                    ContentType = "application/x-x_t",
                    Alias = new List<string>(),
                };
            }
        }
        
        /// <summary>
        /// docx mime：.x_b=application/vnd.openxmlformats-officedocument.wordprocessingml.document
        /// </summary>
        public static MimeModel Docx
        {
            get
            {      
                return new MimeModel
                {
                    Name = "Docx",
                    Extension = ".docx",
                    ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// dotx mime：.x_b=application/vnd.openxmlformats-officedocument.wordprocessingml.template
        /// </summary>
        public static MimeModel Dotx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Dotx",
                    Extension = ".dotx",
                    ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// dotx mime：.x_b=application/vnd.openxmlformats-officedocument.presentationml.presentation
        /// </summary>
        public static MimeModel Pptx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Pptx",
                    Extension = ".pptx",
                    ContentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// ppsx mime：.x_b=application/vnd.openxmlformats-officedocument.presentationml.slideshow
        /// </summary>
        public static MimeModel Ppsx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Ppsx",
                    Extension = ".ppsx",
                    ContentType = "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// potx mime：.x_b=application/vnd.openxmlformats-officedocument.presentationml.template
        /// </summary>
        public static MimeModel Potx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Potx",
                    Extension = ".potx",
                    ContentType = "application/vnd.openxmlformats-officedocument.presentationml.template",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// xlsx mime：.x_b=application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        /// </summary>
        public static MimeModel Xlsx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xlsx",
                    Extension = ".xlsx",
                    ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// xltx mime：.x_b=application/vnd.openxmlformats-officedocument.spreadsheetml.template
        /// </summary>
        public static MimeModel Xltx
        {
            get
            {
                return new MimeModel
                {
                    Name = "Xltx ",
                    Extension = ".xltx",
                    ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
                    Alias = new List<string>(),
                };
            }
        }

        /// <summary>
        /// MsExcel mime：.x_b=application/vnd.ms-excel
        /// </summary>
        public static MimeModel MsExcel
        {
            get
            {
                return new MimeModel
                {
                    Name = "MsExcel",
                    Extension = ".xls",
                    ContentType = "application/vnd.ms-excel",
                    Alias = new List<string>(),
                };
            }
        }
    }
}