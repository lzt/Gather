﻿using System;
using System.Net;

namespace Operate.Net
{
    /// <summary>
    /// HttpClient
    /// 扩展自WebClient，支持CookieContainer
    /// </summary>
    public class HttpClient : WebClient
    {
        // Cookie 容器
        private CookieContainer _cookieContainer;
        // 是否支持自动跳转
        private bool _allowAutoRedirect = true;

        /// <summary>
        /// 创建一个新的 WebClient 实例。
        /// </summary>
        public HttpClient()
        {
            _cookieContainer = new CookieContainer();
        }

        /// <summary>
        /// 创建一个新的 WebClient 实例。
        /// </summary>
        /// <param name="cookies">Cookie 容器</param>
        public HttpClient(CookieContainer cookies)
        {
            _cookieContainer = cookies;
        }

        /// <summary>
        /// 页面是否允许自动跳转，302代码
        /// </summary>
        public bool AllowAutoRedirect
        {
            set { _allowAutoRedirect = value; }
            get { return _allowAutoRedirect; }
        }

        /// <summary>
        /// Cookie 容器
        /// </summary>
        public CookieContainer Cookies
        {
            get { return _cookieContainer; }
            set { _cookieContainer = value; }
        }

        /// <summary>
        /// 返回带有 Cookie 的 HttpWebRequest。
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                var httpRequest = request as HttpWebRequest;
                httpRequest.AllowAutoRedirect = _allowAutoRedirect;
                httpRequest.CookieContainer = _cookieContainer;
            }
            return request;
        }
    }
}
