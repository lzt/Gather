﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/


using System;
using System.Text;
using Newtonsoft.Json;

namespace sanzi.Model.Base
{
    public class BaseModel : Object, ICloneable
    {
        /// <summary>
        /// 转换为Json字符串
        /// </summary>
        /// <returns></returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
	
        /// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <typeparam name="T">输出类型</typeparam>
        /// <returns></returns>
        public virtual T MegerDefault<T>() where T : BaseModel, new()
        {
            var defaultModel = new T();
            var properties = GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
               var v=propertyInfo.GetValue(this, null);
                if (v == null)
                {
                    var value = propertyInfo.GetValue(defaultModel, null);
                    propertyInfo.SetValue(this, value, null);
                }
            }
            return this as T;
        }
    
        /// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <typeparam name="T">输出类型</typeparam>
        /// <returns></returns>
        public virtual T GetClone<T>() where T : BaseModel, new()
        {
            return Clone() as T;
        }

        /// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public virtual object Clone()
        {
            var typeName = GetType().FullName;
            var obj = GetType().Assembly.CreateInstance(typeName);

            var properties = GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(this, null);
                propertyInfo.SetValue(obj, value, null);
            }

            // ReSharper disable AssignNullToNotNullAttribute
            return obj;
            // ReSharper restore AssignNullToNotNullAttribute
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="propertyNameList"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params string [] propertyNameList) 
        {
            var result = new StringBuilder();

            foreach (var cf in propertyNameList)
            {
                var type = GetType().GetProperty(cf).PropertyType;
                if (type.Name.ToLower() == "string")
                {
                    result.AppendFormat(" temp.{0}='{1}' and", cf, GetType().GetProperty(cf).GetValue(this, null));
                }

                if (type.Name.ToLower() == "int32")
                {
                    result.AppendFormat(" temp.{0}={1} and", cf, GetType().GetProperty(cf).GetValue(this, null));
                }
            }

            var r = result.ToString();
            r = r.TrimEnd("and".ToCharArray());

            return r;
        }
    }
}
