﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Area实体字段名称
    /// </summary>
    public enum AreaField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：ParentId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        ParentId,  
        
        /// <summary>
        /// 字段：Leader
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Leader,  
        
        /// <summary>
        /// 字段：Remarks
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        Remarks,  
        
        /// <summary>
        /// 字段：Other
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Other,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：Tel
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(22)
        /// 实际占用长度：22
        /// 是否可空：True
        /// </summary>
        Tel,  
        
    }

    /// <summary>
    /// Area实体模型
    /// </summary>
    [Serializable]
    public partial class Area : ExBaseEntity<Area>
	{
        #region Area Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                
                                
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Name {get; set;} 
                        
        /// <summary>
        /// 字段：ParentId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 ParentId {get; set;} 
                        
        /// <summary>
        /// 字段：Leader
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Leader {get; set;} 
                        
        /// <summary>
        /// 字段：Remarks
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        [Length("500")]
        [Comment("")]
        [SqlType("varchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual String Remarks {get; set;} 
                        
        /// <summary>
        /// 字段：Other
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Other {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Type {get; set;} 
                        
        /// <summary>
        /// 字段：Tel
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(22)
        /// 实际占用长度：22
        /// 是否可空：True
        /// </summary>
        [Length("22")]
        [Comment("")]
        [SqlType("varchar(22)")]
        [Precision("")]
        [Scale("")]
        public virtual String Tel {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Area()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Area()
        {
            //属性初始化
            Name = "";
            ParentId = 0;
            Leader = "";
            Remarks = "";
            Other = "";
            State = 0;
            Type = 0;
            Tel = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Area  GetClone()
        {
            return GetClone<Area>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params AreaField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}