﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Debt实体字段名称
    /// </summary>
    public enum DebtField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Creditor
        /// 描述：债务人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Creditor,  
        
        /// <summary>
        /// 字段：BookNumber
        /// 描述：账面数
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        BookNumber,  
        
        /// <summary>
        /// 字段：AccountNumber
        /// 描述：帐外数
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        AccountNumber,  
        
        /// <summary>
        /// 字段：SourcePartition
        /// 描述：来源划分
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        SourcePartition,  
        
        /// <summary>
        /// 字段：TimeDivision
        /// 描述：时间划分
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        TimeDivision,  
        
        /// <summary>
        /// 字段：CauseDivision
        /// 描述：原因划分
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        CauseDivision,  
        
        /// <summary>
        /// 字段：FTime
        /// 描述：形成时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        FTime,  
        
        /// <summary>
        /// 字段：Attn
        /// 描述：经办人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Attn,  
        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        UName,  
        
    }

    /// <summary>
    /// Debt实体模型
    /// </summary>
    [Serializable]
    public partial class Debt : ExBaseEntity<Debt>
	{
        #region Debt Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Creditor
        /// 描述：债务人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("债务人")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Creditor {get; set;} 
                        
        /// <summary>
        /// 字段：BookNumber
        /// 描述：账面数
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("账面数")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String BookNumber {get; set;} 
                        
        /// <summary>
        /// 字段：AccountNumber
        /// 描述：帐外数
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("帐外数")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String AccountNumber {get; set;} 
                        
        /// <summary>
        /// 字段：SourcePartition
        /// 描述：来源划分
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("来源划分")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String SourcePartition {get; set;} 
                        
        /// <summary>
        /// 字段：TimeDivision
        /// 描述：时间划分
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("时间划分")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String TimeDivision {get; set;} 
                        
        /// <summary>
        /// 字段：CauseDivision
        /// 描述：原因划分
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("原因划分")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String CauseDivision {get; set;} 
                        
        /// <summary>
        /// 字段：FTime
        /// 描述：形成时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("形成时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime FTime {get; set;} 
                        
        /// <summary>
        /// 字段：Attn
        /// 描述：经办人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("经办人")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Attn {get; set;} 
                        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String UName {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Debt()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Debt()
        {
            //属性初始化
            Creditor = "";
            BookNumber = "";
            AccountNumber = "";
            SourcePartition = "";
            TimeDivision = "";
            CauseDivision = "";
            FTime = ConstantCollection.DbDefaultDateTime;
            Attn = "";
            UserId = 0;
            State = 0;
            Country = 0;
            Village = 0;
            Groups = 0;
            UName = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Debt  GetClone()
        {
            return GetClone<Debt>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params DebtField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}