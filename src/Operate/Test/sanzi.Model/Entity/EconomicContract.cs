﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// EconomicContract实体字段名称
    /// </summary>
    public enum EconomicContractField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：经济合同自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Item
        /// 描述：项目
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Item,  
        
        /// <summary>
        /// 字段：Company
        /// 描述：单位
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Company,  
        
        /// <summary>
        /// 字段：Number
        /// 描述：数量
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Number,  
        
        /// <summary>
        /// 字段：Address
        /// 描述：地点
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Address,  
        
        /// <summary>
        /// 字段：Value
        /// 描述：原值
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Value,  
        
        /// <summary>
        /// 字段：STime
        /// 描述：开始时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        STime,  
        
        /// <summary>
        /// 字段：ETime
        /// 描述：终止时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        ETime,  
        
        /// <summary>
        /// 字段：Contractor
        /// 描述：承包人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Contractor,  
        
        /// <summary>
        /// 字段：Contracting
        /// 描述：承包金
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Contracting,  
        
        /// <summary>
        /// 字段：PayMent
        /// 描述：支付方式
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        PayMent,  
        
        /// <summary>
        /// 字段：Contract
        /// 描述：合同履行0,1
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Contract,  
        
        /// <summary>
        /// 字段：Bidding
        /// 描述：招投标0,1
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Bidding,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：Uname
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        Uname,  
        
    }

    /// <summary>
    /// EconomicContract实体模型
    /// </summary>
    [Serializable]
    public partial class EconomicContract : ExBaseEntity<EconomicContract>
	{
        #region EconomicContract Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：经济合同自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("经济合同自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Item
        /// 描述：项目
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("项目")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Item {get; set;} 
                        
        /// <summary>
        /// 字段：Company
        /// 描述：单位
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("单位")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Company {get; set;} 
                        
        /// <summary>
        /// 字段：Number
        /// 描述：数量
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("数量")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Number {get; set;} 
                        
        /// <summary>
        /// 字段：Address
        /// 描述：地点
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("地点")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Address {get; set;} 
                        
        /// <summary>
        /// 字段：Value
        /// 描述：原值
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("原值")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Value {get; set;} 
                        
        /// <summary>
        /// 字段：STime
        /// 描述：开始时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("开始时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime STime {get; set;} 
                        
        /// <summary>
        /// 字段：ETime
        /// 描述：终止时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("终止时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime ETime {get; set;} 
                        
        /// <summary>
        /// 字段：Contractor
        /// 描述：承包人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("承包人")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Contractor {get; set;} 
                        
        /// <summary>
        /// 字段：Contracting
        /// 描述：承包金
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("承包金")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Contracting {get; set;} 
                        
        /// <summary>
        /// 字段：PayMent
        /// 描述：支付方式
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("支付方式")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String PayMent {get; set;} 
                        
        /// <summary>
        /// 字段：Contract
        /// 描述：合同履行0,1
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("合同履行0,1")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Contract {get; set;} 
                        
        /// <summary>
        /// 字段：Bidding
        /// 描述：招投标0,1
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("招投标0,1")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Bidding {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Type {get; set;} 
                        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：Uname
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String Uname {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static EconomicContract()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public EconomicContract()
        {
            //属性初始化
            Item = "";
            Company = "";
            Number = 0;
            Address = "";
            Value = 0;
            STime = ConstantCollection.DbDefaultDateTime;
            ETime = ConstantCollection.DbDefaultDateTime;
            Contractor = "";
            Contracting = "";
            PayMent = "";
            Contract = 0;
            Bidding = 0;
            Type = 0;
            UserId = 0;
            State = 0;
            Country = 0;
            Village = 0;
            Groups = 0;
            Uname = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override EconomicContract  GetClone()
        {
            return GetClone<EconomicContract>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params EconomicContractField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}