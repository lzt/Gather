﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Personnel实体字段名称
    /// </summary>
    public enum PersonnelField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：人员自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Person
        /// 描述：人员姓名
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Person,  
        
        /// <summary>
        /// 字段：Sex
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Sex,  
        
        /// <summary>
        /// 字段：Birthday
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Birthday,  
        
        /// <summary>
        /// 字段：Phone
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        Phone,  
        
        /// <summary>
        /// 字段：FamilyId
        /// 描述：人员隶属家庭
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        FamilyId,  
        
        /// <summary>
        /// 字段：Relationship
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Relationship,  
        
        /// <summary>
        /// 字段：ORelationship
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        ORelationship,  
        
        /// <summary>
        /// 字段：Conditions
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Conditions,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Tel
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Tel,  
        
        /// <summary>
        /// 字段：Chengbao
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        Chengbao,  
        
        /// <summary>
        /// 字段：Zhaijidi
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Zhaijidi,  
        
        /// <summary>
        /// 字段：ZhaiMianji
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        ZhaiMianji,  
        
        /// <summary>
        /// 字段：HouseStruct
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        HouseStruct,  
        
        /// <summary>
        /// 字段：Jianzhu
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Jianzhu,  
        
        /// <summary>
        /// 字段：Youdai
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Youdai,  
        
        /// <summary>
        /// 字段：Remarks
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        Remarks,  
        
        /// <summary>
        /// 字段：Times
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        Times,  
        
    }

    /// <summary>
    /// Personnel实体模型
    /// </summary>
    [Serializable]
    public partial class Personnel : ExBaseEntity<Personnel>
	{
        #region Personnel Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：人员自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("人员自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Person
        /// 描述：人员姓名
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("人员姓名")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Person {get; set;} 
                        
        /// <summary>
        /// 字段：Sex
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Sex {get; set;} 
                        
        /// <summary>
        /// 字段：Birthday
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Birthday {get; set;} 
                        
        /// <summary>
        /// 字段：Phone
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String Phone {get; set;} 
                        
        /// <summary>
        /// 字段：FamilyId
        /// 描述：人员隶属家庭
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("人员隶属家庭")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 FamilyId {get; set;} 
                        
        /// <summary>
        /// 字段：Relationship
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Relationship {get; set;} 
                        
        /// <summary>
        /// 字段：ORelationship
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 ORelationship {get; set;} 
                        
        /// <summary>
        /// 字段：Conditions
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Conditions {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Tel
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Tel {get; set;} 
                        
        /// <summary>
        /// 字段：Chengbao
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String Chengbao {get; set;} 
                        
        /// <summary>
        /// 字段：Zhaijidi
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Zhaijidi {get; set;} 
                        
        /// <summary>
        /// 字段：ZhaiMianji
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String ZhaiMianji {get; set;} 
                        
        /// <summary>
        /// 字段：HouseStruct
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 HouseStruct {get; set;} 
                        
        /// <summary>
        /// 字段：Jianzhu
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Jianzhu {get; set;} 
                        
        /// <summary>
        /// 字段：Youdai
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Youdai {get; set;} 
                        
        /// <summary>
        /// 字段：Remarks
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        [Length("500")]
        [Comment("")]
        [SqlType("varchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual String Remarks {get; set;} 
                        
        /// <summary>
        /// 字段：Times
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime Times {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Personnel()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Personnel()
        {
            //属性初始化
            Person = "";
            Sex = 0;
            Birthday = "";
            Phone = "";
            FamilyId = 0;
            Relationship = "";
            ORelationship = 0;
            Conditions = "";
            Country = 0;
            Village = 0;
            Groups = 0;
            UserId = 0;
            State = 0;
            Tel = "";
            Chengbao = "";
            Zhaijidi = "";
            ZhaiMianji = "";
            HouseStruct = 0;
            Jianzhu = "";
            Youdai = 0;
            Remarks = "";
            Times = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Personnel  GetClone()
        {
            return GetClone<Personnel>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params PersonnelField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}