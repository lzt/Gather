﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Assets实体字段名称
    /// </summary>
    public enum AssetsField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：资产自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：名称
        /// 映射类型：String
        /// 数据库中字段类型：varchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：Business
        /// 描述：经营使用情况
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Business,  
        
        /// <summary>
        /// 字段：InTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        InTime,  
        
        /// <summary>
        /// 字段：Partner
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Partner,  
        
        /// <summary>
        /// 字段：AnnualIncome
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        AnnualIncome,  
        
        /// <summary>
        /// 字段：Contractor
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Contractor,  
        
        /// <summary>
        /// 字段：STime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        STime,  
        
        /// <summary>
        /// 字段：ETime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        ETime,  
        
        /// <summary>
        /// 字段：YearMoney
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        YearMoney,  
        
        /// <summary>
        /// 字段：Contract
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Contract,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        UName,  
        
    }

    /// <summary>
    /// Assets实体模型
    /// </summary>
    [Serializable]
    public partial class Assets : ExBaseEntity<Assets>
	{
        #region Assets Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：资产自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("资产自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Name
        /// 描述：名称
        /// 映射类型：String
        /// 数据库中字段类型：varchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("名称")]
        [SqlType("varchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual String Name {get; set;} 
                        
        /// <summary>
        /// 字段：Business
        /// 描述：经营使用情况
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("经营使用情况")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Business {get; set;} 
                        
        /// <summary>
        /// 字段：InTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime InTime {get; set;} 
                        
        /// <summary>
        /// 字段：Partner
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Partner {get; set;} 
                        
        /// <summary>
        /// 字段：AnnualIncome
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String AnnualIncome {get; set;} 
                        
        /// <summary>
        /// 字段：Contractor
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Contractor {get; set;} 
                        
        /// <summary>
        /// 字段：STime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime STime {get; set;} 
                        
        /// <summary>
        /// 字段：ETime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime ETime {get; set;} 
                        
        /// <summary>
        /// 字段：YearMoney
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String YearMoney {get; set;} 
                        
        /// <summary>
        /// 字段：Contract
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Contract {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Type {get; set;} 
                        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String UName {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Assets()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Assets()
        {
            //属性初始化
            Name = "";
            Business = 0;
            InTime = ConstantCollection.DbDefaultDateTime;
            Partner = "";
            AnnualIncome = "";
            Contractor = "";
            STime = ConstantCollection.DbDefaultDateTime;
            ETime = ConstantCollection.DbDefaultDateTime;
            YearMoney = "";
            Contract = 0;
            Type = 0;
            UserId = 0;
            State = 0;
            Country = 0;
            Village = 0;
            Groups = 0;
            UName = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Assets  GetClone()
        {
            return GetClone<Assets>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params AssetsField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}