﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Complaint实体字段名称
    /// </summary>
    public enum ComplaintField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Title,  
        
        /// <summary>
        /// 字段：Contents
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        Contents,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：Tel
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Tel,  
        
        /// <summary>
        /// 字段：ATime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        ATime,  
        
        /// <summary>
        /// 字段：Results
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        Results,  
        
        /// <summary>
        /// 字段：PersonId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        PersonId,  
        
    }

    /// <summary>
    /// Complaint实体模型
    /// </summary>
    [Serializable]
    public partial class Complaint : ExBaseEntity<Complaint>
	{
        #region Complaint Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                
                                
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Title {get; set;} 
                        
        /// <summary>
        /// 字段：Contents
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        [Length("500")]
        [Comment("")]
        [SqlType("varchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual String Contents {get; set;} 
                        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Name {get; set;} 
                        
        /// <summary>
        /// 字段：Tel
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Tel {get; set;} 
                        
        /// <summary>
        /// 字段：ATime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime ATime {get; set;} 
                        
        /// <summary>
        /// 字段：Results
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        [Length("500")]
        [Comment("")]
        [SqlType("varchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual String Results {get; set;} 
                        
        /// <summary>
        /// 字段：PersonId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 PersonId {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Complaint()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Complaint()
        {
            //属性初始化
            Title = "";
            Contents = "";
            Name = "";
            Tel = "";
            ATime = ConstantCollection.DbDefaultDateTime;
            Results = "";
            PersonId = 0;
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Complaint  GetClone()
        {
            return GetClone<Complaint>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params ComplaintField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}