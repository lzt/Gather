﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Leadership实体字段名称
    /// </summary>
    public enum LeadershipField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Leader
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Leader,  
        
        /// <summary>
        /// 字段：Director
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Director,  
        
        /// <summary>
        /// 字段：CncaDirector
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        CncaDirector,  
        
        /// <summary>
        /// 字段：Documents
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Documents,  
        
        /// <summary>
        /// 字段：Commissioners
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Commissioners,  
        
        /// <summary>
        /// 字段：CncaCommissioners
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        CncaCommissioners,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
    }

    /// <summary>
    /// Leadership实体模型
    /// </summary>
    [Serializable]
    public partial class Leadership : ExBaseEntity<Leadership>
	{
        #region Leadership Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Leader
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Leader {get; set;} 
                        
        /// <summary>
        /// 字段：Director
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Director {get; set;} 
                        
        /// <summary>
        /// 字段：CncaDirector
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String CncaDirector {get; set;} 
                        
        /// <summary>
        /// 字段：Documents
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Documents {get; set;} 
                        
        /// <summary>
        /// 字段：Commissioners
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Commissioners {get; set;} 
                        
        /// <summary>
        /// 字段：CncaCommissioners
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String CncaCommissioners {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Leadership()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Leadership()
        {
            //属性初始化
            Leader = "";
            Director = "";
            CncaDirector = "";
            Documents = "";
            Commissioners = "";
            CncaCommissioners = "";
            Country = 0;
            Village = 0;
            Groups = 0;
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Leadership  GetClone()
        {
            return GetClone<Leadership>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params LeadershipField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}