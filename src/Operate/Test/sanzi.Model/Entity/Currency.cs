﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Currency实体字段名称
    /// </summary>
    public enum CurrencyField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：BankBookMoney
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        BankBookMoney,  
        
        /// <summary>
        /// 字段：BankActualamount
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        BankActualamount,  
        
        /// <summary>
        /// 字段：BankReason
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(300)
        /// 实际占用长度：300
        /// 是否可空：True
        /// </summary>
        BankReason,  
        
        /// <summary>
        /// 字段：InventoryBookMoney
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        InventoryBookMoney,  
        
        /// <summary>
        /// 字段：InventoryActualamount
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        InventoryActualamount,  
        
        /// <summary>
        /// 字段：InventoryReason
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(300)
        /// 实际占用长度：300
        /// 是否可空：True
        /// </summary>
        InventoryReason,  
        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(300)
        /// 实际占用长度：300
        /// 是否可空：True
        /// </summary>
        Remark,  
        
        /// <summary>
        /// 字段：BankAccount
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        BankAccount,  
        
        /// <summary>
        /// 字段：Bank
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Bank,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        UName,  
        
    }

    /// <summary>
    /// Currency实体模型
    /// </summary>
    [Serializable]
    public partial class Currency : ExBaseEntity<Currency>
	{
        #region Currency Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：BankBookMoney
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("money(8)")]
        [Precision("19")]
        [Scale("4")]
        public virtual Decimal BankBookMoney {get; set;} 
                        
        /// <summary>
        /// 字段：BankActualamount
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("money(8)")]
        [Precision("19")]
        [Scale("4")]
        public virtual Decimal BankActualamount {get; set;} 
                        
        /// <summary>
        /// 字段：BankReason
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(300)
        /// 实际占用长度：300
        /// 是否可空：True
        /// </summary>
        [Length("300")]
        [Comment("")]
        [SqlType("varchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual String BankReason {get; set;} 
                        
        /// <summary>
        /// 字段：InventoryBookMoney
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("money(8)")]
        [Precision("19")]
        [Scale("4")]
        public virtual Decimal InventoryBookMoney {get; set;} 
                        
        /// <summary>
        /// 字段：InventoryActualamount
        /// 描述：
        /// 映射类型：Decimal
        /// 数据库中字段类型：money(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("money(8)")]
        [Precision("19")]
        [Scale("4")]
        public virtual Decimal InventoryActualamount {get; set;} 
                        
        /// <summary>
        /// 字段：InventoryReason
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(300)
        /// 实际占用长度：300
        /// 是否可空：True
        /// </summary>
        [Length("300")]
        [Comment("")]
        [SqlType("varchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual String InventoryReason {get; set;} 
                        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(300)
        /// 实际占用长度：300
        /// 是否可空：True
        /// </summary>
        [Length("300")]
        [Comment("")]
        [SqlType("varchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual String Remark {get; set;} 
                        
        /// <summary>
        /// 字段：BankAccount
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String BankAccount {get; set;} 
                        
        /// <summary>
        /// 字段：Bank
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Bank {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String UName {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Currency()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Currency()
        {
            //属性初始化
            BankReason = "";
            InventoryReason = "";
            Remark = "";
            BankAccount = "";
            Bank = "";
            State = 0;
            Country = 0;
            Village = 0;
            Groups = 0;
            UName = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Currency  GetClone()
        {
            return GetClone<Currency>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params CurrencyField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}