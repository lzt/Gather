﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// Property实体字段名称
    /// </summary>
    public enum PropertyField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：资产1
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：名称
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：Unit
        /// 描述：计量单位
        /// 映射类型：String
        /// 数据库中字段类型：varchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        Unit,  
        
        /// <summary>
        /// 字段：Amount
        /// 描述：数量
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Amount,  
        
        /// <summary>
        /// 字段：Value
        /// 描述：原值
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Value,  
        
        /// <summary>
        /// 字段：Depreciation
        /// 描述：折旧
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Depreciation,  
        
        /// <summary>
        /// 字段：Net
        /// 描述：净值
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Net,  
        
        /// <summary>
        /// 字段：Year
        /// 描述：构建年份
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Year,  
        
        /// <summary>
        /// 字段：Address
        /// 描述：地址
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        Address,  
        
        /// <summary>
        /// 字段：Responsible
        /// 描述：责任人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Responsible,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        UName,  
        
    }

    /// <summary>
    /// Property实体模型
    /// </summary>
    [Serializable]
    public partial class Property : ExBaseEntity<Property>
	{
        #region Property Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：资产1
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("资产1")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Name
        /// 描述：名称
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("名称")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Name {get; set;} 
                        
        /// <summary>
        /// 字段：Unit
        /// 描述：计量单位
        /// 映射类型：String
        /// 数据库中字段类型：varchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("计量单位")]
        [SqlType("varchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual String Unit {get; set;} 
                        
        /// <summary>
        /// 字段：Amount
        /// 描述：数量
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("数量")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Amount {get; set;} 
                        
        /// <summary>
        /// 字段：Value
        /// 描述：原值
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("原值")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Value {get; set;} 
                        
        /// <summary>
        /// 字段：Depreciation
        /// 描述：折旧
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("折旧")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Depreciation {get; set;} 
                        
        /// <summary>
        /// 字段：Net
        /// 描述：净值
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("净值")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Net {get; set;} 
                        
        /// <summary>
        /// 字段：Year
        /// 描述：构建年份
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("构建年份")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Year {get; set;} 
                        
        /// <summary>
        /// 字段：Address
        /// 描述：地址
        /// 映射类型：String
        /// 数据库中字段类型：varchar(500)
        /// 实际占用长度：500
        /// 是否可空：True
        /// </summary>
        [Length("500")]
        [Comment("地址")]
        [SqlType("varchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual String Address {get; set;} 
                        
        /// <summary>
        /// 字段：Responsible
        /// 描述：责任人
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("责任人")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Responsible {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Type {get; set;} 
                        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String UName {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Property()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Property()
        {
            //属性初始化
            Name = "";
            Unit = "";
            Amount = "";
            Value = "";
            Depreciation = "";
            Net = "";
            Year = "";
            Address = "";
            Responsible = "";
            Type = 0;
            UserId = 0;
            State = 0;
            Country = 0;
            Village = 0;
            Groups = 0;
            UName = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override Property  GetClone()
        {
            return GetClone<Property>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params PropertyField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}