﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using sanzi.Model.Base;

namespace sanzi.Model.Entity
{
    /// <summary>
    /// ResourceAssets实体字段名称
    /// </summary>
    public enum ResourceAssetsField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：资产自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Assets
        /// 描述：资产名称
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Assets,  
        
        /// <summary>
        /// 字段：Category
        /// 描述：资产类别
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        Category,  
        
        /// <summary>
        /// 字段：Position
        /// 描述：资产位置
        /// 映射类型：String
        /// 数据库中字段类型：varchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        Position,  
        
        /// <summary>
        /// 字段：Area
        /// 描述：面积
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        Area,  
        
        /// <summary>
        /// 字段：Business
        /// 描述：经营使用情况
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Business,  
        
        /// <summary>
        /// 字段：Ownership
        /// 描述：清查核实后确认土地权属
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Ownership,  
        
        /// <summary>
        /// 字段：Remark
        /// 描述：备注
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        Remark,  
        
        /// <summary>
        /// 字段：STime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        STime,  
        
        /// <summary>
        /// 字段：ETime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        ETime,  
        
        /// <summary>
        /// 字段：Contractor
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Contractor,  
        
        /// <summary>
        /// 字段：YearMoney
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        YearMoney,  
        
        /// <summary>
        /// 字段：Payment
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Payment,  
        
        /// <summary>
        /// 字段：Contract
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Contract,  
        
        /// <summary>
        /// 字段：InTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        InTime,  
        
        /// <summary>
        /// 字段：Partner
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Partner,  
        
        /// <summary>
        /// 字段：AnnualIncome
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        AnnualIncome,  
        
        /// <summary>
        /// 字段：Payments
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        Payments,  
        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,  
        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        State,  
        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Country,  
        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Village,  
        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Groups,  
        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        UName,  
        
        /// <summary>
        /// 字段：HetongNum
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        HetongNum,  
        
    }

    /// <summary>
    /// ResourceAssets实体模型
    /// </summary>
    [Serializable]
    public partial class ResourceAssets : ExBaseEntity<ResourceAssets>
	{
        #region ResourceAssets Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：资产自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("资产自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Assets
        /// 描述：资产名称
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("资产名称")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Assets {get; set;} 
                        
        /// <summary>
        /// 字段：Category
        /// 描述：资产类别
        /// 映射类型：String
        /// 数据库中字段类型：varchar(100)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("资产类别")]
        [SqlType("varchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual String Category {get; set;} 
                        
        /// <summary>
        /// 字段：Position
        /// 描述：资产位置
        /// 映射类型：String
        /// 数据库中字段类型：varchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("资产位置")]
        [SqlType("varchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual String Position {get; set;} 
                        
        /// <summary>
        /// 字段：Area
        /// 描述：面积
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("面积")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String Area {get; set;} 
                        
        /// <summary>
        /// 字段：Business
        /// 描述：经营使用情况
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("经营使用情况")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Business {get; set;} 
                        
        /// <summary>
        /// 字段：Ownership
        /// 描述：清查核实后确认土地权属
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("清查核实后确认土地权属")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Ownership {get; set;} 
                        
        /// <summary>
        /// 字段：Remark
        /// 描述：备注
        /// 映射类型：String
        /// 数据库中字段类型：varchar(10)
        /// 实际占用长度：10
        /// 是否可空：True
        /// </summary>
        [Length("10")]
        [Comment("备注")]
        [SqlType("varchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual String Remark {get; set;} 
                        
        /// <summary>
        /// 字段：STime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime STime {get; set;} 
                        
        /// <summary>
        /// 字段：ETime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime ETime {get; set;} 
                        
        /// <summary>
        /// 字段：Contractor
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Contractor {get; set;} 
                        
        /// <summary>
        /// 字段：YearMoney
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String YearMoney {get; set;} 
                        
        /// <summary>
        /// 字段：Payment
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Payment {get; set;} 
                        
        /// <summary>
        /// 字段：Contract
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Contract {get; set;} 
                        
        /// <summary>
        /// 字段：InTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime InTime {get; set;} 
                        
        /// <summary>
        /// 字段：Partner
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Partner {get; set;} 
                        
        /// <summary>
        /// 字段：AnnualIncome
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String AnnualIncome {get; set;} 
                        
        /// <summary>
        /// 字段：Payments
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String Payments {get; set;} 
                        
        /// <summary>
        /// 字段：UserId
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId {get; set;} 
                        
        /// <summary>
        /// 字段：State
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 State {get; set;} 
                        
        /// <summary>
        /// 字段：Country
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Country {get; set;} 
                        
        /// <summary>
        /// 字段：Village
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Village {get; set;} 
                        
        /// <summary>
        /// 字段：Groups
        /// 描述：
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Groups {get; set;} 
                        
        /// <summary>
        /// 字段：UName
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("")]
        [SqlType("varchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String UName {get; set;} 
                        
        /// <summary>
        /// 字段：HetongNum
        /// 描述：
        /// 映射类型：String
        /// 数据库中字段类型：varchar(50)
        /// 实际占用长度：50
        /// 是否可空：True
        /// </summary>
        [Length("50")]
        [Comment("")]
        [SqlType("varchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual String HetongNum {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static ResourceAssets()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public ResourceAssets()
        {
            //属性初始化
            Assets = "";
            Category = "";
            Position = "";
            Area = "";
            Business = 0;
            Ownership = "";
            Remark = "";
            STime = ConstantCollection.DbDefaultDateTime;
            ETime = ConstantCollection.DbDefaultDateTime;
            Contractor = "";
            YearMoney = "";
            Payment = "";
            Contract = 0;
            InTime = ConstantCollection.DbDefaultDateTime;
            Partner = "";
            AnnualIncome = "";
            Payments = "";
            UserId = 0;
            State = 0;
            Country = 0;
            Village = 0;
            Groups = 0;
            UName = "";
            HetongNum = "";
        }

        #endregion

		#region Function

		/// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <returns></returns>
        public override ResourceAssets  GetClone()
        {
            return GetClone<ResourceAssets>();
        }

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params ResourceAssetsField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}