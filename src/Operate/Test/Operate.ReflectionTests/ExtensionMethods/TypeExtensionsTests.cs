﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Operate.Reflection.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.Reflection.ExtensionMethods.Test
{
    public enum UserRoleStatus
    {
        /// <summary>
        /// 不限，仅排除已删除
        /// </summary>
        [System.ComponentModel.Description("不限")]
        Unlimited = -10000,

        /// <summary>
        /// 已启用
        /// </summary>
        [System.ComponentModel.Description("已启用")]
        Enabled = 100,

        /// <summary>
        /// 已禁用
        /// </summary>
        [System.ComponentModel.Description("已禁用")]
        Disabled = -200,

        /// <summary>
        /// 已删除
        /// </summary>
        [System.ComponentModel.Description("已删除")]
        Deleted = -300
    }

    [TestClass()]
    public class TypeExtensionsTests
    {
        [TestMethod()]
        public void GetAttributeTest()
        {
            var data = EnumEx.ConvertEnumToObject<UserRoleStatus>(false, new List<UserRoleStatus>() { UserRoleStatus .Unlimited});
        }
    }

    [Re("n", "v")]
    [Se("tete", "erwr")]
    public class Tt
    {
        [Re("rrr", "vvv")]
        public string T1 { get; set; }
        public string T2 { get; set; }
    }

    public class Se : Attribute
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Se()
        {

        }

        public Se(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }

    public class Re : Attribute
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Re()
        {

        }

        public Re(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
