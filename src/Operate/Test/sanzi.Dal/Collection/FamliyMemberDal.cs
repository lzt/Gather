﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class FamliyMemberDal : ExBaseDal<FamliyMember>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public FamliyMemberDal(): this(Initialization.GetXmlConfig(typeof(FamliyMember)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static FamliyMemberDal CreateDal()
        {
			return new FamliyMemberDal(Initialization.GetXmlConfig(typeof(FamliyMember)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static FamliyMemberDal()
        {
           ModelName= typeof(FamliyMember).Name;
           var item = new FamliyMember();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public FamliyMemberDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "FamliyMember";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public FamliyMember GetByName(String value)
        {
            var model = new FamliyMember { Name = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public FamliyMember GetByName(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Name));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public FamliyMember GetBySex(Int32 value)
        {
            var model = new FamliyMember { Sex = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public FamliyMember GetBySex(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Sex));
        }

        #endregion

                
        #region Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public FamliyMember GetByBirthday(String value)
        {
            var model = new FamliyMember { Birthday = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Birthday));
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public FamliyMember GetByBirthday(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Birthday));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public FamliyMember GetByPhone(String value)
        {
            var model = new FamliyMember { Phone = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public FamliyMember GetByPhone(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Phone));
        }

        #endregion

                
        #region Relationship

        /// <summary>
        /// 根据属性Relationship获取数据实体
        /// </summary>
        public FamliyMember GetByRelationship(String value)
        {
            var model = new FamliyMember { Relationship = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Relationship));
        }

        /// <summary>
        /// 根据属性Relationship获取数据实体
        /// </summary>
        public FamliyMember GetByRelationship(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Relationship));
        }

        #endregion

                
        #region ORelationship

        /// <summary>
        /// 根据属性ORelationship获取数据实体
        /// </summary>
        public FamliyMember GetByORelationship(Int32 value)
        {
            var model = new FamliyMember { ORelationship = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.ORelationship));
        }

        /// <summary>
        /// 根据属性ORelationship获取数据实体
        /// </summary>
        public FamliyMember GetByORelationship(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.ORelationship));
        }

        #endregion

                
        #region Conditions

        /// <summary>
        /// 根据属性Conditions获取数据实体
        /// </summary>
        public FamliyMember GetByConditions(String value)
        {
            var model = new FamliyMember { Conditions = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Conditions));
        }

        /// <summary>
        /// 根据属性Conditions获取数据实体
        /// </summary>
        public FamliyMember GetByConditions(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Conditions));
        }

        #endregion

                
        #region Remarks

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public FamliyMember GetByRemarks(String value)
        {
            var model = new FamliyMember { Remarks = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Remarks));
        }

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public FamliyMember GetByRemarks(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Remarks));
        }

        #endregion

                
        #region Times

        /// <summary>
        /// 根据属性Times获取数据实体
        /// </summary>
        public FamliyMember GetByTimes(DateTime value)
        {
            var model = new FamliyMember { Times = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Times));
        }

        /// <summary>
        /// 根据属性Times获取数据实体
        /// </summary>
        public FamliyMember GetByTimes(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Times));
        }

        #endregion

                
        #region FamliyId

        /// <summary>
        /// 根据属性FamliyId获取数据实体
        /// </summary>
        public FamliyMember GetByFamliyId(Int32 value)
        {
            var model = new FamliyMember { FamliyId = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.FamliyId));
        }

        /// <summary>
        /// 根据属性FamliyId获取数据实体
        /// </summary>
        public FamliyMember GetByFamliyId(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.FamliyId));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public FamliyMember GetByCountry(Int32 value)
        {
            var model = new FamliyMember { Country = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public FamliyMember GetByCountry(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public FamliyMember GetByVillage(Int32 value)
        {
            var model = new FamliyMember { Village = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public FamliyMember GetByVillage(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public FamliyMember GetByGroups(Int32 value)
        {
            var model = new FamliyMember { Groups = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public FamliyMember GetByGroups(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.Groups));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public FamliyMember GetByState(Int32 value)
        {
            var model = new FamliyMember { State = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public FamliyMember GetByState(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.State));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public FamliyMember GetByUserId(Int32 value)
        {
            var model = new FamliyMember { UserId = value };
            return GetByWhere(model.GetFilterString(FamliyMemberField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public FamliyMember GetByUserId(FamliyMember model)
        {
            return GetByWhere(model.GetFilterString(FamliyMemberField.UserId));
        }

        #endregion

        #endregion
    }
}