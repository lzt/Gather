﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class DebtDal : ExBaseDal<Debt>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public DebtDal(): this(Initialization.GetXmlConfig(typeof(Debt)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static DebtDal CreateDal()
        {
			return new DebtDal(Initialization.GetXmlConfig(typeof(Debt)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static DebtDal()
        {
           ModelName= typeof(Debt).Name;
           var item = new Debt();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public DebtDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Debt";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Creditor

        /// <summary>
        /// 根据属性Creditor获取数据实体
        /// </summary>
        public Debt GetByCreditor(String value)
        {
            var model = new Debt { Creditor = value };
            return GetByWhere(model.GetFilterString(DebtField.Creditor));
        }

        /// <summary>
        /// 根据属性Creditor获取数据实体
        /// </summary>
        public Debt GetByCreditor(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.Creditor));
        }

        #endregion

                
        #region BookNumber

        /// <summary>
        /// 根据属性BookNumber获取数据实体
        /// </summary>
        public Debt GetByBookNumber(String value)
        {
            var model = new Debt { BookNumber = value };
            return GetByWhere(model.GetFilterString(DebtField.BookNumber));
        }

        /// <summary>
        /// 根据属性BookNumber获取数据实体
        /// </summary>
        public Debt GetByBookNumber(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.BookNumber));
        }

        #endregion

                
        #region AccountNumber

        /// <summary>
        /// 根据属性AccountNumber获取数据实体
        /// </summary>
        public Debt GetByAccountNumber(String value)
        {
            var model = new Debt { AccountNumber = value };
            return GetByWhere(model.GetFilterString(DebtField.AccountNumber));
        }

        /// <summary>
        /// 根据属性AccountNumber获取数据实体
        /// </summary>
        public Debt GetByAccountNumber(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.AccountNumber));
        }

        #endregion

                
        #region SourcePartition

        /// <summary>
        /// 根据属性SourcePartition获取数据实体
        /// </summary>
        public Debt GetBySourcePartition(String value)
        {
            var model = new Debt { SourcePartition = value };
            return GetByWhere(model.GetFilterString(DebtField.SourcePartition));
        }

        /// <summary>
        /// 根据属性SourcePartition获取数据实体
        /// </summary>
        public Debt GetBySourcePartition(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.SourcePartition));
        }

        #endregion

                
        #region TimeDivision

        /// <summary>
        /// 根据属性TimeDivision获取数据实体
        /// </summary>
        public Debt GetByTimeDivision(String value)
        {
            var model = new Debt { TimeDivision = value };
            return GetByWhere(model.GetFilterString(DebtField.TimeDivision));
        }

        /// <summary>
        /// 根据属性TimeDivision获取数据实体
        /// </summary>
        public Debt GetByTimeDivision(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.TimeDivision));
        }

        #endregion

                
        #region CauseDivision

        /// <summary>
        /// 根据属性CauseDivision获取数据实体
        /// </summary>
        public Debt GetByCauseDivision(String value)
        {
            var model = new Debt { CauseDivision = value };
            return GetByWhere(model.GetFilterString(DebtField.CauseDivision));
        }

        /// <summary>
        /// 根据属性CauseDivision获取数据实体
        /// </summary>
        public Debt GetByCauseDivision(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.CauseDivision));
        }

        #endregion

                
        #region FTime

        /// <summary>
        /// 根据属性FTime获取数据实体
        /// </summary>
        public Debt GetByFTime(DateTime value)
        {
            var model = new Debt { FTime = value };
            return GetByWhere(model.GetFilterString(DebtField.FTime));
        }

        /// <summary>
        /// 根据属性FTime获取数据实体
        /// </summary>
        public Debt GetByFTime(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.FTime));
        }

        #endregion

                
        #region Attn

        /// <summary>
        /// 根据属性Attn获取数据实体
        /// </summary>
        public Debt GetByAttn(String value)
        {
            var model = new Debt { Attn = value };
            return GetByWhere(model.GetFilterString(DebtField.Attn));
        }

        /// <summary>
        /// 根据属性Attn获取数据实体
        /// </summary>
        public Debt GetByAttn(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.Attn));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Debt GetByUserId(Int32 value)
        {
            var model = new Debt { UserId = value };
            return GetByWhere(model.GetFilterString(DebtField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Debt GetByUserId(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Debt GetByState(Int32 value)
        {
            var model = new Debt { State = value };
            return GetByWhere(model.GetFilterString(DebtField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Debt GetByState(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.State));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Debt GetByCountry(Int32 value)
        {
            var model = new Debt { Country = value };
            return GetByWhere(model.GetFilterString(DebtField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Debt GetByCountry(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Debt GetByVillage(Int32 value)
        {
            var model = new Debt { Village = value };
            return GetByWhere(model.GetFilterString(DebtField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Debt GetByVillage(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Debt GetByGroups(Int32 value)
        {
            var model = new Debt { Groups = value };
            return GetByWhere(model.GetFilterString(DebtField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Debt GetByGroups(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.Groups));
        }

        #endregion

                
        #region UName

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Debt GetByUName(String value)
        {
            var model = new Debt { UName = value };
            return GetByWhere(model.GetFilterString(DebtField.UName));
        }

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Debt GetByUName(Debt model)
        {
            return GetByWhere(model.GetFilterString(DebtField.UName));
        }

        #endregion

        #endregion
    }
}