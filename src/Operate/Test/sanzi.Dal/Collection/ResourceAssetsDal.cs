﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class ResourceAssetsDal : ExBaseDal<ResourceAssets>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ResourceAssetsDal(): this(Initialization.GetXmlConfig(typeof(ResourceAssets)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ResourceAssetsDal CreateDal()
        {
			return new ResourceAssetsDal(Initialization.GetXmlConfig(typeof(ResourceAssets)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ResourceAssetsDal()
        {
           ModelName= typeof(ResourceAssets).Name;
           var item = new ResourceAssets();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ResourceAssetsDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ResourceAssets";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Assets

        /// <summary>
        /// 根据属性Assets获取数据实体
        /// </summary>
        public ResourceAssets GetByAssets(String value)
        {
            var model = new ResourceAssets { Assets = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Assets));
        }

        /// <summary>
        /// 根据属性Assets获取数据实体
        /// </summary>
        public ResourceAssets GetByAssets(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Assets));
        }

        #endregion

                
        #region Category

        /// <summary>
        /// 根据属性Category获取数据实体
        /// </summary>
        public ResourceAssets GetByCategory(String value)
        {
            var model = new ResourceAssets { Category = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Category));
        }

        /// <summary>
        /// 根据属性Category获取数据实体
        /// </summary>
        public ResourceAssets GetByCategory(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Category));
        }

        #endregion

                
        #region Position

        /// <summary>
        /// 根据属性Position获取数据实体
        /// </summary>
        public ResourceAssets GetByPosition(String value)
        {
            var model = new ResourceAssets { Position = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Position));
        }

        /// <summary>
        /// 根据属性Position获取数据实体
        /// </summary>
        public ResourceAssets GetByPosition(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Position));
        }

        #endregion

                
        #region Area

        /// <summary>
        /// 根据属性Area获取数据实体
        /// </summary>
        public ResourceAssets GetByArea(String value)
        {
            var model = new ResourceAssets { Area = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Area));
        }

        /// <summary>
        /// 根据属性Area获取数据实体
        /// </summary>
        public ResourceAssets GetByArea(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Area));
        }

        #endregion

                
        #region Business

        /// <summary>
        /// 根据属性Business获取数据实体
        /// </summary>
        public ResourceAssets GetByBusiness(Int32 value)
        {
            var model = new ResourceAssets { Business = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Business));
        }

        /// <summary>
        /// 根据属性Business获取数据实体
        /// </summary>
        public ResourceAssets GetByBusiness(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Business));
        }

        #endregion

                
        #region Ownership

        /// <summary>
        /// 根据属性Ownership获取数据实体
        /// </summary>
        public ResourceAssets GetByOwnership(String value)
        {
            var model = new ResourceAssets { Ownership = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Ownership));
        }

        /// <summary>
        /// 根据属性Ownership获取数据实体
        /// </summary>
        public ResourceAssets GetByOwnership(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Ownership));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public ResourceAssets GetByRemark(String value)
        {
            var model = new ResourceAssets { Remark = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public ResourceAssets GetByRemark(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Remark));
        }

        #endregion

                
        #region STime

        /// <summary>
        /// 根据属性STime获取数据实体
        /// </summary>
        public ResourceAssets GetBySTime(DateTime value)
        {
            var model = new ResourceAssets { STime = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.STime));
        }

        /// <summary>
        /// 根据属性STime获取数据实体
        /// </summary>
        public ResourceAssets GetBySTime(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.STime));
        }

        #endregion

                
        #region ETime

        /// <summary>
        /// 根据属性ETime获取数据实体
        /// </summary>
        public ResourceAssets GetByETime(DateTime value)
        {
            var model = new ResourceAssets { ETime = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.ETime));
        }

        /// <summary>
        /// 根据属性ETime获取数据实体
        /// </summary>
        public ResourceAssets GetByETime(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.ETime));
        }

        #endregion

                
        #region Contractor

        /// <summary>
        /// 根据属性Contractor获取数据实体
        /// </summary>
        public ResourceAssets GetByContractor(String value)
        {
            var model = new ResourceAssets { Contractor = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Contractor));
        }

        /// <summary>
        /// 根据属性Contractor获取数据实体
        /// </summary>
        public ResourceAssets GetByContractor(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Contractor));
        }

        #endregion

                
        #region YearMoney

        /// <summary>
        /// 根据属性YearMoney获取数据实体
        /// </summary>
        public ResourceAssets GetByYearMoney(String value)
        {
            var model = new ResourceAssets { YearMoney = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.YearMoney));
        }

        /// <summary>
        /// 根据属性YearMoney获取数据实体
        /// </summary>
        public ResourceAssets GetByYearMoney(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.YearMoney));
        }

        #endregion

                
        #region Payment

        /// <summary>
        /// 根据属性Payment获取数据实体
        /// </summary>
        public ResourceAssets GetByPayment(String value)
        {
            var model = new ResourceAssets { Payment = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Payment));
        }

        /// <summary>
        /// 根据属性Payment获取数据实体
        /// </summary>
        public ResourceAssets GetByPayment(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Payment));
        }

        #endregion

                
        #region Contract

        /// <summary>
        /// 根据属性Contract获取数据实体
        /// </summary>
        public ResourceAssets GetByContract(Int32 value)
        {
            var model = new ResourceAssets { Contract = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Contract));
        }

        /// <summary>
        /// 根据属性Contract获取数据实体
        /// </summary>
        public ResourceAssets GetByContract(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Contract));
        }

        #endregion

                
        #region InTime

        /// <summary>
        /// 根据属性InTime获取数据实体
        /// </summary>
        public ResourceAssets GetByInTime(DateTime value)
        {
            var model = new ResourceAssets { InTime = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.InTime));
        }

        /// <summary>
        /// 根据属性InTime获取数据实体
        /// </summary>
        public ResourceAssets GetByInTime(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.InTime));
        }

        #endregion

                
        #region Partner

        /// <summary>
        /// 根据属性Partner获取数据实体
        /// </summary>
        public ResourceAssets GetByPartner(String value)
        {
            var model = new ResourceAssets { Partner = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Partner));
        }

        /// <summary>
        /// 根据属性Partner获取数据实体
        /// </summary>
        public ResourceAssets GetByPartner(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Partner));
        }

        #endregion

                
        #region AnnualIncome

        /// <summary>
        /// 根据属性AnnualIncome获取数据实体
        /// </summary>
        public ResourceAssets GetByAnnualIncome(String value)
        {
            var model = new ResourceAssets { AnnualIncome = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.AnnualIncome));
        }

        /// <summary>
        /// 根据属性AnnualIncome获取数据实体
        /// </summary>
        public ResourceAssets GetByAnnualIncome(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.AnnualIncome));
        }

        #endregion

                
        #region Payments

        /// <summary>
        /// 根据属性Payments获取数据实体
        /// </summary>
        public ResourceAssets GetByPayments(String value)
        {
            var model = new ResourceAssets { Payments = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Payments));
        }

        /// <summary>
        /// 根据属性Payments获取数据实体
        /// </summary>
        public ResourceAssets GetByPayments(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Payments));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public ResourceAssets GetByUserId(Int32 value)
        {
            var model = new ResourceAssets { UserId = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public ResourceAssets GetByUserId(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public ResourceAssets GetByState(Int32 value)
        {
            var model = new ResourceAssets { State = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public ResourceAssets GetByState(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.State));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public ResourceAssets GetByCountry(Int32 value)
        {
            var model = new ResourceAssets { Country = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public ResourceAssets GetByCountry(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public ResourceAssets GetByVillage(Int32 value)
        {
            var model = new ResourceAssets { Village = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public ResourceAssets GetByVillage(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public ResourceAssets GetByGroups(Int32 value)
        {
            var model = new ResourceAssets { Groups = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public ResourceAssets GetByGroups(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.Groups));
        }

        #endregion

                
        #region UName

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public ResourceAssets GetByUName(String value)
        {
            var model = new ResourceAssets { UName = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.UName));
        }

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public ResourceAssets GetByUName(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.UName));
        }

        #endregion

                
        #region HetongNum

        /// <summary>
        /// 根据属性HetongNum获取数据实体
        /// </summary>
        public ResourceAssets GetByHetongNum(String value)
        {
            var model = new ResourceAssets { HetongNum = value };
            return GetByWhere(model.GetFilterString(ResourceAssetsField.HetongNum));
        }

        /// <summary>
        /// 根据属性HetongNum获取数据实体
        /// </summary>
        public ResourceAssets GetByHetongNum(ResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ResourceAssetsField.HetongNum));
        }

        #endregion

        #endregion
    }
}