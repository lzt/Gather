﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class ClassResourceAssetsDal : ExBaseDal<ClassResourceAssets>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ClassResourceAssetsDal(): this(Initialization.GetXmlConfig(typeof(ClassResourceAssets)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ClassResourceAssetsDal CreateDal()
        {
			return new ClassResourceAssetsDal(Initialization.GetXmlConfig(typeof(ClassResourceAssets)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ClassResourceAssetsDal()
        {
           ModelName= typeof(ClassResourceAssets).Name;
           var item = new ClassResourceAssets();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ClassResourceAssetsDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ClassResourceAssets";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region ClassName

        /// <summary>
        /// 根据属性ClassName获取数据实体
        /// </summary>
        public ClassResourceAssets GetByClassName(String value)
        {
            var model = new ClassResourceAssets { ClassName = value };
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.ClassName));
        }

        /// <summary>
        /// 根据属性ClassName获取数据实体
        /// </summary>
        public ClassResourceAssets GetByClassName(ClassResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.ClassName));
        }

        #endregion

                
        #region ClassParentId

        /// <summary>
        /// 根据属性ClassParentId获取数据实体
        /// </summary>
        public ClassResourceAssets GetByClassParentId(Int32 value)
        {
            var model = new ClassResourceAssets { ClassParentId = value };
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.ClassParentId));
        }

        /// <summary>
        /// 根据属性ClassParentId获取数据实体
        /// </summary>
        public ClassResourceAssets GetByClassParentId(ClassResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.ClassParentId));
        }

        #endregion

                
        #region ClassLevel

        /// <summary>
        /// 根据属性ClassLevel获取数据实体
        /// </summary>
        public ClassResourceAssets GetByClassLevel(Int32 value)
        {
            var model = new ClassResourceAssets { ClassLevel = value };
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.ClassLevel));
        }

        /// <summary>
        /// 根据属性ClassLevel获取数据实体
        /// </summary>
        public ClassResourceAssets GetByClassLevel(ClassResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.ClassLevel));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public ClassResourceAssets GetByState(Int32 value)
        {
            var model = new ClassResourceAssets { State = value };
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public ClassResourceAssets GetByState(ClassResourceAssets model)
        {
            return GetByWhere(model.GetFilterString(ClassResourceAssetsField.State));
        }

        #endregion

        #endregion
    }
}