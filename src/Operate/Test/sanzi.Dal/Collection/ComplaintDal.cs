﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class ComplaintDal : ExBaseDal<Complaint>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ComplaintDal(): this(Initialization.GetXmlConfig(typeof(Complaint)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ComplaintDal CreateDal()
        {
			return new ComplaintDal(Initialization.GetXmlConfig(typeof(Complaint)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ComplaintDal()
        {
           ModelName= typeof(Complaint).Name;
           var item = new Complaint();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ComplaintDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Complaint";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public Complaint GetByTitle(String value)
        {
            var model = new Complaint { Title = value };
            return GetByWhere(model.GetFilterString(ComplaintField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public Complaint GetByTitle(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.Title));
        }

        #endregion

                
        #region Contents

        /// <summary>
        /// 根据属性Contents获取数据实体
        /// </summary>
        public Complaint GetByContents(String value)
        {
            var model = new Complaint { Contents = value };
            return GetByWhere(model.GetFilterString(ComplaintField.Contents));
        }

        /// <summary>
        /// 根据属性Contents获取数据实体
        /// </summary>
        public Complaint GetByContents(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.Contents));
        }

        #endregion

                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Complaint GetByName(String value)
        {
            var model = new Complaint { Name = value };
            return GetByWhere(model.GetFilterString(ComplaintField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Complaint GetByName(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.Name));
        }

        #endregion

                
        #region Tel

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public Complaint GetByTel(String value)
        {
            var model = new Complaint { Tel = value };
            return GetByWhere(model.GetFilterString(ComplaintField.Tel));
        }

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public Complaint GetByTel(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.Tel));
        }

        #endregion

                
        #region ATime

        /// <summary>
        /// 根据属性ATime获取数据实体
        /// </summary>
        public Complaint GetByATime(DateTime value)
        {
            var model = new Complaint { ATime = value };
            return GetByWhere(model.GetFilterString(ComplaintField.ATime));
        }

        /// <summary>
        /// 根据属性ATime获取数据实体
        /// </summary>
        public Complaint GetByATime(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.ATime));
        }

        #endregion

                
        #region Results

        /// <summary>
        /// 根据属性Results获取数据实体
        /// </summary>
        public Complaint GetByResults(String value)
        {
            var model = new Complaint { Results = value };
            return GetByWhere(model.GetFilterString(ComplaintField.Results));
        }

        /// <summary>
        /// 根据属性Results获取数据实体
        /// </summary>
        public Complaint GetByResults(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.Results));
        }

        #endregion

                
        #region PersonId

        /// <summary>
        /// 根据属性PersonId获取数据实体
        /// </summary>
        public Complaint GetByPersonId(Int32 value)
        {
            var model = new Complaint { PersonId = value };
            return GetByWhere(model.GetFilterString(ComplaintField.PersonId));
        }

        /// <summary>
        /// 根据属性PersonId获取数据实体
        /// </summary>
        public Complaint GetByPersonId(Complaint model)
        {
            return GetByWhere(model.GetFilterString(ComplaintField.PersonId));
        }

        #endregion

        #endregion
    }
}