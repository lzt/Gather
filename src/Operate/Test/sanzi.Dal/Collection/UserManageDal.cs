﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class UserManageDal : ExBaseDal<UserManage>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserManageDal(): this(Initialization.GetXmlConfig(typeof(UserManage)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserManageDal CreateDal()
        {
			return new UserManageDal(Initialization.GetXmlConfig(typeof(UserManage)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserManageDal()
        {
           ModelName= typeof(UserManage).Name;
           var item = new UserManage();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserManageDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserManage";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Username

        /// <summary>
        /// 根据属性Username获取数据实体
        /// </summary>
        public UserManage GetByUsername(String value)
        {
            var model = new UserManage { Username = value };
            return GetByWhere(model.GetFilterString(UserManageField.Username));
        }

        /// <summary>
        /// 根据属性Username获取数据实体
        /// </summary>
        public UserManage GetByUsername(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Username));
        }

        #endregion

                
        #region Pwd

        /// <summary>
        /// 根据属性Pwd获取数据实体
        /// </summary>
        public UserManage GetByPwd(String value)
        {
            var model = new UserManage { Pwd = value };
            return GetByWhere(model.GetFilterString(UserManageField.Pwd));
        }

        /// <summary>
        /// 根据属性Pwd获取数据实体
        /// </summary>
        public UserManage GetByPwd(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Pwd));
        }

        #endregion

                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public UserManage GetByName(String value)
        {
            var model = new UserManage { Name = value };
            return GetByWhere(model.GetFilterString(UserManageField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public UserManage GetByName(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Name));
        }

        #endregion

                
        #region Tel

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public UserManage GetByTel(String value)
        {
            var model = new UserManage { Tel = value };
            return GetByWhere(model.GetFilterString(UserManageField.Tel));
        }

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public UserManage GetByTel(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Tel));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserManage GetByAddress(String value)
        {
            var model = new UserManage { Address = value };
            return GetByWhere(model.GetFilterString(UserManageField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserManage GetByAddress(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Address));
        }

        #endregion

                
        #region Role

        /// <summary>
        /// 根据属性Role获取数据实体
        /// </summary>
        public UserManage GetByRole(Int32 value)
        {
            var model = new UserManage { Role = value };
            return GetByWhere(model.GetFilterString(UserManageField.Role));
        }

        /// <summary>
        /// 根据属性Role获取数据实体
        /// </summary>
        public UserManage GetByRole(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Role));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public UserManage GetByCountry(Int32 value)
        {
            var model = new UserManage { Country = value };
            return GetByWhere(model.GetFilterString(UserManageField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public UserManage GetByCountry(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public UserManage GetByVillage(Int32 value)
        {
            var model = new UserManage { Village = value };
            return GetByWhere(model.GetFilterString(UserManageField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public UserManage GetByVillage(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public UserManage GetByGroups(Int32 value)
        {
            var model = new UserManage { Groups = value };
            return GetByWhere(model.GetFilterString(UserManageField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public UserManage GetByGroups(UserManage model)
        {
            return GetByWhere(model.GetFilterString(UserManageField.Groups));
        }

        #endregion

        #endregion
    }
}