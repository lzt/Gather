﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class LogTableDal : ExBaseDal<LogTable>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public LogTableDal(): this(Initialization.GetXmlConfig(typeof(LogTable)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static LogTableDal CreateDal()
        {
			return new LogTableDal(Initialization.GetXmlConfig(typeof(LogTable)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static LogTableDal()
        {
           ModelName= typeof(LogTable).Name;
           var item = new LogTable();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public LogTableDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "LogTable";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public LogTable GetByUserId(Int32 value)
        {
            var model = new LogTable { UserId = value };
            return GetByWhere(model.GetFilterString(LogTableField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public LogTable GetByUserId(LogTable model)
        {
            return GetByWhere(model.GetFilterString(LogTableField.UserId));
        }

        #endregion

                
        #region Remarks

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public LogTable GetByRemarks(String value)
        {
            var model = new LogTable { Remarks = value };
            return GetByWhere(model.GetFilterString(LogTableField.Remarks));
        }

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public LogTable GetByRemarks(LogTable model)
        {
            return GetByWhere(model.GetFilterString(LogTableField.Remarks));
        }

        #endregion

                
        #region Datetime

        /// <summary>
        /// 根据属性Datetime获取数据实体
        /// </summary>
        public LogTable GetByDatetime(DateTime value)
        {
            var model = new LogTable { Datetime = value };
            return GetByWhere(model.GetFilterString(LogTableField.Datetime));
        }

        /// <summary>
        /// 根据属性Datetime获取数据实体
        /// </summary>
        public LogTable GetByDatetime(LogTable model)
        {
            return GetByWhere(model.GetFilterString(LogTableField.Datetime));
        }

        #endregion

        #endregion
    }
}