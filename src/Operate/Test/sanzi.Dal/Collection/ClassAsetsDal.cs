﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class ClassAsetsDal : ExBaseDal<ClassAsets>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ClassAsetsDal(): this(Initialization.GetXmlConfig(typeof(ClassAsets)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ClassAsetsDal CreateDal()
        {
			return new ClassAsetsDal(Initialization.GetXmlConfig(typeof(ClassAsets)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ClassAsetsDal()
        {
           ModelName= typeof(ClassAsets).Name;
           var item = new ClassAsets();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ClassAsetsDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ClassAsets";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region ClassName

        /// <summary>
        /// 根据属性ClassName获取数据实体
        /// </summary>
        public ClassAsets GetByClassName(String value)
        {
            var model = new ClassAsets { ClassName = value };
            return GetByWhere(model.GetFilterString(ClassAsetsField.ClassName));
        }

        /// <summary>
        /// 根据属性ClassName获取数据实体
        /// </summary>
        public ClassAsets GetByClassName(ClassAsets model)
        {
            return GetByWhere(model.GetFilterString(ClassAsetsField.ClassName));
        }

        #endregion

                
        #region ClassParentId

        /// <summary>
        /// 根据属性ClassParentId获取数据实体
        /// </summary>
        public ClassAsets GetByClassParentId(Int32 value)
        {
            var model = new ClassAsets { ClassParentId = value };
            return GetByWhere(model.GetFilterString(ClassAsetsField.ClassParentId));
        }

        /// <summary>
        /// 根据属性ClassParentId获取数据实体
        /// </summary>
        public ClassAsets GetByClassParentId(ClassAsets model)
        {
            return GetByWhere(model.GetFilterString(ClassAsetsField.ClassParentId));
        }

        #endregion

                
        #region ClassLevel

        /// <summary>
        /// 根据属性ClassLevel获取数据实体
        /// </summary>
        public ClassAsets GetByClassLevel(Int32 value)
        {
            var model = new ClassAsets { ClassLevel = value };
            return GetByWhere(model.GetFilterString(ClassAsetsField.ClassLevel));
        }

        /// <summary>
        /// 根据属性ClassLevel获取数据实体
        /// </summary>
        public ClassAsets GetByClassLevel(ClassAsets model)
        {
            return GetByWhere(model.GetFilterString(ClassAsetsField.ClassLevel));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public ClassAsets GetByState(Int32 value)
        {
            var model = new ClassAsets { State = value };
            return GetByWhere(model.GetFilterString(ClassAsetsField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public ClassAsets GetByState(ClassAsets model)
        {
            return GetByWhere(model.GetFilterString(ClassAsetsField.State));
        }

        #endregion

        #endregion
    }
}