﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class AssetsDal : ExBaseDal<Assets>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AssetsDal(): this(Initialization.GetXmlConfig(typeof(Assets)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AssetsDal CreateDal()
        {
			return new AssetsDal(Initialization.GetXmlConfig(typeof(Assets)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AssetsDal()
        {
           ModelName= typeof(Assets).Name;
           var item = new Assets();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AssetsDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Assets";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Assets GetByName(String value)
        {
            var model = new Assets { Name = value };
            return GetByWhere(model.GetFilterString(AssetsField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Assets GetByName(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Name));
        }

        #endregion

                
        #region Business

        /// <summary>
        /// 根据属性Business获取数据实体
        /// </summary>
        public Assets GetByBusiness(Int32 value)
        {
            var model = new Assets { Business = value };
            return GetByWhere(model.GetFilterString(AssetsField.Business));
        }

        /// <summary>
        /// 根据属性Business获取数据实体
        /// </summary>
        public Assets GetByBusiness(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Business));
        }

        #endregion

                
        #region InTime

        /// <summary>
        /// 根据属性InTime获取数据实体
        /// </summary>
        public Assets GetByInTime(DateTime value)
        {
            var model = new Assets { InTime = value };
            return GetByWhere(model.GetFilterString(AssetsField.InTime));
        }

        /// <summary>
        /// 根据属性InTime获取数据实体
        /// </summary>
        public Assets GetByInTime(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.InTime));
        }

        #endregion

                
        #region Partner

        /// <summary>
        /// 根据属性Partner获取数据实体
        /// </summary>
        public Assets GetByPartner(String value)
        {
            var model = new Assets { Partner = value };
            return GetByWhere(model.GetFilterString(AssetsField.Partner));
        }

        /// <summary>
        /// 根据属性Partner获取数据实体
        /// </summary>
        public Assets GetByPartner(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Partner));
        }

        #endregion

                
        #region AnnualIncome

        /// <summary>
        /// 根据属性AnnualIncome获取数据实体
        /// </summary>
        public Assets GetByAnnualIncome(String value)
        {
            var model = new Assets { AnnualIncome = value };
            return GetByWhere(model.GetFilterString(AssetsField.AnnualIncome));
        }

        /// <summary>
        /// 根据属性AnnualIncome获取数据实体
        /// </summary>
        public Assets GetByAnnualIncome(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.AnnualIncome));
        }

        #endregion

                
        #region Contractor

        /// <summary>
        /// 根据属性Contractor获取数据实体
        /// </summary>
        public Assets GetByContractor(String value)
        {
            var model = new Assets { Contractor = value };
            return GetByWhere(model.GetFilterString(AssetsField.Contractor));
        }

        /// <summary>
        /// 根据属性Contractor获取数据实体
        /// </summary>
        public Assets GetByContractor(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Contractor));
        }

        #endregion

                
        #region STime

        /// <summary>
        /// 根据属性STime获取数据实体
        /// </summary>
        public Assets GetBySTime(DateTime value)
        {
            var model = new Assets { STime = value };
            return GetByWhere(model.GetFilterString(AssetsField.STime));
        }

        /// <summary>
        /// 根据属性STime获取数据实体
        /// </summary>
        public Assets GetBySTime(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.STime));
        }

        #endregion

                
        #region ETime

        /// <summary>
        /// 根据属性ETime获取数据实体
        /// </summary>
        public Assets GetByETime(DateTime value)
        {
            var model = new Assets { ETime = value };
            return GetByWhere(model.GetFilterString(AssetsField.ETime));
        }

        /// <summary>
        /// 根据属性ETime获取数据实体
        /// </summary>
        public Assets GetByETime(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.ETime));
        }

        #endregion

                
        #region YearMoney

        /// <summary>
        /// 根据属性YearMoney获取数据实体
        /// </summary>
        public Assets GetByYearMoney(String value)
        {
            var model = new Assets { YearMoney = value };
            return GetByWhere(model.GetFilterString(AssetsField.YearMoney));
        }

        /// <summary>
        /// 根据属性YearMoney获取数据实体
        /// </summary>
        public Assets GetByYearMoney(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.YearMoney));
        }

        #endregion

                
        #region Contract

        /// <summary>
        /// 根据属性Contract获取数据实体
        /// </summary>
        public Assets GetByContract(Int32 value)
        {
            var model = new Assets { Contract = value };
            return GetByWhere(model.GetFilterString(AssetsField.Contract));
        }

        /// <summary>
        /// 根据属性Contract获取数据实体
        /// </summary>
        public Assets GetByContract(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Contract));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Assets GetByType(Int32 value)
        {
            var model = new Assets { Type = value };
            return GetByWhere(model.GetFilterString(AssetsField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Assets GetByType(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Type));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Assets GetByUserId(Int32 value)
        {
            var model = new Assets { UserId = value };
            return GetByWhere(model.GetFilterString(AssetsField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Assets GetByUserId(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Assets GetByState(Int32 value)
        {
            var model = new Assets { State = value };
            return GetByWhere(model.GetFilterString(AssetsField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Assets GetByState(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.State));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Assets GetByCountry(Int32 value)
        {
            var model = new Assets { Country = value };
            return GetByWhere(model.GetFilterString(AssetsField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Assets GetByCountry(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Assets GetByVillage(Int32 value)
        {
            var model = new Assets { Village = value };
            return GetByWhere(model.GetFilterString(AssetsField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Assets GetByVillage(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Assets GetByGroups(Int32 value)
        {
            var model = new Assets { Groups = value };
            return GetByWhere(model.GetFilterString(AssetsField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Assets GetByGroups(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.Groups));
        }

        #endregion

                
        #region UName

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Assets GetByUName(String value)
        {
            var model = new Assets { UName = value };
            return GetByWhere(model.GetFilterString(AssetsField.UName));
        }

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Assets GetByUName(Assets model)
        {
            return GetByWhere(model.GetFilterString(AssetsField.UName));
        }

        #endregion

        #endregion
    }
}