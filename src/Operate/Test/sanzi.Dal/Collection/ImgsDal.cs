﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class ImgsDal : ExBaseDal<Imgs>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ImgsDal(): this(Initialization.GetXmlConfig(typeof(Imgs)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ImgsDal CreateDal()
        {
			return new ImgsDal(Initialization.GetXmlConfig(typeof(Imgs)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ImgsDal()
        {
           ModelName= typeof(Imgs).Name;
           var item = new Imgs();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ImgsDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Imgs";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Img

        /// <summary>
        /// 根据属性Img获取数据实体
        /// </summary>
        public Imgs GetByImg(String value)
        {
            var model = new Imgs { Img = value };
            return GetByWhere(model.GetFilterString(ImgsField.Img));
        }

        /// <summary>
        /// 根据属性Img获取数据实体
        /// </summary>
        public Imgs GetByImg(Imgs model)
        {
            return GetByWhere(model.GetFilterString(ImgsField.Img));
        }

        #endregion

                
        #region PicId

        /// <summary>
        /// 根据属性PicId获取数据实体
        /// </summary>
        public Imgs GetByPicId(Int32 value)
        {
            var model = new Imgs { PicId = value };
            return GetByWhere(model.GetFilterString(ImgsField.PicId));
        }

        /// <summary>
        /// 根据属性PicId获取数据实体
        /// </summary>
        public Imgs GetByPicId(Imgs model)
        {
            return GetByWhere(model.GetFilterString(ImgsField.PicId));
        }

        #endregion

                
        #region TypeId

        /// <summary>
        /// 根据属性TypeId获取数据实体
        /// </summary>
        public Imgs GetByTypeId(Int32 value)
        {
            var model = new Imgs { TypeId = value };
            return GetByWhere(model.GetFilterString(ImgsField.TypeId));
        }

        /// <summary>
        /// 根据属性TypeId获取数据实体
        /// </summary>
        public Imgs GetByTypeId(Imgs model)
        {
            return GetByWhere(model.GetFilterString(ImgsField.TypeId));
        }

        #endregion

        #endregion
    }
}