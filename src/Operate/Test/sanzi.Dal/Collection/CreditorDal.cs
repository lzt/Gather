﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class CreditorDal : ExBaseDal<Creditor>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CreditorDal(): this(Initialization.GetXmlConfig(typeof(Creditor)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CreditorDal CreateDal()
        {
			return new CreditorDal(Initialization.GetXmlConfig(typeof(Creditor)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CreditorDal()
        {
           ModelName= typeof(Creditor).Name;
           var item = new Creditor();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CreditorDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Creditor";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Creditor GetByName(String value)
        {
            var model = new Creditor { Name = value };
            return GetByWhere(model.GetFilterString(CreditorField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Creditor GetByName(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.Name));
        }

        #endregion

                
        #region TimeDivision

        /// <summary>
        /// 根据属性TimeDivision获取数据实体
        /// </summary>
        public Creditor GetByTimeDivision(Int32 value)
        {
            var model = new Creditor { TimeDivision = value };
            return GetByWhere(model.GetFilterString(CreditorField.TimeDivision));
        }

        /// <summary>
        /// 根据属性TimeDivision获取数据实体
        /// </summary>
        public Creditor GetByTimeDivision(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.TimeDivision));
        }

        #endregion

                
        #region Category

        /// <summary>
        /// 根据属性Category获取数据实体
        /// </summary>
        public Creditor GetByCategory(Int32 value)
        {
            var model = new Creditor { Category = value };
            return GetByWhere(model.GetFilterString(CreditorField.Category));
        }

        /// <summary>
        /// 根据属性Category获取数据实体
        /// </summary>
        public Creditor GetByCategory(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.Category));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Creditor GetByUserId(Int32 value)
        {
            var model = new Creditor { UserId = value };
            return GetByWhere(model.GetFilterString(CreditorField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Creditor GetByUserId(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Creditor GetByState(Int32 value)
        {
            var model = new Creditor { State = value };
            return GetByWhere(model.GetFilterString(CreditorField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Creditor GetByState(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.State));
        }

        #endregion

                
        #region Amount

        /// <summary>
        /// 根据属性Amount获取数据实体
        /// </summary>
        public Creditor GetByAmount(Decimal value)
        {
            var model = new Creditor { Amount = value };
            return GetByWhere(model.GetFilterString(CreditorField.Amount));
        }

        /// <summary>
        /// 根据属性Amount获取数据实体
        /// </summary>
        public Creditor GetByAmount(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.Amount));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Creditor GetByCountry(Int32 value)
        {
            var model = new Creditor { Country = value };
            return GetByWhere(model.GetFilterString(CreditorField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Creditor GetByCountry(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Creditor GetByVillage(Int32 value)
        {
            var model = new Creditor { Village = value };
            return GetByWhere(model.GetFilterString(CreditorField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Creditor GetByVillage(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Creditor GetByGroups(Int32 value)
        {
            var model = new Creditor { Groups = value };
            return GetByWhere(model.GetFilterString(CreditorField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Creditor GetByGroups(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.Groups));
        }

        #endregion

                
        #region UName

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Creditor GetByUName(String value)
        {
            var model = new Creditor { UName = value };
            return GetByWhere(model.GetFilterString(CreditorField.UName));
        }

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Creditor GetByUName(Creditor model)
        {
            return GetByWhere(model.GetFilterString(CreditorField.UName));
        }

        #endregion

        #endregion
    }
}