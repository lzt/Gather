﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class AnnouncementDal : ExBaseDal<Announcement>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AnnouncementDal(): this(Initialization.GetXmlConfig(typeof(Announcement)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AnnouncementDal CreateDal()
        {
			return new AnnouncementDal(Initialization.GetXmlConfig(typeof(Announcement)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AnnouncementDal()
        {
           ModelName= typeof(Announcement).Name;
           var item = new Announcement();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AnnouncementDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Announcement";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Contents

        /// <summary>
        /// 根据属性Contents获取数据实体
        /// </summary>
        public Announcement GetByContents(String value)
        {
            var model = new Announcement { Contents = value };
            return GetByWhere(model.GetFilterString(AnnouncementField.Contents));
        }

        /// <summary>
        /// 根据属性Contents获取数据实体
        /// </summary>
        public Announcement GetByContents(Announcement model)
        {
            return GetByWhere(model.GetFilterString(AnnouncementField.Contents));
        }

        #endregion

                
        #region ATime

        /// <summary>
        /// 根据属性ATime获取数据实体
        /// </summary>
        public Announcement GetByATime(DateTime value)
        {
            var model = new Announcement { ATime = value };
            return GetByWhere(model.GetFilterString(AnnouncementField.ATime));
        }

        /// <summary>
        /// 根据属性ATime获取数据实体
        /// </summary>
        public Announcement GetByATime(Announcement model)
        {
            return GetByWhere(model.GetFilterString(AnnouncementField.ATime));
        }

        #endregion

        #endregion
    }
}