﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class LeadershipDal : ExBaseDal<Leadership>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public LeadershipDal(): this(Initialization.GetXmlConfig(typeof(Leadership)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static LeadershipDal CreateDal()
        {
			return new LeadershipDal(Initialization.GetXmlConfig(typeof(Leadership)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static LeadershipDal()
        {
           ModelName= typeof(Leadership).Name;
           var item = new Leadership();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public LeadershipDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Leadership";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Leader

        /// <summary>
        /// 根据属性Leader获取数据实体
        /// </summary>
        public Leadership GetByLeader(String value)
        {
            var model = new Leadership { Leader = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Leader));
        }

        /// <summary>
        /// 根据属性Leader获取数据实体
        /// </summary>
        public Leadership GetByLeader(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Leader));
        }

        #endregion

                
        #region Director

        /// <summary>
        /// 根据属性Director获取数据实体
        /// </summary>
        public Leadership GetByDirector(String value)
        {
            var model = new Leadership { Director = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Director));
        }

        /// <summary>
        /// 根据属性Director获取数据实体
        /// </summary>
        public Leadership GetByDirector(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Director));
        }

        #endregion

                
        #region CncaDirector

        /// <summary>
        /// 根据属性CncaDirector获取数据实体
        /// </summary>
        public Leadership GetByCncaDirector(String value)
        {
            var model = new Leadership { CncaDirector = value };
            return GetByWhere(model.GetFilterString(LeadershipField.CncaDirector));
        }

        /// <summary>
        /// 根据属性CncaDirector获取数据实体
        /// </summary>
        public Leadership GetByCncaDirector(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.CncaDirector));
        }

        #endregion

                
        #region Documents

        /// <summary>
        /// 根据属性Documents获取数据实体
        /// </summary>
        public Leadership GetByDocuments(String value)
        {
            var model = new Leadership { Documents = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Documents));
        }

        /// <summary>
        /// 根据属性Documents获取数据实体
        /// </summary>
        public Leadership GetByDocuments(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Documents));
        }

        #endregion

                
        #region Commissioners

        /// <summary>
        /// 根据属性Commissioners获取数据实体
        /// </summary>
        public Leadership GetByCommissioners(String value)
        {
            var model = new Leadership { Commissioners = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Commissioners));
        }

        /// <summary>
        /// 根据属性Commissioners获取数据实体
        /// </summary>
        public Leadership GetByCommissioners(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Commissioners));
        }

        #endregion

                
        #region CncaCommissioners

        /// <summary>
        /// 根据属性CncaCommissioners获取数据实体
        /// </summary>
        public Leadership GetByCncaCommissioners(String value)
        {
            var model = new Leadership { CncaCommissioners = value };
            return GetByWhere(model.GetFilterString(LeadershipField.CncaCommissioners));
        }

        /// <summary>
        /// 根据属性CncaCommissioners获取数据实体
        /// </summary>
        public Leadership GetByCncaCommissioners(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.CncaCommissioners));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Leadership GetByCountry(Int32 value)
        {
            var model = new Leadership { Country = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Leadership GetByCountry(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Leadership GetByVillage(Int32 value)
        {
            var model = new Leadership { Village = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Leadership GetByVillage(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Leadership GetByGroups(Int32 value)
        {
            var model = new Leadership { Groups = value };
            return GetByWhere(model.GetFilterString(LeadershipField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Leadership GetByGroups(Leadership model)
        {
            return GetByWhere(model.GetFilterString(LeadershipField.Groups));
        }

        #endregion

        #endregion
    }
}