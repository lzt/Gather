﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class PersonnelDal : ExBaseDal<Personnel>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public PersonnelDal(): this(Initialization.GetXmlConfig(typeof(Personnel)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static PersonnelDal CreateDal()
        {
			return new PersonnelDal(Initialization.GetXmlConfig(typeof(Personnel)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static PersonnelDal()
        {
           ModelName= typeof(Personnel).Name;
           var item = new Personnel();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public PersonnelDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Personnel";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Person

        /// <summary>
        /// 根据属性Person获取数据实体
        /// </summary>
        public Personnel GetByPerson(String value)
        {
            var model = new Personnel { Person = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Person));
        }

        /// <summary>
        /// 根据属性Person获取数据实体
        /// </summary>
        public Personnel GetByPerson(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Person));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public Personnel GetBySex(Int32 value)
        {
            var model = new Personnel { Sex = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public Personnel GetBySex(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Sex));
        }

        #endregion

                
        #region Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public Personnel GetByBirthday(String value)
        {
            var model = new Personnel { Birthday = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Birthday));
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public Personnel GetByBirthday(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Birthday));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public Personnel GetByPhone(String value)
        {
            var model = new Personnel { Phone = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public Personnel GetByPhone(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Phone));
        }

        #endregion

                
        #region FamilyId

        /// <summary>
        /// 根据属性FamilyId获取数据实体
        /// </summary>
        public Personnel GetByFamilyId(Int32 value)
        {
            var model = new Personnel { FamilyId = value };
            return GetByWhere(model.GetFilterString(PersonnelField.FamilyId));
        }

        /// <summary>
        /// 根据属性FamilyId获取数据实体
        /// </summary>
        public Personnel GetByFamilyId(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.FamilyId));
        }

        #endregion

                
        #region Relationship

        /// <summary>
        /// 根据属性Relationship获取数据实体
        /// </summary>
        public Personnel GetByRelationship(String value)
        {
            var model = new Personnel { Relationship = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Relationship));
        }

        /// <summary>
        /// 根据属性Relationship获取数据实体
        /// </summary>
        public Personnel GetByRelationship(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Relationship));
        }

        #endregion

                
        #region ORelationship

        /// <summary>
        /// 根据属性ORelationship获取数据实体
        /// </summary>
        public Personnel GetByORelationship(Int32 value)
        {
            var model = new Personnel { ORelationship = value };
            return GetByWhere(model.GetFilterString(PersonnelField.ORelationship));
        }

        /// <summary>
        /// 根据属性ORelationship获取数据实体
        /// </summary>
        public Personnel GetByORelationship(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.ORelationship));
        }

        #endregion

                
        #region Conditions

        /// <summary>
        /// 根据属性Conditions获取数据实体
        /// </summary>
        public Personnel GetByConditions(String value)
        {
            var model = new Personnel { Conditions = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Conditions));
        }

        /// <summary>
        /// 根据属性Conditions获取数据实体
        /// </summary>
        public Personnel GetByConditions(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Conditions));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Personnel GetByCountry(Int32 value)
        {
            var model = new Personnel { Country = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Personnel GetByCountry(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Personnel GetByVillage(Int32 value)
        {
            var model = new Personnel { Village = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Personnel GetByVillage(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Personnel GetByGroups(Int32 value)
        {
            var model = new Personnel { Groups = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Personnel GetByGroups(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Groups));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Personnel GetByUserId(Int32 value)
        {
            var model = new Personnel { UserId = value };
            return GetByWhere(model.GetFilterString(PersonnelField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Personnel GetByUserId(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Personnel GetByState(Int32 value)
        {
            var model = new Personnel { State = value };
            return GetByWhere(model.GetFilterString(PersonnelField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Personnel GetByState(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.State));
        }

        #endregion

                
        #region Tel

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public Personnel GetByTel(String value)
        {
            var model = new Personnel { Tel = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Tel));
        }

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public Personnel GetByTel(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Tel));
        }

        #endregion

                
        #region Chengbao

        /// <summary>
        /// 根据属性Chengbao获取数据实体
        /// </summary>
        public Personnel GetByChengbao(String value)
        {
            var model = new Personnel { Chengbao = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Chengbao));
        }

        /// <summary>
        /// 根据属性Chengbao获取数据实体
        /// </summary>
        public Personnel GetByChengbao(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Chengbao));
        }

        #endregion

                
        #region Zhaijidi

        /// <summary>
        /// 根据属性Zhaijidi获取数据实体
        /// </summary>
        public Personnel GetByZhaijidi(String value)
        {
            var model = new Personnel { Zhaijidi = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Zhaijidi));
        }

        /// <summary>
        /// 根据属性Zhaijidi获取数据实体
        /// </summary>
        public Personnel GetByZhaijidi(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Zhaijidi));
        }

        #endregion

                
        #region ZhaiMianji

        /// <summary>
        /// 根据属性ZhaiMianji获取数据实体
        /// </summary>
        public Personnel GetByZhaiMianji(String value)
        {
            var model = new Personnel { ZhaiMianji = value };
            return GetByWhere(model.GetFilterString(PersonnelField.ZhaiMianji));
        }

        /// <summary>
        /// 根据属性ZhaiMianji获取数据实体
        /// </summary>
        public Personnel GetByZhaiMianji(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.ZhaiMianji));
        }

        #endregion

                
        #region HouseStruct

        /// <summary>
        /// 根据属性HouseStruct获取数据实体
        /// </summary>
        public Personnel GetByHouseStruct(Int32 value)
        {
            var model = new Personnel { HouseStruct = value };
            return GetByWhere(model.GetFilterString(PersonnelField.HouseStruct));
        }

        /// <summary>
        /// 根据属性HouseStruct获取数据实体
        /// </summary>
        public Personnel GetByHouseStruct(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.HouseStruct));
        }

        #endregion

                
        #region Jianzhu

        /// <summary>
        /// 根据属性Jianzhu获取数据实体
        /// </summary>
        public Personnel GetByJianzhu(String value)
        {
            var model = new Personnel { Jianzhu = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Jianzhu));
        }

        /// <summary>
        /// 根据属性Jianzhu获取数据实体
        /// </summary>
        public Personnel GetByJianzhu(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Jianzhu));
        }

        #endregion

                
        #region Youdai

        /// <summary>
        /// 根据属性Youdai获取数据实体
        /// </summary>
        public Personnel GetByYoudai(Int32 value)
        {
            var model = new Personnel { Youdai = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Youdai));
        }

        /// <summary>
        /// 根据属性Youdai获取数据实体
        /// </summary>
        public Personnel GetByYoudai(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Youdai));
        }

        #endregion

                
        #region Remarks

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public Personnel GetByRemarks(String value)
        {
            var model = new Personnel { Remarks = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Remarks));
        }

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public Personnel GetByRemarks(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Remarks));
        }

        #endregion

                
        #region Times

        /// <summary>
        /// 根据属性Times获取数据实体
        /// </summary>
        public Personnel GetByTimes(DateTime value)
        {
            var model = new Personnel { Times = value };
            return GetByWhere(model.GetFilterString(PersonnelField.Times));
        }

        /// <summary>
        /// 根据属性Times获取数据实体
        /// </summary>
        public Personnel GetByTimes(Personnel model)
        {
            return GetByWhere(model.GetFilterString(PersonnelField.Times));
        }

        #endregion

        #endregion
    }
}