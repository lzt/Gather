﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class CurrencyDal : ExBaseDal<Currency>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CurrencyDal(): this(Initialization.GetXmlConfig(typeof(Currency)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CurrencyDal CreateDal()
        {
			return new CurrencyDal(Initialization.GetXmlConfig(typeof(Currency)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CurrencyDal()
        {
           ModelName= typeof(Currency).Name;
           var item = new Currency();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CurrencyDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Currency";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region BankBookMoney

        /// <summary>
        /// 根据属性BankBookMoney获取数据实体
        /// </summary>
        public Currency GetByBankBookMoney(Decimal value)
        {
            var model = new Currency { BankBookMoney = value };
            return GetByWhere(model.GetFilterString(CurrencyField.BankBookMoney));
        }

        /// <summary>
        /// 根据属性BankBookMoney获取数据实体
        /// </summary>
        public Currency GetByBankBookMoney(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.BankBookMoney));
        }

        #endregion

                
        #region BankActualamount

        /// <summary>
        /// 根据属性BankActualamount获取数据实体
        /// </summary>
        public Currency GetByBankActualamount(Decimal value)
        {
            var model = new Currency { BankActualamount = value };
            return GetByWhere(model.GetFilterString(CurrencyField.BankActualamount));
        }

        /// <summary>
        /// 根据属性BankActualamount获取数据实体
        /// </summary>
        public Currency GetByBankActualamount(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.BankActualamount));
        }

        #endregion

                
        #region BankReason

        /// <summary>
        /// 根据属性BankReason获取数据实体
        /// </summary>
        public Currency GetByBankReason(String value)
        {
            var model = new Currency { BankReason = value };
            return GetByWhere(model.GetFilterString(CurrencyField.BankReason));
        }

        /// <summary>
        /// 根据属性BankReason获取数据实体
        /// </summary>
        public Currency GetByBankReason(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.BankReason));
        }

        #endregion

                
        #region InventoryBookMoney

        /// <summary>
        /// 根据属性InventoryBookMoney获取数据实体
        /// </summary>
        public Currency GetByInventoryBookMoney(Decimal value)
        {
            var model = new Currency { InventoryBookMoney = value };
            return GetByWhere(model.GetFilterString(CurrencyField.InventoryBookMoney));
        }

        /// <summary>
        /// 根据属性InventoryBookMoney获取数据实体
        /// </summary>
        public Currency GetByInventoryBookMoney(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.InventoryBookMoney));
        }

        #endregion

                
        #region InventoryActualamount

        /// <summary>
        /// 根据属性InventoryActualamount获取数据实体
        /// </summary>
        public Currency GetByInventoryActualamount(Decimal value)
        {
            var model = new Currency { InventoryActualamount = value };
            return GetByWhere(model.GetFilterString(CurrencyField.InventoryActualamount));
        }

        /// <summary>
        /// 根据属性InventoryActualamount获取数据实体
        /// </summary>
        public Currency GetByInventoryActualamount(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.InventoryActualamount));
        }

        #endregion

                
        #region InventoryReason

        /// <summary>
        /// 根据属性InventoryReason获取数据实体
        /// </summary>
        public Currency GetByInventoryReason(String value)
        {
            var model = new Currency { InventoryReason = value };
            return GetByWhere(model.GetFilterString(CurrencyField.InventoryReason));
        }

        /// <summary>
        /// 根据属性InventoryReason获取数据实体
        /// </summary>
        public Currency GetByInventoryReason(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.InventoryReason));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public Currency GetByRemark(String value)
        {
            var model = new Currency { Remark = value };
            return GetByWhere(model.GetFilterString(CurrencyField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public Currency GetByRemark(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.Remark));
        }

        #endregion

                
        #region BankAccount

        /// <summary>
        /// 根据属性BankAccount获取数据实体
        /// </summary>
        public Currency GetByBankAccount(String value)
        {
            var model = new Currency { BankAccount = value };
            return GetByWhere(model.GetFilterString(CurrencyField.BankAccount));
        }

        /// <summary>
        /// 根据属性BankAccount获取数据实体
        /// </summary>
        public Currency GetByBankAccount(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.BankAccount));
        }

        #endregion

                
        #region Bank

        /// <summary>
        /// 根据属性Bank获取数据实体
        /// </summary>
        public Currency GetByBank(String value)
        {
            var model = new Currency { Bank = value };
            return GetByWhere(model.GetFilterString(CurrencyField.Bank));
        }

        /// <summary>
        /// 根据属性Bank获取数据实体
        /// </summary>
        public Currency GetByBank(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.Bank));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Currency GetByState(Int32 value)
        {
            var model = new Currency { State = value };
            return GetByWhere(model.GetFilterString(CurrencyField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Currency GetByState(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.State));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Currency GetByCountry(Int32 value)
        {
            var model = new Currency { Country = value };
            return GetByWhere(model.GetFilterString(CurrencyField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Currency GetByCountry(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Currency GetByVillage(Int32 value)
        {
            var model = new Currency { Village = value };
            return GetByWhere(model.GetFilterString(CurrencyField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Currency GetByVillage(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Currency GetByGroups(Int32 value)
        {
            var model = new Currency { Groups = value };
            return GetByWhere(model.GetFilterString(CurrencyField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Currency GetByGroups(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.Groups));
        }

        #endregion

                
        #region UName

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Currency GetByUName(String value)
        {
            var model = new Currency { UName = value };
            return GetByWhere(model.GetFilterString(CurrencyField.UName));
        }

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Currency GetByUName(Currency model)
        {
            return GetByWhere(model.GetFilterString(CurrencyField.UName));
        }

        #endregion

        #endregion
    }
}