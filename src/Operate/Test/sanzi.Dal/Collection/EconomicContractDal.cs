﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class EconomicContractDal : ExBaseDal<EconomicContract>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public EconomicContractDal(): this(Initialization.GetXmlConfig(typeof(EconomicContract)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static EconomicContractDal CreateDal()
        {
			return new EconomicContractDal(Initialization.GetXmlConfig(typeof(EconomicContract)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static EconomicContractDal()
        {
           ModelName= typeof(EconomicContract).Name;
           var item = new EconomicContract();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public EconomicContractDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "EconomicContract";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Item

        /// <summary>
        /// 根据属性Item获取数据实体
        /// </summary>
        public EconomicContract GetByItem(String value)
        {
            var model = new EconomicContract { Item = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Item));
        }

        /// <summary>
        /// 根据属性Item获取数据实体
        /// </summary>
        public EconomicContract GetByItem(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Item));
        }

        #endregion

                
        #region Company

        /// <summary>
        /// 根据属性Company获取数据实体
        /// </summary>
        public EconomicContract GetByCompany(String value)
        {
            var model = new EconomicContract { Company = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Company));
        }

        /// <summary>
        /// 根据属性Company获取数据实体
        /// </summary>
        public EconomicContract GetByCompany(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Company));
        }

        #endregion

                
        #region Number

        /// <summary>
        /// 根据属性Number获取数据实体
        /// </summary>
        public EconomicContract GetByNumber(Int32 value)
        {
            var model = new EconomicContract { Number = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Number));
        }

        /// <summary>
        /// 根据属性Number获取数据实体
        /// </summary>
        public EconomicContract GetByNumber(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Number));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public EconomicContract GetByAddress(String value)
        {
            var model = new EconomicContract { Address = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public EconomicContract GetByAddress(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Address));
        }

        #endregion

                
        #region Value

        /// <summary>
        /// 根据属性Value获取数据实体
        /// </summary>
        public EconomicContract GetByValue(Int32 value)
        {
            var model = new EconomicContract { Value = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Value));
        }

        /// <summary>
        /// 根据属性Value获取数据实体
        /// </summary>
        public EconomicContract GetByValue(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Value));
        }

        #endregion

                
        #region STime

        /// <summary>
        /// 根据属性STime获取数据实体
        /// </summary>
        public EconomicContract GetBySTime(DateTime value)
        {
            var model = new EconomicContract { STime = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.STime));
        }

        /// <summary>
        /// 根据属性STime获取数据实体
        /// </summary>
        public EconomicContract GetBySTime(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.STime));
        }

        #endregion

                
        #region ETime

        /// <summary>
        /// 根据属性ETime获取数据实体
        /// </summary>
        public EconomicContract GetByETime(DateTime value)
        {
            var model = new EconomicContract { ETime = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.ETime));
        }

        /// <summary>
        /// 根据属性ETime获取数据实体
        /// </summary>
        public EconomicContract GetByETime(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.ETime));
        }

        #endregion

                
        #region Contractor

        /// <summary>
        /// 根据属性Contractor获取数据实体
        /// </summary>
        public EconomicContract GetByContractor(String value)
        {
            var model = new EconomicContract { Contractor = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Contractor));
        }

        /// <summary>
        /// 根据属性Contractor获取数据实体
        /// </summary>
        public EconomicContract GetByContractor(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Contractor));
        }

        #endregion

                
        #region Contracting

        /// <summary>
        /// 根据属性Contracting获取数据实体
        /// </summary>
        public EconomicContract GetByContracting(String value)
        {
            var model = new EconomicContract { Contracting = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Contracting));
        }

        /// <summary>
        /// 根据属性Contracting获取数据实体
        /// </summary>
        public EconomicContract GetByContracting(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Contracting));
        }

        #endregion

                
        #region PayMent

        /// <summary>
        /// 根据属性PayMent获取数据实体
        /// </summary>
        public EconomicContract GetByPayMent(String value)
        {
            var model = new EconomicContract { PayMent = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.PayMent));
        }

        /// <summary>
        /// 根据属性PayMent获取数据实体
        /// </summary>
        public EconomicContract GetByPayMent(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.PayMent));
        }

        #endregion

                
        #region Contract

        /// <summary>
        /// 根据属性Contract获取数据实体
        /// </summary>
        public EconomicContract GetByContract(Int32 value)
        {
            var model = new EconomicContract { Contract = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Contract));
        }

        /// <summary>
        /// 根据属性Contract获取数据实体
        /// </summary>
        public EconomicContract GetByContract(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Contract));
        }

        #endregion

                
        #region Bidding

        /// <summary>
        /// 根据属性Bidding获取数据实体
        /// </summary>
        public EconomicContract GetByBidding(Int32 value)
        {
            var model = new EconomicContract { Bidding = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Bidding));
        }

        /// <summary>
        /// 根据属性Bidding获取数据实体
        /// </summary>
        public EconomicContract GetByBidding(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Bidding));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public EconomicContract GetByType(Int32 value)
        {
            var model = new EconomicContract { Type = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public EconomicContract GetByType(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Type));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public EconomicContract GetByUserId(Int32 value)
        {
            var model = new EconomicContract { UserId = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public EconomicContract GetByUserId(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public EconomicContract GetByState(Int32 value)
        {
            var model = new EconomicContract { State = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public EconomicContract GetByState(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.State));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public EconomicContract GetByCountry(Int32 value)
        {
            var model = new EconomicContract { Country = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public EconomicContract GetByCountry(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public EconomicContract GetByVillage(Int32 value)
        {
            var model = new EconomicContract { Village = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public EconomicContract GetByVillage(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public EconomicContract GetByGroups(Int32 value)
        {
            var model = new EconomicContract { Groups = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public EconomicContract GetByGroups(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Groups));
        }

        #endregion

                
        #region Uname

        /// <summary>
        /// 根据属性Uname获取数据实体
        /// </summary>
        public EconomicContract GetByUname(String value)
        {
            var model = new EconomicContract { Uname = value };
            return GetByWhere(model.GetFilterString(EconomicContractField.Uname));
        }

        /// <summary>
        /// 根据属性Uname获取数据实体
        /// </summary>
        public EconomicContract GetByUname(EconomicContract model)
        {
            return GetByWhere(model.GetFilterString(EconomicContractField.Uname));
        }

        #endregion

        #endregion
    }
}