﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class AreaDal : ExBaseDal<Area>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AreaDal(): this(Initialization.GetXmlConfig(typeof(Area)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AreaDal CreateDal()
        {
			return new AreaDal(Initialization.GetXmlConfig(typeof(Area)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AreaDal()
        {
           ModelName= typeof(Area).Name;
           var item = new Area();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AreaDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Area";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Area GetByName(String value)
        {
            var model = new Area { Name = value };
            return GetByWhere(model.GetFilterString(AreaField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Area GetByName(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.Name));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public Area GetByParentId(Int32 value)
        {
            var model = new Area { ParentId = value };
            return GetByWhere(model.GetFilterString(AreaField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public Area GetByParentId(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.ParentId));
        }

        #endregion

                
        #region Leader

        /// <summary>
        /// 根据属性Leader获取数据实体
        /// </summary>
        public Area GetByLeader(String value)
        {
            var model = new Area { Leader = value };
            return GetByWhere(model.GetFilterString(AreaField.Leader));
        }

        /// <summary>
        /// 根据属性Leader获取数据实体
        /// </summary>
        public Area GetByLeader(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.Leader));
        }

        #endregion

                
        #region Remarks

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public Area GetByRemarks(String value)
        {
            var model = new Area { Remarks = value };
            return GetByWhere(model.GetFilterString(AreaField.Remarks));
        }

        /// <summary>
        /// 根据属性Remarks获取数据实体
        /// </summary>
        public Area GetByRemarks(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.Remarks));
        }

        #endregion

                
        #region Other

        /// <summary>
        /// 根据属性Other获取数据实体
        /// </summary>
        public Area GetByOther(String value)
        {
            var model = new Area { Other = value };
            return GetByWhere(model.GetFilterString(AreaField.Other));
        }

        /// <summary>
        /// 根据属性Other获取数据实体
        /// </summary>
        public Area GetByOther(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.Other));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Area GetByState(Int32 value)
        {
            var model = new Area { State = value };
            return GetByWhere(model.GetFilterString(AreaField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Area GetByState(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.State));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Area GetByType(Int32 value)
        {
            var model = new Area { Type = value };
            return GetByWhere(model.GetFilterString(AreaField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Area GetByType(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.Type));
        }

        #endregion

                
        #region Tel

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public Area GetByTel(String value)
        {
            var model = new Area { Tel = value };
            return GetByWhere(model.GetFilterString(AreaField.Tel));
        }

        /// <summary>
        /// 根据属性Tel获取数据实体
        /// </summary>
        public Area GetByTel(Area model)
        {
            return GetByWhere(model.GetFilterString(AreaField.Tel));
        }

        #endregion

        #endregion
    }
}