﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using sanzi.Dal.Base;
using sanzi.Dal.Setting;
using sanzi.Model.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;

namespace sanzi.Dal.Collection
{
    /// <summary>
    /// sanzi操作集
    /// </summary>
    public partial class PropertyDal : ExBaseDal<Property>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public PropertyDal(): this(Initialization.GetXmlConfig(typeof(Property)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static PropertyDal CreateDal()
        {
			return new PropertyDal(Initialization.GetXmlConfig(typeof(Property)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static PropertyDal()
        {
           ModelName= typeof(Property).Name;
           var item = new Property();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public PropertyDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Property";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Property GetByName(String value)
        {
            var model = new Property { Name = value };
            return GetByWhere(model.GetFilterString(PropertyField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Property GetByName(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Name));
        }

        #endregion

                
        #region Unit

        /// <summary>
        /// 根据属性Unit获取数据实体
        /// </summary>
        public Property GetByUnit(String value)
        {
            var model = new Property { Unit = value };
            return GetByWhere(model.GetFilterString(PropertyField.Unit));
        }

        /// <summary>
        /// 根据属性Unit获取数据实体
        /// </summary>
        public Property GetByUnit(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Unit));
        }

        #endregion

                
        #region Amount

        /// <summary>
        /// 根据属性Amount获取数据实体
        /// </summary>
        public Property GetByAmount(String value)
        {
            var model = new Property { Amount = value };
            return GetByWhere(model.GetFilterString(PropertyField.Amount));
        }

        /// <summary>
        /// 根据属性Amount获取数据实体
        /// </summary>
        public Property GetByAmount(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Amount));
        }

        #endregion

                
        #region Value

        /// <summary>
        /// 根据属性Value获取数据实体
        /// </summary>
        public Property GetByValue(String value)
        {
            var model = new Property { Value = value };
            return GetByWhere(model.GetFilterString(PropertyField.Value));
        }

        /// <summary>
        /// 根据属性Value获取数据实体
        /// </summary>
        public Property GetByValue(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Value));
        }

        #endregion

                
        #region Depreciation

        /// <summary>
        /// 根据属性Depreciation获取数据实体
        /// </summary>
        public Property GetByDepreciation(String value)
        {
            var model = new Property { Depreciation = value };
            return GetByWhere(model.GetFilterString(PropertyField.Depreciation));
        }

        /// <summary>
        /// 根据属性Depreciation获取数据实体
        /// </summary>
        public Property GetByDepreciation(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Depreciation));
        }

        #endregion

                
        #region Net

        /// <summary>
        /// 根据属性Net获取数据实体
        /// </summary>
        public Property GetByNet(String value)
        {
            var model = new Property { Net = value };
            return GetByWhere(model.GetFilterString(PropertyField.Net));
        }

        /// <summary>
        /// 根据属性Net获取数据实体
        /// </summary>
        public Property GetByNet(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Net));
        }

        #endregion

                
        #region Year

        /// <summary>
        /// 根据属性Year获取数据实体
        /// </summary>
        public Property GetByYear(String value)
        {
            var model = new Property { Year = value };
            return GetByWhere(model.GetFilterString(PropertyField.Year));
        }

        /// <summary>
        /// 根据属性Year获取数据实体
        /// </summary>
        public Property GetByYear(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Year));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public Property GetByAddress(String value)
        {
            var model = new Property { Address = value };
            return GetByWhere(model.GetFilterString(PropertyField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public Property GetByAddress(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Address));
        }

        #endregion

                
        #region Responsible

        /// <summary>
        /// 根据属性Responsible获取数据实体
        /// </summary>
        public Property GetByResponsible(String value)
        {
            var model = new Property { Responsible = value };
            return GetByWhere(model.GetFilterString(PropertyField.Responsible));
        }

        /// <summary>
        /// 根据属性Responsible获取数据实体
        /// </summary>
        public Property GetByResponsible(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Responsible));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Property GetByType(Int32 value)
        {
            var model = new Property { Type = value };
            return GetByWhere(model.GetFilterString(PropertyField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Property GetByType(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Type));
        }

        #endregion

                
        #region UserId

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Property GetByUserId(Int32 value)
        {
            var model = new Property { UserId = value };
            return GetByWhere(model.GetFilterString(PropertyField.UserId));
        }

        /// <summary>
        /// 根据属性UserId获取数据实体
        /// </summary>
        public Property GetByUserId(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.UserId));
        }

        #endregion

                
        #region State

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Property GetByState(Int32 value)
        {
            var model = new Property { State = value };
            return GetByWhere(model.GetFilterString(PropertyField.State));
        }

        /// <summary>
        /// 根据属性State获取数据实体
        /// </summary>
        public Property GetByState(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.State));
        }

        #endregion

                
        #region Country

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Property GetByCountry(Int32 value)
        {
            var model = new Property { Country = value };
            return GetByWhere(model.GetFilterString(PropertyField.Country));
        }

        /// <summary>
        /// 根据属性Country获取数据实体
        /// </summary>
        public Property GetByCountry(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Country));
        }

        #endregion

                
        #region Village

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Property GetByVillage(Int32 value)
        {
            var model = new Property { Village = value };
            return GetByWhere(model.GetFilterString(PropertyField.Village));
        }

        /// <summary>
        /// 根据属性Village获取数据实体
        /// </summary>
        public Property GetByVillage(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Village));
        }

        #endregion

                
        #region Groups

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Property GetByGroups(Int32 value)
        {
            var model = new Property { Groups = value };
            return GetByWhere(model.GetFilterString(PropertyField.Groups));
        }

        /// <summary>
        /// 根据属性Groups获取数据实体
        /// </summary>
        public Property GetByGroups(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.Groups));
        }

        #endregion

                
        #region UName

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Property GetByUName(String value)
        {
            var model = new Property { UName = value };
            return GetByWhere(model.GetFilterString(PropertyField.UName));
        }

        /// <summary>
        /// 根据属性UName获取数据实体
        /// </summary>
        public Property GetByUName(Property model)
        {
            return GetByWhere(model.GetFilterString(PropertyField.UName));
        }

        #endregion

        #endregion
    }
}