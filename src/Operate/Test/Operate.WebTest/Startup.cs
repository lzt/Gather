﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Operate.WebTest.Startup))]
namespace Operate.WebTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
