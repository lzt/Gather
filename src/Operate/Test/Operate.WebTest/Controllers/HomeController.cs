﻿using Operate.Web.Mvc.Common;
using Operate.Web.Mvc.Common.ExtensionMethods;
using System.Web.Mvc;

namespace Operate.WebTest.Controllers
{
    public class HomeController : ControllerEx
    {
        public ActionResult Index()
        {
            var pageNo = this.Param("pageNo", 1, 1);
            var sds = Request.RequestContext.RouteData;
            var ssd = CurrentActionResultInfo;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}