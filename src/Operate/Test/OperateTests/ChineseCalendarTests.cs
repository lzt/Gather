﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.Reflection.ExtensionMethods;

namespace Operate.Test
{



    [TestClass()]
    public class ChineseCalendarTests
    {
        [TestMethod()]
        public void ChineseCalendarTest()
        {
           // var c = new ChineseCalendar(DateTime.Now);
            Assert.Fail();
        }

        public enum ErrorInfo
        {
            /// <summary>
            /// Code码无效
            /// </summary>
            [System.ComponentModel.Description("Code码无效")]
            CodeError = 10011,

            /// <summary>
            /// 登录过期
            /// </summary>
            [System.ComponentModel.Description("登录过期")]
            LoginExpire = 10021,

            /// <summary>
            /// 入口凭证验证失败
            /// </summary>
            [System.ComponentModel.Description("入口凭证验证失败")]
            ClientHostAuthError = 10031,

            /// <summary>
            /// Token过期
            /// </summary>
            [System.ComponentModel.Description("Token过期")]
            TokenExpire = 10041,

            /// <summary>
            /// 权限验证失败
            /// </summary>
            [System.ComponentModel.Description("权限验证失败")]
            CompetenceAuthError = 10051,

            /// <summary>
            /// 没有权限
            /// </summary>
            [System.ComponentModel.Description("没有权限")]
            CompetenceAuthNone = 10052,

            /// <summary>
            /// 用户无效
            /// </summary>
            [System.ComponentModel.Description("用户无效")]
            UserError = 10099,

            /// <summary>
            /// 数据无效
            /// </summary>
            [System.ComponentModel.Description("数据无效")]
            DataError = 10199,
        }

        [TestMethod()]
        public void ChineseCalendarTest1()
        {
            //var sdsd = ErrorInfo.CodeError.GetType().GetCustomAttributes(false);

      var sd=      ErrorInfo.CodeError.GetAttribute<System.ComponentModel.DescriptionAttribute>();

            Assert.Fail();
        }
    }
}
