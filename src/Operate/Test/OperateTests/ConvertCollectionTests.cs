﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.ExtensionMethods;

namespace Operate.Tests
{
    [TestClass()]
    public class ConvertCollectionTests
    {
        [TestMethod()]
        public void NameValueCollectionToJsonTest()
        {
            var nv = new NameValueCollection();
            nv["a"] = "1";
            nv["b"] = "2";
            nv["c"] = "3";
            nv["d"] = "4";
            nv["e"] = "5";
            nv["f"] = "6";
            nv["g"] = "7";

            var r = ConvertCollection.NameValueCollectionToJson(nv);
        }

        [TestMethod()]
        public void NameValueCollectionToJsonTest1()
        {
            var sds = new NameValueCollection();
            sds["content"] = "<p>asdfsafd</p>";
            var df = sds.ToJson();
        }

        [TestMethod()]
        public void JsonStringToNameValueCollectionTest()
        {
            var s =
                "{\"discount\":\"0.00\",\"quantity\":\"1\",\"total_fee\":\"0.01\",\"notify_id\":\"68720884c41aeb4a4ad794129151a12c3q\",\"payment_type\":\"1\",\"notify_time\":\"2015-02-04 03:07:04\",\"is_total_fee_adjust\":\"Y\",\"trade_status\":\"WAIT_BUYER_PAY\",\"price\":\"0.01\",\"subject\":\"账户充值\",\"buyer_id\":\"2088902457652312\",\"use_coupon\":\"N\",\"sign_type\":\"RSA\",\"seller_id\":\"2088811503395560\",\"buyer_email\":\"15515833216\",\"gmt_create\":\"2015-02-03 17:43:39\",\"body\":\"用户账户充值\",\"notify_type\":\"trade_status_sync\",\"sign\":\"mLHDMwTOgaf9gWw7N8nJXWIb1S78h5z455bhZOohf4vt/59j/eEj6TnTtX8hHdPjOR7+hjs19B3R34e/EexP0FRjQK+cPnjYBnrohIn0kS3JhxUZj30l7Wp+HB69tleo676+pO7gTcY15r/Hcw6egZJ4DELhzsacR6oIB8RyLHk=\",\"seller_email\":\"luyou666@luyou666.com\",\"out_trade_no\":\"RE00000000008601\",\"trade_no\":\"2015020340843431\"}";

            var r = ConvertCollection.JsonToNameValueCollection(s);

            var dic = ConvertCollection.NameValueCollectionToDictionary<string>(r);
        }

        [TestMethod()]
        public void ConvertToUnixTimeTest()
        {
            var sdsd = DateTime.Now.ToUnixTime();
        }
    }
}
