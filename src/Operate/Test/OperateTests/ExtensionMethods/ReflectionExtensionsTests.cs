﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.ExtensionMethods.Tests
{
    public class A
    {
        public int V { get; set; }

        public A()
        {
            var r = new System.Random(DateTime.Now.Millisecond);
            V = r.Next();
        }
    }
    public class A1
    {
        public int V { get; set; }
        public string NickName { get; set; }
    }

    [TestClass()]
    public class ReflectionExtensionsTests
    {
        [TestMethod()]
        public void CopyPropertyToTest()
        {
            var a = new A1() { V = 22, NickName ="ttt"};
            //var a1 = new A1();
            //a.CopyPropertyTo<A1>(ref a1);

            var ss = a.HasProperty("V");

            var pv = a.GetValueByPropertyName<string>("nickname", true);
            a.SetValueByPropertyName("nickname", "345345", true);
        }

        [TestMethod()]
        public void GetCloneTest()
        {
            //var a = new List<string>();
            ////var b = a.GetClone();
            //var type = a.GetType();
            //type.Create()
            //Type[] interfaces = a.GetType().GetInterfaces();
            //StringBuilder ss=new StringBuilder();
            //if (interfaces != null)
            //{
            //    foreach (Type inter in interfaces)
            //    {
            //        //Console.WriteLine(inter.FullName);
            //        ss.AppendLine(inter.Name);
            //    }
            //}
            //var r = ss.ToString();

            //var tr = new A[2] {new A(),new A()};
            // var tr = new List<A>() { new A(), new A() };
            //var r = tr.GetClone();
        }

        [TestMethod()]
        public void EqualsByPropertyTest()
        {

        }

        [TestMethod()]
        public void HasPropertyTest()
        {

        }
    }
}
