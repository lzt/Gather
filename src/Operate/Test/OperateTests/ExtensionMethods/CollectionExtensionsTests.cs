﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.ExtensionMethods.Tests
{
    class Aa
    {
        public int P1 { get; set; }
        public int P2 { get; set; }

        public DateTime Time { get; set; }

    }
    class Bb
    {
        public int P1 { get; set; }
        public int P2 { get; set; }
    }


    [TestClass()]
    public class CollectionExtensionsTests
    {
        [TestMethod()]
        public void JoinTest()
        {
            var list = new List<int> { 32354, 56758, 234535 };
            var res = list.Join("-", "{0}");
        }

        [TestMethod()]
        public void JoinTest1()
        {
            //var r = new System.Random();

            //var b = new Bb() { P1 = 11, P2 = 82 };
            //var intlist = new List<int>() { 11, 12, 333, 45 };
            //var c = new Aa { P1 = 11, P2 = 82, Time = DateTime.Now.AddMinutes(r.Next(100)) };
            //var list = new List<Aa>()
            //{
            //  c,
            //    new Aa { P1 = 11, P2 = 12, Time = DateTime.Now.AddMinutes(r.Next(100))  }, 
            //    new Aa { P1 = 12, P2 = 22, Time = DateTime.Now.AddMinutes(r.Next(100))  }, 
            //    new Aa { P1 = 13, P2 = 32, Time = DateTime.Now.AddMinutes(r.Next(100))  }, 
            //    new Aa { P1 = 14, P2 = 42 , Time = DateTime.Now.AddMinutes(r.Next(100)) },
            //    new Aa { P1 = 11, P2 = 82 , Time = DateTime.Now.AddMinutes(r.Next(100)) },
            //};

            //var ss = list.SortByPropertyValue(ConstantCollection.SortRule.Desc, "Time");

            ////var r1 = list.GetAll(b,"P1");
            ////var list1 = list.DistinctByProperty();
            ////var jion = list.Join(",", "Id={0}", true, "P2");
            ////list.RemoveValue(c, "P1",false);

            ////var sds = b.EqualsByProperty(c);
            ////var vv = list.GetPropertyValueCollection<Aa, int>("P1", false);
        }

        [TestMethod()]
        public void SetTest()
        {
            var list0 = new List<int> { 32354, 56758};
            var list1 = new List<int> { 32354, 56758, 2345356 };
            var list2 = new List<int> { 32354, 56758, 2345358 };

            var res = list1.GetUniversalSet(list2);
        }
    }
}

namespace Operate.ExtensionMethods.Test
{
    [TestClass()]
    public class CollectionExtensionsTests
    {
        [TestMethod()]
        public void ToArrayTest()
        {
            var list = new List<string>();
            list.Add("a");
            list.Add("b");
            list.Add("c");
            list.Add("d");

            var r = list.ToArray();
            var sd = r.ToList();
            Assert.Fail();
        }
    }
}
