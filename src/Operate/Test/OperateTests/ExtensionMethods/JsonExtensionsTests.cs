﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Operate.DataBase.SqlDatabase.Attribute;
using Operate.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.IO.ExtensionMethods;
using Operate.Web.ExtensionMethods;

namespace Operate.ExtensionMethods.Tests
{

    [TestClass()]
    public class JsonExtensionsTests
    {
        [TestMethod()]
        public void CopyValueToTest()
        {
            //var request = (HttpWebRequest)WebRequest.Create("http://db.auto.sohu.com/PARA/TRIMDATA/trim_data_126955.json");
            //var response = request.GetResponse();
            ////var html = response.GetResponseStream().ReadAll(Encoding.Default);
            //var json = response.GetResponseStream().ReadAll(Encoding.Default).UrlDecode().Replace("�", "X");
            //JObject dy = JsonConvert.DeserializeObject<JObject>(json);
            //var carmodel = new CarModel();

            //dy.CopyValueTo(ref carmodel);


            //using (var dal = new CarModelDal())
            //{

                var requestmodel =
                    (HttpWebRequest)
                        WebRequest.Create("http://db.auto.sohu.com/PARA/TRIMDATA/trim_data_{0}.json".FormatValue(121788));
                var responsemodel = requestmodel.GetResponse();
                var json = responsemodel.GetResponseStream().ReadAll(Encoding.Default).UrlDecode().Replace("�", "X");
                json =
                    json.Replace("SIP_T_ID", "ModelId")
                        .Replace("SIP_T_NAME", "Name")
                        .Replace("SIP_T_MODELID", "SeriesId")
                        .Replace("SIP_T_MODELNAME", "ModelName")
                        .Replace("SIP_T_YEAR", "Year")
                        .Replace("SIP_T_DISPL", "Displ")
                        .Replace("SIP_T_PRICE", "Price")
                        .Replace("SIP_T_GEAR", "Gear")
                        .Replace("-'", "3'")
                        .Replace("●'", "1'")
                        .Replace("○'", "2'")
                        .Replace("SIP_T_STA", "Status");

                var dy = JsonConvert.DeserializeObject<JObject>(json);
                var carmodel = new CarModel();
                dy.CopyValueTo(ref carmodel);
                var sdf = "wer";
            //}
        }
    }

    /// <summary>
    /// CarModel实体模型
    /// </summary>
    [Serializable]
    public  class CarModel
    {
        #region CarModel Properties

        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：自增主键
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual int Id { get; set; }


        /// <summary>
        /// 字段：ModelId
        /// 描述：车款Id 来自搜狐数据
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车款Id 来自搜狐数据")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int ModelId { get; set; }

        /// <summary>
        /// 字段：Name
        /// 描述：款型名称
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("款型名称")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Name { get; set; }

        /// <summary>
        /// 字段：SeriesId
        /// 描述：车型Id
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车型Id")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SeriesId { get; set; }

        /// <summary>
        /// 字段：ModelName
        /// 描述：车型名称
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：100
        /// 是否可空：True
        /// </summary>
        [Length("100")]
        [Comment("车型名称")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string ModelName { get; set; }

        /// <summary>
        /// 字段：Year
        /// 描述：年代
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("年代")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int Year { get; set; }

        /// <summary>
        /// 字段：Displ
        /// 描述：发动机
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("发动机")]
        [SqlType("float(8)")]
        [Precision("53")]
        [Scale("")]
        public virtual double Displ { get; set; }

        /// <summary>
        /// 字段：Price
        /// 描述：价格
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("价格")]
        [SqlType("float(8)")]
        [Precision("53")]
        [Scale("")]
        public virtual double Price { get; set; }

        /// <summary>
        /// 字段：Gear
        /// 描述：装置数
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("装置数")]
        [SqlType("nvarchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual string Gear { get; set; }

        /// <summary>
        /// 字段：Status
        /// 描述：状态
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("状态")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int Status { get; set; }

        /// <summary>
        /// 字段：SIP_C_102
        /// 描述：厂商指导价
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("厂商指导价")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_102 { get; set; }

        /// <summary>
        /// 字段：SIP_C_103
        /// 描述：经销商最低报价
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("经销商最低报价")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_103 { get; set; }

        /// <summary>
        /// 字段：SIP_C_104
        /// 描述：车厂
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("车厂")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_104 { get; set; }

        /// <summary>
        /// 字段：SIP_C_105
        /// 描述：级别
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("级别")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_105 { get; set; }

        /// <summary>
        /// 字段：SIP_C_106
        /// 描述：车体结构
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("车体结构")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_106 { get; set; }

        /// <summary>
        /// 字段：SIP_C_293
        /// 描述：长x宽x高(mm)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("长x宽x高(mm)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_293 { get; set; }

        /// <summary>
        /// 字段：SIP_C_107
        /// 描述：发动机
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("发动机")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_107 { get; set; }

        /// <summary>
        /// 字段：SIP_C_108
        /// 描述：变速箱
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("变速箱")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_108 { get; set; }

        /// <summary>
        /// 字段：SIP_C_303
        /// 描述：动力类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("动力类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_303 { get; set; }

        /// <summary>
        /// 字段：SIP_C_112
        /// 描述：官方最高车速(km/h)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("官方最高车速(km/h)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_112 { get; set; }

        /// <summary>
        /// 字段：SIP_C_294
        /// 描述：工信部油耗(L/100km)(城市/市郊/综合)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("工信部油耗(L/100km)(城市/市郊/综合)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_294 { get; set; }

        /// <summary>
        /// 字段：SIP_C_113
        /// 描述：官方0-100加速(s)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("官方0-100加速(s)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_113 { get; set; }

        /// <summary>
        /// 字段：SIP_C_114
        /// 描述：保养周期
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("保养周期")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_114 { get; set; }

        /// <summary>
        /// 字段：SIP_C_304
        /// 描述：保养费用(元，6万公里合计)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("保养费用(元，6万公里合计)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_304 { get; set; }

        /// <summary>
        /// 字段：SIP_C_115
        /// 描述：保修政策
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("保修政策")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_115 { get; set; }

        /// <summary>
        /// 字段：SIP_C_116
        /// 描述：碰撞星级
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("碰撞星级")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_116 { get; set; }

        /// <summary>
        /// 字段：SIP_C_295
        /// 描述：更多实测参数
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("更多实测参数")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_295 { get; set; }

        /// <summary>
        /// 字段：SIP_C_117
        /// 描述：长度(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("长度(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_117 { get; set; }

        /// <summary>
        /// 字段：SIP_C_118
        /// 描述：宽度(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("宽度(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_118 { get; set; }

        /// <summary>
        /// 字段：SIP_C_119
        /// 描述：高度(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("高度(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_119 { get; set; }

        /// <summary>
        /// 字段：SIP_C_120
        /// 描述：轴距(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("轴距(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_120 { get; set; }

        /// <summary>
        /// 字段：SIP_C_121
        /// 描述：前轮距(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前轮距(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_121 { get; set; }

        /// <summary>
        /// 字段：SIP_C_122
        /// 描述：后轮距(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后轮距(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_122 { get; set; }

        /// <summary>
        /// 字段：SIP_C_123
        /// 描述：整备质量(kg)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("整备质量(kg)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_123 { get; set; }

        /// <summary>
        /// 字段：SIP_C_124
        /// 描述：车身结构
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("车身结构")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_124 { get; set; }

        /// <summary>
        /// 字段：SIP_C_125
        /// 描述：车门数(个)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车门数(个)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_125 { get; set; }

        /// <summary>
        /// 字段：SIP_C_126
        /// 描述：座位数(个)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("座位数(个)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_126 { get; set; }

        /// <summary>
        /// 字段：SIP_C_127
        /// 描述：油箱容积(L)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("油箱容积(L)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_127 { get; set; }

        /// <summary>
        /// 字段：SIP_C_128
        /// 描述：行李厢容积(L)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("行李厢容积(L)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_128 { get; set; }

        /// <summary>
        /// 字段：SIP_C_129
        /// 描述：最小离地间隙(mm)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("最小离地间隙(mm)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_129 { get; set; }

        /// <summary>
        /// 字段：SIP_C_130
        /// 描述：最小转弯半径(m)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("最小转弯半径(m)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_130 { get; set; }

        /// <summary>
        /// 字段：SIP_C_131
        /// 描述：接近角(°)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("接近角(°)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_131 { get; set; }

        /// <summary>
        /// 字段：SIP_C_132
        /// 描述：离去角(°)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("离去角(°)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_132 { get; set; }

        /// <summary>
        /// 字段：SIP_C_133
        /// 描述：涉水深度(mm)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("涉水深度(mm)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_133 { get; set; }

        /// <summary>
        /// 字段：SIP_C_134
        /// 描述：发动机描述
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("发动机描述")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_134 { get; set; }

        /// <summary>
        /// 字段：SIP_C_135
        /// 描述：发动机型号
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("发动机型号")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_135 { get; set; }

        /// <summary>
        /// 字段：SIP_C_136
        /// 描述：排量(L)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("排量(L)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_136 { get; set; }

        /// <summary>
        /// 字段：SIP_C_137
        /// 描述：汽缸容积(cc)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("汽缸容积(cc)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_137 { get; set; }

        /// <summary>
        /// 字段：SIP_C_138
        /// 描述：工作方式
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("工作方式")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_138 { get; set; }

        /// <summary>
        /// 字段：SIP_C_139
        /// 描述：汽缸数(个)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("汽缸数(个)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_139 { get; set; }

        /// <summary>
        /// 字段：SIP_C_140
        /// 描述：汽缸排列形式
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("汽缸排列形式")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_140 { get; set; }

        /// <summary>
        /// 字段：SIP_C_141
        /// 描述：每缸气门数(个)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("每缸气门数(个)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_141 { get; set; }

        /// <summary>
        /// 字段：SIP_C_142
        /// 描述：气门结构
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("气门结构")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_142 { get; set; }

        /// <summary>
        /// 字段：SIP_C_143
        /// 描述：压缩比
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("压缩比")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_143 { get; set; }

        /// <summary>
        /// 字段：SIP_C_297
        /// 描述：最大马力(ps)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("最大马力(ps)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_297 { get; set; }

        /// <summary>
        /// 字段：SIP_C_298
        /// 描述：最大功率(kW/rpm)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("最大功率(kW/rpm)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_298 { get; set; }

        /// <summary>
        /// 字段：SIP_C_299
        /// 描述：最大扭矩(N·m/rpm)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("最大扭矩(N·m/rpm)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_299 { get; set; }

        /// <summary>
        /// 字段：SIP_C_148
        /// 描述：升功率(kW/l)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("升功率(kW/l)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_148 { get; set; }

        /// <summary>
        /// 字段：SIP_C_305
        /// 描述：混合类型
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("混合类型")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_305 { get; set; }

        /// <summary>
        /// 字段：SIP_C_306
        /// 描述：插电形式
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("插电形式")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_306 { get; set; }

        /// <summary>
        /// 字段：SIP_C_307
        /// 描述：电动机最大功率(kW)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电动机最大功率(kW)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_307 { get; set; }

        /// <summary>
        /// 字段：SIP_C_308
        /// 描述：电动机最大扭矩
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电动机最大扭矩")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_308 { get; set; }

        /// <summary>
        /// 字段：SIP_C_309
        /// 描述：最大行驶里程(纯电)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("最大行驶里程(纯电)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_309 { get; set; }

        /// <summary>
        /// 字段：SIP_C_310
        /// 描述：电池种类
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电池种类")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_310 { get; set; }

        /// <summary>
        /// 字段：SIP_C_311
        /// 描述：电池性能
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电池性能")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_311 { get; set; }

        /// <summary>
        /// 字段：SIP_C_149
        /// 描述：燃料
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("燃料")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_149 { get; set; }

        /// <summary>
        /// 字段：SIP_C_150
        /// 描述：供油方式
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("供油方式")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_150 { get; set; }

        /// <summary>
        /// 字段：SIP_C_151
        /// 描述：缸盖材料
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("缸盖材料")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_151 { get; set; }

        /// <summary>
        /// 字段：SIP_C_152
        /// 描述：缸体材料
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("缸体材料")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_152 { get; set; }

        /// <summary>
        /// 字段：SIP_C_153
        /// 描述：缸径(mm)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("缸径(mm)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_153 { get; set; }

        /// <summary>
        /// 字段：SIP_C_154
        /// 描述：行程(mm)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("行程(mm)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_154 { get; set; }

        /// <summary>
        /// 字段：SIP_C_155
        /// 描述：排放标准
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("排放标准")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_155 { get; set; }

        /// <summary>
        /// 字段：SIP_C_156
        /// 描述：变速箱简称
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("变速箱简称")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_156 { get; set; }

        /// <summary>
        /// 字段：SIP_C_157
        /// 描述：挡位个数
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("挡位个数")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_157 { get; set; }

        /// <summary>
        /// 字段：SIP_C_158
        /// 描述：变速箱类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("变速箱类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_158 { get; set; }

        /// <summary>
        /// 字段：SIP_C_159
        /// 描述：驱动方式
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("驱动方式")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_159 { get; set; }

        /// <summary>
        /// 字段：SIP_C_160
        /// 描述：前悬挂类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("前悬挂类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_160 { get; set; }

        /// <summary>
        /// 字段：SIP_C_161
        /// 描述：后悬挂类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("后悬挂类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_161 { get; set; }

        /// <summary>
        /// 字段：SIP_C_162
        /// 描述：底盘结构
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("底盘结构")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_162 { get; set; }

        /// <summary>
        /// 字段：SIP_C_163
        /// 描述：前轮胎规格
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("前轮胎规格")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_163 { get; set; }

        /// <summary>
        /// 字段：SIP_C_164
        /// 描述：后轮胎规格
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("后轮胎规格")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_164 { get; set; }

        /// <summary>
        /// 字段：SIP_C_165
        /// 描述：轮毂材料
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("轮毂材料")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_165 { get; set; }

        /// <summary>
        /// 字段：SIP_C_166
        /// 描述：备胎规格
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("备胎规格")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_166 { get; set; }

        /// <summary>
        /// 字段：SIP_C_167
        /// 描述：前制动器类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("前制动器类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_167 { get; set; }

        /// <summary>
        /// 字段：SIP_C_168
        /// 描述：后制动器类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("后制动器类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_168 { get; set; }

        /// <summary>
        /// 字段：SIP_C_169
        /// 描述：驻车制动类型
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("驻车制动类型")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_169 { get; set; }

        /// <summary>
        /// 字段：SIP_C_170
        /// 描述：分动器类型
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("分动器类型")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_170 { get; set; }

        /// <summary>
        /// 字段：SIP_C_171
        /// 描述：转向助力
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("转向助力")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_171 { get; set; }

        /// <summary>
        /// 字段：SIP_C_172
        /// 描述：可调悬挂
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("可调悬挂")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_172 { get; set; }

        /// <summary>
        /// 字段：SIP_C_173
        /// 描述：空气悬挂
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("空气悬挂")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_173 { get; set; }

        /// <summary>
        /// 字段：SIP_C_174
        /// 描述：主动转向系统
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("主动转向系统")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_174 { get; set; }

        /// <summary>
        /// 字段：SIP_C_177
        /// 描述：驾驶座安全气囊
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("驾驶座安全气囊")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_177 { get; set; }

        /// <summary>
        /// 字段：SIP_C_178
        /// 描述：副驾驶安全气囊
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("副驾驶安全气囊")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_178 { get; set; }

        /// <summary>
        /// 字段：SIP_C_179
        /// 描述：前排侧气囊
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前排侧气囊")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_179 { get; set; }

        /// <summary>
        /// 字段：SIP_C_180
        /// 描述：后排侧气囊
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排侧气囊")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_180 { get; set; }

        /// <summary>
        /// 字段：SIP_C_181
        /// 描述：头部气帘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("头部气帘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_181 { get; set; }

        /// <summary>
        /// 字段：SIP_C_183
        /// 描述：膝部气囊
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("膝部气囊")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_183 { get; set; }

        /// <summary>
        /// 字段：SIP_C_184
        /// 描述：安全带未系提示
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("安全带未系提示")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_184 { get; set; }

        /// <summary>
        /// 字段：SIP_C_185
        /// 描述：自动防抱死(ABS等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("自动防抱死(ABS等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_185 { get; set; }

        /// <summary>
        /// 字段：SIP_C_186
        /// 描述：制动力分配(EBD/CBC等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("制动力分配(EBD/CBC等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_186 { get; set; }

        /// <summary>
        /// 字段：SIP_C_187
        /// 描述：刹车辅助(EBA/BAS/BA等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("刹车辅助(EBA/BAS/BA等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_187 { get; set; }

        /// <summary>
        /// 字段：SIP_C_188
        /// 描述：牵引力控制(ASR/TCS/TRC等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("牵引力控制(ASR/TCS/TRC等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_188 { get; set; }

        /// <summary>
        /// 字段：SIP_C_189
        /// 描述：车身稳定控制(ESP/DSC/VSC等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车身稳定控制(ESP/DSC/VSC等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_189 { get; set; }

        /// <summary>
        /// 字段：SIP_C_190
        /// 描述：主动刹车/安全系统
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("主动刹车/安全系统")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_190 { get; set; }

        /// <summary>
        /// 字段：SIP_C_191
        /// 描述：自动驻车/上坡辅助
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("自动驻车/上坡辅助")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_191 { get; set; }

        /// <summary>
        /// 字段：SIP_C_192
        /// 描述：陡坡缓降
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("陡坡缓降")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_192 { get; set; }

        /// <summary>
        /// 字段：SIP_C_193
        /// 描述：发动机电子防盗
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("发动机电子防盗")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_193 { get; set; }

        /// <summary>
        /// 字段：SIP_C_194
        /// 描述：车内中控锁
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车内中控锁")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_194 { get; set; }

        /// <summary>
        /// 字段：SIP_C_195
        /// 描述：遥控钥匙
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("遥控钥匙")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_195 { get; set; }

        /// <summary>
        /// 字段：SIP_C_196
        /// 描述：无钥匙启动系统
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("无钥匙启动系统")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_196 { get; set; }

        /// <summary>
        /// 字段：SIP_C_197
        /// 描述：胎压监测装置
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("胎压监测装置")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_197 { get; set; }

        /// <summary>
        /// 字段：SIP_C_198
        /// 描述：零胎压继续行驶
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("零胎压继续行驶")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_198 { get; set; }

        /// <summary>
        /// 字段：SIP_C_199
        /// 描述：并线辅助
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("并线辅助")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_199 { get; set; }

        /// <summary>
        /// 字段：SIP_C_204
        /// 描述：全景摄像头
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("全景摄像头")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_204 { get; set; }

        /// <summary>
        /// 字段：SIP_C_205
        /// 描述：夜视系统
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("夜视系统")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_205 { get; set; }

        /// <summary>
        /// 字段：SIP_C_312
        /// 描述：ISO FIX儿童座椅接口
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("ISO FIX儿童座椅接口")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_312 { get; set; }

        /// <summary>
        /// 字段：SIP_C_313
        /// 描述：LATCH座椅接口(兼容ISO FIX)
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("LATCH座椅接口(兼容ISO FIX)")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_313 { get; set; }

        /// <summary>
        /// 字段：SIP_C_314
        /// 描述：儿童安全锁
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("儿童安全锁")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_314 { get; set; }

        /// <summary>
        /// 字段：SIP_C_315
        /// 描述：气囊锁止功能
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("气囊锁止功能")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_315 { get; set; }

        /// <summary>
        /// 字段：SIP_C_316
        /// 描述：天窗型式
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("天窗型式")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_316 { get; set; }

        /// <summary>
        /// 字段：SIP_C_210
        /// 描述：运动外观套件
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("运动外观套件")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_210 { get; set; }

        /// <summary>
        /// 字段：SIP_C_211
        /// 描述：电动后舱/行李箱盖
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电动后舱/行李箱盖")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_211 { get; set; }

        /// <summary>
        /// 字段：SIP_C_212
        /// 描述：电动吸合门
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电动吸合门")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_212 { get; set; }

        /// <summary>
        /// 字段：SIP_C_300
        /// 描述：车身其它配置
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车身其它配置")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_300 { get; set; }

        /// <summary>
        /// 字段：SIP_C_213
        /// 描述：真皮方向盘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("真皮方向盘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_213 { get; set; }

        /// <summary>
        /// 字段：SIP_C_214
        /// 描述：方向盘上下调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("方向盘上下调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_214 { get; set; }

        /// <summary>
        /// 字段：SIP_C_215
        /// 描述：方向盘前后调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("方向盘前后调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_215 { get; set; }

        /// <summary>
        /// 字段：SIP_C_216
        /// 描述：方向盘电动调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("方向盘电动调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_216 { get; set; }

        /// <summary>
        /// 字段：SIP_C_217
        /// 描述：多功能方向盘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("多功能方向盘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_217 { get; set; }

        /// <summary>
        /// 字段：SIP_C_218
        /// 描述：方向盘换挡
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("方向盘换挡")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_218 { get; set; }

        /// <summary>
        /// 字段：SIP_C_175
        /// 描述：定速巡航
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("定速巡航")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_175 { get; set; }

        /// <summary>
        /// 字段：SIP_C_176
        /// 描述：自适应巡航
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("自适应巡航")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_176 { get; set; }

        /// <summary>
        /// 字段：SIP_C_219
        /// 描述：行车电脑显示屏
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("行车电脑显示屏")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_219 { get; set; }

        /// <summary>
        /// 字段：SIP_C_220
        /// 描述：自发光仪表盘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("自发光仪表盘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_220 { get; set; }

        /// <summary>
        /// 字段：SIP_C_200
        /// 描述：HUD抬头数字显示
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("HUD抬头数字显示")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_200 { get; set; }

        /// <summary>
        /// 字段：SIP_C_201
        /// 描述：倒车雷达
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("倒车雷达")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_201 { get; set; }

        /// <summary>
        /// 字段：SIP_C_202
        /// 描述：倒车影像
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("倒车影像")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_202 { get; set; }

        /// <summary>
        /// 字段：SIP_C_203
        /// 描述：自动停车入位
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("自动停车入位")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_203 { get; set; }

        /// <summary>
        /// 字段：SIP_C_221
        /// 描述：行李舱灯
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("行李舱灯")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_221 { get; set; }

        /// <summary>
        /// 字段：SIP_C_222
        /// 描述：独立电源接口
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("独立电源接口")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_222 { get; set; }

        /// <summary>
        /// 字段：SIP_C_223
        /// 描述：中控液晶屏分屏显示
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("中控液晶屏分屏显示")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_223 { get; set; }

        /// <summary>
        /// 字段：SIP_C_301
        /// 描述：车内其它配置
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车内其它配置")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_301 { get; set; }

        /// <summary>
        /// 字段：SIP_C_224
        /// 描述：真皮/仿皮座椅
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("真皮/仿皮座椅")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_224 { get; set; }

        /// <summary>
        /// 字段：SIP_C_225
        /// 描述：运动座椅
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("运动座椅")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_225 { get; set; }

        /// <summary>
        /// 字段：SIP_C_226
        /// 描述：座椅高低调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("座椅高低调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_226 { get; set; }

        /// <summary>
        /// 字段：SIP_C_227
        /// 描述：腰部支撑调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("腰部支撑调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_227 { get; set; }

        /// <summary>
        /// 字段：SIP_C_228
        /// 描述：肩部支撑调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("肩部支撑调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_228 { get; set; }

        /// <summary>
        /// 字段：SIP_C_229
        /// 描述：驾驶席座椅电动调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("驾驶席座椅电动调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_229 { get; set; }

        /// <summary>
        /// 字段：SIP_C_230
        /// 描述：副驾驶座椅电动调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("副驾驶座椅电动调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_230 { get; set; }

        /// <summary>
        /// 字段：SIP_C_317
        /// 描述：后排座椅调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排座椅调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_317 { get; set; }

        /// <summary>
        /// 字段：SIP_C_233
        /// 描述：电动座椅记忆
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("电动座椅记忆")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_233 { get; set; }

        /// <summary>
        /// 字段：SIP_C_234
        /// 描述：前排座椅加热
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前排座椅加热")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_234 { get; set; }

        /// <summary>
        /// 字段：SIP_C_235
        /// 描述：后排座椅加热
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排座椅加热")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_235 { get; set; }

        /// <summary>
        /// 字段：SIP_C_236
        /// 描述：座椅通风
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("座椅通风")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_236 { get; set; }

        /// <summary>
        /// 字段：SIP_C_237
        /// 描述：座椅按摩
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("座椅按摩")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_237 { get; set; }

        /// <summary>
        /// 字段：SIP_C_238
        /// 描述：后排座椅整体放倒
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排座椅整体放倒")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_238 { get; set; }

        /// <summary>
        /// 字段：SIP_C_239
        /// 描述：后排座椅比例放倒
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排座椅比例放倒")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_239 { get; set; }

        /// <summary>
        /// 字段：SIP_C_240
        /// 描述：第三排座椅
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("第三排座椅")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_240 { get; set; }

        /// <summary>
        /// 字段：SIP_C_241
        /// 描述：前座中央扶手
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前座中央扶手")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_241 { get; set; }

        /// <summary>
        /// 字段：SIP_C_242
        /// 描述：后座中央扶手
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后座中央扶手")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_242 { get; set; }

        /// <summary>
        /// 字段：SIP_C_321
        /// 描述：CD/DVD
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("CD/DVD")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_321 { get; set; }

        /// <summary>
        /// 字段：SIP_C_247
        /// 描述：CD支持MP3/WMA
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("CD支持MP3/WMA")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_247 { get; set; }

        /// <summary>
        /// 字段：SIP_C_248
        /// 描述：外接音源接口(AUX/USB/iPod等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("外接音源接口(AUX/USB/iPod等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_248 { get; set; }

        /// <summary>
        /// 字段：SIP_C_249
        /// 描述：扬声器喇叭数量(个)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("扬声器喇叭数量(个)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_249 { get; set; }

        /// <summary>
        /// 字段：SIP_C_250
        /// 描述：音响品牌
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("音响品牌")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_250 { get; set; }

        /// <summary>
        /// 字段：SIP_C_251
        /// 描述：蓝牙/车载电话
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("蓝牙/车载电话")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_251 { get; set; }

        /// <summary>
        /// 字段：SIP_C_252
        /// 描述：车载电视
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车载电视")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_252 { get; set; }

        /// <summary>
        /// 字段：SIP_C_253
        /// 描述：中控台液晶屏
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("中控台液晶屏")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_253 { get; set; }

        /// <summary>
        /// 字段：SIP_C_254
        /// 描述：后排液晶屏
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排液晶屏")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_254 { get; set; }

        /// <summary>
        /// 字段：SIP_C_255
        /// 描述：GPS导航系统
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("GPS导航系统")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_255 { get; set; }

        /// <summary>
        /// 字段：SIP_C_256
        /// 描述：内置硬盘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("内置硬盘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_256 { get; set; }

        /// <summary>
        /// 字段：SIP_C_257
        /// 描述：车载信息服务
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车载信息服务")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_257 { get; set; }

        /// <summary>
        /// 字段：SIP_C_258
        /// 描述：人机交互系统(MMI/iDrive等)
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("人机交互系统(MMI/iDrive等)")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_258 { get; set; }

        /// <summary>
        /// 字段：SIP_C_318
        /// 描述：大灯形式
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("大灯形式")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_318 { get; set; }

        /// <summary>
        /// 字段：SIP_C_260
        /// 描述：日间行车灯
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("日间行车灯")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_260 { get; set; }

        /// <summary>
        /// 字段：SIP_C_261
        /// 描述：前雾灯
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前雾灯")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_261 { get; set; }

        /// <summary>
        /// 字段：SIP_C_262
        /// 描述：大灯自动开闭
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("大灯自动开闭")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_262 { get; set; }

        /// <summary>
        /// 字段：SIP_C_263
        /// 描述：大灯随动调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("大灯随动调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_263 { get; set; }

        /// <summary>
        /// 字段：SIP_C_264
        /// 描述：大灯高度可调
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("大灯高度可调")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_264 { get; set; }

        /// <summary>
        /// 字段：SIP_C_265
        /// 描述：大灯清洗装置
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("大灯清洗装置")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_265 { get; set; }

        /// <summary>
        /// 字段：SIP_C_266
        /// 描述：车内氛围灯
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车内氛围灯")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_266 { get; set; }

        /// <summary>
        /// 字段：SIP_C_267
        /// 描述：前排电动车窗
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前排电动车窗")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_267 { get; set; }

        /// <summary>
        /// 字段：SIP_C_268
        /// 描述：后排电动车窗
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排电动车窗")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_268 { get; set; }

        /// <summary>
        /// 字段：SIP_C_269
        /// 描述：车窗一键功能
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车窗一键功能")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_269 { get; set; }

        /// <summary>
        /// 字段：SIP_C_270
        /// 描述：车窗防夹手功能
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车窗防夹手功能")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_270 { get; set; }

        /// <summary>
        /// 字段：SIP_C_271
        /// 描述：防紫外线/隔热玻璃
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("防紫外线/隔热玻璃")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_271 { get; set; }

        /// <summary>
        /// 字段：SIP_C_277
        /// 描述：后视镜防眩目
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后视镜防眩目")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_277 { get; set; }

        /// <summary>
        /// 字段：SIP_C_278
        /// 描述：后视镜电动调节
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后视镜电动调节")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_278 { get; set; }

        /// <summary>
        /// 字段：SIP_C_279
        /// 描述：后视镜加热
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后视镜加热")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_279 { get; set; }

        /// <summary>
        /// 字段：SIP_C_282
        /// 描述：后视镜电动折叠
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后视镜电动折叠")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_282 { get; set; }

        /// <summary>
        /// 字段：SIP_C_319
        /// 描述：内后视镜防眩目
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("内后视镜防眩目")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_319 { get; set; }

        /// <summary>
        /// 字段：SIP_C_272
        /// 描述：前感应雨刷
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前感应雨刷")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_272 { get; set; }

        /// <summary>
        /// 字段：SIP_C_273
        /// 描述：后雨刷
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后雨刷")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_273 { get; set; }

        /// <summary>
        /// 字段：SIP_C_274
        /// 描述：后风挡除霜
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("后风挡除霜")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_274 { get; set; }

        /// <summary>
        /// 字段：SIP_C_275
        /// 描述：后风挡遮阳帘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后风挡遮阳帘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_275 { get; set; }

        /// <summary>
        /// 字段：SIP_C_276
        /// 描述：后排侧遮阳帘
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排侧遮阳帘")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_276 { get; set; }

        /// <summary>
        /// 字段：SIP_C_320
        /// 描述：空调
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("空调")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_320 { get; set; }

        /// <summary>
        /// 字段：SIP_C_285
        /// 描述：后排出风口
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排出风口")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_285 { get; set; }

        /// <summary>
        /// 字段：SIP_C_286
        /// 描述：前排温度分区控制
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("前排温度分区控制")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_286 { get; set; }

        /// <summary>
        /// 字段：SIP_C_287
        /// 描述：后排独立温区控制
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("后排独立温区控制")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_287 { get; set; }

        /// <summary>
        /// 字段：SIP_C_288
        /// 描述：空气调节/花粉过滤
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("空气调节/花粉过滤")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_288 { get; set; }

        /// <summary>
        /// 字段：SIP_C_289
        /// 描述：车载冰箱
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车载冰箱")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_289 { get; set; }

        /// <summary>
        /// 字段：SIP_C_290
        /// 描述：风冷手套箱
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("风冷手套箱")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual int SIP_C_290 { get; set; }

        /// <summary>
        /// 字段：SIP_C_291
        /// 描述：车身可选颜色
        /// 映射类型：string
        /// 数据库中字段类型：ntext
        /// 实际占用长度：16
        /// 是否可空：True
        /// </summary>
        [Length("16")]
        [Comment("车身可选颜色")]
        [SqlType("ntext")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_291 { get; set; }

        /// <summary>
        /// 字段：SIP_C_292
        /// 描述：内饰可选颜色
        /// 映射类型：string
        /// 数据库中字段类型：ntext
        /// 实际占用长度：16
        /// 是否可空：True
        /// </summary>
        [Length("16")]
        [Comment("内饰可选颜色")]
        [SqlType("ntext")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_292 { get; set; }

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static CarModel()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public CarModel()
        {
            //属性初始化
            ModelId = 0;
            Name = "";
            SeriesId = 0;
            ModelName = "";
            Year = 0;
            Displ = 0;
            Price = 0;
            Gear = "";
            Status = 0;
            SIP_C_102 = "";
            SIP_C_103 = "";
            SIP_C_104 = "";
            SIP_C_105 = "";
            SIP_C_106 = "";
            SIP_C_293 = "";
            SIP_C_107 = "";
            SIP_C_108 = "";
            SIP_C_303 = "";
            SIP_C_112 = 0;
            SIP_C_294 = "";
            SIP_C_113 = "";
            SIP_C_114 = "";
            SIP_C_304 = 0;
            SIP_C_115 = "";
            SIP_C_116 = "";
            SIP_C_295 = 0;
            SIP_C_117 = 0;
            SIP_C_118 = 0;
            SIP_C_119 = 0;
            SIP_C_120 = 0;
            SIP_C_121 = 0;
            SIP_C_122 = 0;
            SIP_C_123 = 0;
            SIP_C_124 = "";
            SIP_C_125 = 0;
            SIP_C_126 = 0;
            SIP_C_127 = 0;
            SIP_C_128 = 0;
            SIP_C_129 = 0;
            SIP_C_130 = 0;
            SIP_C_131 = 0;
            SIP_C_132 = 0;
            SIP_C_133 = "";
            SIP_C_134 = "";
            SIP_C_135 = "";
            SIP_C_136 = "";
            SIP_C_137 = 0;
            SIP_C_138 = "";
            SIP_C_139 = 0;
            SIP_C_140 = "";
            SIP_C_141 = 0;
            SIP_C_142 = "";
            SIP_C_143 = 0;
            SIP_C_297 = 0;
            SIP_C_298 = "";
            SIP_C_299 = "";
            SIP_C_148 = "";
            SIP_C_305 = 0;
            SIP_C_306 = 0;
            SIP_C_307 = 0;
            SIP_C_308 = 0;
            SIP_C_309 = 0;
            SIP_C_310 = 0;
            SIP_C_311 = 0;
            SIP_C_149 = "";
            SIP_C_150 = "";
            SIP_C_151 = "";
            SIP_C_152 = "";
            SIP_C_153 = "";
            SIP_C_154 = "";
            SIP_C_155 = "";
            SIP_C_156 = "";
            SIP_C_157 = 0;
            SIP_C_158 = "";
            SIP_C_159 = "";
            SIP_C_160 = "";
            SIP_C_161 = "";
            SIP_C_162 = "";
            SIP_C_163 = "";
            SIP_C_164 = "";
            SIP_C_165 = "";
            SIP_C_166 = "";
            SIP_C_167 = "";
            SIP_C_168 = "";
            SIP_C_169 = "";
            SIP_C_170 = 0;
            SIP_C_171 = "";
            SIP_C_172 = 0;
            SIP_C_173 = 0;
            SIP_C_174 = "";
            SIP_C_177 = 0;
            SIP_C_178 = 0;
            SIP_C_179 = 0;
            SIP_C_180 = 0;
            SIP_C_181 = 0;
            SIP_C_183 = 0;
            SIP_C_184 = 0;
            SIP_C_185 = 0;
            SIP_C_186 = 0;
            SIP_C_187 = 0;
            SIP_C_188 = 0;
            SIP_C_189 = 0;
            SIP_C_190 = 0;
            SIP_C_191 = 0;
            SIP_C_192 = 0;
            SIP_C_193 = 0;
            SIP_C_194 = 0;
            SIP_C_195 = 0;
            SIP_C_196 = 0;
            SIP_C_197 = 0;
            SIP_C_198 = 0;
            SIP_C_199 = 0;
            SIP_C_204 = 0;
            SIP_C_205 = 0;
            SIP_C_312 = 0;
            SIP_C_313 = "";
            SIP_C_314 = "";
            SIP_C_315 = "";
            SIP_C_316 = "";
            SIP_C_210 = 0;
            SIP_C_211 = 0;
            SIP_C_212 = 0;
            SIP_C_300 = 0;
            SIP_C_213 = 0;
            SIP_C_214 = 0;
            SIP_C_215 = 0;
            SIP_C_216 = 0;
            SIP_C_217 = 0;
            SIP_C_218 = 0;
            SIP_C_175 = 0;
            SIP_C_176 = 0;
            SIP_C_219 = 0;
            SIP_C_220 = 0;
            SIP_C_200 = 0;
            SIP_C_201 = 0;
            SIP_C_202 = 0;
            SIP_C_203 = 0;
            SIP_C_221 = 0;
            SIP_C_222 = 0;
            SIP_C_223 = 0;
            SIP_C_301 = 0;
            SIP_C_224 = 0;
            SIP_C_225 = 0;
            SIP_C_226 = 0;
            SIP_C_227 = 0;
            SIP_C_228 = 0;
            SIP_C_229 = 0;
            SIP_C_230 = 0;
            SIP_C_317 = 0;
            SIP_C_233 = 0;
            SIP_C_234 = 0;
            SIP_C_235 = 0;
            SIP_C_236 = 0;
            SIP_C_237 = 0;
            SIP_C_238 = 0;
            SIP_C_239 = 0;
            SIP_C_240 = 0;
            SIP_C_241 = 0;
            SIP_C_242 = 0;
            SIP_C_321 = "";
            SIP_C_247 = 0;
            SIP_C_248 = 0;
            SIP_C_249 = 0;
            SIP_C_250 = "";
            SIP_C_251 = 0;
            SIP_C_252 = 0;
            SIP_C_253 = 0;
            SIP_C_254 = 0;
            SIP_C_255 = 0;
            SIP_C_256 = 0;
            SIP_C_257 = 0;
            SIP_C_258 = 0;
            SIP_C_318 = "";
            SIP_C_260 = 0;
            SIP_C_261 = 0;
            SIP_C_262 = 0;
            SIP_C_263 = 0;
            SIP_C_264 = "";
            SIP_C_265 = 0;
            SIP_C_266 = 0;
            SIP_C_267 = 0;
            SIP_C_268 = 0;
            SIP_C_269 = 0;
            SIP_C_270 = 0;
            SIP_C_271 = 0;
            SIP_C_277 = 0;
            SIP_C_278 = 0;
            SIP_C_279 = 0;
            SIP_C_282 = 0;
            SIP_C_319 = "";
            SIP_C_272 = 0;
            SIP_C_273 = 0;
            SIP_C_274 = "";
            SIP_C_275 = 0;
            SIP_C_276 = 0;
            SIP_C_320 = "";
            SIP_C_285 = 0;
            SIP_C_286 = 0;
            SIP_C_287 = 0;
            SIP_C_288 = 0;
            SIP_C_289 = 0;
            SIP_C_290 = 0;
            SIP_C_291 = "";
            SIP_C_292 = "";
        }

        #endregion
    }
}
