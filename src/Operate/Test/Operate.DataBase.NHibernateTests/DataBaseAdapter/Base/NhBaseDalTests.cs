﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter;

namespace Operate.DataBase.NHibernateTests.DataBaseAdapter.Base
{
    [TestClass()]
    public class NhBaseDalTests
    {
        [TestMethod()]
        public void InsertTest()
        {
            var sqlsfg = new SqlServerConfigEntity("Data Source=luzhitao;Initial Catalog=Enterprise;User ID=sa;Password=123456;", SqlServerVersion.Server2005OrLater);
            var cfg = NibernateConfig.BuilderDefaultConfig(sqlsfg);
            var dal = new SqlServerBaseDal(cfg, Operate.DataBase.SqlDatabase.NHibernateAssistant.ConstantCollection.ConfigFileType.ConfigContent);
            long p, s;
            var d = dal.GetList(1, 15, out p, out s, new List<object> {1},
                ConstantCollection.ListMode.Exclude);
        }

        [TestMethod()]
        public void InsertTest1()
        {

        }

        [TestMethod()]
        public void UpdateTest()
        {

        }

        [TestMethod()]
        public void DeleteTest()
        {

        }

        [TestMethod()]
        public void DeleteByWhereTest()
        {

        }

        [TestMethod()]
        public void GetByPrimarykeyTest()
        {

        }

        [TestMethod()]
        public void GetListTest()
        {

        }

        [TestMethod()]
        public void GetCountTest()
        {

        }

        [TestMethod()]
        public void ExecuteSqlScalarTest()
        {

        }

        [TestMethod()]
        public void ExecuteSqlNonQueryTest()
        {

        }

        [TestMethod()]
        public void ExecuteProcessNonQueryTest()
        {

        }

        [TestMethod()]
        public void ExecuteSqlToDatasetTest()
        {

        }

        [TestMethod()]
        public void ExecuteProcessToDatasetTest()
        {

        }

        [TestMethod()]
        public void ExecuteProcessScalarTest()
        {

        }
    }
}
