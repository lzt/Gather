﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using Operate.DataBase.SqlDatabase.Attribute;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.NHibernateTests.DataBaseAdapter.Model
{
    /// <summary>
    /// UserInfo实体字段名称
    /// </summary>
    public enum UserInfoField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：用户标识
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserProfileId,  
        
        /// <summary>
        /// 字段：Zipcode
        /// 描述：邮政编码
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        Zipcode,  
        
        /// <summary>
        /// 字段：Address
        /// 描述：地址
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：400
        /// 是否可空：True
        /// </summary>
        Address,  
        
        /// <summary>
        /// 字段：ProvinceId
        /// 描述：省Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        ProvinceId,  
        
        /// <summary>
        /// 字段：CityId
        /// 描述：市Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        CityId,  
        
        /// <summary>
        /// 字段：TownId
        /// 描述：区域Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        TownId,  
        
    }

    /// <summary>
    /// UserInfo实体模型
    /// </summary>
    [Serializable]
    public partial class UserInfo : BaseEntity
	{
        #region UserInfo Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                                
                                
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：用户标识
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("用户标识")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserProfileId {get; set;} 
                        
        /// <summary>
        /// 字段：Zipcode
        /// 描述：邮政编码
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(20)
        /// 实际占用长度：20
        /// 是否可空：True
        /// </summary>
        [Length("20")]
        [Comment("邮政编码")]
        [SqlType("nvarchar(20)")]
        [Precision("")]
        [Scale("")]
        public virtual String Zipcode {get; set;} 
                        
        /// <summary>
        /// 字段：Address
        /// 描述：地址
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：400
        /// 是否可空：True
        /// </summary>
        [Length("400")]
        [Comment("地址")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual String Address {get; set;} 
                        
        /// <summary>
        /// 字段：ProvinceId
        /// 描述：省Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("省Id")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 ProvinceId {get; set;} 
                        
        /// <summary>
        /// 字段：CityId
        /// 描述：市Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("市Id")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 CityId {get; set;} 
                        
        /// <summary>
        /// 字段：TownId
        /// 描述：区域Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("区域Id")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 TownId {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static UserInfo()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public UserInfo()
        {
            //属性初始化
            UserProfileId = 0;
            Zipcode = "";
            Address = "";
            ProvinceId = 0;
            CityId = 0;
            TownId = 0;
        }

        #endregion

    }
}