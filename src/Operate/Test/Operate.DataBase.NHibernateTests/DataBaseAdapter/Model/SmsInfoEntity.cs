﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using Operate.DataBase.SqlDatabase.Attribute;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.NHibernateTests.DataBaseAdapter.Model
{
    /// <summary>
    /// SmsInfoEntity实体字段名称
    /// </summary>
    public enum SmsInfoEntityField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：账户Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserProfileId,  
        
        /// <summary>
        /// 字段：Content
        /// 描述：短信内容
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：1000
        /// 是否可空：True
        /// </summary>
        Content,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：创建时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：Phone
        /// 描述：发送对象手机号码
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(22)
        /// 实际占用长度：22
        /// 是否可空：True
        /// </summary>
        Phone,  
        
        /// <summary>
        /// 字段：StatusSuccess
        /// 描述：是否成功发送
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        StatusSuccess,  
        
    }

    /// <summary>
    /// SmsInfoEntity实体模型
    /// </summary>
    [Serializable]
    public partial class SmsInfoEntity : BaseEntity
	{
        #region SmsInfoEntity Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                                                                                                
                                
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：账户Id
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("账户Id")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserProfileId {get; set;} 
                        
        /// <summary>
        /// 字段：Content
        /// 描述：短信内容
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：1000
        /// 是否可空：True
        /// </summary>
        [Length("1000")]
        [Comment("短信内容")]
        [SqlType("nvarchar(1000)")]
        [Precision("")]
        [Scale("")]
        public virtual String Content {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：创建时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("创建时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：Phone
        /// 描述：发送对象手机号码
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(22)
        /// 实际占用长度：22
        /// 是否可空：True
        /// </summary>
        [Length("22")]
        [Comment("发送对象手机号码")]
        [SqlType("nvarchar(22)")]
        [Precision("")]
        [Scale("")]
        public virtual String Phone {get; set;} 
                        
        /// <summary>
        /// 字段：StatusSuccess
        /// 描述：是否成功发送
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("是否成功发送")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 StatusSuccess {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static SmsInfoEntity()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public SmsInfoEntity()
        {
            //属性初始化
            UserProfileId = 0;
            Content = "";
            CreateTime = Operate.ConstantCollection.DbDefaultDateTime;
            Phone = "";
            StatusSuccess = 0;
        }

        #endregion

    }
}