﻿using System;
using System.ComponentModel.DataAnnotations;
using Operate.DataBase.SqlDatabase.Attribute;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.NHibernateTests.DataBaseAdapter.Model
{
    /// <summary>
    /// Car实体字段名称
    /// </summary>
    public enum CarField
    {

        /// <summary>
        /// 字段：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,

        /// <summary>
        /// 字段：CarName
        /// 描述：车名字
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        CarName,

        /// <summary>
        /// 字段：CountryId
        /// 描述：地市ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        CountryId,

        /// <summary>
        /// 字段：Status
        /// 描述：状态1:可租0:暂不可租2:已经被租用
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        Status,

        /// <summary>
        /// 字段：CreateTime
        /// 描述：添加时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        CreateTime,

        /// <summary>
        /// 字段：LevelId
        /// 描述：级别ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        LevelId,

        /// <summary>
        /// 字段：BrandId
        /// 描述：品牌ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        BrandId,

        /// <summary>
        /// 字段：Price
        /// 描述：每日租金
        /// 映射类型：Decimal
        /// 数据库中字段类型：decimal
        /// 实际占用长度：9
        /// 是否可空：True
        /// </summary>
        Price,

        /// <summary>
        /// 字段：Content
        /// 描述：详情
        /// 映射类型：String
        /// 数据库中字段类型：ntext
        /// 实际占用长度：16
        /// 是否可空：True
        /// </summary>
        Content,

        /// <summary>
        /// 字段：Introduction
        /// 描述：简介
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：1000
        /// 是否可空：True
        /// </summary>
        Introduction,

        /// <summary>
        /// 字段：UserId
        /// 描述：车辆所有者ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        UserId,

        /// <summary>
        /// 字段：StartTime
        /// 描述：车辆可租开始时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        StartTime,

        /// <summary>
        /// 字段：EndTime
        /// 描述：车辆可租结束时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        EndTime,

        /// <summary>
        /// 字段：RentTime
        /// 描述：车辆租用时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        RentTime,

        /// <summary>
        /// 字段：ReturnTime
        /// 描述：车辆归还时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        ReturnTime,

        /// <summary>
        /// 字段：Images
        /// 描述：车辆图片
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：1000
        /// 是否可空：True
        /// </summary>
        Images,

        /// <summary>
        /// 字段：Monthly
        /// 描述：月租金
        /// 映射类型：Decimal
        /// 数据库中字段类型：decimal
        /// 实际占用长度：9
        /// 是否可空：True
        /// </summary>
        Monthly,

    }

    /// <summary>
    /// Car实体模型
    /// </summary>
    [Serializable]
    public partial class Car : BaseEntity
    {
        #region Car Properties

        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id { get; set; }


        /// <summary>
        /// 字段：CarName
        /// 描述：车名字
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：200
        /// 是否可空：True
        /// </summary>
        [Length("200")]
        [Comment("车名字")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual String CarName { get; set; }

        /// <summary>
        /// 字段：CountryId
        /// 描述：地市ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("地市ID")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 CountryId { get; set; }

        /// <summary>
        /// 字段：Status
        /// 描述：状态1:可租0:暂不可租2:已经被租用
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("状态1:可租0:暂不可租2:已经被租用")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 Status { get; set; }

        /// <summary>
        /// 字段：CreateTime
        /// 描述：添加时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("添加时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime CreateTime { get; set; }

        /// <summary>
        /// 字段：LevelId
        /// 描述：级别ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("级别ID")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 LevelId { get; set; }

        /// <summary>
        /// 字段：BrandId
        /// 描述：品牌ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("品牌ID")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 BrandId { get; set; }

        /// <summary>
        /// 字段：Price
        /// 描述：每日租金
        /// 映射类型：Decimal
        /// 数据库中字段类型：decimal
        /// 实际占用长度：9
        /// 是否可空：True
        /// </summary>
        [Length("9")]
        [Comment("每日租金")]
        [SqlType("decimal")]
        [Precision("18")]
        [Scale("2")]
        public virtual Decimal Price { get; set; }

        /// <summary>
        /// 字段：Content
        /// 描述：详情
        /// 映射类型：String
        /// 数据库中字段类型：ntext
        /// 实际占用长度：16
        /// 是否可空：True
        /// </summary>
        [Length("16")]
        [Comment("详情")]
        [SqlType("ntext")]
        [Precision("")]
        [Scale("")]
        public virtual String Content { get; set; }

        /// <summary>
        /// 字段：Introduction
        /// 描述：简介
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：1000
        /// 是否可空：True
        /// </summary>
        [Length("1000")]
        [Comment("简介")]
        [SqlType("nvarchar(1000)")]
        [Precision("")]
        [Scale("")]
        public virtual String Introduction { get; set; }

        /// <summary>
        /// 字段：UserId
        /// 描述：车辆所有者ID
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：True
        /// </summary>
        [Length("4")]
        [Comment("车辆所有者ID")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        public virtual Int32 UserId { get; set; }

        /// <summary>
        /// 字段：StartTime
        /// 描述：车辆可租开始时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("车辆可租开始时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime StartTime { get; set; }

        /// <summary>
        /// 字段：EndTime
        /// 描述：车辆可租结束时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("车辆可租结束时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime EndTime { get; set; }

        /// <summary>
        /// 字段：RentTime
        /// 描述：车辆租用时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("车辆租用时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime RentTime { get; set; }

        /// <summary>
        /// 字段：ReturnTime
        /// 描述：车辆归还时间
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：8
        /// 是否可空：True
        /// </summary>
        [Length("8")]
        [Comment("车辆归还时间")]
        [SqlType("datetime")]
        [Precision("23")]
        [Scale("3")]
        public virtual DateTime ReturnTime { get; set; }

        /// <summary>
        /// 字段：Images
        /// 描述：车辆图片
        /// 映射类型：String
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：1000
        /// 是否可空：True
        /// </summary>
        [Length("1000")]
        [Comment("车辆图片")]
        [SqlType("nvarchar(1000)")]
        [Precision("")]
        [Scale("")]
        public virtual String Images { get; set; }

        /// <summary>
        /// 字段：Monthly
        /// 描述：月租金
        /// 映射类型：Decimal
        /// 数据库中字段类型：decimal
        /// 实际占用长度：9
        /// 是否可空：True
        /// </summary>
        [Length("9")]
        [Comment("月租金")]
        [SqlType("decimal")]
        [Precision("18")]
        [Scale("2")]
        public virtual Decimal Monthly { get; set; }

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Car()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Car()
        {
            //属性初始化
            CarName = "";
            CountryId = 0;
            Status = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LevelId = 0;
            BrandId = 0;
            Content = "";
            Introduction = "";
            UserId = 0;
            StartTime = ConstantCollection.DbDefaultDateTime;
            EndTime = ConstantCollection.DbDefaultDateTime;
            RentTime = ConstantCollection.DbDefaultDateTime;
            ReturnTime = ConstantCollection.DbDefaultDateTime;
            Images = "";
        }

        #endregion

        #region Function


        #endregion
    }
}
