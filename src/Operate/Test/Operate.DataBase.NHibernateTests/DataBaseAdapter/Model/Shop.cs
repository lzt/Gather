﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate.DataBase.SqlDatabase.Attribute;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.NHibernateTests.DataBaseAdapter.Model
{
    /// <summary>
    /// Shop实体字段名称
    /// </summary>
    public enum ShopField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        Id,  
        
    }

    /// <summary>
    /// Shop实体模型
    /// </summary>
    [Serializable]
    public partial class Shop : BaseEntity
	{
        #region Shop Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：自增主键
        /// 映射类型：Int32
        /// 数据库中字段类型：int
        /// 实际占用长度：4
        /// 是否可空：False
        /// </summary>
        [Length("4")]
        [Comment("自增主键")]
        [SqlType("int")]
        [Precision("10")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual Int32 Id {get; set;} 
                
                        
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Shop()
        {
            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Shop()
        {
            //属性初始化
        }

        #endregion

		
    }
}