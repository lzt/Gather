﻿using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NHibernate.Cfg.ConfigurationSchema;
using Operate.DataBase.SqlDatabase;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter;

namespace Operate.DataBase.NHibernateTests.DataBaseAdapter
{
    [TestClass()]
    public class SqliteBaseDalTests
    {
        [TestMethod()]
        public void SqliteBaseDalTest()
        {


            //var file = "E:\\project\\Git\\resource\\CodeFactory\\src\\ui\\CodeFactory\\bin\\Debug\\buildresult\\NHibernate\\Grrb.Model\\mapping\\newsinfo.hbm.xml";

            //var mc = new MappingConfiguration(file);

            var c = "data source=E:\\Git\\Src\\CodeFactory\\Database\\Sqlite\\SqliteSimple.db;FailIfMissing=false";
            var configEntity = new SqliteConfigEntity(c, SqliteVersion.Sqlite);
            var dal = new SqliteBaseDal(NibernateConfig.BuilderDefaultConfig(configEntity), Operate.DataBase.SqlDatabase.NHibernateAssistant.ConstantCollection.ConfigFileType.ConfigContent);

            var dbname = dal.Database;

            var d=dal.ExecuteSqlToDatatable("select name from sqlite_master where type='table' order by name");

            return;
            var pList = JsonConvert.DeserializeObject<List<string>>("[\"{\\\"DbType\\\":0,\\\"ParameterName\\\":\\\"@parentserialNumber\\\",\\\"Value\\\":\\\"20141127150612636077\\\",\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":6,\\\"ParameterName\\\":\\\"@createTime\\\",\\\"Value\\\":\\\"2014-11-27T15:06:12.6177965+08:00\\\",\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@userProfileId\\\",\\\"Value\\\":1171,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@parentStatus\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@carid\\\",\\\"Value\\\":2103,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":8,\\\"ParameterName\\\":\\\"@totalPrice\\\",\\\"Value\\\":421.0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":0,\\\"ParameterName\\\":\\\"@serialNumber\\\",\\\"Value\\\":\\\"20141127150612636077\\\",\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@payType\\\",\\\"Value\\\":1,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":6,\\\"ParameterName\\\":\\\"@startTime\\\",\\\"Value\\\":\\\"2014-11-29T08:00:00\\\",\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":6,\\\"ParameterName\\\":\\\"@endTime\\\",\\\"Value\\\":\\\"2014-11-30T15:00:00\\\",\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@rentType\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@payment\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@implement\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@effect\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@provideraccept\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@illegalPayment\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@buyerTake\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@buyerReturn\\\",\\\"Value\\\":0,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\",\"{\\\"DbType\\\":11,\\\"ParameterName\\\":\\\"@serviceType\\\",\\\"Value\\\":1,\\\"Direction\\\":1,\\\"IsNullable\\\":false,\\\"SourceColumn\\\":\\\"\\\",\\\"SourceVersion\\\":512}\"]");
            var paramList = new List<IDataParameter>();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var p in pList)
            {
                var pdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(p);
                if (pdic != null)
                {
                    paramList.Add((DataBaseParamConvert.ToDatabaseParam(pdic, dal.CreateNewParam())));
                }
            }
          var sd=  dal.ExecuteProcessToDatatable("Pro_AddOrder", paramList.ToArray());
            // var cfg = dal.NhibernateHelper.GetCfg();
            // cfg.AddFile(mc.File);

            //var accessConfigEntity = new Config.Entity.AccessConfigEntity("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CMSDB.mdb"));
            //var dal = new AccessBaseDal(NibernateConfig.BuilderDefaultConfig(accessConfigEntity), ConstantCollection.ConfigFileType.ConfigContent);
            //var list = dal.ExecuteSqlToDatatable("select * from article");
        }
    }
}
