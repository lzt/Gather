﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Operate;
using Operate.DataBase.NoSqlDataBase.Memcached;
using Operate.DataBase.NoSqlDataBase.RedisAssistant.StactkExchangeRedis;
using StackExchange.Redis;
using System.Xml;
using Newtonsoft.Json.Linq;
using Operate.Encryption.ExtensionMethods;
using Operate.Reflection;
using Operate.Reflection.ExtensionMethods;
using Operate.Web.Media;
using Operate.ExtensionMethods;
using System.Drawing;
using Operate.Media.Image.ExtensionMethods;
using Operate.Web.ExtensionMethods;
using System.Drawing.Drawing2D;
using Operate.QRcode;

namespace ConsoleTest
{
    internal class Program
    {
        private class B
        {
            public int VB { get; set; }
        }

        private class A : B
        {
            public int V { get; set; }
        }

        private static void Main(string[] args)
        {
            Console.WriteLine(1.In(1, 2, 3));

            //var u = "http://www.a.com?c=1";

            //u = u.UpdateUrlParam("c", "weq");
            //u = u.RemoveUrlParam("c");
            Console.ReadKey();
        }

        private static void TestImage()
        {
            var imageSource = Image.FromFile("C:\\Users\\kitya\\Pictures\\images\\QQ图片20171228153523.png");
            var bt = new Bitmap(imageSource);
            var imageBg = new Bitmap(imageSource.Width, imageSource.Height + 100);
            imageBg.BlackAndWhite();

            //Graphics graphics = Graphics.FromImage((Image)imageBg);
            //graphics.Clear(Color.White);
            //graphics.SmoothingMode = SmoothingMode.Default;
            //graphics.InterpolationMode = InterpolationMode.Default;
            //graphics.DrawImage(imageSource, new Rectangle(0, 0, imageSource.Width, imageSource.Height), 0, 0, imageSource.Width, imageSource.Height, GraphicsUnit.Pixel);

            var url = "http://www.baidu.com/wewr/wetwt/er";

            var qrimg = QRcodeAssistant.Create(url, 20);
            var qrimgbt = new Bitmap(qrimg);
            qrimgbt = qrimgbt.Resize(100);

            var g = Graphics.FromImage(imageSource);
            g.SmoothingMode = SmoothingMode.Default;
            g.InterpolationMode = InterpolationMode.Default;
            g.DrawImage(qrimgbt, imageSource.Width - qrimgbt.Width, imageSource.Height - qrimgbt.Height, qrimgbt.Width, qrimgbt.Height);

            g = Graphics.FromImage(imageBg);
            g.Clear(Color.White);
            g.SmoothingMode = SmoothingMode.Default;
            g.InterpolationMode = InterpolationMode.Default;
            g.DrawImage(imageSource, 0, 0, imageSource.Width, imageSource.Height);

            //var r1 = QRcodeAssistant.CombinImage(qrimgbt, imageSource);
            //var r = QRcodeAssistant.CombinImage(imageBg, r1);

            //imageBg = imageBg.DrawText("亲爱的朋友", new Font("微软雅黑", 12), new SolidBrush(Color.FromArgb(156, 0, 0, 0)), RectangleF.Empty);
            //g = Graphics.FromImage(imageBg);

            g.DrawString("亲爱的朋友\r\n       在此新春佳节之际，恭祝您身体健康，事业顺利，家和美满，\r\n万事大吉。\r\n                                                                      您的好友：张三", new Font("微软雅黑", 12), new SolidBrush(Color.FromArgb(156, 0, 0, 0)), 10, imageSource.Height + 10, new StringFormat());

            //qrimg.Save("C:\\Users\\kitya\\Pictures\\images\\" + Guid.NewGuid().ToString() + ".png");
            //var b3 = imageBg.And(bt);
            imageBg.Save("C:\\Users\\kitya\\Pictures\\images\\" + Guid.NewGuid().ToString() + ".png");
        }

        private static void Test2()
        {
            var data = "[{'v':'11'},{'v':'21'}]";

            var list = JsonConvert.DeserializeObject<List<JObject>>(data);

            foreach (var item in list)
            {
                var a = new A();
                item.CopyValueTo(ref a, true);
                var ss = a.V;
            }

            //var hash = "15617618716";

            //var md5 = hash.ToMd5();

            //var ds = Operate.Encryption.RSA.BouncyCastle.RSAEncryptionByBC.EncryptByPrivateKey(md5, "E:\\pkcs8_private_key.pem", true);
            ////var fss = ds.Base64Decode();
            //Console.WriteLine(ds);

            //var sd = Operate.Encryption.RSA.BouncyCastle.RSAEncryptionByBC.DecryptByPrivateKey(ds, "E:\\pkcs8_private_key.pem", true);

            //ds = Operate.Encryption.RSA.BouncyCastle.RSAEncryptionByBC.EncryptByPrivateKey(hash, "E:\\pkcs8_private_key.pem", true);
            //var fss = ds.Base64Decode();

            //sd = Operate.Encryption.RSA.BouncyCastle.RSAEncryptionByBC.DecryptByPublicKey(ds, "E:\\rsa_public_key.pem", true);

            //var d = Operate.Encryption.RSA.BouncyCastle.RSASignByBC.SignHash(hash, "E:\\pkcs8_private_key.pem", true, true);
            //Console.WriteLine(d);
            Console.ReadKey();
            return;

            using (var iPSearch = new QQIPSearch("D:\\IpQQ\\qqwry.dat"))
            {
                var ip = "42.236.187.89";
                var loc = iPSearch.SearchIPLocation(ip);
                //Console.WriteLine(c.Country, c.Area);
                Console.WriteLine("你查的ip是：{0} 地理位置：{1} {2}", ip, loc.Country, loc.Area);
                return;
            }

            var css = new List<CombineSource>();

            css.Add(new CombineSource()
            {
                Address = "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3134491097,4286019618&fm=23&gp=0.jpg",
                AddressType = SourceAddressType.WebUrl
            });

            css.Add(new CombineSource()
            {
                Address = "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3872985097,3875253203&fm=23&gp=0.jpg",
                AddressType = SourceAddressType.WebUrl
            });

            css.Add(new CombineSource()
            {
                Address = "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2283473828,974189058&fm=23&gp=0.jpg",
                AddressType = SourceAddressType.WebUrl
            });

            css.Add(new CombineSource
            {
                Address = "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1737638726,3536952377&fm=23&gp=0.jpg",
                AddressType = SourceAddressType.WebUrl
            });

            css.Add(new CombineSource
            {
                Address = "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3414112143,2270961746&fm=23&gp=0.jpg",
                AddressType = SourceAddressType.WebUrl
            });

            Combine.CombineImages(css.ToArray(), 100, 100, "D:\\temp\\imgs\\" + Guid.NewGuid().ToString() + ".jpg");
            return;

            //var a = new A();
            //dynamic d = a.ToExpandoObject();

            //var s = d.V;
            //return;

            //var ssd = Operate.Configuration.JsonConfigSet.ConfigManager.GetConfigCollection();

            //var a=new A();
            //var nn = Reflection.GetClassAndPropertyName(() => a.VB);

            //return;

            var xml = "<row><Id>3</Id><EnterpriserId>3</EnterpriserId><Name>攻城练习</Name><Description>练习攻城各项事项</Description><Price>11.0000</Price><ScheduleTime>周一至周五</ScheduleTime><DurationTime>2小时</DurationTime><Content>&lt;p&gt;wagtsdgfasdg&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</Content><Considerations>自备攻城工具</Considerations><Place>线下服务</Place><Recommend>1</Recommend><AdminRemark></AdminRemark><IP>42.236.188.41</IP><Status>2</Status><Remark>更新信息</Remark><CreateTime>2016-08-24T19:25:14.867</CreateTime><CreateUserId>-10</CreateUserId><UpdateTime>2016-08-26T15:21:15.507</UpdateTime><UpdateUserId>-10</UpdateUserId></row>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            string json = JsonConvert.SerializeXmlNode(doc);
            dynamic dowdata = JsonConvert.DeserializeObject(json);

            if (dowdata != null)
            {
                if (dowdata.row != null)
                {
                    var o = dowdata.row;
                    //Newtonsoft.Json.Linq.JToken
                    var json2 = JsonConvertEx.SerializeAndKeyToLower(o);
                    JToken productSnapshot = JsonConvert.DeserializeObject<JToken>(json2);
                    //productSnapshot.
                }
            }
        }

        private static void Test1()
        {
            //MemcachedManager.Serverlist = new Dictionary<string, int>();

            //MemcachedManager.Serverlist.Add("127.0.0.1", 11211);
            //var cache = MemcachedAssistant.Instance;

            RedisManager.WriteHosts = new List<string>() { "127.0.0.1:6379" };
            var cache = StactkExchangeRedisAssistant.Instance;

            var r = new Random();

            var k = Guid.NewGuid().ToString();

            //var rr = cache.Set(k, new A() { V = r.Next() },DateTime.Now.AddDays(50));

            var a = new A() { V = r.Next() };
            var json = JsonConvert.SerializeObject(a);
            var b = JsonConvert.DeserializeObject<A>(json);

            var rr = cache.Set(k, a);

            var g = cache.Get<A>(k);

            //RedisManager.WriteHosts = new List<string>() { "127.0.0.1" };
            //var cache = StactkExchangeRedisAssistant.Instance;

            //var r = new Random();

            //var k = Guid.NewGuid().ToString();

            //var v = new RedisValue();

            //cache.Set(k, r.Next());

            //var c = cache.GetCount();
            //var item = cache.Get<object>(k);

            ////var ctx = new Context();
            ////using (var reqSocket = ctx.CreateSocket(SocketType.Req))
            //////using (var repSocket = ctx.CreateSocket(SocketType.Rep))
            ////{
            ////    //repSocket.Bind("tcp://0.0.0.0:90002");

            ////    reqSocket.Connect("tcp://127.0.0.1:90002");

            ////    for (int i = 0; i < 100; i++)
            ////    {
            ////        var s = i.ToString(CultureInfo.InvariantCulture);
            ////        reqSocket.Send(s);
            ////        Console.WriteLine("request:{0}".FormatValue(s));
            ////    }

            ////    //var msg = repSocket.Recv();
            ////    //var msgStr = Encoding.UTF8.GetString(msg);

            ////    //repSocket.Send(MsgReply);

            ////    var msg = reqSocket.Recv();
            ////    //msgStr = Encoding.UTF8.GetString(msg);
            ////    Console.WriteLine(Encoding.UTF8.GetString(msg));
            ////}

            ////var r = ;
            //using (var zmq = new ZeroMqAssistant("tcp://127.0.0.1:90002"))
            //{
            //    for (int i = 0; i < 100; i++)
            //    {
            //        var s = i.ToString(CultureInfo.InvariantCulture);

            //        var polling = new Polling(PollingEvents.RecvReady, zmq.MqSocket);
            //        zmq.MqSocket.Send(s);
            //        zmq.MqSocket.Recv();
            //        //if (polling.Poll(2000))
            //        //{
            //        //    Console.WriteLine("request:{0}".FormatValue("time out"));
            //        //}
            //        //else
            //        //{
            //        //      zmq.MqSocket.Recv();
            //        //}

            //        Console.WriteLine("request:{0}".FormatValue(s));
            //    }
            //}

            //Console.ReadKey();
        }
    }
}