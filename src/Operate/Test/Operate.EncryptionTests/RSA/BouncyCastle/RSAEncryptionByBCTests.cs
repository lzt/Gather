﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.Encryption.RSA.BouncyCastle;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.Encryption.RSA.BouncyCastle.Tests
{
    [TestClass()]
    public class RSAEncryptionByBCTests
    {
        [TestMethod()]
        public void SignTest()
        {
            var k =
                "MIICWwIBAAKBgQC+4tpARNudgXhh3cM+3/7F31OKCeiQmByFumUnlUs4Lu4rL65+v2v/mteLm/sKpCKP3TubFDMaM9pIMjvEP5tdLcO8VKyFDhpklsrF+BnhGSjqs4gY+3Ckycyg3BrePFOvnkCfD6O5BsCi9/xUFOqagciAdqAjGyi/e07cYbNiUQIDAQABAoGAWaJAhelPjtha5OmzYvTft560pZ8de7hRvGHYWgv6tDklUVYkhBPy7KWTDVWDPm3wZMmn0ZxcmrM6jqg39GFB3F/56BjCF9baYAAEYnHisNvlJNgY/vHilpB684xyfDnWc5IBlUP1n0IFAAR/SXtCG6fcA7dlQrBKEmh5CPYuOpkCQQD63nMVLsKz6j7zjoEfveQ9Ed3QZUp14f7rSH0VrXS00fnGhQ643bHPOJIhcrwRV5JfTxvLcUQd/7vK8fh/fZyvAkEAwspVOK9Zse6a6Vvkj+kZozkq5A1m/xBuxhix7iQAmCv3Sl8doLPu5V+vusaA6qvp6993o+eHclCKA9IfROSw/wJAFDQiFIylhZPR6g+J+qJyZTCxKn1d18yRmSVHXZ2QjpdyhDhnDSBdl5C4xBTXbJYe5aobVPEUSU41M4z1NF9LZQJAEWcALBtGWSYnAtgp2E2L89Akq9EitYDjVKFaLBtdTjsHUcajn/rTSfy9d2NKmyyWrnL9flAuAqF9Yw//qSkBQwJAJVFkKkzsAL4ruNrL1ZVBrAZvTkS2C2WtEkU46MBvvpbaW8lxpPwHmn1DrJpASsmidWVXaMDv1ANzlLk7yE0vgw==";
            var kpath = "E:\\project\\Platform\\design\\Document\\rsa_private_key.pem";
            var pubpath = "E:\\project\\Platform\\design\\Document\\rsa_public_key.pem";

            var pub = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+4tpARNudgXhh3cM+3/7F31OKCeiQmByFumUnlUs4Lu4rL65+v2v/mteLm/sKpCKP3TubFDMaM9pIMjvEP5tdLcO8VKyFDhpklsrF+BnhGSjqs4gY+3Ckycyg3BrePFOvnkCfD6O5BsCi9/xUFOqagciAdqAjGyi/e07cYbNiUQIDAQAB";
            var w3e = RSAEncryptionByBC.Encrypt("123456", pubpath, true);
            var b = RSAEncryptionByBC.Decrypt(w3e, kpath, true);
        }

        [TestMethod()]
        public void SignTest1()
        {

        }
    }
}
