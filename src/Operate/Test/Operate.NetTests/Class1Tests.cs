﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods;
using Operate.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.Net.Mime;

namespace Operate.Net.Tests
{
    [TestClass()]
    public class Class1Tests
    {
        public struct TT
        {
            public string A;
        }

        [TestMethod()]
        public void testTest()
        {
            string template = "/// <summary>\r\n/// {0}\r\n/// mime：{3}\r\n/// </summary>\r\npublic static MimeModel {0}~~\r\nget~~return new MimeModel\r\n~~\r\nName=\"{0}\",\r\nExtension=\"{1}\",\r\nContentType=\"{2}\",\r\nAlias = new List<string>(),\r\n!!;\r\n!!\r\n!!\r\n\r\n";
            var filepath = "f:\\contenttype.txt";
            using (var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                var builser = new StringBuilder();
                var sr = new StreamReader(fs);
                var sd = "1";
                while (true)
                {
                    var line = sr.ReadLine();
                    if (line.IsNullOrEmpty())
                    {
                        break;
                    }

                    var array = line.Split('=');

                    var ex = array[0];
                    var name = ex.Replace(".", "").ToUpperFirst();
                    if (name.IsInt32())
                    {
                        name = "Status" + name;
                    }
                 name=   name.Replace("*", "Any");
                    var cont = array[1];
                    builser.Append(template.FormatValue(name, ex, cont,line).Replace("~~", "{").Replace("!!", "}"));
                }
                sr.Close();

          var rte=  "f:\\contenttyperesult.txt";
          rte.WriteFile(builser.ToString());
            }

            //var m = new MimeModel
            //{
            //    Alias = new List<string>(),
            //    ContentType = "",
            //    Extension = "",
            //    Name = "",
            //};
        }
    }
}
