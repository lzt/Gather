﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.Media.Image.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.Media.Image.ExtensionMethods.Test
{
    [TestClass()]
    public class BitmapExtensionsTests
    {
        [TestMethod()]
        public void BlackAndWhiteTest()
        {
            var c = new Captcha();
            string ck;
            c.LetterCount = 5;
            var img = c.GetImage(out ck);
            img.BlackAndWhite("c://ck.png");
            Assert.Fail();
        }
    }
}
