﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.MessageQueue.ZeroMqAssistant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.MessageQueue.ZeroMqAssistant.Tests
{
    [TestClass()]
    public class ZeroMqAssistantTests
    {
        [TestMethod()]
        public void SendTest()
        {
            var r = new ZeroMqAssistant("tcp://127.0.0.1:90002");
            r.Send("qwreqwr");
        }
    }
}
