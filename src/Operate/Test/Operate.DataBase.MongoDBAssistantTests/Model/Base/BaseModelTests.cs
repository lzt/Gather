﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer;

namespace Operate.DataBase.MongoDBAssistantTests.Model.Base
{
    [TestClass()]
    public class BaseModelTests
    {

        string conn = "mongodb://lzt:123456@127.0.0.1/kernel";
        string dbName = "Test";

        [TestMethod()]
        public void ToDictionaryTest()
        {

            var dal = new MongoDBDal<Users>(conn);

            //var insertlist = new List<Users>()
            //{
            //    new Users() {Name = "114511", Sex = "2134asda40", Nickname = "1"},
            //         new Users() {Name = "114511", Sex = "213123123440", Nickname = "1"},
            //};
            //dal.Insert(insertlist);


            var dic = new Dictionary<string, object>()
            {
                {
                    "Name",
                    "114511"
                }
            };

            var listsearch = dal.GetListByTop(1, dic);

            var user = new Users() { Name = "Jon", Sex = "45" };
            ///var list = dal.GetList<Users>();


            dal.Insert(user);

            var first = dal.GetList();
            first[0].Nickname = "updateqwe1";
            dal.Update(first[0]);
        }

        [TestMethod()]
        public void InsertTest()
        {



            //var list = dal.GetFirst<Users>();


            //var model = new Users()
            //{
            //    Name = "textname",
            //    Sex = "16"
            //};

            //dal.Insert<Users>(model);
        }
    }
}
