﻿using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Model.Base;

namespace Operate.DataBase.MongoDBAssistantTests
{
    public class Users : BaseModel
    {
        public string Name { get; set; }
        public string Sex { set; get; }

        public string Nickname { get; set; }

    }
}
