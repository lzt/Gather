﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods;
using Operate.Web.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.Web.ExtensionMethods.Tests
{
    [TestClass()]
    public class StringExtensionsTests
    {
        [TestMethod()]
        public void DeleteUrlParamTest()
        {
            //var url = "http://www.aa.com?sdfs=qweq&code=121213/=sdf&fdg=srfs";
            //var sds = url.DeleteUrlParam("code");
         //var wer=   url.GetUrlParam("Sdfs");


         string username = "kityandhero";//用户名
         string password = "@a198501";//密码
         //新建一个用于保存cookies的容器 
         CookieContainer container = new CookieContainer();
         //拼接post数据
         string postData = ("txtUserName=" + username);
         postData += ("&txtPassword=" + password);
         postData += ("&txtCheckCode=cc");
         postData += ("&RefUrl=" + "http://5sing.kugou.com/37345099/musicbox/1.html".UrlEncode());
         ASCIIEncoding encoding = new ASCIIEncoding();
         byte[] data = encoding.GetBytes(postData);
         HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://5sing.kugou.com/login/");
         request.Method = "Post";
         request.ContentType = "application/x-www-form-urlencoded";
         request.ContentLength = data.Length;
         request.KeepAlive = true;
         request.CookieContainer = container; //返回的cookie会附加在这个容器里面
         //发送数据
         Stream newStream = request.GetRequestStream();
         newStream.Write(data, 0, data.Length);
         newStream.Close();
         //以下俩句不可缺少
         HttpWebResponse response = (HttpWebResponse)request.GetResponse();
         response.Cookies = container.GetCookies(request.RequestUri);
         var html = response.GetResponseStream().ReadAll();


         var htmldoc = new HtmlAgilityPack.HtmlDocument();
         htmldoc.LoadHtml(html);
         //var nodelist = htmldoc.DocumentNode.SelectNodes("/html/body/div/div/div/ul/li/");
         var nodelist = htmldoc.DocumentNode.SelectNodes("/html/body/div/div/div/ul/li/span/a[@class='action_down']");
         foreach (var ael in nodelist)
         {
             var href = ael.Attributes["href"].Value;
             var id = href.Remove(0, href.LastIndexOf("/") + 1);
             var url = "http://5sing.kugou.com/yc/DownFile.aspx?SongID=2524586&SongType=yc";
             if (href.Contains("/fc/"))
             {
                 url = url.Replace("yc", "fc");
             }
             url = url.UpdateUrlParam("SongID", id);
             //url.Download();
             url.Download("f:\\{0}.mp3".FormatValue(id), container);
             HttpWebRequest singpagerequest = (HttpWebRequest)WebRequest.Create(url);
             singpagerequest.CookieContainer = container; //返回的cookie会附加在这个容器里面
             HttpWebResponse singpageresponse = (HttpWebResponse)singpagerequest.GetResponse();
             singpageresponse.Cookies = container.GetCookies(singpagerequest.RequestUri);
             var sd = singpageresponse.GetResponseStream();
             //sd.Write()
             var html1 = sd.ReadAll();
         }

        }

        [TestMethod()]
        public void ConvertToCookieCollectionTest()
        {
            var ss =
                "__RequestVerificationToken=sZoQpQE276s_AxdJV4WjAlQ9GtPpJcJY8EIDa6Ikl5LwH7-7TAQ2tJxyOqW05M_czCQDPuPlvf_g37S5xmfNqTBC2pWUyiP6fKdXgZ13wRo1; .ASPXAUTH=1BF9C16A2D905EB48E1DA725409737575F65FB0446CE9ED88A3CCF7DA6AFA4EEE0FBCB3DA1173740E32BFBE56F8B1DFAB5BC890D7E7AD7DD99FA811469B065E31F6398D16F4871A1BED3A7E6FF0D5C9CAA5AF0D7E6A17C5685C24F0E314EEA4C";
            var c = ss.ConvertToCookieContainer("www.loc.com");
        }
    }
}

namespace Operate.Web.ExtensionMethods.Test
{
    [TestClass()]
    public class StringExtensionsTests
    {
        [TestMethod()]
        public void NoHTMLTest()
        {
            var url = "http://www.baidu.com/cache/global/img/gs.gif";

            "http://www.test.com/1/txt".Download("c://1.txt");

           // var r = url.PostHttp("",Encoding.Default).GetHref()[0].GetHttp();

            url.Download("c://12.gif");
            Assert.Fail();
        }

        [TestMethod()]
        public void GetHrefTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Get_HttpTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void PostHttpTest()
        {
            Assert.Fail();
        }
    }
}
