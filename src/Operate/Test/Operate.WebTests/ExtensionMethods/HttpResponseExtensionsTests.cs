﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using Operate.Web.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.Web.ExtensionMethods.Tests
{
    [TestClass()]
    public class HttpResponseExtensionsTests
    {
        [TestMethod()]
        public void GetDataToStringTest()
        {
            var request = WebRequest.CreateHttp("http://www.baidu.com");
            var resp = (HttpWebResponse)request.GetResponse();
            var r = resp.GetDataToStringByUTF8Encoding();
        }
    }
}
