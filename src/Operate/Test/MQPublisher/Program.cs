﻿using Operate.MessageQueue.ActiveMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetMQ;
using NetMQ.Sockets;
using Operate.MessageQueue;
using Operate.MessageQueue.MicrosoftMessageQueue;
using Operate.MessageQueue.RabbitMQ;
using Operate.MessageQueue.ZeroMQ;
using RabbitMQ.Client;
using ZeroMQ = Operate.MessageQueue.ZeroMQ.ZeroMQ;

namespace MQPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestActiveMqPublisher();
            //TestRabbitMQPublisher();
            //TestMSMQPublisher();

            //Simple();

            //ZeroMQSimple();
            TestZeroMQPublisher();
        }

        public static void ZeroMQSimple()
        {
            // NOTES
            // 1. Use ThreadLocal<DealerSocket> where each thread has
            //    its own client DealerSocket to talk to server
            // 2. Each thread can send using it own socket
            // 3. Each thread socket is added to poller

            const int delay = 3000; // millis

            var clientSocketPerThread = new ThreadLocal<DealerSocket>();

            using (var server = new RouterSocket("@tcp://127.0.0.1:5556"))
            using (var poller = new NetMQPoller())
            {
                // Start some threads, each with its own DealerSocket
                // to talk to the server socket. Creates lots of sockets,
                // but no nasty race conditions no shared state, each
                // thread has its own socket, happy days.
                for (int i = 0; i < 3; i++)
                {
                    Task.Factory.StartNew(state =>
                    {
                        DealerSocket client = null;

                        if (!clientSocketPerThread.IsValueCreated)
                        {
                            client = new DealerSocket();
                            client.Options.Identity =
                                Encoding.Unicode.GetBytes(state.ToString());
                            client.Connect("tcp://127.0.0.1:5556");
                            client.ReceiveReady += Client_ReceiveReady;
                            clientSocketPerThread.Value = client;
                            poller.Add(client);
                        }
                        else
                        {
                            client = clientSocketPerThread.Value;
                        }

                        while (true)
                        {
                            var messageToServer = new NetMQMessage();
                            messageToServer.AppendEmptyFrame();
                            messageToServer.Append(state.ToString());
                            Console.WriteLine("======================================");
                            Console.WriteLine(" OUTGOING MESSAGE TO SERVER ");
                            Console.WriteLine("======================================");
                            PrintFrames("Client Sending", messageToServer);
                            client.SendMultipartMessage(messageToServer);
                            Thread.Sleep(delay);
                        }

                    }, string.Format("client {0}", i), TaskCreationOptions.LongRunning);
                }

                // start the poller
                poller.RunAsync();

                // server loop
                while (true)
                {
                    var clientMessage = server.ReceiveMultipartMessage();
                    Console.WriteLine("======================================");
                    Console.WriteLine(" INCOMING CLIENT MESSAGE FROM CLIENT ");
                    Console.WriteLine("======================================");
                    PrintFrames("Server receiving", clientMessage);
                    if (clientMessage.FrameCount == 3)
                    {
                        var clientAddress = clientMessage[0];
                        var clientOriginalMessage = clientMessage[2].ConvertToString();
                        string response = string.Format("{0} back from server {1}",
                            clientOriginalMessage, DateTime.Now.ToLongTimeString());
                        var messageToClient = new NetMQMessage();
                        messageToClient.Append(clientAddress);
                        messageToClient.AppendEmptyFrame();
                        messageToClient.Append(response);
                        server.SendMultipartMessage(messageToClient);
                    }
                }
            }
        }

        static void PrintFrames(string operationType, NetMQMessage message)
        {
            for (int i = 0; i < message.FrameCount; i++)
            {
                Console.WriteLine("{0} Socket : Frame[{1}] = {2}", operationType, i,
                    message[i].ConvertToString());
            }
        }

        static void Client_ReceiveReady(object sender, NetMQSocketEventArgs e)
        {
            bool hasmore = false;
            e.Socket.ReceiveFrameBytes(out hasmore);
            if (hasmore)
            {
                string result = e.Socket.ReceiveFrameString(out hasmore);
                Console.WriteLine("REPLY {0}", result);
            }
        }

        private static void Simple()
        {
            Console.WriteLine("Sender");
            Console.WriteLine("Press Enter to send data");
            Console.ReadLine();

            //const string EXCHANGE_NAME = "EXCHANGE3";

            ConnectionFactory factory = new ConnectionFactory();

            var conn = new RabbitMQConnection("localhost")
            {
                ThreadMode = ThreadMode.ThreadPool,
                MaxThread = 100
            };

            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    factory.HostName = conn.Host;

                    channel.ExchangeDeclare(conn.Exchange, conn.Type, false, true, null);

                    for (int i = 0; i < 1000; i++)
                    {
                        string data = i.ToString();

                        byte[] payload = Encoding.ASCII.GetBytes(data);
                        channel.BasicPublish(conn.Exchange, conn.RoutingKey, null, payload);
                        Console.WriteLine(data);
                    }
                }
            }

            Console.WriteLine("Done Sending");
        }

        private static void TestActiveMqPublisher()
        {
            var conn = new ActiveMQConnection("admin", "password", "localhost", "61616");
            var r = new Random();
            using (var activeMq = new ActiveMQ(conn))
            {
                if (activeMq.IsStarted)
                {
                    for (int i = 1; i <= 100; i++)
                    {
                        var msg = r.Next().ToString();
                        activeMq.Send(msg);
                        Console.WriteLine(msg);
                    }
                }
                else
                {
                    Console.WriteLine("连接已经关闭！");
                }
            }

            Console.ReadKey();
        }

        private static void TestZeroMQPublisher()
        {
            var conn = new ZeroMQConnection("tcp://127.0.0.1:5556",Mode.Client);
            var r = new Random();
            using (var zeroMQ = new Operate.MessageQueue.ZeroMQ.ZeroMQ(conn))
            {
                for (int i = 1; i <= 100; i++)
                {
                    var msg = i.ToString();
                    zeroMQ.Send(msg);
                    Console.WriteLine(msg);
                }
            }

            Console.ReadKey();
        }

        private static void TestRabbitMQPublisher()
        {
            var startTime = DateTime.Now;
            var conn = new RabbitMQConnection("localhost")
            {
                ThreadMode = ThreadMode.ThreadPool,
                MaxThread = 100,
                //AutoDelete = true
            };
            var r = new Random();
            using (var rabbitMQ = new Operate.MessageQueue.RabbitMQ.RabbitMQ(conn))
            {
                if (rabbitMQ.IsOpen)
                {
                    for (int i = 1; i <= 1000; i++)
                    {
                        //var msg = r.Next().ToString();
                        var msg = i.ToString();
                        rabbitMQ.Send(msg);
                        Console.WriteLine(msg);
                    }
                }
                else
                {
                    Console.WriteLine("连接已经关闭！");
                }
            }
            var endTime = DateTime.Now;

            Console.WriteLine("--------------------------------------");
            Console.WriteLine(startTime + "---" + endTime);
            Console.ReadKey();
        }

        private static void TestMSMQPublisher()
        {
            var conn = new MSMQConnection(@"FormatName:DIRECT=TCP:127.0.0.1\private$\mqtest", MessagePriority.Normal)
            {
                ThreadMode = ThreadMode.ThreadPool,
                MaxThread = 100
            };
            var r = new Random();
            using (var msmq = new MSMQ(conn))
            {
                if (msmq.IsOpen)
                {
                    for (int i = 1; i <= 10000; i++)
                    {
                        var msg = i.ToString();
                        msmq.Send(msg);
                        Console.WriteLine(msg);
                    }
                }
                else
                {
                    Console.WriteLine("连接已经关闭！");
                }
            }

            Console.ReadKey();
        }
    }
}
