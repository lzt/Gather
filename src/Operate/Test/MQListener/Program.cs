﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using Apache.NMS;
using NetMQ;
using Operate.MessageQueue;
using Operate.MessageQueue.ActiveMQ;
using Operate.MessageQueue.MicrosoftMessageQueue;
using Operate.MessageQueue.RabbitMQ;
using Operate.MessageQueue.ZeroMQ;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Impl;
using IConnection = Apache.NMS.IConnection;

namespace MQListener
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestActiveMqListener();
            //TestRabbitMQListener();

            //Simple();

            //TestMSMqListener();
            TestZeroMQListener();
        }

        private static void Simple()
        {
            Console.WriteLine("Listener");

            //const string EXCHANGE_NAME = "EXCHANGE3";

            var conn = new RabbitMQConnection("localhost")
            {
                ThreadMode = ThreadMode.ThreadPool,
                MaxThread = 100
            };

            ConnectionFactory factory = new ConnectionFactory();

            using (var connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(conn.Exchange, ExchangeType.Topic, false, true, null);

                    string queueName = channel.QueueDeclare();

                    EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (o, e) =>
                    {
                        string data = Encoding.ASCII.GetString(e.Body);
                        Console.WriteLine(data);
                    };

                    string consumerTag = channel.BasicConsume(queueName, true, consumer);

                    channel.QueueBind(queueName, conn.Exchange, conn.RoutingKey);

                    Console.WriteLine("Listening press ENTER to quit");
                    Console.ReadLine();

                    channel.QueueUnbind(queueName, conn.Exchange, conn.RoutingKey, null);
                }
            }
        }

        private static void TestActiveMqListener()
        {
            var conn = new ActiveMQConnection("admin", "password", "localhost", "61616");
            var r = new Random();
            using (var activeMq = new ActiveMQ(conn))
            {
                while (true)
                {
                    if (activeMq.IsStarted)
                    {
                        ITextMessage msg;
                        //activeMq.ReceiveMessage(out msg);

                        //Console.WriteLine(msg.Text);

                        activeMq.ReceiveMessage(PrintResultString);
                    }
                    else
                    {
                        Console.WriteLine("连接已经关闭！");
                        break;
                    }
                }
            }

            Console.ReadKey();
        }

        private static void TestZeroMQListener()
        {
            var conn = new ZeroMQConnection("@tcp://127.0.0.1:5556",Mode.Server);
            var r = new Random();
            using (var zeroMQ = new ZeroMQ(conn))
            {
                while (true)
                {
                        zeroMQ.ReceiveMessage<NetMQMessage>(PrintResultString);
                }
            }

            Console.ReadKey();
        }

        private static void TestRabbitMQListener()
        {
            var startTime = DateTime.Now;
            var conn = new RabbitMQConnection("localhost")
            {
                ThreadMode = ThreadMode.ThreadPool,
                MaxThread = 100
            };
            using (var rabbitMQ = new Operate.MessageQueue.RabbitMQ.RabbitMQ(conn))
            {
                if (rabbitMQ.IsOpen)
                {
                    ITextMessage msg;
                    //activeMq.ReceiveMessage(out msg);

                    //Console.WriteLine(msg.Text);

                    rabbitMQ.ReceiveMessage(PrintResultBytes);

                    //rabbitMQ.TestReceiveMessage(PrintResultBytes);

                    //var bytes = rabbitMQ.GetReceiveMessage<BasicDeliverEventArgs>();

                    //var result = Encoding.UTF8.GetString(bytes.Body);
                    //Console.WriteLine("接受数据：" + result);
                    //i += 1;
                    //Console.WriteLine("---" + DateTime.Now + "------------------" + i);
                }
                else
                {
                    Console.WriteLine("连接已经关闭！");
                }
            }
            var endTime = DateTime.Now;

            Console.WriteLine("--------------------------------------");
            Console.WriteLine(startTime + "---" + endTime);
            Console.ReadKey();
        }

        private static void TestMSMqListener()
        {
            var conn = new MSMQConnection(@".\private$\mqtest", MessagePriority.Normal)
            {
                ThreadMode = ThreadMode.ThreadPool,
                MaxThread = 100
            };
            var r = new Random();
            using (var activeMq = new MSMQ(conn))
            {
                while (true)
                {
                    if (activeMq.IsOpen)
                    {
                        activeMq.ReceiveMessage<Message>(PrintResultString);
                    }
                    else
                    {
                        Console.WriteLine("连接已经关闭！");
                        break;
                    }
                }
            }

            Console.ReadKey();
        }

        private static void PrintResultString(NetMQMessage result)
        {
            foreach (var msg in result)
            {
                Console.WriteLine("接受数据：" + Encoding.UTF8.GetString(msg.Buffer));
            }
            Thread.Sleep(50);
        }

        private static void PrintResultString(Message result)
        {
            Console.WriteLine("接受数据：" + result.Body.ToString());
            Thread.Sleep(50);
        }

        private static void PrintResultString(object result)
        {
            Console.WriteLine("接受数据：" + result);
        }

        private static int i = 0;

        private static void PrintResultBytes(byte[] bytes)
        {
            var result = Encoding.UTF8.GetString(bytes);
            Console.WriteLine("接受数据：" + result);
            //i += 1;
            //Console.WriteLine("---" + DateTime.Now + "------------------" + i);

            Thread.Sleep(50);
        }

        private static void PrintResultBytes(BasicDeliverEventArgs bytes)
        {
            var result = Encoding.UTF8.GetString(bytes.Body);
            Console.WriteLine("接受数据：" + result);
            i += 1;
            Console.WriteLine("---" + DateTime.Now + "------------------" + i);
        }
    }
}
