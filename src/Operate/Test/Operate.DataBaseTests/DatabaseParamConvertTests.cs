﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Operate.DataBase.SqlDatabase;

namespace Operate.DataBaseTests
{
    [TestClass()]
    public class DatabaseParamConvertTests
    {
        [TestMethod()]
        public void ToDictionaryListTest()
        {

            var oo = new SqlParameter("keyword", SqlDbType.NVarChar);

            var dic = DataBaseParamConvert.ToDictionary(oo);
            var dd = JsonConvert.SerializeObject(dic);
            var ppp = JsonConvert.DeserializeObject<Dictionary<string, object>>(dd);

            //SqlServerBaseDal.ToDatabaseParam(ppp);



            SqlParameter[] processParams = new SqlParameter[]
            {
                new SqlParameter("keyword",SqlDbType.NVarChar), 
                new SqlParameter("areaid",SqlDbType.Int), 
                new SqlParameter("levelid",SqlDbType.Int), 
                new SqlParameter("brandid",SqlDbType.Int)
            };
            processParams[0].Value = "";
            processParams[1].Value = 1;
            processParams[2].Value = 2;
            processParams[3].Value = 1;

            var list = (from p in processParams where p != null select JsonConvert.SerializeObject(DataBaseParamConvert.ToDictionary(p))).ToList();

            var processParamsserialized = JsonConvert.SerializeObject(list);

            var pList = JsonConvert.DeserializeObject<List<string>>(processParamsserialized);
            var paramList = new List<IDataParameter>();
            foreach (var p in pList)
            {
                var pdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(p);
                if (pdic != null)
                {
                   // paramList.Add((SqlServerBaseDal.ToDatabaseParam(pdic)));
                }
            }
        }
    }
}
