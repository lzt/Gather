﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.DataBase.SqlDatabase;
using Operate.IO.ExtensionMethods;

namespace Operate.DataBaseTests
{
    [TestClass()]
    public class MappingTemplateTests
    {
        [TestMethod()]
        public void AnalysisDocumentTest()
        {
            var file =
                "E:\\project\\Git\\resource\\CodeFactory\\src\\ui\\CodeFactory\\bin\\Debug\\temp\\NHibernate\\Xml\\Car.mapping.xml"
                    .ReadFile();

            MappingTemplate.AnalysisDocument(file);
        }
    }
}
