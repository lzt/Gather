﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SmsInfoEntityDetailsViewDal : ExViewBaseDal<SmsInfoEntityDetailsView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SmsInfoEntityDetailsViewDal(): this(Initialization.GetXmlConfig(typeof(SmsInfoEntityDetailsView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SmsInfoEntityDetailsViewDal CreateDal()
        {
			return new SmsInfoEntityDetailsViewDal(Initialization.GetXmlConfig(typeof(SmsInfoEntityDetailsView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SmsInfoEntityDetailsViewDal()
        {
           ModelName= typeof(SmsInfoEntityDetailsView).Name;
           var item = new SmsInfoEntityDetailsView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SmsInfoEntityDetailsViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SmsInfoEntityDetailsView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetById(int value)
        {
            var model = new SmsInfoEntityDetailsView { Id = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetById(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.Id));
        }

        #endregion

                
        #region SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetBySourceUserProfileId(int value)
        {
            var model = new SmsInfoEntityDetailsView { SourceUserProfileId = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserProfileId));
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetBySourceUserProfileId(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByContent(string value)
        {
            var model = new SmsInfoEntityDetailsView { Content = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByContent(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.Content));
        }

        #endregion

                
        #region InterfaceFeedback

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByInterfaceFeedback(string value)
        {
            var model = new SmsInfoEntityDetailsView { InterfaceFeedback = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.InterfaceFeedback));
        }

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByInterfaceFeedback(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.InterfaceFeedback));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByCreateTime(DateTime value)
        {
            var model = new SmsInfoEntityDetailsView { CreateTime = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByCreateTime(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.CreateTime));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByPhone(string value)
        {
            var model = new SmsInfoEntityDetailsView { Phone = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByPhone(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.Phone));
        }

        #endregion

                
        #region StatusSuccess

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByStatusSuccess(int value)
        {
            var model = new SmsInfoEntityDetailsView { StatusSuccess = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.StatusSuccess));
        }

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByStatusSuccess(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.StatusSuccess));
        }

        #endregion

                
        #region TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByTargetUserProfileId(int value)
        {
            var model = new SmsInfoEntityDetailsView { TargetUserProfileId = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserProfileId));
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByTargetUserProfileId(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserProfileId));
        }

        #endregion

                
        #region SourceUserLoginId

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetBySourceUserLoginId(string value)
        {
            var model = new SmsInfoEntityDetailsView { SourceUserLoginId = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserLoginId));
        }

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetBySourceUserLoginId(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserLoginId));
        }

        #endregion

                
        #region SourceUserAvatar

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetBySourceUserAvatar(string value)
        {
            var model = new SmsInfoEntityDetailsView { SourceUserAvatar = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserAvatar));
        }

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetBySourceUserAvatar(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserAvatar));
        }

        #endregion

                
        #region TargetUserLoginId

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByTargetUserLoginId(string value)
        {
            var model = new SmsInfoEntityDetailsView { TargetUserLoginId = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserLoginId));
        }

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByTargetUserLoginId(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserLoginId));
        }

        #endregion

                
        #region TargetUserAvatar

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByTargetUserAvatar(string value)
        {
            var model = new SmsInfoEntityDetailsView { TargetUserAvatar = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserAvatar));
        }

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体
        /// </summary>
        public SmsInfoEntityDetailsView GetByTargetUserAvatar(SmsInfoEntityDetailsView model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserAvatar));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { SourceUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By InterfaceFeedback

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByInterfaceFeedback(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { InterfaceFeedback = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.InterfaceFeedback),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByInterfaceFeedback(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.InterfaceFeedback),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusSuccess

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByStatusSuccess(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { StatusSuccess = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.StatusSuccess),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByStatusSuccess(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.StatusSuccess),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { TargetUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceUserLoginId

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListBySourceUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { SourceUserLoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserLoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListBySourceUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserLoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceUserAvatar

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListBySourceUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { SourceUserAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListBySourceUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.SourceUserAvatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserLoginId

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByTargetUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { TargetUserLoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserLoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByTargetUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserLoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserAvatar

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByTargetUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntityDetailsView { TargetUserAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntityDetailsView> GetListByTargetUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntityDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityDetailsViewField.TargetUserAvatar),sort,operateMode);
        }

        #endregion

        #endregion
    }
}