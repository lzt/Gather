﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class EvaluationInfoViewDal : ExViewBaseDal<EvaluationInfoView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public EvaluationInfoViewDal(): this(Initialization.GetXmlConfig(typeof(EvaluationInfoView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static EvaluationInfoViewDal CreateDal()
        {
			return new EvaluationInfoViewDal(Initialization.GetXmlConfig(typeof(EvaluationInfoView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static EvaluationInfoViewDal()
        {
           ModelName= typeof(EvaluationInfoView).Name;
           var item = new EvaluationInfoView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public EvaluationInfoViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "EvaluationInfoView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public EvaluationInfoView GetById(int value)
        {
            var model = new EvaluationInfoView { Id = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public EvaluationInfoView GetById(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Id));
        }

        #endregion

                
        #region RaterId

        /// <summary>
        /// 根据属性RaterId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByRaterId(int value)
        {
            var model = new EvaluationInfoView { RaterId = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.RaterId));
        }

        /// <summary>
        /// 根据属性RaterId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByRaterId(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.RaterId));
        }

        #endregion

                
        #region TargetUserId

        /// <summary>
        /// 根据属性TargetUserId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByTargetUserId(int value)
        {
            var model = new EvaluationInfoView { TargetUserId = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.TargetUserId));
        }

        /// <summary>
        /// 根据属性TargetUserId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByTargetUserId(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.TargetUserId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public EvaluationInfoView GetByContent(string value)
        {
            var model = new EvaluationInfoView { Content = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public EvaluationInfoView GetByContent(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Content));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public EvaluationInfoView GetByCreateTime(DateTime value)
        {
            var model = new EvaluationInfoView { CreateTime = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public EvaluationInfoView GetByCreateTime(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.CreateTime));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public EvaluationInfoView GetByVerification(int value)
        {
            var model = new EvaluationInfoView { Verification = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public EvaluationInfoView GetByVerification(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Verification));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public EvaluationInfoView GetByVerifier(int value)
        {
            var model = new EvaluationInfoView { Verifier = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public EvaluationInfoView GetByVerifier(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Verifier));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public EvaluationInfoView GetByVerificationTime(DateTime value)
        {
            var model = new EvaluationInfoView { VerificationTime = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public EvaluationInfoView GetByVerificationTime(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.VerificationTime));
        }

        #endregion

                
        #region OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByOrderId(int value)
        {
            var model = new EvaluationInfoView { OrderId = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.OrderId));
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByOrderId(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.OrderId));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByLoginId(string value)
        {
            var model = new EvaluationInfoView { LoginId = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public EvaluationInfoView GetByLoginId(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.LoginId));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public EvaluationInfoView GetByAvatar(string value)
        {
            var model = new EvaluationInfoView { Avatar = value };
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public EvaluationInfoView GetByAvatar(EvaluationInfoView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationInfoViewField.Avatar));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By RaterId

        /// <summary>
        /// 根据属性RaterId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByRaterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { RaterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.RaterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RaterId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByRaterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.RaterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserId

        /// <summary>
        /// 根据属性TargetUserId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByTargetUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { TargetUserId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.TargetUserId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByTargetUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.TargetUserId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { OrderId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.OrderId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.OrderId),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationInfoView { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<EvaluationInfoView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationInfoView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationInfoViewField.Avatar),sort,operateMode);
        }

        #endregion

        #endregion
    }
}