﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserProfileUserRoleGroupViewDal : ExViewBaseDal<UserProfileUserRoleGroupView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserProfileUserRoleGroupViewDal(): this(Initialization.GetXmlConfig(typeof(UserProfileUserRoleGroupView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserProfileUserRoleGroupViewDal CreateDal()
        {
			return new UserProfileUserRoleGroupViewDal(Initialization.GetXmlConfig(typeof(UserProfileUserRoleGroupView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserProfileUserRoleGroupViewDal()
        {
           ModelName= typeof(UserProfileUserRoleGroupView).Name;
           var item = new UserProfileUserRoleGroupView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserProfileUserRoleGroupViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType, ModelName, ModelXml)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserProfileUserRoleGroupView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetById(int value)
        {
            var model = new UserProfileUserRoleGroupView { Id = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetById(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Id));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByUserProfileId(int value)
        {
            var model = new UserProfileUserRoleGroupView { UserProfileId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByUserProfileId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.UserProfileId));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLoginId(string value)
        {
            var model = new UserProfileUserRoleGroupView { LoginId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLoginId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LoginId));
        }

        #endregion

                
        #region Password

        /// <summary>
        /// 根据属性Password获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByPassword(string value)
        {
            var model = new UserProfileUserRoleGroupView { Password = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Password));
        }

        /// <summary>
        /// 根据属性Password获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByPassword(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Password));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByStatusEnabled(int value)
        {
            var model = new UserProfileUserRoleGroupView { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByStatusEnabled(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.StatusEnabled));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByNickName(string value)
        {
            var model = new UserProfileUserRoleGroupView { NickName = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByNickName(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.NickName));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreateTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreateTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreateTime));
        }

        #endregion

                
        #region LastAccessTime

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLastAccessTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { LastAccessTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LastAccessTime));
        }

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLastAccessTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LastAccessTime));
        }

        #endregion

                
        #region Language

        /// <summary>
        /// 根据属性Language获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLanguage(string value)
        {
            var model = new UserProfileUserRoleGroupView { Language = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Language));
        }

        /// <summary>
        /// 根据属性Language获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLanguage(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Language));
        }

        #endregion

                
        #region UserType

        /// <summary>
        /// 根据属性UserType获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByUserType(int value)
        {
            var model = new UserProfileUserRoleGroupView { UserType = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.UserType));
        }

        /// <summary>
        /// 根据属性UserType获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByUserType(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.UserType));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByParentId(int value)
        {
            var model = new UserProfileUserRoleGroupView { ParentId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByParentId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.ParentId));
        }

        #endregion

                
        #region OutTime

        /// <summary>
        /// 根据属性OutTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByOutTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { OutTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.OutTime));
        }

        /// <summary>
        /// 根据属性OutTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByOutTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.OutTime));
        }

        #endregion

                
        #region Logins

        /// <summary>
        /// 根据属性Logins获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLogins(int value)
        {
            var model = new UserProfileUserRoleGroupView { Logins = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Logins));
        }

        /// <summary>
        /// 根据属性Logins获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLogins(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Logins));
        }

        #endregion

                
        #region RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByRestrictIp(int value)
        {
            var model = new UserProfileUserRoleGroupView { RestrictIp = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.RestrictIp));
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByRestrictIp(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.RestrictIp));
        }

        #endregion

                
        #region IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIpTactics(string value)
        {
            var model = new UserProfileUserRoleGroupView { IpTactics = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IpTactics));
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIpTactics(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IpTactics));
        }

        #endregion

                
        #region StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByStatusMultiLogin(int value)
        {
            var model = new UserProfileUserRoleGroupView { StatusMultiLogin = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.StatusMultiLogin));
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByStatusMultiLogin(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.StatusMultiLogin));
        }

        #endregion

                
        #region LoginIp

        /// <summary>
        /// 根据属性LoginIp获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLoginIp(string value)
        {
            var model = new UserProfileUserRoleGroupView { LoginIp = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LoginIp));
        }

        /// <summary>
        /// 根据属性LoginIp获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLoginIp(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LoginIp));
        }

        #endregion

                
        #region LoginMac

        /// <summary>
        /// 根据属性LoginMac获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLoginMac(string value)
        {
            var model = new UserProfileUserRoleGroupView { LoginMac = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LoginMac));
        }

        /// <summary>
        /// 根据属性LoginMac获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByLoginMac(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.LoginMac));
        }

        #endregion

                
        #region TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTimephasedLogin(int value)
        {
            var model = new UserProfileUserRoleGroupView { TimephasedLogin = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TimephasedLogin));
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTimephasedLogin(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TimephasedLogin));
        }

        #endregion

                
        #region AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAllowLoginTimeBegin(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { AllowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAllowLoginTimeBegin(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeBegin));
        }

        #endregion

                
        #region AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAllowLoginTimeEnd(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { AllowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAllowLoginTimeEnd(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeEnd));
        }

        #endregion

                
        #region DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDisallowLoginTimeBegin(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { DisallowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDisallowLoginTimeBegin(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeBegin));
        }

        #endregion

                
        #region DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDisallowLoginTimeEnd(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { DisallowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDisallowLoginTimeEnd(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeEnd));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByPhone(string value)
        {
            var model = new UserProfileUserRoleGroupView { Phone = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByPhone(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Phone));
        }

        #endregion

                
        #region Email

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmail(string value)
        {
            var model = new UserProfileUserRoleGroupView { Email = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Email));
        }

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmail(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Email));
        }

        #endregion

                
        #region EmailVerification

        /// <summary>
        /// 根据属性EmailVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailVerification(int value)
        {
            var model = new UserProfileUserRoleGroupView { EmailVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerification));
        }

        /// <summary>
        /// 根据属性EmailVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailVerification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerification));
        }

        #endregion

                
        #region EmailVerifier

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailVerifier(int value)
        {
            var model = new UserProfileUserRoleGroupView { EmailVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerifier));
        }

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailVerifier(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerifier));
        }

        #endregion

                
        #region EmailVerificationTime

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailVerificationTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { EmailVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerificationTime));
        }

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailVerificationTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerificationTime));
        }

        #endregion

                
        #region EmailDenyAuditReason

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailDenyAuditReason(string value)
        {
            var model = new UserProfileUserRoleGroupView { EmailDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailDenyAuditReason));
        }

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmailDenyAuditReason(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmailDenyAuditReason));
        }

        #endregion

                
        #region Salt

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetBySalt(string value)
        {
            var model = new UserProfileUserRoleGroupView { Salt = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Salt));
        }

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetBySalt(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Salt));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByRemark(string value)
        {
            var model = new UserProfileUserRoleGroupView { Remark = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByRemark(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Remark));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByVerification(int value)
        {
            var model = new UserProfileUserRoleGroupView { Verification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByVerification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Verification));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByVerificationTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { VerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByVerificationTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.VerificationTime));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByVerifier(int value)
        {
            var model = new UserProfileUserRoleGroupView { Verifier = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByVerifier(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Verifier));
        }

        #endregion

                
        #region DenyAuditReason

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDenyAuditReason(string value)
        {
            var model = new UserProfileUserRoleGroupView { DenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DenyAuditReason));
        }

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDenyAuditReason(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DenyAuditReason));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTrueName(string value)
        {
            var model = new UserProfileUserRoleGroupView { TrueName = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTrueName(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TrueName));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatar(string value)
        {
            var model = new UserProfileUserRoleGroupView { Avatar = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatar(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Avatar));
        }

        #endregion

                
        #region AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarVerification(int value)
        {
            var model = new UserProfileUserRoleGroupView { AvatarVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerification));
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarVerification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerification));
        }

        #endregion

                
        #region AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarVerifier(int value)
        {
            var model = new UserProfileUserRoleGroupView { AvatarVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerifier));
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarVerifier(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerifier));
        }

        #endregion

                
        #region AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarVerificationTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { AvatarVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerificationTime));
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarVerificationTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerificationTime));
        }

        #endregion

                
        #region AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarDenyAuditReason(string value)
        {
            var model = new UserProfileUserRoleGroupView { AvatarDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarDenyAuditReason));
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvatarDenyAuditReason(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarDenyAuditReason));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetBySex(int value)
        {
            var model = new UserProfileUserRoleGroupView { Sex = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetBySex(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Sex));
        }

        #endregion

                
        #region Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByBirthday(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { Birthday = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Birthday));
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByBirthday(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Birthday));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAppId(string value)
        {
            var model = new UserProfileUserRoleGroupView { AppId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAppId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAppSecret(string value)
        {
            var model = new UserProfileUserRoleGroupView { AppSecret = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAppSecret(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AppSecret));
        }

        #endregion

                
        #region CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCurrentScore(int value)
        {
            var model = new UserProfileUserRoleGroupView { CurrentScore = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CurrentScore));
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCurrentScore(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CurrentScore));
        }

        #endregion

                
        #region TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTotalScore(int value)
        {
            var model = new UserProfileUserRoleGroupView { TotalScore = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TotalScore));
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTotalScore(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TotalScore));
        }

        #endregion

                
        #region TotalIncome

        /// <summary>
        /// 根据属性TotalIncome获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTotalIncome(int value)
        {
            var model = new UserProfileUserRoleGroupView { TotalIncome = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TotalIncome));
        }

        /// <summary>
        /// 根据属性TotalIncome获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTotalIncome(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TotalIncome));
        }

        #endregion

                
        #region AvailableIncome

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvailableIncome(int value)
        {
            var model = new UserProfileUserRoleGroupView { AvailableIncome = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableIncome));
        }

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvailableIncome(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableIncome));
        }

        #endregion

                
        #region Paid

        /// <summary>
        /// 根据属性Paid获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByPaid(int value)
        {
            var model = new UserProfileUserRoleGroupView { Paid = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Paid));
        }

        /// <summary>
        /// 根据属性Paid获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByPaid(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Paid));
        }

        #endregion

                
        #region Zipcode

        /// <summary>
        /// 根据属性Zipcode获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByZipcode(string value)
        {
            var model = new UserProfileUserRoleGroupView { Zipcode = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Zipcode));
        }

        /// <summary>
        /// 根据属性Zipcode获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByZipcode(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Zipcode));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAddress(string value)
        {
            var model = new UserProfileUserRoleGroupView { Address = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAddress(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Address));
        }

        #endregion

                
        #region ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByProvinceId(int value)
        {
            var model = new UserProfileUserRoleGroupView { ProvinceId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.ProvinceId));
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByProvinceId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.ProvinceId));
        }

        #endregion

                
        #region CityId

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCityId(int value)
        {
            var model = new UserProfileUserRoleGroupView { CityId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CityId));
        }

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCityId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CityId));
        }

        #endregion

                
        #region TownId

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTownId(int value)
        {
            var model = new UserProfileUserRoleGroupView { TownId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TownId));
        }

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTownId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TownId));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentification(string value)
        {
            var model = new UserProfileUserRoleGroupView { Identification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Identification));
        }

        #endregion

                
        #region IdentificationImage

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationImage(string value)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationImage = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationImage));
        }

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationImage(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationImage));
        }

        #endregion

                
        #region IdentificationVerification

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationVerification(int value)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerification));
        }

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationVerification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerification));
        }

        #endregion

                
        #region IdentificationVerifier

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationVerifier(int value)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerifier));
        }

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationVerifier(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerifier));
        }

        #endregion

                
        #region IdentificationVerificationTime

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationVerificationTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerificationTime));
        }

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationVerificationTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerificationTime));
        }

        #endregion

                
        #region IdentificationDenyAuditReason

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationDenyAuditReason(string value)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationDenyAuditReason));
        }

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByIdentificationDenyAuditReason(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationDenyAuditReason));
        }

        #endregion

                
        #region DriverLicense

        /// <summary>
        /// 根据属性DriverLicense获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicense(string value)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicense = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicense));
        }

        /// <summary>
        /// 根据属性DriverLicense获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicense(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicense));
        }

        #endregion

                
        #region DriverLicenseImage

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseImage(string value)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseImage = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseImage));
        }

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseImage(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseImage));
        }

        #endregion

                
        #region DriverLicenseVerification

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseVerification(int value)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerification));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseVerification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerification));
        }

        #endregion

                
        #region DriverLicenseVerifier

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseVerifier(int value)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerifier));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseVerifier(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerifier));
        }

        #endregion

                
        #region DriverLicenseVerificationTime

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseVerificationTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerificationTime));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseVerificationTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerificationTime));
        }

        #endregion

                
        #region DriverLicenseDenyAuditReason

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseDenyAuditReason(string value)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseDenyAuditReason));
        }

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDriverLicenseDenyAuditReason(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseDenyAuditReason));
        }

        #endregion

                
        #region CreditCard

        /// <summary>
        /// 根据属性CreditCard获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCard(string value)
        {
            var model = new UserProfileUserRoleGroupView { CreditCard = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCard));
        }

        /// <summary>
        /// 根据属性CreditCard获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCard(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCard));
        }

        #endregion

                
        #region CreditCardImage

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardImage(string value)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardImage = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardImage));
        }

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardImage(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardImage));
        }

        #endregion

                
        #region CreditCardVerification

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardVerification(int value)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerification));
        }

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardVerification(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerification));
        }

        #endregion

                
        #region CreditCardVerifier

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardVerifier(int value)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerifier));
        }

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardVerifier(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerifier));
        }

        #endregion

                
        #region CreditCardVerificationTime

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardVerificationTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerificationTime));
        }

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardVerificationTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerificationTime));
        }

        #endregion

                
        #region CreditCardDenyAuditReason

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardDenyAuditReason(string value)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardDenyAuditReason));
        }

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByCreditCardDenyAuditReason(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardDenyAuditReason));
        }

        #endregion

                
        #region FrozenFunds

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByFrozenFunds(double value)
        {
            var model = new UserProfileUserRoleGroupView { FrozenFunds = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenFunds));
        }

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByFrozenFunds(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenFunds));
        }

        #endregion

                
        #region GiftFunds

        /// <summary>
        /// 根据属性GiftFunds获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGiftFunds(double value)
        {
            var model = new UserProfileUserRoleGroupView { GiftFunds = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GiftFunds));
        }

        /// <summary>
        /// 根据属性GiftFunds获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGiftFunds(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GiftFunds));
        }

        #endregion

                
        #region ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByThawTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { ThawTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.ThawTime));
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByThawTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.ThawTime));
        }

        #endregion

                
        #region FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByFrozenTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupView { FrozenTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenTime));
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByFrozenTime(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenTime));
        }

        #endregion

                
        #region QQ

        /// <summary>
        /// 根据属性QQ获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByQQ(string value)
        {
            var model = new UserProfileUserRoleGroupView { QQ = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.QQ));
        }

        /// <summary>
        /// 根据属性QQ获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByQQ(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.QQ));
        }

        #endregion

                
        #region EmergencyPhone

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmergencyPhone(string value)
        {
            var model = new UserProfileUserRoleGroupView { EmergencyPhone = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmergencyPhone));
        }

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByEmergencyPhone(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.EmergencyPhone));
        }

        #endregion

                
        #region Grade

        /// <summary>
        /// 根据属性Grade获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGrade(int value)
        {
            var model = new UserProfileUserRoleGroupView { Grade = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Grade));
        }

        /// <summary>
        /// 根据属性Grade获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGrade(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.Grade));
        }

        #endregion

                
        #region AvailableRecharge

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvailableRecharge(int value)
        {
            var model = new UserProfileUserRoleGroupView { AvailableRecharge = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableRecharge));
        }

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByAvailableRecharge(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableRecharge));
        }

        #endregion

                
        #region TotalRecharge

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTotalRecharge(int value)
        {
            var model = new UserProfileUserRoleGroupView { TotalRecharge = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TotalRecharge));
        }

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByTotalRecharge(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.TotalRecharge));
        }

        #endregion

                
        #region DiscountCardMode

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDiscountCardMode(int value)
        {
            var model = new UserProfileUserRoleGroupView { DiscountCardMode = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardMode));
        }

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDiscountCardMode(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardMode));
        }

        #endregion

                
        #region DiscountCardDenomination

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDiscountCardDenomination(int value)
        {
            var model = new UserProfileUserRoleGroupView { DiscountCardDenomination = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardDenomination));
        }

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByDiscountCardDenomination(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardDenomination));
        }

        #endregion

                
        #region StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByStatusInvalid(int value)
        {
            var model = new UserProfileUserRoleGroupView { StatusInvalid = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.StatusInvalid));
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByStatusInvalid(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.StatusInvalid));
        }

        #endregion

                
        #region InviterId

        /// <summary>
        /// 根据属性InviterId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByInviterId(int value)
        {
            var model = new UserProfileUserRoleGroupView { InviterId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.InviterId));
        }

        /// <summary>
        /// 根据属性InviterId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByInviterId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.InviterId));
        }

        #endregion

                
        #region GroupId

        /// <summary>
        /// 根据属性GroupId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupId(int value)
        {
            var model = new UserProfileUserRoleGroupView { GroupId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupId));
        }

        /// <summary>
        /// 根据属性GroupId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupId));
        }

        #endregion

                
        #region GroupTitle

        /// <summary>
        /// 根据属性GroupTitle获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupTitle(string value)
        {
            var model = new UserProfileUserRoleGroupView { GroupTitle = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupTitle));
        }

        /// <summary>
        /// 根据属性GroupTitle获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupTitle(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupTitle));
        }

        #endregion

                
        #region GroupRoleList

        /// <summary>
        /// 根据属性GroupRoleList获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupRoleList(string value)
        {
            var model = new UserProfileUserRoleGroupView { GroupRoleList = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupRoleList));
        }

        /// <summary>
        /// 根据属性GroupRoleList获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupRoleList(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupRoleList));
        }

        #endregion

                
        #region GroupParentId

        /// <summary>
        /// 根据属性GroupParentId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupParentId(long value)
        {
            var model = new UserProfileUserRoleGroupView { GroupParentId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupParentId));
        }

        /// <summary>
        /// 根据属性GroupParentId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupParentId(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupParentId));
        }

        #endregion

                
        #region GroupPurpose

        /// <summary>
        /// 根据属性GroupPurpose获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupPurpose(int value)
        {
            var model = new UserProfileUserRoleGroupView { GroupPurpose = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupPurpose));
        }

        /// <summary>
        /// 根据属性GroupPurpose获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupView GetByGroupPurpose(UserProfileUserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupViewField.GroupPurpose));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Password

        /// <summary>
        /// 根据属性Password获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByPassword(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Password = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Password),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Password获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByPassword(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Password),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastAccessTime

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLastAccessTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { LastAccessTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LastAccessTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLastAccessTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LastAccessTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Language

        /// <summary>
        /// 根据属性Language获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLanguage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Language = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Language),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Language获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLanguage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Language),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserType

        /// <summary>
        /// 根据属性UserType获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByUserType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { UserType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.UserType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserType获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByUserType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.UserType),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By OutTime

        /// <summary>
        /// 根据属性OutTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByOutTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { OutTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.OutTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OutTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByOutTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.OutTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Logins

        /// <summary>
        /// 根据属性Logins获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLogins(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Logins = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Logins),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Logins获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLogins(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Logins),sort,operateMode);
        }

        #endregion

                
        #region GetList By RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { RestrictIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.RestrictIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.RestrictIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { IpTactics = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IpTactics),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IpTactics),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { StatusMultiLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.StatusMultiLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.StatusMultiLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginIp

        /// <summary>
        /// 根据属性LoginIp获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLoginIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { LoginIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LoginIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginIp获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLoginIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LoginIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginMac

        /// <summary>
        /// 根据属性LoginMac获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLoginMac(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { LoginMac = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LoginMac),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginMac获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByLoginMac(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.LoginMac),sort,operateMode);
        }

        #endregion

                
        #region GetList By TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { TimephasedLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TimephasedLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TimephasedLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AllowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AllowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AllowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DisallowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DisallowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DisallowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Email

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Email = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Email),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Email),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerification

        /// <summary>
        /// 根据属性EmailVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { EmailVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerifier

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { EmailVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerificationTime

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { EmailVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailDenyAuditReason

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { EmailDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmailDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmailDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Salt

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Salt = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Salt),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Salt),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By DenyAuditReason

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AvatarVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AvatarVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AvatarVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AvatarDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvatarDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Birthday = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Birthday),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Birthday),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CurrentScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CurrentScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CurrentScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { TotalScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TotalScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TotalScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalIncome

        /// <summary>
        /// 根据属性TotalIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTotalIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { TotalIncome = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TotalIncome),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTotalIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TotalIncome),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableIncome

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvailableIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AvailableIncome = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableIncome),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvailableIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableIncome),sort,operateMode);
        }

        #endregion

                
        #region GetList By Paid

        /// <summary>
        /// 根据属性Paid获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByPaid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Paid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Paid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Paid获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByPaid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Paid),sort,operateMode);
        }

        #endregion

                
        #region GetList By Zipcode

        /// <summary>
        /// 根据属性Zipcode获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByZipcode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Zipcode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Zipcode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Zipcode获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByZipcode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Zipcode),sort,operateMode);
        }

        #endregion

                
        #region GetList By Address

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Address = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Address),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Address),sort,operateMode);
        }

        #endregion

                
        #region GetList By ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { ProvinceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.ProvinceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.ProvinceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CityId

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CityId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CityId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CityId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TownId

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { TownId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TownId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TownId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationImage

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerification

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerifier

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerificationTime

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationDenyAuditReason

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { IdentificationDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByIdentificationDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.IdentificationDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicense

        /// <summary>
        /// 根据属性DriverLicense获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicense = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicense),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicense获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicense),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseImage

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerification

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerifier

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerificationTime

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseDenyAuditReason

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DriverLicenseDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDriverLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DriverLicenseDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCard

        /// <summary>
        /// 根据属性CreditCard获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreditCard = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCard),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCard获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCard),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardImage

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerification

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerifier

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerificationTime

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardDenyAuditReason

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { CreditCardDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByCreditCardDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.CreditCardDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenFunds

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByFrozenFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { FrozenFunds = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenFunds),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByFrozenFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenFunds),sort,operateMode);
        }

        #endregion

                
        #region GetList By GiftFunds

        /// <summary>
        /// 根据属性GiftFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGiftFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { GiftFunds = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GiftFunds),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GiftFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGiftFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GiftFunds),sort,operateMode);
        }

        #endregion

                
        #region GetList By ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { ThawTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.ThawTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.ThawTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { FrozenTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.FrozenTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By QQ

        /// <summary>
        /// 根据属性QQ获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByQQ(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { QQ = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.QQ),sort,operateMode);
        }

        /// <summary>
        /// 根据属性QQ获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByQQ(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.QQ),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmergencyPhone

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmergencyPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { EmergencyPhone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmergencyPhone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByEmergencyPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.EmergencyPhone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Grade

        /// <summary>
        /// 根据属性Grade获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { Grade = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Grade),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Grade获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.Grade),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableRecharge

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvailableRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { AvailableRecharge = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableRecharge),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByAvailableRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.AvailableRecharge),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalRecharge

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTotalRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { TotalRecharge = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TotalRecharge),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByTotalRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.TotalRecharge),sort,operateMode);
        }

        #endregion

                
        #region GetList By DiscountCardMode

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDiscountCardMode(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DiscountCardMode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardMode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDiscountCardMode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardMode),sort,operateMode);
        }

        #endregion

                
        #region GetList By DiscountCardDenomination

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDiscountCardDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { DiscountCardDenomination = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardDenomination),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByDiscountCardDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.DiscountCardDenomination),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { StatusInvalid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.StatusInvalid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.StatusInvalid),sort,operateMode);
        }

        #endregion

                
        #region GetList By InviterId

        /// <summary>
        /// 根据属性InviterId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByInviterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { InviterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.InviterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InviterId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByInviterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.InviterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupId

        /// <summary>
        /// 根据属性GroupId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { GroupId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupId),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupTitle

        /// <summary>
        /// 根据属性GroupTitle获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { GroupTitle = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupTitle),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupTitle获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupTitle),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupRoleList

        /// <summary>
        /// 根据属性GroupRoleList获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupRoleList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { GroupRoleList = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupRoleList),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupRoleList获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupRoleList(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupRoleList),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupParentId

        /// <summary>
        /// 根据属性GroupParentId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { GroupParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupParentId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupPurpose

        /// <summary>
        /// 根据属性GroupPurpose获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupView { GroupPurpose = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupPurpose),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupPurpose获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupView> GetListByGroupPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupViewField.GroupPurpose),sort,operateMode);
        }

        #endregion

        #endregion
    }
}