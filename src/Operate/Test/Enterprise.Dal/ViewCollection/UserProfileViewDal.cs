﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserProfileViewDal : ExViewBaseDal<UserProfileView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserProfileViewDal(): this(Initialization.GetXmlConfig(typeof(UserProfileView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserProfileViewDal CreateDal()
        {
			return new UserProfileViewDal(Initialization.GetXmlConfig(typeof(UserProfileView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserProfileViewDal()
        {
           ModelName= typeof(UserProfileView).Name;
           var item = new UserProfileView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserProfileViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserProfileView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserProfileView GetById(int value)
        {
            var model = new UserProfileView { Id = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserProfileView GetById(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Id));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfileView GetByLoginId(string value)
        {
            var model = new UserProfileView { LoginId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfileView GetByLoginId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.LoginId));
        }

        #endregion

                
        #region Password

        /// <summary>
        /// 根据属性Password获取数据实体
        /// </summary>
        public UserProfileView GetByPassword(string value)
        {
            var model = new UserProfileView { Password = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Password));
        }

        /// <summary>
        /// 根据属性Password获取数据实体
        /// </summary>
        public UserProfileView GetByPassword(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Password));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfileView GetByStatusEnabled(int value)
        {
            var model = new UserProfileView { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfileView GetByStatusEnabled(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.StatusEnabled));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfileView GetByNickName(string value)
        {
            var model = new UserProfileView { NickName = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfileView GetByNickName(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.NickName));
        }

        #endregion

                
        #region LastAccessTime

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体
        /// </summary>
        public UserProfileView GetByLastAccessTime(DateTime value)
        {
            var model = new UserProfileView { LastAccessTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.LastAccessTime));
        }

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体
        /// </summary>
        public UserProfileView GetByLastAccessTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.LastAccessTime));
        }

        #endregion

                
        #region Language

        /// <summary>
        /// 根据属性Language获取数据实体
        /// </summary>
        public UserProfileView GetByLanguage(string value)
        {
            var model = new UserProfileView { Language = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Language));
        }

        /// <summary>
        /// 根据属性Language获取数据实体
        /// </summary>
        public UserProfileView GetByLanguage(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Language));
        }

        #endregion

                
        #region UserType

        /// <summary>
        /// 根据属性UserType获取数据实体
        /// </summary>
        public UserProfileView GetByUserType(int value)
        {
            var model = new UserProfileView { UserType = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.UserType));
        }

        /// <summary>
        /// 根据属性UserType获取数据实体
        /// </summary>
        public UserProfileView GetByUserType(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.UserType));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserProfileView GetByParentId(int value)
        {
            var model = new UserProfileView { ParentId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserProfileView GetByParentId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.ParentId));
        }

        #endregion

                
        #region OutTime

        /// <summary>
        /// 根据属性OutTime获取数据实体
        /// </summary>
        public UserProfileView GetByOutTime(DateTime value)
        {
            var model = new UserProfileView { OutTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.OutTime));
        }

        /// <summary>
        /// 根据属性OutTime获取数据实体
        /// </summary>
        public UserProfileView GetByOutTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.OutTime));
        }

        #endregion

                
        #region Logins

        /// <summary>
        /// 根据属性Logins获取数据实体
        /// </summary>
        public UserProfileView GetByLogins(int value)
        {
            var model = new UserProfileView { Logins = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Logins));
        }

        /// <summary>
        /// 根据属性Logins获取数据实体
        /// </summary>
        public UserProfileView GetByLogins(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Logins));
        }

        #endregion

                
        #region RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserProfileView GetByRestrictIp(int value)
        {
            var model = new UserProfileView { RestrictIp = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.RestrictIp));
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserProfileView GetByRestrictIp(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.RestrictIp));
        }

        #endregion

                
        #region IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserProfileView GetByIpTactics(string value)
        {
            var model = new UserProfileView { IpTactics = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.IpTactics));
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserProfileView GetByIpTactics(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.IpTactics));
        }

        #endregion

                
        #region StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserProfileView GetByStatusMultiLogin(int value)
        {
            var model = new UserProfileView { StatusMultiLogin = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.StatusMultiLogin));
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserProfileView GetByStatusMultiLogin(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.StatusMultiLogin));
        }

        #endregion

                
        #region LoginIp

        /// <summary>
        /// 根据属性LoginIp获取数据实体
        /// </summary>
        public UserProfileView GetByLoginIp(string value)
        {
            var model = new UserProfileView { LoginIp = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.LoginIp));
        }

        /// <summary>
        /// 根据属性LoginIp获取数据实体
        /// </summary>
        public UserProfileView GetByLoginIp(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.LoginIp));
        }

        #endregion

                
        #region LoginMac

        /// <summary>
        /// 根据属性LoginMac获取数据实体
        /// </summary>
        public UserProfileView GetByLoginMac(string value)
        {
            var model = new UserProfileView { LoginMac = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.LoginMac));
        }

        /// <summary>
        /// 根据属性LoginMac获取数据实体
        /// </summary>
        public UserProfileView GetByLoginMac(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.LoginMac));
        }

        #endregion

                
        #region TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserProfileView GetByTimephasedLogin(int value)
        {
            var model = new UserProfileView { TimephasedLogin = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.TimephasedLogin));
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserProfileView GetByTimephasedLogin(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.TimephasedLogin));
        }

        #endregion

                
        #region AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileView GetByAllowLoginTimeBegin(DateTime value)
        {
            var model = new UserProfileView { AllowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AllowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileView GetByAllowLoginTimeBegin(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AllowLoginTimeBegin));
        }

        #endregion

                
        #region AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileView GetByAllowLoginTimeEnd(DateTime value)
        {
            var model = new UserProfileView { AllowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AllowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileView GetByAllowLoginTimeEnd(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AllowLoginTimeEnd));
        }

        #endregion

                
        #region DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileView GetByDisallowLoginTimeBegin(DateTime value)
        {
            var model = new UserProfileView { DisallowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DisallowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfileView GetByDisallowLoginTimeBegin(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DisallowLoginTimeBegin));
        }

        #endregion

                
        #region DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileView GetByDisallowLoginTimeEnd(DateTime value)
        {
            var model = new UserProfileView { DisallowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DisallowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfileView GetByDisallowLoginTimeEnd(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DisallowLoginTimeEnd));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public UserProfileView GetByPhone(string value)
        {
            var model = new UserProfileView { Phone = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public UserProfileView GetByPhone(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Phone));
        }

        #endregion

                
        #region Email

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public UserProfileView GetByEmail(string value)
        {
            var model = new UserProfileView { Email = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Email));
        }

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public UserProfileView GetByEmail(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Email));
        }

        #endregion

                
        #region EmailVerification

        /// <summary>
        /// 根据属性EmailVerification获取数据实体
        /// </summary>
        public UserProfileView GetByEmailVerification(int value)
        {
            var model = new UserProfileView { EmailVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailVerification));
        }

        /// <summary>
        /// 根据属性EmailVerification获取数据实体
        /// </summary>
        public UserProfileView GetByEmailVerification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailVerification));
        }

        #endregion

                
        #region EmailVerifier

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByEmailVerifier(int value)
        {
            var model = new UserProfileView { EmailVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailVerifier));
        }

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByEmailVerifier(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailVerifier));
        }

        #endregion

                
        #region EmailVerificationTime

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByEmailVerificationTime(DateTime value)
        {
            var model = new UserProfileView { EmailVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailVerificationTime));
        }

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByEmailVerificationTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailVerificationTime));
        }

        #endregion

                
        #region EmailDenyAuditReason

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByEmailDenyAuditReason(string value)
        {
            var model = new UserProfileView { EmailDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailDenyAuditReason));
        }

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByEmailDenyAuditReason(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmailDenyAuditReason));
        }

        #endregion

                
        #region Salt

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public UserProfileView GetBySalt(string value)
        {
            var model = new UserProfileView { Salt = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Salt));
        }

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public UserProfileView GetBySalt(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Salt));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserProfileView GetByRemark(string value)
        {
            var model = new UserProfileView { Remark = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserProfileView GetByRemark(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Remark));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public UserProfileView GetByVerification(int value)
        {
            var model = new UserProfileView { Verification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public UserProfileView GetByVerification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Verification));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByVerificationTime(DateTime value)
        {
            var model = new UserProfileView { VerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByVerificationTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.VerificationTime));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public UserProfileView GetByVerifier(int value)
        {
            var model = new UserProfileView { Verifier = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public UserProfileView GetByVerifier(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Verifier));
        }

        #endregion

                
        #region DenyAuditReason

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByDenyAuditReason(string value)
        {
            var model = new UserProfileView { DenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DenyAuditReason));
        }

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByDenyAuditReason(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DenyAuditReason));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfileView GetByTrueName(string value)
        {
            var model = new UserProfileView { TrueName = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfileView GetByTrueName(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.TrueName));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public UserProfileView GetByAvatar(string value)
        {
            var model = new UserProfileView { Avatar = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public UserProfileView GetByAvatar(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Avatar));
        }

        #endregion

                
        #region AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarVerification(int value)
        {
            var model = new UserProfileView { AvatarVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarVerification));
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarVerification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarVerification));
        }

        #endregion

                
        #region AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarVerifier(int value)
        {
            var model = new UserProfileView { AvatarVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarVerifier));
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarVerifier(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarVerifier));
        }

        #endregion

                
        #region AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarVerificationTime(DateTime value)
        {
            var model = new UserProfileView { AvatarVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarVerificationTime));
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarVerificationTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarVerificationTime));
        }

        #endregion

                
        #region AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarDenyAuditReason(string value)
        {
            var model = new UserProfileView { AvatarDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarDenyAuditReason));
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByAvatarDenyAuditReason(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvatarDenyAuditReason));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfileView GetBySex(int value)
        {
            var model = new UserProfileView { Sex = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfileView GetBySex(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Sex));
        }

        #endregion

                
        #region Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfileView GetByBirthday(DateTime value)
        {
            var model = new UserProfileView { Birthday = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Birthday));
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfileView GetByBirthday(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Birthday));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfileView GetByAppId(string value)
        {
            var model = new UserProfileView { AppId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfileView GetByAppId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfileView GetByAppSecret(string value)
        {
            var model = new UserProfileView { AppSecret = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfileView GetByAppSecret(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AppSecret));
        }

        #endregion

                
        #region CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public UserProfileView GetByCurrentScore(int value)
        {
            var model = new UserProfileView { CurrentScore = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CurrentScore));
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public UserProfileView GetByCurrentScore(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CurrentScore));
        }

        #endregion

                
        #region TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public UserProfileView GetByTotalScore(int value)
        {
            var model = new UserProfileView { TotalScore = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.TotalScore));
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public UserProfileView GetByTotalScore(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.TotalScore));
        }

        #endregion

                
        #region TotalIncome

        /// <summary>
        /// 根据属性TotalIncome获取数据实体
        /// </summary>
        public UserProfileView GetByTotalIncome(int value)
        {
            var model = new UserProfileView { TotalIncome = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.TotalIncome));
        }

        /// <summary>
        /// 根据属性TotalIncome获取数据实体
        /// </summary>
        public UserProfileView GetByTotalIncome(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.TotalIncome));
        }

        #endregion

                
        #region AvailableIncome

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体
        /// </summary>
        public UserProfileView GetByAvailableIncome(int value)
        {
            var model = new UserProfileView { AvailableIncome = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvailableIncome));
        }

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体
        /// </summary>
        public UserProfileView GetByAvailableIncome(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvailableIncome));
        }

        #endregion

                
        #region Paid

        /// <summary>
        /// 根据属性Paid获取数据实体
        /// </summary>
        public UserProfileView GetByPaid(int value)
        {
            var model = new UserProfileView { Paid = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Paid));
        }

        /// <summary>
        /// 根据属性Paid获取数据实体
        /// </summary>
        public UserProfileView GetByPaid(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Paid));
        }

        #endregion

                
        #region Zipcode

        /// <summary>
        /// 根据属性Zipcode获取数据实体
        /// </summary>
        public UserProfileView GetByZipcode(string value)
        {
            var model = new UserProfileView { Zipcode = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Zipcode));
        }

        /// <summary>
        /// 根据属性Zipcode获取数据实体
        /// </summary>
        public UserProfileView GetByZipcode(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Zipcode));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserProfileView GetByAddress(string value)
        {
            var model = new UserProfileView { Address = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserProfileView GetByAddress(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Address));
        }

        #endregion

                
        #region ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public UserProfileView GetByProvinceId(int value)
        {
            var model = new UserProfileView { ProvinceId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.ProvinceId));
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public UserProfileView GetByProvinceId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.ProvinceId));
        }

        #endregion

                
        #region CityId

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public UserProfileView GetByCityId(int value)
        {
            var model = new UserProfileView { CityId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CityId));
        }

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public UserProfileView GetByCityId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CityId));
        }

        #endregion

                
        #region TownId

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public UserProfileView GetByTownId(int value)
        {
            var model = new UserProfileView { TownId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.TownId));
        }

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public UserProfileView GetByTownId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.TownId));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfileView GetByIdentification(string value)
        {
            var model = new UserProfileView { Identification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfileView GetByIdentification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Identification));
        }

        #endregion

                
        #region IdentificationImage

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationImage(string value)
        {
            var model = new UserProfileView { IdentificationImage = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationImage));
        }

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationImage(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationImage));
        }

        #endregion

                
        #region IdentificationVerification

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationVerification(int value)
        {
            var model = new UserProfileView { IdentificationVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationVerification));
        }

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationVerification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationVerification));
        }

        #endregion

                
        #region IdentificationVerifier

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationVerifier(int value)
        {
            var model = new UserProfileView { IdentificationVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationVerifier));
        }

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationVerifier(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationVerifier));
        }

        #endregion

                
        #region IdentificationVerificationTime

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationVerificationTime(DateTime value)
        {
            var model = new UserProfileView { IdentificationVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationVerificationTime));
        }

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationVerificationTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationVerificationTime));
        }

        #endregion

                
        #region IdentificationDenyAuditReason

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationDenyAuditReason(string value)
        {
            var model = new UserProfileView { IdentificationDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationDenyAuditReason));
        }

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByIdentificationDenyAuditReason(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.IdentificationDenyAuditReason));
        }

        #endregion

                
        #region DriverLicense

        /// <summary>
        /// 根据属性DriverLicense获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicense(string value)
        {
            var model = new UserProfileView { DriverLicense = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicense));
        }

        /// <summary>
        /// 根据属性DriverLicense获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicense(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicense));
        }

        #endregion

                
        #region DriverLicenseImage

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseImage(string value)
        {
            var model = new UserProfileView { DriverLicenseImage = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseImage));
        }

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseImage(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseImage));
        }

        #endregion

                
        #region DriverLicenseVerification

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseVerification(int value)
        {
            var model = new UserProfileView { DriverLicenseVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseVerification));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseVerification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseVerification));
        }

        #endregion

                
        #region DriverLicenseVerifier

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseVerifier(int value)
        {
            var model = new UserProfileView { DriverLicenseVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseVerifier));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseVerifier(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseVerifier));
        }

        #endregion

                
        #region DriverLicenseVerificationTime

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseVerificationTime(DateTime value)
        {
            var model = new UserProfileView { DriverLicenseVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseVerificationTime));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseVerificationTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseVerificationTime));
        }

        #endregion

                
        #region DriverLicenseDenyAuditReason

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseDenyAuditReason(string value)
        {
            var model = new UserProfileView { DriverLicenseDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseDenyAuditReason));
        }

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByDriverLicenseDenyAuditReason(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DriverLicenseDenyAuditReason));
        }

        #endregion

                
        #region CreditCard

        /// <summary>
        /// 根据属性CreditCard获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCard(string value)
        {
            var model = new UserProfileView { CreditCard = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCard));
        }

        /// <summary>
        /// 根据属性CreditCard获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCard(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCard));
        }

        #endregion

                
        #region CreditCardImage

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardImage(string value)
        {
            var model = new UserProfileView { CreditCardImage = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardImage));
        }

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardImage(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardImage));
        }

        #endregion

                
        #region CreditCardVerification

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardVerification(int value)
        {
            var model = new UserProfileView { CreditCardVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardVerification));
        }

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardVerification(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardVerification));
        }

        #endregion

                
        #region CreditCardVerifier

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardVerifier(int value)
        {
            var model = new UserProfileView { CreditCardVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardVerifier));
        }

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardVerifier(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardVerifier));
        }

        #endregion

                
        #region CreditCardVerificationTime

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardVerificationTime(DateTime value)
        {
            var model = new UserProfileView { CreditCardVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardVerificationTime));
        }

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardVerificationTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardVerificationTime));
        }

        #endregion

                
        #region CreditCardDenyAuditReason

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardDenyAuditReason(string value)
        {
            var model = new UserProfileView { CreditCardDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardDenyAuditReason));
        }

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体
        /// </summary>
        public UserProfileView GetByCreditCardDenyAuditReason(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreditCardDenyAuditReason));
        }

        #endregion

                
        #region FrozenFunds

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体
        /// </summary>
        public UserProfileView GetByFrozenFunds(double value)
        {
            var model = new UserProfileView { FrozenFunds = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.FrozenFunds));
        }

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体
        /// </summary>
        public UserProfileView GetByFrozenFunds(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.FrozenFunds));
        }

        #endregion

                
        #region GiftFunds

        /// <summary>
        /// 根据属性GiftFunds获取数据实体
        /// </summary>
        public UserProfileView GetByGiftFunds(double value)
        {
            var model = new UserProfileView { GiftFunds = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.GiftFunds));
        }

        /// <summary>
        /// 根据属性GiftFunds获取数据实体
        /// </summary>
        public UserProfileView GetByGiftFunds(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.GiftFunds));
        }

        #endregion

                
        #region ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public UserProfileView GetByThawTime(DateTime value)
        {
            var model = new UserProfileView { ThawTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.ThawTime));
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public UserProfileView GetByThawTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.ThawTime));
        }

        #endregion

                
        #region FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public UserProfileView GetByFrozenTime(DateTime value)
        {
            var model = new UserProfileView { FrozenTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.FrozenTime));
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public UserProfileView GetByFrozenTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.FrozenTime));
        }

        #endregion

                
        #region QQ

        /// <summary>
        /// 根据属性QQ获取数据实体
        /// </summary>
        public UserProfileView GetByQQ(string value)
        {
            var model = new UserProfileView { QQ = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.QQ));
        }

        /// <summary>
        /// 根据属性QQ获取数据实体
        /// </summary>
        public UserProfileView GetByQQ(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.QQ));
        }

        #endregion

                
        #region EmergencyPhone

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体
        /// </summary>
        public UserProfileView GetByEmergencyPhone(string value)
        {
            var model = new UserProfileView { EmergencyPhone = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmergencyPhone));
        }

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体
        /// </summary>
        public UserProfileView GetByEmergencyPhone(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.EmergencyPhone));
        }

        #endregion

                
        #region Grade

        /// <summary>
        /// 根据属性Grade获取数据实体
        /// </summary>
        public UserProfileView GetByGrade(int value)
        {
            var model = new UserProfileView { Grade = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Grade));
        }

        /// <summary>
        /// 根据属性Grade获取数据实体
        /// </summary>
        public UserProfileView GetByGrade(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Grade));
        }

        #endregion

                
        #region AvailableRecharge

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体
        /// </summary>
        public UserProfileView GetByAvailableRecharge(int value)
        {
            var model = new UserProfileView { AvailableRecharge = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvailableRecharge));
        }

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体
        /// </summary>
        public UserProfileView GetByAvailableRecharge(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.AvailableRecharge));
        }

        #endregion

                
        #region TotalRecharge

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体
        /// </summary>
        public UserProfileView GetByTotalRecharge(int value)
        {
            var model = new UserProfileView { TotalRecharge = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.TotalRecharge));
        }

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体
        /// </summary>
        public UserProfileView GetByTotalRecharge(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.TotalRecharge));
        }

        #endregion

                
        #region DiscountCardMode

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体
        /// </summary>
        public UserProfileView GetByDiscountCardMode(int value)
        {
            var model = new UserProfileView { DiscountCardMode = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DiscountCardMode));
        }

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体
        /// </summary>
        public UserProfileView GetByDiscountCardMode(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DiscountCardMode));
        }

        #endregion

                
        #region DiscountCardDenomination

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体
        /// </summary>
        public UserProfileView GetByDiscountCardDenomination(int value)
        {
            var model = new UserProfileView { DiscountCardDenomination = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.DiscountCardDenomination));
        }

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体
        /// </summary>
        public UserProfileView GetByDiscountCardDenomination(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.DiscountCardDenomination));
        }

        #endregion

                
        #region StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfileView GetByStatusInvalid(int value)
        {
            var model = new UserProfileView { StatusInvalid = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.StatusInvalid));
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfileView GetByStatusInvalid(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.StatusInvalid));
        }

        #endregion

                
        #region InviterId

        /// <summary>
        /// 根据属性InviterId获取数据实体
        /// </summary>
        public UserProfileView GetByInviterId(int value)
        {
            var model = new UserProfileView { InviterId = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.InviterId));
        }

        /// <summary>
        /// 根据属性InviterId获取数据实体
        /// </summary>
        public UserProfileView GetByInviterId(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.InviterId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfileView GetByIp(string value)
        {
            var model = new UserProfileView { Ip = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfileView GetByIp(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfileView GetByCreateBy(long value)
        {
            var model = new UserProfileView { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfileView GetByCreateBy(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileView GetByCreateTime(DateTime value)
        {
            var model = new UserProfileView { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileView GetByCreateTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfileView GetByLastModifyBy(long value)
        {
            var model = new UserProfileView { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfileView GetByLastModifyBy(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfileView GetByLastModifyTime(DateTime value)
        {
            var model = new UserProfileView { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfileView GetByLastModifyTime(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.LastModifyTime));
        }

        #endregion

                
        #region Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserProfileView GetByPurpose(int value)
        {
            var model = new UserProfileView { Purpose = value };
            return GetByWhere(model.GetFilterString(UserProfileViewField.Purpose));
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserProfileView GetByPurpose(UserProfileView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileViewField.Purpose));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Password

        /// <summary>
        /// 根据属性Password获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPassword(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Password = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Password),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Password获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPassword(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Password),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastAccessTime

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLastAccessTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { LastAccessTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LastAccessTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLastAccessTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LastAccessTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Language

        /// <summary>
        /// 根据属性Language获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLanguage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Language = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Language),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Language获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLanguage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Language),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserType

        /// <summary>
        /// 根据属性UserType获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByUserType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { UserType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.UserType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserType获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByUserType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.UserType),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By OutTime

        /// <summary>
        /// 根据属性OutTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByOutTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { OutTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.OutTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OutTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByOutTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.OutTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Logins

        /// <summary>
        /// 根据属性Logins获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLogins(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Logins = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Logins),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Logins获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLogins(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Logins),sort,operateMode);
        }

        #endregion

                
        #region GetList By RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { RestrictIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.RestrictIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.RestrictIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { IpTactics = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IpTactics),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IpTactics),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { StatusMultiLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.StatusMultiLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.StatusMultiLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginIp

        /// <summary>
        /// 根据属性LoginIp获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLoginIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { LoginIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LoginIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginIp获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLoginIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LoginIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginMac

        /// <summary>
        /// 根据属性LoginMac获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLoginMac(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { LoginMac = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LoginMac),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginMac获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLoginMac(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LoginMac),sort,operateMode);
        }

        #endregion

                
        #region GetList By TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { TimephasedLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TimephasedLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TimephasedLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AllowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AllowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AllowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AllowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AllowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AllowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DisallowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DisallowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DisallowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DisallowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DisallowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DisallowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Email

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Email = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Email),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Email),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerification

        /// <summary>
        /// 根据属性EmailVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { EmailVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerifier

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { EmailVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerificationTime

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { EmailVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailDenyAuditReason

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { EmailDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmailDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmailDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Salt

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Salt = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Salt),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Salt),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By DenyAuditReason

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AvatarVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AvatarVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AvatarVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AvatarDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvatarDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Birthday = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Birthday),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Birthday),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CurrentScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CurrentScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CurrentScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { TotalScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TotalScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TotalScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalIncome

        /// <summary>
        /// 根据属性TotalIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTotalIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { TotalIncome = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TotalIncome),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTotalIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TotalIncome),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableIncome

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvailableIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AvailableIncome = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvailableIncome),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvailableIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvailableIncome),sort,operateMode);
        }

        #endregion

                
        #region GetList By Paid

        /// <summary>
        /// 根据属性Paid获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPaid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Paid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Paid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Paid获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPaid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Paid),sort,operateMode);
        }

        #endregion

                
        #region GetList By Zipcode

        /// <summary>
        /// 根据属性Zipcode获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByZipcode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Zipcode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Zipcode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Zipcode获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByZipcode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Zipcode),sort,operateMode);
        }

        #endregion

                
        #region GetList By Address

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Address = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Address),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Address),sort,operateMode);
        }

        #endregion

                
        #region GetList By ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { ProvinceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.ProvinceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.ProvinceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CityId

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CityId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CityId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CityId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TownId

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { TownId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TownId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TownId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationImage

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { IdentificationImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerification

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { IdentificationVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerifier

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { IdentificationVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerificationTime

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { IdentificationVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationDenyAuditReason

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { IdentificationDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIdentificationDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.IdentificationDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicense

        /// <summary>
        /// 根据属性DriverLicense获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DriverLicense = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicense),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicense获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicense),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseImage

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DriverLicenseImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerification

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DriverLicenseVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerifier

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DriverLicenseVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerificationTime

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DriverLicenseVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseDenyAuditReason

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DriverLicenseDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDriverLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DriverLicenseDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCard

        /// <summary>
        /// 根据属性CreditCard获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreditCard = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCard),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCard获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCard),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardImage

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreditCardImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerification

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreditCardVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerifier

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreditCardVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerificationTime

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreditCardVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardDenyAuditReason

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreditCardDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreditCardDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreditCardDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenFunds

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByFrozenFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { FrozenFunds = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.FrozenFunds),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByFrozenFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.FrozenFunds),sort,operateMode);
        }

        #endregion

                
        #region GetList By GiftFunds

        /// <summary>
        /// 根据属性GiftFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByGiftFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { GiftFunds = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.GiftFunds),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GiftFunds获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByGiftFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.GiftFunds),sort,operateMode);
        }

        #endregion

                
        #region GetList By ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { ThawTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.ThawTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.ThawTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { FrozenTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.FrozenTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.FrozenTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By QQ

        /// <summary>
        /// 根据属性QQ获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByQQ(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { QQ = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.QQ),sort,operateMode);
        }

        /// <summary>
        /// 根据属性QQ获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByQQ(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.QQ),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmergencyPhone

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmergencyPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { EmergencyPhone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmergencyPhone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByEmergencyPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.EmergencyPhone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Grade

        /// <summary>
        /// 根据属性Grade获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByGrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Grade = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Grade),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Grade获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByGrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Grade),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableRecharge

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvailableRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { AvailableRecharge = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvailableRecharge),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByAvailableRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.AvailableRecharge),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalRecharge

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTotalRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { TotalRecharge = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TotalRecharge),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByTotalRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.TotalRecharge),sort,operateMode);
        }

        #endregion

                
        #region GetList By DiscountCardMode

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDiscountCardMode(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DiscountCardMode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DiscountCardMode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDiscountCardMode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DiscountCardMode),sort,operateMode);
        }

        #endregion

                
        #region GetList By DiscountCardDenomination

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDiscountCardDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { DiscountCardDenomination = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DiscountCardDenomination),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByDiscountCardDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.DiscountCardDenomination),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { StatusInvalid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.StatusInvalid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.StatusInvalid),sort,operateMode);
        }

        #endregion

                
        #region GetList By InviterId

        /// <summary>
        /// 根据属性InviterId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByInviterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { InviterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.InviterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InviterId获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByInviterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.InviterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.LastModifyTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileView { Purpose = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Purpose),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserProfileView> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileViewField.Purpose),sort,operateMode);
        }

        #endregion

        #endregion
    }
}