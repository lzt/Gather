﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserProfileCompanyRelationViewDal : ExViewBaseDal<UserProfileCompanyRelationView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserProfileCompanyRelationViewDal(): this(Initialization.GetXmlConfig(typeof(UserProfileCompanyRelationView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserProfileCompanyRelationViewDal CreateDal()
        {
			return new UserProfileCompanyRelationViewDal(Initialization.GetXmlConfig(typeof(UserProfileCompanyRelationView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserProfileCompanyRelationViewDal()
        {
           ModelName= typeof(UserProfileCompanyRelationView).Name;
           var item = new UserProfileCompanyRelationView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserProfileCompanyRelationViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserProfileCompanyRelationView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetById(int value)
        {
            var model = new UserProfileCompanyRelationView { Id = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetById(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Id));
        }

        #endregion

                
        #region RelationId

        /// <summary>
        /// 根据属性RelationId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByRelationId(int value)
        {
            var model = new UserProfileCompanyRelationView { RelationId = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.RelationId));
        }

        /// <summary>
        /// 根据属性RelationId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByRelationId(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.RelationId));
        }

        #endregion

                
        #region CompanyId

        /// <summary>
        /// 根据属性CompanyId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyId(int value)
        {
            var model = new UserProfileCompanyRelationView { CompanyId = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyId));
        }

        /// <summary>
        /// 根据属性CompanyId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyId(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyId));
        }

        #endregion

                
        #region Position

        /// <summary>
        /// 根据属性Position获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByPosition(string value)
        {
            var model = new UserProfileCompanyRelationView { Position = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Position));
        }

        /// <summary>
        /// 根据属性Position获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByPosition(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Position));
        }

        #endregion

                
        #region StatusIsGovernor

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByStatusIsGovernor(int value)
        {
            var model = new UserProfileCompanyRelationView { StatusIsGovernor = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.StatusIsGovernor));
        }

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByStatusIsGovernor(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.StatusIsGovernor));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByLoginId(string value)
        {
            var model = new UserProfileCompanyRelationView { LoginId = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByLoginId(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.LoginId));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByNickName(string value)
        {
            var model = new UserProfileCompanyRelationView { NickName = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByNickName(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.NickName));
        }

        #endregion

                
        #region UserPhone

        /// <summary>
        /// 根据属性UserPhone获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByUserPhone(string value)
        {
            var model = new UserProfileCompanyRelationView { UserPhone = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.UserPhone));
        }

        /// <summary>
        /// 根据属性UserPhone获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByUserPhone(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.UserPhone));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByIdentification(string value)
        {
            var model = new UserProfileCompanyRelationView { Identification = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByIdentification(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Identification));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByTrueName(string value)
        {
            var model = new UserProfileCompanyRelationView { TrueName = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByTrueName(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.TrueName));
        }

        #endregion

                
        #region UserAvatar

        /// <summary>
        /// 根据属性UserAvatar获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByUserAvatar(string value)
        {
            var model = new UserProfileCompanyRelationView { UserAvatar = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.UserAvatar));
        }

        /// <summary>
        /// 根据属性UserAvatar获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByUserAvatar(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.UserAvatar));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetBySex(int value)
        {
            var model = new UserProfileCompanyRelationView { Sex = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetBySex(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Sex));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByAppId(string value)
        {
            var model = new UserProfileCompanyRelationView { AppId = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByAppId(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByAppSecret(string value)
        {
            var model = new UserProfileCompanyRelationView { AppSecret = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByAppSecret(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.AppSecret));
        }

        #endregion

                
        #region UserAddress

        /// <summary>
        /// 根据属性UserAddress获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByUserAddress(string value)
        {
            var model = new UserProfileCompanyRelationView { UserAddress = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.UserAddress));
        }

        /// <summary>
        /// 根据属性UserAddress获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByUserAddress(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.UserAddress));
        }

        #endregion

                
        #region StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByStatusInvalid(int value)
        {
            var model = new UserProfileCompanyRelationView { StatusInvalid = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.StatusInvalid));
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByStatusInvalid(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.StatusInvalid));
        }

        #endregion

                
        #region Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByBirthday(DateTime value)
        {
            var model = new UserProfileCompanyRelationView { Birthday = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Birthday));
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByBirthday(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.Birthday));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByStatusEnabled(int value)
        {
            var model = new UserProfileCompanyRelationView { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByStatusEnabled(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.StatusEnabled));
        }

        #endregion

                
        #region CompanyName

        /// <summary>
        /// 根据属性CompanyName获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyName(string value)
        {
            var model = new UserProfileCompanyRelationView { CompanyName = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyName));
        }

        /// <summary>
        /// 根据属性CompanyName获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyName(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyName));
        }

        #endregion

                
        #region CompanyCorporation

        /// <summary>
        /// 根据属性CompanyCorporation获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyCorporation(string value)
        {
            var model = new UserProfileCompanyRelationView { CompanyCorporation = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyCorporation));
        }

        /// <summary>
        /// 根据属性CompanyCorporation获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyCorporation(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyCorporation));
        }

        #endregion

                
        #region CompanyAddress

        /// <summary>
        /// 根据属性CompanyAddress获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyAddress(string value)
        {
            var model = new UserProfileCompanyRelationView { CompanyAddress = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAddress));
        }

        /// <summary>
        /// 根据属性CompanyAddress获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyAddress(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAddress));
        }

        #endregion

                
        #region CompanyAvatar

        /// <summary>
        /// 根据属性CompanyAvatar获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyAvatar(string value)
        {
            var model = new UserProfileCompanyRelationView { CompanyAvatar = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAvatar));
        }

        /// <summary>
        /// 根据属性CompanyAvatar获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyAvatar(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAvatar));
        }

        #endregion

                
        #region CompanyRegistrationTime

        /// <summary>
        /// 根据属性CompanyRegistrationTime获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyRegistrationTime(DateTime value)
        {
            var model = new UserProfileCompanyRelationView { CompanyRegistrationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyRegistrationTime));
        }

        /// <summary>
        /// 根据属性CompanyRegistrationTime获取数据实体
        /// </summary>
        public UserProfileCompanyRelationView GetByCompanyRegistrationTime(UserProfileCompanyRelationView model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationViewField.CompanyRegistrationTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By RelationId

        /// <summary>
        /// 根据属性RelationId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByRelationId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { RelationId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.RelationId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RelationId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByRelationId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.RelationId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyId

        /// <summary>
        /// 根据属性CompanyId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { CompanyId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Position

        /// <summary>
        /// 根据属性Position获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByPosition(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { Position = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Position),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Position获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByPosition(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Position),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusIsGovernor

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByStatusIsGovernor(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { StatusIsGovernor = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.StatusIsGovernor),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByStatusIsGovernor(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.StatusIsGovernor),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserPhone

        /// <summary>
        /// 根据属性UserPhone获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByUserPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { UserPhone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.UserPhone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserPhone获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByUserPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.UserPhone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserAvatar

        /// <summary>
        /// 根据属性UserAvatar获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { UserAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.UserAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserAvatar获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.UserAvatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserAddress

        /// <summary>
        /// 根据属性UserAddress获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByUserAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { UserAddress = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.UserAddress),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserAddress获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByUserAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.UserAddress),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { StatusInvalid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.StatusInvalid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.StatusInvalid),sort,operateMode);
        }

        #endregion

                
        #region GetList By Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { Birthday = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Birthday),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.Birthday),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyName

        /// <summary>
        /// 根据属性CompanyName获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { CompanyName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyName获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyName),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyCorporation

        /// <summary>
        /// 根据属性CompanyCorporation获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyCorporation(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { CompanyCorporation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyCorporation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyCorporation获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyCorporation(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyCorporation),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyAddress

        /// <summary>
        /// 根据属性CompanyAddress获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { CompanyAddress = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAddress),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyAddress获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAddress),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyAvatar

        /// <summary>
        /// 根据属性CompanyAvatar获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { CompanyAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyAvatar获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyAvatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyRegistrationTime

        /// <summary>
        /// 根据属性CompanyRegistrationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyRegistrationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelationView { CompanyRegistrationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyRegistrationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyRegistrationTime获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelationView> GetListByCompanyRegistrationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelationView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationViewField.CompanyRegistrationTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}