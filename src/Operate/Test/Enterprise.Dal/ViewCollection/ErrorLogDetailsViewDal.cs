﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class ErrorLogDetailsViewDal : ExViewBaseDal<ErrorLogDetailsView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ErrorLogDetailsViewDal(): this(Initialization.GetXmlConfig(typeof(ErrorLogDetailsView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ErrorLogDetailsViewDal CreateDal()
        {
			return new ErrorLogDetailsViewDal(Initialization.GetXmlConfig(typeof(ErrorLogDetailsView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ErrorLogDetailsViewDal()
        {
           ModelName= typeof(ErrorLogDetailsView).Name;
           var item = new ErrorLogDetailsView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ErrorLogDetailsViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ErrorLogDetailsView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetById(int value)
        {
            var model = new ErrorLogDetailsView { Id = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetById(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Id));
        }

        #endregion

                
        #region Source

        /// <summary>
        /// 根据属性Source获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetBySource(string value)
        {
            var model = new ErrorLogDetailsView { Source = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Source));
        }

        /// <summary>
        /// 根据属性Source获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetBySource(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Source));
        }

        #endregion

                
        #region Message

        /// <summary>
        /// 根据属性Message获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByMessage(string value)
        {
            var model = new ErrorLogDetailsView { Message = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Message));
        }

        /// <summary>
        /// 根据属性Message获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByMessage(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Message));
        }

        #endregion

                
        #region StackTrace

        /// <summary>
        /// 根据属性StackTrace获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByStackTrace(string value)
        {
            var model = new ErrorLogDetailsView { StackTrace = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.StackTrace));
        }

        /// <summary>
        /// 根据属性StackTrace获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByStackTrace(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.StackTrace));
        }

        #endregion

                
        #region Scene

        /// <summary>
        /// 根据属性Scene获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByScene(string value)
        {
            var model = new ErrorLogDetailsView { Scene = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Scene));
        }

        /// <summary>
        /// 根据属性Scene获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByScene(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Scene));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByType(int value)
        {
            var model = new ErrorLogDetailsView { Type = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByType(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Type));
        }

        #endregion

                
        #region Header

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByHeader(string value)
        {
            var model = new ErrorLogDetailsView { Header = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Header));
        }

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByHeader(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Header));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByUrl(string value)
        {
            var model = new ErrorLogDetailsView { Url = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByUrl(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Url));
        }

        #endregion

                
        #region FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByFormParams(string value)
        {
            var model = new ErrorLogDetailsView { FormParams = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.FormParams));
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByFormParams(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.FormParams));
        }

        #endregion

                
        #region UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByUrlParams(string value)
        {
            var model = new ErrorLogDetailsView { UrlParams = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.UrlParams));
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByUrlParams(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.UrlParams));
        }

        #endregion

                
        #region Host

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByHost(string value)
        {
            var model = new ErrorLogDetailsView { Host = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Host));
        }

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByHost(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Host));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByUserProfileId(int value)
        {
            var model = new ErrorLogDetailsView { UserProfileId = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByUserProfileId(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.UserProfileId));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByLoginId(string value)
        {
            var model = new ErrorLogDetailsView { LoginId = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByLoginId(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.LoginId));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByNickName(string value)
        {
            var model = new ErrorLogDetailsView { NickName = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByNickName(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.NickName));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByTrueName(string value)
        {
            var model = new ErrorLogDetailsView { TrueName = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByTrueName(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.TrueName));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByAvatar(string value)
        {
            var model = new ErrorLogDetailsView { Avatar = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByAvatar(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Avatar));
        }

        #endregion

                
        #region Email

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByEmail(string value)
        {
            var model = new ErrorLogDetailsView { Email = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Email));
        }

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByEmail(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Email));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByIdentification(string value)
        {
            var model = new ErrorLogDetailsView { Identification = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByIdentification(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Identification));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByPhone(string value)
        {
            var model = new ErrorLogDetailsView { Phone = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByPhone(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Phone));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetBySex(int value)
        {
            var model = new ErrorLogDetailsView { Sex = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetBySex(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Sex));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByAppId(string value)
        {
            var model = new ErrorLogDetailsView { AppId = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByAppId(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByAppSecret(string value)
        {
            var model = new ErrorLogDetailsView { AppSecret = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByAppSecret(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.AppSecret));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByIp(string value)
        {
            var model = new ErrorLogDetailsView { Ip = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByIp(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByCreateBy(long value)
        {
            var model = new ErrorLogDetailsView { CreateBy = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByCreateBy(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByCreateTime(DateTime value)
        {
            var model = new ErrorLogDetailsView { CreateTime = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByCreateTime(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByLastModifyBy(long value)
        {
            var model = new ErrorLogDetailsView { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByLastModifyBy(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByLastModifyTime(DateTime value)
        {
            var model = new ErrorLogDetailsView { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ErrorLogDetailsView GetByLastModifyTime(ErrorLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogDetailsViewField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By Source

        /// <summary>
        /// 根据属性Source获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListBySource(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Source = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Source),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Source获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListBySource(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Source),sort,operateMode);
        }

        #endregion

                
        #region GetList By Message

        /// <summary>
        /// 根据属性Message获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByMessage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Message = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Message),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Message获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByMessage(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Message),sort,operateMode);
        }

        #endregion

                
        #region GetList By StackTrace

        /// <summary>
        /// 根据属性StackTrace获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByStackTrace(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { StackTrace = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.StackTrace),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StackTrace获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByStackTrace(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.StackTrace),sort,operateMode);
        }

        #endregion

                
        #region GetList By Scene

        /// <summary>
        /// 根据属性Scene获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByScene(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Scene = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Scene),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Scene获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByScene(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Scene),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Header

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Header = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Header),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Header),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { FormParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.FormParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.FormParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { UrlParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.UrlParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.UrlParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By Host

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Host = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Host),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Host),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By Email

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Email = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Email),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Email),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLogDetailsView { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ErrorLogDetailsView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogDetailsViewField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}