﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class LogonLogDetailsViewDal : ExViewBaseDal<LogonLogDetailsView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public LogonLogDetailsViewDal(): this(Initialization.GetXmlConfig(typeof(LogonLogDetailsView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static LogonLogDetailsViewDal CreateDal()
        {
			return new LogonLogDetailsViewDal(Initialization.GetXmlConfig(typeof(LogonLogDetailsView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static LogonLogDetailsViewDal()
        {
           ModelName= typeof(LogonLogDetailsView).Name;
           var item = new LogonLogDetailsView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public LogonLogDetailsViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "LogonLogDetailsView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public LogonLogDetailsView GetById(int value)
        {
            var model = new LogonLogDetailsView { Id = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public LogonLogDetailsView GetById(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Id));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByUserProfileId(int value)
        {
            var model = new LogonLogDetailsView { UserProfileId = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByUserProfileId(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.UserProfileId));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByUrl(string value)
        {
            var model = new LogonLogDetailsView { Url = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByUrl(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Url));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByLoginId(string value)
        {
            var model = new LogonLogDetailsView { LoginId = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByLoginId(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.LoginId));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByNickName(string value)
        {
            var model = new LogonLogDetailsView { NickName = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByNickName(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.NickName));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByTrueName(string value)
        {
            var model = new LogonLogDetailsView { TrueName = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByTrueName(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.TrueName));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByAvatar(string value)
        {
            var model = new LogonLogDetailsView { Avatar = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByAvatar(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Avatar));
        }

        #endregion

                
        #region Email

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByEmail(string value)
        {
            var model = new LogonLogDetailsView { Email = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Email));
        }

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByEmail(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Email));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByIdentification(string value)
        {
            var model = new LogonLogDetailsView { Identification = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByIdentification(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Identification));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByPhone(string value)
        {
            var model = new LogonLogDetailsView { Phone = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByPhone(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Phone));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public LogonLogDetailsView GetBySex(int value)
        {
            var model = new LogonLogDetailsView { Sex = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public LogonLogDetailsView GetBySex(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Sex));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByAppId(string value)
        {
            var model = new LogonLogDetailsView { AppId = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByAppId(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByAppSecret(string value)
        {
            var model = new LogonLogDetailsView { AppSecret = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByAppSecret(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.AppSecret));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByIp(string value)
        {
            var model = new LogonLogDetailsView { Ip = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByIp(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.Ip));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByCreateTime(DateTime value)
        {
            var model = new LogonLogDetailsView { CreateTime = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByCreateTime(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.CreateTime));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByCreateBy(long value)
        {
            var model = new LogonLogDetailsView { CreateBy = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByCreateBy(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.CreateBy));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByLastModifyBy(long value)
        {
            var model = new LogonLogDetailsView { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByLastModifyBy(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByLastModifyTime(DateTime value)
        {
            var model = new LogonLogDetailsView { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public LogonLogDetailsView GetByLastModifyTime(LogonLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(LogonLogDetailsViewField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By Email

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Email = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Email),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Email),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLogDetailsView { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<LogonLogDetailsView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogDetailsViewField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}