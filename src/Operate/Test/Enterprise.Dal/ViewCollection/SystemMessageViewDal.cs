﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SystemMessageViewDal : ExViewBaseDal<SystemMessageView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SystemMessageViewDal(): this(Initialization.GetXmlConfig(typeof(SystemMessageView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SystemMessageViewDal CreateDal()
        {
			return new SystemMessageViewDal(Initialization.GetXmlConfig(typeof(SystemMessageView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SystemMessageViewDal()
        {
           ModelName= typeof(SystemMessageView).Name;
           var item = new SystemMessageView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SystemMessageViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SystemMessageView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public SystemMessageView GetById(int value)
        {
            var model = new SystemMessageView { Id = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public SystemMessageView GetById(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Id));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SystemMessageView GetByContent(string value)
        {
            var model = new SystemMessageView { Content = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SystemMessageView GetByContent(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Content));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public SystemMessageView GetByType(int value)
        {
            var model = new SystemMessageView { Type = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public SystemMessageView GetByType(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Type));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SystemMessageView GetByCreateTime(DateTime value)
        {
            var model = new SystemMessageView { CreateTime = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SystemMessageView GetByCreateTime(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.CreateTime));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public SystemMessageView GetByTitle(string value)
        {
            var model = new SystemMessageView { Title = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public SystemMessageView GetByTitle(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.Title));
        }

        #endregion

                
        #region StatusRead

        /// <summary>
        /// 根据属性StatusRead获取数据实体
        /// </summary>
        public SystemMessageView GetByStatusRead(int value)
        {
            var model = new SystemMessageView { StatusRead = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.StatusRead));
        }

        /// <summary>
        /// 根据属性StatusRead获取数据实体
        /// </summary>
        public SystemMessageView GetByStatusRead(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.StatusRead));
        }

        #endregion

                
        #region ReaderUserProfileId

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体
        /// </summary>
        public SystemMessageView GetByReaderUserProfileId(int value)
        {
            var model = new SystemMessageView { ReaderUserProfileId = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.ReaderUserProfileId));
        }

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体
        /// </summary>
        public SystemMessageView GetByReaderUserProfileId(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.ReaderUserProfileId));
        }

        #endregion

                
        #region StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public SystemMessageView GetByStatusDelete(int value)
        {
            var model = new SystemMessageView { StatusDelete = value };
            return GetByWhere(model.GetFilterString(SystemMessageViewField.StatusDelete));
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public SystemMessageView GetByStatusDelete(SystemMessageView model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageViewField.StatusDelete));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusRead

        /// <summary>
        /// 根据属性StatusRead获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { StatusRead = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.StatusRead),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusRead获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.StatusRead),sort,operateMode);
        }

        #endregion

                
        #region GetList By ReaderUserProfileId

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByReaderUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { ReaderUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.ReaderUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByReaderUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.ReaderUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageView { StatusDelete = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.StatusDelete),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<SystemMessageView> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageViewField.StatusDelete),sort,operateMode);
        }

        #endregion

        #endregion
    }
}