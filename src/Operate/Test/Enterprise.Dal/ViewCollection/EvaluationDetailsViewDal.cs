﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class EvaluationDetailsViewDal : ExViewBaseDal<EvaluationDetailsView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public EvaluationDetailsViewDal(): this(Initialization.GetXmlConfig(typeof(EvaluationDetailsView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static EvaluationDetailsViewDal CreateDal()
        {
			return new EvaluationDetailsViewDal(Initialization.GetXmlConfig(typeof(EvaluationDetailsView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static EvaluationDetailsViewDal()
        {
           ModelName= typeof(EvaluationDetailsView).Name;
           var item = new EvaluationDetailsView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public EvaluationDetailsViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "EvaluationDetailsView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public EvaluationDetailsView GetById(int value)
        {
            var model = new EvaluationDetailsView { Id = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public EvaluationDetailsView GetById(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Id));
        }

        #endregion

                
        #region RaterId

        /// <summary>
        /// 根据属性RaterId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByRaterId(int value)
        {
            var model = new EvaluationDetailsView { RaterId = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.RaterId));
        }

        /// <summary>
        /// 根据属性RaterId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByRaterId(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.RaterId));
        }

        #endregion

                
        #region TargetUserId

        /// <summary>
        /// 根据属性TargetUserId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByTargetUserId(int value)
        {
            var model = new EvaluationDetailsView { TargetUserId = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.TargetUserId));
        }

        /// <summary>
        /// 根据属性TargetUserId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByTargetUserId(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.TargetUserId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByContent(string value)
        {
            var model = new EvaluationDetailsView { Content = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByContent(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Content));
        }

        #endregion

                
        #region EvaluationTime

        /// <summary>
        /// 根据属性EvaluationTime获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByEvaluationTime(DateTime value)
        {
            var model = new EvaluationDetailsView { EvaluationTime = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.EvaluationTime));
        }

        /// <summary>
        /// 根据属性EvaluationTime获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByEvaluationTime(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.EvaluationTime));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByVerification(int value)
        {
            var model = new EvaluationDetailsView { Verification = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByVerification(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Verification));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByVerifier(int value)
        {
            var model = new EvaluationDetailsView { Verifier = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByVerifier(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.Verifier));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByVerificationTime(DateTime value)
        {
            var model = new EvaluationDetailsView { VerificationTime = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByVerificationTime(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.VerificationTime));
        }

        #endregion

                
        #region OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByOrderId(int value)
        {
            var model = new EvaluationDetailsView { OrderId = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.OrderId));
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByOrderId(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.OrderId));
        }

        #endregion

                
        #region RaterLOginId

        /// <summary>
        /// 根据属性RaterLOginId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByRaterLOginId(string value)
        {
            var model = new EvaluationDetailsView { RaterLOginId = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.RaterLOginId));
        }

        /// <summary>
        /// 根据属性RaterLOginId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByRaterLOginId(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.RaterLOginId));
        }

        #endregion

                
        #region RaterAvatar

        /// <summary>
        /// 根据属性RaterAvatar获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByRaterAvatar(string value)
        {
            var model = new EvaluationDetailsView { RaterAvatar = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.RaterAvatar));
        }

        /// <summary>
        /// 根据属性RaterAvatar获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByRaterAvatar(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.RaterAvatar));
        }

        #endregion

                
        #region TargetUserLoginId

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByTargetUserLoginId(string value)
        {
            var model = new EvaluationDetailsView { TargetUserLoginId = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.TargetUserLoginId));
        }

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByTargetUserLoginId(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.TargetUserLoginId));
        }

        #endregion

                
        #region TargetUserAvatar

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByTargetUserAvatar(string value)
        {
            var model = new EvaluationDetailsView { TargetUserAvatar = value };
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.TargetUserAvatar));
        }

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体
        /// </summary>
        public EvaluationDetailsView GetByTargetUserAvatar(EvaluationDetailsView model)
        {
            return GetByWhere(model.GetFilterString(EvaluationDetailsViewField.TargetUserAvatar));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By RaterId

        /// <summary>
        /// 根据属性RaterId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByRaterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { RaterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.RaterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RaterId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByRaterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.RaterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserId

        /// <summary>
        /// 根据属性TargetUserId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByTargetUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { TargetUserId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.TargetUserId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByTargetUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.TargetUserId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By EvaluationTime

        /// <summary>
        /// 根据属性EvaluationTime获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByEvaluationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { EvaluationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.EvaluationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EvaluationTime获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByEvaluationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.EvaluationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { OrderId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.OrderId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.OrderId),sort,operateMode);
        }

        #endregion

                
        #region GetList By RaterLOginId

        /// <summary>
        /// 根据属性RaterLOginId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByRaterLOginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { RaterLOginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.RaterLOginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RaterLOginId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByRaterLOginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.RaterLOginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By RaterAvatar

        /// <summary>
        /// 根据属性RaterAvatar获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByRaterAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { RaterAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.RaterAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RaterAvatar获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByRaterAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.RaterAvatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserLoginId

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByTargetUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { TargetUserLoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.TargetUserLoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByTargetUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.TargetUserLoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserAvatar

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByTargetUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new EvaluationDetailsView { TargetUserAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.TargetUserAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体列表
        /// </summary>
        public IList<EvaluationDetailsView> GetListByTargetUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, EvaluationDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationDetailsViewField.TargetUserAvatar),sort,operateMode);
        }

        #endregion

        #endregion
    }
}