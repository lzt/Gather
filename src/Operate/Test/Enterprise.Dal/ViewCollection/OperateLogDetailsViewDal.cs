﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class OperateLogDetailsViewDal : ExViewBaseDal<OperateLogDetailsView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public OperateLogDetailsViewDal(): this(Initialization.GetXmlConfig(typeof(OperateLogDetailsView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static OperateLogDetailsViewDal CreateDal()
        {
			return new OperateLogDetailsViewDal(Initialization.GetXmlConfig(typeof(OperateLogDetailsView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static OperateLogDetailsViewDal()
        {
           ModelName= typeof(OperateLogDetailsView).Name;
           var item = new OperateLogDetailsView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public OperateLogDetailsViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "OperateLogDetailsView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public OperateLogDetailsView GetById(int value)
        {
            var model = new OperateLogDetailsView { Id = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public OperateLogDetailsView GetById(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Id));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByUserProfileId(int value)
        {
            var model = new OperateLogDetailsView { UserProfileId = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByUserProfileId(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.UserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByContent(string value)
        {
            var model = new OperateLogDetailsView { Content = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByContent(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Content));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByType(int value)
        {
            var model = new OperateLogDetailsView { Type = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByType(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Type));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByUrl(string value)
        {
            var model = new OperateLogDetailsView { Url = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByUrl(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Url));
        }

        #endregion

                
        #region FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByFormParams(string value)
        {
            var model = new OperateLogDetailsView { FormParams = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.FormParams));
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByFormParams(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.FormParams));
        }

        #endregion

                
        #region UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByUrlParams(string value)
        {
            var model = new OperateLogDetailsView { UrlParams = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.UrlParams));
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByUrlParams(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.UrlParams));
        }

        #endregion

                
        #region Host

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByHost(string value)
        {
            var model = new OperateLogDetailsView { Host = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Host));
        }

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByHost(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Host));
        }

        #endregion

                
        #region Header

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByHeader(string value)
        {
            var model = new OperateLogDetailsView { Header = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Header));
        }

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByHeader(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Header));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByTitle(string value)
        {
            var model = new OperateLogDetailsView { Title = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByTitle(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Title));
        }

        #endregion

                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByLoginId(string value)
        {
            var model = new OperateLogDetailsView { LoginId = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByLoginId(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.LoginId));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByNickName(string value)
        {
            var model = new OperateLogDetailsView { NickName = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByNickName(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.NickName));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByTrueName(string value)
        {
            var model = new OperateLogDetailsView { TrueName = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByTrueName(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.TrueName));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByAvatar(string value)
        {
            var model = new OperateLogDetailsView { Avatar = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByAvatar(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Avatar));
        }

        #endregion

                
        #region Email

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByEmail(string value)
        {
            var model = new OperateLogDetailsView { Email = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Email));
        }

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByEmail(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Email));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByIdentification(string value)
        {
            var model = new OperateLogDetailsView { Identification = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByIdentification(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Identification));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByPhone(string value)
        {
            var model = new OperateLogDetailsView { Phone = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByPhone(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Phone));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public OperateLogDetailsView GetBySex(int value)
        {
            var model = new OperateLogDetailsView { Sex = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public OperateLogDetailsView GetBySex(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Sex));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByAppId(string value)
        {
            var model = new OperateLogDetailsView { AppId = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByAppId(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByAppSecret(string value)
        {
            var model = new OperateLogDetailsView { AppSecret = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByAppSecret(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.AppSecret));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByIp(string value)
        {
            var model = new OperateLogDetailsView { Ip = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByIp(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByCreateBy(long value)
        {
            var model = new OperateLogDetailsView { CreateBy = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByCreateBy(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByCreateTime(DateTime value)
        {
            var model = new OperateLogDetailsView { CreateTime = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByCreateTime(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByLastModifyBy(long value)
        {
            var model = new OperateLogDetailsView { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByLastModifyBy(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByLastModifyTime(DateTime value)
        {
            var model = new OperateLogDetailsView { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public OperateLogDetailsView GetByLastModifyTime(OperateLogDetailsView model)
        {
            return GetByWhere(model.GetFilterString(OperateLogDetailsViewField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { FormParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.FormParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.FormParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { UrlParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.UrlParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.UrlParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By Host

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Host = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Host),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Host),sort,operateMode);
        }

        #endregion

                
        #region GetList By Header

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Header = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Header),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Header),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By Email

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Email = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Email),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Email),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLogDetailsView { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<OperateLogDetailsView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLogDetailsView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogDetailsViewField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}