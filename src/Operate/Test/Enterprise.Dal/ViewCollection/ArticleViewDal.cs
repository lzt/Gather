﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class ArticleViewDal : ExViewBaseDal<ArticleView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ArticleViewDal(): this(Initialization.GetXmlConfig(typeof(ArticleView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ArticleViewDal CreateDal()
        {
			return new ArticleViewDal(Initialization.GetXmlConfig(typeof(ArticleView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ArticleViewDal()
        {
           ModelName= typeof(ArticleView).Name;
           var item = new ArticleView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ArticleViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ArticleView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public ArticleView GetById(int value)
        {
            var model = new ArticleView { Id = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public ArticleView GetById(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.Id));
        }

        #endregion

                
        #region InfoSectionId

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体
        /// </summary>
        public ArticleView GetByInfoSectionId(int value)
        {
            var model = new ArticleView { InfoSectionId = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.InfoSectionId));
        }

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体
        /// </summary>
        public ArticleView GetByInfoSectionId(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.InfoSectionId));
        }

        #endregion

                
        #region InfoEntryId

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体
        /// </summary>
        public ArticleView GetByInfoEntryId(int value)
        {
            var model = new ArticleView { InfoEntryId = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.InfoEntryId));
        }

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体
        /// </summary>
        public ArticleView GetByInfoEntryId(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.InfoEntryId));
        }

        #endregion

                
        #region ImageUrl

        /// <summary>
        /// 根据属性ImageUrl获取数据实体
        /// </summary>
        public ArticleView GetByImageUrl(string value)
        {
            var model = new ArticleView { ImageUrl = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.ImageUrl));
        }

        /// <summary>
        /// 根据属性ImageUrl获取数据实体
        /// </summary>
        public ArticleView GetByImageUrl(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.ImageUrl));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public ArticleView GetByTitle(string value)
        {
            var model = new ArticleView { Title = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public ArticleView GetByTitle(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.Title));
        }

        #endregion

                
        #region Introduction

        /// <summary>
        /// 根据属性Introduction获取数据实体
        /// </summary>
        public ArticleView GetByIntroduction(string value)
        {
            var model = new ArticleView { Introduction = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.Introduction));
        }

        /// <summary>
        /// 根据属性Introduction获取数据实体
        /// </summary>
        public ArticleView GetByIntroduction(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.Introduction));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public ArticleView GetByContent(string value)
        {
            var model = new ArticleView { Content = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public ArticleView GetByContent(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.Content));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ArticleView GetByCreateTime(DateTime value)
        {
            var model = new ArticleView { CreateTime = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ArticleView GetByCreateTime(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.CreateTime));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ArticleView GetByLastModifyTime(DateTime value)
        {
            var model = new ArticleView { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ArticleView GetByLastModifyTime(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.LastModifyTime));
        }

        #endregion

                
        #region ReadCount

        /// <summary>
        /// 根据属性ReadCount获取数据实体
        /// </summary>
        public ArticleView GetByReadCount(int value)
        {
            var model = new ArticleView { ReadCount = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.ReadCount));
        }

        /// <summary>
        /// 根据属性ReadCount获取数据实体
        /// </summary>
        public ArticleView GetByReadCount(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.ReadCount));
        }

        #endregion

                
        #region OrderValue

        /// <summary>
        /// 根据属性OrderValue获取数据实体
        /// </summary>
        public ArticleView GetByOrderValue(int value)
        {
            var model = new ArticleView { OrderValue = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.OrderValue));
        }

        /// <summary>
        /// 根据属性OrderValue获取数据实体
        /// </summary>
        public ArticleView GetByOrderValue(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.OrderValue));
        }

        #endregion

                
        #region StatusRecommend

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体
        /// </summary>
        public ArticleView GetByStatusRecommend(int value)
        {
            var model = new ArticleView { StatusRecommend = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.StatusRecommend));
        }

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体
        /// </summary>
        public ArticleView GetByStatusRecommend(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.StatusRecommend));
        }

        #endregion

                
        #region StatusTop

        /// <summary>
        /// 根据属性StatusTop获取数据实体
        /// </summary>
        public ArticleView GetByStatusTop(int value)
        {
            var model = new ArticleView { StatusTop = value };
            return GetByWhere(model.GetFilterString(ArticleViewField.StatusTop));
        }

        /// <summary>
        /// 根据属性StatusTop获取数据实体
        /// </summary>
        public ArticleView GetByStatusTop(ArticleView model)
        {
            return GetByWhere(model.GetFilterString(ArticleViewField.StatusTop));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By InfoSectionId

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByInfoSectionId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { InfoSectionId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.InfoSectionId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByInfoSectionId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.InfoSectionId),sort,operateMode);
        }

        #endregion

                
        #region GetList By InfoEntryId

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByInfoEntryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { InfoEntryId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.InfoEntryId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByInfoEntryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.InfoEntryId),sort,operateMode);
        }

        #endregion

                
        #region GetList By ImageUrl

        /// <summary>
        /// 根据属性ImageUrl获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByImageUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { ImageUrl = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.ImageUrl),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ImageUrl获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByImageUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.ImageUrl),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Introduction

        /// <summary>
        /// 根据属性Introduction获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByIntroduction(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { Introduction = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Introduction),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Introduction获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByIntroduction(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Introduction),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.LastModifyTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By ReadCount

        /// <summary>
        /// 根据属性ReadCount获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByReadCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { ReadCount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.ReadCount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ReadCount获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByReadCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.ReadCount),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrderValue

        /// <summary>
        /// 根据属性OrderValue获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByOrderValue(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { OrderValue = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.OrderValue),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrderValue获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByOrderValue(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.OrderValue),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusRecommend

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByStatusRecommend(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { StatusRecommend = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.StatusRecommend),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByStatusRecommend(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.StatusRecommend),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusTop

        /// <summary>
        /// 根据属性StatusTop获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByStatusTop(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ArticleView { StatusTop = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.StatusTop),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusTop获取数据实体列表
        /// </summary>
        public IList<ArticleView> GetListByStatusTop(int currentPage, int pagesize, out long totalPages, out long totalRecords, ArticleView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ArticleViewField.StatusTop),sort,operateMode);
        }

        #endregion

        #endregion
    }
}