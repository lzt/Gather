﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserMessageViewDal : ExViewBaseDal<UserMessageView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserMessageViewDal(): this(Initialization.GetXmlConfig(typeof(UserMessageView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserMessageViewDal CreateDal()
        {
			return new UserMessageViewDal(Initialization.GetXmlConfig(typeof(UserMessageView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserMessageViewDal()
        {
           ModelName= typeof(UserMessageView).Name;
           var item = new UserMessageView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserMessageViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserMessageView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserMessageView GetById(int value)
        {
            var model = new UserMessageView { Id = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserMessageView GetById(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.Id));
        }

        #endregion

                
        #region SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public UserMessageView GetBySourceUserProfileId(int value)
        {
            var model = new UserMessageView { SourceUserProfileId = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.SourceUserProfileId));
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public UserMessageView GetBySourceUserProfileId(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.SourceUserProfileId));
        }

        #endregion

                
        #region TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public UserMessageView GetByTargetUserProfileId(int value)
        {
            var model = new UserMessageView { TargetUserProfileId = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetUserProfileId));
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public UserMessageView GetByTargetUserProfileId(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetUserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public UserMessageView GetByContent(string value)
        {
            var model = new UserMessageView { Content = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public UserMessageView GetByContent(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.Content));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public UserMessageView GetByType(int value)
        {
            var model = new UserMessageView { Type = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public UserMessageView GetByType(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.Type));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserMessageView GetByCreateTime(DateTime value)
        {
            var model = new UserMessageView { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserMessageView GetByCreateTime(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.CreateTime));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserMessageView GetByTitle(string value)
        {
            var model = new UserMessageView { Title = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserMessageView GetByTitle(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.Title));
        }

        #endregion

                
        #region TargetStatusRead

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体
        /// </summary>
        public UserMessageView GetByTargetStatusRead(int value)
        {
            var model = new UserMessageView { TargetStatusRead = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetStatusRead));
        }

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体
        /// </summary>
        public UserMessageView GetByTargetStatusRead(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetStatusRead));
        }

        #endregion

                
        #region TargetStatusDelete

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体
        /// </summary>
        public UserMessageView GetByTargetStatusDelete(int value)
        {
            var model = new UserMessageView { TargetStatusDelete = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetStatusDelete));
        }

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体
        /// </summary>
        public UserMessageView GetByTargetStatusDelete(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetStatusDelete));
        }

        #endregion

                
        #region SourceUserLoginId

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体
        /// </summary>
        public UserMessageView GetBySourceUserLoginId(string value)
        {
            var model = new UserMessageView { SourceUserLoginId = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.SourceUserLoginId));
        }

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体
        /// </summary>
        public UserMessageView GetBySourceUserLoginId(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.SourceUserLoginId));
        }

        #endregion

                
        #region SourceUserAvatar

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体
        /// </summary>
        public UserMessageView GetBySourceUserAvatar(string value)
        {
            var model = new UserMessageView { SourceUserAvatar = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.SourceUserAvatar));
        }

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体
        /// </summary>
        public UserMessageView GetBySourceUserAvatar(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.SourceUserAvatar));
        }

        #endregion

                
        #region TargetUserLoginId

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体
        /// </summary>
        public UserMessageView GetByTargetUserLoginId(string value)
        {
            var model = new UserMessageView { TargetUserLoginId = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetUserLoginId));
        }

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体
        /// </summary>
        public UserMessageView GetByTargetUserLoginId(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetUserLoginId));
        }

        #endregion

                
        #region TargetUserAvatar

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体
        /// </summary>
        public UserMessageView GetByTargetUserAvatar(string value)
        {
            var model = new UserMessageView { TargetUserAvatar = value };
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetUserAvatar));
        }

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体
        /// </summary>
        public UserMessageView GetByTargetUserAvatar(UserMessageView model)
        {
            return GetByWhere(model.GetFilterString(UserMessageViewField.TargetUserAvatar));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { SourceUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.SourceUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.SourceUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { TargetUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetStatusRead

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { TargetStatusRead = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetStatusRead),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetStatusRead),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetStatusDelete

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { TargetStatusDelete = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetStatusDelete),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetStatusDelete),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceUserLoginId

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListBySourceUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { SourceUserLoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.SourceUserLoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserLoginId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListBySourceUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.SourceUserLoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceUserAvatar

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListBySourceUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { SourceUserAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.SourceUserAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserAvatar获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListBySourceUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.SourceUserAvatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserLoginId

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { TargetUserLoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetUserLoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserLoginId获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetUserLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetUserLoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserAvatar

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessageView { TargetUserAvatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetUserAvatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserAvatar获取数据实体列表
        /// </summary>
        public IList<UserMessageView> GetListByTargetUserAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessageView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageViewField.TargetUserAvatar),sort,operateMode);
        }

        #endregion

        #endregion
    }
}