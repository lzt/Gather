﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.ViewEntity;

namespace Enterprise.Dal.ViewCollection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserRoleGroupViewDal : ExViewBaseDal<UserRoleGroupView>
    {
        #region Fields

        private static readonly string ModelName = "";

        private static readonly string ModelXml = "";

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserRoleGroupViewDal(): this(Initialization.GetXmlConfig(typeof(UserRoleGroupView)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserRoleGroupViewDal CreateDal()
        {
			return new UserRoleGroupViewDal(Initialization.GetXmlConfig(typeof(UserRoleGroupView)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserRoleGroupViewDal()
        {
           ModelName= typeof(UserRoleGroupView).Name;
           var item = new UserRoleGroupView();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserRoleGroupViewDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserRoleGroupView";
			PrimarykeyName="Id";
        }

        #region Search By Filter
        
        #region Id

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserRoleGroupView GetById(int value)
        {
            var model = new UserRoleGroupView { Id = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Id));
        }

        /// <summary>
        /// 根据属性Id获取数据实体
        /// </summary>
        public UserRoleGroupView GetById(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Id));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserRoleGroupView GetByTitle(string value)
        {
            var model = new UserRoleGroupView { Title = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserRoleGroupView GetByTitle(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Title));
        }

        #endregion

                
        #region RoleList

        /// <summary>
        /// 根据属性RoleList获取数据实体
        /// </summary>
        public UserRoleGroupView GetByRoleList(string value)
        {
            var model = new UserRoleGroupView { RoleList = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.RoleList));
        }

        /// <summary>
        /// 根据属性RoleList获取数据实体
        /// </summary>
        public UserRoleGroupView GetByRoleList(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.RoleList));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserRoleGroupView GetByStatusEnabled(int value)
        {
            var model = new UserRoleGroupView { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserRoleGroupView GetByStatusEnabled(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.StatusEnabled));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserRoleGroupView GetByRemark(string value)
        {
            var model = new UserRoleGroupView { Remark = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserRoleGroupView GetByRemark(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Remark));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserRoleGroupView GetByParentId(long value)
        {
            var model = new UserRoleGroupView { ParentId = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserRoleGroupView GetByParentId(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.ParentId));
        }

        #endregion

                
        #region Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserRoleGroupView GetByPurpose(int value)
        {
            var model = new UserRoleGroupView { Purpose = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Purpose));
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserRoleGroupView GetByPurpose(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Purpose));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRoleGroupView GetByIp(string value)
        {
            var model = new UserRoleGroupView { Ip = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRoleGroupView GetByIp(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRoleGroupView GetByCreateBy(long value)
        {
            var model = new UserRoleGroupView { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRoleGroupView GetByCreateBy(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRoleGroupView GetByCreateTime(DateTime value)
        {
            var model = new UserRoleGroupView { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRoleGroupView GetByCreateTime(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRoleGroupView GetByLastModifyBy(long value)
        {
            var model = new UserRoleGroupView { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRoleGroupView GetByLastModifyBy(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRoleGroupView GetByLastModifyTime(DateTime value)
        {
            var model = new UserRoleGroupView { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRoleGroupView GetByLastModifyTime(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.LastModifyTime));
        }

        #endregion

                
        #region UsedCount

        /// <summary>
        /// 根据属性UsedCount获取数据实体
        /// </summary>
        public UserRoleGroupView GetByUsedCount(long value)
        {
            var model = new UserRoleGroupView { UsedCount = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.UsedCount));
        }

        /// <summary>
        /// 根据属性UsedCount获取数据实体
        /// </summary>
        public UserRoleGroupView GetByUsedCount(UserRoleGroupView model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupViewField.UsedCount));
        }

        #endregion

        #endregion

        #region GetList By Filter
        
        #region GetList By Id

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { Id = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Id),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Id获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListById(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Id),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By RoleList

        /// <summary>
        /// 根据属性RoleList获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByRoleList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { RoleList = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.RoleList),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RoleList获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByRoleList(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.RoleList),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { Purpose = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Purpose),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Purpose),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.LastModifyTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By UsedCount

        /// <summary>
        /// 根据属性UsedCount获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByUsedCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroupView { UsedCount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.UsedCount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UsedCount获取数据实体列表
        /// </summary>
        public IList<UserRoleGroupView> GetListByUsedCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroupView model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupViewField.UsedCount),sort,operateMode);
        }

        #endregion

        #endregion
    }
}