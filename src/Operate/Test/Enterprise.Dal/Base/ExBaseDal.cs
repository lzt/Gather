﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.Model;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter;

namespace Enterprise.Dal.Base
{
    public class ExBaseDal<T> : SqliteBaseDal<T>  where T :  BaseEntity, new()
    {
	    public ExBaseDal(string hibernateCfgFilePath)
            : base(hibernateCfgFilePath)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

        public ExBaseDal(string hibernateCfgFilePath, string mappingName, string mappingXml)
            : base(hibernateCfgFilePath, mappingName,mappingXml)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

		public ExBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

        public ExBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
            : base(configContent, configFileType, mappingName, mappingXml)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

        void ConfigInfo_ConnectionStringChanged(object sender,EventArgs e)
        {
            if (!string.IsNullOrEmpty(Initialization.NHibernateConfigContent))
            {
                Initialization.NHibernateConfigContent = NibernateConfig.ChangeConnectionToXmlConfig(Initialization.NHibernateConfigContent, Config.ConnectionString);
            }
        }

        /// <summary>
        /// 清除配置文档内容缓存
        /// </summary>
        public void ClearConFigCache()
        {
            Initialization.NHibernateConfigContent = null;
        }
    }
}
