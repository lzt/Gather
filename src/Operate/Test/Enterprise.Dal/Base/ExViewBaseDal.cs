﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using System.Data;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.Model;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;

namespace Enterprise.Dal.Base
{
    public class ExViewBaseDal<T> : SqliteBaseDal<T>  where T :  BaseEntity, new()
    {
	    public ExViewBaseDal(string hibernateCfgFilePath)
            : base(hibernateCfgFilePath)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

        public ExViewBaseDal(string hibernateCfgFilePath, string mappingName, string mappingXml)
            : base(hibernateCfgFilePath, mappingName,mappingXml)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

		public ExViewBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

        public ExViewBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
            : base(configContent, configFileType, mappingName, mappingXml)
        {
            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
        }

        void ConfigInfo_ConnectionStringChanged(object sender,EventArgs e)
        {
            if (!string.IsNullOrEmpty(Initialization.NHibernateConfigContent))
            {
                Initialization.NHibernateConfigContent = NibernateConfig.ChangeConnectionToXmlConfig(Initialization.NHibernateConfigContent, Config.ConnectionString);
            }
        }

        /// <summary>
        /// 清除配置文档内容缓存
        /// </summary>
        public void ClearConFigCache()
        {
            Initialization.NHibernateConfigContent = null;
        }

        #region Disabled

         #region Insert

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItem">将要插入的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Insert(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelJson">将要插入的数据实体Json</param>
        /// <param name="operateMode"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Insert(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Insert(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 更新DataTable
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="row"></param>
        /// <param name="columes"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void InsertByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 插入DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void InsertByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Update(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据Json</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Update(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Update(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 更新DataRow
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="row"></param>
        /// <param name="columes"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void UpdateByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 更新DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将更新的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>     
        [Obsolete("此方法不能在视图中使用", true)]
        public override void UpdateByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        #endregion

        #region Delete

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="primaryValue">即将要删除的数据主键</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void DeleteByPrimarykey(object primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Delete(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Delete(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItemList">将要删除的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void Delete(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void DeleteByWhere(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 删除DataRow
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void DeleteByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 删除DataRow
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="primaryName">主键名称</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>
        /// <param name="ar"></param>   
        [Obsolete("此方法不能在视图中使用", true)]
        public override void DeleteByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        #endregion

        #region Async

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncInsert(T modelChatLog, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="executeAysncInsertFinished">是否执行AysncInsertFinished</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncInsert(T modelChatLog, bool executeAysncInsertFinished, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncUpdate(T modelChatLog, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="executeAysncUpdateFinished">是否执行AysncUpdateFinished</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncUpdate(T modelChatLog, bool executeAysncUpdateFinished, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncDelete(T modelChatLog, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="executeAysncDeleteFinished">是否执行AysncDeleteFinished</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncDelete(T modelChatLog, bool executeAysncDeleteFinished, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncCustom(string sql,  IDataParameter[] parameters, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="executeAysncCustomFinished">是否执行AysncCustomFinished</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override bool AsyncCustom(string sql, bool executeAysncCustomFinished,  IDataParameter[] parameters, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        #endregion

        #region RunSql

        /// <summary>
        /// 执行Sql语句 , 返回第一行第一列
        /// </summary>
        /// <param name="sql">存储过程名</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override object ExecuteSqlScalar(string sql,  IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 执行Sql语句  无返回结果
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void ExecuteSqlNonQuery(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行Sql语句 返回Json字符串
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override string ExecuteSqlToJson(string sql, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行Sql语句 返回DataTable
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override DataTable ExecuteSqlToDatatable(string sql, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行sql语句 返回List Model
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override IList<T> ExecuteSqlToList(string sql, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行sql语句 返回Model
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override T ExecuteSqlToModel(string sql, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        #endregion

        #region RunProcess

        /// <summary>
        /// 执行存储过程（附带参数）  返回第一行第一列
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override object ExecuteProcessScalar(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        /// <summary>
        /// 执行存储过程（附带参数）  无返回结果
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        [Obsolete("此方法不能在视图中使用", true)]
        public override void ExecuteProcessNonQuery(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行Sql语句 返回Json字符串
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override string ExecuteProcessToJson(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行存储过程（附带参数） 返回DataTable
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override DataTable ExecuteProcessToDatatable(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        ///  执行存储过程（附带参数）DataSet
        ///  </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override DataSet ExecuteProcessToDataset(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行存储过程（附带参数） 返回List Model
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override IList<T> ExecuteProcessToList(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        ///  <summary>
        /// 执行存储过程（附带参数） 返回Model
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        ///  <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        [Obsolete("此方法不能在视图中使用", true)]
        public override T ExecuteProcessToModel(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)
        {
            throw new Exception("此方法不能在视图中使用");
        }

        #endregion

        #endregion
    }
}
