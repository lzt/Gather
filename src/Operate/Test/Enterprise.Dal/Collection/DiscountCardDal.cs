﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class DiscountCardDal : ExBaseDal<DiscountCard>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public DiscountCardDal(): this(Initialization.GetXmlConfig(typeof(DiscountCard)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static DiscountCardDal CreateDal()
        {
			return new DiscountCardDal(Initialization.GetXmlConfig(typeof(DiscountCard)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static DiscountCardDal()
        {
           ModelName= typeof(DiscountCard).Name;
           var item = new DiscountCard();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public DiscountCardDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "DiscountCard";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public DiscountCard GetByUserProfileId(int value)
        {
            var model = new DiscountCard { UserProfileId = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public DiscountCard GetByUserProfileId(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.UserProfileId));
        }

        #endregion

                
        #region Denomination

        /// <summary>
        /// 根据属性Denomination获取数据实体
        /// </summary>
        public DiscountCard GetByDenomination(int value)
        {
            var model = new DiscountCard { Denomination = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.Denomination));
        }

        /// <summary>
        /// 根据属性Denomination获取数据实体
        /// </summary>
        public DiscountCard GetByDenomination(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.Denomination));
        }

        #endregion

                
        #region StatusUsed

        /// <summary>
        /// 根据属性StatusUsed获取数据实体
        /// </summary>
        public DiscountCard GetByStatusUsed(int value)
        {
            var model = new DiscountCard { StatusUsed = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.StatusUsed));
        }

        /// <summary>
        /// 根据属性StatusUsed获取数据实体
        /// </summary>
        public DiscountCard GetByStatusUsed(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.StatusUsed));
        }

        #endregion

                
        #region StartTime

        /// <summary>
        /// 根据属性StartTime获取数据实体
        /// </summary>
        public DiscountCard GetByStartTime(DateTime value)
        {
            var model = new DiscountCard { StartTime = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.StartTime));
        }

        /// <summary>
        /// 根据属性StartTime获取数据实体
        /// </summary>
        public DiscountCard GetByStartTime(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.StartTime));
        }

        #endregion

                
        #region EndTime

        /// <summary>
        /// 根据属性EndTime获取数据实体
        /// </summary>
        public DiscountCard GetByEndTime(DateTime value)
        {
            var model = new DiscountCard { EndTime = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.EndTime));
        }

        /// <summary>
        /// 根据属性EndTime获取数据实体
        /// </summary>
        public DiscountCard GetByEndTime(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.EndTime));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public DiscountCard GetByType(int value)
        {
            var model = new DiscountCard { Type = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public DiscountCard GetByType(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.Type));
        }

        #endregion

                
        #region RechargeOrderId

        /// <summary>
        /// 根据属性RechargeOrderId获取数据实体
        /// </summary>
        public DiscountCard GetByRechargeOrderId(int value)
        {
            var model = new DiscountCard { RechargeOrderId = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.RechargeOrderId));
        }

        /// <summary>
        /// 根据属性RechargeOrderId获取数据实体
        /// </summary>
        public DiscountCard GetByRechargeOrderId(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.RechargeOrderId));
        }

        #endregion

                
        #region CardNumber

        /// <summary>
        /// 根据属性CardNumber获取数据实体
        /// </summary>
        public DiscountCard GetByCardNumber(string value)
        {
            var model = new DiscountCard { CardNumber = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.CardNumber));
        }

        /// <summary>
        /// 根据属性CardNumber获取数据实体
        /// </summary>
        public DiscountCard GetByCardNumber(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.CardNumber));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public DiscountCard GetByIp(string value)
        {
            var model = new DiscountCard { Ip = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public DiscountCard GetByIp(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public DiscountCard GetByCreateBy(long value)
        {
            var model = new DiscountCard { CreateBy = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public DiscountCard GetByCreateBy(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public DiscountCard GetByCreateTime(DateTime value)
        {
            var model = new DiscountCard { CreateTime = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public DiscountCard GetByCreateTime(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public DiscountCard GetByLastModifyBy(long value)
        {
            var model = new DiscountCard { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public DiscountCard GetByLastModifyBy(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public DiscountCard GetByLastModifyTime(DateTime value)
        {
            var model = new DiscountCard { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(DiscountCardField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public DiscountCard GetByLastModifyTime(DiscountCard model)
        {
            return GetByWhere(model.GetFilterString(DiscountCardField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Denomination

        /// <summary>
        /// 根据属性Denomination获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { Denomination = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.Denomination),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Denomination获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.Denomination),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusUsed

        /// <summary>
        /// 根据属性StatusUsed获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByStatusUsed(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { StatusUsed = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.StatusUsed),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusUsed获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByStatusUsed(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.StatusUsed),sort,operateMode);
        }

        #endregion

                
        #region GetList By StartTime

        /// <summary>
        /// 根据属性StartTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByStartTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { StartTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.StartTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StartTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByStartTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.StartTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By EndTime

        /// <summary>
        /// 根据属性EndTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByEndTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { EndTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.EndTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EndTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByEndTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.EndTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By RechargeOrderId

        /// <summary>
        /// 根据属性RechargeOrderId获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByRechargeOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { RechargeOrderId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.RechargeOrderId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RechargeOrderId获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByRechargeOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.RechargeOrderId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CardNumber

        /// <summary>
        /// 根据属性CardNumber获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByCardNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { CardNumber = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.CardNumber),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CardNumber获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByCardNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.CardNumber),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new DiscountCard { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<DiscountCard> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DiscountCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(DiscountCardField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}