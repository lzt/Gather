﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SystemMessageDal : ExBaseDal<SystemMessage>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SystemMessageDal(): this(Initialization.GetXmlConfig(typeof(SystemMessage)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SystemMessageDal CreateDal()
        {
			return new SystemMessageDal(Initialization.GetXmlConfig(typeof(SystemMessage)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SystemMessageDal()
        {
           ModelName= typeof(SystemMessage).Name;
           var item = new SystemMessage();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SystemMessageDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SystemMessage";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SystemMessage GetByContent(string value)
        {
            var model = new SystemMessage { Content = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SystemMessage GetByContent(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.Content));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public SystemMessage GetByType(int value)
        {
            var model = new SystemMessage { Type = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public SystemMessage GetByType(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.Type));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public SystemMessage GetByTitle(string value)
        {
            var model = new SystemMessage { Title = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public SystemMessage GetByTitle(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.Title));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SystemMessage GetByIp(string value)
        {
            var model = new SystemMessage { Ip = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SystemMessage GetByIp(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SystemMessage GetByCreateBy(long value)
        {
            var model = new SystemMessage { CreateBy = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SystemMessage GetByCreateBy(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SystemMessage GetByCreateTime(DateTime value)
        {
            var model = new SystemMessage { CreateTime = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SystemMessage GetByCreateTime(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SystemMessage GetByLastModifyBy(long value)
        {
            var model = new SystemMessage { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SystemMessage GetByLastModifyBy(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SystemMessage GetByLastModifyTime(DateTime value)
        {
            var model = new SystemMessage { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(SystemMessageField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SystemMessage GetByLastModifyTime(SystemMessage model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessage { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SystemMessage> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}