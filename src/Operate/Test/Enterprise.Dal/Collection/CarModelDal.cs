﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class CarModelDal : ExBaseDal<CarModel>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CarModelDal(): this(Initialization.GetXmlConfig(typeof(CarModel)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CarModelDal CreateDal()
        {
			return new CarModelDal(Initialization.GetXmlConfig(typeof(CarModel)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CarModelDal()
        {
           ModelName= typeof(CarModel).Name;
           var item = new CarModel();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CarModelDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "CarModel";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region ModelId

        /// <summary>
        /// 根据属性ModelId获取数据实体
        /// </summary>
        public CarModel GetByModelId(int value)
        {
            var model = new CarModel { ModelId = value };
            return GetByWhere(model.GetFilterString(CarModelField.ModelId));
        }

        /// <summary>
        /// 根据属性ModelId获取数据实体
        /// </summary>
        public CarModel GetByModelId(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.ModelId));
        }

        #endregion

                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarModel GetByName(string value)
        {
            var model = new CarModel { Name = value };
            return GetByWhere(model.GetFilterString(CarModelField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarModel GetByName(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Name));
        }

        #endregion

                
        #region SeriesId

        /// <summary>
        /// 根据属性SeriesId获取数据实体
        /// </summary>
        public CarModel GetBySeriesId(int value)
        {
            var model = new CarModel { SeriesId = value };
            return GetByWhere(model.GetFilterString(CarModelField.SeriesId));
        }

        /// <summary>
        /// 根据属性SeriesId获取数据实体
        /// </summary>
        public CarModel GetBySeriesId(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SeriesId));
        }

        #endregion

                
        #region ModelName

        /// <summary>
        /// 根据属性ModelName获取数据实体
        /// </summary>
        public CarModel GetByModelName(string value)
        {
            var model = new CarModel { ModelName = value };
            return GetByWhere(model.GetFilterString(CarModelField.ModelName));
        }

        /// <summary>
        /// 根据属性ModelName获取数据实体
        /// </summary>
        public CarModel GetByModelName(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.ModelName));
        }

        #endregion

                
        #region Year

        /// <summary>
        /// 根据属性Year获取数据实体
        /// </summary>
        public CarModel GetByYear(int value)
        {
            var model = new CarModel { Year = value };
            return GetByWhere(model.GetFilterString(CarModelField.Year));
        }

        /// <summary>
        /// 根据属性Year获取数据实体
        /// </summary>
        public CarModel GetByYear(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Year));
        }

        #endregion

                
        #region Displ

        /// <summary>
        /// 根据属性Displ获取数据实体
        /// </summary>
        public CarModel GetByDispl(double value)
        {
            var model = new CarModel { Displ = value };
            return GetByWhere(model.GetFilterString(CarModelField.Displ));
        }

        /// <summary>
        /// 根据属性Displ获取数据实体
        /// </summary>
        public CarModel GetByDispl(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Displ));
        }

        #endregion

                
        #region Price

        /// <summary>
        /// 根据属性Price获取数据实体
        /// </summary>
        public CarModel GetByPrice(double value)
        {
            var model = new CarModel { Price = value };
            return GetByWhere(model.GetFilterString(CarModelField.Price));
        }

        /// <summary>
        /// 根据属性Price获取数据实体
        /// </summary>
        public CarModel GetByPrice(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Price));
        }

        #endregion

                
        #region Gear

        /// <summary>
        /// 根据属性Gear获取数据实体
        /// </summary>
        public CarModel GetByGear(string value)
        {
            var model = new CarModel { Gear = value };
            return GetByWhere(model.GetFilterString(CarModelField.Gear));
        }

        /// <summary>
        /// 根据属性Gear获取数据实体
        /// </summary>
        public CarModel GetByGear(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Gear));
        }

        #endregion

                
        #region Status

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public CarModel GetByStatus(int value)
        {
            var model = new CarModel { Status = value };
            return GetByWhere(model.GetFilterString(CarModelField.Status));
        }

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public CarModel GetByStatus(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Status));
        }

        #endregion

                
        #region SIP_C_102

        /// <summary>
        /// 根据属性SIP_C_102获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_102(string value)
        {
            var model = new CarModel { SIP_C_102 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_102));
        }

        /// <summary>
        /// 根据属性SIP_C_102获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_102(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_102));
        }

        #endregion

                
        #region SIP_C_103

        /// <summary>
        /// 根据属性SIP_C_103获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_103(string value)
        {
            var model = new CarModel { SIP_C_103 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_103));
        }

        /// <summary>
        /// 根据属性SIP_C_103获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_103(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_103));
        }

        #endregion

                
        #region SIP_C_104

        /// <summary>
        /// 根据属性SIP_C_104获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_104(string value)
        {
            var model = new CarModel { SIP_C_104 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_104));
        }

        /// <summary>
        /// 根据属性SIP_C_104获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_104(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_104));
        }

        #endregion

                
        #region SIP_C_105

        /// <summary>
        /// 根据属性SIP_C_105获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_105(string value)
        {
            var model = new CarModel { SIP_C_105 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_105));
        }

        /// <summary>
        /// 根据属性SIP_C_105获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_105(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_105));
        }

        #endregion

                
        #region SIP_C_106

        /// <summary>
        /// 根据属性SIP_C_106获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_106(string value)
        {
            var model = new CarModel { SIP_C_106 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_106));
        }

        /// <summary>
        /// 根据属性SIP_C_106获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_106(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_106));
        }

        #endregion

                
        #region SIP_C_293

        /// <summary>
        /// 根据属性SIP_C_293获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_293(string value)
        {
            var model = new CarModel { SIP_C_293 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_293));
        }

        /// <summary>
        /// 根据属性SIP_C_293获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_293(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_293));
        }

        #endregion

                
        #region SIP_C_107

        /// <summary>
        /// 根据属性SIP_C_107获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_107(string value)
        {
            var model = new CarModel { SIP_C_107 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_107));
        }

        /// <summary>
        /// 根据属性SIP_C_107获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_107(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_107));
        }

        #endregion

                
        #region SIP_C_108

        /// <summary>
        /// 根据属性SIP_C_108获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_108(string value)
        {
            var model = new CarModel { SIP_C_108 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_108));
        }

        /// <summary>
        /// 根据属性SIP_C_108获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_108(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_108));
        }

        #endregion

                
        #region SIP_C_303

        /// <summary>
        /// 根据属性SIP_C_303获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_303(string value)
        {
            var model = new CarModel { SIP_C_303 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_303));
        }

        /// <summary>
        /// 根据属性SIP_C_303获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_303(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_303));
        }

        #endregion

                
        #region SIP_C_112

        /// <summary>
        /// 根据属性SIP_C_112获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_112(string value)
        {
            var model = new CarModel { SIP_C_112 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_112));
        }

        /// <summary>
        /// 根据属性SIP_C_112获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_112(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_112));
        }

        #endregion

                
        #region SIP_C_294

        /// <summary>
        /// 根据属性SIP_C_294获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_294(string value)
        {
            var model = new CarModel { SIP_C_294 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_294));
        }

        /// <summary>
        /// 根据属性SIP_C_294获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_294(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_294));
        }

        #endregion

                
        #region SIP_C_113

        /// <summary>
        /// 根据属性SIP_C_113获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_113(string value)
        {
            var model = new CarModel { SIP_C_113 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_113));
        }

        /// <summary>
        /// 根据属性SIP_C_113获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_113(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_113));
        }

        #endregion

                
        #region SIP_C_114

        /// <summary>
        /// 根据属性SIP_C_114获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_114(string value)
        {
            var model = new CarModel { SIP_C_114 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_114));
        }

        /// <summary>
        /// 根据属性SIP_C_114获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_114(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_114));
        }

        #endregion

                
        #region SIP_C_304

        /// <summary>
        /// 根据属性SIP_C_304获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_304(string value)
        {
            var model = new CarModel { SIP_C_304 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_304));
        }

        /// <summary>
        /// 根据属性SIP_C_304获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_304(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_304));
        }

        #endregion

                
        #region SIP_C_115

        /// <summary>
        /// 根据属性SIP_C_115获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_115(string value)
        {
            var model = new CarModel { SIP_C_115 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_115));
        }

        /// <summary>
        /// 根据属性SIP_C_115获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_115(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_115));
        }

        #endregion

                
        #region SIP_C_116

        /// <summary>
        /// 根据属性SIP_C_116获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_116(string value)
        {
            var model = new CarModel { SIP_C_116 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_116));
        }

        /// <summary>
        /// 根据属性SIP_C_116获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_116(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_116));
        }

        #endregion

                
        #region SIP_C_295

        /// <summary>
        /// 根据属性SIP_C_295获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_295(string value)
        {
            var model = new CarModel { SIP_C_295 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_295));
        }

        /// <summary>
        /// 根据属性SIP_C_295获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_295(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_295));
        }

        #endregion

                
        #region SIP_C_117

        /// <summary>
        /// 根据属性SIP_C_117获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_117(string value)
        {
            var model = new CarModel { SIP_C_117 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_117));
        }

        /// <summary>
        /// 根据属性SIP_C_117获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_117(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_117));
        }

        #endregion

                
        #region SIP_C_118

        /// <summary>
        /// 根据属性SIP_C_118获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_118(string value)
        {
            var model = new CarModel { SIP_C_118 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_118));
        }

        /// <summary>
        /// 根据属性SIP_C_118获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_118(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_118));
        }

        #endregion

                
        #region SIP_C_119

        /// <summary>
        /// 根据属性SIP_C_119获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_119(string value)
        {
            var model = new CarModel { SIP_C_119 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_119));
        }

        /// <summary>
        /// 根据属性SIP_C_119获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_119(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_119));
        }

        #endregion

                
        #region SIP_C_120

        /// <summary>
        /// 根据属性SIP_C_120获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_120(string value)
        {
            var model = new CarModel { SIP_C_120 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_120));
        }

        /// <summary>
        /// 根据属性SIP_C_120获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_120(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_120));
        }

        #endregion

                
        #region SIP_C_121

        /// <summary>
        /// 根据属性SIP_C_121获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_121(string value)
        {
            var model = new CarModel { SIP_C_121 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_121));
        }

        /// <summary>
        /// 根据属性SIP_C_121获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_121(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_121));
        }

        #endregion

                
        #region SIP_C_122

        /// <summary>
        /// 根据属性SIP_C_122获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_122(string value)
        {
            var model = new CarModel { SIP_C_122 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_122));
        }

        /// <summary>
        /// 根据属性SIP_C_122获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_122(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_122));
        }

        #endregion

                
        #region SIP_C_123

        /// <summary>
        /// 根据属性SIP_C_123获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_123(string value)
        {
            var model = new CarModel { SIP_C_123 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_123));
        }

        /// <summary>
        /// 根据属性SIP_C_123获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_123(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_123));
        }

        #endregion

                
        #region SIP_C_124

        /// <summary>
        /// 根据属性SIP_C_124获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_124(string value)
        {
            var model = new CarModel { SIP_C_124 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_124));
        }

        /// <summary>
        /// 根据属性SIP_C_124获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_124(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_124));
        }

        #endregion

                
        #region SIP_C_125

        /// <summary>
        /// 根据属性SIP_C_125获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_125(string value)
        {
            var model = new CarModel { SIP_C_125 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_125));
        }

        /// <summary>
        /// 根据属性SIP_C_125获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_125(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_125));
        }

        #endregion

                
        #region SIP_C_126

        /// <summary>
        /// 根据属性SIP_C_126获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_126(string value)
        {
            var model = new CarModel { SIP_C_126 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_126));
        }

        /// <summary>
        /// 根据属性SIP_C_126获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_126(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_126));
        }

        #endregion

                
        #region SIP_C_127

        /// <summary>
        /// 根据属性SIP_C_127获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_127(string value)
        {
            var model = new CarModel { SIP_C_127 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_127));
        }

        /// <summary>
        /// 根据属性SIP_C_127获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_127(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_127));
        }

        #endregion

                
        #region SIP_C_128

        /// <summary>
        /// 根据属性SIP_C_128获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_128(string value)
        {
            var model = new CarModel { SIP_C_128 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_128));
        }

        /// <summary>
        /// 根据属性SIP_C_128获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_128(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_128));
        }

        #endregion

                
        #region SIP_C_129

        /// <summary>
        /// 根据属性SIP_C_129获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_129(string value)
        {
            var model = new CarModel { SIP_C_129 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_129));
        }

        /// <summary>
        /// 根据属性SIP_C_129获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_129(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_129));
        }

        #endregion

                
        #region SIP_C_130

        /// <summary>
        /// 根据属性SIP_C_130获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_130(string value)
        {
            var model = new CarModel { SIP_C_130 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_130));
        }

        /// <summary>
        /// 根据属性SIP_C_130获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_130(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_130));
        }

        #endregion

                
        #region SIP_C_131

        /// <summary>
        /// 根据属性SIP_C_131获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_131(string value)
        {
            var model = new CarModel { SIP_C_131 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_131));
        }

        /// <summary>
        /// 根据属性SIP_C_131获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_131(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_131));
        }

        #endregion

                
        #region SIP_C_132

        /// <summary>
        /// 根据属性SIP_C_132获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_132(string value)
        {
            var model = new CarModel { SIP_C_132 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_132));
        }

        /// <summary>
        /// 根据属性SIP_C_132获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_132(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_132));
        }

        #endregion

                
        #region SIP_C_133

        /// <summary>
        /// 根据属性SIP_C_133获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_133(string value)
        {
            var model = new CarModel { SIP_C_133 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_133));
        }

        /// <summary>
        /// 根据属性SIP_C_133获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_133(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_133));
        }

        #endregion

                
        #region SIP_C_134

        /// <summary>
        /// 根据属性SIP_C_134获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_134(string value)
        {
            var model = new CarModel { SIP_C_134 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_134));
        }

        /// <summary>
        /// 根据属性SIP_C_134获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_134(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_134));
        }

        #endregion

                
        #region SIP_C_135

        /// <summary>
        /// 根据属性SIP_C_135获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_135(string value)
        {
            var model = new CarModel { SIP_C_135 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_135));
        }

        /// <summary>
        /// 根据属性SIP_C_135获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_135(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_135));
        }

        #endregion

                
        #region SIP_C_136

        /// <summary>
        /// 根据属性SIP_C_136获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_136(string value)
        {
            var model = new CarModel { SIP_C_136 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_136));
        }

        /// <summary>
        /// 根据属性SIP_C_136获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_136(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_136));
        }

        #endregion

                
        #region SIP_C_137

        /// <summary>
        /// 根据属性SIP_C_137获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_137(string value)
        {
            var model = new CarModel { SIP_C_137 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_137));
        }

        /// <summary>
        /// 根据属性SIP_C_137获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_137(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_137));
        }

        #endregion

                
        #region SIP_C_138

        /// <summary>
        /// 根据属性SIP_C_138获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_138(string value)
        {
            var model = new CarModel { SIP_C_138 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_138));
        }

        /// <summary>
        /// 根据属性SIP_C_138获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_138(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_138));
        }

        #endregion

                
        #region SIP_C_139

        /// <summary>
        /// 根据属性SIP_C_139获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_139(string value)
        {
            var model = new CarModel { SIP_C_139 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_139));
        }

        /// <summary>
        /// 根据属性SIP_C_139获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_139(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_139));
        }

        #endregion

                
        #region SIP_C_140

        /// <summary>
        /// 根据属性SIP_C_140获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_140(string value)
        {
            var model = new CarModel { SIP_C_140 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_140));
        }

        /// <summary>
        /// 根据属性SIP_C_140获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_140(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_140));
        }

        #endregion

                
        #region SIP_C_141

        /// <summary>
        /// 根据属性SIP_C_141获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_141(string value)
        {
            var model = new CarModel { SIP_C_141 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_141));
        }

        /// <summary>
        /// 根据属性SIP_C_141获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_141(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_141));
        }

        #endregion

                
        #region SIP_C_142

        /// <summary>
        /// 根据属性SIP_C_142获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_142(string value)
        {
            var model = new CarModel { SIP_C_142 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_142));
        }

        /// <summary>
        /// 根据属性SIP_C_142获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_142(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_142));
        }

        #endregion

                
        #region SIP_C_143

        /// <summary>
        /// 根据属性SIP_C_143获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_143(string value)
        {
            var model = new CarModel { SIP_C_143 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_143));
        }

        /// <summary>
        /// 根据属性SIP_C_143获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_143(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_143));
        }

        #endregion

                
        #region SIP_C_297

        /// <summary>
        /// 根据属性SIP_C_297获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_297(string value)
        {
            var model = new CarModel { SIP_C_297 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_297));
        }

        /// <summary>
        /// 根据属性SIP_C_297获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_297(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_297));
        }

        #endregion

                
        #region SIP_C_298

        /// <summary>
        /// 根据属性SIP_C_298获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_298(string value)
        {
            var model = new CarModel { SIP_C_298 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_298));
        }

        /// <summary>
        /// 根据属性SIP_C_298获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_298(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_298));
        }

        #endregion

                
        #region SIP_C_299

        /// <summary>
        /// 根据属性SIP_C_299获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_299(string value)
        {
            var model = new CarModel { SIP_C_299 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_299));
        }

        /// <summary>
        /// 根据属性SIP_C_299获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_299(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_299));
        }

        #endregion

                
        #region SIP_C_148

        /// <summary>
        /// 根据属性SIP_C_148获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_148(string value)
        {
            var model = new CarModel { SIP_C_148 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_148));
        }

        /// <summary>
        /// 根据属性SIP_C_148获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_148(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_148));
        }

        #endregion

                
        #region SIP_C_305

        /// <summary>
        /// 根据属性SIP_C_305获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_305(string value)
        {
            var model = new CarModel { SIP_C_305 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_305));
        }

        /// <summary>
        /// 根据属性SIP_C_305获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_305(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_305));
        }

        #endregion

                
        #region SIP_C_306

        /// <summary>
        /// 根据属性SIP_C_306获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_306(string value)
        {
            var model = new CarModel { SIP_C_306 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_306));
        }

        /// <summary>
        /// 根据属性SIP_C_306获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_306(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_306));
        }

        #endregion

                
        #region SIP_C_307

        /// <summary>
        /// 根据属性SIP_C_307获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_307(string value)
        {
            var model = new CarModel { SIP_C_307 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_307));
        }

        /// <summary>
        /// 根据属性SIP_C_307获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_307(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_307));
        }

        #endregion

                
        #region SIP_C_308

        /// <summary>
        /// 根据属性SIP_C_308获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_308(string value)
        {
            var model = new CarModel { SIP_C_308 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_308));
        }

        /// <summary>
        /// 根据属性SIP_C_308获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_308(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_308));
        }

        #endregion

                
        #region SIP_C_309

        /// <summary>
        /// 根据属性SIP_C_309获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_309(string value)
        {
            var model = new CarModel { SIP_C_309 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_309));
        }

        /// <summary>
        /// 根据属性SIP_C_309获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_309(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_309));
        }

        #endregion

                
        #region SIP_C_310

        /// <summary>
        /// 根据属性SIP_C_310获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_310(string value)
        {
            var model = new CarModel { SIP_C_310 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_310));
        }

        /// <summary>
        /// 根据属性SIP_C_310获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_310(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_310));
        }

        #endregion

                
        #region SIP_C_311

        /// <summary>
        /// 根据属性SIP_C_311获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_311(string value)
        {
            var model = new CarModel { SIP_C_311 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_311));
        }

        /// <summary>
        /// 根据属性SIP_C_311获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_311(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_311));
        }

        #endregion

                
        #region SIP_C_149

        /// <summary>
        /// 根据属性SIP_C_149获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_149(string value)
        {
            var model = new CarModel { SIP_C_149 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_149));
        }

        /// <summary>
        /// 根据属性SIP_C_149获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_149(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_149));
        }

        #endregion

                
        #region SIP_C_150

        /// <summary>
        /// 根据属性SIP_C_150获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_150(string value)
        {
            var model = new CarModel { SIP_C_150 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_150));
        }

        /// <summary>
        /// 根据属性SIP_C_150获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_150(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_150));
        }

        #endregion

                
        #region SIP_C_151

        /// <summary>
        /// 根据属性SIP_C_151获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_151(string value)
        {
            var model = new CarModel { SIP_C_151 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_151));
        }

        /// <summary>
        /// 根据属性SIP_C_151获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_151(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_151));
        }

        #endregion

                
        #region SIP_C_152

        /// <summary>
        /// 根据属性SIP_C_152获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_152(string value)
        {
            var model = new CarModel { SIP_C_152 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_152));
        }

        /// <summary>
        /// 根据属性SIP_C_152获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_152(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_152));
        }

        #endregion

                
        #region SIP_C_153

        /// <summary>
        /// 根据属性SIP_C_153获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_153(string value)
        {
            var model = new CarModel { SIP_C_153 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_153));
        }

        /// <summary>
        /// 根据属性SIP_C_153获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_153(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_153));
        }

        #endregion

                
        #region SIP_C_154

        /// <summary>
        /// 根据属性SIP_C_154获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_154(string value)
        {
            var model = new CarModel { SIP_C_154 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_154));
        }

        /// <summary>
        /// 根据属性SIP_C_154获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_154(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_154));
        }

        #endregion

                
        #region SIP_C_155

        /// <summary>
        /// 根据属性SIP_C_155获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_155(string value)
        {
            var model = new CarModel { SIP_C_155 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_155));
        }

        /// <summary>
        /// 根据属性SIP_C_155获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_155(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_155));
        }

        #endregion

                
        #region SIP_C_156

        /// <summary>
        /// 根据属性SIP_C_156获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_156(string value)
        {
            var model = new CarModel { SIP_C_156 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_156));
        }

        /// <summary>
        /// 根据属性SIP_C_156获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_156(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_156));
        }

        #endregion

                
        #region SIP_C_157

        /// <summary>
        /// 根据属性SIP_C_157获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_157(string value)
        {
            var model = new CarModel { SIP_C_157 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_157));
        }

        /// <summary>
        /// 根据属性SIP_C_157获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_157(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_157));
        }

        #endregion

                
        #region SIP_C_158

        /// <summary>
        /// 根据属性SIP_C_158获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_158(string value)
        {
            var model = new CarModel { SIP_C_158 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_158));
        }

        /// <summary>
        /// 根据属性SIP_C_158获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_158(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_158));
        }

        #endregion

                
        #region SIP_C_159

        /// <summary>
        /// 根据属性SIP_C_159获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_159(string value)
        {
            var model = new CarModel { SIP_C_159 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_159));
        }

        /// <summary>
        /// 根据属性SIP_C_159获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_159(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_159));
        }

        #endregion

                
        #region SIP_C_160

        /// <summary>
        /// 根据属性SIP_C_160获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_160(string value)
        {
            var model = new CarModel { SIP_C_160 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_160));
        }

        /// <summary>
        /// 根据属性SIP_C_160获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_160(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_160));
        }

        #endregion

                
        #region SIP_C_161

        /// <summary>
        /// 根据属性SIP_C_161获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_161(string value)
        {
            var model = new CarModel { SIP_C_161 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_161));
        }

        /// <summary>
        /// 根据属性SIP_C_161获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_161(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_161));
        }

        #endregion

                
        #region SIP_C_162

        /// <summary>
        /// 根据属性SIP_C_162获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_162(string value)
        {
            var model = new CarModel { SIP_C_162 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_162));
        }

        /// <summary>
        /// 根据属性SIP_C_162获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_162(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_162));
        }

        #endregion

                
        #region SIP_C_163

        /// <summary>
        /// 根据属性SIP_C_163获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_163(string value)
        {
            var model = new CarModel { SIP_C_163 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_163));
        }

        /// <summary>
        /// 根据属性SIP_C_163获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_163(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_163));
        }

        #endregion

                
        #region SIP_C_164

        /// <summary>
        /// 根据属性SIP_C_164获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_164(string value)
        {
            var model = new CarModel { SIP_C_164 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_164));
        }

        /// <summary>
        /// 根据属性SIP_C_164获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_164(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_164));
        }

        #endregion

                
        #region SIP_C_165

        /// <summary>
        /// 根据属性SIP_C_165获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_165(string value)
        {
            var model = new CarModel { SIP_C_165 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_165));
        }

        /// <summary>
        /// 根据属性SIP_C_165获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_165(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_165));
        }

        #endregion

                
        #region SIP_C_166

        /// <summary>
        /// 根据属性SIP_C_166获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_166(string value)
        {
            var model = new CarModel { SIP_C_166 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_166));
        }

        /// <summary>
        /// 根据属性SIP_C_166获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_166(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_166));
        }

        #endregion

                
        #region SIP_C_167

        /// <summary>
        /// 根据属性SIP_C_167获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_167(string value)
        {
            var model = new CarModel { SIP_C_167 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_167));
        }

        /// <summary>
        /// 根据属性SIP_C_167获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_167(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_167));
        }

        #endregion

                
        #region SIP_C_168

        /// <summary>
        /// 根据属性SIP_C_168获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_168(string value)
        {
            var model = new CarModel { SIP_C_168 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_168));
        }

        /// <summary>
        /// 根据属性SIP_C_168获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_168(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_168));
        }

        #endregion

                
        #region SIP_C_169

        /// <summary>
        /// 根据属性SIP_C_169获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_169(string value)
        {
            var model = new CarModel { SIP_C_169 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_169));
        }

        /// <summary>
        /// 根据属性SIP_C_169获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_169(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_169));
        }

        #endregion

                
        #region SIP_C_170

        /// <summary>
        /// 根据属性SIP_C_170获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_170(string value)
        {
            var model = new CarModel { SIP_C_170 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_170));
        }

        /// <summary>
        /// 根据属性SIP_C_170获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_170(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_170));
        }

        #endregion

                
        #region SIP_C_171

        /// <summary>
        /// 根据属性SIP_C_171获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_171(string value)
        {
            var model = new CarModel { SIP_C_171 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_171));
        }

        /// <summary>
        /// 根据属性SIP_C_171获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_171(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_171));
        }

        #endregion

                
        #region SIP_C_172

        /// <summary>
        /// 根据属性SIP_C_172获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_172(string value)
        {
            var model = new CarModel { SIP_C_172 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_172));
        }

        /// <summary>
        /// 根据属性SIP_C_172获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_172(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_172));
        }

        #endregion

                
        #region SIP_C_173

        /// <summary>
        /// 根据属性SIP_C_173获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_173(string value)
        {
            var model = new CarModel { SIP_C_173 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_173));
        }

        /// <summary>
        /// 根据属性SIP_C_173获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_173(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_173));
        }

        #endregion

                
        #region SIP_C_174

        /// <summary>
        /// 根据属性SIP_C_174获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_174(string value)
        {
            var model = new CarModel { SIP_C_174 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_174));
        }

        /// <summary>
        /// 根据属性SIP_C_174获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_174(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_174));
        }

        #endregion

                
        #region SIP_C_177

        /// <summary>
        /// 根据属性SIP_C_177获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_177(string value)
        {
            var model = new CarModel { SIP_C_177 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_177));
        }

        /// <summary>
        /// 根据属性SIP_C_177获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_177(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_177));
        }

        #endregion

                
        #region SIP_C_178

        /// <summary>
        /// 根据属性SIP_C_178获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_178(string value)
        {
            var model = new CarModel { SIP_C_178 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_178));
        }

        /// <summary>
        /// 根据属性SIP_C_178获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_178(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_178));
        }

        #endregion

                
        #region SIP_C_179

        /// <summary>
        /// 根据属性SIP_C_179获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_179(string value)
        {
            var model = new CarModel { SIP_C_179 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_179));
        }

        /// <summary>
        /// 根据属性SIP_C_179获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_179(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_179));
        }

        #endregion

                
        #region SIP_C_180

        /// <summary>
        /// 根据属性SIP_C_180获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_180(string value)
        {
            var model = new CarModel { SIP_C_180 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_180));
        }

        /// <summary>
        /// 根据属性SIP_C_180获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_180(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_180));
        }

        #endregion

                
        #region SIP_C_181

        /// <summary>
        /// 根据属性SIP_C_181获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_181(string value)
        {
            var model = new CarModel { SIP_C_181 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_181));
        }

        /// <summary>
        /// 根据属性SIP_C_181获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_181(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_181));
        }

        #endregion

                
        #region SIP_C_183

        /// <summary>
        /// 根据属性SIP_C_183获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_183(string value)
        {
            var model = new CarModel { SIP_C_183 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_183));
        }

        /// <summary>
        /// 根据属性SIP_C_183获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_183(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_183));
        }

        #endregion

                
        #region SIP_C_184

        /// <summary>
        /// 根据属性SIP_C_184获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_184(string value)
        {
            var model = new CarModel { SIP_C_184 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_184));
        }

        /// <summary>
        /// 根据属性SIP_C_184获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_184(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_184));
        }

        #endregion

                
        #region SIP_C_185

        /// <summary>
        /// 根据属性SIP_C_185获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_185(string value)
        {
            var model = new CarModel { SIP_C_185 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_185));
        }

        /// <summary>
        /// 根据属性SIP_C_185获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_185(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_185));
        }

        #endregion

                
        #region SIP_C_186

        /// <summary>
        /// 根据属性SIP_C_186获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_186(string value)
        {
            var model = new CarModel { SIP_C_186 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_186));
        }

        /// <summary>
        /// 根据属性SIP_C_186获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_186(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_186));
        }

        #endregion

                
        #region SIP_C_187

        /// <summary>
        /// 根据属性SIP_C_187获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_187(string value)
        {
            var model = new CarModel { SIP_C_187 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_187));
        }

        /// <summary>
        /// 根据属性SIP_C_187获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_187(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_187));
        }

        #endregion

                
        #region SIP_C_188

        /// <summary>
        /// 根据属性SIP_C_188获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_188(string value)
        {
            var model = new CarModel { SIP_C_188 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_188));
        }

        /// <summary>
        /// 根据属性SIP_C_188获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_188(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_188));
        }

        #endregion

                
        #region SIP_C_189

        /// <summary>
        /// 根据属性SIP_C_189获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_189(string value)
        {
            var model = new CarModel { SIP_C_189 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_189));
        }

        /// <summary>
        /// 根据属性SIP_C_189获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_189(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_189));
        }

        #endregion

                
        #region SIP_C_190

        /// <summary>
        /// 根据属性SIP_C_190获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_190(string value)
        {
            var model = new CarModel { SIP_C_190 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_190));
        }

        /// <summary>
        /// 根据属性SIP_C_190获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_190(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_190));
        }

        #endregion

                
        #region SIP_C_191

        /// <summary>
        /// 根据属性SIP_C_191获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_191(string value)
        {
            var model = new CarModel { SIP_C_191 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_191));
        }

        /// <summary>
        /// 根据属性SIP_C_191获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_191(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_191));
        }

        #endregion

                
        #region SIP_C_192

        /// <summary>
        /// 根据属性SIP_C_192获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_192(string value)
        {
            var model = new CarModel { SIP_C_192 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_192));
        }

        /// <summary>
        /// 根据属性SIP_C_192获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_192(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_192));
        }

        #endregion

                
        #region SIP_C_193

        /// <summary>
        /// 根据属性SIP_C_193获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_193(string value)
        {
            var model = new CarModel { SIP_C_193 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_193));
        }

        /// <summary>
        /// 根据属性SIP_C_193获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_193(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_193));
        }

        #endregion

                
        #region SIP_C_194

        /// <summary>
        /// 根据属性SIP_C_194获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_194(string value)
        {
            var model = new CarModel { SIP_C_194 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_194));
        }

        /// <summary>
        /// 根据属性SIP_C_194获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_194(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_194));
        }

        #endregion

                
        #region SIP_C_195

        /// <summary>
        /// 根据属性SIP_C_195获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_195(string value)
        {
            var model = new CarModel { SIP_C_195 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_195));
        }

        /// <summary>
        /// 根据属性SIP_C_195获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_195(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_195));
        }

        #endregion

                
        #region SIP_C_196

        /// <summary>
        /// 根据属性SIP_C_196获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_196(string value)
        {
            var model = new CarModel { SIP_C_196 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_196));
        }

        /// <summary>
        /// 根据属性SIP_C_196获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_196(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_196));
        }

        #endregion

                
        #region SIP_C_197

        /// <summary>
        /// 根据属性SIP_C_197获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_197(string value)
        {
            var model = new CarModel { SIP_C_197 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_197));
        }

        /// <summary>
        /// 根据属性SIP_C_197获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_197(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_197));
        }

        #endregion

                
        #region SIP_C_198

        /// <summary>
        /// 根据属性SIP_C_198获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_198(string value)
        {
            var model = new CarModel { SIP_C_198 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_198));
        }

        /// <summary>
        /// 根据属性SIP_C_198获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_198(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_198));
        }

        #endregion

                
        #region SIP_C_199

        /// <summary>
        /// 根据属性SIP_C_199获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_199(string value)
        {
            var model = new CarModel { SIP_C_199 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_199));
        }

        /// <summary>
        /// 根据属性SIP_C_199获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_199(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_199));
        }

        #endregion

                
        #region SIP_C_204

        /// <summary>
        /// 根据属性SIP_C_204获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_204(string value)
        {
            var model = new CarModel { SIP_C_204 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_204));
        }

        /// <summary>
        /// 根据属性SIP_C_204获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_204(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_204));
        }

        #endregion

                
        #region SIP_C_205

        /// <summary>
        /// 根据属性SIP_C_205获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_205(string value)
        {
            var model = new CarModel { SIP_C_205 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_205));
        }

        /// <summary>
        /// 根据属性SIP_C_205获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_205(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_205));
        }

        #endregion

                
        #region SIP_C_312

        /// <summary>
        /// 根据属性SIP_C_312获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_312(string value)
        {
            var model = new CarModel { SIP_C_312 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_312));
        }

        /// <summary>
        /// 根据属性SIP_C_312获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_312(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_312));
        }

        #endregion

                
        #region SIP_C_313

        /// <summary>
        /// 根据属性SIP_C_313获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_313(string value)
        {
            var model = new CarModel { SIP_C_313 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_313));
        }

        /// <summary>
        /// 根据属性SIP_C_313获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_313(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_313));
        }

        #endregion

                
        #region SIP_C_314

        /// <summary>
        /// 根据属性SIP_C_314获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_314(string value)
        {
            var model = new CarModel { SIP_C_314 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_314));
        }

        /// <summary>
        /// 根据属性SIP_C_314获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_314(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_314));
        }

        #endregion

                
        #region SIP_C_315

        /// <summary>
        /// 根据属性SIP_C_315获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_315(string value)
        {
            var model = new CarModel { SIP_C_315 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_315));
        }

        /// <summary>
        /// 根据属性SIP_C_315获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_315(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_315));
        }

        #endregion

                
        #region SIP_C_316

        /// <summary>
        /// 根据属性SIP_C_316获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_316(string value)
        {
            var model = new CarModel { SIP_C_316 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_316));
        }

        /// <summary>
        /// 根据属性SIP_C_316获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_316(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_316));
        }

        #endregion

                
        #region SIP_C_210

        /// <summary>
        /// 根据属性SIP_C_210获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_210(string value)
        {
            var model = new CarModel { SIP_C_210 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_210));
        }

        /// <summary>
        /// 根据属性SIP_C_210获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_210(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_210));
        }

        #endregion

                
        #region SIP_C_211

        /// <summary>
        /// 根据属性SIP_C_211获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_211(string value)
        {
            var model = new CarModel { SIP_C_211 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_211));
        }

        /// <summary>
        /// 根据属性SIP_C_211获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_211(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_211));
        }

        #endregion

                
        #region SIP_C_212

        /// <summary>
        /// 根据属性SIP_C_212获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_212(string value)
        {
            var model = new CarModel { SIP_C_212 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_212));
        }

        /// <summary>
        /// 根据属性SIP_C_212获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_212(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_212));
        }

        #endregion

                
        #region SIP_C_300

        /// <summary>
        /// 根据属性SIP_C_300获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_300(string value)
        {
            var model = new CarModel { SIP_C_300 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_300));
        }

        /// <summary>
        /// 根据属性SIP_C_300获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_300(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_300));
        }

        #endregion

                
        #region SIP_C_213

        /// <summary>
        /// 根据属性SIP_C_213获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_213(string value)
        {
            var model = new CarModel { SIP_C_213 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_213));
        }

        /// <summary>
        /// 根据属性SIP_C_213获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_213(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_213));
        }

        #endregion

                
        #region SIP_C_214

        /// <summary>
        /// 根据属性SIP_C_214获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_214(string value)
        {
            var model = new CarModel { SIP_C_214 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_214));
        }

        /// <summary>
        /// 根据属性SIP_C_214获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_214(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_214));
        }

        #endregion

                
        #region SIP_C_215

        /// <summary>
        /// 根据属性SIP_C_215获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_215(string value)
        {
            var model = new CarModel { SIP_C_215 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_215));
        }

        /// <summary>
        /// 根据属性SIP_C_215获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_215(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_215));
        }

        #endregion

                
        #region SIP_C_216

        /// <summary>
        /// 根据属性SIP_C_216获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_216(string value)
        {
            var model = new CarModel { SIP_C_216 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_216));
        }

        /// <summary>
        /// 根据属性SIP_C_216获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_216(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_216));
        }

        #endregion

                
        #region SIP_C_217

        /// <summary>
        /// 根据属性SIP_C_217获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_217(string value)
        {
            var model = new CarModel { SIP_C_217 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_217));
        }

        /// <summary>
        /// 根据属性SIP_C_217获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_217(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_217));
        }

        #endregion

                
        #region SIP_C_218

        /// <summary>
        /// 根据属性SIP_C_218获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_218(string value)
        {
            var model = new CarModel { SIP_C_218 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_218));
        }

        /// <summary>
        /// 根据属性SIP_C_218获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_218(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_218));
        }

        #endregion

                
        #region SIP_C_175

        /// <summary>
        /// 根据属性SIP_C_175获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_175(string value)
        {
            var model = new CarModel { SIP_C_175 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_175));
        }

        /// <summary>
        /// 根据属性SIP_C_175获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_175(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_175));
        }

        #endregion

                
        #region SIP_C_176

        /// <summary>
        /// 根据属性SIP_C_176获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_176(string value)
        {
            var model = new CarModel { SIP_C_176 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_176));
        }

        /// <summary>
        /// 根据属性SIP_C_176获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_176(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_176));
        }

        #endregion

                
        #region SIP_C_219

        /// <summary>
        /// 根据属性SIP_C_219获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_219(string value)
        {
            var model = new CarModel { SIP_C_219 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_219));
        }

        /// <summary>
        /// 根据属性SIP_C_219获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_219(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_219));
        }

        #endregion

                
        #region SIP_C_220

        /// <summary>
        /// 根据属性SIP_C_220获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_220(string value)
        {
            var model = new CarModel { SIP_C_220 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_220));
        }

        /// <summary>
        /// 根据属性SIP_C_220获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_220(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_220));
        }

        #endregion

                
        #region SIP_C_200

        /// <summary>
        /// 根据属性SIP_C_200获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_200(string value)
        {
            var model = new CarModel { SIP_C_200 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_200));
        }

        /// <summary>
        /// 根据属性SIP_C_200获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_200(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_200));
        }

        #endregion

                
        #region SIP_C_201

        /// <summary>
        /// 根据属性SIP_C_201获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_201(string value)
        {
            var model = new CarModel { SIP_C_201 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_201));
        }

        /// <summary>
        /// 根据属性SIP_C_201获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_201(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_201));
        }

        #endregion

                
        #region SIP_C_202

        /// <summary>
        /// 根据属性SIP_C_202获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_202(string value)
        {
            var model = new CarModel { SIP_C_202 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_202));
        }

        /// <summary>
        /// 根据属性SIP_C_202获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_202(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_202));
        }

        #endregion

                
        #region SIP_C_203

        /// <summary>
        /// 根据属性SIP_C_203获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_203(string value)
        {
            var model = new CarModel { SIP_C_203 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_203));
        }

        /// <summary>
        /// 根据属性SIP_C_203获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_203(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_203));
        }

        #endregion

                
        #region SIP_C_221

        /// <summary>
        /// 根据属性SIP_C_221获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_221(string value)
        {
            var model = new CarModel { SIP_C_221 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_221));
        }

        /// <summary>
        /// 根据属性SIP_C_221获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_221(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_221));
        }

        #endregion

                
        #region SIP_C_222

        /// <summary>
        /// 根据属性SIP_C_222获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_222(string value)
        {
            var model = new CarModel { SIP_C_222 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_222));
        }

        /// <summary>
        /// 根据属性SIP_C_222获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_222(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_222));
        }

        #endregion

                
        #region SIP_C_223

        /// <summary>
        /// 根据属性SIP_C_223获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_223(string value)
        {
            var model = new CarModel { SIP_C_223 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_223));
        }

        /// <summary>
        /// 根据属性SIP_C_223获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_223(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_223));
        }

        #endregion

                
        #region SIP_C_301

        /// <summary>
        /// 根据属性SIP_C_301获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_301(string value)
        {
            var model = new CarModel { SIP_C_301 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_301));
        }

        /// <summary>
        /// 根据属性SIP_C_301获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_301(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_301));
        }

        #endregion

                
        #region SIP_C_224

        /// <summary>
        /// 根据属性SIP_C_224获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_224(string value)
        {
            var model = new CarModel { SIP_C_224 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_224));
        }

        /// <summary>
        /// 根据属性SIP_C_224获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_224(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_224));
        }

        #endregion

                
        #region SIP_C_225

        /// <summary>
        /// 根据属性SIP_C_225获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_225(string value)
        {
            var model = new CarModel { SIP_C_225 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_225));
        }

        /// <summary>
        /// 根据属性SIP_C_225获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_225(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_225));
        }

        #endregion

                
        #region SIP_C_226

        /// <summary>
        /// 根据属性SIP_C_226获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_226(string value)
        {
            var model = new CarModel { SIP_C_226 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_226));
        }

        /// <summary>
        /// 根据属性SIP_C_226获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_226(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_226));
        }

        #endregion

                
        #region SIP_C_227

        /// <summary>
        /// 根据属性SIP_C_227获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_227(string value)
        {
            var model = new CarModel { SIP_C_227 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_227));
        }

        /// <summary>
        /// 根据属性SIP_C_227获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_227(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_227));
        }

        #endregion

                
        #region SIP_C_228

        /// <summary>
        /// 根据属性SIP_C_228获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_228(string value)
        {
            var model = new CarModel { SIP_C_228 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_228));
        }

        /// <summary>
        /// 根据属性SIP_C_228获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_228(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_228));
        }

        #endregion

                
        #region SIP_C_229

        /// <summary>
        /// 根据属性SIP_C_229获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_229(string value)
        {
            var model = new CarModel { SIP_C_229 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_229));
        }

        /// <summary>
        /// 根据属性SIP_C_229获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_229(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_229));
        }

        #endregion

                
        #region SIP_C_230

        /// <summary>
        /// 根据属性SIP_C_230获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_230(string value)
        {
            var model = new CarModel { SIP_C_230 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_230));
        }

        /// <summary>
        /// 根据属性SIP_C_230获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_230(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_230));
        }

        #endregion

                
        #region SIP_C_317

        /// <summary>
        /// 根据属性SIP_C_317获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_317(string value)
        {
            var model = new CarModel { SIP_C_317 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_317));
        }

        /// <summary>
        /// 根据属性SIP_C_317获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_317(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_317));
        }

        #endregion

                
        #region SIP_C_233

        /// <summary>
        /// 根据属性SIP_C_233获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_233(string value)
        {
            var model = new CarModel { SIP_C_233 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_233));
        }

        /// <summary>
        /// 根据属性SIP_C_233获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_233(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_233));
        }

        #endregion

                
        #region SIP_C_234

        /// <summary>
        /// 根据属性SIP_C_234获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_234(string value)
        {
            var model = new CarModel { SIP_C_234 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_234));
        }

        /// <summary>
        /// 根据属性SIP_C_234获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_234(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_234));
        }

        #endregion

                
        #region SIP_C_235

        /// <summary>
        /// 根据属性SIP_C_235获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_235(string value)
        {
            var model = new CarModel { SIP_C_235 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_235));
        }

        /// <summary>
        /// 根据属性SIP_C_235获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_235(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_235));
        }

        #endregion

                
        #region SIP_C_236

        /// <summary>
        /// 根据属性SIP_C_236获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_236(string value)
        {
            var model = new CarModel { SIP_C_236 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_236));
        }

        /// <summary>
        /// 根据属性SIP_C_236获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_236(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_236));
        }

        #endregion

                
        #region SIP_C_237

        /// <summary>
        /// 根据属性SIP_C_237获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_237(string value)
        {
            var model = new CarModel { SIP_C_237 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_237));
        }

        /// <summary>
        /// 根据属性SIP_C_237获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_237(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_237));
        }

        #endregion

                
        #region SIP_C_238

        /// <summary>
        /// 根据属性SIP_C_238获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_238(string value)
        {
            var model = new CarModel { SIP_C_238 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_238));
        }

        /// <summary>
        /// 根据属性SIP_C_238获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_238(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_238));
        }

        #endregion

                
        #region SIP_C_239

        /// <summary>
        /// 根据属性SIP_C_239获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_239(string value)
        {
            var model = new CarModel { SIP_C_239 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_239));
        }

        /// <summary>
        /// 根据属性SIP_C_239获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_239(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_239));
        }

        #endregion

                
        #region SIP_C_240

        /// <summary>
        /// 根据属性SIP_C_240获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_240(string value)
        {
            var model = new CarModel { SIP_C_240 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_240));
        }

        /// <summary>
        /// 根据属性SIP_C_240获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_240(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_240));
        }

        #endregion

                
        #region SIP_C_241

        /// <summary>
        /// 根据属性SIP_C_241获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_241(string value)
        {
            var model = new CarModel { SIP_C_241 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_241));
        }

        /// <summary>
        /// 根据属性SIP_C_241获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_241(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_241));
        }

        #endregion

                
        #region SIP_C_242

        /// <summary>
        /// 根据属性SIP_C_242获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_242(string value)
        {
            var model = new CarModel { SIP_C_242 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_242));
        }

        /// <summary>
        /// 根据属性SIP_C_242获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_242(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_242));
        }

        #endregion

                
        #region SIP_C_321

        /// <summary>
        /// 根据属性SIP_C_321获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_321(string value)
        {
            var model = new CarModel { SIP_C_321 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_321));
        }

        /// <summary>
        /// 根据属性SIP_C_321获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_321(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_321));
        }

        #endregion

                
        #region SIP_C_247

        /// <summary>
        /// 根据属性SIP_C_247获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_247(string value)
        {
            var model = new CarModel { SIP_C_247 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_247));
        }

        /// <summary>
        /// 根据属性SIP_C_247获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_247(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_247));
        }

        #endregion

                
        #region SIP_C_248

        /// <summary>
        /// 根据属性SIP_C_248获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_248(string value)
        {
            var model = new CarModel { SIP_C_248 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_248));
        }

        /// <summary>
        /// 根据属性SIP_C_248获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_248(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_248));
        }

        #endregion

                
        #region SIP_C_249

        /// <summary>
        /// 根据属性SIP_C_249获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_249(string value)
        {
            var model = new CarModel { SIP_C_249 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_249));
        }

        /// <summary>
        /// 根据属性SIP_C_249获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_249(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_249));
        }

        #endregion

                
        #region SIP_C_250

        /// <summary>
        /// 根据属性SIP_C_250获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_250(string value)
        {
            var model = new CarModel { SIP_C_250 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_250));
        }

        /// <summary>
        /// 根据属性SIP_C_250获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_250(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_250));
        }

        #endregion

                
        #region SIP_C_251

        /// <summary>
        /// 根据属性SIP_C_251获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_251(string value)
        {
            var model = new CarModel { SIP_C_251 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_251));
        }

        /// <summary>
        /// 根据属性SIP_C_251获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_251(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_251));
        }

        #endregion

                
        #region SIP_C_252

        /// <summary>
        /// 根据属性SIP_C_252获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_252(string value)
        {
            var model = new CarModel { SIP_C_252 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_252));
        }

        /// <summary>
        /// 根据属性SIP_C_252获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_252(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_252));
        }

        #endregion

                
        #region SIP_C_253

        /// <summary>
        /// 根据属性SIP_C_253获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_253(string value)
        {
            var model = new CarModel { SIP_C_253 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_253));
        }

        /// <summary>
        /// 根据属性SIP_C_253获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_253(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_253));
        }

        #endregion

                
        #region SIP_C_254

        /// <summary>
        /// 根据属性SIP_C_254获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_254(string value)
        {
            var model = new CarModel { SIP_C_254 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_254));
        }

        /// <summary>
        /// 根据属性SIP_C_254获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_254(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_254));
        }

        #endregion

                
        #region SIP_C_255

        /// <summary>
        /// 根据属性SIP_C_255获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_255(string value)
        {
            var model = new CarModel { SIP_C_255 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_255));
        }

        /// <summary>
        /// 根据属性SIP_C_255获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_255(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_255));
        }

        #endregion

                
        #region SIP_C_256

        /// <summary>
        /// 根据属性SIP_C_256获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_256(string value)
        {
            var model = new CarModel { SIP_C_256 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_256));
        }

        /// <summary>
        /// 根据属性SIP_C_256获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_256(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_256));
        }

        #endregion

                
        #region SIP_C_257

        /// <summary>
        /// 根据属性SIP_C_257获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_257(string value)
        {
            var model = new CarModel { SIP_C_257 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_257));
        }

        /// <summary>
        /// 根据属性SIP_C_257获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_257(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_257));
        }

        #endregion

                
        #region SIP_C_258

        /// <summary>
        /// 根据属性SIP_C_258获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_258(string value)
        {
            var model = new CarModel { SIP_C_258 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_258));
        }

        /// <summary>
        /// 根据属性SIP_C_258获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_258(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_258));
        }

        #endregion

                
        #region SIP_C_318

        /// <summary>
        /// 根据属性SIP_C_318获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_318(string value)
        {
            var model = new CarModel { SIP_C_318 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_318));
        }

        /// <summary>
        /// 根据属性SIP_C_318获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_318(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_318));
        }

        #endregion

                
        #region SIP_C_260

        /// <summary>
        /// 根据属性SIP_C_260获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_260(string value)
        {
            var model = new CarModel { SIP_C_260 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_260));
        }

        /// <summary>
        /// 根据属性SIP_C_260获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_260(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_260));
        }

        #endregion

                
        #region SIP_C_261

        /// <summary>
        /// 根据属性SIP_C_261获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_261(string value)
        {
            var model = new CarModel { SIP_C_261 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_261));
        }

        /// <summary>
        /// 根据属性SIP_C_261获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_261(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_261));
        }

        #endregion

                
        #region SIP_C_262

        /// <summary>
        /// 根据属性SIP_C_262获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_262(string value)
        {
            var model = new CarModel { SIP_C_262 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_262));
        }

        /// <summary>
        /// 根据属性SIP_C_262获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_262(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_262));
        }

        #endregion

                
        #region SIP_C_263

        /// <summary>
        /// 根据属性SIP_C_263获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_263(string value)
        {
            var model = new CarModel { SIP_C_263 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_263));
        }

        /// <summary>
        /// 根据属性SIP_C_263获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_263(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_263));
        }

        #endregion

                
        #region SIP_C_264

        /// <summary>
        /// 根据属性SIP_C_264获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_264(string value)
        {
            var model = new CarModel { SIP_C_264 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_264));
        }

        /// <summary>
        /// 根据属性SIP_C_264获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_264(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_264));
        }

        #endregion

                
        #region SIP_C_265

        /// <summary>
        /// 根据属性SIP_C_265获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_265(string value)
        {
            var model = new CarModel { SIP_C_265 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_265));
        }

        /// <summary>
        /// 根据属性SIP_C_265获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_265(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_265));
        }

        #endregion

                
        #region SIP_C_266

        /// <summary>
        /// 根据属性SIP_C_266获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_266(string value)
        {
            var model = new CarModel { SIP_C_266 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_266));
        }

        /// <summary>
        /// 根据属性SIP_C_266获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_266(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_266));
        }

        #endregion

                
        #region SIP_C_267

        /// <summary>
        /// 根据属性SIP_C_267获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_267(string value)
        {
            var model = new CarModel { SIP_C_267 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_267));
        }

        /// <summary>
        /// 根据属性SIP_C_267获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_267(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_267));
        }

        #endregion

                
        #region SIP_C_268

        /// <summary>
        /// 根据属性SIP_C_268获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_268(string value)
        {
            var model = new CarModel { SIP_C_268 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_268));
        }

        /// <summary>
        /// 根据属性SIP_C_268获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_268(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_268));
        }

        #endregion

                
        #region SIP_C_269

        /// <summary>
        /// 根据属性SIP_C_269获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_269(string value)
        {
            var model = new CarModel { SIP_C_269 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_269));
        }

        /// <summary>
        /// 根据属性SIP_C_269获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_269(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_269));
        }

        #endregion

                
        #region SIP_C_270

        /// <summary>
        /// 根据属性SIP_C_270获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_270(string value)
        {
            var model = new CarModel { SIP_C_270 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_270));
        }

        /// <summary>
        /// 根据属性SIP_C_270获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_270(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_270));
        }

        #endregion

                
        #region SIP_C_271

        /// <summary>
        /// 根据属性SIP_C_271获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_271(string value)
        {
            var model = new CarModel { SIP_C_271 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_271));
        }

        /// <summary>
        /// 根据属性SIP_C_271获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_271(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_271));
        }

        #endregion

                
        #region SIP_C_277

        /// <summary>
        /// 根据属性SIP_C_277获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_277(string value)
        {
            var model = new CarModel { SIP_C_277 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_277));
        }

        /// <summary>
        /// 根据属性SIP_C_277获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_277(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_277));
        }

        #endregion

                
        #region SIP_C_278

        /// <summary>
        /// 根据属性SIP_C_278获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_278(string value)
        {
            var model = new CarModel { SIP_C_278 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_278));
        }

        /// <summary>
        /// 根据属性SIP_C_278获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_278(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_278));
        }

        #endregion

                
        #region SIP_C_279

        /// <summary>
        /// 根据属性SIP_C_279获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_279(string value)
        {
            var model = new CarModel { SIP_C_279 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_279));
        }

        /// <summary>
        /// 根据属性SIP_C_279获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_279(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_279));
        }

        #endregion

                
        #region SIP_C_282

        /// <summary>
        /// 根据属性SIP_C_282获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_282(string value)
        {
            var model = new CarModel { SIP_C_282 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_282));
        }

        /// <summary>
        /// 根据属性SIP_C_282获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_282(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_282));
        }

        #endregion

                
        #region SIP_C_319

        /// <summary>
        /// 根据属性SIP_C_319获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_319(string value)
        {
            var model = new CarModel { SIP_C_319 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_319));
        }

        /// <summary>
        /// 根据属性SIP_C_319获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_319(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_319));
        }

        #endregion

                
        #region SIP_C_272

        /// <summary>
        /// 根据属性SIP_C_272获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_272(string value)
        {
            var model = new CarModel { SIP_C_272 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_272));
        }

        /// <summary>
        /// 根据属性SIP_C_272获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_272(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_272));
        }

        #endregion

                
        #region SIP_C_273

        /// <summary>
        /// 根据属性SIP_C_273获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_273(string value)
        {
            var model = new CarModel { SIP_C_273 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_273));
        }

        /// <summary>
        /// 根据属性SIP_C_273获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_273(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_273));
        }

        #endregion

                
        #region SIP_C_274

        /// <summary>
        /// 根据属性SIP_C_274获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_274(string value)
        {
            var model = new CarModel { SIP_C_274 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_274));
        }

        /// <summary>
        /// 根据属性SIP_C_274获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_274(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_274));
        }

        #endregion

                
        #region SIP_C_275

        /// <summary>
        /// 根据属性SIP_C_275获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_275(string value)
        {
            var model = new CarModel { SIP_C_275 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_275));
        }

        /// <summary>
        /// 根据属性SIP_C_275获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_275(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_275));
        }

        #endregion

                
        #region SIP_C_276

        /// <summary>
        /// 根据属性SIP_C_276获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_276(string value)
        {
            var model = new CarModel { SIP_C_276 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_276));
        }

        /// <summary>
        /// 根据属性SIP_C_276获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_276(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_276));
        }

        #endregion

                
        #region SIP_C_320

        /// <summary>
        /// 根据属性SIP_C_320获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_320(string value)
        {
            var model = new CarModel { SIP_C_320 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_320));
        }

        /// <summary>
        /// 根据属性SIP_C_320获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_320(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_320));
        }

        #endregion

                
        #region SIP_C_285

        /// <summary>
        /// 根据属性SIP_C_285获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_285(string value)
        {
            var model = new CarModel { SIP_C_285 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_285));
        }

        /// <summary>
        /// 根据属性SIP_C_285获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_285(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_285));
        }

        #endregion

                
        #region SIP_C_286

        /// <summary>
        /// 根据属性SIP_C_286获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_286(string value)
        {
            var model = new CarModel { SIP_C_286 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_286));
        }

        /// <summary>
        /// 根据属性SIP_C_286获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_286(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_286));
        }

        #endregion

                
        #region SIP_C_287

        /// <summary>
        /// 根据属性SIP_C_287获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_287(string value)
        {
            var model = new CarModel { SIP_C_287 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_287));
        }

        /// <summary>
        /// 根据属性SIP_C_287获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_287(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_287));
        }

        #endregion

                
        #region SIP_C_288

        /// <summary>
        /// 根据属性SIP_C_288获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_288(string value)
        {
            var model = new CarModel { SIP_C_288 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_288));
        }

        /// <summary>
        /// 根据属性SIP_C_288获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_288(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_288));
        }

        #endregion

                
        #region SIP_C_289

        /// <summary>
        /// 根据属性SIP_C_289获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_289(string value)
        {
            var model = new CarModel { SIP_C_289 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_289));
        }

        /// <summary>
        /// 根据属性SIP_C_289获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_289(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_289));
        }

        #endregion

                
        #region SIP_C_290

        /// <summary>
        /// 根据属性SIP_C_290获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_290(string value)
        {
            var model = new CarModel { SIP_C_290 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_290));
        }

        /// <summary>
        /// 根据属性SIP_C_290获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_290(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_290));
        }

        #endregion

                
        #region SIP_C_291

        /// <summary>
        /// 根据属性SIP_C_291获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_291(string value)
        {
            var model = new CarModel { SIP_C_291 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_291));
        }

        /// <summary>
        /// 根据属性SIP_C_291获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_291(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_291));
        }

        #endregion

                
        #region SIP_C_292

        /// <summary>
        /// 根据属性SIP_C_292获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_292(string value)
        {
            var model = new CarModel { SIP_C_292 = value };
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_292));
        }

        /// <summary>
        /// 根据属性SIP_C_292获取数据实体
        /// </summary>
        public CarModel GetBySIP_C_292(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.SIP_C_292));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarModel GetByIp(string value)
        {
            var model = new CarModel { Ip = value };
            return GetByWhere(model.GetFilterString(CarModelField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarModel GetByIp(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarModel GetByCreateBy(long value)
        {
            var model = new CarModel { CreateBy = value };
            return GetByWhere(model.GetFilterString(CarModelField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarModel GetByCreateBy(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarModel GetByCreateTime(DateTime value)
        {
            var model = new CarModel { CreateTime = value };
            return GetByWhere(model.GetFilterString(CarModelField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarModel GetByCreateTime(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarModel GetByLastModifyBy(long value)
        {
            var model = new CarModel { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(CarModelField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarModel GetByLastModifyBy(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarModel GetByLastModifyTime(DateTime value)
        {
            var model = new CarModel { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(CarModelField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarModel GetByLastModifyTime(CarModel model)
        {
            return GetByWhere(model.GetFilterString(CarModelField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By ModelId

        /// <summary>
        /// 根据属性ModelId获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByModelId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { ModelId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.ModelId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ModelId获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByModelId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.ModelId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By SeriesId

        /// <summary>
        /// 根据属性SeriesId获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySeriesId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SeriesId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SeriesId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SeriesId获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySeriesId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SeriesId),sort,operateMode);
        }

        #endregion

                
        #region GetList By ModelName

        /// <summary>
        /// 根据属性ModelName获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByModelName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { ModelName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.ModelName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ModelName获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByModelName(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.ModelName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Year

        /// <summary>
        /// 根据属性Year获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByYear(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Year = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Year),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Year获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByYear(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Year),sort,operateMode);
        }

        #endregion

                
        #region GetList By Displ

        /// <summary>
        /// 根据属性Displ获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByDispl(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Displ = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Displ),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Displ获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByDispl(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Displ),sort,operateMode);
        }

        #endregion

                
        #region GetList By Price

        /// <summary>
        /// 根据属性Price获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByPrice(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Price = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Price),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Price获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByPrice(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Price),sort,operateMode);
        }

        #endregion

                
        #region GetList By Gear

        /// <summary>
        /// 根据属性Gear获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByGear(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Gear = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Gear),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Gear获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByGear(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Gear),sort,operateMode);
        }

        #endregion

                
        #region GetList By Status

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Status = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Status),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Status),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_102

        /// <summary>
        /// 根据属性SIP_C_102获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_102(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_102 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_102),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_102获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_102(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_102),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_103

        /// <summary>
        /// 根据属性SIP_C_103获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_103(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_103 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_103),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_103获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_103(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_103),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_104

        /// <summary>
        /// 根据属性SIP_C_104获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_104(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_104 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_104),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_104获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_104(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_104),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_105

        /// <summary>
        /// 根据属性SIP_C_105获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_105(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_105 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_105),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_105获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_105(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_105),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_106

        /// <summary>
        /// 根据属性SIP_C_106获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_106(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_106 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_106),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_106获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_106(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_106),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_293

        /// <summary>
        /// 根据属性SIP_C_293获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_293(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_293 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_293),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_293获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_293(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_293),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_107

        /// <summary>
        /// 根据属性SIP_C_107获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_107(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_107 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_107),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_107获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_107(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_107),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_108

        /// <summary>
        /// 根据属性SIP_C_108获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_108(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_108 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_108),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_108获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_108(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_108),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_303

        /// <summary>
        /// 根据属性SIP_C_303获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_303(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_303 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_303),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_303获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_303(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_303),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_112

        /// <summary>
        /// 根据属性SIP_C_112获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_112(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_112 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_112),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_112获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_112(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_112),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_294

        /// <summary>
        /// 根据属性SIP_C_294获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_294(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_294 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_294),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_294获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_294(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_294),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_113

        /// <summary>
        /// 根据属性SIP_C_113获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_113(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_113 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_113),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_113获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_113(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_113),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_114

        /// <summary>
        /// 根据属性SIP_C_114获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_114(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_114 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_114),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_114获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_114(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_114),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_304

        /// <summary>
        /// 根据属性SIP_C_304获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_304(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_304 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_304),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_304获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_304(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_304),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_115

        /// <summary>
        /// 根据属性SIP_C_115获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_115(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_115 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_115),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_115获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_115(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_115),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_116

        /// <summary>
        /// 根据属性SIP_C_116获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_116(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_116 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_116),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_116获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_116(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_116),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_295

        /// <summary>
        /// 根据属性SIP_C_295获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_295(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_295 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_295),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_295获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_295(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_295),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_117

        /// <summary>
        /// 根据属性SIP_C_117获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_117(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_117 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_117),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_117获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_117(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_117),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_118

        /// <summary>
        /// 根据属性SIP_C_118获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_118(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_118 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_118),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_118获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_118(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_118),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_119

        /// <summary>
        /// 根据属性SIP_C_119获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_119(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_119 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_119),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_119获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_119(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_119),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_120

        /// <summary>
        /// 根据属性SIP_C_120获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_120(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_120 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_120),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_120获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_120(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_120),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_121

        /// <summary>
        /// 根据属性SIP_C_121获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_121(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_121 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_121),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_121获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_121(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_121),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_122

        /// <summary>
        /// 根据属性SIP_C_122获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_122(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_122 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_122),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_122获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_122(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_122),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_123

        /// <summary>
        /// 根据属性SIP_C_123获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_123(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_123 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_123),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_123获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_123(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_123),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_124

        /// <summary>
        /// 根据属性SIP_C_124获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_124(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_124 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_124),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_124获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_124(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_124),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_125

        /// <summary>
        /// 根据属性SIP_C_125获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_125(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_125 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_125),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_125获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_125(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_125),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_126

        /// <summary>
        /// 根据属性SIP_C_126获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_126(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_126 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_126),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_126获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_126(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_126),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_127

        /// <summary>
        /// 根据属性SIP_C_127获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_127(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_127 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_127),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_127获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_127(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_127),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_128

        /// <summary>
        /// 根据属性SIP_C_128获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_128(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_128 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_128),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_128获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_128(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_128),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_129

        /// <summary>
        /// 根据属性SIP_C_129获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_129(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_129 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_129),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_129获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_129(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_129),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_130

        /// <summary>
        /// 根据属性SIP_C_130获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_130(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_130 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_130),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_130获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_130(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_130),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_131

        /// <summary>
        /// 根据属性SIP_C_131获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_131(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_131 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_131),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_131获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_131(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_131),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_132

        /// <summary>
        /// 根据属性SIP_C_132获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_132(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_132 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_132),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_132获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_132(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_132),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_133

        /// <summary>
        /// 根据属性SIP_C_133获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_133(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_133 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_133),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_133获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_133(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_133),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_134

        /// <summary>
        /// 根据属性SIP_C_134获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_134(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_134 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_134),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_134获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_134(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_134),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_135

        /// <summary>
        /// 根据属性SIP_C_135获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_135(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_135 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_135),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_135获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_135(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_135),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_136

        /// <summary>
        /// 根据属性SIP_C_136获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_136(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_136 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_136),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_136获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_136(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_136),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_137

        /// <summary>
        /// 根据属性SIP_C_137获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_137(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_137 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_137),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_137获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_137(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_137),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_138

        /// <summary>
        /// 根据属性SIP_C_138获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_138(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_138 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_138),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_138获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_138(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_138),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_139

        /// <summary>
        /// 根据属性SIP_C_139获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_139(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_139 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_139),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_139获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_139(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_139),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_140

        /// <summary>
        /// 根据属性SIP_C_140获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_140(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_140 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_140),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_140获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_140(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_140),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_141

        /// <summary>
        /// 根据属性SIP_C_141获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_141(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_141 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_141),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_141获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_141(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_141),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_142

        /// <summary>
        /// 根据属性SIP_C_142获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_142(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_142 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_142),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_142获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_142(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_142),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_143

        /// <summary>
        /// 根据属性SIP_C_143获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_143(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_143 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_143),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_143获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_143(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_143),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_297

        /// <summary>
        /// 根据属性SIP_C_297获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_297(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_297 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_297),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_297获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_297(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_297),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_298

        /// <summary>
        /// 根据属性SIP_C_298获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_298(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_298 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_298),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_298获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_298(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_298),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_299

        /// <summary>
        /// 根据属性SIP_C_299获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_299(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_299 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_299),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_299获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_299(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_299),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_148

        /// <summary>
        /// 根据属性SIP_C_148获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_148(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_148 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_148),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_148获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_148(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_148),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_305

        /// <summary>
        /// 根据属性SIP_C_305获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_305(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_305 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_305),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_305获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_305(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_305),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_306

        /// <summary>
        /// 根据属性SIP_C_306获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_306(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_306 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_306),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_306获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_306(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_306),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_307

        /// <summary>
        /// 根据属性SIP_C_307获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_307(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_307 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_307),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_307获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_307(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_307),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_308

        /// <summary>
        /// 根据属性SIP_C_308获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_308(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_308 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_308),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_308获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_308(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_308),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_309

        /// <summary>
        /// 根据属性SIP_C_309获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_309(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_309 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_309),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_309获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_309(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_309),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_310

        /// <summary>
        /// 根据属性SIP_C_310获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_310(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_310 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_310),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_310获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_310(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_310),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_311

        /// <summary>
        /// 根据属性SIP_C_311获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_311(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_311 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_311),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_311获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_311(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_311),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_149

        /// <summary>
        /// 根据属性SIP_C_149获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_149(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_149 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_149),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_149获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_149(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_149),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_150

        /// <summary>
        /// 根据属性SIP_C_150获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_150(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_150 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_150),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_150获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_150(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_150),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_151

        /// <summary>
        /// 根据属性SIP_C_151获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_151(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_151 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_151),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_151获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_151(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_151),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_152

        /// <summary>
        /// 根据属性SIP_C_152获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_152(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_152 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_152),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_152获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_152(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_152),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_153

        /// <summary>
        /// 根据属性SIP_C_153获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_153(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_153 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_153),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_153获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_153(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_153),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_154

        /// <summary>
        /// 根据属性SIP_C_154获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_154(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_154 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_154),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_154获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_154(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_154),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_155

        /// <summary>
        /// 根据属性SIP_C_155获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_155(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_155 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_155),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_155获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_155(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_155),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_156

        /// <summary>
        /// 根据属性SIP_C_156获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_156(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_156 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_156),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_156获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_156(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_156),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_157

        /// <summary>
        /// 根据属性SIP_C_157获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_157(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_157 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_157),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_157获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_157(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_157),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_158

        /// <summary>
        /// 根据属性SIP_C_158获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_158(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_158 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_158),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_158获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_158(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_158),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_159

        /// <summary>
        /// 根据属性SIP_C_159获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_159(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_159 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_159),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_159获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_159(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_159),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_160

        /// <summary>
        /// 根据属性SIP_C_160获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_160(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_160 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_160),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_160获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_160(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_160),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_161

        /// <summary>
        /// 根据属性SIP_C_161获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_161(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_161 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_161),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_161获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_161(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_161),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_162

        /// <summary>
        /// 根据属性SIP_C_162获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_162(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_162 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_162),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_162获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_162(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_162),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_163

        /// <summary>
        /// 根据属性SIP_C_163获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_163(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_163 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_163),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_163获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_163(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_163),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_164

        /// <summary>
        /// 根据属性SIP_C_164获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_164(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_164 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_164),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_164获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_164(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_164),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_165

        /// <summary>
        /// 根据属性SIP_C_165获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_165(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_165 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_165),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_165获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_165(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_165),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_166

        /// <summary>
        /// 根据属性SIP_C_166获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_166(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_166 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_166),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_166获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_166(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_166),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_167

        /// <summary>
        /// 根据属性SIP_C_167获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_167(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_167 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_167),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_167获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_167(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_167),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_168

        /// <summary>
        /// 根据属性SIP_C_168获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_168(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_168 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_168),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_168获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_168(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_168),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_169

        /// <summary>
        /// 根据属性SIP_C_169获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_169(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_169 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_169),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_169获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_169(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_169),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_170

        /// <summary>
        /// 根据属性SIP_C_170获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_170(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_170 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_170),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_170获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_170(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_170),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_171

        /// <summary>
        /// 根据属性SIP_C_171获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_171(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_171 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_171),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_171获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_171(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_171),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_172

        /// <summary>
        /// 根据属性SIP_C_172获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_172(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_172 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_172),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_172获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_172(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_172),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_173

        /// <summary>
        /// 根据属性SIP_C_173获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_173(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_173 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_173),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_173获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_173(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_173),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_174

        /// <summary>
        /// 根据属性SIP_C_174获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_174(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_174 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_174),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_174获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_174(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_174),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_177

        /// <summary>
        /// 根据属性SIP_C_177获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_177(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_177 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_177),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_177获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_177(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_177),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_178

        /// <summary>
        /// 根据属性SIP_C_178获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_178(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_178 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_178),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_178获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_178(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_178),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_179

        /// <summary>
        /// 根据属性SIP_C_179获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_179(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_179 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_179),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_179获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_179(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_179),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_180

        /// <summary>
        /// 根据属性SIP_C_180获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_180(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_180 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_180),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_180获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_180(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_180),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_181

        /// <summary>
        /// 根据属性SIP_C_181获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_181(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_181 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_181),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_181获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_181(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_181),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_183

        /// <summary>
        /// 根据属性SIP_C_183获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_183(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_183 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_183),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_183获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_183(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_183),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_184

        /// <summary>
        /// 根据属性SIP_C_184获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_184(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_184 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_184),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_184获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_184(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_184),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_185

        /// <summary>
        /// 根据属性SIP_C_185获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_185(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_185 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_185),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_185获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_185(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_185),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_186

        /// <summary>
        /// 根据属性SIP_C_186获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_186(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_186 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_186),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_186获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_186(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_186),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_187

        /// <summary>
        /// 根据属性SIP_C_187获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_187(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_187 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_187),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_187获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_187(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_187),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_188

        /// <summary>
        /// 根据属性SIP_C_188获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_188(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_188 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_188),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_188获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_188(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_188),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_189

        /// <summary>
        /// 根据属性SIP_C_189获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_189(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_189 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_189),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_189获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_189(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_189),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_190

        /// <summary>
        /// 根据属性SIP_C_190获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_190(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_190 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_190),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_190获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_190(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_190),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_191

        /// <summary>
        /// 根据属性SIP_C_191获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_191(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_191 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_191),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_191获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_191(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_191),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_192

        /// <summary>
        /// 根据属性SIP_C_192获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_192(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_192 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_192),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_192获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_192(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_192),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_193

        /// <summary>
        /// 根据属性SIP_C_193获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_193(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_193 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_193),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_193获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_193(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_193),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_194

        /// <summary>
        /// 根据属性SIP_C_194获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_194(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_194 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_194),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_194获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_194(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_194),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_195

        /// <summary>
        /// 根据属性SIP_C_195获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_195(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_195 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_195),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_195获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_195(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_195),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_196

        /// <summary>
        /// 根据属性SIP_C_196获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_196(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_196 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_196),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_196获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_196(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_196),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_197

        /// <summary>
        /// 根据属性SIP_C_197获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_197(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_197 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_197),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_197获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_197(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_197),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_198

        /// <summary>
        /// 根据属性SIP_C_198获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_198(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_198 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_198),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_198获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_198(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_198),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_199

        /// <summary>
        /// 根据属性SIP_C_199获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_199(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_199 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_199),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_199获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_199(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_199),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_204

        /// <summary>
        /// 根据属性SIP_C_204获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_204(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_204 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_204),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_204获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_204(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_204),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_205

        /// <summary>
        /// 根据属性SIP_C_205获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_205(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_205 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_205),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_205获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_205(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_205),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_312

        /// <summary>
        /// 根据属性SIP_C_312获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_312(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_312 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_312),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_312获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_312(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_312),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_313

        /// <summary>
        /// 根据属性SIP_C_313获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_313(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_313 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_313),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_313获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_313(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_313),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_314

        /// <summary>
        /// 根据属性SIP_C_314获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_314(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_314 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_314),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_314获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_314(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_314),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_315

        /// <summary>
        /// 根据属性SIP_C_315获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_315(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_315 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_315),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_315获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_315(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_315),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_316

        /// <summary>
        /// 根据属性SIP_C_316获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_316(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_316 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_316),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_316获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_316(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_316),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_210

        /// <summary>
        /// 根据属性SIP_C_210获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_210(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_210 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_210),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_210获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_210(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_210),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_211

        /// <summary>
        /// 根据属性SIP_C_211获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_211(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_211 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_211),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_211获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_211(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_211),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_212

        /// <summary>
        /// 根据属性SIP_C_212获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_212(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_212 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_212),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_212获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_212(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_212),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_300

        /// <summary>
        /// 根据属性SIP_C_300获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_300(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_300 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_300),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_300获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_300(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_300),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_213

        /// <summary>
        /// 根据属性SIP_C_213获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_213(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_213 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_213),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_213获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_213(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_213),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_214

        /// <summary>
        /// 根据属性SIP_C_214获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_214(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_214 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_214),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_214获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_214(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_214),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_215

        /// <summary>
        /// 根据属性SIP_C_215获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_215(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_215 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_215),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_215获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_215(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_215),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_216

        /// <summary>
        /// 根据属性SIP_C_216获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_216(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_216 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_216),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_216获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_216(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_216),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_217

        /// <summary>
        /// 根据属性SIP_C_217获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_217(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_217 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_217),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_217获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_217(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_217),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_218

        /// <summary>
        /// 根据属性SIP_C_218获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_218(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_218 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_218),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_218获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_218(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_218),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_175

        /// <summary>
        /// 根据属性SIP_C_175获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_175(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_175 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_175),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_175获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_175(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_175),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_176

        /// <summary>
        /// 根据属性SIP_C_176获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_176(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_176 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_176),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_176获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_176(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_176),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_219

        /// <summary>
        /// 根据属性SIP_C_219获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_219(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_219 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_219),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_219获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_219(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_219),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_220

        /// <summary>
        /// 根据属性SIP_C_220获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_220(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_220 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_220),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_220获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_220(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_220),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_200

        /// <summary>
        /// 根据属性SIP_C_200获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_200(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_200 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_200),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_200获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_200(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_200),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_201

        /// <summary>
        /// 根据属性SIP_C_201获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_201(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_201 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_201),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_201获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_201(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_201),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_202

        /// <summary>
        /// 根据属性SIP_C_202获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_202(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_202 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_202),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_202获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_202(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_202),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_203

        /// <summary>
        /// 根据属性SIP_C_203获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_203(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_203 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_203),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_203获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_203(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_203),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_221

        /// <summary>
        /// 根据属性SIP_C_221获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_221(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_221 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_221),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_221获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_221(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_221),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_222

        /// <summary>
        /// 根据属性SIP_C_222获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_222(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_222 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_222),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_222获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_222(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_222),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_223

        /// <summary>
        /// 根据属性SIP_C_223获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_223(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_223 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_223),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_223获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_223(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_223),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_301

        /// <summary>
        /// 根据属性SIP_C_301获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_301(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_301 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_301),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_301获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_301(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_301),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_224

        /// <summary>
        /// 根据属性SIP_C_224获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_224(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_224 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_224),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_224获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_224(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_224),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_225

        /// <summary>
        /// 根据属性SIP_C_225获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_225(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_225 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_225),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_225获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_225(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_225),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_226

        /// <summary>
        /// 根据属性SIP_C_226获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_226(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_226 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_226),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_226获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_226(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_226),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_227

        /// <summary>
        /// 根据属性SIP_C_227获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_227(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_227 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_227),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_227获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_227(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_227),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_228

        /// <summary>
        /// 根据属性SIP_C_228获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_228(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_228 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_228),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_228获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_228(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_228),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_229

        /// <summary>
        /// 根据属性SIP_C_229获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_229(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_229 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_229),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_229获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_229(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_229),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_230

        /// <summary>
        /// 根据属性SIP_C_230获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_230(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_230 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_230),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_230获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_230(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_230),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_317

        /// <summary>
        /// 根据属性SIP_C_317获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_317(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_317 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_317),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_317获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_317(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_317),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_233

        /// <summary>
        /// 根据属性SIP_C_233获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_233(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_233 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_233),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_233获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_233(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_233),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_234

        /// <summary>
        /// 根据属性SIP_C_234获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_234(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_234 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_234),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_234获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_234(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_234),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_235

        /// <summary>
        /// 根据属性SIP_C_235获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_235(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_235 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_235),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_235获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_235(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_235),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_236

        /// <summary>
        /// 根据属性SIP_C_236获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_236(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_236 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_236),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_236获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_236(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_236),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_237

        /// <summary>
        /// 根据属性SIP_C_237获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_237(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_237 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_237),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_237获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_237(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_237),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_238

        /// <summary>
        /// 根据属性SIP_C_238获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_238(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_238 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_238),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_238获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_238(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_238),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_239

        /// <summary>
        /// 根据属性SIP_C_239获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_239(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_239 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_239),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_239获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_239(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_239),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_240

        /// <summary>
        /// 根据属性SIP_C_240获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_240(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_240 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_240),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_240获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_240(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_240),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_241

        /// <summary>
        /// 根据属性SIP_C_241获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_241(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_241 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_241),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_241获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_241(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_241),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_242

        /// <summary>
        /// 根据属性SIP_C_242获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_242(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_242 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_242),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_242获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_242(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_242),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_321

        /// <summary>
        /// 根据属性SIP_C_321获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_321(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_321 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_321),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_321获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_321(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_321),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_247

        /// <summary>
        /// 根据属性SIP_C_247获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_247(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_247 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_247),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_247获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_247(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_247),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_248

        /// <summary>
        /// 根据属性SIP_C_248获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_248(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_248 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_248),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_248获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_248(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_248),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_249

        /// <summary>
        /// 根据属性SIP_C_249获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_249(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_249 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_249),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_249获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_249(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_249),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_250

        /// <summary>
        /// 根据属性SIP_C_250获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_250(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_250 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_250),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_250获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_250(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_250),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_251

        /// <summary>
        /// 根据属性SIP_C_251获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_251(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_251 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_251),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_251获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_251(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_251),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_252

        /// <summary>
        /// 根据属性SIP_C_252获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_252(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_252 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_252),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_252获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_252(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_252),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_253

        /// <summary>
        /// 根据属性SIP_C_253获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_253(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_253 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_253),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_253获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_253(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_253),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_254

        /// <summary>
        /// 根据属性SIP_C_254获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_254(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_254 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_254),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_254获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_254(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_254),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_255

        /// <summary>
        /// 根据属性SIP_C_255获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_255(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_255 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_255),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_255获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_255(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_255),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_256

        /// <summary>
        /// 根据属性SIP_C_256获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_256(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_256 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_256),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_256获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_256(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_256),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_257

        /// <summary>
        /// 根据属性SIP_C_257获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_257(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_257 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_257),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_257获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_257(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_257),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_258

        /// <summary>
        /// 根据属性SIP_C_258获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_258(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_258 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_258),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_258获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_258(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_258),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_318

        /// <summary>
        /// 根据属性SIP_C_318获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_318(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_318 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_318),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_318获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_318(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_318),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_260

        /// <summary>
        /// 根据属性SIP_C_260获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_260(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_260 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_260),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_260获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_260(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_260),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_261

        /// <summary>
        /// 根据属性SIP_C_261获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_261(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_261 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_261),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_261获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_261(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_261),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_262

        /// <summary>
        /// 根据属性SIP_C_262获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_262(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_262 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_262),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_262获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_262(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_262),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_263

        /// <summary>
        /// 根据属性SIP_C_263获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_263(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_263 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_263),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_263获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_263(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_263),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_264

        /// <summary>
        /// 根据属性SIP_C_264获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_264(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_264 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_264),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_264获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_264(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_264),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_265

        /// <summary>
        /// 根据属性SIP_C_265获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_265(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_265 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_265),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_265获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_265(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_265),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_266

        /// <summary>
        /// 根据属性SIP_C_266获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_266(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_266 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_266),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_266获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_266(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_266),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_267

        /// <summary>
        /// 根据属性SIP_C_267获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_267(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_267 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_267),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_267获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_267(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_267),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_268

        /// <summary>
        /// 根据属性SIP_C_268获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_268(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_268 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_268),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_268获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_268(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_268),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_269

        /// <summary>
        /// 根据属性SIP_C_269获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_269(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_269 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_269),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_269获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_269(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_269),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_270

        /// <summary>
        /// 根据属性SIP_C_270获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_270(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_270 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_270),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_270获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_270(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_270),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_271

        /// <summary>
        /// 根据属性SIP_C_271获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_271(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_271 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_271),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_271获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_271(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_271),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_277

        /// <summary>
        /// 根据属性SIP_C_277获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_277(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_277 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_277),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_277获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_277(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_277),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_278

        /// <summary>
        /// 根据属性SIP_C_278获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_278(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_278 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_278),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_278获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_278(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_278),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_279

        /// <summary>
        /// 根据属性SIP_C_279获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_279(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_279 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_279),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_279获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_279(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_279),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_282

        /// <summary>
        /// 根据属性SIP_C_282获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_282(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_282 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_282),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_282获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_282(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_282),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_319

        /// <summary>
        /// 根据属性SIP_C_319获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_319(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_319 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_319),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_319获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_319(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_319),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_272

        /// <summary>
        /// 根据属性SIP_C_272获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_272(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_272 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_272),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_272获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_272(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_272),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_273

        /// <summary>
        /// 根据属性SIP_C_273获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_273(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_273 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_273),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_273获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_273(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_273),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_274

        /// <summary>
        /// 根据属性SIP_C_274获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_274(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_274 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_274),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_274获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_274(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_274),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_275

        /// <summary>
        /// 根据属性SIP_C_275获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_275(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_275 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_275),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_275获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_275(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_275),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_276

        /// <summary>
        /// 根据属性SIP_C_276获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_276(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_276 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_276),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_276获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_276(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_276),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_320

        /// <summary>
        /// 根据属性SIP_C_320获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_320(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_320 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_320),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_320获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_320(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_320),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_285

        /// <summary>
        /// 根据属性SIP_C_285获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_285(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_285 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_285),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_285获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_285(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_285),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_286

        /// <summary>
        /// 根据属性SIP_C_286获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_286(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_286 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_286),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_286获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_286(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_286),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_287

        /// <summary>
        /// 根据属性SIP_C_287获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_287(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_287 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_287),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_287获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_287(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_287),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_288

        /// <summary>
        /// 根据属性SIP_C_288获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_288(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_288 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_288),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_288获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_288(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_288),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_289

        /// <summary>
        /// 根据属性SIP_C_289获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_289(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_289 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_289),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_289获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_289(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_289),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_290

        /// <summary>
        /// 根据属性SIP_C_290获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_290(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_290 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_290),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_290获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_290(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_290),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_291

        /// <summary>
        /// 根据属性SIP_C_291获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_291(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_291 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_291),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_291获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_291(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_291),sort,operateMode);
        }

        #endregion

                
        #region GetList By SIP_C_292

        /// <summary>
        /// 根据属性SIP_C_292获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_292(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { SIP_C_292 = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_292),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SIP_C_292获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListBySIP_C_292(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.SIP_C_292),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarModel { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarModel> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarModel model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarModelField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}