﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class CarBrandDal : ExBaseDal<CarBrand>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CarBrandDal(): this(Initialization.GetXmlConfig(typeof(CarBrand)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CarBrandDal CreateDal()
        {
			return new CarBrandDal(Initialization.GetXmlConfig(typeof(CarBrand)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CarBrandDal()
        {
           ModelName= typeof(CarBrand).Name;
           var item = new CarBrand();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CarBrandDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "CarBrand";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarBrand GetByName(string value)
        {
            var model = new CarBrand { Name = value };
            return GetByWhere(model.GetFilterString(CarBrandField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarBrand GetByName(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.Name));
        }

        #endregion

                
        #region Logo

        /// <summary>
        /// 根据属性Logo获取数据实体
        /// </summary>
        public CarBrand GetByLogo(string value)
        {
            var model = new CarBrand { Logo = value };
            return GetByWhere(model.GetFilterString(CarBrandField.Logo));
        }

        /// <summary>
        /// 根据属性Logo获取数据实体
        /// </summary>
        public CarBrand GetByLogo(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.Logo));
        }

        #endregion

                
        #region Letter

        /// <summary>
        /// 根据属性Letter获取数据实体
        /// </summary>
        public CarBrand GetByLetter(string value)
        {
            var model = new CarBrand { Letter = value };
            return GetByWhere(model.GetFilterString(CarBrandField.Letter));
        }

        /// <summary>
        /// 根据属性Letter获取数据实体
        /// </summary>
        public CarBrand GetByLetter(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.Letter));
        }

        #endregion

                
        #region PinYin

        /// <summary>
        /// 根据属性PinYin获取数据实体
        /// </summary>
        public CarBrand GetByPinYin(string value)
        {
            var model = new CarBrand { PinYin = value };
            return GetByWhere(model.GetFilterString(CarBrandField.PinYin));
        }

        /// <summary>
        /// 根据属性PinYin获取数据实体
        /// </summary>
        public CarBrand GetByPinYin(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.PinYin));
        }

        #endregion

                
        #region BrandId

        /// <summary>
        /// 根据属性BrandId获取数据实体
        /// </summary>
        public CarBrand GetByBrandId(int value)
        {
            var model = new CarBrand { BrandId = value };
            return GetByWhere(model.GetFilterString(CarBrandField.BrandId));
        }

        /// <summary>
        /// 根据属性BrandId获取数据实体
        /// </summary>
        public CarBrand GetByBrandId(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.BrandId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarBrand GetByIp(string value)
        {
            var model = new CarBrand { Ip = value };
            return GetByWhere(model.GetFilterString(CarBrandField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarBrand GetByIp(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarBrand GetByCreateBy(long value)
        {
            var model = new CarBrand { CreateBy = value };
            return GetByWhere(model.GetFilterString(CarBrandField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarBrand GetByCreateBy(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarBrand GetByCreateTime(DateTime value)
        {
            var model = new CarBrand { CreateTime = value };
            return GetByWhere(model.GetFilterString(CarBrandField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarBrand GetByCreateTime(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarBrand GetByLastModifyBy(long value)
        {
            var model = new CarBrand { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(CarBrandField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarBrand GetByLastModifyBy(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarBrand GetByLastModifyTime(DateTime value)
        {
            var model = new CarBrand { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(CarBrandField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarBrand GetByLastModifyTime(CarBrand model)
        {
            return GetByWhere(model.GetFilterString(CarBrandField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By Logo

        /// <summary>
        /// 根据属性Logo获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLogo(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { Logo = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Logo),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Logo获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLogo(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Logo),sort,operateMode);
        }

        #endregion

                
        #region GetList By Letter

        /// <summary>
        /// 根据属性Letter获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLetter(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { Letter = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Letter),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Letter获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLetter(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Letter),sort,operateMode);
        }

        #endregion

                
        #region GetList By PinYin

        /// <summary>
        /// 根据属性PinYin获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByPinYin(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { PinYin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.PinYin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PinYin获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByPinYin(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.PinYin),sort,operateMode);
        }

        #endregion

                
        #region GetList By BrandId

        /// <summary>
        /// 根据属性BrandId获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByBrandId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { BrandId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.BrandId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BrandId获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByBrandId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.BrandId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarBrand { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarBrand> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarBrand model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarBrandField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}