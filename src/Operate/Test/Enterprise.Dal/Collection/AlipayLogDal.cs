﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AlipayLogDal : ExBaseDal<AlipayLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AlipayLogDal(): this(Initialization.GetXmlConfig(typeof(AlipayLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AlipayLogDal CreateDal()
        {
			return new AlipayLogDal(Initialization.GetXmlConfig(typeof(AlipayLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AlipayLogDal()
        {
           ModelName= typeof(AlipayLog).Name;
           var item = new AlipayLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AlipayLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "AlipayLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public AlipayLog GetByUserProfileId(int value)
        {
            var model = new AlipayLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public AlipayLog GetByUserProfileId(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.UserProfileId));
        }

        #endregion

                
        #region Exterface

        /// <summary>
        /// 根据属性Exterface获取数据实体
        /// </summary>
        public AlipayLog GetByExterface(string value)
        {
            var model = new AlipayLog { Exterface = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Exterface));
        }

        /// <summary>
        /// 根据属性Exterface获取数据实体
        /// </summary>
        public AlipayLog GetByExterface(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Exterface));
        }

        #endregion

                
        #region Body

        /// <summary>
        /// 根据属性Body获取数据实体
        /// </summary>
        public AlipayLog GetByBody(string value)
        {
            var model = new AlipayLog { Body = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Body));
        }

        /// <summary>
        /// 根据属性Body获取数据实体
        /// </summary>
        public AlipayLog GetByBody(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Body));
        }

        #endregion

                
        #region BuyerEmail

        /// <summary>
        /// 根据属性BuyerEmail获取数据实体
        /// </summary>
        public AlipayLog GetByBuyerEmail(string value)
        {
            var model = new AlipayLog { BuyerEmail = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.BuyerEmail));
        }

        /// <summary>
        /// 根据属性BuyerEmail获取数据实体
        /// </summary>
        public AlipayLog GetByBuyerEmail(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.BuyerEmail));
        }

        #endregion

                
        #region BuyerId

        /// <summary>
        /// 根据属性BuyerId获取数据实体
        /// </summary>
        public AlipayLog GetByBuyerId(string value)
        {
            var model = new AlipayLog { BuyerId = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.BuyerId));
        }

        /// <summary>
        /// 根据属性BuyerId获取数据实体
        /// </summary>
        public AlipayLog GetByBuyerId(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.BuyerId));
        }

        #endregion

                
        #region IsSuccess

        /// <summary>
        /// 根据属性IsSuccess获取数据实体
        /// </summary>
        public AlipayLog GetByIsSuccess(string value)
        {
            var model = new AlipayLog { IsSuccess = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.IsSuccess));
        }

        /// <summary>
        /// 根据属性IsSuccess获取数据实体
        /// </summary>
        public AlipayLog GetByIsSuccess(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.IsSuccess));
        }

        #endregion

                
        #region NotifyId

        /// <summary>
        /// 根据属性NotifyId获取数据实体
        /// </summary>
        public AlipayLog GetByNotifyId(string value)
        {
            var model = new AlipayLog { NotifyId = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.NotifyId));
        }

        /// <summary>
        /// 根据属性NotifyId获取数据实体
        /// </summary>
        public AlipayLog GetByNotifyId(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.NotifyId));
        }

        #endregion

                
        #region NotifyTime

        /// <summary>
        /// 根据属性NotifyTime获取数据实体
        /// </summary>
        public AlipayLog GetByNotifyTime(string value)
        {
            var model = new AlipayLog { NotifyTime = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.NotifyTime));
        }

        /// <summary>
        /// 根据属性NotifyTime获取数据实体
        /// </summary>
        public AlipayLog GetByNotifyTime(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.NotifyTime));
        }

        #endregion

                
        #region NotifyType

        /// <summary>
        /// 根据属性NotifyType获取数据实体
        /// </summary>
        public AlipayLog GetByNotifyType(string value)
        {
            var model = new AlipayLog { NotifyType = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.NotifyType));
        }

        /// <summary>
        /// 根据属性NotifyType获取数据实体
        /// </summary>
        public AlipayLog GetByNotifyType(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.NotifyType));
        }

        #endregion

                
        #region OurTradeNo

        /// <summary>
        /// 根据属性OurTradeNo获取数据实体
        /// </summary>
        public AlipayLog GetByOurTradeNo(string value)
        {
            var model = new AlipayLog { OurTradeNo = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.OurTradeNo));
        }

        /// <summary>
        /// 根据属性OurTradeNo获取数据实体
        /// </summary>
        public AlipayLog GetByOurTradeNo(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.OurTradeNo));
        }

        #endregion

                
        #region PaymentType

        /// <summary>
        /// 根据属性PaymentType获取数据实体
        /// </summary>
        public AlipayLog GetByPaymentType(string value)
        {
            var model = new AlipayLog { PaymentType = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.PaymentType));
        }

        /// <summary>
        /// 根据属性PaymentType获取数据实体
        /// </summary>
        public AlipayLog GetByPaymentType(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.PaymentType));
        }

        #endregion

                
        #region SellerEmail

        /// <summary>
        /// 根据属性SellerEmail获取数据实体
        /// </summary>
        public AlipayLog GetBySellerEmail(string value)
        {
            var model = new AlipayLog { SellerEmail = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.SellerEmail));
        }

        /// <summary>
        /// 根据属性SellerEmail获取数据实体
        /// </summary>
        public AlipayLog GetBySellerEmail(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.SellerEmail));
        }

        #endregion

                
        #region SellerId

        /// <summary>
        /// 根据属性SellerId获取数据实体
        /// </summary>
        public AlipayLog GetBySellerId(string value)
        {
            var model = new AlipayLog { SellerId = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.SellerId));
        }

        /// <summary>
        /// 根据属性SellerId获取数据实体
        /// </summary>
        public AlipayLog GetBySellerId(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.SellerId));
        }

        #endregion

                
        #region Sign

        /// <summary>
        /// 根据属性Sign获取数据实体
        /// </summary>
        public AlipayLog GetBySign(string value)
        {
            var model = new AlipayLog { Sign = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Sign));
        }

        /// <summary>
        /// 根据属性Sign获取数据实体
        /// </summary>
        public AlipayLog GetBySign(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Sign));
        }

        #endregion

                
        #region SignType

        /// <summary>
        /// 根据属性SignType获取数据实体
        /// </summary>
        public AlipayLog GetBySignType(string value)
        {
            var model = new AlipayLog { SignType = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.SignType));
        }

        /// <summary>
        /// 根据属性SignType获取数据实体
        /// </summary>
        public AlipayLog GetBySignType(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.SignType));
        }

        #endregion

                
        #region Subject

        /// <summary>
        /// 根据属性Subject获取数据实体
        /// </summary>
        public AlipayLog GetBySubject(string value)
        {
            var model = new AlipayLog { Subject = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Subject));
        }

        /// <summary>
        /// 根据属性Subject获取数据实体
        /// </summary>
        public AlipayLog GetBySubject(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Subject));
        }

        #endregion

                
        #region TotalFee

        /// <summary>
        /// 根据属性TotalFee获取数据实体
        /// </summary>
        public AlipayLog GetByTotalFee(string value)
        {
            var model = new AlipayLog { TotalFee = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.TotalFee));
        }

        /// <summary>
        /// 根据属性TotalFee获取数据实体
        /// </summary>
        public AlipayLog GetByTotalFee(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.TotalFee));
        }

        #endregion

                
        #region TradeNo

        /// <summary>
        /// 根据属性TradeNo获取数据实体
        /// </summary>
        public AlipayLog GetByTradeNo(string value)
        {
            var model = new AlipayLog { TradeNo = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.TradeNo));
        }

        /// <summary>
        /// 根据属性TradeNo获取数据实体
        /// </summary>
        public AlipayLog GetByTradeNo(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.TradeNo));
        }

        #endregion

                
        #region TradeStatus

        /// <summary>
        /// 根据属性TradeStatus获取数据实体
        /// </summary>
        public AlipayLog GetByTradeStatus(string value)
        {
            var model = new AlipayLog { TradeStatus = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.TradeStatus));
        }

        /// <summary>
        /// 根据属性TradeStatus获取数据实体
        /// </summary>
        public AlipayLog GetByTradeStatus(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.TradeStatus));
        }

        #endregion

                
        #region Quantity

        /// <summary>
        /// 根据属性Quantity获取数据实体
        /// </summary>
        public AlipayLog GetByQuantity(string value)
        {
            var model = new AlipayLog { Quantity = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Quantity));
        }

        /// <summary>
        /// 根据属性Quantity获取数据实体
        /// </summary>
        public AlipayLog GetByQuantity(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Quantity));
        }

        #endregion

                
        #region Price

        /// <summary>
        /// 根据属性Price获取数据实体
        /// </summary>
        public AlipayLog GetByPrice(string value)
        {
            var model = new AlipayLog { Price = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Price));
        }

        /// <summary>
        /// 根据属性Price获取数据实体
        /// </summary>
        public AlipayLog GetByPrice(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Price));
        }

        #endregion

                
        #region GmtCreate

        /// <summary>
        /// 根据属性GmtCreate获取数据实体
        /// </summary>
        public AlipayLog GetByGmtCreate(string value)
        {
            var model = new AlipayLog { GmtCreate = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.GmtCreate));
        }

        /// <summary>
        /// 根据属性GmtCreate获取数据实体
        /// </summary>
        public AlipayLog GetByGmtCreate(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.GmtCreate));
        }

        #endregion

                
        #region GmtPayment

        /// <summary>
        /// 根据属性GmtPayment获取数据实体
        /// </summary>
        public AlipayLog GetByGmtPayment(string value)
        {
            var model = new AlipayLog { GmtPayment = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.GmtPayment));
        }

        /// <summary>
        /// 根据属性GmtPayment获取数据实体
        /// </summary>
        public AlipayLog GetByGmtPayment(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.GmtPayment));
        }

        #endregion

                
        #region IsTotalFeeAdjust

        /// <summary>
        /// 根据属性IsTotalFeeAdjust获取数据实体
        /// </summary>
        public AlipayLog GetByIsTotalFeeAdjust(string value)
        {
            var model = new AlipayLog { IsTotalFeeAdjust = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.IsTotalFeeAdjust));
        }

        /// <summary>
        /// 根据属性IsTotalFeeAdjust获取数据实体
        /// </summary>
        public AlipayLog GetByIsTotalFeeAdjust(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.IsTotalFeeAdjust));
        }

        #endregion

                
        #region UseCoupon

        /// <summary>
        /// 根据属性UseCoupon获取数据实体
        /// </summary>
        public AlipayLog GetByUseCoupon(string value)
        {
            var model = new AlipayLog { UseCoupon = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.UseCoupon));
        }

        /// <summary>
        /// 根据属性UseCoupon获取数据实体
        /// </summary>
        public AlipayLog GetByUseCoupon(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.UseCoupon));
        }

        #endregion

                
        #region Discount

        /// <summary>
        /// 根据属性Discount获取数据实体
        /// </summary>
        public AlipayLog GetByDiscount(string value)
        {
            var model = new AlipayLog { Discount = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Discount));
        }

        /// <summary>
        /// 根据属性Discount获取数据实体
        /// </summary>
        public AlipayLog GetByDiscount(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Discount));
        }

        #endregion

                
        #region RefundStatus

        /// <summary>
        /// 根据属性RefundStatus获取数据实体
        /// </summary>
        public AlipayLog GetByRefundStatus(string value)
        {
            var model = new AlipayLog { RefundStatus = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.RefundStatus));
        }

        /// <summary>
        /// 根据属性RefundStatus获取数据实体
        /// </summary>
        public AlipayLog GetByRefundStatus(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.RefundStatus));
        }

        #endregion

                
        #region GmtRefund

        /// <summary>
        /// 根据属性GmtRefund获取数据实体
        /// </summary>
        public AlipayLog GetByGmtRefund(string value)
        {
            var model = new AlipayLog { GmtRefund = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.GmtRefund));
        }

        /// <summary>
        /// 根据属性GmtRefund获取数据实体
        /// </summary>
        public AlipayLog GetByGmtRefund(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.GmtRefund));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AlipayLog GetByIp(string value)
        {
            var model = new AlipayLog { Ip = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AlipayLog GetByIp(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AlipayLog GetByCreateBy(long value)
        {
            var model = new AlipayLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AlipayLog GetByCreateBy(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AlipayLog GetByCreateTime(DateTime value)
        {
            var model = new AlipayLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AlipayLog GetByCreateTime(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AlipayLog GetByLastModifyBy(long value)
        {
            var model = new AlipayLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AlipayLog GetByLastModifyBy(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AlipayLog GetByLastModifyTime(DateTime value)
        {
            var model = new AlipayLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(AlipayLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AlipayLog GetByLastModifyTime(AlipayLog model)
        {
            return GetByWhere(model.GetFilterString(AlipayLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Exterface

        /// <summary>
        /// 根据属性Exterface获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByExterface(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Exterface = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Exterface),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Exterface获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByExterface(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Exterface),sort,operateMode);
        }

        #endregion

                
        #region GetList By Body

        /// <summary>
        /// 根据属性Body获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByBody(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Body = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Body),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Body获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByBody(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Body),sort,operateMode);
        }

        #endregion

                
        #region GetList By BuyerEmail

        /// <summary>
        /// 根据属性BuyerEmail获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByBuyerEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { BuyerEmail = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.BuyerEmail),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BuyerEmail获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByBuyerEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.BuyerEmail),sort,operateMode);
        }

        #endregion

                
        #region GetList By BuyerId

        /// <summary>
        /// 根据属性BuyerId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByBuyerId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { BuyerId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.BuyerId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BuyerId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByBuyerId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.BuyerId),sort,operateMode);
        }

        #endregion

                
        #region GetList By IsSuccess

        /// <summary>
        /// 根据属性IsSuccess获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByIsSuccess(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { IsSuccess = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.IsSuccess),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IsSuccess获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByIsSuccess(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.IsSuccess),sort,operateMode);
        }

        #endregion

                
        #region GetList By NotifyId

        /// <summary>
        /// 根据属性NotifyId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByNotifyId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { NotifyId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.NotifyId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NotifyId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByNotifyId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.NotifyId),sort,operateMode);
        }

        #endregion

                
        #region GetList By NotifyTime

        /// <summary>
        /// 根据属性NotifyTime获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByNotifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { NotifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.NotifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NotifyTime获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByNotifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.NotifyTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By NotifyType

        /// <summary>
        /// 根据属性NotifyType获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByNotifyType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { NotifyType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.NotifyType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NotifyType获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByNotifyType(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.NotifyType),sort,operateMode);
        }

        #endregion

                
        #region GetList By OurTradeNo

        /// <summary>
        /// 根据属性OurTradeNo获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByOurTradeNo(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { OurTradeNo = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.OurTradeNo),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OurTradeNo获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByOurTradeNo(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.OurTradeNo),sort,operateMode);
        }

        #endregion

                
        #region GetList By PaymentType

        /// <summary>
        /// 根据属性PaymentType获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByPaymentType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { PaymentType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.PaymentType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PaymentType获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByPaymentType(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.PaymentType),sort,operateMode);
        }

        #endregion

                
        #region GetList By SellerEmail

        /// <summary>
        /// 根据属性SellerEmail获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySellerEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { SellerEmail = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.SellerEmail),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SellerEmail获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySellerEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.SellerEmail),sort,operateMode);
        }

        #endregion

                
        #region GetList By SellerId

        /// <summary>
        /// 根据属性SellerId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySellerId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { SellerId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.SellerId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SellerId获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySellerId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.SellerId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sign

        /// <summary>
        /// 根据属性Sign获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySign(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Sign = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Sign),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sign获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySign(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Sign),sort,operateMode);
        }

        #endregion

                
        #region GetList By SignType

        /// <summary>
        /// 根据属性SignType获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySignType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { SignType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.SignType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SignType获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySignType(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.SignType),sort,operateMode);
        }

        #endregion

                
        #region GetList By Subject

        /// <summary>
        /// 根据属性Subject获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySubject(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Subject = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Subject),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Subject获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListBySubject(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Subject),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalFee

        /// <summary>
        /// 根据属性TotalFee获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByTotalFee(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { TotalFee = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.TotalFee),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalFee获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByTotalFee(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.TotalFee),sort,operateMode);
        }

        #endregion

                
        #region GetList By TradeNo

        /// <summary>
        /// 根据属性TradeNo获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByTradeNo(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { TradeNo = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.TradeNo),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TradeNo获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByTradeNo(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.TradeNo),sort,operateMode);
        }

        #endregion

                
        #region GetList By TradeStatus

        /// <summary>
        /// 根据属性TradeStatus获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByTradeStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { TradeStatus = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.TradeStatus),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TradeStatus获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByTradeStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.TradeStatus),sort,operateMode);
        }

        #endregion

                
        #region GetList By Quantity

        /// <summary>
        /// 根据属性Quantity获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByQuantity(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Quantity = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Quantity),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Quantity获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByQuantity(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Quantity),sort,operateMode);
        }

        #endregion

                
        #region GetList By Price

        /// <summary>
        /// 根据属性Price获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByPrice(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Price = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Price),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Price获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByPrice(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Price),sort,operateMode);
        }

        #endregion

                
        #region GetList By GmtCreate

        /// <summary>
        /// 根据属性GmtCreate获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByGmtCreate(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { GmtCreate = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.GmtCreate),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GmtCreate获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByGmtCreate(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.GmtCreate),sort,operateMode);
        }

        #endregion

                
        #region GetList By GmtPayment

        /// <summary>
        /// 根据属性GmtPayment获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByGmtPayment(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { GmtPayment = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.GmtPayment),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GmtPayment获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByGmtPayment(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.GmtPayment),sort,operateMode);
        }

        #endregion

                
        #region GetList By IsTotalFeeAdjust

        /// <summary>
        /// 根据属性IsTotalFeeAdjust获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByIsTotalFeeAdjust(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { IsTotalFeeAdjust = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.IsTotalFeeAdjust),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IsTotalFeeAdjust获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByIsTotalFeeAdjust(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.IsTotalFeeAdjust),sort,operateMode);
        }

        #endregion

                
        #region GetList By UseCoupon

        /// <summary>
        /// 根据属性UseCoupon获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByUseCoupon(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { UseCoupon = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.UseCoupon),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UseCoupon获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByUseCoupon(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.UseCoupon),sort,operateMode);
        }

        #endregion

                
        #region GetList By Discount

        /// <summary>
        /// 根据属性Discount获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByDiscount(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Discount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Discount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Discount获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByDiscount(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Discount),sort,operateMode);
        }

        #endregion

                
        #region GetList By RefundStatus

        /// <summary>
        /// 根据属性RefundStatus获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByRefundStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { RefundStatus = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.RefundStatus),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RefundStatus获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByRefundStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.RefundStatus),sort,operateMode);
        }

        #endregion

                
        #region GetList By GmtRefund

        /// <summary>
        /// 根据属性GmtRefund获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByGmtRefund(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { GmtRefund = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.GmtRefund),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GmtRefund获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByGmtRefund(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.GmtRefund),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AlipayLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AlipayLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AlipayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AlipayLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}