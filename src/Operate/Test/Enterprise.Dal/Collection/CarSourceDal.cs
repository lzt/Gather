﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class CarSourceDal : ExBaseDal<CarSource>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CarSourceDal(): this(Initialization.GetXmlConfig(typeof(CarSource)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CarSourceDal CreateDal()
        {
			return new CarSourceDal(Initialization.GetXmlConfig(typeof(CarSource)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CarSourceDal()
        {
           ModelName= typeof(CarSource).Name;
           var item = new CarSource();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CarSourceDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "CarSource";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarSource GetByName(string value)
        {
            var model = new CarSource { Name = value };
            return GetByWhere(model.GetFilterString(CarSourceField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarSource GetByName(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.Name));
        }

        #endregion

                
        #region BrandId

        /// <summary>
        /// 根据属性BrandId获取数据实体
        /// </summary>
        public CarSource GetByBrandId(int value)
        {
            var model = new CarSource { BrandId = value };
            return GetByWhere(model.GetFilterString(CarSourceField.BrandId));
        }

        /// <summary>
        /// 根据属性BrandId获取数据实体
        /// </summary>
        public CarSource GetByBrandId(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.BrandId));
        }

        #endregion

                
        #region SourceId

        /// <summary>
        /// 根据属性SourceId获取数据实体
        /// </summary>
        public CarSource GetBySourceId(int value)
        {
            var model = new CarSource { SourceId = value };
            return GetByWhere(model.GetFilterString(CarSourceField.SourceId));
        }

        /// <summary>
        /// 根据属性SourceId获取数据实体
        /// </summary>
        public CarSource GetBySourceId(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.SourceId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarSource GetByIp(string value)
        {
            var model = new CarSource { Ip = value };
            return GetByWhere(model.GetFilterString(CarSourceField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarSource GetByIp(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarSource GetByCreateBy(long value)
        {
            var model = new CarSource { CreateBy = value };
            return GetByWhere(model.GetFilterString(CarSourceField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarSource GetByCreateBy(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarSource GetByCreateTime(DateTime value)
        {
            var model = new CarSource { CreateTime = value };
            return GetByWhere(model.GetFilterString(CarSourceField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarSource GetByCreateTime(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarSource GetByLastModifyBy(long value)
        {
            var model = new CarSource { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(CarSourceField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarSource GetByLastModifyBy(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarSource GetByLastModifyTime(DateTime value)
        {
            var model = new CarSource { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(CarSourceField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarSource GetByLastModifyTime(CarSource model)
        {
            return GetByWhere(model.GetFilterString(CarSourceField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By BrandId

        /// <summary>
        /// 根据属性BrandId获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByBrandId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { BrandId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.BrandId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BrandId获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByBrandId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.BrandId),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceId

        /// <summary>
        /// 根据属性SourceId获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListBySourceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { SourceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.SourceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceId获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListBySourceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.SourceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSource { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarSource> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSource model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSourceField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}