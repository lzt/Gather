﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class InfoSectionDal : ExBaseDal<InfoSection>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public InfoSectionDal(): this(Initialization.GetXmlConfig(typeof(InfoSection)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static InfoSectionDal CreateDal()
        {
			return new InfoSectionDal(Initialization.GetXmlConfig(typeof(InfoSection)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static InfoSectionDal()
        {
           ModelName= typeof(InfoSection).Name;
           var item = new InfoSection();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public InfoSectionDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "InfoSection";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public InfoSection GetByTitle(string value)
        {
            var model = new InfoSection { Title = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public InfoSection GetByTitle(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.Title));
        }

        #endregion

                
        #region Introduction

        /// <summary>
        /// 根据属性Introduction获取数据实体
        /// </summary>
        public InfoSection GetByIntroduction(string value)
        {
            var model = new InfoSection { Introduction = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.Introduction));
        }

        /// <summary>
        /// 根据属性Introduction获取数据实体
        /// </summary>
        public InfoSection GetByIntroduction(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.Introduction));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public InfoSection GetByContent(string value)
        {
            var model = new InfoSection { Content = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public InfoSection GetByContent(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.Content));
        }

        #endregion

                
        #region ImageUrl

        /// <summary>
        /// 根据属性ImageUrl获取数据实体
        /// </summary>
        public InfoSection GetByImageUrl(string value)
        {
            var model = new InfoSection { ImageUrl = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.ImageUrl));
        }

        /// <summary>
        /// 根据属性ImageUrl获取数据实体
        /// </summary>
        public InfoSection GetByImageUrl(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.ImageUrl));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public InfoSection GetByParentId(int value)
        {
            var model = new InfoSection { ParentId = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public InfoSection GetByParentId(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.ParentId));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public InfoSection GetByType(int value)
        {
            var model = new InfoSection { Type = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public InfoSection GetByType(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.Type));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public InfoSection GetByIp(string value)
        {
            var model = new InfoSection { Ip = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public InfoSection GetByIp(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public InfoSection GetByCreateBy(long value)
        {
            var model = new InfoSection { CreateBy = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public InfoSection GetByCreateBy(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public InfoSection GetByCreateTime(DateTime value)
        {
            var model = new InfoSection { CreateTime = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public InfoSection GetByCreateTime(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public InfoSection GetByLastModifyBy(long value)
        {
            var model = new InfoSection { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public InfoSection GetByLastModifyBy(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public InfoSection GetByLastModifyTime(DateTime value)
        {
            var model = new InfoSection { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(InfoSectionField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public InfoSection GetByLastModifyTime(InfoSection model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Introduction

        /// <summary>
        /// 根据属性Introduction获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByIntroduction(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { Introduction = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Introduction),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Introduction获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByIntroduction(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Introduction),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By ImageUrl

        /// <summary>
        /// 根据属性ImageUrl获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByImageUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { ImageUrl = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.ImageUrl),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ImageUrl获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByImageUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.ImageUrl),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSection { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<InfoSection> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSection model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}