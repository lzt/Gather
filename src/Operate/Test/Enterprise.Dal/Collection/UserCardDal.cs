﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserCardDal : ExBaseDal<UserCard>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserCardDal(): this(Initialization.GetXmlConfig(typeof(UserCard)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserCardDal CreateDal()
        {
			return new UserCardDal(Initialization.GetXmlConfig(typeof(UserCard)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserCardDal()
        {
           ModelName= typeof(UserCard).Name;
           var item = new UserCard();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserCardDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserCard";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Image

        /// <summary>
        /// 根据属性Image获取数据实体
        /// </summary>
        public UserCard GetByImage(string value)
        {
            var model = new UserCard { Image = value };
            return GetByWhere(model.GetFilterString(UserCardField.Image));
        }

        /// <summary>
        /// 根据属性Image获取数据实体
        /// </summary>
        public UserCard GetByImage(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Image));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserCard GetByUserProfileId(int value)
        {
            var model = new UserCard { UserProfileId = value };
            return GetByWhere(model.GetFilterString(UserCardField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserCard GetByUserProfileId(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.UserProfileId));
        }

        #endregion

                
        #region Card

        /// <summary>
        /// 根据属性Card获取数据实体
        /// </summary>
        public UserCard GetByCard(string value)
        {
            var model = new UserCard { Card = value };
            return GetByWhere(model.GetFilterString(UserCardField.Card));
        }

        /// <summary>
        /// 根据属性Card获取数据实体
        /// </summary>
        public UserCard GetByCard(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Card));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserCard GetByRemark(string value)
        {
            var model = new UserCard { Remark = value };
            return GetByWhere(model.GetFilterString(UserCardField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserCard GetByRemark(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Remark));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserCard GetByTitle(string value)
        {
            var model = new UserCard { Title = value };
            return GetByWhere(model.GetFilterString(UserCardField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserCard GetByTitle(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Title));
        }

        #endregion

                
        #region Affiliation

        /// <summary>
        /// 根据属性Affiliation获取数据实体
        /// </summary>
        public UserCard GetByAffiliation(string value)
        {
            var model = new UserCard { Affiliation = value };
            return GetByWhere(model.GetFilterString(UserCardField.Affiliation));
        }

        /// <summary>
        /// 根据属性Affiliation获取数据实体
        /// </summary>
        public UserCard GetByAffiliation(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Affiliation));
        }

        #endregion

                
        #region DepositBank

        /// <summary>
        /// 根据属性DepositBank获取数据实体
        /// </summary>
        public UserCard GetByDepositBank(string value)
        {
            var model = new UserCard { DepositBank = value };
            return GetByWhere(model.GetFilterString(UserCardField.DepositBank));
        }

        /// <summary>
        /// 根据属性DepositBank获取数据实体
        /// </summary>
        public UserCard GetByDepositBank(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.DepositBank));
        }

        #endregion

                
        #region Subbranch

        /// <summary>
        /// 根据属性Subbranch获取数据实体
        /// </summary>
        public UserCard GetBySubbranch(string value)
        {
            var model = new UserCard { Subbranch = value };
            return GetByWhere(model.GetFilterString(UserCardField.Subbranch));
        }

        /// <summary>
        /// 根据属性Subbranch获取数据实体
        /// </summary>
        public UserCard GetBySubbranch(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Subbranch));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserCard GetByIp(string value)
        {
            var model = new UserCard { Ip = value };
            return GetByWhere(model.GetFilterString(UserCardField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserCard GetByIp(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserCard GetByCreateBy(long value)
        {
            var model = new UserCard { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserCardField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserCard GetByCreateBy(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserCard GetByCreateTime(DateTime value)
        {
            var model = new UserCard { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserCardField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserCard GetByCreateTime(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserCard GetByLastModifyBy(long value)
        {
            var model = new UserCard { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserCardField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserCard GetByLastModifyBy(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserCard GetByLastModifyTime(DateTime value)
        {
            var model = new UserCard { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserCardField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserCard GetByLastModifyTime(UserCard model)
        {
            return GetByWhere(model.GetFilterString(UserCardField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Image

        /// <summary>
        /// 根据属性Image获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Image = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Image),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Image获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Image),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Card

        /// <summary>
        /// 根据属性Card获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Card = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Card),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Card获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Card),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Affiliation

        /// <summary>
        /// 根据属性Affiliation获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByAffiliation(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Affiliation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Affiliation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Affiliation获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByAffiliation(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Affiliation),sort,operateMode);
        }

        #endregion

                
        #region GetList By DepositBank

        /// <summary>
        /// 根据属性DepositBank获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByDepositBank(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { DepositBank = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.DepositBank),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DepositBank获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByDepositBank(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.DepositBank),sort,operateMode);
        }

        #endregion

                
        #region GetList By Subbranch

        /// <summary>
        /// 根据属性Subbranch获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListBySubbranch(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Subbranch = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Subbranch),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Subbranch获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListBySubbranch(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Subbranch),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserCard { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserCard> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserCard model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserCardField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}