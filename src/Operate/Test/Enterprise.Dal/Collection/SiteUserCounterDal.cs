﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SiteUserCounterDal : ExBaseDal<SiteUserCounter>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SiteUserCounterDal(): this(Initialization.GetXmlConfig(typeof(SiteUserCounter)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SiteUserCounterDal CreateDal()
        {
			return new SiteUserCounterDal(Initialization.GetXmlConfig(typeof(SiteUserCounter)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SiteUserCounterDal()
        {
           ModelName= typeof(SiteUserCounter).Name;
           var item = new SiteUserCounter();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SiteUserCounterDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SiteUserCounter";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region RecordDate

        /// <summary>
        /// 根据属性RecordDate获取数据实体
        /// </summary>
        public SiteUserCounter GetByRecordDate(DateTime value)
        {
            var model = new SiteUserCounter { RecordDate = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.RecordDate));
        }

        /// <summary>
        /// 根据属性RecordDate获取数据实体
        /// </summary>
        public SiteUserCounter GetByRecordDate(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.RecordDate));
        }

        #endregion

                
        #region TokenRefreshCounter

        /// <summary>
        /// 根据属性TokenRefreshCounter获取数据实体
        /// </summary>
        public SiteUserCounter GetByTokenRefreshCounter(int value)
        {
            var model = new SiteUserCounter { TokenRefreshCounter = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.TokenRefreshCounter));
        }

        /// <summary>
        /// 根据属性TokenRefreshCounter获取数据实体
        /// </summary>
        public SiteUserCounter GetByTokenRefreshCounter(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.TokenRefreshCounter));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SiteUserCounter GetByIp(string value)
        {
            var model = new SiteUserCounter { Ip = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SiteUserCounter GetByIp(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SiteUserCounter GetByCreateBy(long value)
        {
            var model = new SiteUserCounter { CreateBy = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SiteUserCounter GetByCreateBy(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SiteUserCounter GetByCreateTime(DateTime value)
        {
            var model = new SiteUserCounter { CreateTime = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SiteUserCounter GetByCreateTime(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SiteUserCounter GetByLastModifyBy(long value)
        {
            var model = new SiteUserCounter { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SiteUserCounter GetByLastModifyBy(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SiteUserCounter GetByLastModifyTime(DateTime value)
        {
            var model = new SiteUserCounter { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(SiteUserCounterField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SiteUserCounter GetByLastModifyTime(SiteUserCounter model)
        {
            return GetByWhere(model.GetFilterString(SiteUserCounterField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By RecordDate

        /// <summary>
        /// 根据属性RecordDate获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByRecordDate(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { RecordDate = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.RecordDate),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RecordDate获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByRecordDate(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.RecordDate),sort,operateMode);
        }

        #endregion

                
        #region GetList By TokenRefreshCounter

        /// <summary>
        /// 根据属性TokenRefreshCounter获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByTokenRefreshCounter(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { TokenRefreshCounter = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.TokenRefreshCounter),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TokenRefreshCounter获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByTokenRefreshCounter(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.TokenRefreshCounter),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUserCounter { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SiteUserCounter> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUserCounter model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserCounterField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}