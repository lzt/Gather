﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class InfoSectionEntryRelationDal : ExBaseDal<InfoSectionEntryRelation>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public InfoSectionEntryRelationDal(): this(Initialization.GetXmlConfig(typeof(InfoSectionEntryRelation)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static InfoSectionEntryRelationDal CreateDal()
        {
			return new InfoSectionEntryRelationDal(Initialization.GetXmlConfig(typeof(InfoSectionEntryRelation)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static InfoSectionEntryRelationDal()
        {
           ModelName= typeof(InfoSectionEntryRelation).Name;
           var item = new InfoSectionEntryRelation();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public InfoSectionEntryRelationDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "InfoSectionEntryRelation";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region InfoSectionId

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByInfoSectionId(int value)
        {
            var model = new InfoSectionEntryRelation { InfoSectionId = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.InfoSectionId));
        }

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByInfoSectionId(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.InfoSectionId));
        }

        #endregion

                
        #region InfoEntryId

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByInfoEntryId(int value)
        {
            var model = new InfoSectionEntryRelation { InfoEntryId = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.InfoEntryId));
        }

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByInfoEntryId(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.InfoEntryId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByIp(string value)
        {
            var model = new InfoSectionEntryRelation { Ip = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByIp(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByCreateBy(long value)
        {
            var model = new InfoSectionEntryRelation { CreateBy = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByCreateBy(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByCreateTime(DateTime value)
        {
            var model = new InfoSectionEntryRelation { CreateTime = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByCreateTime(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByLastModifyBy(long value)
        {
            var model = new InfoSectionEntryRelation { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByLastModifyBy(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByLastModifyTime(DateTime value)
        {
            var model = new InfoSectionEntryRelation { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public InfoSectionEntryRelation GetByLastModifyTime(InfoSectionEntryRelation model)
        {
            return GetByWhere(model.GetFilterString(InfoSectionEntryRelationField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By InfoSectionId

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByInfoSectionId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { InfoSectionId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.InfoSectionId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InfoSectionId获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByInfoSectionId(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.InfoSectionId),sort,operateMode);
        }

        #endregion

                
        #region GetList By InfoEntryId

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByInfoEntryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { InfoEntryId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.InfoEntryId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InfoEntryId获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByInfoEntryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.InfoEntryId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoSectionEntryRelation { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<InfoSectionEntryRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoSectionEntryRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoSectionEntryRelationField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}