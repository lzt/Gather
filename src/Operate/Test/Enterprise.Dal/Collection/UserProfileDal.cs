﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserProfileDal : ExBaseDal<UserProfile>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserProfileDal(): this(Initialization.GetXmlConfig(typeof(UserProfile)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserProfileDal CreateDal()
        {
			return new UserProfileDal(Initialization.GetXmlConfig(typeof(UserProfile)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserProfileDal()
        {
           ModelName= typeof(UserProfile).Name;
           var item = new UserProfile();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserProfileDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserProfile";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfile GetByLoginId(string value)
        {
            var model = new UserProfile { LoginId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.LoginId));
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体
        /// </summary>
        public UserProfile GetByLoginId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.LoginId));
        }

        #endregion

                
        #region Password

        /// <summary>
        /// 根据属性Password获取数据实体
        /// </summary>
        public UserProfile GetByPassword(string value)
        {
            var model = new UserProfile { Password = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Password));
        }

        /// <summary>
        /// 根据属性Password获取数据实体
        /// </summary>
        public UserProfile GetByPassword(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Password));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfile GetByStatusEnabled(int value)
        {
            var model = new UserProfile { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserProfileField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserProfile GetByStatusEnabled(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.StatusEnabled));
        }

        #endregion

                
        #region NickName

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfile GetByNickName(string value)
        {
            var model = new UserProfile { NickName = value };
            return GetByWhere(model.GetFilterString(UserProfileField.NickName));
        }

        /// <summary>
        /// 根据属性NickName获取数据实体
        /// </summary>
        public UserProfile GetByNickName(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.NickName));
        }

        #endregion

                
        #region LastAccessTime

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体
        /// </summary>
        public UserProfile GetByLastAccessTime(DateTime value)
        {
            var model = new UserProfile { LastAccessTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.LastAccessTime));
        }

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体
        /// </summary>
        public UserProfile GetByLastAccessTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.LastAccessTime));
        }

        #endregion

                
        #region Language

        /// <summary>
        /// 根据属性Language获取数据实体
        /// </summary>
        public UserProfile GetByLanguage(string value)
        {
            var model = new UserProfile { Language = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Language));
        }

        /// <summary>
        /// 根据属性Language获取数据实体
        /// </summary>
        public UserProfile GetByLanguage(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Language));
        }

        #endregion

                
        #region UserType

        /// <summary>
        /// 根据属性UserType获取数据实体
        /// </summary>
        public UserProfile GetByUserType(int value)
        {
            var model = new UserProfile { UserType = value };
            return GetByWhere(model.GetFilterString(UserProfileField.UserType));
        }

        /// <summary>
        /// 根据属性UserType获取数据实体
        /// </summary>
        public UserProfile GetByUserType(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.UserType));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserProfile GetByParentId(int value)
        {
            var model = new UserProfile { ParentId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserProfile GetByParentId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.ParentId));
        }

        #endregion

                
        #region OutTime

        /// <summary>
        /// 根据属性OutTime获取数据实体
        /// </summary>
        public UserProfile GetByOutTime(DateTime value)
        {
            var model = new UserProfile { OutTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.OutTime));
        }

        /// <summary>
        /// 根据属性OutTime获取数据实体
        /// </summary>
        public UserProfile GetByOutTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.OutTime));
        }

        #endregion

                
        #region Logins

        /// <summary>
        /// 根据属性Logins获取数据实体
        /// </summary>
        public UserProfile GetByLogins(int value)
        {
            var model = new UserProfile { Logins = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Logins));
        }

        /// <summary>
        /// 根据属性Logins获取数据实体
        /// </summary>
        public UserProfile GetByLogins(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Logins));
        }

        #endregion

                
        #region RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserProfile GetByRestrictIp(int value)
        {
            var model = new UserProfile { RestrictIp = value };
            return GetByWhere(model.GetFilterString(UserProfileField.RestrictIp));
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserProfile GetByRestrictIp(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.RestrictIp));
        }

        #endregion

                
        #region IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserProfile GetByIpTactics(string value)
        {
            var model = new UserProfile { IpTactics = value };
            return GetByWhere(model.GetFilterString(UserProfileField.IpTactics));
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserProfile GetByIpTactics(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.IpTactics));
        }

        #endregion

                
        #region StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserProfile GetByStatusMultiLogin(int value)
        {
            var model = new UserProfile { StatusMultiLogin = value };
            return GetByWhere(model.GetFilterString(UserProfileField.StatusMultiLogin));
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserProfile GetByStatusMultiLogin(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.StatusMultiLogin));
        }

        #endregion

                
        #region LoginIp

        /// <summary>
        /// 根据属性LoginIp获取数据实体
        /// </summary>
        public UserProfile GetByLoginIp(string value)
        {
            var model = new UserProfile { LoginIp = value };
            return GetByWhere(model.GetFilterString(UserProfileField.LoginIp));
        }

        /// <summary>
        /// 根据属性LoginIp获取数据实体
        /// </summary>
        public UserProfile GetByLoginIp(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.LoginIp));
        }

        #endregion

                
        #region LoginMac

        /// <summary>
        /// 根据属性LoginMac获取数据实体
        /// </summary>
        public UserProfile GetByLoginMac(string value)
        {
            var model = new UserProfile { LoginMac = value };
            return GetByWhere(model.GetFilterString(UserProfileField.LoginMac));
        }

        /// <summary>
        /// 根据属性LoginMac获取数据实体
        /// </summary>
        public UserProfile GetByLoginMac(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.LoginMac));
        }

        #endregion

                
        #region TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserProfile GetByTimephasedLogin(int value)
        {
            var model = new UserProfile { TimephasedLogin = value };
            return GetByWhere(model.GetFilterString(UserProfileField.TimephasedLogin));
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserProfile GetByTimephasedLogin(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.TimephasedLogin));
        }

        #endregion

                
        #region AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfile GetByAllowLoginTimeBegin(DateTime value)
        {
            var model = new UserProfile { AllowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AllowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfile GetByAllowLoginTimeBegin(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AllowLoginTimeBegin));
        }

        #endregion

                
        #region AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfile GetByAllowLoginTimeEnd(DateTime value)
        {
            var model = new UserProfile { AllowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AllowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfile GetByAllowLoginTimeEnd(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AllowLoginTimeEnd));
        }

        #endregion

                
        #region DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfile GetByDisallowLoginTimeBegin(DateTime value)
        {
            var model = new UserProfile { DisallowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DisallowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserProfile GetByDisallowLoginTimeBegin(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DisallowLoginTimeBegin));
        }

        #endregion

                
        #region DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfile GetByDisallowLoginTimeEnd(DateTime value)
        {
            var model = new UserProfile { DisallowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DisallowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserProfile GetByDisallowLoginTimeEnd(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DisallowLoginTimeEnd));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public UserProfile GetByPhone(string value)
        {
            var model = new UserProfile { Phone = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public UserProfile GetByPhone(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Phone));
        }

        #endregion

                
        #region Email

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public UserProfile GetByEmail(string value)
        {
            var model = new UserProfile { Email = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Email));
        }

        /// <summary>
        /// 根据属性Email获取数据实体
        /// </summary>
        public UserProfile GetByEmail(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Email));
        }

        #endregion

                
        #region EmailVerification

        /// <summary>
        /// 根据属性EmailVerification获取数据实体
        /// </summary>
        public UserProfile GetByEmailVerification(int value)
        {
            var model = new UserProfile { EmailVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.EmailVerification));
        }

        /// <summary>
        /// 根据属性EmailVerification获取数据实体
        /// </summary>
        public UserProfile GetByEmailVerification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.EmailVerification));
        }

        #endregion

                
        #region EmailVerifier

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体
        /// </summary>
        public UserProfile GetByEmailVerifier(int value)
        {
            var model = new UserProfile { EmailVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileField.EmailVerifier));
        }

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体
        /// </summary>
        public UserProfile GetByEmailVerifier(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.EmailVerifier));
        }

        #endregion

                
        #region EmailVerificationTime

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByEmailVerificationTime(DateTime value)
        {
            var model = new UserProfile { EmailVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.EmailVerificationTime));
        }

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByEmailVerificationTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.EmailVerificationTime));
        }

        #endregion

                
        #region EmailDenyAuditReason

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByEmailDenyAuditReason(string value)
        {
            var model = new UserProfile { EmailDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileField.EmailDenyAuditReason));
        }

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByEmailDenyAuditReason(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.EmailDenyAuditReason));
        }

        #endregion

                
        #region Salt

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public UserProfile GetBySalt(string value)
        {
            var model = new UserProfile { Salt = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Salt));
        }

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public UserProfile GetBySalt(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Salt));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserProfile GetByRemark(string value)
        {
            var model = new UserProfile { Remark = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserProfile GetByRemark(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Remark));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public UserProfile GetByVerification(int value)
        {
            var model = new UserProfile { Verification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public UserProfile GetByVerification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Verification));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByVerificationTime(DateTime value)
        {
            var model = new UserProfile { VerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByVerificationTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.VerificationTime));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public UserProfile GetByVerifier(int value)
        {
            var model = new UserProfile { Verifier = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public UserProfile GetByVerifier(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Verifier));
        }

        #endregion

                
        #region DenyAuditReason

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByDenyAuditReason(string value)
        {
            var model = new UserProfile { DenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DenyAuditReason));
        }

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByDenyAuditReason(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DenyAuditReason));
        }

        #endregion

                
        #region TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfile GetByTrueName(string value)
        {
            var model = new UserProfile { TrueName = value };
            return GetByWhere(model.GetFilterString(UserProfileField.TrueName));
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体
        /// </summary>
        public UserProfile GetByTrueName(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.TrueName));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public UserProfile GetByAvatar(string value)
        {
            var model = new UserProfile { Avatar = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public UserProfile GetByAvatar(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Avatar));
        }

        #endregion

                
        #region AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public UserProfile GetByAvatarVerification(int value)
        {
            var model = new UserProfile { AvatarVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarVerification));
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public UserProfile GetByAvatarVerification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarVerification));
        }

        #endregion

                
        #region AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public UserProfile GetByAvatarVerifier(int value)
        {
            var model = new UserProfile { AvatarVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarVerifier));
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public UserProfile GetByAvatarVerifier(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarVerifier));
        }

        #endregion

                
        #region AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByAvatarVerificationTime(DateTime value)
        {
            var model = new UserProfile { AvatarVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarVerificationTime));
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByAvatarVerificationTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarVerificationTime));
        }

        #endregion

                
        #region AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByAvatarDenyAuditReason(string value)
        {
            var model = new UserProfile { AvatarDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarDenyAuditReason));
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByAvatarDenyAuditReason(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AvatarDenyAuditReason));
        }

        #endregion

                
        #region Sex

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfile GetBySex(int value)
        {
            var model = new UserProfile { Sex = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Sex));
        }

        /// <summary>
        /// 根据属性Sex获取数据实体
        /// </summary>
        public UserProfile GetBySex(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Sex));
        }

        #endregion

                
        #region Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfile GetByBirthday(DateTime value)
        {
            var model = new UserProfile { Birthday = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Birthday));
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体
        /// </summary>
        public UserProfile GetByBirthday(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Birthday));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfile GetByAppId(string value)
        {
            var model = new UserProfile { AppId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public UserProfile GetByAppId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfile GetByAppSecret(string value)
        {
            var model = new UserProfile { AppSecret = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public UserProfile GetByAppSecret(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AppSecret));
        }

        #endregion

                
        #region CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public UserProfile GetByCurrentScore(int value)
        {
            var model = new UserProfile { CurrentScore = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CurrentScore));
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public UserProfile GetByCurrentScore(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CurrentScore));
        }

        #endregion

                
        #region TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public UserProfile GetByTotalScore(int value)
        {
            var model = new UserProfile { TotalScore = value };
            return GetByWhere(model.GetFilterString(UserProfileField.TotalScore));
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public UserProfile GetByTotalScore(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.TotalScore));
        }

        #endregion

                
        #region TotalIncome

        /// <summary>
        /// 根据属性TotalIncome获取数据实体
        /// </summary>
        public UserProfile GetByTotalIncome(int value)
        {
            var model = new UserProfile { TotalIncome = value };
            return GetByWhere(model.GetFilterString(UserProfileField.TotalIncome));
        }

        /// <summary>
        /// 根据属性TotalIncome获取数据实体
        /// </summary>
        public UserProfile GetByTotalIncome(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.TotalIncome));
        }

        #endregion

                
        #region AvailableIncome

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体
        /// </summary>
        public UserProfile GetByAvailableIncome(int value)
        {
            var model = new UserProfile { AvailableIncome = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AvailableIncome));
        }

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体
        /// </summary>
        public UserProfile GetByAvailableIncome(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AvailableIncome));
        }

        #endregion

                
        #region Paid

        /// <summary>
        /// 根据属性Paid获取数据实体
        /// </summary>
        public UserProfile GetByPaid(int value)
        {
            var model = new UserProfile { Paid = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Paid));
        }

        /// <summary>
        /// 根据属性Paid获取数据实体
        /// </summary>
        public UserProfile GetByPaid(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Paid));
        }

        #endregion

                
        #region Zipcode

        /// <summary>
        /// 根据属性Zipcode获取数据实体
        /// </summary>
        public UserProfile GetByZipcode(string value)
        {
            var model = new UserProfile { Zipcode = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Zipcode));
        }

        /// <summary>
        /// 根据属性Zipcode获取数据实体
        /// </summary>
        public UserProfile GetByZipcode(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Zipcode));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserProfile GetByAddress(string value)
        {
            var model = new UserProfile { Address = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public UserProfile GetByAddress(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Address));
        }

        #endregion

                
        #region ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public UserProfile GetByProvinceId(int value)
        {
            var model = new UserProfile { ProvinceId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.ProvinceId));
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public UserProfile GetByProvinceId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.ProvinceId));
        }

        #endregion

                
        #region CityId

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public UserProfile GetByCityId(int value)
        {
            var model = new UserProfile { CityId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CityId));
        }

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public UserProfile GetByCityId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CityId));
        }

        #endregion

                
        #region TownId

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public UserProfile GetByTownId(int value)
        {
            var model = new UserProfile { TownId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.TownId));
        }

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public UserProfile GetByTownId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.TownId));
        }

        #endregion

                
        #region Identification

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfile GetByIdentification(string value)
        {
            var model = new UserProfile { Identification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Identification));
        }

        /// <summary>
        /// 根据属性Identification获取数据实体
        /// </summary>
        public UserProfile GetByIdentification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Identification));
        }

        #endregion

                
        #region IdentificationImage

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationImage(string value)
        {
            var model = new UserProfile { IdentificationImage = value };
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationImage));
        }

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationImage(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationImage));
        }

        #endregion

                
        #region IdentificationVerification

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationVerification(int value)
        {
            var model = new UserProfile { IdentificationVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationVerification));
        }

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationVerification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationVerification));
        }

        #endregion

                
        #region IdentificationVerifier

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationVerifier(int value)
        {
            var model = new UserProfile { IdentificationVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationVerifier));
        }

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationVerifier(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationVerifier));
        }

        #endregion

                
        #region IdentificationVerificationTime

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationVerificationTime(DateTime value)
        {
            var model = new UserProfile { IdentificationVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationVerificationTime));
        }

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationVerificationTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationVerificationTime));
        }

        #endregion

                
        #region IdentificationDenyAuditReason

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationDenyAuditReason(string value)
        {
            var model = new UserProfile { IdentificationDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationDenyAuditReason));
        }

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByIdentificationDenyAuditReason(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.IdentificationDenyAuditReason));
        }

        #endregion

                
        #region DriverLicense

        /// <summary>
        /// 根据属性DriverLicense获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicense(string value)
        {
            var model = new UserProfile { DriverLicense = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicense));
        }

        /// <summary>
        /// 根据属性DriverLicense获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicense(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicense));
        }

        #endregion

                
        #region DriverLicenseImage

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseImage(string value)
        {
            var model = new UserProfile { DriverLicenseImage = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseImage));
        }

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseImage(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseImage));
        }

        #endregion

                
        #region DriverLicenseVerification

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseVerification(int value)
        {
            var model = new UserProfile { DriverLicenseVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseVerification));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseVerification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseVerification));
        }

        #endregion

                
        #region DriverLicenseVerifier

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseVerifier(int value)
        {
            var model = new UserProfile { DriverLicenseVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseVerifier));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseVerifier(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseVerifier));
        }

        #endregion

                
        #region DriverLicenseVerificationTime

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseVerificationTime(DateTime value)
        {
            var model = new UserProfile { DriverLicenseVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseVerificationTime));
        }

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseVerificationTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseVerificationTime));
        }

        #endregion

                
        #region DriverLicenseDenyAuditReason

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseDenyAuditReason(string value)
        {
            var model = new UserProfile { DriverLicenseDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseDenyAuditReason));
        }

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByDriverLicenseDenyAuditReason(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DriverLicenseDenyAuditReason));
        }

        #endregion

                
        #region CreditCard

        /// <summary>
        /// 根据属性CreditCard获取数据实体
        /// </summary>
        public UserProfile GetByCreditCard(string value)
        {
            var model = new UserProfile { CreditCard = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCard));
        }

        /// <summary>
        /// 根据属性CreditCard获取数据实体
        /// </summary>
        public UserProfile GetByCreditCard(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCard));
        }

        #endregion

                
        #region CreditCardImage

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardImage(string value)
        {
            var model = new UserProfile { CreditCardImage = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardImage));
        }

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardImage(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardImage));
        }

        #endregion

                
        #region CreditCardVerification

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardVerification(int value)
        {
            var model = new UserProfile { CreditCardVerification = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardVerification));
        }

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardVerification(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardVerification));
        }

        #endregion

                
        #region CreditCardVerifier

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardVerifier(int value)
        {
            var model = new UserProfile { CreditCardVerifier = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardVerifier));
        }

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardVerifier(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardVerifier));
        }

        #endregion

                
        #region CreditCardVerificationTime

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardVerificationTime(DateTime value)
        {
            var model = new UserProfile { CreditCardVerificationTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardVerificationTime));
        }

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardVerificationTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardVerificationTime));
        }

        #endregion

                
        #region CreditCardDenyAuditReason

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardDenyAuditReason(string value)
        {
            var model = new UserProfile { CreditCardDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardDenyAuditReason));
        }

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体
        /// </summary>
        public UserProfile GetByCreditCardDenyAuditReason(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreditCardDenyAuditReason));
        }

        #endregion

                
        #region FrozenFunds

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体
        /// </summary>
        public UserProfile GetByFrozenFunds(double value)
        {
            var model = new UserProfile { FrozenFunds = value };
            return GetByWhere(model.GetFilterString(UserProfileField.FrozenFunds));
        }

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体
        /// </summary>
        public UserProfile GetByFrozenFunds(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.FrozenFunds));
        }

        #endregion

                
        #region GiftFunds

        /// <summary>
        /// 根据属性GiftFunds获取数据实体
        /// </summary>
        public UserProfile GetByGiftFunds(double value)
        {
            var model = new UserProfile { GiftFunds = value };
            return GetByWhere(model.GetFilterString(UserProfileField.GiftFunds));
        }

        /// <summary>
        /// 根据属性GiftFunds获取数据实体
        /// </summary>
        public UserProfile GetByGiftFunds(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.GiftFunds));
        }

        #endregion

                
        #region ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public UserProfile GetByThawTime(DateTime value)
        {
            var model = new UserProfile { ThawTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.ThawTime));
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public UserProfile GetByThawTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.ThawTime));
        }

        #endregion

                
        #region FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public UserProfile GetByFrozenTime(DateTime value)
        {
            var model = new UserProfile { FrozenTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.FrozenTime));
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public UserProfile GetByFrozenTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.FrozenTime));
        }

        #endregion

                
        #region QQ

        /// <summary>
        /// 根据属性QQ获取数据实体
        /// </summary>
        public UserProfile GetByQQ(string value)
        {
            var model = new UserProfile { QQ = value };
            return GetByWhere(model.GetFilterString(UserProfileField.QQ));
        }

        /// <summary>
        /// 根据属性QQ获取数据实体
        /// </summary>
        public UserProfile GetByQQ(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.QQ));
        }

        #endregion

                
        #region EmergencyPhone

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体
        /// </summary>
        public UserProfile GetByEmergencyPhone(string value)
        {
            var model = new UserProfile { EmergencyPhone = value };
            return GetByWhere(model.GetFilterString(UserProfileField.EmergencyPhone));
        }

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体
        /// </summary>
        public UserProfile GetByEmergencyPhone(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.EmergencyPhone));
        }

        #endregion

                
        #region Grade

        /// <summary>
        /// 根据属性Grade获取数据实体
        /// </summary>
        public UserProfile GetByGrade(int value)
        {
            var model = new UserProfile { Grade = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Grade));
        }

        /// <summary>
        /// 根据属性Grade获取数据实体
        /// </summary>
        public UserProfile GetByGrade(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Grade));
        }

        #endregion

                
        #region AvailableRecharge

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体
        /// </summary>
        public UserProfile GetByAvailableRecharge(int value)
        {
            var model = new UserProfile { AvailableRecharge = value };
            return GetByWhere(model.GetFilterString(UserProfileField.AvailableRecharge));
        }

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体
        /// </summary>
        public UserProfile GetByAvailableRecharge(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.AvailableRecharge));
        }

        #endregion

                
        #region TotalRecharge

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体
        /// </summary>
        public UserProfile GetByTotalRecharge(int value)
        {
            var model = new UserProfile { TotalRecharge = value };
            return GetByWhere(model.GetFilterString(UserProfileField.TotalRecharge));
        }

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体
        /// </summary>
        public UserProfile GetByTotalRecharge(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.TotalRecharge));
        }

        #endregion

                
        #region DiscountCardMode

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体
        /// </summary>
        public UserProfile GetByDiscountCardMode(int value)
        {
            var model = new UserProfile { DiscountCardMode = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DiscountCardMode));
        }

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体
        /// </summary>
        public UserProfile GetByDiscountCardMode(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DiscountCardMode));
        }

        #endregion

                
        #region DiscountCardDenomination

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体
        /// </summary>
        public UserProfile GetByDiscountCardDenomination(int value)
        {
            var model = new UserProfile { DiscountCardDenomination = value };
            return GetByWhere(model.GetFilterString(UserProfileField.DiscountCardDenomination));
        }

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体
        /// </summary>
        public UserProfile GetByDiscountCardDenomination(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.DiscountCardDenomination));
        }

        #endregion

                
        #region StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfile GetByStatusInvalid(int value)
        {
            var model = new UserProfile { StatusInvalid = value };
            return GetByWhere(model.GetFilterString(UserProfileField.StatusInvalid));
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public UserProfile GetByStatusInvalid(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.StatusInvalid));
        }

        #endregion

                
        #region InviterId

        /// <summary>
        /// 根据属性InviterId获取数据实体
        /// </summary>
        public UserProfile GetByInviterId(int value)
        {
            var model = new UserProfile { InviterId = value };
            return GetByWhere(model.GetFilterString(UserProfileField.InviterId));
        }

        /// <summary>
        /// 根据属性InviterId获取数据实体
        /// </summary>
        public UserProfile GetByInviterId(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.InviterId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfile GetByIp(string value)
        {
            var model = new UserProfile { Ip = value };
            return GetByWhere(model.GetFilterString(UserProfileField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfile GetByIp(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfile GetByCreateBy(long value)
        {
            var model = new UserProfile { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfile GetByCreateBy(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfile GetByCreateTime(DateTime value)
        {
            var model = new UserProfile { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfile GetByCreateTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfile GetByLastModifyBy(long value)
        {
            var model = new UserProfile { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserProfileField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfile GetByLastModifyBy(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfile GetByLastModifyTime(DateTime value)
        {
            var model = new UserProfile { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserProfileField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfile GetByLastModifyTime(UserProfile model)
        {
            return GetByWhere(model.GetFilterString(UserProfileField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By LoginId

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { LoginId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LoginId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLoginId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LoginId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Password

        /// <summary>
        /// 根据属性Password获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByPassword(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Password = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Password),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Password获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByPassword(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Password),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By NickName

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { NickName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.NickName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性NickName获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByNickName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.NickName),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastAccessTime

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLastAccessTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { LastAccessTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LastAccessTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastAccessTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLastAccessTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LastAccessTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Language

        /// <summary>
        /// 根据属性Language获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLanguage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Language = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Language),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Language获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLanguage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Language),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserType

        /// <summary>
        /// 根据属性UserType获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByUserType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { UserType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.UserType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserType获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByUserType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.UserType),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By OutTime

        /// <summary>
        /// 根据属性OutTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByOutTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { OutTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.OutTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OutTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByOutTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.OutTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Logins

        /// <summary>
        /// 根据属性Logins获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLogins(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Logins = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Logins),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Logins获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLogins(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Logins),sort,operateMode);
        }

        #endregion

                
        #region GetList By RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { RestrictIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.RestrictIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.RestrictIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { IpTactics = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IpTactics),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IpTactics),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { StatusMultiLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.StatusMultiLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.StatusMultiLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginIp

        /// <summary>
        /// 根据属性LoginIp获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLoginIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { LoginIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LoginIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginIp获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLoginIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LoginIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By LoginMac

        /// <summary>
        /// 根据属性LoginMac获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLoginMac(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { LoginMac = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LoginMac),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LoginMac获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLoginMac(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LoginMac),sort,operateMode);
        }

        #endregion

                
        #region GetList By TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { TimephasedLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TimephasedLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TimephasedLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AllowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AllowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AllowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AllowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AllowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AllowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DisallowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DisallowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DisallowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DisallowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DisallowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DisallowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Email

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Email = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Email),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Email获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmail(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Email),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerification

        /// <summary>
        /// 根据属性EmailVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { EmailVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerifier

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { EmailVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailVerificationTime

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { EmailVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmailDenyAuditReason

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { EmailDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmailDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmailDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmailDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Salt

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Salt = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Salt),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Salt),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By DenyAuditReason

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By TrueName

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { TrueName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TrueName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TrueName获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTrueName(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TrueName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AvatarVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AvatarVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AvatarVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AvatarDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvatarDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Sex

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Sex = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Sex),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Sex获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListBySex(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Sex),sort,operateMode);
        }

        #endregion

                
        #region GetList By Birthday

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Birthday = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Birthday),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Birthday获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByBirthday(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Birthday),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CurrentScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CurrentScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CurrentScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { TotalScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TotalScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TotalScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalIncome

        /// <summary>
        /// 根据属性TotalIncome获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTotalIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { TotalIncome = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TotalIncome),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalIncome获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTotalIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TotalIncome),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableIncome

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvailableIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AvailableIncome = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvailableIncome),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableIncome获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvailableIncome(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvailableIncome),sort,operateMode);
        }

        #endregion

                
        #region GetList By Paid

        /// <summary>
        /// 根据属性Paid获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByPaid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Paid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Paid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Paid获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByPaid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Paid),sort,operateMode);
        }

        #endregion

                
        #region GetList By Zipcode

        /// <summary>
        /// 根据属性Zipcode获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByZipcode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Zipcode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Zipcode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Zipcode获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByZipcode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Zipcode),sort,operateMode);
        }

        #endregion

                
        #region GetList By Address

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Address = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Address),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Address),sort,operateMode);
        }

        #endregion

                
        #region GetList By ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { ProvinceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.ProvinceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.ProvinceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CityId

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CityId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CityId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CityId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TownId

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { TownId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TownId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TownId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Identification

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Identification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Identification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Identification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Identification),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationImage

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { IdentificationImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationImage获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerification

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { IdentificationVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerifier

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { IdentificationVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationVerificationTime

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { IdentificationVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By IdentificationDenyAuditReason

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { IdentificationDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IdentificationDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIdentificationDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.IdentificationDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicense

        /// <summary>
        /// 根据属性DriverLicense获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DriverLicense = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicense),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicense获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicense),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseImage

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DriverLicenseImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseImage获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerification

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DriverLicenseVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerifier

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DriverLicenseVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseVerificationTime

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DriverLicenseVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By DriverLicenseDenyAuditReason

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DriverLicenseDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DriverLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDriverLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DriverLicenseDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCard

        /// <summary>
        /// 根据属性CreditCard获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreditCard = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCard),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCard获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCard),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardImage

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreditCardImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardImage获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerification

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreditCardVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerification获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerifier

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreditCardVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerifier获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardVerificationTime

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreditCardVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardVerificationTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreditCardDenyAuditReason

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreditCardDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreditCardDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreditCardDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreditCardDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenFunds

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByFrozenFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { FrozenFunds = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.FrozenFunds),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenFunds获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByFrozenFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.FrozenFunds),sort,operateMode);
        }

        #endregion

                
        #region GetList By GiftFunds

        /// <summary>
        /// 根据属性GiftFunds获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByGiftFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { GiftFunds = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.GiftFunds),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GiftFunds获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByGiftFunds(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.GiftFunds),sort,operateMode);
        }

        #endregion

                
        #region GetList By ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { ThawTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.ThawTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.ThawTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { FrozenTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.FrozenTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.FrozenTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By QQ

        /// <summary>
        /// 根据属性QQ获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByQQ(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { QQ = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.QQ),sort,operateMode);
        }

        /// <summary>
        /// 根据属性QQ获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByQQ(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.QQ),sort,operateMode);
        }

        #endregion

                
        #region GetList By EmergencyPhone

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmergencyPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { EmergencyPhone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmergencyPhone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性EmergencyPhone获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByEmergencyPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.EmergencyPhone),sort,operateMode);
        }

        #endregion

                
        #region GetList By Grade

        /// <summary>
        /// 根据属性Grade获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByGrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Grade = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Grade),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Grade获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByGrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Grade),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableRecharge

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvailableRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { AvailableRecharge = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvailableRecharge),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByAvailableRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.AvailableRecharge),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalRecharge

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTotalRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { TotalRecharge = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TotalRecharge),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalRecharge获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByTotalRecharge(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.TotalRecharge),sort,operateMode);
        }

        #endregion

                
        #region GetList By DiscountCardMode

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDiscountCardMode(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DiscountCardMode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DiscountCardMode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DiscountCardMode获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDiscountCardMode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DiscountCardMode),sort,operateMode);
        }

        #endregion

                
        #region GetList By DiscountCardDenomination

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDiscountCardDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { DiscountCardDenomination = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DiscountCardDenomination),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DiscountCardDenomination获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByDiscountCardDenomination(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.DiscountCardDenomination),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { StatusInvalid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.StatusInvalid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.StatusInvalid),sort,operateMode);
        }

        #endregion

                
        #region GetList By InviterId

        /// <summary>
        /// 根据属性InviterId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByInviterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { InviterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.InviterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InviterId获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByInviterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.InviterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfile { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfile> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfile model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}