﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SiteUserDal : ExBaseDal<SiteUser>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SiteUserDal(): this(Initialization.GetXmlConfig(typeof(SiteUser)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SiteUserDal CreateDal()
        {
			return new SiteUserDal(Initialization.GetXmlConfig(typeof(SiteUser)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SiteUserDal()
        {
           ModelName= typeof(SiteUser).Name;
           var item = new SiteUser();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SiteUserDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SiteUser";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public SiteUser GetByName(string value)
        {
            var model = new SiteUser { Name = value };
            return GetByWhere(model.GetFilterString(SiteUserField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public SiteUser GetByName(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.Name));
        }

        #endregion

                
        #region Host

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public SiteUser GetByHost(string value)
        {
            var model = new SiteUser { Host = value };
            return GetByWhere(model.GetFilterString(SiteUserField.Host));
        }

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public SiteUser GetByHost(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.Host));
        }

        #endregion

                
        #region ClientSecret

        /// <summary>
        /// 根据属性ClientSecret获取数据实体
        /// </summary>
        public SiteUser GetByClientSecret(string value)
        {
            var model = new SiteUser { ClientSecret = value };
            return GetByWhere(model.GetFilterString(SiteUserField.ClientSecret));
        }

        /// <summary>
        /// 根据属性ClientSecret获取数据实体
        /// </summary>
        public SiteUser GetByClientSecret(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.ClientSecret));
        }

        #endregion

                
        #region ClientIdentifier

        /// <summary>
        /// 根据属性ClientIdentifier获取数据实体
        /// </summary>
        public SiteUser GetByClientIdentifier(string value)
        {
            var model = new SiteUser { ClientIdentifier = value };
            return GetByWhere(model.GetFilterString(SiteUserField.ClientIdentifier));
        }

        /// <summary>
        /// 根据属性ClientIdentifier获取数据实体
        /// </summary>
        public SiteUser GetByClientIdentifier(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.ClientIdentifier));
        }

        #endregion

                
        #region ServerSecret

        /// <summary>
        /// 根据属性ServerSecret获取数据实体
        /// </summary>
        public SiteUser GetByServerSecret(string value)
        {
            var model = new SiteUser { ServerSecret = value };
            return GetByWhere(model.GetFilterString(SiteUserField.ServerSecret));
        }

        /// <summary>
        /// 根据属性ServerSecret获取数据实体
        /// </summary>
        public SiteUser GetByServerSecret(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.ServerSecret));
        }

        #endregion

                
        #region SiteType

        /// <summary>
        /// 根据属性SiteType获取数据实体
        /// </summary>
        public SiteUser GetBySiteType(long value)
        {
            var model = new SiteUser { SiteType = value };
            return GetByWhere(model.GetFilterString(SiteUserField.SiteType));
        }

        /// <summary>
        /// 根据属性SiteType获取数据实体
        /// </summary>
        public SiteUser GetBySiteType(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.SiteType));
        }

        #endregion

                
        #region AppId

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public SiteUser GetByAppId(string value)
        {
            var model = new SiteUser { AppId = value };
            return GetByWhere(model.GetFilterString(SiteUserField.AppId));
        }

        /// <summary>
        /// 根据属性AppId获取数据实体
        /// </summary>
        public SiteUser GetByAppId(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.AppId));
        }

        #endregion

                
        #region AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public SiteUser GetByAppSecret(string value)
        {
            var model = new SiteUser { AppSecret = value };
            return GetByWhere(model.GetFilterString(SiteUserField.AppSecret));
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体
        /// </summary>
        public SiteUser GetByAppSecret(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.AppSecret));
        }

        #endregion

                
        #region Salt

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public SiteUser GetBySalt(string value)
        {
            var model = new SiteUser { Salt = value };
            return GetByWhere(model.GetFilterString(SiteUserField.Salt));
        }

        /// <summary>
        /// 根据属性Salt获取数据实体
        /// </summary>
        public SiteUser GetBySalt(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.Salt));
        }

        #endregion

                
        #region Callback

        /// <summary>
        /// 根据属性Callback获取数据实体
        /// </summary>
        public SiteUser GetByCallback(string value)
        {
            var model = new SiteUser { Callback = value };
            return GetByWhere(model.GetFilterString(SiteUserField.Callback));
        }

        /// <summary>
        /// 根据属性Callback获取数据实体
        /// </summary>
        public SiteUser GetByCallback(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.Callback));
        }

        #endregion

                
        #region ExpiresTime

        /// <summary>
        /// 根据属性ExpiresTime获取数据实体
        /// </summary>
        public SiteUser GetByExpiresTime(DateTime value)
        {
            var model = new SiteUser { ExpiresTime = value };
            return GetByWhere(model.GetFilterString(SiteUserField.ExpiresTime));
        }

        /// <summary>
        /// 根据属性ExpiresTime获取数据实体
        /// </summary>
        public SiteUser GetByExpiresTime(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.ExpiresTime));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public SiteUser GetByStatusEnabled(long value)
        {
            var model = new SiteUser { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(SiteUserField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public SiteUser GetByStatusEnabled(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.StatusEnabled));
        }

        #endregion

                
        #region StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public SiteUser GetByStatusDelete(long value)
        {
            var model = new SiteUser { StatusDelete = value };
            return GetByWhere(model.GetFilterString(SiteUserField.StatusDelete));
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public SiteUser GetByStatusDelete(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.StatusDelete));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SiteUser GetByIp(string value)
        {
            var model = new SiteUser { Ip = value };
            return GetByWhere(model.GetFilterString(SiteUserField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SiteUser GetByIp(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SiteUser GetByCreateBy(long value)
        {
            var model = new SiteUser { CreateBy = value };
            return GetByWhere(model.GetFilterString(SiteUserField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SiteUser GetByCreateBy(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SiteUser GetByCreateTime(DateTime value)
        {
            var model = new SiteUser { CreateTime = value };
            return GetByWhere(model.GetFilterString(SiteUserField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SiteUser GetByCreateTime(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SiteUser GetByLastModifyBy(long value)
        {
            var model = new SiteUser { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(SiteUserField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SiteUser GetByLastModifyBy(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SiteUser GetByLastModifyTime(DateTime value)
        {
            var model = new SiteUser { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(SiteUserField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SiteUser GetByLastModifyTime(SiteUser model)
        {
            return GetByWhere(model.GetFilterString(SiteUserField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By Host

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { Host = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Host),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Host),sort,operateMode);
        }

        #endregion

                
        #region GetList By ClientSecret

        /// <summary>
        /// 根据属性ClientSecret获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByClientSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { ClientSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ClientSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ClientSecret获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByClientSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ClientSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By ClientIdentifier

        /// <summary>
        /// 根据属性ClientIdentifier获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByClientIdentifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { ClientIdentifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ClientIdentifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ClientIdentifier获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByClientIdentifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ClientIdentifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By ServerSecret

        /// <summary>
        /// 根据属性ServerSecret获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByServerSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { ServerSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ServerSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ServerSecret获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByServerSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ServerSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By SiteType

        /// <summary>
        /// 根据属性SiteType获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListBySiteType(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { SiteType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.SiteType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SiteType获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListBySiteType(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.SiteType),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppId

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { AppId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.AppId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppId获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByAppId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.AppId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AppSecret

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { AppSecret = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.AppSecret),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AppSecret获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByAppSecret(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.AppSecret),sort,operateMode);
        }

        #endregion

                
        #region GetList By Salt

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { Salt = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Salt),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Salt获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListBySalt(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Salt),sort,operateMode);
        }

        #endregion

                
        #region GetList By Callback

        /// <summary>
        /// 根据属性Callback获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByCallback(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { Callback = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Callback),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Callback获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByCallback(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Callback),sort,operateMode);
        }

        #endregion

                
        #region GetList By ExpiresTime

        /// <summary>
        /// 根据属性ExpiresTime获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByExpiresTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { ExpiresTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ExpiresTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ExpiresTime获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByExpiresTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.ExpiresTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { StatusDelete = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.StatusDelete),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.StatusDelete),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SiteUser { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SiteUser> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SiteUser model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SiteUserField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}