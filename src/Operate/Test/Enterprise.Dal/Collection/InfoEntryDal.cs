﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class InfoEntryDal : ExBaseDal<InfoEntry>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public InfoEntryDal(): this(Initialization.GetXmlConfig(typeof(InfoEntry)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static InfoEntryDal CreateDal()
        {
			return new InfoEntryDal(Initialization.GetXmlConfig(typeof(InfoEntry)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static InfoEntryDal()
        {
           ModelName= typeof(InfoEntry).Name;
           var item = new InfoEntry();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public InfoEntryDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "InfoEntry";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region ImageUrl

        /// <summary>
        /// 根据属性ImageUrl获取数据实体
        /// </summary>
        public InfoEntry GetByImageUrl(string value)
        {
            var model = new InfoEntry { ImageUrl = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.ImageUrl));
        }

        /// <summary>
        /// 根据属性ImageUrl获取数据实体
        /// </summary>
        public InfoEntry GetByImageUrl(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.ImageUrl));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public InfoEntry GetByTitle(string value)
        {
            var model = new InfoEntry { Title = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public InfoEntry GetByTitle(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.Title));
        }

        #endregion

                
        #region Introduction

        /// <summary>
        /// 根据属性Introduction获取数据实体
        /// </summary>
        public InfoEntry GetByIntroduction(string value)
        {
            var model = new InfoEntry { Introduction = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.Introduction));
        }

        /// <summary>
        /// 根据属性Introduction获取数据实体
        /// </summary>
        public InfoEntry GetByIntroduction(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.Introduction));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public InfoEntry GetByContent(string value)
        {
            var model = new InfoEntry { Content = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public InfoEntry GetByContent(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.Content));
        }

        #endregion

                
        #region ReadCount

        /// <summary>
        /// 根据属性ReadCount获取数据实体
        /// </summary>
        public InfoEntry GetByReadCount(int value)
        {
            var model = new InfoEntry { ReadCount = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.ReadCount));
        }

        /// <summary>
        /// 根据属性ReadCount获取数据实体
        /// </summary>
        public InfoEntry GetByReadCount(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.ReadCount));
        }

        #endregion

                
        #region OrderValue

        /// <summary>
        /// 根据属性OrderValue获取数据实体
        /// </summary>
        public InfoEntry GetByOrderValue(int value)
        {
            var model = new InfoEntry { OrderValue = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.OrderValue));
        }

        /// <summary>
        /// 根据属性OrderValue获取数据实体
        /// </summary>
        public InfoEntry GetByOrderValue(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.OrderValue));
        }

        #endregion

                
        #region StatusRecommend

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体
        /// </summary>
        public InfoEntry GetByStatusRecommend(int value)
        {
            var model = new InfoEntry { StatusRecommend = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.StatusRecommend));
        }

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体
        /// </summary>
        public InfoEntry GetByStatusRecommend(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.StatusRecommend));
        }

        #endregion

                
        #region StatusTop

        /// <summary>
        /// 根据属性StatusTop获取数据实体
        /// </summary>
        public InfoEntry GetByStatusTop(int value)
        {
            var model = new InfoEntry { StatusTop = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.StatusTop));
        }

        /// <summary>
        /// 根据属性StatusTop获取数据实体
        /// </summary>
        public InfoEntry GetByStatusTop(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.StatusTop));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public InfoEntry GetByIp(string value)
        {
            var model = new InfoEntry { Ip = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public InfoEntry GetByIp(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public InfoEntry GetByCreateBy(long value)
        {
            var model = new InfoEntry { CreateBy = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public InfoEntry GetByCreateBy(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public InfoEntry GetByCreateTime(DateTime value)
        {
            var model = new InfoEntry { CreateTime = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public InfoEntry GetByCreateTime(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public InfoEntry GetByLastModifyBy(long value)
        {
            var model = new InfoEntry { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public InfoEntry GetByLastModifyBy(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public InfoEntry GetByLastModifyTime(DateTime value)
        {
            var model = new InfoEntry { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(InfoEntryField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public InfoEntry GetByLastModifyTime(InfoEntry model)
        {
            return GetByWhere(model.GetFilterString(InfoEntryField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By ImageUrl

        /// <summary>
        /// 根据属性ImageUrl获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByImageUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { ImageUrl = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.ImageUrl),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ImageUrl获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByImageUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.ImageUrl),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Introduction

        /// <summary>
        /// 根据属性Introduction获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByIntroduction(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { Introduction = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Introduction),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Introduction获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByIntroduction(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Introduction),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By ReadCount

        /// <summary>
        /// 根据属性ReadCount获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByReadCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { ReadCount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.ReadCount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ReadCount获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByReadCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.ReadCount),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrderValue

        /// <summary>
        /// 根据属性OrderValue获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByOrderValue(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { OrderValue = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.OrderValue),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrderValue获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByOrderValue(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.OrderValue),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusRecommend

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByStatusRecommend(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { StatusRecommend = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.StatusRecommend),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusRecommend获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByStatusRecommend(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.StatusRecommend),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusTop

        /// <summary>
        /// 根据属性StatusTop获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByStatusTop(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { StatusTop = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.StatusTop),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusTop获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByStatusTop(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.StatusTop),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new InfoEntry { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<InfoEntry> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, InfoEntry model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(InfoEntryField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}