﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class OnlineMessageDal : ExBaseDal<OnlineMessage>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public OnlineMessageDal(): this(Initialization.GetXmlConfig(typeof(OnlineMessage)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static OnlineMessageDal CreateDal()
        {
			return new OnlineMessageDal(Initialization.GetXmlConfig(typeof(OnlineMessage)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static OnlineMessageDal()
        {
           ModelName= typeof(OnlineMessage).Name;
           var item = new OnlineMessage();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public OnlineMessageDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "OnlineMessage";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public OnlineMessage GetByUserProfileId(int value)
        {
            var model = new OnlineMessage { UserProfileId = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public OnlineMessage GetByUserProfileId(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.UserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public OnlineMessage GetByContent(string value)
        {
            var model = new OnlineMessage { Content = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public OnlineMessage GetByContent(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.Content));
        }

        #endregion

                
        #region ReplyUserId

        /// <summary>
        /// 根据属性ReplyUserId获取数据实体
        /// </summary>
        public OnlineMessage GetByReplyUserId(int value)
        {
            var model = new OnlineMessage { ReplyUserId = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.ReplyUserId));
        }

        /// <summary>
        /// 根据属性ReplyUserId获取数据实体
        /// </summary>
        public OnlineMessage GetByReplyUserId(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.ReplyUserId));
        }

        #endregion

                
        #region Reply

        /// <summary>
        /// 根据属性Reply获取数据实体
        /// </summary>
        public OnlineMessage GetByReply(string value)
        {
            var model = new OnlineMessage { Reply = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.Reply));
        }

        /// <summary>
        /// 根据属性Reply获取数据实体
        /// </summary>
        public OnlineMessage GetByReply(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.Reply));
        }

        #endregion

                
        #region ReplyTime

        /// <summary>
        /// 根据属性ReplyTime获取数据实体
        /// </summary>
        public OnlineMessage GetByReplyTime(DateTime value)
        {
            var model = new OnlineMessage { ReplyTime = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.ReplyTime));
        }

        /// <summary>
        /// 根据属性ReplyTime获取数据实体
        /// </summary>
        public OnlineMessage GetByReplyTime(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.ReplyTime));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public OnlineMessage GetByVerifier(int value)
        {
            var model = new OnlineMessage { Verifier = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public OnlineMessage GetByVerifier(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.Verifier));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public OnlineMessage GetByVerificationTime(DateTime value)
        {
            var model = new OnlineMessage { VerificationTime = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public OnlineMessage GetByVerificationTime(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.VerificationTime));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public OnlineMessage GetByVerification(int value)
        {
            var model = new OnlineMessage { Verification = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public OnlineMessage GetByVerification(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.Verification));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public OnlineMessage GetByIp(string value)
        {
            var model = new OnlineMessage { Ip = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public OnlineMessage GetByIp(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public OnlineMessage GetByCreateBy(long value)
        {
            var model = new OnlineMessage { CreateBy = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public OnlineMessage GetByCreateBy(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public OnlineMessage GetByCreateTime(DateTime value)
        {
            var model = new OnlineMessage { CreateTime = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public OnlineMessage GetByCreateTime(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public OnlineMessage GetByLastModifyBy(long value)
        {
            var model = new OnlineMessage { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public OnlineMessage GetByLastModifyBy(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public OnlineMessage GetByLastModifyTime(DateTime value)
        {
            var model = new OnlineMessage { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(OnlineMessageField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public OnlineMessage GetByLastModifyTime(OnlineMessage model)
        {
            return GetByWhere(model.GetFilterString(OnlineMessageField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By ReplyUserId

        /// <summary>
        /// 根据属性ReplyUserId获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByReplyUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { ReplyUserId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.ReplyUserId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ReplyUserId获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByReplyUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.ReplyUserId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Reply

        /// <summary>
        /// 根据属性Reply获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByReply(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { Reply = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Reply),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Reply获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByReply(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Reply),sort,operateMode);
        }

        #endregion

                
        #region GetList By ReplyTime

        /// <summary>
        /// 根据属性ReplyTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByReplyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { ReplyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.ReplyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ReplyTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByReplyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.ReplyTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OnlineMessage { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<OnlineMessage> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OnlineMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OnlineMessageField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}