﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class BlacklistDal : ExBaseDal<Blacklist>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public BlacklistDal(): this(Initialization.GetXmlConfig(typeof(Blacklist)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static BlacklistDal CreateDal()
        {
			return new BlacklistDal(Initialization.GetXmlConfig(typeof(Blacklist)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static BlacklistDal()
        {
           ModelName= typeof(Blacklist).Name;
           var item = new Blacklist();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public BlacklistDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Blacklist";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region SubmitterId

        /// <summary>
        /// 根据属性SubmitterId获取数据实体
        /// </summary>
        public Blacklist GetBySubmitterId(int value)
        {
            var model = new Blacklist { SubmitterId = value };
            return GetByWhere(model.GetFilterString(BlacklistField.SubmitterId));
        }

        /// <summary>
        /// 根据属性SubmitterId获取数据实体
        /// </summary>
        public Blacklist GetBySubmitterId(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.SubmitterId));
        }

        #endregion

                
        #region AccusedId

        /// <summary>
        /// 根据属性AccusedId获取数据实体
        /// </summary>
        public Blacklist GetByAccusedId(int value)
        {
            var model = new Blacklist { AccusedId = value };
            return GetByWhere(model.GetFilterString(BlacklistField.AccusedId));
        }

        /// <summary>
        /// 根据属性AccusedId获取数据实体
        /// </summary>
        public Blacklist GetByAccusedId(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.AccusedId));
        }

        #endregion

                
        #region Reason

        /// <summary>
        /// 根据属性Reason获取数据实体
        /// </summary>
        public Blacklist GetByReason(string value)
        {
            var model = new Blacklist { Reason = value };
            return GetByWhere(model.GetFilterString(BlacklistField.Reason));
        }

        /// <summary>
        /// 根据属性Reason获取数据实体
        /// </summary>
        public Blacklist GetByReason(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.Reason));
        }

        #endregion

                
        #region Result

        /// <summary>
        /// 根据属性Result获取数据实体
        /// </summary>
        public Blacklist GetByResult(string value)
        {
            var model = new Blacklist { Result = value };
            return GetByWhere(model.GetFilterString(BlacklistField.Result));
        }

        /// <summary>
        /// 根据属性Result获取数据实体
        /// </summary>
        public Blacklist GetByResult(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.Result));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public Blacklist GetByVerifier(int value)
        {
            var model = new Blacklist { Verifier = value };
            return GetByWhere(model.GetFilterString(BlacklistField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public Blacklist GetByVerifier(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.Verifier));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public Blacklist GetByVerificationTime(DateTime value)
        {
            var model = new Blacklist { VerificationTime = value };
            return GetByWhere(model.GetFilterString(BlacklistField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public Blacklist GetByVerificationTime(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.VerificationTime));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Blacklist GetByIp(string value)
        {
            var model = new Blacklist { Ip = value };
            return GetByWhere(model.GetFilterString(BlacklistField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Blacklist GetByIp(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Blacklist GetByCreateBy(long value)
        {
            var model = new Blacklist { CreateBy = value };
            return GetByWhere(model.GetFilterString(BlacklistField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Blacklist GetByCreateBy(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Blacklist GetByCreateTime(DateTime value)
        {
            var model = new Blacklist { CreateTime = value };
            return GetByWhere(model.GetFilterString(BlacklistField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Blacklist GetByCreateTime(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Blacklist GetByLastModifyBy(long value)
        {
            var model = new Blacklist { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(BlacklistField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Blacklist GetByLastModifyBy(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Blacklist GetByLastModifyTime(DateTime value)
        {
            var model = new Blacklist { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(BlacklistField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Blacklist GetByLastModifyTime(Blacklist model)
        {
            return GetByWhere(model.GetFilterString(BlacklistField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By SubmitterId

        /// <summary>
        /// 根据属性SubmitterId获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListBySubmitterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { SubmitterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.SubmitterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SubmitterId获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListBySubmitterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.SubmitterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AccusedId

        /// <summary>
        /// 根据属性AccusedId获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByAccusedId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { AccusedId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.AccusedId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AccusedId获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByAccusedId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.AccusedId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Reason

        /// <summary>
        /// 根据属性Reason获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { Reason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Reason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Reason获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Reason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Result

        /// <summary>
        /// 根据属性Result获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByResult(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { Result = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Result),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Result获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByResult(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Result),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Blacklist { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Blacklist> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Blacklist model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BlacklistField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}