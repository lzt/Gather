﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class LinkInfoDal : ExBaseDal<LinkInfo>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public LinkInfoDal(): this(Initialization.GetXmlConfig(typeof(LinkInfo)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static LinkInfoDal CreateDal()
        {
			return new LinkInfoDal(Initialization.GetXmlConfig(typeof(LinkInfo)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static LinkInfoDal()
        {
           ModelName= typeof(LinkInfo).Name;
           var item = new LinkInfo();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public LinkInfoDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "LinkInfo";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public LinkInfo GetByName(string value)
        {
            var model = new LinkInfo { Name = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public LinkInfo GetByName(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.Name));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public LinkInfo GetByUrl(string value)
        {
            var model = new LinkInfo { Url = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public LinkInfo GetByUrl(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.Url));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public LinkInfo GetByType(int value)
        {
            var model = new LinkInfo { Type = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public LinkInfo GetByType(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.Type));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public LinkInfo GetByIp(string value)
        {
            var model = new LinkInfo { Ip = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public LinkInfo GetByIp(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public LinkInfo GetByCreateBy(long value)
        {
            var model = new LinkInfo { CreateBy = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public LinkInfo GetByCreateBy(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public LinkInfo GetByCreateTime(DateTime value)
        {
            var model = new LinkInfo { CreateTime = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public LinkInfo GetByCreateTime(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public LinkInfo GetByLastModifyBy(long value)
        {
            var model = new LinkInfo { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public LinkInfo GetByLastModifyBy(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public LinkInfo GetByLastModifyTime(DateTime value)
        {
            var model = new LinkInfo { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(LinkInfoField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public LinkInfo GetByLastModifyTime(LinkInfo model)
        {
            return GetByWhere(model.GetFilterString(LinkInfoField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LinkInfo { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<LinkInfo> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, LinkInfo model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LinkInfoField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}