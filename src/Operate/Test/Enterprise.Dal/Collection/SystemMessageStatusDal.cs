﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SystemMessageStatusDal : ExBaseDal<SystemMessageStatus>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SystemMessageStatusDal(): this(Initialization.GetXmlConfig(typeof(SystemMessageStatus)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SystemMessageStatusDal CreateDal()
        {
			return new SystemMessageStatusDal(Initialization.GetXmlConfig(typeof(SystemMessageStatus)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SystemMessageStatusDal()
        {
           ModelName= typeof(SystemMessageStatus).Name;
           var item = new SystemMessageStatus();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SystemMessageStatusDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SystemMessageStatus";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region MessageId

        /// <summary>
        /// 根据属性MessageId获取数据实体
        /// </summary>
        public SystemMessageStatus GetByMessageId(int value)
        {
            var model = new SystemMessageStatus { MessageId = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.MessageId));
        }

        /// <summary>
        /// 根据属性MessageId获取数据实体
        /// </summary>
        public SystemMessageStatus GetByMessageId(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.MessageId));
        }

        #endregion

                
        #region ReaderUserProfileId

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体
        /// </summary>
        public SystemMessageStatus GetByReaderUserProfileId(int value)
        {
            var model = new SystemMessageStatus { ReaderUserProfileId = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.ReaderUserProfileId));
        }

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体
        /// </summary>
        public SystemMessageStatus GetByReaderUserProfileId(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.ReaderUserProfileId));
        }

        #endregion

                
        #region StatusRead

        /// <summary>
        /// 根据属性StatusRead获取数据实体
        /// </summary>
        public SystemMessageStatus GetByStatusRead(int value)
        {
            var model = new SystemMessageStatus { StatusRead = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.StatusRead));
        }

        /// <summary>
        /// 根据属性StatusRead获取数据实体
        /// </summary>
        public SystemMessageStatus GetByStatusRead(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.StatusRead));
        }

        #endregion

                
        #region StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public SystemMessageStatus GetByStatusDelete(int value)
        {
            var model = new SystemMessageStatus { StatusDelete = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.StatusDelete));
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public SystemMessageStatus GetByStatusDelete(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.StatusDelete));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SystemMessageStatus GetByIp(string value)
        {
            var model = new SystemMessageStatus { Ip = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SystemMessageStatus GetByIp(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SystemMessageStatus GetByCreateBy(long value)
        {
            var model = new SystemMessageStatus { CreateBy = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SystemMessageStatus GetByCreateBy(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SystemMessageStatus GetByCreateTime(DateTime value)
        {
            var model = new SystemMessageStatus { CreateTime = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SystemMessageStatus GetByCreateTime(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SystemMessageStatus GetByLastModifyBy(long value)
        {
            var model = new SystemMessageStatus { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SystemMessageStatus GetByLastModifyBy(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SystemMessageStatus GetByLastModifyTime(DateTime value)
        {
            var model = new SystemMessageStatus { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SystemMessageStatus GetByLastModifyTime(SystemMessageStatus model)
        {
            return GetByWhere(model.GetFilterString(SystemMessageStatusField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By MessageId

        /// <summary>
        /// 根据属性MessageId获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByMessageId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { MessageId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.MessageId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性MessageId获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByMessageId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.MessageId),sort,operateMode);
        }

        #endregion

                
        #region GetList By ReaderUserProfileId

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByReaderUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { ReaderUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.ReaderUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ReaderUserProfileId获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByReaderUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.ReaderUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusRead

        /// <summary>
        /// 根据属性StatusRead获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { StatusRead = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.StatusRead),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusRead获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.StatusRead),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { StatusDelete = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.StatusDelete),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.StatusDelete),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SystemMessageStatus { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SystemMessageStatus> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SystemMessageStatus model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SystemMessageStatusField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}