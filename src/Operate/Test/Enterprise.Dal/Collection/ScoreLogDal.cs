﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class ScoreLogDal : ExBaseDal<ScoreLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ScoreLogDal(): this(Initialization.GetXmlConfig(typeof(ScoreLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ScoreLogDal CreateDal()
        {
			return new ScoreLogDal(Initialization.GetXmlConfig(typeof(ScoreLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ScoreLogDal()
        {
           ModelName= typeof(ScoreLog).Name;
           var item = new ScoreLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ScoreLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ScoreLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public ScoreLog GetByUserProfileId(int value)
        {
            var model = new ScoreLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public ScoreLog GetByUserProfileId(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.UserProfileId));
        }

        #endregion

                
        #region CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public ScoreLog GetByCurrentScore(int value)
        {
            var model = new ScoreLog { CurrentScore = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.CurrentScore));
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体
        /// </summary>
        public ScoreLog GetByCurrentScore(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.CurrentScore));
        }

        #endregion

                
        #region TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public ScoreLog GetByTotalScore(int value)
        {
            var model = new ScoreLog { TotalScore = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.TotalScore));
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体
        /// </summary>
        public ScoreLog GetByTotalScore(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.TotalScore));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public ScoreLog GetByIp(string value)
        {
            var model = new ScoreLog { Ip = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public ScoreLog GetByIp(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public ScoreLog GetByCreateBy(long value)
        {
            var model = new ScoreLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public ScoreLog GetByCreateBy(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ScoreLog GetByCreateTime(DateTime value)
        {
            var model = new ScoreLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ScoreLog GetByCreateTime(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public ScoreLog GetByLastModifyBy(long value)
        {
            var model = new ScoreLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public ScoreLog GetByLastModifyBy(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ScoreLog GetByLastModifyTime(DateTime value)
        {
            var model = new ScoreLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(ScoreLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ScoreLog GetByLastModifyTime(ScoreLog model)
        {
            return GetByWhere(model.GetFilterString(ScoreLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CurrentScore

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { CurrentScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.CurrentScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CurrentScore获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByCurrentScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.CurrentScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalScore

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { TotalScore = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.TotalScore),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalScore获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByTotalScore(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.TotalScore),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ScoreLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ScoreLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ScoreLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ScoreLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}