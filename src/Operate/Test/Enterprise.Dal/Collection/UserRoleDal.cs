﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserRoleDal : ExBaseDal<UserRole>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserRoleDal(): this(Initialization.GetXmlConfig(typeof(UserRole)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserRoleDal CreateDal()
        {
			return new UserRoleDal(Initialization.GetXmlConfig(typeof(UserRole)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserRoleDal()
        {
           ModelName= typeof(UserRole).Name;
           var item = new UserRole();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserRoleDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserRole";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserRole GetByTitle(string value)
        {
            var model = new UserRole { Title = value };
            return GetByWhere(model.GetFilterString(UserRoleField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserRole GetByTitle(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.Title));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserRole GetByStatusEnabled(int value)
        {
            var model = new UserRole { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserRoleField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserRole GetByStatusEnabled(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.StatusEnabled));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserRole GetByRemark(string value)
        {
            var model = new UserRole { Remark = value };
            return GetByWhere(model.GetFilterString(UserRoleField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserRole GetByRemark(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.Remark));
        }

        #endregion

                
        #region CreatedBy

        /// <summary>
        /// 根据属性CreatedBy获取数据实体
        /// </summary>
        public UserRole GetByCreatedBy(string value)
        {
            var model = new UserRole { CreatedBy = value };
            return GetByWhere(model.GetFilterString(UserRoleField.CreatedBy));
        }

        /// <summary>
        /// 根据属性CreatedBy获取数据实体
        /// </summary>
        public UserRole GetByCreatedBy(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.CreatedBy));
        }

        #endregion

                
        #region ModifiedBy

        /// <summary>
        /// 根据属性ModifiedBy获取数据实体
        /// </summary>
        public UserRole GetByModifiedBy(string value)
        {
            var model = new UserRole { ModifiedBy = value };
            return GetByWhere(model.GetFilterString(UserRoleField.ModifiedBy));
        }

        /// <summary>
        /// 根据属性ModifiedBy获取数据实体
        /// </summary>
        public UserRole GetByModifiedBy(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.ModifiedBy));
        }

        #endregion

                
        #region SpecialOption

        /// <summary>
        /// 根据属性SpecialOption获取数据实体
        /// </summary>
        public UserRole GetBySpecialOption(string value)
        {
            var model = new UserRole { SpecialOption = value };
            return GetByWhere(model.GetFilterString(UserRoleField.SpecialOption));
        }

        /// <summary>
        /// 根据属性SpecialOption获取数据实体
        /// </summary>
        public UserRole GetBySpecialOption(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.SpecialOption));
        }

        #endregion

                
        #region RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserRole GetByRestrictIp(int value)
        {
            var model = new UserRole { RestrictIp = value };
            return GetByWhere(model.GetFilterString(UserRoleField.RestrictIp));
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体
        /// </summary>
        public UserRole GetByRestrictIp(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.RestrictIp));
        }

        #endregion

                
        #region IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserRole GetByIpTactics(string value)
        {
            var model = new UserRole { IpTactics = value };
            return GetByWhere(model.GetFilterString(UserRoleField.IpTactics));
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体
        /// </summary>
        public UserRole GetByIpTactics(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.IpTactics));
        }

        #endregion

                
        #region StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserRole GetByStatusMultiLogin(int value)
        {
            var model = new UserRole { StatusMultiLogin = value };
            return GetByWhere(model.GetFilterString(UserRoleField.StatusMultiLogin));
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体
        /// </summary>
        public UserRole GetByStatusMultiLogin(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.StatusMultiLogin));
        }

        #endregion

                
        #region TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserRole GetByTimephasedLogin(int value)
        {
            var model = new UserRole { TimephasedLogin = value };
            return GetByWhere(model.GetFilterString(UserRoleField.TimephasedLogin));
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体
        /// </summary>
        public UserRole GetByTimephasedLogin(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.TimephasedLogin));
        }

        #endregion

                
        #region AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserRole GetByAllowLoginTimeBegin(DateTime value)
        {
            var model = new UserRole { AllowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserRoleField.AllowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体
        /// </summary>
        public UserRole GetByAllowLoginTimeBegin(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.AllowLoginTimeBegin));
        }

        #endregion

                
        #region AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserRole GetByAllowLoginTimeEnd(DateTime value)
        {
            var model = new UserRole { AllowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserRoleField.AllowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体
        /// </summary>
        public UserRole GetByAllowLoginTimeEnd(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.AllowLoginTimeEnd));
        }

        #endregion

                
        #region DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserRole GetByDisallowLoginTimeBegin(DateTime value)
        {
            var model = new UserRole { DisallowLoginTimeBegin = value };
            return GetByWhere(model.GetFilterString(UserRoleField.DisallowLoginTimeBegin));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体
        /// </summary>
        public UserRole GetByDisallowLoginTimeBegin(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.DisallowLoginTimeBegin));
        }

        #endregion

                
        #region DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserRole GetByDisallowLoginTimeEnd(DateTime value)
        {
            var model = new UserRole { DisallowLoginTimeEnd = value };
            return GetByWhere(model.GetFilterString(UserRoleField.DisallowLoginTimeEnd));
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体
        /// </summary>
        public UserRole GetByDisallowLoginTimeEnd(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.DisallowLoginTimeEnd));
        }

        #endregion

                
        #region StatusChanged

        /// <summary>
        /// 根据属性StatusChanged获取数据实体
        /// </summary>
        public UserRole GetByStatusChanged(int value)
        {
            var model = new UserRole { StatusChanged = value };
            return GetByWhere(model.GetFilterString(UserRoleField.StatusChanged));
        }

        /// <summary>
        /// 根据属性StatusChanged获取数据实体
        /// </summary>
        public UserRole GetByStatusChanged(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.StatusChanged));
        }

        #endregion

                
        #region Competence

        /// <summary>
        /// 根据属性Competence获取数据实体
        /// </summary>
        public UserRole GetByCompetence(string value)
        {
            var model = new UserRole { Competence = value };
            return GetByWhere(model.GetFilterString(UserRoleField.Competence));
        }

        /// <summary>
        /// 根据属性Competence获取数据实体
        /// </summary>
        public UserRole GetByCompetence(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.Competence));
        }

        #endregion

                
        #region UrlMarkList

        /// <summary>
        /// 根据属性UrlMarkList获取数据实体
        /// </summary>
        public UserRole GetByUrlMarkList(string value)
        {
            var model = new UserRole { UrlMarkList = value };
            return GetByWhere(model.GetFilterString(UserRoleField.UrlMarkList));
        }

        /// <summary>
        /// 根据属性UrlMarkList获取数据实体
        /// </summary>
        public UserRole GetByUrlMarkList(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.UrlMarkList));
        }

        #endregion

                
        #region Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserRole GetByPurpose(int value)
        {
            var model = new UserRole { Purpose = value };
            return GetByWhere(model.GetFilterString(UserRoleField.Purpose));
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserRole GetByPurpose(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.Purpose));
        }

        #endregion

                
        #region StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public UserRole GetByStatusDelete(long value)
        {
            var model = new UserRole { StatusDelete = value };
            return GetByWhere(model.GetFilterString(UserRoleField.StatusDelete));
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体
        /// </summary>
        public UserRole GetByStatusDelete(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.StatusDelete));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRole GetByIp(string value)
        {
            var model = new UserRole { Ip = value };
            return GetByWhere(model.GetFilterString(UserRoleField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRole GetByIp(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRole GetByCreateBy(long value)
        {
            var model = new UserRole { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserRoleField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRole GetByCreateBy(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRole GetByCreateTime(DateTime value)
        {
            var model = new UserRole { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserRoleField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRole GetByCreateTime(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRole GetByLastModifyBy(long value)
        {
            var model = new UserRole { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserRoleField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRole GetByLastModifyBy(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRole GetByLastModifyTime(DateTime value)
        {
            var model = new UserRole { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserRoleField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRole GetByLastModifyTime(UserRole model)
        {
            return GetByWhere(model.GetFilterString(UserRoleField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreatedBy

        /// <summary>
        /// 根据属性CreatedBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCreatedBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { CreatedBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.CreatedBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreatedBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCreatedBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.CreatedBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By ModifiedBy

        /// <summary>
        /// 根据属性ModifiedBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByModifiedBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { ModifiedBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.ModifiedBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ModifiedBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByModifiedBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.ModifiedBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By SpecialOption

        /// <summary>
        /// 根据属性SpecialOption获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListBySpecialOption(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { SpecialOption = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.SpecialOption),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SpecialOption获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListBySpecialOption(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.SpecialOption),sort,operateMode);
        }

        #endregion

                
        #region GetList By RestrictIp

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { RestrictIp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.RestrictIp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RestrictIp获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByRestrictIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.RestrictIp),sort,operateMode);
        }

        #endregion

                
        #region GetList By IpTactics

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { IpTactics = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.IpTactics),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IpTactics获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByIpTactics(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.IpTactics),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusMultiLogin

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { StatusMultiLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusMultiLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusMultiLogin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusMultiLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusMultiLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By TimephasedLogin

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { TimephasedLogin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.TimephasedLogin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TimephasedLogin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByTimephasedLogin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.TimephasedLogin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeBegin

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { AllowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.AllowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByAllowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.AllowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowLoginTimeEnd

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { AllowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.AllowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByAllowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.AllowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeBegin

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { DisallowLoginTimeBegin = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.DisallowLoginTimeBegin),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeBegin获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByDisallowLoginTimeBegin(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.DisallowLoginTimeBegin),sort,operateMode);
        }

        #endregion

                
        #region GetList By DisallowLoginTimeEnd

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { DisallowLoginTimeEnd = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.DisallowLoginTimeEnd),sort,operateMode);
        }

        /// <summary>
        /// 根据属性DisallowLoginTimeEnd获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByDisallowLoginTimeEnd(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.DisallowLoginTimeEnd),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusChanged

        /// <summary>
        /// 根据属性StatusChanged获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusChanged(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { StatusChanged = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusChanged),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusChanged获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusChanged(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusChanged),sort,operateMode);
        }

        #endregion

                
        #region GetList By Competence

        /// <summary>
        /// 根据属性Competence获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCompetence(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { Competence = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Competence),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Competence获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCompetence(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Competence),sort,operateMode);
        }

        #endregion

                
        #region GetList By UrlMarkList

        /// <summary>
        /// 根据属性UrlMarkList获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByUrlMarkList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { UrlMarkList = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.UrlMarkList),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UrlMarkList获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByUrlMarkList(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.UrlMarkList),sort,operateMode);
        }

        #endregion

                
        #region GetList By Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { Purpose = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Purpose),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Purpose),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusDelete

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { StatusDelete = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusDelete),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusDelete获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.StatusDelete),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRole { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRole> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRole model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}