﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AccessCodeDal : ExBaseDal<AccessCode>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AccessCodeDal(): this(Initialization.GetXmlConfig(typeof(AccessCode)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AccessCodeDal CreateDal()
        {
			return new AccessCodeDal(Initialization.GetXmlConfig(typeof(AccessCode)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AccessCodeDal()
        {
           ModelName= typeof(AccessCode).Name;
           var item = new AccessCode();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AccessCodeDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "AccessCode";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Code

        /// <summary>
        /// 根据属性Code获取数据实体
        /// </summary>
        public AccessCode GetByCode(string value)
        {
            var model = new AccessCode { Code = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.Code));
        }

        /// <summary>
        /// 根据属性Code获取数据实体
        /// </summary>
        public AccessCode GetByCode(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.Code));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public AccessCode GetByUserProfileId(int value)
        {
            var model = new AccessCode { UserProfileId = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public AccessCode GetByUserProfileId(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.UserProfileId));
        }

        #endregion

                
        #region Token

        /// <summary>
        /// 根据属性Token获取数据实体
        /// </summary>
        public AccessCode GetByToken(string value)
        {
            var model = new AccessCode { Token = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.Token));
        }

        /// <summary>
        /// 根据属性Token获取数据实体
        /// </summary>
        public AccessCode GetByToken(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.Token));
        }

        #endregion

                
        #region TokenExpiresTime

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体
        /// </summary>
        public AccessCode GetByTokenExpiresTime(DateTime value)
        {
            var model = new AccessCode { TokenExpiresTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.TokenExpiresTime));
        }

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体
        /// </summary>
        public AccessCode GetByTokenExpiresTime(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.TokenExpiresTime));
        }

        #endregion

                
        #region Effective

        /// <summary>
        /// 根据属性Effective获取数据实体
        /// </summary>
        public AccessCode GetByEffective(int value)
        {
            var model = new AccessCode { Effective = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.Effective));
        }

        /// <summary>
        /// 根据属性Effective获取数据实体
        /// </summary>
        public AccessCode GetByEffective(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.Effective));
        }

        #endregion

                
        #region SiteUserId

        /// <summary>
        /// 根据属性SiteUserId获取数据实体
        /// </summary>
        public AccessCode GetBySiteUserId(long value)
        {
            var model = new AccessCode { SiteUserId = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.SiteUserId));
        }

        /// <summary>
        /// 根据属性SiteUserId获取数据实体
        /// </summary>
        public AccessCode GetBySiteUserId(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.SiteUserId));
        }

        #endregion

                
        #region Scope

        /// <summary>
        /// 根据属性Scope获取数据实体
        /// </summary>
        public AccessCode GetByScope(long value)
        {
            var model = new AccessCode { Scope = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.Scope));
        }

        /// <summary>
        /// 根据属性Scope获取数据实体
        /// </summary>
        public AccessCode GetByScope(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.Scope));
        }

        #endregion

                
        #region Reuse

        /// <summary>
        /// 根据属性Reuse获取数据实体
        /// </summary>
        public AccessCode GetByReuse(long value)
        {
            var model = new AccessCode { Reuse = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.Reuse));
        }

        /// <summary>
        /// 根据属性Reuse获取数据实体
        /// </summary>
        public AccessCode GetByReuse(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.Reuse));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessCode GetByIp(string value)
        {
            var model = new AccessCode { Ip = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessCode GetByIp(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessCode GetByCreateBy(long value)
        {
            var model = new AccessCode { CreateBy = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessCode GetByCreateBy(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessCode GetByCreateTime(DateTime value)
        {
            var model = new AccessCode { CreateTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessCode GetByCreateTime(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessCode GetByLastModifyBy(long value)
        {
            var model = new AccessCode { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AccessCodeField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessCode GetByLastModifyBy(AccessCode model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeField.LastModifyBy));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Code

        /// <summary>
        /// 根据属性Code获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { Code = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Code),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Code获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Code),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Token

        /// <summary>
        /// 根据属性Token获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByToken(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { Token = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Token),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Token获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByToken(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Token),sort,operateMode);
        }

        #endregion

                
        #region GetList By TokenExpiresTime

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByTokenExpiresTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { TokenExpiresTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.TokenExpiresTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByTokenExpiresTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.TokenExpiresTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Effective

        /// <summary>
        /// 根据属性Effective获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByEffective(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { Effective = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Effective),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Effective获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByEffective(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Effective),sort,operateMode);
        }

        #endregion

                
        #region GetList By SiteUserId

        /// <summary>
        /// 根据属性SiteUserId获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListBySiteUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { SiteUserId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.SiteUserId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SiteUserId获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListBySiteUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.SiteUserId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Scope

        /// <summary>
        /// 根据属性Scope获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByScope(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { Scope = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Scope),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Scope获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByScope(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Scope),sort,operateMode);
        }

        #endregion

                
        #region GetList By Reuse

        /// <summary>
        /// 根据属性Reuse获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByReuse(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { Reuse = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Reuse),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Reuse获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByReuse(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Reuse),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCode { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessCode> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCode model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeField.LastModifyBy),sort,operateMode);
        }

        #endregion

        #endregion
    }
}