﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AppVersionDal : ExBaseDal<AppVersion>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AppVersionDal(): this(Initialization.GetXmlConfig(typeof(AppVersion)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AppVersionDal CreateDal()
        {
			return new AppVersionDal(Initialization.GetXmlConfig(typeof(AppVersion)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AppVersionDal()
        {
           ModelName= typeof(AppVersion).Name;
           var item = new AppVersion();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AppVersionDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "AppVersion";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region VersionName

        /// <summary>
        /// 根据属性VersionName获取数据实体
        /// </summary>
        public AppVersion GetByVersionName(string value)
        {
            var model = new AppVersion { VersionName = value };
            return GetByWhere(model.GetFilterString(AppVersionField.VersionName));
        }

        /// <summary>
        /// 根据属性VersionName获取数据实体
        /// </summary>
        public AppVersion GetByVersionName(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.VersionName));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public AppVersion GetByType(int value)
        {
            var model = new AppVersion { Type = value };
            return GetByWhere(model.GetFilterString(AppVersionField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public AppVersion GetByType(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.Type));
        }

        #endregion

                
        #region ForceUpgrade

        /// <summary>
        /// 根据属性ForceUpgrade获取数据实体
        /// </summary>
        public AppVersion GetByForceUpgrade(int value)
        {
            var model = new AppVersion { ForceUpgrade = value };
            return GetByWhere(model.GetFilterString(AppVersionField.ForceUpgrade));
        }

        /// <summary>
        /// 根据属性ForceUpgrade获取数据实体
        /// </summary>
        public AppVersion GetByForceUpgrade(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.ForceUpgrade));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public AppVersion GetByUrl(string value)
        {
            var model = new AppVersion { Url = value };
            return GetByWhere(model.GetFilterString(AppVersionField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public AppVersion GetByUrl(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.Url));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public AppVersion GetByRemark(string value)
        {
            var model = new AppVersion { Remark = value };
            return GetByWhere(model.GetFilterString(AppVersionField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public AppVersion GetByRemark(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.Remark));
        }

        #endregion

                
        #region Illustration

        /// <summary>
        /// 根据属性Illustration获取数据实体
        /// </summary>
        public AppVersion GetByIllustration(string value)
        {
            var model = new AppVersion { Illustration = value };
            return GetByWhere(model.GetFilterString(AppVersionField.Illustration));
        }

        /// <summary>
        /// 根据属性Illustration获取数据实体
        /// </summary>
        public AppVersion GetByIllustration(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.Illustration));
        }

        #endregion

                
        #region VersionNumber

        /// <summary>
        /// 根据属性VersionNumber获取数据实体
        /// </summary>
        public AppVersion GetByVersionNumber(int value)
        {
            var model = new AppVersion { VersionNumber = value };
            return GetByWhere(model.GetFilterString(AppVersionField.VersionNumber));
        }

        /// <summary>
        /// 根据属性VersionNumber获取数据实体
        /// </summary>
        public AppVersion GetByVersionNumber(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.VersionNumber));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AppVersion GetByIp(string value)
        {
            var model = new AppVersion { Ip = value };
            return GetByWhere(model.GetFilterString(AppVersionField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AppVersion GetByIp(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AppVersion GetByCreateBy(long value)
        {
            var model = new AppVersion { CreateBy = value };
            return GetByWhere(model.GetFilterString(AppVersionField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AppVersion GetByCreateBy(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AppVersion GetByCreateTime(DateTime value)
        {
            var model = new AppVersion { CreateTime = value };
            return GetByWhere(model.GetFilterString(AppVersionField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AppVersion GetByCreateTime(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AppVersion GetByLastModifyBy(long value)
        {
            var model = new AppVersion { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AppVersionField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AppVersion GetByLastModifyBy(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AppVersion GetByLastModifyTime(DateTime value)
        {
            var model = new AppVersion { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(AppVersionField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AppVersion GetByLastModifyTime(AppVersion model)
        {
            return GetByWhere(model.GetFilterString(AppVersionField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By VersionName

        /// <summary>
        /// 根据属性VersionName获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByVersionName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { VersionName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.VersionName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VersionName获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByVersionName(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.VersionName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By ForceUpgrade

        /// <summary>
        /// 根据属性ForceUpgrade获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByForceUpgrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { ForceUpgrade = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.ForceUpgrade),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ForceUpgrade获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByForceUpgrade(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.ForceUpgrade),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Illustration

        /// <summary>
        /// 根据属性Illustration获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByIllustration(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { Illustration = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Illustration),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Illustration获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByIllustration(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Illustration),sort,operateMode);
        }

        #endregion

                
        #region GetList By VersionNumber

        /// <summary>
        /// 根据属性VersionNumber获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByVersionNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { VersionNumber = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.VersionNumber),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VersionNumber获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByVersionNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.VersionNumber),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AppVersion { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AppVersion> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AppVersion model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AppVersionField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}