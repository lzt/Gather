﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UnionpayResultDal : ExBaseDal<UnionpayResult>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UnionpayResultDal(): this(Initialization.GetXmlConfig(typeof(UnionpayResult)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UnionpayResultDal CreateDal()
        {
			return new UnionpayResultDal(Initialization.GetXmlConfig(typeof(UnionpayResult)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UnionpayResultDal()
        {
           ModelName= typeof(UnionpayResult).Name;
           var item = new UnionpayResult();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UnionpayResultDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UnionpayResult";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region SerialNumber

        /// <summary>
        /// 根据属性SerialNumber获取数据实体
        /// </summary>
        public UnionpayResult GetBySerialNumber(string value)
        {
            var model = new UnionpayResult { SerialNumber = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.SerialNumber));
        }

        /// <summary>
        /// 根据属性SerialNumber获取数据实体
        /// </summary>
        public UnionpayResult GetBySerialNumber(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.SerialNumber));
        }

        #endregion

                
        #region Status

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public UnionpayResult GetByStatus(int value)
        {
            var model = new UnionpayResult { Status = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.Status));
        }

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public UnionpayResult GetByStatus(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.Status));
        }

        #endregion

                
        #region TxnTime

        /// <summary>
        /// 根据属性TxnTime获取数据实体
        /// </summary>
        public UnionpayResult GetByTxnTime(string value)
        {
            var model = new UnionpayResult { TxnTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.TxnTime));
        }

        /// <summary>
        /// 根据属性TxnTime获取数据实体
        /// </summary>
        public UnionpayResult GetByTxnTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.TxnTime));
        }

        #endregion

                
        #region PaymentTime

        /// <summary>
        /// 根据属性PaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByPaymentTime(string value)
        {
            var model = new UnionpayResult { PaymentTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.PaymentTime));
        }

        /// <summary>
        /// 根据属性PaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByPaymentTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.PaymentTime));
        }

        #endregion

                
        #region CanclePaymentTime

        /// <summary>
        /// 根据属性CanclePaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByCanclePaymentTime(string value)
        {
            var model = new UnionpayResult { CanclePaymentTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.CanclePaymentTime));
        }

        /// <summary>
        /// 根据属性CanclePaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByCanclePaymentTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.CanclePaymentTime));
        }

        #endregion

                
        #region FinishPaymentTime

        /// <summary>
        /// 根据属性FinishPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByFinishPaymentTime(string value)
        {
            var model = new UnionpayResult { FinishPaymentTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.FinishPaymentTime));
        }

        /// <summary>
        /// 根据属性FinishPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByFinishPaymentTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.FinishPaymentTime));
        }

        #endregion

                
        #region IllegalPaymentTime

        /// <summary>
        /// 根据属性IllegalPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByIllegalPaymentTime(string value)
        {
            var model = new UnionpayResult { IllegalPaymentTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.IllegalPaymentTime));
        }

        /// <summary>
        /// 根据属性IllegalPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByIllegalPaymentTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.IllegalPaymentTime));
        }

        #endregion

                
        #region CancelIllegalPaymentTime

        /// <summary>
        /// 根据属性CancelIllegalPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByCancelIllegalPaymentTime(string value)
        {
            var model = new UnionpayResult { CancelIllegalPaymentTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.CancelIllegalPaymentTime));
        }

        /// <summary>
        /// 根据属性CancelIllegalPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByCancelIllegalPaymentTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.CancelIllegalPaymentTime));
        }

        #endregion

                
        #region FinishIllegalPaymentTime

        /// <summary>
        /// 根据属性FinishIllegalPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByFinishIllegalPaymentTime(string value)
        {
            var model = new UnionpayResult { FinishIllegalPaymentTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.FinishIllegalPaymentTime));
        }

        /// <summary>
        /// 根据属性FinishIllegalPaymentTime获取数据实体
        /// </summary>
        public UnionpayResult GetByFinishIllegalPaymentTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.FinishIllegalPaymentTime));
        }

        #endregion

                
        #region PreAuthorizationFinishedType

        /// <summary>
        /// 根据属性PreAuthorizationFinishedType获取数据实体
        /// </summary>
        public UnionpayResult GetByPreAuthorizationFinishedType(int value)
        {
            var model = new UnionpayResult { PreAuthorizationFinishedType = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.PreAuthorizationFinishedType));
        }

        /// <summary>
        /// 根据属性PreAuthorizationFinishedType获取数据实体
        /// </summary>
        public UnionpayResult GetByPreAuthorizationFinishedType(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.PreAuthorizationFinishedType));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UnionpayResult GetByIp(string value)
        {
            var model = new UnionpayResult { Ip = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UnionpayResult GetByIp(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UnionpayResult GetByCreateBy(long value)
        {
            var model = new UnionpayResult { CreateBy = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UnionpayResult GetByCreateBy(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UnionpayResult GetByCreateTime(DateTime value)
        {
            var model = new UnionpayResult { CreateTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UnionpayResult GetByCreateTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UnionpayResult GetByLastModifyBy(long value)
        {
            var model = new UnionpayResult { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UnionpayResult GetByLastModifyBy(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UnionpayResult GetByLastModifyTime(DateTime value)
        {
            var model = new UnionpayResult { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UnionpayResultField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UnionpayResult GetByLastModifyTime(UnionpayResult model)
        {
            return GetByWhere(model.GetFilterString(UnionpayResultField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By SerialNumber

        /// <summary>
        /// 根据属性SerialNumber获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListBySerialNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { SerialNumber = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.SerialNumber),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SerialNumber获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListBySerialNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.SerialNumber),sort,operateMode);
        }

        #endregion

                
        #region GetList By Status

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { Status = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.Status),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.Status),sort,operateMode);
        }

        #endregion

                
        #region GetList By TxnTime

        /// <summary>
        /// 根据属性TxnTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByTxnTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { TxnTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.TxnTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TxnTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByTxnTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.TxnTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By PaymentTime

        /// <summary>
        /// 根据属性PaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { PaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.PaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.PaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By CanclePaymentTime

        /// <summary>
        /// 根据属性CanclePaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCanclePaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { CanclePaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CanclePaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CanclePaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCanclePaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CanclePaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By FinishPaymentTime

        /// <summary>
        /// 根据属性FinishPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByFinishPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { FinishPaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.FinishPaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FinishPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByFinishPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.FinishPaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By IllegalPaymentTime

        /// <summary>
        /// 根据属性IllegalPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByIllegalPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { IllegalPaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.IllegalPaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性IllegalPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByIllegalPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.IllegalPaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By CancelIllegalPaymentTime

        /// <summary>
        /// 根据属性CancelIllegalPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCancelIllegalPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { CancelIllegalPaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CancelIllegalPaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CancelIllegalPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCancelIllegalPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CancelIllegalPaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By FinishIllegalPaymentTime

        /// <summary>
        /// 根据属性FinishIllegalPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByFinishIllegalPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { FinishIllegalPaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.FinishIllegalPaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FinishIllegalPaymentTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByFinishIllegalPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.FinishIllegalPaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By PreAuthorizationFinishedType

        /// <summary>
        /// 根据属性PreAuthorizationFinishedType获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByPreAuthorizationFinishedType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { PreAuthorizationFinishedType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.PreAuthorizationFinishedType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PreAuthorizationFinishedType获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByPreAuthorizationFinishedType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.PreAuthorizationFinishedType),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayResult { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UnionpayResult> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayResult model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayResultField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}