﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class ShuttleDal : ExBaseDal<Shuttle>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ShuttleDal(): this(Initialization.GetXmlConfig(typeof(Shuttle)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ShuttleDal CreateDal()
        {
			return new ShuttleDal(Initialization.GetXmlConfig(typeof(Shuttle)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ShuttleDal()
        {
           ModelName= typeof(Shuttle).Name;
           var item = new Shuttle();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ShuttleDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Shuttle";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Shuttle GetByName(string value)
        {
            var model = new Shuttle { Name = value };
            return GetByWhere(model.GetFilterString(ShuttleField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Shuttle GetByName(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.Name));
        }

        #endregion

                
        #region ParentAreaId

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体
        /// </summary>
        public Shuttle GetByParentAreaId(int value)
        {
            var model = new Shuttle { ParentAreaId = value };
            return GetByWhere(model.GetFilterString(ShuttleField.ParentAreaId));
        }

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体
        /// </summary>
        public Shuttle GetByParentAreaId(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.ParentAreaId));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public Shuttle GetByParentId(int value)
        {
            var model = new Shuttle { ParentId = value };
            return GetByWhere(model.GetFilterString(ShuttleField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public Shuttle GetByParentId(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.ParentId));
        }

        #endregion

                
        #region Lat

        /// <summary>
        /// 根据属性Lat获取数据实体
        /// </summary>
        public Shuttle GetByLat(double value)
        {
            var model = new Shuttle { Lat = value };
            return GetByWhere(model.GetFilterString(ShuttleField.Lat));
        }

        /// <summary>
        /// 根据属性Lat获取数据实体
        /// </summary>
        public Shuttle GetByLat(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.Lat));
        }

        #endregion

                
        #region Lng

        /// <summary>
        /// 根据属性Lng获取数据实体
        /// </summary>
        public Shuttle GetByLng(double value)
        {
            var model = new Shuttle { Lng = value };
            return GetByWhere(model.GetFilterString(ShuttleField.Lng));
        }

        /// <summary>
        /// 根据属性Lng获取数据实体
        /// </summary>
        public Shuttle GetByLng(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.Lng));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Shuttle GetByIp(string value)
        {
            var model = new Shuttle { Ip = value };
            return GetByWhere(model.GetFilterString(ShuttleField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Shuttle GetByIp(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Shuttle GetByCreateBy(long value)
        {
            var model = new Shuttle { CreateBy = value };
            return GetByWhere(model.GetFilterString(ShuttleField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Shuttle GetByCreateBy(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Shuttle GetByCreateTime(DateTime value)
        {
            var model = new Shuttle { CreateTime = value };
            return GetByWhere(model.GetFilterString(ShuttleField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Shuttle GetByCreateTime(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Shuttle GetByLastModifyBy(long value)
        {
            var model = new Shuttle { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(ShuttleField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Shuttle GetByLastModifyBy(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Shuttle GetByLastModifyTime(DateTime value)
        {
            var model = new Shuttle { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(ShuttleField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Shuttle GetByLastModifyTime(Shuttle model)
        {
            return GetByWhere(model.GetFilterString(ShuttleField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentAreaId

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByParentAreaId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { ParentAreaId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.ParentAreaId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByParentAreaId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.ParentAreaId),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Lat

        /// <summary>
        /// 根据属性Lat获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLat(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { Lat = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Lat),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Lat获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLat(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Lat),sort,operateMode);
        }

        #endregion

                
        #region GetList By Lng

        /// <summary>
        /// 根据属性Lng获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLng(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { Lng = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Lng),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Lng获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLng(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Lng),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Shuttle { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Shuttle> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Shuttle model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ShuttleField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}