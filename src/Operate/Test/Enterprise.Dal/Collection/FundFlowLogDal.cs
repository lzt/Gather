﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class FundFlowLogDal : ExBaseDal<FundFlowLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public FundFlowLogDal(): this(Initialization.GetXmlConfig(typeof(FundFlowLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static FundFlowLogDal CreateDal()
        {
			return new FundFlowLogDal(Initialization.GetXmlConfig(typeof(FundFlowLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static FundFlowLogDal()
        {
           ModelName= typeof(FundFlowLog).Name;
           var item = new FundFlowLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public FundFlowLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "FundFlowLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Card

        /// <summary>
        /// 根据属性Card获取数据实体
        /// </summary>
        public FundFlowLog GetByCard(string value)
        {
            var model = new FundFlowLog { Card = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.Card));
        }

        /// <summary>
        /// 根据属性Card获取数据实体
        /// </summary>
        public FundFlowLog GetByCard(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.Card));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public FundFlowLog GetByUserProfileId(int value)
        {
            var model = new FundFlowLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public FundFlowLog GetByUserProfileId(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.UserProfileId));
        }

        #endregion

                
        #region Money

        /// <summary>
        /// 根据属性Money获取数据实体
        /// </summary>
        public FundFlowLog GetByMoney(int value)
        {
            var model = new FundFlowLog { Money = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.Money));
        }

        /// <summary>
        /// 根据属性Money获取数据实体
        /// </summary>
        public FundFlowLog GetByMoney(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.Money));
        }

        #endregion

                
        #region OperateTime

        /// <summary>
        /// 根据属性OperateTime获取数据实体
        /// </summary>
        public FundFlowLog GetByOperateTime(DateTime value)
        {
            var model = new FundFlowLog { OperateTime = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.OperateTime));
        }

        /// <summary>
        /// 根据属性OperateTime获取数据实体
        /// </summary>
        public FundFlowLog GetByOperateTime(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.OperateTime));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public FundFlowLog GetByType(int value)
        {
            var model = new FundFlowLog { Type = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public FundFlowLog GetByType(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.Type));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public FundFlowLog GetByRemark(string value)
        {
            var model = new FundFlowLog { Remark = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public FundFlowLog GetByRemark(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.Remark));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public FundFlowLog GetByTitle(string value)
        {
            var model = new FundFlowLog { Title = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public FundFlowLog GetByTitle(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.Title));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public FundFlowLog GetByIp(string value)
        {
            var model = new FundFlowLog { Ip = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public FundFlowLog GetByIp(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public FundFlowLog GetByCreateBy(long value)
        {
            var model = new FundFlowLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public FundFlowLog GetByCreateBy(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public FundFlowLog GetByCreateTime(DateTime value)
        {
            var model = new FundFlowLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public FundFlowLog GetByCreateTime(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public FundFlowLog GetByLastModifyBy(long value)
        {
            var model = new FundFlowLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public FundFlowLog GetByLastModifyBy(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public FundFlowLog GetByLastModifyTime(DateTime value)
        {
            var model = new FundFlowLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(FundFlowLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public FundFlowLog GetByLastModifyTime(FundFlowLog model)
        {
            return GetByWhere(model.GetFilterString(FundFlowLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Card

        /// <summary>
        /// 根据属性Card获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { Card = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Card),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Card获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByCard(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Card),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Money

        /// <summary>
        /// 根据属性Money获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByMoney(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { Money = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Money),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Money获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByMoney(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Money),sort,operateMode);
        }

        #endregion

                
        #region GetList By OperateTime

        /// <summary>
        /// 根据属性OperateTime获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByOperateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { OperateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.OperateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OperateTime获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByOperateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.OperateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new FundFlowLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<FundFlowLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, FundFlowLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FundFlowLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}