﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AccessUrlDal : ExBaseDal<AccessUrl>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AccessUrlDal(): this(Initialization.GetXmlConfig(typeof(AccessUrl)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AccessUrlDal CreateDal()
        {
			return new AccessUrlDal(Initialization.GetXmlConfig(typeof(AccessUrl)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AccessUrlDal()
        {
           ModelName= typeof(AccessUrl).Name;
           var item = new AccessUrl();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AccessUrlDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "AccessUrl";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public AccessUrl GetByName(string value)
        {
            var model = new AccessUrl { Name = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public AccessUrl GetByName(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.Name));
        }

        #endregion

                
        #region GuidTag

        /// <summary>
        /// 根据属性GuidTag获取数据实体
        /// </summary>
        public AccessUrl GetByGuidTag(string value)
        {
            var model = new AccessUrl { GuidTag = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.GuidTag));
        }

        /// <summary>
        /// 根据属性GuidTag获取数据实体
        /// </summary>
        public AccessUrl GetByGuidTag(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.GuidTag));
        }

        #endregion

                
        #region UrlPath

        /// <summary>
        /// 根据属性UrlPath获取数据实体
        /// </summary>
        public AccessUrl GetByUrlPath(string value)
        {
            var model = new AccessUrl { UrlPath = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.UrlPath));
        }

        /// <summary>
        /// 根据属性UrlPath获取数据实体
        /// </summary>
        public AccessUrl GetByUrlPath(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.UrlPath));
        }

        #endregion

                
        #region Expand

        /// <summary>
        /// 根据属性Expand获取数据实体
        /// </summary>
        public AccessUrl GetByExpand(string value)
        {
            var model = new AccessUrl { Expand = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.Expand));
        }

        /// <summary>
        /// 根据属性Expand获取数据实体
        /// </summary>
        public AccessUrl GetByExpand(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.Expand));
        }

        #endregion

                
        #region GroupId

        /// <summary>
        /// 根据属性GroupId获取数据实体
        /// </summary>
        public AccessUrl GetByGroupId(int value)
        {
            var model = new AccessUrl { GroupId = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.GroupId));
        }

        /// <summary>
        /// 根据属性GroupId获取数据实体
        /// </summary>
        public AccessUrl GetByGroupId(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.GroupId));
        }

        #endregion

                
        #region GroupName

        /// <summary>
        /// 根据属性GroupName获取数据实体
        /// </summary>
        public AccessUrl GetByGroupName(string value)
        {
            var model = new AccessUrl { GroupName = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.GroupName));
        }

        /// <summary>
        /// 根据属性GroupName获取数据实体
        /// </summary>
        public AccessUrl GetByGroupName(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.GroupName));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public AccessUrl GetByRemark(string value)
        {
            var model = new AccessUrl { Remark = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public AccessUrl GetByRemark(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.Remark));
        }

        #endregion

                
        #region AllowAnonymous

        /// <summary>
        /// 根据属性AllowAnonymous获取数据实体
        /// </summary>
        public AccessUrl GetByAllowAnonymous(int value)
        {
            var model = new AccessUrl { AllowAnonymous = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.AllowAnonymous));
        }

        /// <summary>
        /// 根据属性AllowAnonymous获取数据实体
        /// </summary>
        public AccessUrl GetByAllowAnonymous(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.AllowAnonymous));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public AccessUrl GetByStatusEnabled(int value)
        {
            var model = new AccessUrl { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public AccessUrl GetByStatusEnabled(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.StatusEnabled));
        }

        #endregion

                
        #region Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public AccessUrl GetByPurpose(int value)
        {
            var model = new AccessUrl { Purpose = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.Purpose));
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public AccessUrl GetByPurpose(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.Purpose));
        }

        #endregion

                
        #region StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public AccessUrl GetByStatusInvalid(int value)
        {
            var model = new AccessUrl { StatusInvalid = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.StatusInvalid));
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体
        /// </summary>
        public AccessUrl GetByStatusInvalid(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.StatusInvalid));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessUrl GetByIp(string value)
        {
            var model = new AccessUrl { Ip = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessUrl GetByIp(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessUrl GetByCreateBy(long value)
        {
            var model = new AccessUrl { CreateBy = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessUrl GetByCreateBy(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessUrl GetByCreateTime(DateTime value)
        {
            var model = new AccessUrl { CreateTime = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessUrl GetByCreateTime(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessUrl GetByLastModifyBy(long value)
        {
            var model = new AccessUrl { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessUrl GetByLastModifyBy(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AccessUrl GetByLastModifyTime(DateTime value)
        {
            var model = new AccessUrl { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(AccessUrlField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AccessUrl GetByLastModifyTime(AccessUrl model)
        {
            return GetByWhere(model.GetFilterString(AccessUrlField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By GuidTag

        /// <summary>
        /// 根据属性GuidTag获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByGuidTag(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { GuidTag = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.GuidTag),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GuidTag获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByGuidTag(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.GuidTag),sort,operateMode);
        }

        #endregion

                
        #region GetList By UrlPath

        /// <summary>
        /// 根据属性UrlPath获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByUrlPath(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { UrlPath = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.UrlPath),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UrlPath获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByUrlPath(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.UrlPath),sort,operateMode);
        }

        #endregion

                
        #region GetList By Expand

        /// <summary>
        /// 根据属性Expand获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByExpand(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { Expand = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Expand),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Expand获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByExpand(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Expand),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupId

        /// <summary>
        /// 根据属性GroupId获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { GroupId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.GroupId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupId获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.GroupId),sort,operateMode);
        }

        #endregion

                
        #region GetList By GroupName

        /// <summary>
        /// 根据属性GroupName获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByGroupName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { GroupName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.GroupName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性GroupName获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByGroupName(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.GroupName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By AllowAnonymous

        /// <summary>
        /// 根据属性AllowAnonymous获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByAllowAnonymous(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { AllowAnonymous = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.AllowAnonymous),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AllowAnonymous获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByAllowAnonymous(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.AllowAnonymous),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { Purpose = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Purpose),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Purpose),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusInvalid

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { StatusInvalid = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.StatusInvalid),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusInvalid获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByStatusInvalid(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.StatusInvalid),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessUrl { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AccessUrl> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessUrl model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessUrlField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}