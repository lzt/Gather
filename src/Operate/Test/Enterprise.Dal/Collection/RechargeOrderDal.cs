﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class RechargeOrderDal : ExBaseDal<RechargeOrder>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public RechargeOrderDal(): this(Initialization.GetXmlConfig(typeof(RechargeOrder)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static RechargeOrderDal CreateDal()
        {
			return new RechargeOrderDal(Initialization.GetXmlConfig(typeof(RechargeOrder)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static RechargeOrderDal()
        {
           ModelName= typeof(RechargeOrder).Name;
           var item = new RechargeOrder();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public RechargeOrderDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "RechargeOrder";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public RechargeOrder GetByUserProfileId(int value)
        {
            var model = new RechargeOrder { UserProfileId = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public RechargeOrder GetByUserProfileId(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.UserProfileId));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public RechargeOrder GetByRemark(string value)
        {
            var model = new RechargeOrder { Remark = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public RechargeOrder GetByRemark(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.Remark));
        }

        #endregion

                
        #region Money

        /// <summary>
        /// 根据属性Money获取数据实体
        /// </summary>
        public RechargeOrder GetByMoney(int value)
        {
            var model = new RechargeOrder { Money = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.Money));
        }

        /// <summary>
        /// 根据属性Money获取数据实体
        /// </summary>
        public RechargeOrder GetByMoney(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.Money));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public RechargeOrder GetByTitle(string value)
        {
            var model = new RechargeOrder { Title = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public RechargeOrder GetByTitle(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.Title));
        }

        #endregion

                
        #region SerialNumber

        /// <summary>
        /// 根据属性SerialNumber获取数据实体
        /// </summary>
        public RechargeOrder GetBySerialNumber(string value)
        {
            var model = new RechargeOrder { SerialNumber = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.SerialNumber));
        }

        /// <summary>
        /// 根据属性SerialNumber获取数据实体
        /// </summary>
        public RechargeOrder GetBySerialNumber(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.SerialNumber));
        }

        #endregion

                
        #region TransactionNumber

        /// <summary>
        /// 根据属性TransactionNumber获取数据实体
        /// </summary>
        public RechargeOrder GetByTransactionNumber(string value)
        {
            var model = new RechargeOrder { TransactionNumber = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.TransactionNumber));
        }

        /// <summary>
        /// 根据属性TransactionNumber获取数据实体
        /// </summary>
        public RechargeOrder GetByTransactionNumber(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.TransactionNumber));
        }

        #endregion

                
        #region PaymentTypeId

        /// <summary>
        /// 根据属性PaymentTypeId获取数据实体
        /// </summary>
        public RechargeOrder GetByPaymentTypeId(int value)
        {
            var model = new RechargeOrder { PaymentTypeId = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.PaymentTypeId));
        }

        /// <summary>
        /// 根据属性PaymentTypeId获取数据实体
        /// </summary>
        public RechargeOrder GetByPaymentTypeId(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.PaymentTypeId));
        }

        #endregion

                
        #region PaymentTime

        /// <summary>
        /// 根据属性PaymentTime获取数据实体
        /// </summary>
        public RechargeOrder GetByPaymentTime(DateTime value)
        {
            var model = new RechargeOrder { PaymentTime = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.PaymentTime));
        }

        /// <summary>
        /// 根据属性PaymentTime获取数据实体
        /// </summary>
        public RechargeOrder GetByPaymentTime(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.PaymentTime));
        }

        #endregion

                
        #region Payment

        /// <summary>
        /// 根据属性Payment获取数据实体
        /// </summary>
        public RechargeOrder GetByPayment(int value)
        {
            var model = new RechargeOrder { Payment = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.Payment));
        }

        /// <summary>
        /// 根据属性Payment获取数据实体
        /// </summary>
        public RechargeOrder GetByPayment(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.Payment));
        }

        #endregion

                
        #region ExpireTime

        /// <summary>
        /// 根据属性ExpireTime获取数据实体
        /// </summary>
        public RechargeOrder GetByExpireTime(DateTime value)
        {
            var model = new RechargeOrder { ExpireTime = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.ExpireTime));
        }

        /// <summary>
        /// 根据属性ExpireTime获取数据实体
        /// </summary>
        public RechargeOrder GetByExpireTime(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.ExpireTime));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public RechargeOrder GetByIp(string value)
        {
            var model = new RechargeOrder { Ip = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public RechargeOrder GetByIp(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public RechargeOrder GetByCreateBy(long value)
        {
            var model = new RechargeOrder { CreateBy = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public RechargeOrder GetByCreateBy(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public RechargeOrder GetByCreateTime(DateTime value)
        {
            var model = new RechargeOrder { CreateTime = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public RechargeOrder GetByCreateTime(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public RechargeOrder GetByLastModifyBy(long value)
        {
            var model = new RechargeOrder { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public RechargeOrder GetByLastModifyBy(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public RechargeOrder GetByLastModifyTime(DateTime value)
        {
            var model = new RechargeOrder { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(RechargeOrderField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public RechargeOrder GetByLastModifyTime(RechargeOrder model)
        {
            return GetByWhere(model.GetFilterString(RechargeOrderField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By Money

        /// <summary>
        /// 根据属性Money获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByMoney(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { Money = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Money),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Money获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByMoney(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Money),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By SerialNumber

        /// <summary>
        /// 根据属性SerialNumber获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListBySerialNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { SerialNumber = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.SerialNumber),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SerialNumber获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListBySerialNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.SerialNumber),sort,operateMode);
        }

        #endregion

                
        #region GetList By TransactionNumber

        /// <summary>
        /// 根据属性TransactionNumber获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByTransactionNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { TransactionNumber = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.TransactionNumber),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TransactionNumber获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByTransactionNumber(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.TransactionNumber),sort,operateMode);
        }

        #endregion

                
        #region GetList By PaymentTypeId

        /// <summary>
        /// 根据属性PaymentTypeId获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByPaymentTypeId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { PaymentTypeId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.PaymentTypeId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PaymentTypeId获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByPaymentTypeId(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.PaymentTypeId),sort,operateMode);
        }

        #endregion

                
        #region GetList By PaymentTime

        /// <summary>
        /// 根据属性PaymentTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { PaymentTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.PaymentTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PaymentTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByPaymentTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.PaymentTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Payment

        /// <summary>
        /// 根据属性Payment获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByPayment(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { Payment = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Payment),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Payment获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByPayment(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Payment),sort,operateMode);
        }

        #endregion

                
        #region GetList By ExpireTime

        /// <summary>
        /// 根据属性ExpireTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByExpireTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { ExpireTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.ExpireTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ExpireTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByExpireTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.ExpireTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new RechargeOrder { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<RechargeOrder> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, RechargeOrder model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(RechargeOrderField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}