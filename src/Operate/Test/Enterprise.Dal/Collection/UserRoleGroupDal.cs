﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserRoleGroupDal : ExBaseDal<UserRoleGroup>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserRoleGroupDal(): this(Initialization.GetXmlConfig(typeof(UserRoleGroup)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserRoleGroupDal CreateDal()
        {
			return new UserRoleGroupDal(Initialization.GetXmlConfig(typeof(UserRoleGroup)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserRoleGroupDal()
        {
           ModelName= typeof(UserRoleGroup).Name;
           var item = new UserRoleGroup();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserRoleGroupDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserRoleGroup";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserRoleGroup GetByTitle(string value)
        {
            var model = new UserRoleGroup { Title = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserRoleGroup GetByTitle(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Title));
        }

        #endregion

                
        #region RoleList

        /// <summary>
        /// 根据属性RoleList获取数据实体
        /// </summary>
        public UserRoleGroup GetByRoleList(string value)
        {
            var model = new UserRoleGroup { RoleList = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.RoleList));
        }

        /// <summary>
        /// 根据属性RoleList获取数据实体
        /// </summary>
        public UserRoleGroup GetByRoleList(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.RoleList));
        }

        #endregion

                
        #region StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserRoleGroup GetByStatusEnabled(int value)
        {
            var model = new UserRoleGroup { StatusEnabled = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.StatusEnabled));
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体
        /// </summary>
        public UserRoleGroup GetByStatusEnabled(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.StatusEnabled));
        }

        #endregion

                
        #region Remark

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserRoleGroup GetByRemark(string value)
        {
            var model = new UserRoleGroup { Remark = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Remark));
        }

        /// <summary>
        /// 根据属性Remark获取数据实体
        /// </summary>
        public UserRoleGroup GetByRemark(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Remark));
        }

        #endregion

                
        #region ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserRoleGroup GetByParentId(long value)
        {
            var model = new UserRoleGroup { ParentId = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.ParentId));
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体
        /// </summary>
        public UserRoleGroup GetByParentId(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.ParentId));
        }

        #endregion

                
        #region Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserRoleGroup GetByPurpose(int value)
        {
            var model = new UserRoleGroup { Purpose = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Purpose));
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体
        /// </summary>
        public UserRoleGroup GetByPurpose(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Purpose));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRoleGroup GetByIp(string value)
        {
            var model = new UserRoleGroup { Ip = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRoleGroup GetByIp(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRoleGroup GetByCreateBy(long value)
        {
            var model = new UserRoleGroup { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRoleGroup GetByCreateBy(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRoleGroup GetByCreateTime(DateTime value)
        {
            var model = new UserRoleGroup { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRoleGroup GetByCreateTime(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRoleGroup GetByLastModifyBy(long value)
        {
            var model = new UserRoleGroup { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRoleGroup GetByLastModifyBy(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRoleGroup GetByLastModifyTime(DateTime value)
        {
            var model = new UserRoleGroup { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserRoleGroupField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRoleGroup GetByLastModifyTime(UserRoleGroup model)
        {
            return GetByWhere(model.GetFilterString(UserRoleGroupField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By RoleList

        /// <summary>
        /// 根据属性RoleList获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByRoleList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { RoleList = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.RoleList),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RoleList获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByRoleList(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.RoleList),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusEnabled

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { StatusEnabled = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.StatusEnabled),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusEnabled获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByStatusEnabled(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.StatusEnabled),sort,operateMode);
        }

        #endregion

                
        #region GetList By Remark

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { Remark = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Remark),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Remark获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByRemark(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Remark),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentId

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { ParentId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.ParentId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentId获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByParentId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.ParentId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Purpose

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { Purpose = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Purpose),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Purpose获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByPurpose(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Purpose),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleGroup { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRoleGroup> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleGroup model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleGroupField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}