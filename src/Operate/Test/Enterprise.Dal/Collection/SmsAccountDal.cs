﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SmsAccountDal : ExBaseDal<SmsAccount>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SmsAccountDal(): this(Initialization.GetXmlConfig(typeof(SmsAccount)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SmsAccountDal CreateDal()
        {
			return new SmsAccountDal(Initialization.GetXmlConfig(typeof(SmsAccount)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SmsAccountDal()
        {
           ModelName= typeof(SmsAccount).Name;
           var item = new SmsAccount();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SmsAccountDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SmsAccount";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public SmsAccount GetByUserProfileId(int value)
        {
            var model = new SmsAccount { UserProfileId = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public SmsAccount GetByUserProfileId(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.UserProfileId));
        }

        #endregion

                
        #region AvailableCount

        /// <summary>
        /// 根据属性AvailableCount获取数据实体
        /// </summary>
        public SmsAccount GetByAvailableCount(int value)
        {
            var model = new SmsAccount { AvailableCount = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.AvailableCount));
        }

        /// <summary>
        /// 根据属性AvailableCount获取数据实体
        /// </summary>
        public SmsAccount GetByAvailableCount(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.AvailableCount));
        }

        #endregion

                
        #region TotalCount

        /// <summary>
        /// 根据属性TotalCount获取数据实体
        /// </summary>
        public SmsAccount GetByTotalCount(int value)
        {
            var model = new SmsAccount { TotalCount = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.TotalCount));
        }

        /// <summary>
        /// 根据属性TotalCount获取数据实体
        /// </summary>
        public SmsAccount GetByTotalCount(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.TotalCount));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SmsAccount GetByIp(string value)
        {
            var model = new SmsAccount { Ip = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SmsAccount GetByIp(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SmsAccount GetByCreateBy(long value)
        {
            var model = new SmsAccount { CreateBy = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SmsAccount GetByCreateBy(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SmsAccount GetByCreateTime(DateTime value)
        {
            var model = new SmsAccount { CreateTime = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SmsAccount GetByCreateTime(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SmsAccount GetByLastModifyBy(long value)
        {
            var model = new SmsAccount { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SmsAccount GetByLastModifyBy(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SmsAccount GetByLastModifyTime(DateTime value)
        {
            var model = new SmsAccount { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(SmsAccountField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SmsAccount GetByLastModifyTime(SmsAccount model)
        {
            return GetByWhere(model.GetFilterString(SmsAccountField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvailableCount

        /// <summary>
        /// 根据属性AvailableCount获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByAvailableCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { AvailableCount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.AvailableCount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvailableCount获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByAvailableCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.AvailableCount),sort,operateMode);
        }

        #endregion

                
        #region GetList By TotalCount

        /// <summary>
        /// 根据属性TotalCount获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByTotalCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { TotalCount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.TotalCount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TotalCount获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByTotalCount(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.TotalCount),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsAccount { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SmsAccount> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsAccount model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsAccountField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}