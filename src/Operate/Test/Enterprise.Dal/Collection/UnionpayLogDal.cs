﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UnionpayLogDal : ExBaseDal<UnionpayLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UnionpayLogDal(): this(Initialization.GetXmlConfig(typeof(UnionpayLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UnionpayLogDal CreateDal()
        {
			return new UnionpayLogDal(Initialization.GetXmlConfig(typeof(UnionpayLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UnionpayLogDal()
        {
           ModelName= typeof(UnionpayLog).Name;
           var item = new UnionpayLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UnionpayLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UnionpayLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UnionpayLog GetByUserProfileId(int value)
        {
            var model = new UnionpayLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UnionpayLog GetByUserProfileId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.UserProfileId));
        }

        #endregion

                
        #region QueryId

        /// <summary>
        /// 根据属性QueryId获取数据实体
        /// </summary>
        public UnionpayLog GetByQueryId(string value)
        {
            var model = new UnionpayLog { QueryId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.QueryId));
        }

        /// <summary>
        /// 根据属性QueryId获取数据实体
        /// </summary>
        public UnionpayLog GetByQueryId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.QueryId));
        }

        #endregion

                
        #region RespCode

        /// <summary>
        /// 根据属性RespCode获取数据实体
        /// </summary>
        public UnionpayLog GetByRespCode(string value)
        {
            var model = new UnionpayLog { RespCode = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.RespCode));
        }

        /// <summary>
        /// 根据属性RespCode获取数据实体
        /// </summary>
        public UnionpayLog GetByRespCode(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.RespCode));
        }

        #endregion

                
        #region RespMsg

        /// <summary>
        /// 根据属性RespMsg获取数据实体
        /// </summary>
        public UnionpayLog GetByRespMsg(string value)
        {
            var model = new UnionpayLog { RespMsg = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.RespMsg));
        }

        /// <summary>
        /// 根据属性RespMsg获取数据实体
        /// </summary>
        public UnionpayLog GetByRespMsg(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.RespMsg));
        }

        #endregion

                
        #region TraceNo

        /// <summary>
        /// 根据属性TraceNo获取数据实体
        /// </summary>
        public UnionpayLog GetByTraceNo(string value)
        {
            var model = new UnionpayLog { TraceNo = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.TraceNo));
        }

        /// <summary>
        /// 根据属性TraceNo获取数据实体
        /// </summary>
        public UnionpayLog GetByTraceNo(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.TraceNo));
        }

        #endregion

                
        #region PreAuthId

        /// <summary>
        /// 根据属性PreAuthId获取数据实体
        /// </summary>
        public UnionpayLog GetByPreAuthId(string value)
        {
            var model = new UnionpayLog { PreAuthId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.PreAuthId));
        }

        /// <summary>
        /// 根据属性PreAuthId获取数据实体
        /// </summary>
        public UnionpayLog GetByPreAuthId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.PreAuthId));
        }

        #endregion

                
        #region CurrencyCode

        /// <summary>
        /// 根据属性CurrencyCode获取数据实体
        /// </summary>
        public UnionpayLog GetByCurrencyCode(string value)
        {
            var model = new UnionpayLog { CurrencyCode = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.CurrencyCode));
        }

        /// <summary>
        /// 根据属性CurrencyCode获取数据实体
        /// </summary>
        public UnionpayLog GetByCurrencyCode(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.CurrencyCode));
        }

        #endregion

                
        #region OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public UnionpayLog GetByOrderId(string value)
        {
            var model = new UnionpayLog { OrderId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.OrderId));
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public UnionpayLog GetByOrderId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.OrderId));
        }

        #endregion

                
        #region TraceTime

        /// <summary>
        /// 根据属性TraceTime获取数据实体
        /// </summary>
        public UnionpayLog GetByTraceTime(string value)
        {
            var model = new UnionpayLog { TraceTime = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.TraceTime));
        }

        /// <summary>
        /// 根据属性TraceTime获取数据实体
        /// </summary>
        public UnionpayLog GetByTraceTime(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.TraceTime));
        }

        #endregion

                
        #region SignMethod

        /// <summary>
        /// 根据属性SignMethod获取数据实体
        /// </summary>
        public UnionpayLog GetBySignMethod(string value)
        {
            var model = new UnionpayLog { SignMethod = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.SignMethod));
        }

        /// <summary>
        /// 根据属性SignMethod获取数据实体
        /// </summary>
        public UnionpayLog GetBySignMethod(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.SignMethod));
        }

        #endregion

                
        #region BizType

        /// <summary>
        /// 根据属性BizType获取数据实体
        /// </summary>
        public UnionpayLog GetByBizType(string value)
        {
            var model = new UnionpayLog { BizType = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.BizType));
        }

        /// <summary>
        /// 根据属性BizType获取数据实体
        /// </summary>
        public UnionpayLog GetByBizType(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.BizType));
        }

        #endregion

                
        #region Signature

        /// <summary>
        /// 根据属性Signature获取数据实体
        /// </summary>
        public UnionpayLog GetBySignature(string value)
        {
            var model = new UnionpayLog { Signature = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.Signature));
        }

        /// <summary>
        /// 根据属性Signature获取数据实体
        /// </summary>
        public UnionpayLog GetBySignature(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.Signature));
        }

        #endregion

                
        #region TxnAmt

        /// <summary>
        /// 根据属性TxnAmt获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnAmt(string value)
        {
            var model = new UnionpayLog { TxnAmt = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnAmt));
        }

        /// <summary>
        /// 根据属性TxnAmt获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnAmt(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnAmt));
        }

        #endregion

                
        #region Version

        /// <summary>
        /// 根据属性Version获取数据实体
        /// </summary>
        public UnionpayLog GetByVersion(string value)
        {
            var model = new UnionpayLog { Version = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.Version));
        }

        /// <summary>
        /// 根据属性Version获取数据实体
        /// </summary>
        public UnionpayLog GetByVersion(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.Version));
        }

        #endregion

                
        #region SettleDate

        /// <summary>
        /// 根据属性SettleDate获取数据实体
        /// </summary>
        public UnionpayLog GetBySettleDate(string value)
        {
            var model = new UnionpayLog { SettleDate = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.SettleDate));
        }

        /// <summary>
        /// 根据属性SettleDate获取数据实体
        /// </summary>
        public UnionpayLog GetBySettleDate(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.SettleDate));
        }

        #endregion

                
        #region SettleAmt

        /// <summary>
        /// 根据属性SettleAmt获取数据实体
        /// </summary>
        public UnionpayLog GetBySettleAmt(string value)
        {
            var model = new UnionpayLog { SettleAmt = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.SettleAmt));
        }

        /// <summary>
        /// 根据属性SettleAmt获取数据实体
        /// </summary>
        public UnionpayLog GetBySettleAmt(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.SettleAmt));
        }

        #endregion

                
        #region TxnSubType

        /// <summary>
        /// 根据属性TxnSubType获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnSubType(string value)
        {
            var model = new UnionpayLog { TxnSubType = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnSubType));
        }

        /// <summary>
        /// 根据属性TxnSubType获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnSubType(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnSubType));
        }

        #endregion

                
        #region Encoding

        /// <summary>
        /// 根据属性Encoding获取数据实体
        /// </summary>
        public UnionpayLog GetByEncoding(string value)
        {
            var model = new UnionpayLog { Encoding = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.Encoding));
        }

        /// <summary>
        /// 根据属性Encoding获取数据实体
        /// </summary>
        public UnionpayLog GetByEncoding(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.Encoding));
        }

        #endregion

                
        #region SettleCurrencyCode

        /// <summary>
        /// 根据属性SettleCurrencyCode获取数据实体
        /// </summary>
        public UnionpayLog GetBySettleCurrencyCode(string value)
        {
            var model = new UnionpayLog { SettleCurrencyCode = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.SettleCurrencyCode));
        }

        /// <summary>
        /// 根据属性SettleCurrencyCode获取数据实体
        /// </summary>
        public UnionpayLog GetBySettleCurrencyCode(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.SettleCurrencyCode));
        }

        #endregion

                
        #region AccessType

        /// <summary>
        /// 根据属性AccessType获取数据实体
        /// </summary>
        public UnionpayLog GetByAccessType(string value)
        {
            var model = new UnionpayLog { AccessType = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.AccessType));
        }

        /// <summary>
        /// 根据属性AccessType获取数据实体
        /// </summary>
        public UnionpayLog GetByAccessType(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.AccessType));
        }

        #endregion

                
        #region TxnType

        /// <summary>
        /// 根据属性TxnType获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnType(string value)
        {
            var model = new UnionpayLog { TxnType = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnType));
        }

        /// <summary>
        /// 根据属性TxnType获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnType(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnType));
        }

        #endregion

                
        #region CertId

        /// <summary>
        /// 根据属性CertId获取数据实体
        /// </summary>
        public UnionpayLog GetByCertId(string value)
        {
            var model = new UnionpayLog { CertId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.CertId));
        }

        /// <summary>
        /// 根据属性CertId获取数据实体
        /// </summary>
        public UnionpayLog GetByCertId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.CertId));
        }

        #endregion

                
        #region TxnTime

        /// <summary>
        /// 根据属性TxnTime获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnTime(string value)
        {
            var model = new UnionpayLog { TxnTime = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnTime));
        }

        /// <summary>
        /// 根据属性TxnTime获取数据实体
        /// </summary>
        public UnionpayLog GetByTxnTime(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.TxnTime));
        }

        #endregion

                
        #region MerId

        /// <summary>
        /// 根据属性MerId获取数据实体
        /// </summary>
        public UnionpayLog GetByMerId(string value)
        {
            var model = new UnionpayLog { MerId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.MerId));
        }

        /// <summary>
        /// 根据属性MerId获取数据实体
        /// </summary>
        public UnionpayLog GetByMerId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.MerId));
        }

        #endregion

                
        #region OrigQryId

        /// <summary>
        /// 根据属性OrigQryId获取数据实体
        /// </summary>
        public UnionpayLog GetByOrigQryId(string value)
        {
            var model = new UnionpayLog { OrigQryId = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.OrigQryId));
        }

        /// <summary>
        /// 根据属性OrigQryId获取数据实体
        /// </summary>
        public UnionpayLog GetByOrigQryId(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.OrigQryId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UnionpayLog GetByIp(string value)
        {
            var model = new UnionpayLog { Ip = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UnionpayLog GetByIp(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UnionpayLog GetByCreateBy(long value)
        {
            var model = new UnionpayLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UnionpayLog GetByCreateBy(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UnionpayLog GetByCreateTime(DateTime value)
        {
            var model = new UnionpayLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UnionpayLog GetByCreateTime(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UnionpayLog GetByLastModifyBy(long value)
        {
            var model = new UnionpayLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UnionpayLog GetByLastModifyBy(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UnionpayLog GetByLastModifyTime(DateTime value)
        {
            var model = new UnionpayLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UnionpayLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UnionpayLog GetByLastModifyTime(UnionpayLog model)
        {
            return GetByWhere(model.GetFilterString(UnionpayLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By QueryId

        /// <summary>
        /// 根据属性QueryId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByQueryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { QueryId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.QueryId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性QueryId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByQueryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.QueryId),sort,operateMode);
        }

        #endregion

                
        #region GetList By RespCode

        /// <summary>
        /// 根据属性RespCode获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByRespCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { RespCode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.RespCode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RespCode获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByRespCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.RespCode),sort,operateMode);
        }

        #endregion

                
        #region GetList By RespMsg

        /// <summary>
        /// 根据属性RespMsg获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByRespMsg(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { RespMsg = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.RespMsg),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RespMsg获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByRespMsg(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.RespMsg),sort,operateMode);
        }

        #endregion

                
        #region GetList By TraceNo

        /// <summary>
        /// 根据属性TraceNo获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTraceNo(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { TraceNo = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TraceNo),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TraceNo获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTraceNo(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TraceNo),sort,operateMode);
        }

        #endregion

                
        #region GetList By PreAuthId

        /// <summary>
        /// 根据属性PreAuthId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByPreAuthId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { PreAuthId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.PreAuthId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PreAuthId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByPreAuthId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.PreAuthId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CurrencyCode

        /// <summary>
        /// 根据属性CurrencyCode获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCurrencyCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { CurrencyCode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CurrencyCode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CurrencyCode获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCurrencyCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CurrencyCode),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { OrderId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.OrderId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.OrderId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TraceTime

        /// <summary>
        /// 根据属性TraceTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTraceTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { TraceTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TraceTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TraceTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTraceTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TraceTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By SignMethod

        /// <summary>
        /// 根据属性SignMethod获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySignMethod(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { SignMethod = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SignMethod),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SignMethod获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySignMethod(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SignMethod),sort,operateMode);
        }

        #endregion

                
        #region GetList By BizType

        /// <summary>
        /// 根据属性BizType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByBizType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { BizType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.BizType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BizType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByBizType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.BizType),sort,operateMode);
        }

        #endregion

                
        #region GetList By Signature

        /// <summary>
        /// 根据属性Signature获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySignature(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { Signature = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Signature),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Signature获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySignature(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Signature),sort,operateMode);
        }

        #endregion

                
        #region GetList By TxnAmt

        /// <summary>
        /// 根据属性TxnAmt获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnAmt(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { TxnAmt = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnAmt),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TxnAmt获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnAmt(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnAmt),sort,operateMode);
        }

        #endregion

                
        #region GetList By Version

        /// <summary>
        /// 根据属性Version获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByVersion(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { Version = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Version),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Version获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByVersion(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Version),sort,operateMode);
        }

        #endregion

                
        #region GetList By SettleDate

        /// <summary>
        /// 根据属性SettleDate获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySettleDate(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { SettleDate = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SettleDate),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SettleDate获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySettleDate(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SettleDate),sort,operateMode);
        }

        #endregion

                
        #region GetList By SettleAmt

        /// <summary>
        /// 根据属性SettleAmt获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySettleAmt(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { SettleAmt = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SettleAmt),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SettleAmt获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySettleAmt(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SettleAmt),sort,operateMode);
        }

        #endregion

                
        #region GetList By TxnSubType

        /// <summary>
        /// 根据属性TxnSubType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnSubType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { TxnSubType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnSubType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TxnSubType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnSubType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnSubType),sort,operateMode);
        }

        #endregion

                
        #region GetList By Encoding

        /// <summary>
        /// 根据属性Encoding获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByEncoding(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { Encoding = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Encoding),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Encoding获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByEncoding(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Encoding),sort,operateMode);
        }

        #endregion

                
        #region GetList By SettleCurrencyCode

        /// <summary>
        /// 根据属性SettleCurrencyCode获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySettleCurrencyCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { SettleCurrencyCode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SettleCurrencyCode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SettleCurrencyCode获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListBySettleCurrencyCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.SettleCurrencyCode),sort,operateMode);
        }

        #endregion

                
        #region GetList By AccessType

        /// <summary>
        /// 根据属性AccessType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByAccessType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { AccessType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.AccessType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AccessType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByAccessType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.AccessType),sort,operateMode);
        }

        #endregion

                
        #region GetList By TxnType

        /// <summary>
        /// 根据属性TxnType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnType(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { TxnType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TxnType获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnType),sort,operateMode);
        }

        #endregion

                
        #region GetList By CertId

        /// <summary>
        /// 根据属性CertId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCertId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { CertId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CertId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CertId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCertId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CertId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TxnTime

        /// <summary>
        /// 根据属性TxnTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { TxnTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TxnTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByTxnTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.TxnTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By MerId

        /// <summary>
        /// 根据属性MerId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByMerId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { MerId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.MerId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性MerId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByMerId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.MerId),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrigQryId

        /// <summary>
        /// 根据属性OrigQryId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByOrigQryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { OrigQryId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.OrigQryId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrigQryId获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByOrigQryId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.OrigQryId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UnionpayLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UnionpayLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UnionpayLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UnionpayLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}