﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserMessageDal : ExBaseDal<UserMessage>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserMessageDal(): this(Initialization.GetXmlConfig(typeof(UserMessage)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserMessageDal CreateDal()
        {
			return new UserMessageDal(Initialization.GetXmlConfig(typeof(UserMessage)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserMessageDal()
        {
           ModelName= typeof(UserMessage).Name;
           var item = new UserMessage();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserMessageDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserMessage";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public UserMessage GetBySourceUserProfileId(int value)
        {
            var model = new UserMessage { SourceUserProfileId = value };
            return GetByWhere(model.GetFilterString(UserMessageField.SourceUserProfileId));
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public UserMessage GetBySourceUserProfileId(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.SourceUserProfileId));
        }

        #endregion

                
        #region TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public UserMessage GetByTargetUserProfileId(int value)
        {
            var model = new UserMessage { TargetUserProfileId = value };
            return GetByWhere(model.GetFilterString(UserMessageField.TargetUserProfileId));
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public UserMessage GetByTargetUserProfileId(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.TargetUserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public UserMessage GetByContent(string value)
        {
            var model = new UserMessage { Content = value };
            return GetByWhere(model.GetFilterString(UserMessageField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public UserMessage GetByContent(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.Content));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public UserMessage GetByType(int value)
        {
            var model = new UserMessage { Type = value };
            return GetByWhere(model.GetFilterString(UserMessageField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public UserMessage GetByType(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.Type));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserMessage GetByTitle(string value)
        {
            var model = new UserMessage { Title = value };
            return GetByWhere(model.GetFilterString(UserMessageField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public UserMessage GetByTitle(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.Title));
        }

        #endregion

                
        #region TargetStatusRead

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体
        /// </summary>
        public UserMessage GetByTargetStatusRead(int value)
        {
            var model = new UserMessage { TargetStatusRead = value };
            return GetByWhere(model.GetFilterString(UserMessageField.TargetStatusRead));
        }

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体
        /// </summary>
        public UserMessage GetByTargetStatusRead(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.TargetStatusRead));
        }

        #endregion

                
        #region TargetStatusDelete

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体
        /// </summary>
        public UserMessage GetByTargetStatusDelete(int value)
        {
            var model = new UserMessage { TargetStatusDelete = value };
            return GetByWhere(model.GetFilterString(UserMessageField.TargetStatusDelete));
        }

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体
        /// </summary>
        public UserMessage GetByTargetStatusDelete(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.TargetStatusDelete));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserMessage GetByIp(string value)
        {
            var model = new UserMessage { Ip = value };
            return GetByWhere(model.GetFilterString(UserMessageField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserMessage GetByIp(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserMessage GetByCreateBy(long value)
        {
            var model = new UserMessage { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserMessageField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserMessage GetByCreateBy(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserMessage GetByCreateTime(DateTime value)
        {
            var model = new UserMessage { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserMessageField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserMessage GetByCreateTime(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserMessage GetByLastModifyBy(long value)
        {
            var model = new UserMessage { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserMessageField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserMessage GetByLastModifyBy(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserMessage GetByLastModifyTime(DateTime value)
        {
            var model = new UserMessage { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserMessageField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserMessage GetByLastModifyTime(UserMessage model)
        {
            return GetByWhere(model.GetFilterString(UserMessageField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { SourceUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.SourceUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.SourceUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { TargetUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.TargetUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.TargetUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetStatusRead

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTargetStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { TargetStatusRead = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.TargetStatusRead),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetStatusRead获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTargetStatusRead(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.TargetStatusRead),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetStatusDelete

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTargetStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { TargetStatusDelete = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.TargetStatusDelete),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetStatusDelete获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByTargetStatusDelete(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.TargetStatusDelete),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserMessage { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserMessage> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserMessage model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserMessageField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}