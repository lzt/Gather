﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AddressDal : ExBaseDal<Address>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AddressDal(): this(Initialization.GetXmlConfig(typeof(Address)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AddressDal CreateDal()
        {
			return new AddressDal(Initialization.GetXmlConfig(typeof(Address)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AddressDal()
        {
           ModelName= typeof(Address).Name;
           var item = new Address();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AddressDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Address";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public Address GetByUserProfileId(int value)
        {
            var model = new Address { UserProfileId = value };
            return GetByWhere(model.GetFilterString(AddressField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public Address GetByUserProfileId(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.UserProfileId));
        }

        #endregion

                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Address GetByName(string value)
        {
            var model = new Address { Name = value };
            return GetByWhere(model.GetFilterString(AddressField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Address GetByName(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.Name));
        }

        #endregion

                
        #region Status

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public Address GetByStatus(int value)
        {
            var model = new Address { Status = value };
            return GetByWhere(model.GetFilterString(AddressField.Status));
        }

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public Address GetByStatus(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.Status));
        }

        #endregion

                
        #region ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public Address GetByProvinceId(int value)
        {
            var model = new Address { ProvinceId = value };
            return GetByWhere(model.GetFilterString(AddressField.ProvinceId));
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public Address GetByProvinceId(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.ProvinceId));
        }

        #endregion

                
        #region CityId

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public Address GetByCityId(int value)
        {
            var model = new Address { CityId = value };
            return GetByWhere(model.GetFilterString(AddressField.CityId));
        }

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public Address GetByCityId(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.CityId));
        }

        #endregion

                
        #region TownId

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public Address GetByTownId(int value)
        {
            var model = new Address { TownId = value };
            return GetByWhere(model.GetFilterString(AddressField.TownId));
        }

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public Address GetByTownId(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.TownId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Address GetByIp(string value)
        {
            var model = new Address { Ip = value };
            return GetByWhere(model.GetFilterString(AddressField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Address GetByIp(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Address GetByCreateBy(long value)
        {
            var model = new Address { CreateBy = value };
            return GetByWhere(model.GetFilterString(AddressField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Address GetByCreateBy(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Address GetByCreateTime(DateTime value)
        {
            var model = new Address { CreateTime = value };
            return GetByWhere(model.GetFilterString(AddressField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Address GetByCreateTime(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Address GetByLastModifyBy(long value)
        {
            var model = new Address { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AddressField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Address GetByLastModifyBy(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Address GetByLastModifyTime(DateTime value)
        {
            var model = new Address { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(AddressField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Address GetByLastModifyTime(Address model)
        {
            return GetByWhere(model.GetFilterString(AddressField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Address> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Address> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By Status

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<Address> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { Status = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.Status),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<Address> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.Status),sort,operateMode);
        }

        #endregion

                
        #region GetList By ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { ProvinceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.ProvinceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.ProvinceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CityId

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { CityId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.CityId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.CityId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TownId

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { TownId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.TownId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<Address> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.TownId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Address> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Address> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Address> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Address> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Address> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Address> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Address> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Address> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Address> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Address { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Address> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Address model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AddressField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}