﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class LogonLogDal : ExBaseDal<LogonLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public LogonLogDal(): this(Initialization.GetXmlConfig(typeof(LogonLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static LogonLogDal CreateDal()
        {
			return new LogonLogDal(Initialization.GetXmlConfig(typeof(LogonLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static LogonLogDal()
        {
           ModelName= typeof(LogonLog).Name;
           var item = new LogonLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public LogonLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "LogonLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public LogonLog GetByUserProfileId(int value)
        {
            var model = new LogonLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(LogonLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public LogonLog GetByUserProfileId(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.UserProfileId));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public LogonLog GetByUrl(string value)
        {
            var model = new LogonLog { Url = value };
            return GetByWhere(model.GetFilterString(LogonLogField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public LogonLog GetByUrl(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.Url));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public LogonLog GetByIp(string value)
        {
            var model = new LogonLog { Ip = value };
            return GetByWhere(model.GetFilterString(LogonLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public LogonLog GetByIp(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public LogonLog GetByCreateBy(long value)
        {
            var model = new LogonLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(LogonLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public LogonLog GetByCreateBy(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public LogonLog GetByCreateTime(DateTime value)
        {
            var model = new LogonLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(LogonLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public LogonLog GetByCreateTime(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public LogonLog GetByLastModifyBy(long value)
        {
            var model = new LogonLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(LogonLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public LogonLog GetByLastModifyBy(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public LogonLog GetByLastModifyTime(DateTime value)
        {
            var model = new LogonLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(LogonLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public LogonLog GetByLastModifyTime(LogonLog model)
        {
            return GetByWhere(model.GetFilterString(LogonLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new LogonLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<LogonLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, LogonLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(LogonLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}