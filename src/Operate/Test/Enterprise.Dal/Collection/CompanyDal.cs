﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class CompanyDal : ExBaseDal<Company>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CompanyDal(): this(Initialization.GetXmlConfig(typeof(Company)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CompanyDal CreateDal()
        {
			return new CompanyDal(Initialization.GetXmlConfig(typeof(Company)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CompanyDal()
        {
           ModelName= typeof(Company).Name;
           var item = new Company();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CompanyDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Company";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Company GetByName(string value)
        {
            var model = new Company { Name = value };
            return GetByWhere(model.GetFilterString(CompanyField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Company GetByName(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.Name));
        }

        #endregion

                
        #region BusinessLicense

        /// <summary>
        /// 根据属性BusinessLicense获取数据实体
        /// </summary>
        public Company GetByBusinessLicense(string value)
        {
            var model = new Company { BusinessLicense = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicense));
        }

        /// <summary>
        /// 根据属性BusinessLicense获取数据实体
        /// </summary>
        public Company GetByBusinessLicense(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicense));
        }

        #endregion

                
        #region BusinessLicenseImage

        /// <summary>
        /// 根据属性BusinessLicenseImage获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseImage(string value)
        {
            var model = new Company { BusinessLicenseImage = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseImage));
        }

        /// <summary>
        /// 根据属性BusinessLicenseImage获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseImage(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseImage));
        }

        #endregion

                
        #region BusinessLicenseVerification

        /// <summary>
        /// 根据属性BusinessLicenseVerification获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseVerification(int value)
        {
            var model = new Company { BusinessLicenseVerification = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseVerification));
        }

        /// <summary>
        /// 根据属性BusinessLicenseVerification获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseVerification(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseVerification));
        }

        #endregion

                
        #region BusinessLicenseVerifier

        /// <summary>
        /// 根据属性BusinessLicenseVerifier获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseVerifier(int value)
        {
            var model = new Company { BusinessLicenseVerifier = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseVerifier));
        }

        /// <summary>
        /// 根据属性BusinessLicenseVerifier获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseVerifier(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseVerifier));
        }

        #endregion

                
        #region BusinessLicenseVerificationTime

        /// <summary>
        /// 根据属性BusinessLicenseVerificationTime获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseVerificationTime(DateTime value)
        {
            var model = new Company { BusinessLicenseVerificationTime = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseVerificationTime));
        }

        /// <summary>
        /// 根据属性BusinessLicenseVerificationTime获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseVerificationTime(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseVerificationTime));
        }

        #endregion

                
        #region BusinessLicenseDenyAuditReason

        /// <summary>
        /// 根据属性BusinessLicenseDenyAuditReason获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseDenyAuditReason(string value)
        {
            var model = new Company { BusinessLicenseDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseDenyAuditReason));
        }

        /// <summary>
        /// 根据属性BusinessLicenseDenyAuditReason获取数据实体
        /// </summary>
        public Company GetByBusinessLicenseDenyAuditReason(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessLicenseDenyAuditReason));
        }

        #endregion

                
        #region OrganizationCodeCertificate

        /// <summary>
        /// 根据属性OrganizationCodeCertificate获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificate(string value)
        {
            var model = new Company { OrganizationCodeCertificate = value };
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificate));
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificate获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificate(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificate));
        }

        #endregion

                
        #region OrganizationCodeCertificateImage

        /// <summary>
        /// 根据属性OrganizationCodeCertificateImage获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateImage(string value)
        {
            var model = new Company { OrganizationCodeCertificateImage = value };
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateImage));
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateImage获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateImage(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateImage));
        }

        #endregion

                
        #region OrganizationCodeCertificateVerification

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerification获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateVerification(int value)
        {
            var model = new Company { OrganizationCodeCertificateVerification = value };
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateVerification));
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerification获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateVerification(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateVerification));
        }

        #endregion

                
        #region OrganizationCodeCertificateVerifier

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerifier获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateVerifier(int value)
        {
            var model = new Company { OrganizationCodeCertificateVerifier = value };
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateVerifier));
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerifier获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateVerifier(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateVerifier));
        }

        #endregion

                
        #region OrganizationCodeCertificateVerificationTime

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerificationTime获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateVerificationTime(DateTime value)
        {
            var model = new Company { OrganizationCodeCertificateVerificationTime = value };
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateVerificationTime));
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerificationTime获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateVerificationTime(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateVerificationTime));
        }

        #endregion

                
        #region OrganizationCodeCertificateDenyAuditReason

        /// <summary>
        /// 根据属性OrganizationCodeCertificateDenyAuditReason获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateDenyAuditReason(string value)
        {
            var model = new Company { OrganizationCodeCertificateDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateDenyAuditReason));
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateDenyAuditReason获取数据实体
        /// </summary>
        public Company GetByOrganizationCodeCertificateDenyAuditReason(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.OrganizationCodeCertificateDenyAuditReason));
        }

        #endregion

                
        #region Corporation

        /// <summary>
        /// 根据属性Corporation获取数据实体
        /// </summary>
        public Company GetByCorporation(string value)
        {
            var model = new Company { Corporation = value };
            return GetByWhere(model.GetFilterString(CompanyField.Corporation));
        }

        /// <summary>
        /// 根据属性Corporation获取数据实体
        /// </summary>
        public Company GetByCorporation(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.Corporation));
        }

        #endregion

                
        #region BusinessAccount

        /// <summary>
        /// 根据属性BusinessAccount获取数据实体
        /// </summary>
        public Company GetByBusinessAccount(string value)
        {
            var model = new Company { BusinessAccount = value };
            return GetByWhere(model.GetFilterString(CompanyField.BusinessAccount));
        }

        /// <summary>
        /// 根据属性BusinessAccount获取数据实体
        /// </summary>
        public Company GetByBusinessAccount(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.BusinessAccount));
        }

        #endregion

                
        #region Address

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public Company GetByAddress(string value)
        {
            var model = new Company { Address = value };
            return GetByWhere(model.GetFilterString(CompanyField.Address));
        }

        /// <summary>
        /// 根据属性Address获取数据实体
        /// </summary>
        public Company GetByAddress(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.Address));
        }

        #endregion

                
        #region ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public Company GetByProvinceId(int value)
        {
            var model = new Company { ProvinceId = value };
            return GetByWhere(model.GetFilterString(CompanyField.ProvinceId));
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体
        /// </summary>
        public Company GetByProvinceId(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.ProvinceId));
        }

        #endregion

                
        #region CityId

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public Company GetByCityId(int value)
        {
            var model = new Company { CityId = value };
            return GetByWhere(model.GetFilterString(CompanyField.CityId));
        }

        /// <summary>
        /// 根据属性CityId获取数据实体
        /// </summary>
        public Company GetByCityId(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.CityId));
        }

        #endregion

                
        #region TownId

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public Company GetByTownId(int value)
        {
            var model = new Company { TownId = value };
            return GetByWhere(model.GetFilterString(CompanyField.TownId));
        }

        /// <summary>
        /// 根据属性TownId获取数据实体
        /// </summary>
        public Company GetByTownId(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.TownId));
        }

        #endregion

                
        #region Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public Company GetByAvatar(string value)
        {
            var model = new Company { Avatar = value };
            return GetByWhere(model.GetFilterString(CompanyField.Avatar));
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体
        /// </summary>
        public Company GetByAvatar(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.Avatar));
        }

        #endregion

                
        #region AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public Company GetByAvatarVerification(int value)
        {
            var model = new Company { AvatarVerification = value };
            return GetByWhere(model.GetFilterString(CompanyField.AvatarVerification));
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体
        /// </summary>
        public Company GetByAvatarVerification(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.AvatarVerification));
        }

        #endregion

                
        #region AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public Company GetByAvatarVerifier(int value)
        {
            var model = new Company { AvatarVerifier = value };
            return GetByWhere(model.GetFilterString(CompanyField.AvatarVerifier));
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体
        /// </summary>
        public Company GetByAvatarVerifier(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.AvatarVerifier));
        }

        #endregion

                
        #region AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public Company GetByAvatarVerificationTime(DateTime value)
        {
            var model = new Company { AvatarVerificationTime = value };
            return GetByWhere(model.GetFilterString(CompanyField.AvatarVerificationTime));
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体
        /// </summary>
        public Company GetByAvatarVerificationTime(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.AvatarVerificationTime));
        }

        #endregion

                
        #region AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public Company GetByAvatarDenyAuditReason(string value)
        {
            var model = new Company { AvatarDenyAuditReason = value };
            return GetByWhere(model.GetFilterString(CompanyField.AvatarDenyAuditReason));
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体
        /// </summary>
        public Company GetByAvatarDenyAuditReason(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.AvatarDenyAuditReason));
        }

        #endregion

                
        #region RegistrationTime

        /// <summary>
        /// 根据属性RegistrationTime获取数据实体
        /// </summary>
        public Company GetByRegistrationTime(DateTime value)
        {
            var model = new Company { RegistrationTime = value };
            return GetByWhere(model.GetFilterString(CompanyField.RegistrationTime));
        }

        /// <summary>
        /// 根据属性RegistrationTime获取数据实体
        /// </summary>
        public Company GetByRegistrationTime(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.RegistrationTime));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Company GetByIp(string value)
        {
            var model = new Company { Ip = value };
            return GetByWhere(model.GetFilterString(CompanyField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Company GetByIp(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Company GetByCreateBy(long value)
        {
            var model = new Company { CreateBy = value };
            return GetByWhere(model.GetFilterString(CompanyField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Company GetByCreateBy(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Company GetByCreateTime(DateTime value)
        {
            var model = new Company { CreateTime = value };
            return GetByWhere(model.GetFilterString(CompanyField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Company GetByCreateTime(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Company GetByLastModifyBy(long value)
        {
            var model = new Company { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(CompanyField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Company GetByLastModifyBy(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Company GetByLastModifyTime(DateTime value)
        {
            var model = new Company { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(CompanyField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Company GetByLastModifyTime(Company model)
        {
            return GetByWhere(model.GetFilterString(CompanyField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Company> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Company> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessLicense

        /// <summary>
        /// 根据属性BusinessLicense获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessLicense = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicense),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessLicense获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicense(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicense),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessLicenseImage

        /// <summary>
        /// 根据属性BusinessLicenseImage获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessLicenseImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessLicenseImage获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessLicenseVerification

        /// <summary>
        /// 根据属性BusinessLicenseVerification获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessLicenseVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessLicenseVerification获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessLicenseVerifier

        /// <summary>
        /// 根据属性BusinessLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessLicenseVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessLicenseVerifier获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessLicenseVerificationTime

        /// <summary>
        /// 根据属性BusinessLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessLicenseVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessLicenseVerificationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessLicenseDenyAuditReason

        /// <summary>
        /// 根据属性BusinessLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessLicenseDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessLicenseDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessLicenseDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessLicenseDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrganizationCodeCertificate

        /// <summary>
        /// 根据属性OrganizationCodeCertificate获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificate(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { OrganizationCodeCertificate = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificate),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificate获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificate(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificate),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrganizationCodeCertificateImage

        /// <summary>
        /// 根据属性OrganizationCodeCertificateImage获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { OrganizationCodeCertificateImage = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateImage),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateImage获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateImage),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrganizationCodeCertificateVerification

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerification获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { OrganizationCodeCertificateVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerification获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrganizationCodeCertificateVerifier

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerifier获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { OrganizationCodeCertificateVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerifier获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrganizationCodeCertificateVerificationTime

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerificationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { OrganizationCodeCertificateVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateVerificationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrganizationCodeCertificateDenyAuditReason

        /// <summary>
        /// 根据属性OrganizationCodeCertificateDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { OrganizationCodeCertificateDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrganizationCodeCertificateDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<Company> GetListByOrganizationCodeCertificateDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.OrganizationCodeCertificateDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By Corporation

        /// <summary>
        /// 根据属性Corporation获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCorporation(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { Corporation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Corporation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Corporation获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCorporation(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Corporation),sort,operateMode);
        }

        #endregion

                
        #region GetList By BusinessAccount

        /// <summary>
        /// 根据属性BusinessAccount获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessAccount(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { BusinessAccount = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessAccount),sort,operateMode);
        }

        /// <summary>
        /// 根据属性BusinessAccount获取数据实体列表
        /// </summary>
        public IList<Company> GetListByBusinessAccount(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.BusinessAccount),sort,operateMode);
        }

        #endregion

                
        #region GetList By Address

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { Address = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Address),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Address获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAddress(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Address),sort,operateMode);
        }

        #endregion

                
        #region GetList By ProvinceId

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<Company> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { ProvinceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.ProvinceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ProvinceId获取数据实体列表
        /// </summary>
        public IList<Company> GetListByProvinceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.ProvinceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CityId

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { CityId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.CityId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CityId获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCityId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.CityId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TownId

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<Company> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { TownId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.TownId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TownId获取数据实体列表
        /// </summary>
        public IList<Company> GetListByTownId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.TownId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Avatar

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { Avatar = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Avatar),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Avatar获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatar(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Avatar),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerification

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { AvatarVerification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarVerification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerification获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarVerification),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerifier

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { AvatarVerifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarVerifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerifier获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarVerifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarVerificationTime

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { AvatarVerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarVerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarVerificationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarVerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By AvatarDenyAuditReason

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { AvatarDenyAuditReason = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarDenyAuditReason),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AvatarDenyAuditReason获取数据实体列表
        /// </summary>
        public IList<Company> GetListByAvatarDenyAuditReason(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.AvatarDenyAuditReason),sort,operateMode);
        }

        #endregion

                
        #region GetList By RegistrationTime

        /// <summary>
        /// 根据属性RegistrationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByRegistrationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { RegistrationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.RegistrationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RegistrationTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByRegistrationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.RegistrationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Company> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Company> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Company> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Company> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Company { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Company> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Company model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CompanyField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}