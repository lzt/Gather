﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserProfileCompanyRelationDal : ExBaseDal<UserProfileCompanyRelation>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserProfileCompanyRelationDal(): this(Initialization.GetXmlConfig(typeof(UserProfileCompanyRelation)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserProfileCompanyRelationDal CreateDal()
        {
			return new UserProfileCompanyRelationDal(Initialization.GetXmlConfig(typeof(UserProfileCompanyRelation)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserProfileCompanyRelationDal()
        {
           ModelName= typeof(UserProfileCompanyRelation).Name;
           var item = new UserProfileCompanyRelation();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserProfileCompanyRelationDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserProfileCompanyRelation";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByUserProfileId(int value)
        {
            var model = new UserProfileCompanyRelation { UserProfileId = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByUserProfileId(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.UserProfileId));
        }

        #endregion

                
        #region CompanyId

        /// <summary>
        /// 根据属性CompanyId获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByCompanyId(int value)
        {
            var model = new UserProfileCompanyRelation { CompanyId = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.CompanyId));
        }

        /// <summary>
        /// 根据属性CompanyId获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByCompanyId(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.CompanyId));
        }

        #endregion

                
        #region Position

        /// <summary>
        /// 根据属性Position获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByPosition(string value)
        {
            var model = new UserProfileCompanyRelation { Position = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.Position));
        }

        /// <summary>
        /// 根据属性Position获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByPosition(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.Position));
        }

        #endregion

                
        #region StatusIsGovernor

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByStatusIsGovernor(int value)
        {
            var model = new UserProfileCompanyRelation { StatusIsGovernor = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.StatusIsGovernor));
        }

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByStatusIsGovernor(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.StatusIsGovernor));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByIp(string value)
        {
            var model = new UserProfileCompanyRelation { Ip = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByIp(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByCreateBy(long value)
        {
            var model = new UserProfileCompanyRelation { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByCreateBy(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByCreateTime(DateTime value)
        {
            var model = new UserProfileCompanyRelation { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByCreateTime(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByLastModifyBy(long value)
        {
            var model = new UserProfileCompanyRelation { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByLastModifyBy(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByLastModifyTime(DateTime value)
        {
            var model = new UserProfileCompanyRelation { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfileCompanyRelation GetByLastModifyTime(UserProfileCompanyRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileCompanyRelationField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By CompanyId

        /// <summary>
        /// 根据属性CompanyId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByCompanyId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { CompanyId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.CompanyId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CompanyId获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByCompanyId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.CompanyId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Position

        /// <summary>
        /// 根据属性Position获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByPosition(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { Position = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.Position),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Position获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByPosition(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.Position),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusIsGovernor

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByStatusIsGovernor(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { StatusIsGovernor = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.StatusIsGovernor),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusIsGovernor获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByStatusIsGovernor(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.StatusIsGovernor),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileCompanyRelation { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfileCompanyRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileCompanyRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileCompanyRelationField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}