﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AccessCodeHistoryDal : ExBaseDal<AccessCodeHistory>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AccessCodeHistoryDal(): this(Initialization.GetXmlConfig(typeof(AccessCodeHistory)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AccessCodeHistoryDal CreateDal()
        {
			return new AccessCodeHistoryDal(Initialization.GetXmlConfig(typeof(AccessCodeHistory)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AccessCodeHistoryDal()
        {
           ModelName= typeof(AccessCodeHistory).Name;
           var item = new AccessCodeHistory();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AccessCodeHistoryDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "AccessCodeHistory";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Code

        /// <summary>
        /// 根据属性Code获取数据实体
        /// </summary>
        public AccessCodeHistory GetByCode(string value)
        {
            var model = new AccessCodeHistory { Code = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Code));
        }

        /// <summary>
        /// 根据属性Code获取数据实体
        /// </summary>
        public AccessCodeHistory GetByCode(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Code));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public AccessCodeHistory GetByUserProfileId(int value)
        {
            var model = new AccessCodeHistory { UserProfileId = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public AccessCodeHistory GetByUserProfileId(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.UserProfileId));
        }

        #endregion

                
        #region Token

        /// <summary>
        /// 根据属性Token获取数据实体
        /// </summary>
        public AccessCodeHistory GetByToken(string value)
        {
            var model = new AccessCodeHistory { Token = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Token));
        }

        /// <summary>
        /// 根据属性Token获取数据实体
        /// </summary>
        public AccessCodeHistory GetByToken(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Token));
        }

        #endregion

                
        #region TokenExpiresTime

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体
        /// </summary>
        public AccessCodeHistory GetByTokenExpiresTime(DateTime value)
        {
            var model = new AccessCodeHistory { TokenExpiresTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.TokenExpiresTime));
        }

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体
        /// </summary>
        public AccessCodeHistory GetByTokenExpiresTime(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.TokenExpiresTime));
        }

        #endregion

                
        #region Effective

        /// <summary>
        /// 根据属性Effective获取数据实体
        /// </summary>
        public AccessCodeHistory GetByEffective(int value)
        {
            var model = new AccessCodeHistory { Effective = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Effective));
        }

        /// <summary>
        /// 根据属性Effective获取数据实体
        /// </summary>
        public AccessCodeHistory GetByEffective(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Effective));
        }

        #endregion

                
        #region SiteUserId

        /// <summary>
        /// 根据属性SiteUserId获取数据实体
        /// </summary>
        public AccessCodeHistory GetBySiteUserId(long value)
        {
            var model = new AccessCodeHistory { SiteUserId = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.SiteUserId));
        }

        /// <summary>
        /// 根据属性SiteUserId获取数据实体
        /// </summary>
        public AccessCodeHistory GetBySiteUserId(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.SiteUserId));
        }

        #endregion

                
        #region Reuse

        /// <summary>
        /// 根据属性Reuse获取数据实体
        /// </summary>
        public AccessCodeHistory GetByReuse(long value)
        {
            var model = new AccessCodeHistory { Reuse = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Reuse));
        }

        /// <summary>
        /// 根据属性Reuse获取数据实体
        /// </summary>
        public AccessCodeHistory GetByReuse(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Reuse));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessCodeHistory GetByIp(string value)
        {
            var model = new AccessCodeHistory { Ip = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessCodeHistory GetByIp(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessCodeHistory GetByCreateBy(long value)
        {
            var model = new AccessCodeHistory { CreateBy = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessCodeHistory GetByCreateBy(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessCodeHistory GetByCreateTime(DateTime value)
        {
            var model = new AccessCodeHistory { CreateTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessCodeHistory GetByCreateTime(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessCodeHistory GetByLastModifyBy(long value)
        {
            var model = new AccessCodeHistory { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessCodeHistory GetByLastModifyBy(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AccessCodeHistory GetByLastModifyTime(DateTime value)
        {
            var model = new AccessCodeHistory { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AccessCodeHistory GetByLastModifyTime(AccessCodeHistory model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeHistoryField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Code

        /// <summary>
        /// 根据属性Code获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { Code = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Code),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Code获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Code),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Token

        /// <summary>
        /// 根据属性Token获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByToken(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { Token = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Token),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Token获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByToken(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Token),sort,operateMode);
        }

        #endregion

                
        #region GetList By TokenExpiresTime

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByTokenExpiresTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { TokenExpiresTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.TokenExpiresTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TokenExpiresTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByTokenExpiresTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.TokenExpiresTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Effective

        /// <summary>
        /// 根据属性Effective获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByEffective(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { Effective = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Effective),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Effective获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByEffective(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Effective),sort,operateMode);
        }

        #endregion

                
        #region GetList By SiteUserId

        /// <summary>
        /// 根据属性SiteUserId获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListBySiteUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { SiteUserId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.SiteUserId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SiteUserId获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListBySiteUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.SiteUserId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Reuse

        /// <summary>
        /// 根据属性Reuse获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByReuse(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { Reuse = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Reuse),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Reuse获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByReuse(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Reuse),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeHistory { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeHistory> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeHistory model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeHistoryField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}