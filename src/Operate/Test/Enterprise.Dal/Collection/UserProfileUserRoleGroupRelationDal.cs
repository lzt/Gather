﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserProfileUserRoleGroupRelationDal : ExBaseDal<UserProfileUserRoleGroupRelation>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserProfileUserRoleGroupRelationDal(): this(Initialization.GetXmlConfig(typeof(UserProfileUserRoleGroupRelation)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserProfileUserRoleGroupRelationDal CreateDal()
        {
			return new UserProfileUserRoleGroupRelationDal(Initialization.GetXmlConfig(typeof(UserProfileUserRoleGroupRelation)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserProfileUserRoleGroupRelationDal()
        {
           ModelName= typeof(UserProfileUserRoleGroupRelation).Name;
           var item = new UserProfileUserRoleGroupRelation();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserProfileUserRoleGroupRelationDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserProfileUserRoleGroupRelation";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserRoleGroupId

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByUserRoleGroupId(long value)
        {
            var model = new UserProfileUserRoleGroupRelation { UserRoleGroupId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.UserRoleGroupId));
        }

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByUserRoleGroupId(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.UserRoleGroupId));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByUserProfileId(long value)
        {
            var model = new UserProfileUserRoleGroupRelation { UserProfileId = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByUserProfileId(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.UserProfileId));
        }

        #endregion

                
        #region Status

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByStatus(long value)
        {
            var model = new UserProfileUserRoleGroupRelation { Status = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.Status));
        }

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByStatus(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.Status));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByIp(string value)
        {
            var model = new UserProfileUserRoleGroupRelation { Ip = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByIp(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByCreateBy(long value)
        {
            var model = new UserProfileUserRoleGroupRelation { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByCreateBy(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByCreateTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupRelation { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByCreateTime(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByLastModifyBy(long value)
        {
            var model = new UserProfileUserRoleGroupRelation { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByLastModifyBy(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByLastModifyTime(DateTime value)
        {
            var model = new UserProfileUserRoleGroupRelation { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserProfileUserRoleGroupRelation GetByLastModifyTime(UserProfileUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserRoleGroupId

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByUserRoleGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { UserRoleGroupId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.UserRoleGroupId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByUserRoleGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.UserRoleGroupId),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Status

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { Status = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.Status),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.Status),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserProfileUserRoleGroupRelation { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserProfileUserRoleGroupRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserProfileUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserProfileUserRoleGroupRelationField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}