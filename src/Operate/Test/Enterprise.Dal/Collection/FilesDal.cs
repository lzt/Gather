﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class FilesDal : ExBaseDal<Files>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public FilesDal(): this(Initialization.GetXmlConfig(typeof(Files)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static FilesDal CreateDal()
        {
			return new FilesDal(Initialization.GetXmlConfig(typeof(Files)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static FilesDal()
        {
           ModelName= typeof(Files).Name;
           var item = new Files();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public FilesDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Files";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public Files GetByUserProfileId(int value)
        {
            var model = new Files { UserProfileId = value };
            return GetByWhere(model.GetFilterString(FilesField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public Files GetByUserProfileId(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.UserProfileId));
        }

        #endregion

                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Files GetByName(string value)
        {
            var model = new Files { Name = value };
            return GetByWhere(model.GetFilterString(FilesField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Files GetByName(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.Name));
        }

        #endregion

                
        #region SourceName

        /// <summary>
        /// 根据属性SourceName获取数据实体
        /// </summary>
        public Files GetBySourceName(string value)
        {
            var model = new Files { SourceName = value };
            return GetByWhere(model.GetFilterString(FilesField.SourceName));
        }

        /// <summary>
        /// 根据属性SourceName获取数据实体
        /// </summary>
        public Files GetBySourceName(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.SourceName));
        }

        #endregion

                
        #region Path

        /// <summary>
        /// 根据属性Path获取数据实体
        /// </summary>
        public Files GetByPath(string value)
        {
            var model = new Files { Path = value };
            return GetByWhere(model.GetFilterString(FilesField.Path));
        }

        /// <summary>
        /// 根据属性Path获取数据实体
        /// </summary>
        public Files GetByPath(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.Path));
        }

        #endregion

                
        #region FileType

        /// <summary>
        /// 根据属性FileType获取数据实体
        /// </summary>
        public Files GetByFileType(int value)
        {
            var model = new Files { FileType = value };
            return GetByWhere(model.GetFilterString(FilesField.FileType));
        }

        /// <summary>
        /// 根据属性FileType获取数据实体
        /// </summary>
        public Files GetByFileType(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.FileType));
        }

        #endregion

                
        #region ExtensionName

        /// <summary>
        /// 根据属性ExtensionName获取数据实体
        /// </summary>
        public Files GetByExtensionName(string value)
        {
            var model = new Files { ExtensionName = value };
            return GetByWhere(model.GetFilterString(FilesField.ExtensionName));
        }

        /// <summary>
        /// 根据属性ExtensionName获取数据实体
        /// </summary>
        public Files GetByExtensionName(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.ExtensionName));
        }

        #endregion

                
        #region FileSize

        /// <summary>
        /// 根据属性FileSize获取数据实体
        /// </summary>
        public Files GetByFileSize(int value)
        {
            var model = new Files { FileSize = value };
            return GetByWhere(model.GetFilterString(FilesField.FileSize));
        }

        /// <summary>
        /// 根据属性FileSize获取数据实体
        /// </summary>
        public Files GetByFileSize(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.FileSize));
        }

        #endregion

                
        #region PixelSize

        /// <summary>
        /// 根据属性PixelSize获取数据实体
        /// </summary>
        public Files GetByPixelSize(string value)
        {
            var model = new Files { PixelSize = value };
            return GetByWhere(model.GetFilterString(FilesField.PixelSize));
        }

        /// <summary>
        /// 根据属性PixelSize获取数据实体
        /// </summary>
        public Files GetByPixelSize(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.PixelSize));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Files GetByIp(string value)
        {
            var model = new Files { Ip = value };
            return GetByWhere(model.GetFilterString(FilesField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Files GetByIp(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Files GetByCreateBy(long value)
        {
            var model = new Files { CreateBy = value };
            return GetByWhere(model.GetFilterString(FilesField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Files GetByCreateBy(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Files GetByCreateTime(DateTime value)
        {
            var model = new Files { CreateTime = value };
            return GetByWhere(model.GetFilterString(FilesField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Files GetByCreateTime(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Files GetByLastModifyBy(long value)
        {
            var model = new Files { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(FilesField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Files GetByLastModifyBy(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Files GetByLastModifyTime(DateTime value)
        {
            var model = new Files { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(FilesField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Files GetByLastModifyTime(Files model)
        {
            return GetByWhere(model.GetFilterString(FilesField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<Files> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<Files> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Files> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Files> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceName

        /// <summary>
        /// 根据属性SourceName获取数据实体列表
        /// </summary>
        public IList<Files> GetListBySourceName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { SourceName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.SourceName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceName获取数据实体列表
        /// </summary>
        public IList<Files> GetListBySourceName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.SourceName),sort,operateMode);
        }

        #endregion

                
        #region GetList By Path

        /// <summary>
        /// 根据属性Path获取数据实体列表
        /// </summary>
        public IList<Files> GetListByPath(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { Path = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.Path),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Path获取数据实体列表
        /// </summary>
        public IList<Files> GetListByPath(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.Path),sort,operateMode);
        }

        #endregion

                
        #region GetList By FileType

        /// <summary>
        /// 根据属性FileType获取数据实体列表
        /// </summary>
        public IList<Files> GetListByFileType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { FileType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.FileType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FileType获取数据实体列表
        /// </summary>
        public IList<Files> GetListByFileType(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.FileType),sort,operateMode);
        }

        #endregion

                
        #region GetList By ExtensionName

        /// <summary>
        /// 根据属性ExtensionName获取数据实体列表
        /// </summary>
        public IList<Files> GetListByExtensionName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { ExtensionName = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.ExtensionName),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ExtensionName获取数据实体列表
        /// </summary>
        public IList<Files> GetListByExtensionName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.ExtensionName),sort,operateMode);
        }

        #endregion

                
        #region GetList By FileSize

        /// <summary>
        /// 根据属性FileSize获取数据实体列表
        /// </summary>
        public IList<Files> GetListByFileSize(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { FileSize = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.FileSize),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FileSize获取数据实体列表
        /// </summary>
        public IList<Files> GetListByFileSize(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.FileSize),sort,operateMode);
        }

        #endregion

                
        #region GetList By PixelSize

        /// <summary>
        /// 根据属性PixelSize获取数据实体列表
        /// </summary>
        public IList<Files> GetListByPixelSize(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { PixelSize = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.PixelSize),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PixelSize获取数据实体列表
        /// </summary>
        public IList<Files> GetListByPixelSize(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.PixelSize),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Files> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Files> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Files> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Files> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Files> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Files> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Files> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Files> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Files> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Files { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Files> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Files model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(FilesField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}