﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class EvaluationDal : ExBaseDal<Evaluation>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public EvaluationDal(): this(Initialization.GetXmlConfig(typeof(Evaluation)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static EvaluationDal CreateDal()
        {
			return new EvaluationDal(Initialization.GetXmlConfig(typeof(Evaluation)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static EvaluationDal()
        {
           ModelName= typeof(Evaluation).Name;
           var item = new Evaluation();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public EvaluationDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Evaluation";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region RaterId

        /// <summary>
        /// 根据属性RaterId获取数据实体
        /// </summary>
        public Evaluation GetByRaterId(int value)
        {
            var model = new Evaluation { RaterId = value };
            return GetByWhere(model.GetFilterString(EvaluationField.RaterId));
        }

        /// <summary>
        /// 根据属性RaterId获取数据实体
        /// </summary>
        public Evaluation GetByRaterId(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.RaterId));
        }

        #endregion

                
        #region TargetUserId

        /// <summary>
        /// 根据属性TargetUserId获取数据实体
        /// </summary>
        public Evaluation GetByTargetUserId(int value)
        {
            var model = new Evaluation { TargetUserId = value };
            return GetByWhere(model.GetFilterString(EvaluationField.TargetUserId));
        }

        /// <summary>
        /// 根据属性TargetUserId获取数据实体
        /// </summary>
        public Evaluation GetByTargetUserId(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.TargetUserId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public Evaluation GetByContent(string value)
        {
            var model = new Evaluation { Content = value };
            return GetByWhere(model.GetFilterString(EvaluationField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public Evaluation GetByContent(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.Content));
        }

        #endregion

                
        #region Verification

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public Evaluation GetByVerification(int value)
        {
            var model = new Evaluation { Verification = value };
            return GetByWhere(model.GetFilterString(EvaluationField.Verification));
        }

        /// <summary>
        /// 根据属性Verification获取数据实体
        /// </summary>
        public Evaluation GetByVerification(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.Verification));
        }

        #endregion

                
        #region Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public Evaluation GetByVerifier(int value)
        {
            var model = new Evaluation { Verifier = value };
            return GetByWhere(model.GetFilterString(EvaluationField.Verifier));
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体
        /// </summary>
        public Evaluation GetByVerifier(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.Verifier));
        }

        #endregion

                
        #region VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public Evaluation GetByVerificationTime(DateTime value)
        {
            var model = new Evaluation { VerificationTime = value };
            return GetByWhere(model.GetFilterString(EvaluationField.VerificationTime));
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体
        /// </summary>
        public Evaluation GetByVerificationTime(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.VerificationTime));
        }

        #endregion

                
        #region OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public Evaluation GetByOrderId(int value)
        {
            var model = new Evaluation { OrderId = value };
            return GetByWhere(model.GetFilterString(EvaluationField.OrderId));
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体
        /// </summary>
        public Evaluation GetByOrderId(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.OrderId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Evaluation GetByIp(string value)
        {
            var model = new Evaluation { Ip = value };
            return GetByWhere(model.GetFilterString(EvaluationField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Evaluation GetByIp(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Evaluation GetByCreateBy(long value)
        {
            var model = new Evaluation { CreateBy = value };
            return GetByWhere(model.GetFilterString(EvaluationField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Evaluation GetByCreateBy(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Evaluation GetByCreateTime(DateTime value)
        {
            var model = new Evaluation { CreateTime = value };
            return GetByWhere(model.GetFilterString(EvaluationField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Evaluation GetByCreateTime(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Evaluation GetByLastModifyBy(long value)
        {
            var model = new Evaluation { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(EvaluationField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Evaluation GetByLastModifyBy(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Evaluation GetByLastModifyTime(DateTime value)
        {
            var model = new Evaluation { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(EvaluationField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Evaluation GetByLastModifyTime(Evaluation model)
        {
            return GetByWhere(model.GetFilterString(EvaluationField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By RaterId

        /// <summary>
        /// 根据属性RaterId获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByRaterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { RaterId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.RaterId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性RaterId获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByRaterId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.RaterId),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserId

        /// <summary>
        /// 根据属性TargetUserId获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByTargetUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { TargetUserId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.TargetUserId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserId获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByTargetUserId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.TargetUserId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verification

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { Verification = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Verification),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verification获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByVerification(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Verification),sort,operateMode);
        }

        #endregion

                
        #region GetList By Verifier

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { Verifier = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Verifier),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Verifier获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByVerifier(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Verifier),sort,operateMode);
        }

        #endregion

                
        #region GetList By VerificationTime

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { VerificationTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.VerificationTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性VerificationTime获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByVerificationTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.VerificationTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By OrderId

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { OrderId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.OrderId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性OrderId获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByOrderId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.OrderId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Evaluation { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Evaluation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Evaluation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(EvaluationField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}