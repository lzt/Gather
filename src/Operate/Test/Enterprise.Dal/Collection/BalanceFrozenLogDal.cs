﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class BalanceFrozenLogDal : ExBaseDal<BalanceFrozenLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public BalanceFrozenLogDal(): this(Initialization.GetXmlConfig(typeof(BalanceFrozenLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static BalanceFrozenLogDal CreateDal()
        {
			return new BalanceFrozenLogDal(Initialization.GetXmlConfig(typeof(BalanceFrozenLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static BalanceFrozenLogDal()
        {
           ModelName= typeof(BalanceFrozenLog).Name;
           var item = new BalanceFrozenLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public BalanceFrozenLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "BalanceFrozenLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByUserProfileId(int value)
        {
            var model = new BalanceFrozenLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByUserProfileId(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.UserProfileId));
        }

        #endregion

                
        #region FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByFrozenTime(DateTime value)
        {
            var model = new BalanceFrozenLog { FrozenTime = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.FrozenTime));
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByFrozenTime(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.FrozenTime));
        }

        #endregion

                
        #region Money

        /// <summary>
        /// 根据属性Money获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByMoney(int value)
        {
            var model = new BalanceFrozenLog { Money = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.Money));
        }

        /// <summary>
        /// 根据属性Money获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByMoney(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.Money));
        }

        #endregion

                
        #region ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByThawTime(DateTime value)
        {
            var model = new BalanceFrozenLog { ThawTime = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.ThawTime));
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByThawTime(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.ThawTime));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByIp(string value)
        {
            var model = new BalanceFrozenLog { Ip = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByIp(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByCreateBy(long value)
        {
            var model = new BalanceFrozenLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByCreateBy(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByCreateTime(DateTime value)
        {
            var model = new BalanceFrozenLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByCreateTime(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByLastModifyBy(long value)
        {
            var model = new BalanceFrozenLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByLastModifyBy(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByLastModifyTime(DateTime value)
        {
            var model = new BalanceFrozenLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public BalanceFrozenLog GetByLastModifyTime(BalanceFrozenLog model)
        {
            return GetByWhere(model.GetFilterString(BalanceFrozenLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By FrozenTime

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { FrozenTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.FrozenTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FrozenTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByFrozenTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.FrozenTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Money

        /// <summary>
        /// 根据属性Money获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByMoney(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { Money = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.Money),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Money获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByMoney(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.Money),sort,operateMode);
        }

        #endregion

                
        #region GetList By ThawTime

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { ThawTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.ThawTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ThawTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByThawTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.ThawTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new BalanceFrozenLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<BalanceFrozenLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, BalanceFrozenLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(BalanceFrozenLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}