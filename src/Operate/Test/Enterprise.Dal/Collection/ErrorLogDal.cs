﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class ErrorLogDal : ExBaseDal<ErrorLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public ErrorLogDal(): this(Initialization.GetXmlConfig(typeof(ErrorLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static ErrorLogDal CreateDal()
        {
			return new ErrorLogDal(Initialization.GetXmlConfig(typeof(ErrorLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static ErrorLogDal()
        {
           ModelName= typeof(ErrorLog).Name;
           var item = new ErrorLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public ErrorLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "ErrorLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Source

        /// <summary>
        /// 根据属性Source获取数据实体
        /// </summary>
        public ErrorLog GetBySource(string value)
        {
            var model = new ErrorLog { Source = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Source));
        }

        /// <summary>
        /// 根据属性Source获取数据实体
        /// </summary>
        public ErrorLog GetBySource(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Source));
        }

        #endregion

                
        #region Message

        /// <summary>
        /// 根据属性Message获取数据实体
        /// </summary>
        public ErrorLog GetByMessage(string value)
        {
            var model = new ErrorLog { Message = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Message));
        }

        /// <summary>
        /// 根据属性Message获取数据实体
        /// </summary>
        public ErrorLog GetByMessage(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Message));
        }

        #endregion

                
        #region StackTrace

        /// <summary>
        /// 根据属性StackTrace获取数据实体
        /// </summary>
        public ErrorLog GetByStackTrace(string value)
        {
            var model = new ErrorLog { StackTrace = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.StackTrace));
        }

        /// <summary>
        /// 根据属性StackTrace获取数据实体
        /// </summary>
        public ErrorLog GetByStackTrace(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.StackTrace));
        }

        #endregion

                
        #region Scene

        /// <summary>
        /// 根据属性Scene获取数据实体
        /// </summary>
        public ErrorLog GetByScene(string value)
        {
            var model = new ErrorLog { Scene = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Scene));
        }

        /// <summary>
        /// 根据属性Scene获取数据实体
        /// </summary>
        public ErrorLog GetByScene(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Scene));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public ErrorLog GetByType(int value)
        {
            var model = new ErrorLog { Type = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public ErrorLog GetByType(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Type));
        }

        #endregion

                
        #region Header

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public ErrorLog GetByHeader(string value)
        {
            var model = new ErrorLog { Header = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Header));
        }

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public ErrorLog GetByHeader(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Header));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public ErrorLog GetByUrl(string value)
        {
            var model = new ErrorLog { Url = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public ErrorLog GetByUrl(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Url));
        }

        #endregion

                
        #region FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public ErrorLog GetByFormParams(string value)
        {
            var model = new ErrorLog { FormParams = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.FormParams));
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public ErrorLog GetByFormParams(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.FormParams));
        }

        #endregion

                
        #region UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public ErrorLog GetByUrlParams(string value)
        {
            var model = new ErrorLog { UrlParams = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.UrlParams));
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public ErrorLog GetByUrlParams(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.UrlParams));
        }

        #endregion

                
        #region Host

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public ErrorLog GetByHost(string value)
        {
            var model = new ErrorLog { Host = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Host));
        }

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public ErrorLog GetByHost(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Host));
        }

        #endregion

                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public ErrorLog GetByUserProfileId(int value)
        {
            var model = new ErrorLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public ErrorLog GetByUserProfileId(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.UserProfileId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public ErrorLog GetByIp(string value)
        {
            var model = new ErrorLog { Ip = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public ErrorLog GetByIp(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public ErrorLog GetByCreateBy(long value)
        {
            var model = new ErrorLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public ErrorLog GetByCreateBy(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ErrorLog GetByCreateTime(DateTime value)
        {
            var model = new ErrorLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public ErrorLog GetByCreateTime(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public ErrorLog GetByLastModifyBy(long value)
        {
            var model = new ErrorLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public ErrorLog GetByLastModifyBy(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ErrorLog GetByLastModifyTime(DateTime value)
        {
            var model = new ErrorLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(ErrorLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public ErrorLog GetByLastModifyTime(ErrorLog model)
        {
            return GetByWhere(model.GetFilterString(ErrorLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Source

        /// <summary>
        /// 根据属性Source获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListBySource(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Source = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Source),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Source获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListBySource(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Source),sort,operateMode);
        }

        #endregion

                
        #region GetList By Message

        /// <summary>
        /// 根据属性Message获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByMessage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Message = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Message),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Message获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByMessage(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Message),sort,operateMode);
        }

        #endregion

                
        #region GetList By StackTrace

        /// <summary>
        /// 根据属性StackTrace获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByStackTrace(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { StackTrace = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.StackTrace),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StackTrace获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByStackTrace(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.StackTrace),sort,operateMode);
        }

        #endregion

                
        #region GetList By Scene

        /// <summary>
        /// 根据属性Scene获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByScene(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Scene = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Scene),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Scene获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByScene(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Scene),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Header

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Header = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Header),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Header),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { FormParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.FormParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.FormParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { UrlParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.UrlParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.UrlParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By Host

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Host = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Host),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Host),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new ErrorLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<ErrorLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, ErrorLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(ErrorLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}