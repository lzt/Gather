﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class NonceDal : ExBaseDal<Nonce>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public NonceDal(): this(Initialization.GetXmlConfig(typeof(Nonce)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static NonceDal CreateDal()
        {
			return new NonceDal(Initialization.GetXmlConfig(typeof(Nonce)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static NonceDal()
        {
           ModelName= typeof(Nonce).Name;
           var item = new Nonce();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public NonceDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Nonce";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Context

        /// <summary>
        /// 根据属性Context获取数据实体
        /// </summary>
        public Nonce GetByContext(string value)
        {
            var model = new Nonce { Context = value };
            return GetByWhere(model.GetFilterString(NonceField.Context));
        }

        /// <summary>
        /// 根据属性Context获取数据实体
        /// </summary>
        public Nonce GetByContext(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.Context));
        }

        #endregion

                
        #region Code

        /// <summary>
        /// 根据属性Code获取数据实体
        /// </summary>
        public Nonce GetByCode(string value)
        {
            var model = new Nonce { Code = value };
            return GetByWhere(model.GetFilterString(NonceField.Code));
        }

        /// <summary>
        /// 根据属性Code获取数据实体
        /// </summary>
        public Nonce GetByCode(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.Code));
        }

        #endregion

                
        #region Timestamp

        /// <summary>
        /// 根据属性Timestamp获取数据实体
        /// </summary>
        public Nonce GetByTimestamp(DateTime value)
        {
            var model = new Nonce { Timestamp = value };
            return GetByWhere(model.GetFilterString(NonceField.Timestamp));
        }

        /// <summary>
        /// 根据属性Timestamp获取数据实体
        /// </summary>
        public Nonce GetByTimestamp(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.Timestamp));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Nonce GetByIp(string value)
        {
            var model = new Nonce { Ip = value };
            return GetByWhere(model.GetFilterString(NonceField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Nonce GetByIp(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Nonce GetByCreateBy(long value)
        {
            var model = new Nonce { CreateBy = value };
            return GetByWhere(model.GetFilterString(NonceField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Nonce GetByCreateBy(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Nonce GetByCreateTime(DateTime value)
        {
            var model = new Nonce { CreateTime = value };
            return GetByWhere(model.GetFilterString(NonceField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Nonce GetByCreateTime(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Nonce GetByLastModifyBy(long value)
        {
            var model = new Nonce { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(NonceField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Nonce GetByLastModifyBy(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Nonce GetByLastModifyTime(DateTime value)
        {
            var model = new Nonce { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(NonceField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Nonce GetByLastModifyTime(Nonce model)
        {
            return GetByWhere(model.GetFilterString(NonceField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Context

        /// <summary>
        /// 根据属性Context获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByContext(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { Context = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Context),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Context获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByContext(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Context),sort,operateMode);
        }

        #endregion

                
        #region GetList By Code

        /// <summary>
        /// 根据属性Code获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { Code = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Code),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Code获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Code),sort,operateMode);
        }

        #endregion

                
        #region GetList By Timestamp

        /// <summary>
        /// 根据属性Timestamp获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByTimestamp(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { Timestamp = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Timestamp),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Timestamp获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByTimestamp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Timestamp),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Nonce { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Nonce> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Nonce model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(NonceField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}