﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class OperateLogDal : ExBaseDal<OperateLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public OperateLogDal(): this(Initialization.GetXmlConfig(typeof(OperateLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static OperateLogDal CreateDal()
        {
			return new OperateLogDal(Initialization.GetXmlConfig(typeof(OperateLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static OperateLogDal()
        {
           ModelName= typeof(OperateLog).Name;
           var item = new OperateLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public OperateLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "OperateLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public OperateLog GetByUserProfileId(int value)
        {
            var model = new OperateLog { UserProfileId = value };
            return GetByWhere(model.GetFilterString(OperateLogField.UserProfileId));
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体
        /// </summary>
        public OperateLog GetByUserProfileId(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.UserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public OperateLog GetByContent(string value)
        {
            var model = new OperateLog { Content = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public OperateLog GetByContent(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Content));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public OperateLog GetByType(int value)
        {
            var model = new OperateLog { Type = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public OperateLog GetByType(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Type));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public OperateLog GetByUrl(string value)
        {
            var model = new OperateLog { Url = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public OperateLog GetByUrl(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Url));
        }

        #endregion

                
        #region FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public OperateLog GetByFormParams(string value)
        {
            var model = new OperateLog { FormParams = value };
            return GetByWhere(model.GetFilterString(OperateLogField.FormParams));
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体
        /// </summary>
        public OperateLog GetByFormParams(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.FormParams));
        }

        #endregion

                
        #region UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public OperateLog GetByUrlParams(string value)
        {
            var model = new OperateLog { UrlParams = value };
            return GetByWhere(model.GetFilterString(OperateLogField.UrlParams));
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体
        /// </summary>
        public OperateLog GetByUrlParams(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.UrlParams));
        }

        #endregion

                
        #region Host

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public OperateLog GetByHost(string value)
        {
            var model = new OperateLog { Host = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Host));
        }

        /// <summary>
        /// 根据属性Host获取数据实体
        /// </summary>
        public OperateLog GetByHost(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Host));
        }

        #endregion

                
        #region Header

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public OperateLog GetByHeader(string value)
        {
            var model = new OperateLog { Header = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Header));
        }

        /// <summary>
        /// 根据属性Header获取数据实体
        /// </summary>
        public OperateLog GetByHeader(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Header));
        }

        #endregion

                
        #region Title

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public OperateLog GetByTitle(string value)
        {
            var model = new OperateLog { Title = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Title));
        }

        /// <summary>
        /// 根据属性Title获取数据实体
        /// </summary>
        public OperateLog GetByTitle(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Title));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public OperateLog GetByIp(string value)
        {
            var model = new OperateLog { Ip = value };
            return GetByWhere(model.GetFilterString(OperateLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public OperateLog GetByIp(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public OperateLog GetByCreateBy(long value)
        {
            var model = new OperateLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(OperateLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public OperateLog GetByCreateBy(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public OperateLog GetByCreateTime(DateTime value)
        {
            var model = new OperateLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(OperateLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public OperateLog GetByCreateTime(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public OperateLog GetByLastModifyBy(long value)
        {
            var model = new OperateLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(OperateLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public OperateLog GetByLastModifyBy(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public OperateLog GetByLastModifyTime(DateTime value)
        {
            var model = new OperateLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(OperateLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public OperateLog GetByLastModifyTime(OperateLog model)
        {
            return GetByWhere(model.GetFilterString(OperateLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserProfileId

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { UserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.UserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserProfileId获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.UserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By FormParams

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { FormParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.FormParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FormParams获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByFormParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.FormParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By UrlParams

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { UrlParams = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.UrlParams),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UrlParams获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByUrlParams(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.UrlParams),sort,operateMode);
        }

        #endregion

                
        #region GetList By Host

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Host = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Host),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Host获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByHost(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Host),sort,operateMode);
        }

        #endregion

                
        #region GetList By Header

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Header = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Header),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Header获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByHeader(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Header),sort,operateMode);
        }

        #endregion

                
        #region GetList By Title

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Title = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Title),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Title获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByTitle(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Title),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new OperateLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<OperateLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, OperateLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(OperateLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}