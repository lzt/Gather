﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class GeographicDal : ExBaseDal<Geographic>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public GeographicDal(): this(Initialization.GetXmlConfig(typeof(Geographic)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static GeographicDal CreateDal()
        {
			return new GeographicDal(Initialization.GetXmlConfig(typeof(Geographic)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static GeographicDal()
        {
           ModelName= typeof(Geographic).Name;
           var item = new Geographic();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public GeographicDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "Geographic";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Geographic GetByName(string value)
        {
            var model = new Geographic { Name = value };
            return GetByWhere(model.GetFilterString(GeographicField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public Geographic GetByName(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.Name));
        }

        #endregion

                
        #region ParentAreaId

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体
        /// </summary>
        public Geographic GetByParentAreaId(int value)
        {
            var model = new Geographic { ParentAreaId = value };
            return GetByWhere(model.GetFilterString(GeographicField.ParentAreaId));
        }

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体
        /// </summary>
        public Geographic GetByParentAreaId(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.ParentAreaId));
        }

        #endregion

                
        #region Letters

        /// <summary>
        /// 根据属性Letters获取数据实体
        /// </summary>
        public Geographic GetByLetters(string value)
        {
            var model = new Geographic { Letters = value };
            return GetByWhere(model.GetFilterString(GeographicField.Letters));
        }

        /// <summary>
        /// 根据属性Letters获取数据实体
        /// </summary>
        public Geographic GetByLetters(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.Letters));
        }

        #endregion

                
        #region Abbreviation

        /// <summary>
        /// 根据属性Abbreviation获取数据实体
        /// </summary>
        public Geographic GetByAbbreviation(string value)
        {
            var model = new Geographic { Abbreviation = value };
            return GetByWhere(model.GetFilterString(GeographicField.Abbreviation));
        }

        /// <summary>
        /// 根据属性Abbreviation获取数据实体
        /// </summary>
        public Geographic GetByAbbreviation(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.Abbreviation));
        }

        #endregion

                
        #region FirstLetter

        /// <summary>
        /// 根据属性FirstLetter获取数据实体
        /// </summary>
        public Geographic GetByFirstLetter(string value)
        {
            var model = new Geographic { FirstLetter = value };
            return GetByWhere(model.GetFilterString(GeographicField.FirstLetter));
        }

        /// <summary>
        /// 根据属性FirstLetter获取数据实体
        /// </summary>
        public Geographic GetByFirstLetter(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.FirstLetter));
        }

        #endregion

                
        #region PinYinAbbreviations

        /// <summary>
        /// 根据属性PinYinAbbreviations获取数据实体
        /// </summary>
        public Geographic GetByPinYinAbbreviations(string value)
        {
            var model = new Geographic { PinYinAbbreviations = value };
            return GetByWhere(model.GetFilterString(GeographicField.PinYinAbbreviations));
        }

        /// <summary>
        /// 根据属性PinYinAbbreviations获取数据实体
        /// </summary>
        public Geographic GetByPinYinAbbreviations(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.PinYinAbbreviations));
        }

        #endregion

                
        #region ZipCode

        /// <summary>
        /// 根据属性ZipCode获取数据实体
        /// </summary>
        public Geographic GetByZipCode(string value)
        {
            var model = new Geographic { ZipCode = value };
            return GetByWhere(model.GetFilterString(GeographicField.ZipCode));
        }

        /// <summary>
        /// 根据属性ZipCode获取数据实体
        /// </summary>
        public Geographic GetByZipCode(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.ZipCode));
        }

        #endregion

                
        #region Type

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Geographic GetByType(int value)
        {
            var model = new Geographic { Type = value };
            return GetByWhere(model.GetFilterString(GeographicField.Type));
        }

        /// <summary>
        /// 根据属性Type获取数据实体
        /// </summary>
        public Geographic GetByType(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.Type));
        }

        #endregion

                
        #region AreaCode

        /// <summary>
        /// 根据属性AreaCode获取数据实体
        /// </summary>
        public Geographic GetByAreaCode(string value)
        {
            var model = new Geographic { AreaCode = value };
            return GetByWhere(model.GetFilterString(GeographicField.AreaCode));
        }

        /// <summary>
        /// 根据属性AreaCode获取数据实体
        /// </summary>
        public Geographic GetByAreaCode(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.AreaCode));
        }

        #endregion

                
        #region AreaId

        /// <summary>
        /// 根据属性AreaId获取数据实体
        /// </summary>
        public Geographic GetByAreaId(int value)
        {
            var model = new Geographic { AreaId = value };
            return GetByWhere(model.GetFilterString(GeographicField.AreaId));
        }

        /// <summary>
        /// 根据属性AreaId获取数据实体
        /// </summary>
        public Geographic GetByAreaId(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.AreaId));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Geographic GetByIp(string value)
        {
            var model = new Geographic { Ip = value };
            return GetByWhere(model.GetFilterString(GeographicField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public Geographic GetByIp(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Geographic GetByCreateBy(long value)
        {
            var model = new Geographic { CreateBy = value };
            return GetByWhere(model.GetFilterString(GeographicField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public Geographic GetByCreateBy(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Geographic GetByCreateTime(DateTime value)
        {
            var model = new Geographic { CreateTime = value };
            return GetByWhere(model.GetFilterString(GeographicField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public Geographic GetByCreateTime(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Geographic GetByLastModifyBy(long value)
        {
            var model = new Geographic { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(GeographicField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public Geographic GetByLastModifyBy(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Geographic GetByLastModifyTime(DateTime value)
        {
            var model = new Geographic { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(GeographicField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public Geographic GetByLastModifyTime(Geographic model)
        {
            return GetByWhere(model.GetFilterString(GeographicField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By ParentAreaId

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByParentAreaId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { ParentAreaId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.ParentAreaId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ParentAreaId获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByParentAreaId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.ParentAreaId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Letters

        /// <summary>
        /// 根据属性Letters获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByLetters(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { Letters = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Letters),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Letters获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByLetters(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Letters),sort,operateMode);
        }

        #endregion

                
        #region GetList By Abbreviation

        /// <summary>
        /// 根据属性Abbreviation获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByAbbreviation(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { Abbreviation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Abbreviation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Abbreviation获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByAbbreviation(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Abbreviation),sort,operateMode);
        }

        #endregion

                
        #region GetList By FirstLetter

        /// <summary>
        /// 根据属性FirstLetter获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByFirstLetter(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { FirstLetter = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.FirstLetter),sort,operateMode);
        }

        /// <summary>
        /// 根据属性FirstLetter获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByFirstLetter(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.FirstLetter),sort,operateMode);
        }

        #endregion

                
        #region GetList By PinYinAbbreviations

        /// <summary>
        /// 根据属性PinYinAbbreviations获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByPinYinAbbreviations(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { PinYinAbbreviations = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.PinYinAbbreviations),sort,operateMode);
        }

        /// <summary>
        /// 根据属性PinYinAbbreviations获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByPinYinAbbreviations(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.PinYinAbbreviations),sort,operateMode);
        }

        #endregion

                
        #region GetList By ZipCode

        /// <summary>
        /// 根据属性ZipCode获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByZipCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { ZipCode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.ZipCode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性ZipCode获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByZipCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.ZipCode),sort,operateMode);
        }

        #endregion

                
        #region GetList By Type

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { Type = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Type),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Type获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByType(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Type),sort,operateMode);
        }

        #endregion

                
        #region GetList By AreaCode

        /// <summary>
        /// 根据属性AreaCode获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByAreaCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { AreaCode = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.AreaCode),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AreaCode获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByAreaCode(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.AreaCode),sort,operateMode);
        }

        #endregion

                
        #region GetList By AreaId

        /// <summary>
        /// 根据属性AreaId获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByAreaId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { AreaId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.AreaId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AreaId获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByAreaId(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.AreaId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new Geographic { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<Geographic> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, Geographic model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(GeographicField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}