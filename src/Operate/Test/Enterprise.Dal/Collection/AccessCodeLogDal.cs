﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class AccessCodeLogDal : ExBaseDal<AccessCodeLog>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public AccessCodeLogDal(): this(Initialization.GetXmlConfig(typeof(AccessCodeLog)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static AccessCodeLogDal CreateDal()
        {
			return new AccessCodeLogDal(Initialization.GetXmlConfig(typeof(AccessCodeLog)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static AccessCodeLogDal()
        {
           ModelName= typeof(AccessCodeLog).Name;
           var item = new AccessCodeLog();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public AccessCodeLogDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "AccessCodeLog";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region AccessCodeId

        /// <summary>
        /// 根据属性AccessCodeId获取数据实体
        /// </summary>
        public AccessCodeLog GetByAccessCodeId(int value)
        {
            var model = new AccessCodeLog { AccessCodeId = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.AccessCodeId));
        }

        /// <summary>
        /// 根据属性AccessCodeId获取数据实体
        /// </summary>
        public AccessCodeLog GetByAccessCodeId(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.AccessCodeId));
        }

        #endregion

                
        #region Domain

        /// <summary>
        /// 根据属性Domain获取数据实体
        /// </summary>
        public AccessCodeLog GetByDomain(string value)
        {
            var model = new AccessCodeLog { Domain = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.Domain));
        }

        /// <summary>
        /// 根据属性Domain获取数据实体
        /// </summary>
        public AccessCodeLog GetByDomain(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.Domain));
        }

        #endregion

                
        #region Url

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public AccessCodeLog GetByUrl(string value)
        {
            var model = new AccessCodeLog { Url = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.Url));
        }

        /// <summary>
        /// 根据属性Url获取数据实体
        /// </summary>
        public AccessCodeLog GetByUrl(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.Url));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessCodeLog GetByIp(string value)
        {
            var model = new AccessCodeLog { Ip = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public AccessCodeLog GetByIp(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessCodeLog GetByCreateBy(long value)
        {
            var model = new AccessCodeLog { CreateBy = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public AccessCodeLog GetByCreateBy(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessCodeLog GetByCreateTime(DateTime value)
        {
            var model = new AccessCodeLog { CreateTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public AccessCodeLog GetByCreateTime(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessCodeLog GetByLastModifyBy(long value)
        {
            var model = new AccessCodeLog { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public AccessCodeLog GetByLastModifyBy(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AccessCodeLog GetByLastModifyTime(DateTime value)
        {
            var model = new AccessCodeLog { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(AccessCodeLogField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public AccessCodeLog GetByLastModifyTime(AccessCodeLog model)
        {
            return GetByWhere(model.GetFilterString(AccessCodeLogField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By AccessCodeId

        /// <summary>
        /// 根据属性AccessCodeId获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByAccessCodeId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { AccessCodeId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.AccessCodeId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性AccessCodeId获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByAccessCodeId(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.AccessCodeId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Domain

        /// <summary>
        /// 根据属性Domain获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByDomain(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { Domain = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.Domain),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Domain获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByDomain(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.Domain),sort,operateMode);
        }

        #endregion

                
        #region GetList By Url

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { Url = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.Url),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Url获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByUrl(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.Url),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new AccessCodeLog { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<AccessCodeLog> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, AccessCodeLog model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(AccessCodeLogField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}