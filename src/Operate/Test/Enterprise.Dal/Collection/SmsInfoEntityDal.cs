﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class SmsInfoEntityDal : ExBaseDal<SmsInfoEntity>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public SmsInfoEntityDal(): this(Initialization.GetXmlConfig(typeof(SmsInfoEntity)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static SmsInfoEntityDal CreateDal()
        {
			return new SmsInfoEntityDal(Initialization.GetXmlConfig(typeof(SmsInfoEntity)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static SmsInfoEntityDal()
        {
           ModelName= typeof(SmsInfoEntity).Name;
           var item = new SmsInfoEntity();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public SmsInfoEntityDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "SmsInfoEntity";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntity GetBySourceUserProfileId(int value)
        {
            var model = new SmsInfoEntity { SourceUserProfileId = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.SourceUserProfileId));
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntity GetBySourceUserProfileId(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.SourceUserProfileId));
        }

        #endregion

                
        #region Content

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SmsInfoEntity GetByContent(string value)
        {
            var model = new SmsInfoEntity { Content = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.Content));
        }

        /// <summary>
        /// 根据属性Content获取数据实体
        /// </summary>
        public SmsInfoEntity GetByContent(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.Content));
        }

        #endregion

                
        #region Phone

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public SmsInfoEntity GetByPhone(string value)
        {
            var model = new SmsInfoEntity { Phone = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.Phone));
        }

        /// <summary>
        /// 根据属性Phone获取数据实体
        /// </summary>
        public SmsInfoEntity GetByPhone(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.Phone));
        }

        #endregion

                
        #region StatusSuccess

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体
        /// </summary>
        public SmsInfoEntity GetByStatusSuccess(int value)
        {
            var model = new SmsInfoEntity { StatusSuccess = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.StatusSuccess));
        }

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体
        /// </summary>
        public SmsInfoEntity GetByStatusSuccess(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.StatusSuccess));
        }

        #endregion

                
        #region TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntity GetByTargetUserProfileId(int value)
        {
            var model = new SmsInfoEntity { TargetUserProfileId = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.TargetUserProfileId));
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体
        /// </summary>
        public SmsInfoEntity GetByTargetUserProfileId(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.TargetUserProfileId));
        }

        #endregion

                
        #region InterfaceFeedback

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体
        /// </summary>
        public SmsInfoEntity GetByInterfaceFeedback(string value)
        {
            var model = new SmsInfoEntity { InterfaceFeedback = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.InterfaceFeedback));
        }

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体
        /// </summary>
        public SmsInfoEntity GetByInterfaceFeedback(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.InterfaceFeedback));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SmsInfoEntity GetByIp(string value)
        {
            var model = new SmsInfoEntity { Ip = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public SmsInfoEntity GetByIp(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SmsInfoEntity GetByCreateBy(long value)
        {
            var model = new SmsInfoEntity { CreateBy = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public SmsInfoEntity GetByCreateBy(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SmsInfoEntity GetByCreateTime(DateTime value)
        {
            var model = new SmsInfoEntity { CreateTime = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public SmsInfoEntity GetByCreateTime(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SmsInfoEntity GetByLastModifyBy(long value)
        {
            var model = new SmsInfoEntity { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public SmsInfoEntity GetByLastModifyBy(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SmsInfoEntity GetByLastModifyTime(DateTime value)
        {
            var model = new SmsInfoEntity { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public SmsInfoEntity GetByLastModifyTime(SmsInfoEntity model)
        {
            return GetByWhere(model.GetFilterString(SmsInfoEntityField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By SourceUserProfileId

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { SourceUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.SourceUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListBySourceUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.SourceUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Content

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { Content = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.Content),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Content获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByContent(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.Content),sort,operateMode);
        }

        #endregion

                
        #region GetList By Phone

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { Phone = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.Phone),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Phone获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByPhone(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.Phone),sort,operateMode);
        }

        #endregion

                
        #region GetList By StatusSuccess

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByStatusSuccess(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { StatusSuccess = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.StatusSuccess),sort,operateMode);
        }

        /// <summary>
        /// 根据属性StatusSuccess获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByStatusSuccess(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.StatusSuccess),sort,operateMode);
        }

        #endregion

                
        #region GetList By TargetUserProfileId

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { TargetUserProfileId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.TargetUserProfileId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性TargetUserProfileId获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByTargetUserProfileId(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.TargetUserProfileId),sort,operateMode);
        }

        #endregion

                
        #region GetList By InterfaceFeedback

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByInterfaceFeedback(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { InterfaceFeedback = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.InterfaceFeedback),sort,operateMode);
        }

        /// <summary>
        /// 根据属性InterfaceFeedback获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByInterfaceFeedback(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.InterfaceFeedback),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new SmsInfoEntity { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<SmsInfoEntity> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, SmsInfoEntity model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(SmsInfoEntityField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}