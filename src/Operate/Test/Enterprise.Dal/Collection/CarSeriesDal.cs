﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class CarSeriesDal : ExBaseDal<CarSeries>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CarSeriesDal(): this(Initialization.GetXmlConfig(typeof(CarSeries)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CarSeriesDal CreateDal()
        {
			return new CarSeriesDal(Initialization.GetXmlConfig(typeof(CarSeries)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static CarSeriesDal()
        {
           ModelName= typeof(CarSeries).Name;
           var item = new CarSeries();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CarSeriesDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "CarSeries";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region SeriesId

        /// <summary>
        /// 根据属性SeriesId获取数据实体
        /// </summary>
        public CarSeries GetBySeriesId(int value)
        {
            var model = new CarSeries { SeriesId = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.SeriesId));
        }

        /// <summary>
        /// 根据属性SeriesId获取数据实体
        /// </summary>
        public CarSeries GetBySeriesId(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.SeriesId));
        }

        #endregion

                
        #region Name

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarSeries GetByName(string value)
        {
            var model = new CarSeries { Name = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.Name));
        }

        /// <summary>
        /// 根据属性Name获取数据实体
        /// </summary>
        public CarSeries GetByName(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.Name));
        }

        #endregion

                
        #region Image

        /// <summary>
        /// 根据属性Image获取数据实体
        /// </summary>
        public CarSeries GetByImage(string value)
        {
            var model = new CarSeries { Image = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.Image));
        }

        /// <summary>
        /// 根据属性Image获取数据实体
        /// </summary>
        public CarSeries GetByImage(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.Image));
        }

        #endregion

                
        #region SourceId

        /// <summary>
        /// 根据属性SourceId获取数据实体
        /// </summary>
        public CarSeries GetBySourceId(int value)
        {
            var model = new CarSeries { SourceId = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.SourceId));
        }

        /// <summary>
        /// 根据属性SourceId获取数据实体
        /// </summary>
        public CarSeries GetBySourceId(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.SourceId));
        }

        #endregion

                
        #region Abbv

        /// <summary>
        /// 根据属性Abbv获取数据实体
        /// </summary>
        public CarSeries GetByAbbv(string value)
        {
            var model = new CarSeries { Abbv = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.Abbv));
        }

        /// <summary>
        /// 根据属性Abbv获取数据实体
        /// </summary>
        public CarSeries GetByAbbv(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.Abbv));
        }

        #endregion

                
        #region CarType

        /// <summary>
        /// 根据属性CarType获取数据实体
        /// </summary>
        public CarSeries GetByCarType(int value)
        {
            var model = new CarSeries { CarType = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.CarType));
        }

        /// <summary>
        /// 根据属性CarType获取数据实体
        /// </summary>
        public CarSeries GetByCarType(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.CarType));
        }

        #endregion

                
        #region MinQuotation

        /// <summary>
        /// 根据属性MinQuotation获取数据实体
        /// </summary>
        public CarSeries GetByMinQuotation(double value)
        {
            var model = new CarSeries { MinQuotation = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.MinQuotation));
        }

        /// <summary>
        /// 根据属性MinQuotation获取数据实体
        /// </summary>
        public CarSeries GetByMinQuotation(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.MinQuotation));
        }

        #endregion

                
        #region MaxQuotation

        /// <summary>
        /// 根据属性MaxQuotation获取数据实体
        /// </summary>
        public CarSeries GetByMaxQuotation(double value)
        {
            var model = new CarSeries { MaxQuotation = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.MaxQuotation));
        }

        /// <summary>
        /// 根据属性MaxQuotation获取数据实体
        /// </summary>
        public CarSeries GetByMaxQuotation(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.MaxQuotation));
        }

        #endregion

                
        #region Quotation

        /// <summary>
        /// 根据属性Quotation获取数据实体
        /// </summary>
        public CarSeries GetByQuotation(string value)
        {
            var model = new CarSeries { Quotation = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.Quotation));
        }

        /// <summary>
        /// 根据属性Quotation获取数据实体
        /// </summary>
        public CarSeries GetByQuotation(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.Quotation));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarSeries GetByIp(string value)
        {
            var model = new CarSeries { Ip = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public CarSeries GetByIp(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarSeries GetByCreateBy(long value)
        {
            var model = new CarSeries { CreateBy = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public CarSeries GetByCreateBy(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarSeries GetByCreateTime(DateTime value)
        {
            var model = new CarSeries { CreateTime = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public CarSeries GetByCreateTime(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarSeries GetByLastModifyBy(long value)
        {
            var model = new CarSeries { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public CarSeries GetByLastModifyBy(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarSeries GetByLastModifyTime(DateTime value)
        {
            var model = new CarSeries { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(CarSeriesField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public CarSeries GetByLastModifyTime(CarSeries model)
        {
            return GetByWhere(model.GetFilterString(CarSeriesField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By SeriesId

        /// <summary>
        /// 根据属性SeriesId获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListBySeriesId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { SeriesId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.SeriesId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SeriesId获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListBySeriesId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.SeriesId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Name

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { Name = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Name),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Name获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByName(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Name),sort,operateMode);
        }

        #endregion

                
        #region GetList By Image

        /// <summary>
        /// 根据属性Image获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { Image = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Image),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Image获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByImage(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Image),sort,operateMode);
        }

        #endregion

                
        #region GetList By SourceId

        /// <summary>
        /// 根据属性SourceId获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListBySourceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { SourceId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.SourceId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性SourceId获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListBySourceId(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.SourceId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Abbv

        /// <summary>
        /// 根据属性Abbv获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByAbbv(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { Abbv = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Abbv),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Abbv获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByAbbv(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Abbv),sort,operateMode);
        }

        #endregion

                
        #region GetList By CarType

        /// <summary>
        /// 根据属性CarType获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByCarType(int currentPage, int pagesize, out long totalPages, out long totalRecords, int value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { CarType = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.CarType),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CarType获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByCarType(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.CarType),sort,operateMode);
        }

        #endregion

                
        #region GetList By MinQuotation

        /// <summary>
        /// 根据属性MinQuotation获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByMinQuotation(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { MinQuotation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.MinQuotation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性MinQuotation获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByMinQuotation(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.MinQuotation),sort,operateMode);
        }

        #endregion

                
        #region GetList By MaxQuotation

        /// <summary>
        /// 根据属性MaxQuotation获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByMaxQuotation(int currentPage, int pagesize, out long totalPages, out long totalRecords, double value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { MaxQuotation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.MaxQuotation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性MaxQuotation获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByMaxQuotation(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.MaxQuotation),sort,operateMode);
        }

        #endregion

                
        #region GetList By Quotation

        /// <summary>
        /// 根据属性Quotation获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByQuotation(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { Quotation = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Quotation),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Quotation获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByQuotation(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Quotation),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new CarSeries { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<CarSeries> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, CarSeries model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(CarSeriesField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}