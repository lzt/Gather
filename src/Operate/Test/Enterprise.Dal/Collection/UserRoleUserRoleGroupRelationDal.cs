﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.Collections.Generic;
using Enterprise.Dal.Base;
using Enterprise.Dal.Setting;
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Enterprise.Model.Entity;

namespace Enterprise.Dal.Collection
{
    /// <summary>
    /// Enterprise操作集
    /// </summary>
    public partial class UserRoleUserRoleGroupRelationDal : ExBaseDal<UserRoleUserRoleGroupRelation>
    {
		#region Fields

        private static readonly string ModelName;

        private static readonly string ModelXml;

        #endregion

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public UserRoleUserRoleGroupRelationDal(): this(Initialization.GetXmlConfig(typeof(UserRoleUserRoleGroupRelation)), ConstantCollection.ConfigFileType.ConfigContent)
        {

        }

	    /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static UserRoleUserRoleGroupRelationDal CreateDal()
        {
			return new UserRoleUserRoleGroupRelationDal(Initialization.GetXmlConfig(typeof(UserRoleUserRoleGroupRelation)),ConstantCollection.ConfigFileType.ConfigContent);
        }

        static UserRoleUserRoleGroupRelationDal()
        {
           ModelName= typeof(UserRoleUserRoleGroupRelation).Name;
           var item = new UserRoleUserRoleGroupRelation();
           ModelXml = item.CreateMappingXml();
        }

		/// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public UserRoleUserRoleGroupRelationDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
            EntityName = "UserRoleUserRoleGroupRelation";
			PrimarykeyName="Id";
        }

        #region Search By Filter
                
        #region UserRoleGroupId

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByUserRoleGroupId(long value)
        {
            var model = new UserRoleUserRoleGroupRelation { UserRoleGroupId = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleGroupId));
        }

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByUserRoleGroupId(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleGroupId));
        }

        #endregion

                
        #region UserRoleId

        /// <summary>
        /// 根据属性UserRoleId获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByUserRoleId(long value)
        {
            var model = new UserRoleUserRoleGroupRelation { UserRoleId = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleId));
        }

        /// <summary>
        /// 根据属性UserRoleId获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByUserRoleId(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleId));
        }

        #endregion

                
        #region Status

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByStatus(long value)
        {
            var model = new UserRoleUserRoleGroupRelation { Status = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.Status));
        }

        /// <summary>
        /// 根据属性Status获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByStatus(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.Status));
        }

        #endregion

                
        #region Ip

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByIp(string value)
        {
            var model = new UserRoleUserRoleGroupRelation { Ip = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.Ip));
        }

        /// <summary>
        /// 根据属性Ip获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByIp(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.Ip));
        }

        #endregion

                
        #region CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByCreateBy(long value)
        {
            var model = new UserRoleUserRoleGroupRelation { CreateBy = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateBy));
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByCreateBy(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateBy));
        }

        #endregion

                
        #region CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByCreateTime(DateTime value)
        {
            var model = new UserRoleUserRoleGroupRelation { CreateTime = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateTime));
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByCreateTime(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateTime));
        }

        #endregion

                
        #region LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByLastModifyBy(long value)
        {
            var model = new UserRoleUserRoleGroupRelation { LastModifyBy = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyBy));
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByLastModifyBy(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyBy));
        }

        #endregion

                
        #region LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByLastModifyTime(DateTime value)
        {
            var model = new UserRoleUserRoleGroupRelation { LastModifyTime = value };
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyTime));
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体
        /// </summary>
        public UserRoleUserRoleGroupRelation GetByLastModifyTime(UserRoleUserRoleGroupRelation model)
        {
            return GetByWhere(model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyTime));
        }

        #endregion

        #endregion

        #region GetList By Filter
                
        #region GetList By UserRoleGroupId

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByUserRoleGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { UserRoleGroupId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleGroupId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserRoleGroupId获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByUserRoleGroupId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleGroupId),sort,operateMode);
        }

        #endregion

                
        #region GetList By UserRoleId

        /// <summary>
        /// 根据属性UserRoleId获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByUserRoleId(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { UserRoleId = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleId),sort,operateMode);
        }

        /// <summary>
        /// 根据属性UserRoleId获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByUserRoleId(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.UserRoleId),sort,operateMode);
        }

        #endregion

                
        #region GetList By Status

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { Status = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.Status),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Status获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByStatus(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.Status),sort,operateMode);
        }

        #endregion

                
        #region GetList By Ip

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, string value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { Ip = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.Ip),sort,operateMode);
        }

        /// <summary>
        /// 根据属性Ip获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByIp(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.Ip),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateBy

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { CreateBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateBy获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByCreateBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By CreateTime

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { CreateTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性CreateTime获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByCreateTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.CreateTime),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyBy

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, long value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { LastModifyBy = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyBy),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyBy获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByLastModifyBy(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyBy),sort,operateMode);
        }

        #endregion

                
        #region GetList By LastModifyTime

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, DateTime value, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            var model = new UserRoleUserRoleGroupRelation { LastModifyTime = value };
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyTime),sort,operateMode);
        }

        /// <summary>
        /// 根据属性LastModifyTime获取数据实体列表
        /// </summary>
        public IList<UserRoleUserRoleGroupRelation> GetListByLastModifyTime(int currentPage, int pagesize, out long totalPages, out long totalRecords, UserRoleUserRoleGroupRelation model, string sort="Id desc", DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            return GetList(currentPage, pagesize, out totalPages, out totalRecords, model.GetFilterString(UserRoleUserRoleGroupRelationField.LastModifyTime),sort,operateMode);
        }

        #endregion

        #endregion
    }
}