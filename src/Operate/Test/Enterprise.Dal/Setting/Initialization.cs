﻿/*设置文件，手动修改需小心*/

using System;
using System.Configuration;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity;
using Operate.IO.ExtensionMethods;

namespace Enterprise.Dal.Setting
{
    /// <summary>
    /// 初始配置
    /// </summary>
    public class Initialization
    {
        #region properties

        /// <summary>
        /// 数据库类型
        /// </summary>
        public static SqliteVersion DatabaseVersion { get; set; }

        /// <summary>
        /// NHibernate配置文件内容
        /// </summary>
        public static string NHibernateConfigContent { get; set; }

        #endregion

        #region Constructor

        static Initialization()
        {
            NHibernateConfigContent = "";
        }

        #endregion

        #region Method

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public static string GetXmlConfig(Type type)
        {
            if (string.IsNullOrEmpty(NHibernateConfigContent))
            {
                var assembly = type.Assembly;
                var assemblyPath = assembly.CodeBase.ToLower().Replace(assembly.ManifestModule.Name.ToLower(), "");
                assemblyPath = assemblyPath.Replace("file:///", "");
                assemblyPath = assemblyPath.Replace("/", "\\");
                var hibernateCfgFilePath = assemblyPath + "Enterprise.hibernate.cfg.xml";
                string hibernateCfgContent;
                if (hibernateCfgFilePath.ExistFile())
                {
                    hibernateCfgContent = hibernateCfgFilePath.ReadFile();
                }
                else
                {
                    var connectionString = ConfigurationManager.AppSettings["EnterpriseConnectionStrings"];
                    if (connectionString != null)
                    {
                        var cfgEntity = new SqliteConfigEntity(connectionString, DatabaseVersion)
                        {
                            Assembly = "Enterprise.Model"
                        };
                        hibernateCfgContent = NibernateConfig.BuilderDefaultConfig(cfgEntity);
                    }
                    else
                    {
                        throw new Exception("没有找到配置文件，并且没有在Web.Config中找到配置项\"EnterpriseConnectionStrings\"");
                    }
                }

                NHibernateConfigContent = hibernateCfgContent;
            }

            return NHibernateConfigContent;
        }

        #endregion
    }
}
