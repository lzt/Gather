﻿/*扩展类文件，可以在此扩展类属性*/

using Enterprise.Model.Entity;
using Operate.ExtensionMethods;

// ReSharper disable once CheckNamespace
namespace Enterprise.Dal.Collection
{
    public partial class SiteUserDal 
    {
        public SiteUser GetByClientIdentifierAndClientSecretAndHost(string identifier, string secret,string host)
        {
            return GetByWhere(" ClientIdentifier='{0}' and ClientSecret='{1}' and HostName='{2}'".FormatValue(identifier, secret, host));
        }
    }
}
