﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using Operate.DataBase.SqlDatabase.NHibernateAssistant;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter;
using Enterprise.Dal.Setting;

namespace Enterprise.Dal.Common
{
    /// <summary>
    /// 一般操作数据访问层
    /// </summary>
    public class CommonDal : SqliteBaseDal
    {
        /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public CommonDal() : this(Initialization.GetXmlConfig(typeof(CommonDal)), ConstantCollection.ConfigFileType.ConfigContent)
        {
        }

        /// <summary>
        /// 创建对象
        /// </summary>
        /// <returns></returns>
        public static CommonDal CreateDal()
        {
            return new CommonDal(Initialization.GetXmlConfig(typeof(CommonDal)), ConstantCollection.ConfigFileType.ConfigContent);
        }

        /// <summary>
        /// 实例化新数据操作类
        /// </summary>
        public CommonDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            Session = NhibernateHelper.GetSession();
        }
    }
}
