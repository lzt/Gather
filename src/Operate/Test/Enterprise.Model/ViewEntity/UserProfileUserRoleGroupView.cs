﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.ViewEntity
{
    /// <summary>
    /// UserProfileUserRoleGroupView实体字段名称
    /// </summary>
    public enum UserProfileUserRoleGroupViewField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UserProfileId,  
        
        /// <summary>
        /// 字段：LoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LoginId,  
        
        /// <summary>
        /// 字段：Password
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Password,  
        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusEnabled,  
        
        /// <summary>
        /// 字段：NickName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        NickName,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：LastAccessTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastAccessTime,  
        
        /// <summary>
        /// 字段：Language
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Language,  
        
        /// <summary>
        /// 字段：UserType
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UserType,  
        
        /// <summary>
        /// 字段：ParentId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ParentId,  
        
        /// <summary>
        /// 字段：OutTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        OutTime,  
        
        /// <summary>
        /// 字段：Logins
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Logins,  
        
        /// <summary>
        /// 字段：RestrictIp
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        RestrictIp,  
        
        /// <summary>
        /// 字段：IpTactics
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IpTactics,  
        
        /// <summary>
        /// 字段：StatusMultiLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusMultiLogin,  
        
        /// <summary>
        /// 字段：LoginIp
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LoginIp,  
        
        /// <summary>
        /// 字段：LoginMac
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LoginMac,  
        
        /// <summary>
        /// 字段：TimephasedLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TimephasedLogin,  
        
        /// <summary>
        /// 字段：AllowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AllowLoginTimeBegin,  
        
        /// <summary>
        /// 字段：AllowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AllowLoginTimeEnd,  
        
        /// <summary>
        /// 字段：DisallowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DisallowLoginTimeBegin,  
        
        /// <summary>
        /// 字段：DisallowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DisallowLoginTimeEnd,  
        
        /// <summary>
        /// 字段：Phone
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Phone,  
        
        /// <summary>
        /// 字段：Email
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Email,  
        
        /// <summary>
        /// 字段：EmailVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        EmailVerification,  
        
        /// <summary>
        /// 字段：EmailVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        EmailVerifier,  
        
        /// <summary>
        /// 字段：EmailVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        EmailVerificationTime,  
        
        /// <summary>
        /// 字段：EmailDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        EmailDenyAuditReason,  
        
        /// <summary>
        /// 字段：Salt
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Salt,  
        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Remark,  
        
        /// <summary>
        /// 字段：Verification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Verification,  
        
        /// <summary>
        /// 字段：VerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        VerificationTime,  
        
        /// <summary>
        /// 字段：Verifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Verifier,  
        
        /// <summary>
        /// 字段：DenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DenyAuditReason,  
        
        /// <summary>
        /// 字段：TrueName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TrueName,  
        
        /// <summary>
        /// 字段：Avatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Avatar,  
        
        /// <summary>
        /// 字段：AvatarVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AvatarVerification,  
        
        /// <summary>
        /// 字段：AvatarVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AvatarVerifier,  
        
        /// <summary>
        /// 字段：AvatarVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AvatarVerificationTime,  
        
        /// <summary>
        /// 字段：AvatarDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AvatarDenyAuditReason,  
        
        /// <summary>
        /// 字段：Sex
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Sex,  
        
        /// <summary>
        /// 字段：Birthday
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Birthday,  
        
        /// <summary>
        /// 字段：AppId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AppId,  
        
        /// <summary>
        /// 字段：AppSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AppSecret,  
        
        /// <summary>
        /// 字段：CurrentScore
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CurrentScore,  
        
        /// <summary>
        /// 字段：TotalScore
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TotalScore,  
        
        /// <summary>
        /// 字段：TotalIncome
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TotalIncome,  
        
        /// <summary>
        /// 字段：AvailableIncome
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AvailableIncome,  
        
        /// <summary>
        /// 字段：Paid
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Paid,  
        
        /// <summary>
        /// 字段：Zipcode
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Zipcode,  
        
        /// <summary>
        /// 字段：Address
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Address,  
        
        /// <summary>
        /// 字段：ProvinceId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ProvinceId,  
        
        /// <summary>
        /// 字段：CityId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CityId,  
        
        /// <summary>
        /// 字段：TownId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TownId,  
        
        /// <summary>
        /// 字段：Identification
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Identification,  
        
        /// <summary>
        /// 字段：IdentificationImage
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IdentificationImage,  
        
        /// <summary>
        /// 字段：IdentificationVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IdentificationVerification,  
        
        /// <summary>
        /// 字段：IdentificationVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IdentificationVerifier,  
        
        /// <summary>
        /// 字段：IdentificationVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IdentificationVerificationTime,  
        
        /// <summary>
        /// 字段：IdentificationDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IdentificationDenyAuditReason,  
        
        /// <summary>
        /// 字段：DriverLicense
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DriverLicense,  
        
        /// <summary>
        /// 字段：DriverLicenseImage
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DriverLicenseImage,  
        
        /// <summary>
        /// 字段：DriverLicenseVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DriverLicenseVerification,  
        
        /// <summary>
        /// 字段：DriverLicenseVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DriverLicenseVerifier,  
        
        /// <summary>
        /// 字段：DriverLicenseVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DriverLicenseVerificationTime,  
        
        /// <summary>
        /// 字段：DriverLicenseDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DriverLicenseDenyAuditReason,  
        
        /// <summary>
        /// 字段：CreditCard
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreditCard,  
        
        /// <summary>
        /// 字段：CreditCardImage
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreditCardImage,  
        
        /// <summary>
        /// 字段：CreditCardVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreditCardVerification,  
        
        /// <summary>
        /// 字段：CreditCardVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreditCardVerifier,  
        
        /// <summary>
        /// 字段：CreditCardVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreditCardVerificationTime,  
        
        /// <summary>
        /// 字段：CreditCardDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreditCardDenyAuditReason,  
        
        /// <summary>
        /// 字段：FrozenFunds
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        FrozenFunds,  
        
        /// <summary>
        /// 字段：GiftFunds
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        GiftFunds,  
        
        /// <summary>
        /// 字段：ThawTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ThawTime,  
        
        /// <summary>
        /// 字段：FrozenTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        FrozenTime,  
        
        /// <summary>
        /// 字段：QQ
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        QQ,  
        
        /// <summary>
        /// 字段：EmergencyPhone
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        EmergencyPhone,  
        
        /// <summary>
        /// 字段：Grade
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Grade,  
        
        /// <summary>
        /// 字段：AvailableRecharge
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AvailableRecharge,  
        
        /// <summary>
        /// 字段：TotalRecharge
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TotalRecharge,  
        
        /// <summary>
        /// 字段：DiscountCardMode
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DiscountCardMode,  
        
        /// <summary>
        /// 字段：DiscountCardDenomination
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DiscountCardDenomination,  
        
        /// <summary>
        /// 字段：StatusInvalid
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusInvalid,  
        
        /// <summary>
        /// 字段：InviterId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        InviterId,  
        
        /// <summary>
        /// 字段：GroupId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        GroupId,  
        
        /// <summary>
        /// 字段：GroupTitle
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        GroupTitle,  
        
        /// <summary>
        /// 字段：GroupRoleList
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        GroupRoleList,  
        
        /// <summary>
        /// 字段：GroupParentId
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        GroupParentId,  
        
        /// <summary>
        /// 字段：GroupPurpose
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        GroupPurpose,  
        
    }

    /// <summary>
    /// UserProfileUserRoleGroupView实体模型
    /// </summary>
    [Serializable]
    public partial class UserProfileUserRoleGroupView : ExBaseEntity<UserProfileUserRoleGroupView>
	{
        #region UserProfileUserRoleGroupView Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        [Key]
                public virtual int Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        public virtual int UserProfileId {get; set;} 
                        
        /// <summary>
        /// 字段：LoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string LoginId {get; set;} 
                        
        /// <summary>
        /// 字段：Password
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string Password {get; set;} 
                        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusEnabled {get; set;} 
                        
        /// <summary>
        /// 字段：NickName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string NickName {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：LastAccessTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime LastAccessTime {get; set;} 
                        
        /// <summary>
        /// 字段：Language
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string Language {get; set;} 
                        
        /// <summary>
        /// 字段：UserType
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int UserType {get; set;} 
                        
        /// <summary>
        /// 字段：ParentId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int ParentId {get; set;} 
                        
        /// <summary>
        /// 字段：OutTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime OutTime {get; set;} 
                        
        /// <summary>
        /// 字段：Logins
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Logins {get; set;} 
                        
        /// <summary>
        /// 字段：RestrictIp
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int RestrictIp {get; set;} 
                        
        /// <summary>
        /// 字段：IpTactics
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(1000)")]
        [Precision("")]
        [Scale("")]
        public virtual string IpTactics {get; set;} 
                        
        /// <summary>
        /// 字段：StatusMultiLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusMultiLogin {get; set;} 
                        
        /// <summary>
        /// 字段：LoginIp
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string LoginIp {get; set;} 
                        
        /// <summary>
        /// 字段：LoginMac
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string LoginMac {get; set;} 
                        
        /// <summary>
        /// 字段：TimephasedLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TimephasedLogin {get; set;} 
                        
        /// <summary>
        /// 字段：AllowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime AllowLoginTimeBegin {get; set;} 
                        
        /// <summary>
        /// 字段：AllowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime AllowLoginTimeEnd {get; set;} 
                        
        /// <summary>
        /// 字段：DisallowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime DisallowLoginTimeBegin {get; set;} 
                        
        /// <summary>
        /// 字段：DisallowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime DisallowLoginTimeEnd {get; set;} 
                        
        /// <summary>
        /// 字段：Phone
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string Phone {get; set;} 
                        
        /// <summary>
        /// 字段：Email
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string Email {get; set;} 
                        
        /// <summary>
        /// 字段：EmailVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int EmailVerification {get; set;} 
                        
        /// <summary>
        /// 字段：EmailVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int EmailVerifier {get; set;} 
                        
        /// <summary>
        /// 字段：EmailVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime EmailVerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：EmailDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string EmailDenyAuditReason {get; set;} 
                        
        /// <summary>
        /// 字段：Salt
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Salt {get; set;} 
                        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(2000)")]
        [Precision("")]
        [Scale("")]
        public virtual string Remark {get; set;} 
                        
        /// <summary>
        /// 字段：Verification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Verification {get; set;} 
                        
        /// <summary>
        /// 字段：VerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime VerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：Verifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Verifier {get; set;} 
                        
        /// <summary>
        /// 字段：DenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string DenyAuditReason {get; set;} 
                        
        /// <summary>
        /// 字段：TrueName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string TrueName {get; set;} 
                        
        /// <summary>
        /// 字段：Avatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string Avatar {get; set;} 
                        
        /// <summary>
        /// 字段：AvatarVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int AvatarVerification {get; set;} 
                        
        /// <summary>
        /// 字段：AvatarVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int AvatarVerifier {get; set;} 
                        
        /// <summary>
        /// 字段：AvatarVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime AvatarVerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：AvatarDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string AvatarDenyAuditReason {get; set;} 
                        
        /// <summary>
        /// 字段：Sex
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Sex {get; set;} 
                        
        /// <summary>
        /// 字段：Birthday
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime Birthday {get; set;} 
                        
        /// <summary>
        /// 字段：AppId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string AppId {get; set;} 
                        
        /// <summary>
        /// 字段：AppSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string AppSecret {get; set;} 
                        
        /// <summary>
        /// 字段：CurrentScore
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int CurrentScore {get; set;} 
                        
        /// <summary>
        /// 字段：TotalScore
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TotalScore {get; set;} 
                        
        /// <summary>
        /// 字段：TotalIncome
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TotalIncome {get; set;} 
                        
        /// <summary>
        /// 字段：AvailableIncome
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int AvailableIncome {get; set;} 
                        
        /// <summary>
        /// 字段：Paid
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Paid {get; set;} 
                        
        /// <summary>
        /// 字段：Zipcode
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual string Zipcode {get; set;} 
                        
        /// <summary>
        /// 字段：Address
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string Address {get; set;} 
                        
        /// <summary>
        /// 字段：ProvinceId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int ProvinceId {get; set;} 
                        
        /// <summary>
        /// 字段：CityId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int CityId {get; set;} 
                        
        /// <summary>
        /// 字段：TownId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TownId {get; set;} 
                        
        /// <summary>
        /// 字段：Identification
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Identification {get; set;} 
                        
        /// <summary>
        /// 字段：IdentificationImage
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string IdentificationImage {get; set;} 
                        
        /// <summary>
        /// 字段：IdentificationVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int IdentificationVerification {get; set;} 
                        
        /// <summary>
        /// 字段：IdentificationVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int IdentificationVerifier {get; set;} 
                        
        /// <summary>
        /// 字段：IdentificationVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime IdentificationVerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：IdentificationDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string IdentificationDenyAuditReason {get; set;} 
                        
        /// <summary>
        /// 字段：DriverLicense
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string DriverLicense {get; set;} 
                        
        /// <summary>
        /// 字段：DriverLicenseImage
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string DriverLicenseImage {get; set;} 
                        
        /// <summary>
        /// 字段：DriverLicenseVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int DriverLicenseVerification {get; set;} 
                        
        /// <summary>
        /// 字段：DriverLicenseVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int DriverLicenseVerifier {get; set;} 
                        
        /// <summary>
        /// 字段：DriverLicenseVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime DriverLicenseVerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：DriverLicenseDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string DriverLicenseDenyAuditReason {get; set;} 
                        
        /// <summary>
        /// 字段：CreditCard
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string CreditCard {get; set;} 
                        
        /// <summary>
        /// 字段：CreditCardImage
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string CreditCardImage {get; set;} 
                        
        /// <summary>
        /// 字段：CreditCardVerification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int CreditCardVerification {get; set;} 
                        
        /// <summary>
        /// 字段：CreditCardVerifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int CreditCardVerifier {get; set;} 
                        
        /// <summary>
        /// 字段：CreditCardVerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreditCardVerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：CreditCardDenyAuditReason
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string CreditCardDenyAuditReason {get; set;} 
                        
        /// <summary>
        /// 字段：FrozenFunds
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("float(8)")]
        [Precision("")]
        [Scale("")]
        public virtual double FrozenFunds {get; set;} 
                        
        /// <summary>
        /// 字段：GiftFunds
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("float(8)")]
        [Precision("")]
        [Scale("")]
        public virtual double GiftFunds {get; set;} 
                        
        /// <summary>
        /// 字段：ThawTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime ThawTime {get; set;} 
                        
        /// <summary>
        /// 字段：FrozenTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime FrozenTime {get; set;} 
                        
        /// <summary>
        /// 字段：QQ
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string QQ {get; set;} 
                        
        /// <summary>
        /// 字段：EmergencyPhone
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string EmergencyPhone {get; set;} 
                        
        /// <summary>
        /// 字段：Grade
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Grade {get; set;} 
                        
        /// <summary>
        /// 字段：AvailableRecharge
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int AvailableRecharge {get; set;} 
                        
        /// <summary>
        /// 字段：TotalRecharge
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TotalRecharge {get; set;} 
                        
        /// <summary>
        /// 字段：DiscountCardMode
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int DiscountCardMode {get; set;} 
                        
        /// <summary>
        /// 字段：DiscountCardDenomination
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int DiscountCardDenomination {get; set;} 
                        
        /// <summary>
        /// 字段：StatusInvalid
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusInvalid {get; set;} 
                        
        /// <summary>
        /// 字段：InviterId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int InviterId {get; set;} 
                        
        /// <summary>
        /// 字段：GroupId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        public virtual int GroupId {get; set;} 
                        
        /// <summary>
        /// 字段：GroupTitle
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string GroupTitle {get; set;} 
                        
        /// <summary>
        /// 字段：GroupRoleList
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string GroupRoleList {get; set;} 
                        
        /// <summary>
        /// 字段：GroupParentId
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long GroupParentId {get; set;} 
                        
        /// <summary>
        /// 字段：GroupPurpose
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int GroupPurpose {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static UserProfileUserRoleGroupView()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public UserProfileUserRoleGroupView()
        {
            //属性初始化
            Id = 0;
            UserProfileId = 0;
            LoginId = "";
            Password = "";
            StatusEnabled = 0;
            NickName = "";
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LastAccessTime = ConstantCollection.DbDefaultDateTime;
            Language = "";
            UserType = 0;
            ParentId = 0;
            OutTime = ConstantCollection.DbDefaultDateTime;
            Logins = 0;
            RestrictIp = 0;
            IpTactics = "";
            StatusMultiLogin = 0;
            LoginIp = "";
            LoginMac = "";
            TimephasedLogin = 0;
            AllowLoginTimeBegin = ConstantCollection.DbDefaultDateTime;
            AllowLoginTimeEnd = ConstantCollection.DbDefaultDateTime;
            DisallowLoginTimeBegin = ConstantCollection.DbDefaultDateTime;
            DisallowLoginTimeEnd = ConstantCollection.DbDefaultDateTime;
            Phone = "";
            Email = "";
            EmailVerification = 0;
            EmailVerifier = 0;
            EmailVerificationTime = ConstantCollection.DbDefaultDateTime;
            EmailDenyAuditReason = "";
            Salt = "";
            Remark = "";
            Verification = 0;
            VerificationTime = ConstantCollection.DbDefaultDateTime;
            Verifier = 0;
            DenyAuditReason = "";
            TrueName = "";
            Avatar = "";
            AvatarVerification = 0;
            AvatarVerifier = 0;
            AvatarVerificationTime = ConstantCollection.DbDefaultDateTime;
            AvatarDenyAuditReason = "";
            Sex = 0;
            Birthday = ConstantCollection.DbDefaultDateTime;
            AppId = "";
            AppSecret = "";
            CurrentScore = 0;
            TotalScore = 0;
            TotalIncome = 0;
            AvailableIncome = 0;
            Paid = 0;
            Zipcode = "";
            Address = "";
            ProvinceId = 0;
            CityId = 0;
            TownId = 0;
            Identification = "";
            IdentificationImage = "";
            IdentificationVerification = 0;
            IdentificationVerifier = 0;
            IdentificationVerificationTime = ConstantCollection.DbDefaultDateTime;
            IdentificationDenyAuditReason = "";
            DriverLicense = "";
            DriverLicenseImage = "";
            DriverLicenseVerification = 0;
            DriverLicenseVerifier = 0;
            DriverLicenseVerificationTime = ConstantCollection.DbDefaultDateTime;
            DriverLicenseDenyAuditReason = "";
            CreditCard = "";
            CreditCardImage = "";
            CreditCardVerification = 0;
            CreditCardVerifier = 0;
            CreditCardVerificationTime = ConstantCollection.DbDefaultDateTime;
            CreditCardDenyAuditReason = "";
            FrozenFunds = 0;
            GiftFunds = 0;
            ThawTime = ConstantCollection.DbDefaultDateTime;
            FrozenTime = ConstantCollection.DbDefaultDateTime;
            QQ = "";
            EmergencyPhone = "";
            Grade = 0;
            AvailableRecharge = 0;
            TotalRecharge = 0;
            DiscountCardMode = 0;
            DiscountCardDenomination = 0;
            StatusInvalid = 0;
            InviterId = 0;
            GroupId = 0;
            GroupTitle = "";
            GroupRoleList = "";
            GroupParentId = 0;
            GroupPurpose = 0;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params UserProfileUserRoleGroupViewField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}