﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;
using Xunit;

namespace UnitTests.Reflection.Emit
{
    public class MethodBuilder
    {
        [Fact]
        public void Create()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            Assert.NotNull(Method);
        }

        [Fact]
        public void Add()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1=Method.CreateLocal("Local1",typeof(int));
            VariableBase Local2=Method.CreateLocal("Local2",typeof(int));
            Assert.NotNull(Method.Add(Local1, Local2));
        }

        [Fact]
        public void Assign()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.DoesNotThrow(() => Method.Assign(Local1, Local2));
        }

        [Fact]
        public void Box()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.Box(Local1));
        }

        [Fact]
        public void Call()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            IMethodBuilder Method2 = TestType.CreateMethod("TestMethod2");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.DoesNotThrow(() => Method.This.Call(Method2, null));
        }

        [Fact]
        public void Cast()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.DoesNotThrow(() => Method.Cast(Method.This, typeof(object)));
        }

        [Fact]
        public void TryCatch()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Operate.Reflection.Emit.Commands.Try Try = Method.Try();
            Operate.Reflection.Emit.Commands.Catch Catch = Method.Catch(typeof(Exception));
            Method.EndTry();
            Assert.NotNull(Try);
            Assert.NotNull(Catch);
        }

        [Fact]
        public void CreateConstant()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.CreateConstant(12));
        }

        [Fact]
        public void CreateLocal()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Local1);
            Assert.NotNull(Local2);
        }

        [Fact]
        public void Divide()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.Divide(Local1, Local2));
        }

        [Fact]
        public void If()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Operate.Reflection.Emit.Commands.If If = Method.If(Local1, Operate.Reflection.Emit.Enums.Comparison.Equal, Local2);
            Method.EndIf(If);
            Assert.NotNull(If);
        }

        [Fact]
        public void Modulo()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.Modulo(Local1, Local2));
        }

        [Fact]
        public void Multiply()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.Multiply(Local1, Local2));
        }

        [Fact]
        public void NewObj()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.NewObj(typeof(DateTime), new object[] { Local1 }));
        }

        [Fact]
        public void Return()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod", returnType: typeof(int));
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.DoesNotThrow(() => Method.Return(Local1));
        }

        [Fact]
        public void Subtract()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.Subtract(Local1, Local2));
        }

        [Fact]
        public void Throw()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.DoesNotThrow(() => Method.Throw(Method.NewObj(typeof(Exception))));
        }

        [Fact]
        public void UnBox()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Assert.NotNull(Method.UnBox(Method.Box(Local1), typeof(int)));
        }

        [Fact]
        public void While()
        {
            Operate.Reflection.Emit.Assembly Assembly = new Operate.Reflection.Emit.Assembly("TestAssembly");
            Operate.Reflection.Emit.TypeBuilder TestType = Assembly.CreateType("TestType");
            IMethodBuilder Method = TestType.CreateMethod("TestMethod");
            VariableBase Local1 = Method.CreateLocal("Local1", typeof(int));
            VariableBase Local2 = Method.CreateLocal("Local2", typeof(int));
            Operate.Reflection.Emit.Commands.While While = Method.While(Local1, Operate.Reflection.Emit.Enums.Comparison.Equal, Local2);
            Method.EndWhile(While);
            Assert.NotNull(While);
        }
    }
}
