﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.IO.ExtensionMethods;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Operate.IO.ExtensionMethods.Test
{
    [TestClass()]
    public class StreamExtensionsTests
    {
        [TestMethod()]
        public void CopyTest()
        {
            //var sdsd = Convert.ToInt32(7777777777777);
            var memoryStream = new MemoryStream();
            using (var fs = new FileStream("c://Penguins.jpg",FileMode.Open,FileAccess.Read))
            {
              fs.CopyToStream(memoryStream,400000);
            }
            var img = new System.Drawing.Bitmap(memoryStream);

            img.Save("c://testcopy.jpg");

            Assert.Fail();
        }

        [TestMethod()]
        public void CopyTest1()
        {
            Assert.Fail();
        }
    }
}
