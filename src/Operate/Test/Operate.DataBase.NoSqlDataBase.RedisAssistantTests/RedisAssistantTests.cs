﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operate.DataBase.NoSqlDataBase.RedisAssistant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Operate.DataBase.NoSqlDataBase.Cache.ContainerCacheStragegy;
using Operate.DataBase.NoSqlDataBase.RedisAssistant.StactkExchangeRedis.ContainerCacheStragegy;

namespace Operate.DataBase.NoSqlDataBase.RedisAssistant.Tests
{
    [TestClass()]
    public class RedisAssistantTests
    {
        [TestMethod()]
        public void ConnectServerTest()
        {
            //var c = new RedisAssistant("192.168.1.252:6379" );
            //var sd = c.Get<object>("HostCache_AllHost");

            // //RedisAssistant.Host = "192.168.1.252";
            // //RedisAssistant.Port = 6379;
            // //RedisAssistant.ConnectServer();

            // var c = new RedisAssistant("192.168.1.252", 6379);
            // var listid = "TestList";
            // c.AddItemToList(listid,"1");
            // var r = c.GetAllItemsFromList(listid);
            // c.RemoveAllFromList(listid);
            //r = c.GetAllItemsFromList(listid);
            // if (r.Count==1)
            // {
            //     c.RemoveItemFromList(listid,r[0]);
            //     r = c.GetAllItemsFromList(listid);
            // }

            // //c.Get<string>("aa");

            // //var sds = RedisAssistant.Get<string>("CodeCache_SDRzSUFBQUFBQUFFQUhNTUR3MzN6TFF3Q2pRb2lZbzB5akJLOHd3UHppdE1kZGEyOENsekxNaE85eXMwZEM0SVNjczNMMHRNTlFzc1RZcHc5VXF0U0RJemRncjB6OGdLTHFsTXp0ZE9MRTl4eWZQTHRUUjExemUyREFxc1RDb29DclVGQUdXZU03ZFlBQUFB");
            // //RedisAssistant.Set("aa", "aavlaue");
            // //var r = RedisAssistant.Get<string>("aa");

        }

        private RedisContainerCacheStrategy cache;
        public RedisAssistantTests()
        {
            cache = RedisContainerCacheStrategy.Instance;
        }

        /// <summary>
        /// 单例测试
        /// </summary>
        [TestMethod]
        public void SingletonTest()
        {
            var cache2 = RedisContainerCacheStrategy.Instance;
            Assert.AreEqual(cache.GetHashCode(), cache2.GetHashCode());
        }

        /// <summary>
        /// 插入测试
        /// </summary>
        [TestMethod]
        public void InsertToCacheTest()
        {
            var key = Guid.NewGuid().ToString();
            var count = cache.GetCount();
            cache.InsertToCache(key, new ContainerItemCollection()
            {

            });

            var item = cache.Get(key);
            Assert.IsNotNull(item);

            Console.WriteLine(item.CacheSetKey);
            Console.WriteLine(item.CreateTime);

            var count2 = cache.GetCount();
            Assert.AreEqual(count + 1, count2);
        }

        /// <summary>
        /// 删除测试
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            //CacheStrategyFactory.RegisterContainerCacheStrategy(() => RedisContainerCacheStrategy.Instance);//Redis

            var key = Guid.NewGuid().ToString();
            var count = cache.GetCount();
            cache.InsertToCache(key, new ContainerItemCollection()
            {

            });

            var item = cache.Get(key);
            Assert.IsNotNull(item);
            var count2 = cache.GetCount();
            Assert.AreEqual(count + 1, count2);

            cache.RemoveFromCache(key);
            item = cache.Get(key);
            Assert.IsNull(item);
            var count3 = cache.GetCount();
            Assert.AreEqual(count, count3);
        }
    }
}
