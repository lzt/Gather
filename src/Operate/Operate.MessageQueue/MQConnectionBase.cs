﻿namespace Operate.MessageQueue
{
    public abstract class MQConnectionBase
    {
        public const int DefaultMaxThread = 5;

        #region Properties

        public ThreadMode ThreadMode { get; set; }

        public int MaxThread { get; set; }

        public bool UseLogger { get; set; }

        #endregion
    }
}
