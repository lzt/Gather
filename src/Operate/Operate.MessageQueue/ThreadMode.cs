﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Operate.MessageQueue
{
    /// <summary>
    /// 线程模式
    /// </summary>
    public enum ThreadMode
    {
        /// <summary>
        /// 单线程模式
        /// </summary>
        [Description("单线程模式")]
        SingleThread = 1,

        /// <summary>
        /// 线程池模式
        /// </summary>
        [Description("线程池模式")]
        ThreadPool = 2
    }
}
