namespace Operate.MessageQueue
{
    public interface IMessageSender
    {
        void Send(string message);
    }
}