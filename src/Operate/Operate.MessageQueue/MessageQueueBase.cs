﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Operate.Log;
using Operate.Log.log4net;
using Operate.Threading;

namespace Operate.MessageQueue
{
    public abstract class MessageQueueBase
    {
        protected  OperateTaskScheduler TaskScheduler;
        protected  List<Task> TaskList;
        private ILogger _logger = new Logger();

        /// <summary>
        /// Logger
        /// </summary>
        protected virtual ILogger Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }

        protected MessageQueueBase()
        {
            TaskList = new List<Task>();
            TaskScheduler = null;
        }

        protected abstract bool GetUserLogger();

        protected void TaskExec<T>(Action<T> actionT, T arg)
        {
            CancellationTokenSource tks = new CancellationTokenSource();
            CancellationToken token = tks.Token;
            try
            {
                var action = ConvertCollection.ConvertAction(actionT);
                var task = new Task(o => action(arg), token);
                TaskList.Add(task);
                task.Start(TaskScheduler);
            }
            catch (Exception ex)
            {
                tks.Cancel();
                if (GetUserLogger())
                {
                    Logger.RecordException(ex);
                }
            }
        }
    }
}
