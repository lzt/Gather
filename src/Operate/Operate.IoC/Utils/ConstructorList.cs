﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Reflection;
using Operate.IoC.Mappings;

#endregion

namespace Operate.IoC.Utils
{
    /// <summary>
    /// Utility functions that deal with constructors for an object
    /// </summary>
    public static class ConstructorList
    {
        /// <summary>
        /// Chooses a constructor
        /// </summary>
        /// <param name="implementationType">Type of the class</param>
        /// <param name="mappingManager">Mapping manager</param>
        /// <returns>The most appropriate constructor</returns>
        public static ConstructorInfo ChooseConstructor(Type implementationType, MappingManager mappingManager)
        {
            ConstructorInfo[] constructors = implementationType.GetConstructors();
            int maxValue = int.MinValue;
            ConstructorInfo currentConstructor = null;
            foreach (ConstructorInfo constructor in constructors)
            {
                int count = GetParameterCount(constructor, mappingManager);
                if (count > maxValue)
                {
                    currentConstructor = constructor;
                    maxValue = count;
                }
            }
            return currentConstructor;
        }

        /// <summary>
        /// Gets the number of parameters that the system recognizes
        /// </summary>
        /// <param name="constructor">Constructor to check</param>
        /// <param name="mappingManager">Mapping manager</param>
        /// <returns>The number of parameters that it has knoweledge of</returns>
        private static int GetParameterCount(ConstructorInfo constructor, MappingManager mappingManager)
        {
            int count = 0;
            ParameterInfo[] parameters = constructor.GetParameters();
            foreach (ParameterInfo parameter in parameters)
            {
                bool inject = true;
                object[] attributes = parameter.GetCustomAttributes(false);
                if (attributes.Length > 0)
                {
                    // ReSharper disable LoopCanBeConvertedToQuery
                    foreach (Attribute attribute in attributes)
                    // ReSharper restore LoopCanBeConvertedToQuery
                    {
                        if (mappingManager.GetMapping(parameter.ParameterType, attribute.GetType()) != null)
                        {
                            ++count;
                            inject = false;
                            break;
                        }
                    }
                }
                if (inject)
                {
                    if (mappingManager.GetMapping(parameter.ParameterType) != null)
                        ++count;
                }
            }
            if (count == parameters.Length)
                return count;
            return int.MinValue;
        }
    }
}
