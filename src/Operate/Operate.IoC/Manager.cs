﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Operate.ExtensionMethods;
using Operate.IoC.Mappings;
using Operate.IoC.Mappings.Interfaces;
using Operate.IoC.Providers;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.IoC
{
    /// <summary>
    /// Manager class
    /// </summary>
    public class Manager
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Manager()
        {
            if (ProviderManager==null)
                ProviderManager = new ProviderManager();
            if (MappingManager==null)
                MappingManager = new MappingManager(ProviderManager);
        }

        #endregion

        #region Functions

        /// <summary>
        /// Loads all mapping modules found within the assembly
        /// </summary>
        /// <param name="moduleAssembly">Module assembly</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public void Setup(Assembly moduleAssembly)
        {
            ProviderManager.Setup(moduleAssembly);
            MappingManager.Setup(moduleAssembly);
        }

        /// <summary>
        /// Loads all mapping modules found within the assemblies
        /// </summary>
        /// <param name="moduleAssemblies">Module assemblies</param>
        public void Setup(IEnumerable<Assembly> moduleAssemblies)
        {
            moduleAssemblies.ForEach(Setup);
        }

        /// <summary>
        /// Loads all mapping modules found within a specific directory
        /// </summary>
        /// <param name="directory">Directory to scan for modules</param>
        /// <param name="scanSubDirectories">Determines if sub directories should be scanned</param>
        public void Setup(string directory, bool scanSubDirectories = true)
        {
            Setup(new DirectoryInfo(directory).LoadAssemblies(scanSubDirectories));
        }

        /// <summary>
        /// Creates an object of the specified type
        /// </summary>
        /// <typeparam name="TServiceType">Service type</typeparam>
        /// <returns>An object of the specified type</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public TServiceType Get<TServiceType>()
        {
            return (TServiceType)Get(typeof(TServiceType));
        }

        /// <summary>
        /// Creates an object of the specified type associated with the attribute type
        /// </summary>
        /// <typeparam name="TServiceType">Service type</typeparam>
        /// <typeparam name="TAttributeType">Attribute type</typeparam>
        /// <returns>An object of the specified type</returns>
        public TServiceType Get<TServiceType, TAttributeType>()
        {
            return (TServiceType)Get(typeof(TServiceType), typeof(TAttributeType));
        }

        /// <summary>
        /// Creates an object of the specified type
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <returns>An object of the specified type</returns>
        public static object Get(Type serviceType)
        {
            IMapping mapping = MappingManager.GetMapping(serviceType);
            if (mapping==null)
                throw new ArgumentException("ServiceType not found in mappings");
            return mapping.Implementation.Create();
        }

        /// <summary>
        /// Creates an object of the specified type associated with the attribute type
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <param name="attributeType">Attribute type</param>
        /// <returns>An object of the specified type</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public object Get(Type serviceType, Type attributeType)
        {
            IMapping mapping = MappingManager.GetMapping(serviceType, attributeType);
            if (mapping==null)
                throw new ArgumentException("ServiceType not found in mappings");
            return mapping.Implementation.Create();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Provider manager
        /// </summary>
        protected static ProviderManager ProviderManager { get; set; }

        /// <summary>
        /// Mapping manager
        /// </summary>
        protected static MappingManager MappingManager { get; set; }

        #endregion
    }
}