﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Operate.ExtensionMethods;
using Operate.IoC.Mappings.BaseClasses;
using Operate.IoC.Mappings.Interfaces;
using Operate.IoC.Mappings.Internal_Classes;
using Operate.IoC.Providers;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.IoC.Mappings
{
    /// <summary>
    /// Mapping manager
    /// </summary>
    public class MappingManager
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MappingManager(ProviderManager providerManager)
        {
            Modules = new List<IModule>();
            Mappings = new List<IMapping>();
            ProviderManager = providerManager;
        }

        #endregion

        #region Functions

        /// <summary>
        /// Scans the assembly for mapping modules and creates them
        /// </summary>
        /// <param name="moduleAssembly"></param>
        public void Setup(Assembly moduleAssembly)
        {
            IEnumerable<Type> modules = moduleAssembly.Types(typeof(IModule));
            var tempModules = new List<IModule>();
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Type module in modules)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (!module.IsInterface && !module.IsAbstract)
                    tempModules.Add((IModule)module.Assembly.CreateInstance(module.FullName));
            }
            foreach (IModule module in tempModules)
            {
                module.Manager = this;
                module.Setup();
            }
            Modules.Add(tempModules);
        }

        /// <summary>
        /// Creates a mapping object
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <returns>a mapping object</returns>
        public IMapping CreateMapping(Type serviceType)
        {
            IMapping mapping = new Mapping(serviceType, ProviderManager, this);
            Mappings.Add(mapping);
            return mapping;
        }

        /// <summary>
        /// Creates a mapping object
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <param name="attributeType">Attribute type</param>
        /// <returns>A mapping object</returns>
        public IMapping CreateMapping(Type serviceType, Type attributeType)
        {
            IMapping mapping = new Mapping(serviceType, attributeType, ProviderManager, this);
            Mappings.Add(mapping);
            return mapping;
        }

        /// <summary>
        /// Gets the mapping that matches this service type
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <returns>The mapping associated with this service type</returns>
        public IMapping GetMapping(Type serviceType)
        {
            var key = new MappingKey(serviceType, null, ProviderManager, this);
            return Mappings.FirstOrDefault(x => x.Equals(key));
        }

        /// <summary>
        /// Gets the mapping that matches this service type and attribute type
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <param name="attributeType">Attribute type</param>
        /// <returns>The mapping associated with this service type and attribute type</returns>
        public IMapping GetMapping(Type serviceType, Type attributeType)
        {
            var key = new MappingKey(serviceType, attributeType, ProviderManager, this);
            return Mappings.FirstOrDefault(x => x.Equals(key));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Modules listing
        /// </summary>
        protected ICollection<IModule> Modules { get; private set; }

        /// <summary>
        /// Mapping listing
        /// </summary>
        protected ICollection<IMapping> Mappings { get; private set; }

        /// <summary>
        /// Provider manager
        /// </summary>
        protected ProviderManager ProviderManager { get; set; }

        #endregion
    }
}
