﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.IoC.Mappings.Interfaces;
using Operate.IoC.Providers;
using Operate.IoC.Providers.Interfaces;
using Operate.IoC.Providers.Scope;

#endregion

namespace Operate.IoC.Mappings.BaseClasses
{
    /// <summary>
    /// Base mapping
    /// </summary>
    public class BaseMapping : IMapping
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <param name="attributeType">Attribute type</param>
        /// <param name="mappingManager">Mapping manager</param>
        /// <param name="providerManager">Provider manager</param>
        public BaseMapping(Type serviceType, Type attributeType, ProviderManager providerManager, MappingManager mappingManager)
        {
            ServiceType = serviceType;
            AttributeType = attributeType;
            ProviderManager = providerManager;
            MappingManager = mappingManager;
            Scope = new StandardScope();
        }

        #endregion

        #region Functions

        /// <summary>
        /// Maps an item to an implementation type
        /// </summary>
        /// <typeparam name="TImplementationType">implementation type</typeparam>
        /// <returns>The mapping object</returns>
        public IMapping To<TImplementationType>()
        {
            return To(typeof(TImplementationType));
        }

        /// <summary>
        /// Maps an item to an implementation type
        /// </summary>
        /// <param name="implementationType">implementation type</param>
        /// <returns>The mapping object</returns>
        public IMapping To(Type implementationType)
        {
            Implementation = ProviderManager.GetProvider(Scope).CreateImplementation(implementationType, MappingManager);
            return this;
        }

        /// <summary>
        /// Maps an item to an implementation function
        /// </summary>
        /// <typeparam name="TImplementationType">implementation type</typeparam>
        /// <param name="implementation">implementation function</param>
        /// <returns>The mapping object</returns>
        public IMapping To<TImplementationType>(Func<TImplementationType> implementation)
        {
            Implementation = ProviderManager.GetProvider(Scope).CreateImplementation(implementation);
            return this;
        }

        /// <summary>
        /// Maps an item to an implementation class
        /// </summary>
        /// <param name="implementation">implementation class</param>
        /// <returns>The mapping object</returns>
        public IMapping To(IImplementation implementation)
        {
            Implementation = implementation;
            return this;
        }

        /// <summary>
        /// Sets the scope for the mapping
        /// </summary>
        /// <param name="scope">The scope to use</param>
        /// <returns>Mapping object</returns>
        public IMapping SetScope(BaseScope scope)
        {
            Scope = scope;
            Implementation = ProviderManager.GetProvider(scope).CreateImplementation(Implementation, MappingManager);
            return this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Service type
        /// </summary>
        public Type ServiceType { get; protected set; }

        /// <summary>
        /// Attribute type
        /// </summary>
        public Type AttributeType { get; protected set; }

        /// <summary>
        /// Scope
        /// </summary>
        public BaseScope Scope { get; protected set; }

        /// <summary>
        /// implementation
        /// </summary>
        public IImplementation Implementation { get; protected set; }

        /// <summary>
        /// Provider manager
        /// </summary>
        private ProviderManager ProviderManager { get; set; }

        /// <summary>
        /// Mapping manager
        /// </summary>
        private MappingManager MappingManager { get; set; }

        #endregion
    }
}
