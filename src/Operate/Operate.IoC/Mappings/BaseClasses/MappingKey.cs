﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.IoC.Mappings.Interfaces;
using Operate.IoC.Providers;

#endregion

namespace Operate.IoC.Mappings.BaseClasses
{
    /// <summary>
    /// Mapping key
    /// </summary>
    public class MappingKey : BaseMapping
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <param name="attributeType">Attribute type</param>
        /// <param name="providerManager">Provider manager</param>
        /// <param name="mappingManager">Mapping manager</param>
        public MappingKey(Type serviceType, Type attributeType, ProviderManager providerManager, MappingManager mappingManager)
            : base(serviceType, attributeType, providerManager, mappingManager)
        {
        }

        #endregion

        #region Functions

        /// <summary>
        /// Determines if the mapping keys are equal
        /// </summary>
        /// <param name="obj">The object to check against</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public override bool Equals(object obj)
        {
            var tempobj = obj as IMapping;
            return tempobj != null
                && tempobj.AttributeType == AttributeType
                && tempobj.ServiceType == ServiceType;
        }

        /// <summary>
        /// Gets the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            int attributeHash = AttributeType == null ? 1 : AttributeType.GetHashCode();
            int serviceHash = ServiceType == null ? 1 : ServiceType.GetHashCode();
            return serviceHash * attributeHash;
        }

        #endregion
    }
}
