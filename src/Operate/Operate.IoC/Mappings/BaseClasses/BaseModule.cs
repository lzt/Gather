﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.IoC.Mappings.Interfaces;

#endregion

namespace Operate.IoC.Mappings.BaseClasses
{
    /// <summary>
    /// Base module class
    /// </summary>
    public abstract class BaseModule : IModule
    {
        #region Functions

        /// <summary>
        /// Sets up the module
        /// </summary>
        public abstract void Setup();

        /// <summary>
        /// Creates a mapping using a specific service type
        /// </summary>
        /// <typeparam name="TServiceType">Service type</typeparam>
        /// <returns>A mapping object</returns>
        public virtual IMapping Map<TServiceType>()
        {
            return Map(typeof(TServiceType));
        }

        /// <summary>
        /// Creates a mapping using a specific service type and attribute type
        /// </summary>
        /// <typeparam name="TServiceType">Service type</typeparam>
        /// <typeparam name="TAttributeType">Attribute type</typeparam>
        /// <returns>A mapping object</returns>
        public virtual IMapping Map<TServiceType, TAttributeType>()
        {
            return Map(typeof(TServiceType), typeof(TAttributeType));
        }

        /// <summary>
        /// Creates a mapping using a specific service type
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <param name="attributeType">Attribute type</param>
        /// <returns>A mapping object</returns>
        public virtual IMapping Map(Type serviceType, Type attributeType = null)
        {
            return Manager.CreateMapping(serviceType, attributeType);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Mapping manager
        /// </summary>
        public virtual MappingManager Manager { get; set; }

        #endregion
    }
}
