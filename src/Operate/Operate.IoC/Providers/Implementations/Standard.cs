﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Operate.ExtensionMethods;
using Operate.IoC.Mappings;
using Operate.IoC.Mappings.Attributes;
using Operate.IoC.Mappings.Interfaces;
using Operate.IoC.Providers.BaseClasses;
using Operate.IoC.Utils;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.IoC.Providers.Implementations
{
    /// <summary>
    /// Standard implementation class
    /// </summary>
    public class Standard : BaseImplementation
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="implementationType">implementation type</param>
        /// <param name="mappingManager">Mapping manager</param>
        public Standard(Type implementationType, MappingManager mappingManager)
        {
            ReturnType = implementationType;
            MappingManager = mappingManager;
        }

        #endregion

        #region Functions

        #region Create

        /// <summary>
        /// Creates an object
        /// </summary>
        /// <returns>The object</returns>
        public override object Create()
        {
            ConstructorInfo constructor = ConstructorList.ChooseConstructor(ReturnType, MappingManager);
            object instance = CreateInstance(constructor);
            SetupProperties(instance);
            SetupMethods(instance);
            return instance;
        }

        #endregion

        #region SetupMethods

        private void SetupMethods(object instance)
        {
            if (instance==null)
                return;
            foreach (MethodInfo method in instance.GetType().GetMethods().Where(IsInjectable))
                method.Invoke(instance, method.GetParameters().ForEach(x => CreateInstance(x)).ToArray());
        }

        #endregion

        #region SetupProperties

        private void SetupProperties(object instance)
        {
            if (instance==null)
                return;
            instance.GetType()
                .GetProperties()
                .Where(IsInjectable)
                .ForEach<PropertyInfo>(x => instance.Property(x, CreateInstance(x)));
        }

        #endregion

        #region IsInjectable

        private static bool IsInjectable(MethodInfo method)
        {
            return IsInjectable(method.GetCustomAttributes(false));
        }

        private static bool IsInjectable(PropertyInfo property)
        {
            return IsInjectable(property.GetCustomAttributes(false));
        }

        private static bool IsInjectable(IEnumerable<object> attributes)
        {
            return attributes.OfType<Inject>().Any();
        }

        #endregion

        #region CreateInstance

        private object CreateInstance(ConstructorInfo constructor)
        {
            if (constructor==null || MappingManager==null)
                return null;
            var parameterValues = new List<object>();
            constructor.GetParameters().ForEach(x => parameterValues.Add(CreateInstance(x)));
            return constructor.Invoke(parameterValues.ToArray());
        }

        private object CreateInstance(ParameterInfo parameter)
        {
            return CreateInstance(parameter.GetCustomAttributes(false), parameter.ParameterType);
        }

        private object CreateInstance(PropertyInfo property)
        {
            return CreateInstance(property.GetCustomAttributes(false), property.PropertyType);
        }

        private object CreateInstance(object[] attributes, Type type)
        {
            if (attributes.Length > 0)
            {
                foreach (Attribute attribute in attributes)
                {
                    object tempObject = GetObject(type, attribute.GetType());
                    if (tempObject != null)
                        return tempObject;
                }
            }
            return GetObject(type);
        }

        #endregion

        #region GetObject

        private object GetObject(Type type)
        {
            IMapping mapping = MappingManager.GetMapping(type);
            return mapping==null ? null : mapping.Implementation.Create();
        }

        private object GetObject(Type type, Type attributeType)
        {
            IMapping mapping = MappingManager.GetMapping(type, attributeType);
            return mapping==null ? null : mapping.Implementation.Create();
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Mapping manager
        /// </summary>
        protected virtual MappingManager MappingManager { get; set; }

        #endregion
    }
}
