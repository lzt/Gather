﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// Sql辅助
    /// </summary>
    public class SqlAssistant
    {
        /// <summary>
        ///  简单过滤Sql语句,替换 ' 为 ''
        /// </summary>
        /// <param name="sqlParam"></param>
        /// <returns></returns>
        public static string FilterParam(string sqlParam)
        {
            return sqlParam.Replace("'", "''");
        }

        private const string StrKeyWord = @"select|insert|delete|from|count\(|drop table|update|truncate|asc\(|mid\(|char\(|xp_cmdshell|exec master|netlocalgroup administrators|:|net user|""|or|and";
        private const string StrRegex = @"[-|;|,|/|(|)|[|]|}|{|%|@|\*|!|']";

        /// <summary>
        /// 只读属性 SQL关键字
        /// </summary>
        public static string KeyWord
        {
            get
            {
                return StrKeyWord;
            }
        }
        /// <summary>
        /// 只读属性过滤特殊字符
        /// </summary>
        public static string RegexString
        {
            get
            {
                return StrRegex;
            }
        }

        /// <summary>
        /// 检查提交表单中是否存在SQL注入可能关键字
        /// </summary>
        /// <param name="nv">对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public static bool CheckInput(Dictionary<string, string> nv)
        {
            if (nv.Count > 0)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var item in nv)
                {
                    //检查参数值是否合法
                    if (CheckKeyWord(item.Value))
                    {
                        //存在SQL关键字
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 检查提交表单中是否存在SQL注入可能关键字
        /// </summary>
        /// <param name="nv">对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public static bool CheckInput(NameValueCollection nv)
        {
            if (nv.Count > 0)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var key in nv.AllKeys)
                {
                    //检查参数值是否合法
                    if (CheckKeyWord(nv[key]))
                    {
                        //存在SQL关键字
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 检查提交表单中是否存在SQL注入可能关键字
        /// </summary>
        /// <param name="inputs">对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public static bool CheckInput(IEnumerable<string> inputs)
        {
            var enumerable = inputs as string[] ?? inputs.ToArray();
            if (enumerable.Any())
            {
                //获取提交的表单项不为0 逐个比较参数
                for (int i = 0; i < enumerable.Length; i++)
                {
                    //检查参数值是否合法
                    if (CheckKeyWord(enumerable[i]))
                    {
                        //存在SQL关键字
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 检查提交表单中是否存在SQL注入可能关键字
        /// </summary>
        /// <param name="inputs">对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public static bool CheckInput(IList<string> inputs)
        {
            var enumerable = inputs as string[] ?? inputs.ToArray();
            if (enumerable.Any())
            {
                //获取提交的表单项不为0 逐个比较参数
                // ReSharper disable once LoopCanBeConvertedToQuery
                for (int i = 0; i < enumerable.Length; i++)
                {
                    //检查参数值是否合法
                    if (CheckKeyWord(enumerable[i]))
                    {
                        //存在SQL关键字
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 检查提交表单中是否存在SQL注入可能关键字
        /// </summary>
        /// <param name="inputs">对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public static bool CheckInput(ICollection<string> inputs)
        {
            var enumerable = inputs as string[] ?? inputs.ToArray();
            if (enumerable.Any())
            {
                //获取提交的表单项不为0 逐个比较参数
                // ReSharper disable once LoopCanBeConvertedToQuery
                for (int i = 0; i < enumerable.Length; i++)
                {
                    //检查参数值是否合法
                    if (CheckKeyWord(enumerable[i]))
                    {
                        //存在SQL关键字
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 静态方法，检查_sword是否包涵SQL关键字
        /// </summary>
        /// <param name="word">被检查的字符串</param>
        /// <returns>存在SQL关键字返回true，不存在返回false</returns>
        public static bool CheckKeyWord(string word)
        {
            if (Regex.IsMatch(word, StrKeyWord, RegexOptions.IgnoreCase) || Regex.IsMatch(word, StrRegex))
                return true;
            return false;
        }
    }
}
