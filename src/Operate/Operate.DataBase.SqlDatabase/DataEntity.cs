﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// 数据表列实体类
    /// </summary>
    [Serializable]
    public class DataEntity
    {
        /// <summary>
        /// 数据表列名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 架构名称
        /// </summary>
        public string Schema { get; set; }

        /// <summary>
        /// 实体类型
        /// </summary>
        public ConstantCollection.DataEntityType DataEntityType { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DataEntity()
        {
            Schema = "dbo";
        }
    }
}