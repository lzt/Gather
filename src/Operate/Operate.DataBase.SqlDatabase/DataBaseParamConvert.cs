﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// DataBaseParamConvert
    /// </summary>
    public class DataBaseParamConvert
    {
        private static readonly List<string> NeedTransmission = new List<string> { "DbType", "Direction", "IsNullable", "ParameterName", "SourceColumn", "SourceVersion", "Value" };

        /// <summary>
        /// ToDictionaryList
        /// </summary>
        /// <param name="param"></param>
        /// <param name="filertNeed"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDictionary(IDataParameter param, bool filertNeed = true)
        {
            var type = param.GetType();
            var property = type.GetProperties();
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyInfo info in property)
            {
                if (filertNeed)
                {
                    if (NeedTransmission.Contains(info.Name))
                    {
                        if (info.Name == "DbType")
                        {
                            var typename = info.GetValue(param, null).ToString();
                            const DbType t = DbType.String;
                            var v = t.GetValueByName(typename);
                            dictionary.Add(info.Name, v);
                        }
                        else if (info.Name == "Direction")
                        {
                            var typename = info.GetValue(param, null).ToString();
                            const ParameterDirection t = ParameterDirection.Input;
                            var v = t.GetValueByName(typename);
                            dictionary.Add(info.Name, v);
                        }
                        else if (info.Name == "SourceVersion")
                        {
                            var typename = info.GetValue(param, null).ToString();
                            const DataRowVersion t = DataRowVersion.Current;
                            var v = t.GetValueByName(typename);
                            dictionary.Add(info.Name, v);
                        }
                        else
                        {
                            dictionary.Add(info.Name, info.GetValue(param, null));
                        }
                    }
                }
                else
                {
                    dictionary.Add(info.Name, info.GetValue(param, null));
                }
            }
            return dictionary;
        }

        /// <summary>
        /// ToDatabaseParam
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="param"></param>
        /// <param name="byfilter"></param>
        /// <typeparam name="TDataParameter"></typeparam>
        /// <returns></returns>
        public static TDataParameter ToDatabaseParam<TDataParameter>(Dictionary<string, object> dic,TDataParameter param, bool byfilter = true) where TDataParameter : IDataParameter
        {
            //var param = new TDataParameter();

            var type = param.GetType();
            var property = type.GetProperties();
            foreach (var p in property)
            {
                if (p.CanWrite)
                {
                    if (byfilter)
                    {
                        if (NeedTransmission.Contains(p.Name))
                        {
                            if (p.Name == "DbType")
                            {
                                var value = Convert.ToInt32(dic.GetValue(p.Name));
                                var v = (DbType) value;
                                p.SetValue(param, v, null);
                            }
                            else if (p.Name == "Direction")
                            {
                                var value = Convert.ToInt32(dic.GetValue(p.Name));
                                var v = (ParameterDirection)value;
                                p.SetValue(param, v, null);
                            }
                            else if (p.Name == "SourceVersion")
                            {
                                var value = Convert.ToInt32(dic.GetValue(p.Name));
                                var v = (DataRowVersion)value;
                                p.SetValue(param, v, null);
                            }
                            else
                            {
                                var name = p.Name;
                                var value = dic.GetValue(name);
                                p.SetValue(param, value, null);
                            }
                        }
                    }
                    else
                    {
                        var name = p.Name;
                        var value = dic.GetValue(name);
                        p.SetValue(param, value, null);
                    }
                }
            }

            return param;
        }
    }
}
