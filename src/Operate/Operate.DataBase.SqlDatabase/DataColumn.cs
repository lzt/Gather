﻿using System;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// DataColumn
    /// </summary>
    [Serializable]
    public class DataColumn
    {
         /// <summary>
        /// 列子段名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 映射到.Net的字段类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 列信息
        /// </summary>
        public DataColumnInfo ColumnInfo { get; set; }

        /// <summary>
        /// 初始化
        /// </summary>
        public DataColumn()
        {
            Name = "";
            Type = "";
            ColumnInfo = new DataColumnInfo();
        }
    }
}
