﻿namespace Operate.DataBase.SqlDatabase.InterFace
{
    /// <summary>
    /// 配置接口
    /// </summary>
    public interface IConfig
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        string ConnectionString { get;  }
    }
}
