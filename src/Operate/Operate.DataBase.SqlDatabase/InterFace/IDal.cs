﻿using System;
using System.Collections.Generic;
using System.Data;
using Operate.DataBase.SqlDatabase.Event;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.SqlDatabase.InterFace
{
    /// <summary>
    /// 泛型模版类接口
    /// </summary>
    public interface IDal<T> : ICommon, ISqlDal where T : BaseEntity, new()
    {
        #region Properties

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        string FieldProtectionRight { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        string Database { get; }

        #endregion

        #region event

        /// <summary>
        /// 存储过程l执行完毕回调事件
        /// </summary>
        event ProcessFinishedHandler ProcessFinished;

        /// <summary>
        /// 自定义Sql执行完毕回调事件
        /// </summary>
        event CustomSqlFinishedHandler CustomSqlFinished;

        /// <summary>
        /// 插入Sql执行完毕回调事件
        /// </summary>
       event InsertFinishedHandler InsertFinished;

        /// <summary>
        /// 更新Sql执行完毕回调事件
        /// </summary>
        event UpdateFinishedHandler UpdateFinished;

        /// <summary>
        /// 删除Sql执行完毕回调事件
        /// </summary>
        event DeleteFinishedHandler DeleteFinished;

        /// <summary>
        /// 数据异步插入完成回调事件
        /// </summary>
        event AysncInsertFinishedHandler AysncInsertFinished;

        /// <summary>
        /// 数据异步更新完成回调事件
        /// </summary>
        event AysncUpdateFinishedHandler AysncUpdateFinished;

        /// <summary>
        /// 数据异步删除完成回调事件
        /// </summary>
        event AysncDeleteFinishedHandler AysncDeleteFinished;

        /// <summary>
        /// 自定义Sql异步完成回调事件
        /// </summary>
        event AysncCustomFinishedHandler AysncCustomFinished;

        #endregion

        #region RunSql

        /////  <summary>
        ///// 执行sql语句 返回List Model
        /////  </summary>
        /////  <param name="sql">sql语句</param>
        ///// <returns></returns>
        //IList<T> ExecuteSqlToList(string sql);

        ///  <summary>
        /// 执行sql语句 返回List Model
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        IList<T> ExecuteSqlToList(string sql, IDataParameter[] parameters=null, IAsyncResult ar = null);

        #endregion

        #region RunProcess

        ///// <summary>
        /////执行存储过程（无参数） 返回List Model
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //IList<T> ExecuteProcessToList(string processName);

        ///  <summary>
        /// 执行存储过程（附带参数） 返回List Model
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        IList<T> ExecuteProcessToList(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null);

        #endregion

        #region Async

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="ar"></param>
        bool AsyncInsert(T modelChatLog, IAsyncResult ar = null);

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="executeAysncInsertFinished">是否执行AysncInsertFinished</param>
        /// <param name="ar"></param>
        bool AsyncInsert(T modelChatLog, bool executeAysncInsertFinished, IAsyncResult ar = null);

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="ar"></param>
        bool AsyncUpdate(T modelChatLog, IAsyncResult ar = null);

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="executeAysncUpdateFinished">是否执行AysncUpdateFinished</param>
        /// <param name="ar"></param>
        bool AsyncUpdate(T modelChatLog, bool executeAysncUpdateFinished, IAsyncResult ar = null);

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="ar"></param>
        bool AsyncDelete(T modelChatLog, IAsyncResult ar = null);

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog"></param>
        /// <param name="executeAysncDeleteFinished">是否执行AysncDeleteFinished</param>
        /// <param name="ar"></param>
        bool AsyncDelete(T modelChatLog, bool executeAysncDeleteFinished, IAsyncResult ar = null);

        #endregion

        #region Script

        /// <summary>
        /// 获取创建脚本
        /// </summary>
        /// <returns></returns>
        string GetCreateScript(ConstantCollection.DataEntityType type);

        #endregion
    }
}
