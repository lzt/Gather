﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.DataBase.SqlDatabase.InterFace
{
    /// <summary>
    /// ICommon
    /// </summary>
    public interface ICommon
    {
        /// <summary>
        /// GenericDal
        /// </summary>
        IGenericDal GenericDalExecutor { get; set; }

        /// <summary>
        /// 对应Model主键名称
        /// </summary>
        string PrimarykeyName { get; set; }

        /// <summary>
        /// ModeType
        /// </summary>
        Type ModeType { get;   }
    }
}
