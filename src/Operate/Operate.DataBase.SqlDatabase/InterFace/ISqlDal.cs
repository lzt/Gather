﻿using System;
using System.Data;

namespace Operate.DataBase.SqlDatabase.InterFace
{
    /// <summary>
    /// ISqlDal
    /// </summary>
    public interface ISqlDal
    {
        #region Method

        #region GetSql

        /// <summary>
        /// 获取插入命令
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <returns></returns>
        string GetInsertSqlByDataRow(string tableName, DataRow row, DataColumnCollection columes);

        #endregion

        #region Insert

        /// <summary>
        /// 更新DataTable
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="row"></param>
        /// <param name="columes"></param>
        /// <param name="ar"></param>
        void InsertByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null);

        /// <summary>
        /// 插入DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>
        void InsertByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null);

        #endregion

        #region Update

        /// <summary>
        /// 更新DataRow
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="row"></param>
        /// <param name="columes"></param>
        /// <param name="ar"></param>
        void UpdateByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null);

        /// <summary>
        /// 更新DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将更新的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>     
        void UpdateByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null);

        #endregion

        #region Delete

        /// <summary>
        /// 删除DataRow
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="row"></param>
        /// <param name="columes"></param>
        /// <param name="ar"></param>
        void DeleteByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null);


        /// <summary>
        /// 删除DataRow
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="primaryName">主键名称</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>
        /// <param name="ar"></param>   
        void DeleteByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null);

        #endregion

        #region Count

        /// <summary>
        /// 根据条件获取结果数
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        int GetCountBySql(string where = "");

        #endregion

        #region RunSql

        /// <summary>
        /// 执行Sql语句 , 返回第一行第一列
        /// </summary>
        /// <param name="sql">存储过程名</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        object ExecuteSqlScalar(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null);

        /// <summary>
        /// 执行Sql语句  无返回结果
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        void ExecuteSqlNonQuery(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null);

        ///  <summary>
        /// 执行Sql语句 返回Json字符串
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        string ExecuteSqlToJson(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null);

        ///  <summary>
        /// 执行Sql语句 返回DataTable
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        DataTable ExecuteSqlToDatatable(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null);

        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        DataSet ExecuteSqlToDataset(string sql,  IDataParameter[] parameters=null, IAsyncResult ar = null);

        #endregion

        #region RunProcess

        ///// <summary>
        ///// 执行存储过程（无参数）  返回第一行第一列
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        //object ExecuteProcessScalar(string processName);

        /// <summary>
        /// 执行存储过程（附带参数）  返回第一行第一列
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        object ExecuteProcessScalar(string processName,  IDataParameter[] parameters=null, IAsyncResult ar = null);

        ///// <summary>
        ///// 执行存储过程（无参数）  无返回结果
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        //void ExecuteProcessNonQuery(string processName);

        /// <summary>
        /// 执行存储过程（附带参数）  无返回结果
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        void ExecuteProcessNonQuery(string processName,  IDataParameter[] parameters=null, IAsyncResult ar = null);

        ///// <summary>
        /////执行Sql语句 返回Json字符串
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //string ExecuteProcessToJson(string processName);

        ///  <summary>
        /// 执行Sql语句 返回Json字符串
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        string ExecuteProcessToJson(string processName,   IDataParameter[] parameters=null, IAsyncResult ar = null);

        ///  <summary>
        /// 执行Sql语句 返回dynamic
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        dynamic ExecuteProcessTodynamic(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null);

        ///// <summary>
        /////执行存储过程（无参数） 返回DataTable
        ///// </summary>
        ///// <param name="processName"></param>
        ///// <returns></returns>
        //DataTable ExecuteProcessToDatatable(string processName);

        ///  <summary>
        /// 执行存储过程（附带参数） 返回DataTable
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        DataTable ExecuteProcessToDatatable(string processName,  IDataParameter[] parameters=null, IAsyncResult ar = null);

        /////  <summary>
        ///// 执行存储过程（无参数）返回DataSet
        /////  </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //DataSet ExecuteProcessToDataset(string processName);

        ///  <summary>
        ///  执行存储过程（附带参数）DataSet
        ///  </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        DataSet ExecuteProcessToDataset(string processName,  IDataParameter[] parameters=null, IAsyncResult ar = null);

        #endregion

        #region Async

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        bool AsyncCustom(string sql,  IDataParameter[] parameters, IAsyncResult ar=null);

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="executeAysncCustomFinished">是否执行AysncCustomFinished</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        bool AsyncCustom(string sql, bool executeAysncCustomFinished,  IDataParameter[] parameters, IAsyncResult ar=null);

        #endregion

        #region DataBaseParam

        /// <summary>
        /// CreateNewParam
        /// </summary>
        /// <returns></returns>
        IDataParameter CreateNewParam();

        #endregion

        #region Common

        /// <summary>
        /// 获取程序集路径
        /// </summary>
        /// <returns></returns>
        string GetAssemblyPath();

        #endregion

        #endregion
    }
}
