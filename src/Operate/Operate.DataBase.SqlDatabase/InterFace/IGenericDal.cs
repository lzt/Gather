﻿using System.Collections.Generic;
using NHibernate;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.SqlDatabase.InterFace
{
    /// <summary>
    /// 数据处理接口
    /// </summary>
    public interface IGenericDal
    {
        #region Properties

        /// <summary>
        /// 对应Model主键名称
        /// </summary>
        string PrimarykeyName { get; set; }

        /// <summary>
        /// EntityName
        /// </summary>
        string EntityName { get; set; }

        #endregion

        #region Method

        #region Insert

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItem">将要插入的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Insert<TModel>(TModel modelItem, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelJson">将要插入的数据实体Json</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Insert<TModel>(string modelJson, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Insert<TModel>(List<TModel> modelItemList, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        #endregion

        #region Update

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Update<TModel>(TModel modelItem, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据实体Json</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Update<TModel>(string modelJson, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Update<TModel>(List<TModel> modelItemList, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        #endregion

        #region Delete

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItem">即将要删除的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Delete<TModel>(TModel modelItem, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelJson">即将要删除的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Delete<TModel>(string modelJson, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="primaryValue">即将要删除的数据主键</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void DeleteByPrimarykey<TModel>(object primaryValue, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 批量删除数据
        /// </summary>
        /// <param name="primaryValueList">即将要删除的数据主键集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void DeleteByPrimarykeyList<TModel>(List<object> primaryValueList, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItemList">即将要删除的数据实体集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        void Delete<TModel>(List<TModel> modelItemList, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        void DeleteByWhere(string where, ref ISession session,bool closeSession=true);

        #endregion

        #region GetOne

        /// <summary>
        /// 根据主键获取数据实体
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>数据实体</returns>
        TModel GetByPrimarykey<TModel>(object primaryValue, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 通过条件获取
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="where"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        TModel GetByWhere<TModel>(string where, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        #endregion

        #region ExistByProperty

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="pkId">主键值</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>数据实体</returns>
        bool Exist<TModel>(int pkId, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 根据条件判断数据是否存在
        /// </summary>
        /// <param name="where">筛选条件</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>数据实体</returns>
        bool Exist<TModel>(string where, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        #endregion

        #region GetList

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <returns>实体列表</returns>
        IList<TModel> GetList<TModel>( ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="listId">主键列表</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(List<object> listId, ref ISession session,bool closeSession=true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new();

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, List<object> listId, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new();

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalRecords">总计路数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="totalPages">总页面数</param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new();

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>实体列表</returns>
        IList<TModel> GetList<TModel>(string where, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <returns>实体列表</returns>
        IList<TModel> GetList<TModel>(List<object> listId, string where, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new();

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <returns>实体列表</returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, List<object> listId, string where, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new();

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalPages">总页数</param>
        /// <returns>实体列表</returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, string where, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new();

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="sort">排序语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>实体列表</returns>
        IList<TModel> GetList<TModel>(string where, string sort, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="where"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, string where, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">总页数</param>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 返回分页列表页
        ///  </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">总页数</param>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">总页数</param>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where,
            string sort, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">总页数</param>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where,
            string sort, string group, ref ISession session,bool closeSession=true) where TModel : BaseEntity, new();

        #endregion

        #region Count

        /// <summary>
        /// 根据条件获取结果数
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        int GetCount(string where, ref ISession session,bool closeSession=true);

        #endregion

        #endregion
    }
}
