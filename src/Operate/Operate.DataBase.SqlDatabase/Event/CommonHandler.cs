﻿namespace Operate.DataBase.SqlDatabase.Event
{
    /// <summary>
    /// 存储过程l执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ProcessFinishedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 自定义Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void CustomSqlFinishedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 插入Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void InsertFinishedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 更新Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void UpdateFinishedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 删除Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void DeleteFinishedHandler(object sender, System.EventArgs e);
}
