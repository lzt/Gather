﻿namespace Operate.DataBase.SqlDatabase.Event
{
    /// <summary>
    /// 异步自定义Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void AysncCustomFinishedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 异步插入Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void AysncInsertFinishedHandler(object sender,System.EventArgs e);

    /// <summary>
    /// 异步更新Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void AysncUpdateFinishedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 异步删除Sql执行完毕回调事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void AysncDeleteFinishedHandler(object sender, System.EventArgs e);
}
