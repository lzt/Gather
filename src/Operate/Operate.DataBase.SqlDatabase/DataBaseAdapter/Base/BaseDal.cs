﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Operate.DataBase.SqlDatabase.Event;
using Operate.DataBase.SqlDatabase.InterFace;
using Operate.DataBase.SqlDatabase.Model;
using Operate.Exception;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase.DataBaseAdapter.Base
{
    /// <summary>
    /// GenericBaseDal
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseDal<T> : IDal<T> where T : BaseEntity, new()
    {
        #region Filed

        private string _primarykeyName;

        protected string _entityName;

        #endregion

        #region Properties

        /// <summary>
        /// GenericDal
        /// </summary>
        public IGenericDal GenericDalExecutor { get; set; }

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        public string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        public string FieldProtectionRight { get; set; }

        /// <summary>
        /// 当前数据库
        /// </summary>
        public virtual string Database { get; set; }

        /// <summary>
        /// 对应Model主键名称
        /// </summary>
        public virtual string PrimarykeyName
        {
            get { return _primarykeyName; }
            set
            {
                _primarykeyName = value;
                GenericDalExecutor.PrimarykeyName = _primarykeyName;
            }
        }

        /// <summary>
        /// 实体名称
        /// </summary>
        public string EntityName
        {
            get { return _entityName; }
            set
            {
                _entityName = value;
                (GenericDalExecutor).EntityName = _entityName;
            }
        }

        /// <summary>
        /// ModeType
        /// </summary>
        public Type ModeType { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// BaseDal
        /// </summary>
        protected BaseDal()
        {
            ModeType = typeof(T);
        }

        #endregion

        #region Event

        #region ProcessFinished

        /// <summary>
        /// 存储过程l执行完毕回调事件
        /// </summary>
        public event ProcessFinishedHandler ProcessFinished;

        /// <summary>
        /// 存储过程l执行完毕后执行回调事件
        /// </summary>
        /// <param name="ar"></param>
        protected virtual void OnProcessFinished(IAsyncResult ar)
        {
            var handler = ProcessFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region CustomSqlFinished

        /// <summary>
        /// 自定义Sql执行完毕回调事件
        /// </summary>
        public event CustomSqlFinishedHandler CustomSqlFinished;

        /// <summary>
        /// 自定义Sql执行完毕后执行回调事件
        /// </summary>
        /// <param name="ar"></param>
        protected virtual void OnCustomSqlFinished(IAsyncResult ar)
        {
            var handler = CustomSqlFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region InsertFinished

        /// <summary>
        /// 数据插入完毕回调事件
        /// </summary>
        public event InsertFinishedHandler InsertFinished;

        /// <summary>
        /// 数据插入完毕后执行回调事件
        /// </summary>
        /// <param name="ar"></param>
        protected virtual void OnInsertFinished(IAsyncResult ar)
        {
            var handler = InsertFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region UpdateFinished

        /// <summary>
        /// 更新Sql执行完毕回调事件
        /// </summary>
        public event UpdateFinishedHandler UpdateFinished;

        /// <summary>
        /// 数据更新完毕后执行回调事件
        /// </summary>
        /// <param name="ar"></param>
        protected virtual void OnUpdateFinished(IAsyncResult ar)
        {
            var handler = UpdateFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region DeleteFinished

        /// <summary>
        /// 删除Sql执行完毕回调事件
        /// </summary>
        public event DeleteFinishedHandler DeleteFinished;

        /// <summary>
        /// 数据删除完毕后执行回调事件
        /// </summary>
        /// <param name="ar"></param>
        protected virtual void OnDeleteFinished(IAsyncResult ar)
        {
            var handler = DeleteFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region AysncInsertFinished

        /// <summary>
        /// 数据异步插入完成回调事件
        /// </summary>
        public event AysncInsertFinishedHandler AysncInsertFinished;

        /// <summary>
        /// 数据异步插入完成后执行回调事件
        /// </summary>
        /// <param name="ar"></param>
        protected virtual void OnAysncInsertFinished(IAsyncResult ar)
        {
            var handler = AysncInsertFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region AysncUpdateFinished

        /// <summary>
        /// 数据异步更新完成回调事件
        /// </summary>
        public event AysncUpdateFinishedHandler AysncUpdateFinished;

        /// <summary>
        /// 数据异步更新完成后执行回调事件
        /// </summary>
        protected virtual void OnAysncUpdateFinished(IAsyncResult ar)
        {
            var handler = AysncUpdateFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region AysncDeleteFinished

        /// <summary>
        /// 数据异步删除完成回调事件
        /// </summary>
        public event AysncDeleteFinishedHandler AysncDeleteFinished;

        /// <summary>
        /// 数据异步删除完成后执行回调事件
        /// </summary>
        protected virtual void OnAysncDeleteFinished(IAsyncResult ar)
        {
            var handler = AysncDeleteFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region AysncCustomFinished

        /// <summary>
        /// 自定义Sql异步完成回调事件
        /// </summary>
        public event AysncCustomFinishedHandler AysncCustomFinished;

        /// <summary>
        /// 自定义Sql异步完成后执行回调事件
        /// </summary>
        protected virtual void OnAysncCustomFinished(IAsyncResult ar)
        {
            var handler = AysncCustomFinished;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #endregion

        #region Function

        /// <summary>
        /// 载入外置Config文件
        /// </summary>
        /// <param name="path">配置文件路径</param>
        /// <returns></returns>
        protected string LoadConfigFromXmlDocument(string path)
        {
            string result;
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var sr = new StreamReader(fs);
                result = sr.ReadToEnd();
            }
            return result;
        }

        #endregion

        #region GetSql

        /// <summary>
        /// 获取插入命令
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <returns></returns>
        public string GetInsertSqlByDataRow(string tableName, DataRow row, DataColumnCollection columes)
        {
            return DataTableToSql.GetInsertSql(tableName, PrimarykeyName, row, columes);
        }

        /// <summary>
        /// 获取更新命令
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="row">即将更新的DataRow</param>
        /// <param name="columes">目标表列集合</param>     
        /// <returns></returns>
        private string GetUpdateSql(string tableName, DataRow row, DataColumnCollection columes)
        {
            return DataTableToSql.GetUpdateSql(tableName, PrimarykeyName, row, columes);
        }

        /// <summary>
        /// 获取删除命令
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>
        /// <returns></returns>
        private string GetDeleteSql(string tableName, DataRow row,
            DataColumnCollection columes)
        {
            return DataTableToSql.GetDeleteSql(tableName, PrimarykeyName, row, columes);
        }

        #endregion

        #region GetOne

        /// <summary>
        /// 根据主键获取数据实体
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <returns>数据实体</returns>
        protected virtual T GetByPrimarykeyBySql(object primaryValue)
        {
            if (primaryValue.IsNull())
            {
                throw new EmptyException();
            }

            var m = new T();
            var sql = m.GetSelectSqlByPrimary(primaryValue);
            return ExecuteSqlToModel(sql);
        }

        /// <summary>
        /// 通过条件获取
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        protected virtual T GetByWhereBySql(string where)
        {
            if (where.IsNullOrEmpty())
            {
                throw new EmptyException();
            }

            var m = new T();
            var sql = m.GetSelectSql(where);
            return ExecuteSqlToModel(sql);
        }

        #endregion

        #region InsertBySql

        /// <summary>
        /// 插入DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>
        public virtual void InsertByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            var sql = GetInsertSqlByDataRow(tableName, row, columes);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 插入DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>
        public virtual void InsertByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            var sql = DataTableToSql.GetInsertSql(tableName, primaryName, row, columes);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItem">将要插入的数据实体</param>
        /// <param name="ar"></param>
        protected virtual void InsertBySql(T modelItem, IAsyncResult ar = null)
        {
            if (modelItem.IsNull())
            {
                throw new EmptyException();
            }

            var sql = modelItem.GetInsertSql();
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelJson"></param>
        /// <param name="ar"></param>
        protected virtual void InsertBySql(string modelJson, IAsyncResult ar = null)
        {
            if (modelJson.IsNullOrEmpty())
            {
                throw new EmptyException();
            }

            var m = JsonConvert.DeserializeObject<T>(modelJson);
            InsertBySql(m, ar);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="ar"></param>
        protected virtual void InsertBySql(List<T> modelItemList, IAsyncResult ar = null)
        {
            if (modelItemList.IsNull())
            {
                throw new EmptyException();
            }

            foreach (var m in modelItemList)
            {
                InsertBySql(m, ar);
            }
        }

        #endregion

        #region UpdateBySql

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="ar"></param>
        protected virtual void UpdateBySql(T modelItem, IAsyncResult ar = null)
        {
            if (modelItem.IsNull())
            {
                throw new EmptyException();
            }

            var sql = modelItem.GetUpdateSql();
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelJson"></param>
        /// <param name="ar"></param>
        protected virtual void UpdateBySql(string modelJson, IAsyncResult ar = null)
        {
            if (modelJson.IsNullOrEmpty())
            {
                throw new EmptyException();
            }

            var m = JsonConvert.DeserializeObject<T>(modelJson);
            UpdateBySql(m, ar);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="ar"></param>
        protected virtual void UpdateBySql(List<T> modelItemList, IAsyncResult ar = null)
        {
            if (modelItemList.IsNull())
            {
                throw new EmptyException();
            }

            foreach (var m in modelItemList)
            {
                UpdateBySql(m, ar);
            }
        }

        /// <summary>
        /// 更新DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="row">即将更新的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>        
        public virtual void UpdateByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            var sql = GetUpdateSql(tableName, row, columes);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 更新DataRow
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将更新的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <param name="ar"></param>     
        public virtual void UpdateByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            var sql = DataTableToSql.GetUpdateSql(tableName, primaryName, row, columes);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        #endregion

        #region DeleteBySql

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItem">即将要删除的数据实体</param>
        /// <param name="ar"></param>
        protected virtual void DeleteBySql(T modelItem, IAsyncResult ar = null)
        {
            if (modelItem.IsNull())
            {
                throw new EmptyException();
            }

            var sql = modelItem.GetDeleteSql();
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelJson">即将要删除的数据实体</param>
        /// <param name="ar"></param>
        protected virtual void DeleteBySql(string modelJson, IAsyncResult ar = null)
        {
            if (modelJson.IsNullOrEmpty())
            {
                throw new EmptyException();
            }

            var m = JsonConvert.DeserializeObject<T>(modelJson);
            DeleteBySql(m, ar);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItemList">将要删除的数据实体集合</param>
        /// <param name="ar"></param>
        protected virtual void DeleteBySql(List<T> modelItemList, IAsyncResult ar = null)
        {
            if (modelItemList.IsNull())
            {
                throw new EmptyException();
            }

            foreach (var m in modelItemList)
            {
                DeleteBySql(m, ar);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="primaryValue">即将要删除的数据主键</param>
        /// <param name="ar"></param>
        protected virtual void DeleteByPrimarykeyBySql(object primaryValue, IAsyncResult ar = null)
        {
            if (primaryValue.IsNull())
            {
                throw new EmptyException();
            }

            var m = new T();
            var sql = m.GetDeleteSqlByPrimary(primaryValue);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        protected virtual void DeleteByWhereBySql(string where, IAsyncResult ar = null)
        {
            if (where.IsNullOrEmpty())
            {
                throw new EmptyException();
            }

            var m = new T();
            var sql = m.GetDeleteSql(where);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 通过主键批量删除
        /// </summary>
        /// <param name="primaryValueList">主键值集合</param>
        /// <param name="where">其他条件</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        protected virtual void DeleteByPrimarykeyListBySql(object[] primaryValueList, string where = "", IAsyncResult ar = null)
        {
            if (primaryValueList.IsNull())
            {
                throw new EmptyException();
            }

            var list = primaryValueList.ToList();
            DeleteByPrimarykeyListBySql(list, where, ar);
        }

        /// <summary>
        /// 通过主键批量删除
        /// </summary>
        /// <param name="primaryValueList">主键值集合</param>
        /// <param name="where">其他条件</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        protected virtual void DeleteByPrimarykeyListBySql(List<object> primaryValueList, string where = "", IAsyncResult ar = null)
        {
            if (primaryValueList.IsNull())
            {
                throw new EmptyException();
            }

            var wherestr = primaryValueList.Aggregate("", (current, i) => current + string.Format(" {0}={1} or", PrimarykeyName, i));
            var haswhere = false;
            if (!string.IsNullOrEmpty(where))
            {
                if (string.IsNullOrEmpty(where.Trim()))
                {
                    haswhere = true;
                    wherestr += where;
                }
            }
            if (!haswhere)
            {
                wherestr = wherestr.TrimEnd("or".ToCharArray());
            }
            DeleteByWhereBySql(wherestr, ar);
            GetListBySql(primaryValueList, where);
        }

        /// <summary>
        /// 删除DataRow
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>
        /// <param name="ar"></param>
        public virtual void DeleteByDataRow(string tableName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            var sql = GetDeleteSql(tableName, row, columes);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        /// <summary>
        /// 删除DataRow
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="primaryName">主键名称</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>
        /// <param name="ar"></param>   
        public virtual void DeleteByDataRow(string tableName, string primaryName, DataRow row, DataColumnCollection columes, IAsyncResult ar = null)
        {
            var sql = DataTableToSql.GetDeleteSql(tableName, primaryName, row, columes);
            ExecuteSqlNonQuery(sql, null, ar);
        }

        #endregion

        #region GetListBySql

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <returns>实体列表</returns>
        protected virtual IList<T> GetListBySql()
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(-1, -1, out totalPages, out totalRecords, "", " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="primaryValueList">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(List<object> primaryValueList, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(-1, -1, out totalPages, out totalRecords, primaryValueList, mode);
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="primaryValueList">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, List<object> primaryValueList, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, primaryValueList, mode);
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalRecords">总计路数</param>
        /// <param name="primaryValueList">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="totalPages">总页面数</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> primaryValueList, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include)
        {
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, primaryValueList, "", mode);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <returns>实体列表</returns>
        protected virtual IList<T> GetListBySql(string where)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(-1, -1, out totalPages, out totalRecords, where, " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="primaryValueList">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <returns>实体列表</returns>
        protected virtual IList<T> GetListBySql(List<object> primaryValueList, string where, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include)
        {
            return GetListBySql(-1, -1, primaryValueList, where, mode);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="primaryValueList">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <returns>实体列表</returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, List<object> primaryValueList, string where, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, primaryValueList, where, mode);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="primaryValueList">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalPages">总页数</param>
        /// <returns>实体列表</returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> primaryValueList, string where, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include)
        {
            totalPages = 0;
            totalRecords = 0;
            var list = primaryValueList ?? new List<object>();
            string whereFilter = "";
            if (mode == ConstantCollection.ListMode.Include)
            {
                if (list.Count > 0)
                {
                    whereFilter = list.Aggregate("", (current, i) => current + (" " + EntityName + "." + PrimarykeyName + " = " + i + " or"));
                    whereFilter = whereFilter.Remove(whereFilter.LastIndexOf("or", StringComparison.Ordinal));
                }
                else
                {
                    return new List<T>();
                }
            }
            else if (mode == ConstantCollection.ListMode.Exclude)
            {
                if (list.Count > 0)
                {
                    whereFilter = list.Aggregate("", (current, i) => current + (" " + EntityName + "." + PrimarykeyName + " != " + i + " and"));
                    whereFilter = whereFilter.Remove(whereFilter.LastIndexOf("and", StringComparison.Ordinal));
                }
            }

            if (!string.IsNullOrEmpty(where))
            {
                if (!string.IsNullOrEmpty(whereFilter))
                {
                    whereFilter = " (" + whereFilter + ") and " + where;
                }
                else
                {
                    whereFilter = where;
                }
            }

            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, whereFilter, " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="sort">排序语句</param>
        /// <returns>实体列表</returns>
        protected virtual IList<T> GetListBySql(string where, string sort)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(-1, -1, out totalPages, out totalRecords, where, sort, "");
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, "", " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="where"></param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, string where)
        {
            long totalPages;
            long totalRecords;
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, where, " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords)
        {
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, "", " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 返回分页列表页
        ///  </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where)
        {
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, where, " " + EntityName + "." + PrimarykeyName + " desc ", "");
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort)
        {
            return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, where, sort, "");
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <returns></returns>
        protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords,
            string where, string sort, string group)
        {
            throw new NeedRewriteException("protected virtual IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords,string where, string sort, string group)");
        }

        #endregion

        #region Count

        /// <summary>
        /// GetCount
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public int GetCountBySql(string where = "")
        {
            var m = new T();
            var sql = m.GetCountSql(where);
            var r = ExecuteSqlScalar(sql);
            return Convert.ToInt32(r);
        }

        #endregion

        #region RunSql

        /// <summary>
        /// 执行Sql语句 , 返回第一行第一列
        /// </summary>
        /// <param name="sql">存储过程名</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        public virtual object ExecuteSqlScalar(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new NeedRewriteException("public virtual object ExecuteSqlScalar(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)");
        }

        /// <summary>
        /// 执行Sql语句  无返回结果
        /// </summary>
        /// <param name="sql">存储过程名</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        public virtual void ExecuteSqlNonQuery(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new NeedRewriteException("public virtual void ExecuteSqlNonQuery(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)");
        }


        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new NeedRewriteException("public virtual DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters = null,IAsyncResult ar = null)");
        }

        ///  <summary>
        /// 执行Sql语句 返回DataTable
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual DataTable ExecuteSqlToDatatable(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var ds = ExecuteSqlToDataset(sql, parameters, ar);
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        ///  <summary>
        /// 执行sql语句 返回List Model
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual IList<T> ExecuteSqlToList(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            return DataConvert.ConvertToModel<T>(ExecuteSqlToDatatable(sql, parameters, ar));
        }

        ///  <summary>
        /// 执行sql语句 返回Model
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual T ExecuteSqlToModel(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var list = DataConvert.ConvertToModel<T>(ExecuteSqlToDatatable(sql, parameters, ar));
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        ///  <summary>
        /// 执行Sql语句 返回Json字符串
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual string ExecuteSqlToJson(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var dt = ExecuteSqlToDatatable(sql, parameters, ar);
            if (dt != null)
            {
                return JsonConvert.SerializeObject(dt);
            }
            return "";
        }

        #endregion

        #region RunProcess

        #region RunProcessCore

        ///// <summary>
        ///// 执行存储过程（无参数）  无返回结果
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        //public virtual void ExecuteProcessNonQuery(string processName)
        //{
        //    ExecuteProcessNonQuery(processName, null);
        //}

        /// <summary>
        /// 执行存储过程（附带参数）  无返回结果
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        public virtual void ExecuteProcessNonQuery(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new NeedRewriteException("public virtual void ExecuteProcessNonQuery(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)");
        }

        ///// <summary>
        ///// 执行存储过程（无参数）  无返回结果
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        //public virtual object ExecuteProcessScalar(string processName)
        //{
        //    return ExecuteProcessScalar(processName, null);
        //}

        /// <summary>
        /// 执行存储过程（附带参数）  无返回结果
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        public virtual object ExecuteProcessScalar(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new NeedRewriteException("public virtual object ExecuteProcessScalar(string processName,  IDataParameter[] parameters=null, IAsyncResult ar = null)");
        }

        ///// <summary>
        /////执行Sql语句 返回Json字符串
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //public virtual string ExecuteProcessToJson(string processName)
        //{
        //    var dt = ExecuteProcessToDatatable(processName);
        //    if (dt != null)
        //    {
        //        return JsonConvert.SerializeObject(dt);
        //    }
        //    return "";
        //}

        ///  <summary>
        /// 执行Sql语句 返回Json字符串
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual string ExecuteProcessToJson(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var dt = ExecuteProcessToDatatable(processName, parameters, ar);
            if (dt != null)
            {
                return JsonConvert.SerializeObject(dt);
            }
            return "";
        }

        ///  <summary>
        /// 执行Sql语句 返回dynamic
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual dynamic ExecuteProcessTodynamic(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var json = ExecuteProcessToJson(processName, parameters, ar);
            if (string.IsNullOrWhiteSpace(json))
            {
                return JsonConvert.DeserializeObject(json);
            }
            return null;
        }

        ///// <summary>
        /////执行存储过程（无参数） 返回DataTable
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //public virtual DataTable ExecuteProcessToDatatable(string processName)
        //{
        //    var ds = ExecuteProcessToDataset(processName);
        //    if (ds.Tables.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        ///  <summary>
        /// 执行存储过程（附带参数） 返回DataTable
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual DataTable ExecuteProcessToDatatable(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var ds = ExecuteProcessToDataset(processName, parameters, ar);
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        /////  <summary>
        ///// 执行存储过程（无参数）返回DataSet
        /////  </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //public virtual DataSet ExecuteProcessToDataset(string processName)
        //{
        //    return ExecuteProcessToDataset(processName, null);
        //}

        ///  <summary>
        ///  执行存储过程（附带参数）DataSet
        ///  </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual DataSet ExecuteProcessToDataset(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            throw new NeedRewriteException("public virtual DataSet ExecuteProcessToDataset(string processName, IDataParameter[] parameters=null, IAsyncResult ar = null)");
        }

        #endregion

        ///// <summary>
        /////执行存储过程（无参数） 返回List Model
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //public virtual IList<T> ExecuteProcessToList(string processName)
        //{
        //    return ExecuteProcessToList(processName, null);
        //}

        ///  <summary>
        /// 执行存储过程（附带参数） 返回List Model
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual IList<T> ExecuteProcessToList(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            return DataConvert.ConvertToModel<T>(ExecuteProcessToDatatable(processName, parameters, ar));
        }

        ///// <summary>
        /////执行存储过程（无参数） 返回Model
        ///// </summary>
        ///// <param name="processName">存储过程名</param>
        ///// <returns></returns>
        //public virtual T ExecuteProcessToModel(string processName)
        //{
        //    return ExecuteProcessToModel(processName, null);
        //}

        ///  <summary>
        /// 执行存储过程（附带参数） 返回Model
        ///  </summary>
        ///  <param name="processName">存储过程名</param>
        ///  <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual T ExecuteProcessToModel(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            var list = DataConvert.ConvertToModel<T>(ExecuteProcessToDatatable(processName, parameters, ar));
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        #endregion

        #region ExistByProperty

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <returns>数据实体</returns>
        protected virtual bool ExistBySql(object primaryValue)
        {
            var listId = new List<object> { primaryValue };
            var list = GetListBySql(listId);
            return list != null && list.Count > 0;
        }

        /// <summary>
        /// 根据条件判断数据是否存在
        /// </summary>
        /// <param name="where">筛选条件</param>
        /// <returns>数据实体</returns>
        protected virtual bool ExistBySql(string where)
        {
            var list = GetListBySql(where);
            return list != null && list.Count > 0;
        }

        #endregion

        #region Async

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog">实体参数</param>
        /// <param name="ar"></param>
        public virtual bool AsyncInsert(T modelChatLog, IAsyncResult ar = null)
        {
            return AsyncInsert(modelChatLog, false, ar);
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog">实体参数</param>
        /// <param name="executeAysncInsertFinished">是否执行AysncInsertFinished</param>
        /// <param name="ar"></param>
        public virtual bool AsyncInsert(T modelChatLog, bool executeAysncInsertFinished, IAsyncResult ar = null)
        {
            if (executeAysncInsertFinished)
            {
                OnAysncInsertFinished(ar);
            }
            return false;
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog">实体参数</param>
        /// <param name="ar"></param>
        public virtual bool AsyncUpdate(T modelChatLog, IAsyncResult ar = null)
        {
            return AsyncUpdate(modelChatLog, false, ar);
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog">实体参数</param>
        /// <param name="executeAysncUpdateFinished">是否执行AysncUpdateFinished</param>
        /// <param name="ar"></param>
        public virtual bool AsyncUpdate(T modelChatLog, bool executeAysncUpdateFinished, IAsyncResult ar = null)
        {
            if (executeAysncUpdateFinished)
            {
                OnAysncUpdateFinished(ar);
            }
            return false;
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog">实体参数</param>
        /// <param name="ar"></param>
        public virtual bool AsyncDelete(T modelChatLog, IAsyncResult ar = null)
        {
            return AsyncDelete(modelChatLog, false, ar);
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="modelChatLog">实体参数</param>
        /// <param name="executeAysncDeleteFinished">是否执行AysncDeleteFinished</param>
        /// <param name="ar"></param>
        public virtual bool AsyncDelete(T modelChatLog, bool executeAysncDeleteFinished, IAsyncResult ar = null)
        {
            if (executeAysncDeleteFinished)
            {
                OnAysncDeleteFinished(ar);
            }
            return false;
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        public virtual bool AsyncCustom(string sql, IDataParameter[] parameters, IAsyncResult ar = null)
        {
            return AsyncCustom(sql, false, parameters, ar);
        }

        /// <summary>
        /// 异步执行 (返回false，必须重载使用)
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="executeAysncCustomFinished">是否执行AysncCustomFinished</param>
        /// <param name="parameters"></param>
        /// <param name="ar"></param>
        public virtual bool AsyncCustom(string sql, bool executeAysncCustomFinished, IDataParameter[] parameters, IAsyncResult ar = null)
        {
            if (executeAysncCustomFinished)
            {
                OnAysncCustomFinished(ar);
            }
            return false;
        }

        #endregion

        #region DataBaseParam

        /// <summary>
        /// CreateNewParam
        /// </summary>
        /// <returns></returns>
        public virtual IDataParameter CreateNewParam()
        {
            return null;
        }

        /// <summary>
        /// ToDatabaseParam
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IDataParameter ToDatabaseParam(Dictionary<string, object> dic)
        {
            return DataBaseParamConvert.ToDatabaseParam(dic, CreateNewParam());
        }

        #endregion

        #region Script

        /// <summary>
        /// 获取创建脚本
        /// </summary>
        /// <returns></returns>
        public abstract string GetCreateScript(ConstantCollection.DataEntityType type);

        #endregion

        #region Common

        /// <summary>
        /// 获取程序集路径
        /// </summary>
        /// <returns></returns>
        public string GetAssemblyPath()
        {
            return GetType().Assembly.GetPath();
        }

        #endregion
    }
}
