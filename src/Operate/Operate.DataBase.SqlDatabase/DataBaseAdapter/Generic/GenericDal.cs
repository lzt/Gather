﻿/*
* Code By 卢志涛
*/

using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NHibernate;
using Operate.DataBase.SqlDatabase.InterFace;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.SqlDatabase.DataBaseAdapter.Generic
{
    /// <summary>
    /// 数据基础处理累
    /// </summary>
    public abstract class GenericDal : IGenericDal
    {
        #region Properties

        /// <summary>
        /// 对应Model主键名称
        /// </summary>
        public virtual string PrimarykeyName { get; set; }

        /// <summary>
        /// EntityName
        /// </summary>
        public string EntityName { get; set; }

        #endregion

        #region Method

        #region Insert

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItem">将要插入的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void Insert<TModel>(TModel modelItem, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelJson">将要插入的数据实体Json</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public virtual void Insert<TModel>(string modelJson, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            var m = JsonConvert.DeserializeObject<TModel>(modelJson);
            Insert(m, ref session, closeSession);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void Insert<TModel>(List<TModel> modelItemList, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        #endregion

        #region Update

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void Update<TModel>(TModel modelItem, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据实体Json</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public virtual void Update<TModel>(string modelJson, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            var m = JsonConvert.DeserializeObject<TModel>(modelJson);
            Update(m, ref session, closeSession);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void Update<TModel>(List<TModel> modelItemList, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        #endregion

        #region Delete

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItem">即将要删除的数据实体</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void Delete<TModel>(TModel modelItem, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据实体Json</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public virtual void Delete<TModel>(string modelJson, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            var m = JsonConvert.DeserializeObject<TModel>(modelJson);
            Delete(m, ref session, closeSession);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItemList">将要删除的数据实体集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void Delete<TModel>(List<TModel> modelItemList, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="primaryValue">即将要删除的数据主键</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void DeleteByPrimarykey<TModel>(object primaryValue, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 批量删除数据
        /// </summary>
        /// <param name="primaryValueList">即将要删除的数据主键集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public abstract void DeleteByPrimarykeyList<TModel>(List<object> primaryValueList, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public abstract void DeleteByWhere(string where, ref ISession session, bool closeSession = true);

        #endregion

        #region GetOne

        /// <summary>
        /// 根据主键获取数据实体
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>数据实体</returns>
        public abstract TModel GetByPrimarykey<TModel>(object primaryValue, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        /// <summary>
        /// 通过条件获取
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="where"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public virtual TModel GetByWhere<TModel>(string where, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            TModel result = null;
            var list = GetList<TModel>(where, ref session, false) ?? new List<TModel>();
            if (list.Count > 0)
            {
                result = list[0];
            }
            if (closeSession)
            {
                session.Close();
            }
            return result;
        }

        #endregion

        #region ExistByProperty

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>数据实体</returns>
        public bool Exist<TModel>(int primaryValue, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            var listId = new List<object> { primaryValue };
            var list = GetList<TModel>(listId, ref session, false);
            if (closeSession)
            {
                session.Close();
            }
            return list != null && list.Count > 0;
        }

        /// <summary>
        /// 根据条件判断数据是否存在
        /// </summary>
        /// <param name="where">筛选条件</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>数据实体</returns>
        public bool Exist<TModel>(string where, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            var list = GetList<TModel>(where, ref session, false);
            if (closeSession)
            {
                session.Close();
            }
            return list != null && list.Count > 0;
        }

        #endregion

        #region GetList

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <returns>实体列表</returns>
        public virtual IList<TModel> GetList<TModel>(ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(-1, -1, out totalPages, out totalRecords, "", " " + PrimarykeyName + " desc ", "", ref session, closeSession);
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="listId">主键列表</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        public virtual IList<TModel> GetList<TModel>(List<object> listId, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(-1, -1, out totalPages, out totalRecords, listId, ref session, closeSession, mode);
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, List<object> listId, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, listId, ref session, closeSession, mode);
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalRecords">总计路数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="totalPages">总页面数</param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new()
        {
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, listId, "", ref session, closeSession, mode);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>实体列表</returns>
        public virtual IList<TModel> GetList<TModel>(string where, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(-1, -1, out totalPages, out totalRecords, where, " " + PrimarykeyName + " desc ", "", ref session, closeSession);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <returns>实体列表</returns>
        public virtual IList<TModel> GetList<TModel>(List<object> listId, string @where, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new()
        {
            return GetList<TModel>(-1, -1, listId, where, ref session, closeSession, mode);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <returns>实体列表</returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, List<object> listId, string @where, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, listId, where, ref session, closeSession, mode);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalPages">总页数</param>
        /// <returns>实体列表</returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, string @where, ref ISession session, bool closeSession = true, ConstantCollection.ListMode mode = ConstantCollection.ListMode.Include) where TModel : BaseEntity, new()
        {
            totalPages = 0;
            totalRecords = 0;
            var list = listId ?? new List<object>();
            string whereFilter = "";
            if (mode == ConstantCollection.ListMode.Include)
            {
                if (list.Count > 0)
                {
                    whereFilter = list.Aggregate("", (current, i) => current + (" " + PrimarykeyName + " = " + i + " or"));
                    whereFilter = whereFilter.Remove(whereFilter.LastIndexOf("or", StringComparison.Ordinal));
                }
                else
                {
                    return new List<TModel>();
                }
            }
            else if (mode == ConstantCollection.ListMode.Exclude)
            {
                if (list.Count > 0)
                {
                    whereFilter = list.Aggregate("", (current, i) => current + (" " + PrimarykeyName + " != " + i + " and"));
                    whereFilter = whereFilter.Remove(whereFilter.LastIndexOf("and", StringComparison.Ordinal));
                }
            }

            if (!string.IsNullOrEmpty(where))
            {
                if (!string.IsNullOrEmpty(whereFilter))
                {
                    whereFilter = " (" + whereFilter + ") and " + where;
                }
                else
                {
                    whereFilter = where;
                }
            }

            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, whereFilter, " " + PrimarykeyName + " desc ", "", ref session, closeSession);
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="sort">排序语句</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns>实体列表</returns>
        public virtual IList<TModel> GetList<TModel>(string where, string sort, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(-1, -1, out totalPages, out totalRecords, where, sort, "", ref  session, closeSession);
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, "", " " + PrimarykeyName + " desc ", "", ref  session, closeSession);
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="where"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, string where, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            long totalPages;
            long totalRecords;
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, where, " " + PrimarykeyName + " desc ", "", ref session, closeSession);
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, "", " " + PrimarykeyName + " desc ", "", ref session, closeSession);
        }

        /// <summary>
        /// 返回分页列表页
        ///  </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, where, " " + PrimarykeyName + " desc ", "", ref session, closeSession);
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public virtual IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string @where, string sort, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new()
        {
            return GetList<TModel>(currentPage, pagesize, out totalPages, out totalRecords, where, sort, "", ref session, closeSession);
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public abstract IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, string group, ref ISession session, bool closeSession = true) where TModel : BaseEntity, new();

        #endregion

        #region Count

        /// <summary>
        /// 根据条件获取结果数
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public abstract int GetCount(string where, ref ISession session, bool closeSession = true);

        #endregion

        #endregion
    }
}
