﻿using System;
using System.Collections.Generic;

namespace Operate.DataBase.SqlDatabase
{
    #region DatatableColume

    /// <summary>
    /// DatatableColumeCollection
    /// </summary>
    [Serializable]
    public class DatatableColumeCollection
    {
        /// <summary>
        /// ConnectionString
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Dialect
        /// </summary>
        public string Dialect { get; set; }

        /// <summary>
        /// DriverClass
        /// </summary>
        public string DriverClass { get; set; }

        /// <summary>
        /// DataBaseType
        /// </summary>
        public string DataBaseType { get; set; }

        /// <summary>
        /// DataBaseParamNameSpace
        /// </summary>
        public string DataBaseParamNameSpace { get; set; }

        /// <summary>
        /// DataBaseParamType
        /// </summary>
        public string DataBaseParamType { get; set; }

        /// <summary>
        /// Guid
        /// </summary>
        public List<string> Guid { get; set; }

        /// <summary>
        /// NameSpaceName
        /// </summary>
        public string NameSpaceName { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Schema
        /// </summary>
        public string Schema { get; set; }

        /// <summary>
        /// PkName
        /// </summary>
        public string PkName { get; set; }

        /// <summary>
        /// PkType
        /// </summary>
        public string PkType { get; set; }

        /// <summary>
        /// Statement
        /// </summary>
        public string Statement { get; set; }

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        public string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        public string FieldProtectionRight { get; set; }

        /// <summary>
        /// List
        /// </summary>
        public List<DataColumn> List { get; set; }

        /// <summary>
        /// Tables
        /// </summary>
        public List<DataEntity> Tables { get; set; }

        /// <summary>
        /// Other
        /// </summary>
        public object Other { get; set; }

        /// <summary>
        /// DatatableColumeCollection
        /// </summary>
        public DatatableColumeCollection()
        {
            Guid = new List<string>() {
                System.Guid.NewGuid().ToString(),
                System.Guid.NewGuid().ToString()
            };
            List = new List<DataColumn>();
            Tables = new List<DataEntity>();

            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }
    }

    #endregion DatatableColume
}