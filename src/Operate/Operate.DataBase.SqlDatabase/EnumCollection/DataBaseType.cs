﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Operate.DataBase.SqlDatabase.EnumCollection
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DataBaseType
    {
        /// <summary>
        /// Access
        /// </summary>
        [Description("Access")]
        Access = 1,

        /// <summary>
        /// SqlServer
        /// </summary>
        [Description("SqlServer")]
        SqlServer = 2,

        /// <summary>
        /// Sqlite
        /// </summary>
        [Description("Sqlite")]
        Sqlite = 3,

        /// <summary>
        /// MySql
        /// </summary>
        [Description("MySql")]
        MySql = 4,

        /// <summary>
        /// Db2
        /// </summary>
        [Description("Db2")]
        Db2 = 5,

        /// <summary>
        /// Firebird
        /// </summary>
        [Description("Firebird")]
        Firebird = 6,

        /// <summary>
        /// Ingres
        /// </summary>
        [Description("Ingres")]
        Ingres = 7,

        /// <summary>
        /// Oracle
        /// </summary>
        [Description("Oracle")]
        Oracle = 8,

        /// <summary>
        /// PostgreSql
        /// </summary>
        [Description("PostgreSql")]
        PostgreSql = 9,

        /// <summary>
        /// Sybase
        /// </summary>
        [Description("Sybase")]
        Sybase = 10,
    }
}
