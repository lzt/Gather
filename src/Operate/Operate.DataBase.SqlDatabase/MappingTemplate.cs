﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// MappingTemplate
    /// </summary>
    public class MappingTemplate
    {

        #region AnalysisDocument

        private static DataColumnInfo AnalysisColumnInfo(XmlNode primaryNode)
        {
            var columnInfo = new DataColumnInfo();

            if (primaryNode.HasChildNodes)
            {
                var columnNode = primaryNode.FirstChild;
                if (columnNode.Name == "column")
                {
                    if (columnNode.Attributes != null)
                    {
                        var length = columnNode.Attributes["length"] == null ? "0" : columnNode.Attributes["length"].Value;
                        columnInfo.DataLength = length.IsNullOrEmpty() ? 0 : Convert.ToInt32(length);

                        var sqlType = columnNode.Attributes["sql-type"] == null ? "" : columnNode.Attributes["sql-type"].Value;
                        columnInfo.DataBaseType = sqlType.IsNullOrEmpty() ? "" : sqlType;

                        var precision = columnNode.Attributes["precision"] == null ? "" : columnNode.Attributes["precision"].Value;
                        columnInfo.Precision = precision.IsNullOrEmpty() ? "" : precision;

                        var scale = columnNode.Attributes["scale"] == null ? "" : columnNode.Attributes["scale"].Value;
                        columnInfo.Scale = scale.IsNullOrEmpty() ? "" : scale;

                        if (columnNode.HasChildNodes)
                        {
                            var commentNode = columnNode.FirstChild;
                            if (commentNode.Name == "comment")
                            {
                                columnInfo.Description = commentNode.InnerText;
                            }
                        }
                    }
                }
            }

            return columnInfo;
        }

        /// <summary>
        /// 分析模版，生成模版参数
        /// </summary>
        public static DatatableColumeCollection AnalysisDocument(string xmlDocumentSource)
        {
            var templateParam = new DatatableColumeCollection();
            var document = new XmlDocument();
            document.LoadXml(xmlDocumentSource);
            const string prefxkey = "nh";
            var nsMgr = new XmlNamespaceManager(document.NameTable);
            nsMgr.AddNamespace(prefxkey, "urn:nhibernate-mapping-2.2");

            var classNode = document.SelectSingleNode(string.Format("/{0}:hibernate-mapping/{0}:class", prefxkey), nsMgr);

            if (classNode != null && classNode.Attributes != null)
            {
                templateParam.Name = classNode.Attributes["table"].Value;

                var primaryNode = document.SelectSingleNode(string.Format("/{0}:hibernate-mapping/{0}:class/{0}:id", prefxkey), nsMgr);

                if (primaryNode != null)
                {
                    if (primaryNode.Attributes != null)
                    {
                        var columnInfo = AnalysisColumnInfo(primaryNode);
                        columnInfo.IsPrimaryKey = true;
                        columnInfo.IsIdentity = true;
                        columnInfo.IsNullable = false;
                        var primaryitem = new DataColumn
                        {
                            Name = primaryNode.Attributes["name"].Value,
                            Type = primaryNode.Attributes["type"].Value.Replace("StringClob","string"),
                            ColumnInfo = columnInfo
                        };
                        templateParam.List.Add(primaryitem);
                    }
                }

                var otherNodes = document.SelectNodes(string.Format("/{0}:hibernate-mapping/{0}:class/{0}:property", prefxkey), nsMgr);
                if (otherNodes != null)
                {
                    foreach (XmlNode n in otherNodes)
                    {
                        if (n.Attributes != null)
                        {
                            var columnInfo = AnalysisColumnInfo(n);
                            columnInfo.IsPrimaryKey = false;
                            columnInfo.IsIdentity = false;
                            columnInfo.IsNullable = true;
                            var item = new DataColumn
                            {
                                Name = n.Attributes["name"].Value,
                                Type = n.Attributes["type"].Value.Replace("StringClob", "string"),
                                ColumnInfo = columnInfo
                            };
                            templateParam.List.Add(item);
                        }
                    }
                }
            }

            return templateParam;
        }

        #endregion

        /// <summary>
        /// XmlTemplate
        /// </summary>
        public static string XmlTemplate
        {
            get
            {
                var stream = typeof(MappingTemplate).Assembly.GetManifestResourceStream("Operate.DataBase.SqlDatabase.MappingTemplate.txt");
                if (stream != null)
                {
                    stream.Position = 3;//过滤Bom
                    return stream.ReadAll();
                }
                return "";
            }
        }

        /// <summary>
        /// BuildMapping
        /// </summary>
        /// <returns></returns>
        public static string BuildMapping(string assemblyname, string namespaceString, string typefullname, string typename, List<DataColumn> columnList)
        {
            string result = XmlTemplate;
            var primaryMapping = "";
            var propertiesMapping = "";
            foreach (var c in columnList)
            {
                if (c.ColumnInfo.IsPrimaryKey)
                {
                    primaryMapping = BuildKeyMapping(c.Name, c.Type, c.ColumnInfo.Description);
                }
                else
                {
                    propertiesMapping += BuildPropertyMapping(c.Name, c.Type, c.ColumnInfo.Description, c.ColumnInfo.DataBaseType, c.ColumnInfo.DataLength.ToString(CultureInfo.InvariantCulture), c.ColumnInfo.Precision, c.ColumnInfo.Scale);
                }
            }

            result = ReplaceTemplate(result, primaryMapping, propertiesMapping, assemblyname, namespaceString, typefullname, typename);

            return result;
        }

        /// <summary>
        /// ReplaceValue
        /// </summary>
        /// <param name="template"></param>
        /// <param name="primaryMapping"></param>
        /// <param name="propertiesMapping"></param>
        /// <param name="assemblyname"></param>
        /// <param name="namespaceString"></param>
        /// <param name="typefullname"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        public static string ReplaceTemplate(string template, string primaryMapping, string propertiesMapping, string assemblyname, string namespaceString, string typefullname, string typename)
        {
            return template.Replace("$id$", primaryMapping).Replace(" $property$", propertiesMapping).Replace("$assemblyname$", assemblyname).Replace("$namespace$", namespaceString).Replace("$typefullname$", typefullname).Replace("$typename$", typename);
        }

        /// <summary>
        /// BuildKeyMapping
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static string BuildKeyMapping(string name, string type, string comment)
        {
            return "<id name=\"{0}\" column=\"{0}\" type=\"{1}\" unsaved-value=\"{2}\">{3}<generator class=\"{4}\"/></id>".FormatValue(name, type, type=="int"|| type=="long"?"0":"", comment.IsNullOrEmpty() ? "" : ("<column name=\"{0}\"><comment>{1}</comment></column>".FormatValue(name, comment)),type=="int"|| type=="long"? "native": "uuid.hex");
        }

        /// <summary>
        /// BuildPropertyMapping
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="comment"></param>
        /// <param name="sqltype"></param>
        /// <param name="length"></param>
        /// <param name="precision"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static string BuildPropertyMapping(string name, string type, string comment, string sqltype, string length, string precision, string scale)
        {
            if (sqltype.ToLower().In(new[] { "ntext", "text", "mediumtext", "longtext" }))
            {
                return "<property name=\"{0}\" column=\"{0}\" type=\"{1}\" ><column name=\"{0}\" >{2}</column></property>".FormatValue(name, "StringClob", comment.IsNullOrEmpty() ? "" : ("<comment>{0}</comment>".FormatValue(comment)));
            }
            return "<property name=\"{0}\" column=\"{0}\" type=\"{1}\" ><column name=\"{0}\" {3} {4} {5} {6}>{2}</column></property>".FormatValue(name, type, comment.IsNullOrEmpty() ? "" : ("<comment>{0}</comment>".FormatValue(comment)), sqltype.IsNullOrEmpty() ? "" : "sql-type=\"{0}\"".FormatValue(sqltype),(length.IsNullOrEmpty() || length=="0") ? "" : "length=\"{0}\"".FormatValue(length), precision.IsNullOrEmpty() ? "" : "precision=\"{0}\"".FormatValue(precision), scale.IsNullOrEmpty() ? "" : "scale=\"{0}\"".FormatValue(scale));
        }
    }
}
