﻿using System.ComponentModel;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// 常量枚举集合
    /// </summary>
    public class ConstantCollection : DataBase.ConstantCollection
    {
        /// <summary>
        /// 数据库实体类型
        /// </summary>
        public enum DataEntityType
        {
            /// <summary>
            /// 数据表
            /// </summary>
            [Description("数据表")]
            DataTable = 0,

            /// <summary>
            /// 视图
            /// </summary>
            [Description("视图")]
            DataView = 1,
        }
    }
}
