﻿<?xml version="1.0" encoding="utf-8" ?>
<hibernate-mapping xmlns="urn:nhibernate-mapping-2.2" assembly="$assemblyname$" namespace="$namespace$"  auto-import="true" default-cascade="none">
  <class name="$typefullname$,$assemblyname$" table="$typename$"  entity-name="$typename$">
  $id$
  $property$
  </class>
</hibernate-mapping>