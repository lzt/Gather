﻿using System;
using System.Data;
using System.Globalization;
using System.Text;
using Operate.Exception;
using Operate.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// DataTableToSql
    /// </summary>
    public class DataTableToSql
    {
        #region Static GetSql

        /// <summary>
        /// 获取更新命令
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将更新的DataRow</param>
        /// <param name="columes">目标表列集合</param>     
        /// <returns></returns>
        public static string GetUpdateSql(string tableName, string primaryName, DataRow row, DataColumnCollection columes)
        {
            if (tableName.IsNullOrEmpty() || primaryName.IsNullOrEmpty() || row.IsNull() || columes.IsNull())
            {
                throw new EmptyException();
            }

            var updateCmd = "update " + tableName + " set ";
            int primaryValue = -1;
            var resultFiled = new StringBuilder();
            foreach (System.Data.DataColumn col in columes)
            {
                if (!col.ColumnName.Equals(primaryName, StringComparison.OrdinalIgnoreCase))
                {
                    var type = col.DataType;
                    if (type == typeof(int))
                    {
                        resultFiled.AppendFormat(col.ColumnName + "=" + row[col.ColumnName] + ",");
                    }
                    else if (type == typeof(DateTime))
                    {
                        var val = Convert.ToDateTime(row[col.ColumnName]);
                        if (val == Operate.ConstantCollection.NetDefaultDateTime)
                        {
                            val = Operate.ConstantCollection.DbDefaultDateTime;
                        }
                        resultFiled.AppendFormat(col.ColumnName + "='" + val.ToString(CultureInfo.InvariantCulture) + "',");
                    }
                    else
                    {
                        resultFiled.AppendFormat(col.ColumnName + "='" + row[col.ColumnName] + "',");
                    }
                }
                else
                {
                    primaryValue = Convert.ToInt32(row[col.ColumnName]);
                }
            }

            var sql = updateCmd + resultFiled.ToString().Trim(',') + " where " + primaryName + "=" + primaryValue;
            return sql;
        }


        /// <summary>
        /// 获取插入命令
        /// </summary>
        /// <param name="tableName">数据表名</param>
        /// <param name="primaryName">主键名</param>
        /// <param name="row">即将插入的DataRow</param>
        /// <param name="columes">目标表列集合</param>
        /// <returns></returns>
        public static string GetInsertSql(string tableName, string primaryName, DataRow row, DataColumnCollection columes)
        {
            if (tableName.IsNullOrEmpty()||primaryName.IsNullOrEmpty()||row.IsNull()||columes.IsNull())
            {
                throw new EmptyException();
            }

            var insertCmd = "insert into " + tableName + " ";
            var resultFiled = new StringBuilder(" (");
            var resultValue = new StringBuilder(" (");
            foreach (System.Data.DataColumn col in columes)
            {
                if (!col.ColumnName.Equals(primaryName, StringComparison.OrdinalIgnoreCase))
                {
                    var type = col.DataType;
                    //var type = propertyInfo.GetValue(modelChatLog, null).GetType();
                    if (type == typeof(int))
                    {
                        resultFiled.AppendFormat(col.ColumnName + ",");
                        resultValue.AppendFormat((row[col.ColumnName] == DBNull.Value ? 0 : row[col.ColumnName]) + ",");
                    }
                    else if (type == typeof(DateTime))
                    {
                        var val = Convert.ToDateTime(row[col.ColumnName] == DBNull.Value ? Operate.ConstantCollection.DbDefaultDateTime : row[col.ColumnName]);
                        resultFiled.AppendFormat(col.ColumnName + ",");
                        if (val == Operate.ConstantCollection.NetDefaultDateTime)
                        {
                            val = Operate.ConstantCollection.DbDefaultDateTime;
                        }
                        resultValue.AppendFormat("'" + val.ToString(CultureInfo.InvariantCulture) + "',");
                    }
                    else
                    {
                        resultFiled.AppendFormat(col.ColumnName + ",");
                        resultValue.AppendFormat("'" + row[col.ColumnName] + "',");
                    }
                }
            }

            var sql = insertCmd + resultFiled.ToString().Trim(',') + ") values " + resultValue.ToString().Trim(',') + ")";
            return sql;
        }

        /// <summary>
        /// 获取删除命令
        /// </summary>
        /// <param name="tableName">数据表明</param>
        /// <param name="primaryName">主键名称</param>
        /// <param name="row">目标DataRow</param>
        /// <param name="columes">数据表列集合</param>   
        /// <returns></returns>
        public static string GetDeleteSql(string tableName, string primaryName, DataRow row, DataColumnCollection columes)
        {
            if (tableName.IsNullOrEmpty() || primaryName.IsNullOrEmpty() || row.IsNull() || columes.IsNull())
            {
                throw new EmptyException();
            }

            var updateCmd = "delete from " + tableName + " ";
            int primaryValue = -1;
            foreach (System.Data.DataColumn col in columes)
            {
                if (col.ColumnName.Equals(primaryName, StringComparison.OrdinalIgnoreCase))
                {
                    primaryValue = Convert.ToInt32(row[col.ColumnName]);
                }
            }
            var sql = updateCmd + " where " + primaryName + "=" + primaryValue;
            return sql;
        }


        #endregion
    }
}
