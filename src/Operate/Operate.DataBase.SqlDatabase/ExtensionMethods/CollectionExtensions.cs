﻿using Operate.DataBase.SqlDatabase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.DataBase.SqlDatabase.ExtensionMethods
{
    /// <summary>
    /// CollectionExtensions
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// GetMultiFilterString
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyNameList"></param>
        /// <returns></returns>
        public static string GetMultiFilterString<T>(this ICollection<T> source, params string[] propertyNameList) where T : BaseEntity
        {
            return BaseEntity.GetMultiFilterString(source, propertyNameList);
        }
    }
}
