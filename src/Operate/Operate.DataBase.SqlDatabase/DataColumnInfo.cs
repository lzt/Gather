﻿using System;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// DataColumnInfo
    /// </summary>
    [Serializable]
    public class DataColumnInfo
    {
        #region Properties

        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        public string DataBaseType { get; set; }

        /// <summary>
        /// 字段描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 数据长度
        /// </summary>
        public int DataLength { get; set; }

        /// <summary>
        /// 是否可空
        /// </summary>
        public bool IsNullable { get; set; }

        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// 字段精度
        /// </summary>
        public string Precision { get; set; }

        /// <summary>
        /// 字段规模
        /// </summary>
        public string Scale { get; set; }

        /// <summary>
        /// 字段默认值
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// 是否标识
        /// </summary>
        public bool IsIdentity { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// 初始化
        /// </summary>
        public DataColumnInfo()
        {
            ColumnName = "";
            DataBaseType = "";
            Description = "";
            DataLength = 0;
            IsNullable = true;
            IsPrimaryKey = false;
            Precision = "";
            Scale = "";
            DefaultValue = "";
            IsIdentity = false;
        }

        #endregion
    }
}
