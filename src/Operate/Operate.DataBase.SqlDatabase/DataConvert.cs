﻿using System.Collections.Generic;
using System.Data;
using System.Dynamic;

namespace Operate.DataBase.SqlDatabase
{
    /// <summary>
    /// DataTableConvert
    /// </summary>
    public class DataConvert
    {
        /// <summary>
        /// DataReader转换为dataset
        /// </summary>
        /// <param name="reader">DataReader</param>
        /// <returns></returns>
        public static DataSet ConvertToDataSet(IDataReader reader)
        {
            return ConvertCollection.DataReaderToDataSet(reader);
        }

        /// <summary>
        /// ConvertToDataTable
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable(IDataReader reader)
        {
            return ConvertCollection.DataReaderToDataTable(reader);
        }

        /// <summary>
        /// ConvertToJson
        /// </summary>
        /// <param name="reader">    </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static string ConvertToJson(IDataReader reader, bool keyToLower = true)
        {
            return ConvertCollection.DataReaderToJson(reader, keyToLower);
        }

        /// <summary>
        /// ConvertToDataTable
        /// </summary>
        /// <param name="dataTable"> </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static string ConvertToJson(DataTable dataTable, bool keyToLower = true)
        {
            return ConvertCollection.DataTableToJson(dataTable, keyToLower);
        }

        /// <summary>
        /// ConvertToDictionaryList
        /// </summary>
        /// <param name="reader">    </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static IList<IDictionary<string, object>> ConvertToDictionaryList(IDataReader reader, bool keyToLower = true)
        {
            return ConvertCollection.DataReaderToDictionaryList(reader, keyToLower);
        }

        /// <summary>
        /// ConvertToDictionaryList
        /// </summary>
        /// <param name="dataTable"> </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static IList<IDictionary<string, object>> ConvertToDictionaryList(DataTable dataTable, bool keyToLower = true)
        {
            return ConvertCollection.DataTableToDictionaryList(dataTable, keyToLower);
        }

        /// <summary>
        /// ConvertToExpandoObjectList
        /// </summary>
        /// <param name="reader">    </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static List<ExpandoObject> ConvertToExpandoObjectList(IDataReader reader, bool keyToLower = true)
        {
            return ConvertCollection.DataReaderToExpandoObjectList(reader, keyToLower);
        }

        /// <summary>
        /// ConvertToExpandoObjectList
        /// </summary>
        /// <param name="dataTable"> </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static List<ExpandoObject> ConvertToExpandoObjectList(DataTable dataTable, bool keyToLower = true)
        {
            return ConvertCollection.DataTableToExpandoObjectList(dataTable, keyToLower);
        }

        /// <summary>
        /// DataTable转换为Model(忽略不匹配项)
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="table">     要转换的数据表</param>
        /// <param name="longToint"> 遇到long类型自动转换为int</param>
        /// <param name="ignoreCase">忽略大小写(默认忽略大小写)</param>
        /// <returns></returns>
        public static IList<TModel> ConvertToModel<TModel>(DataTable table, bool longToint = true, bool ignoreCase = true)
        {
            return ConvertCollection.DataTableToModel<TModel>(table, longToint, ignoreCase);
        }
    }
}