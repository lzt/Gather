﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Reflection;
using System.Text;
using Operate.DataBase.SqlDatabase.Attribute;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;
using System.Collections.Generic;

namespace Operate.DataBase.SqlDatabase.Model
{
    /// <summary>
    /// BaseModel
    /// </summary>
    public class BaseEntity : BaseModel
    {
        #region Property

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected static string FieldProtectionRight { get; set; }

        /// <summary>
        /// PrimarykeyBox
        /// </summary>
        public static NameValueCollection PrimarykeyBox { get; set; }

        #endregion

        #region Constructor

        static BaseEntity()
        {
            PrimarykeyBox = new NameValueCollection();

            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        #endregion

        #region Function

        #region PrimarykeyName

        /// <summary>
        /// 获取主键名
        /// </summary>
        /// <returns></returns>
        public virtual string GetPrimarykeyName()
        {
            var name = GetClassName();
            if (PrimarykeyBox.Get(name).IsNullOrEmpty())
            {
                var properties = GetType().GetProperties();
                foreach (var p in properties)
                {
                    if (p.ContainAttribute<KeyAttribute>())
                    {
                        PrimarykeyBox.Add(name, p.Name);
                        break;
                    }
                }
            }

            return PrimarykeyBox.Get(name);
        }

        #endregion

        #region CountSql

        /// <summary>
        /// 获取Insert语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetCountSql(string where = null)
        {
            const string selectCmd = "select ";

            var resultFiled = new StringBuilder();
            var properties = GetType().GetProperties();
            var primarykeyName = GetPrimarykeyName();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.Name.ToLower() == primarykeyName)
                {
                    resultFiled.AppendFormat(" count(" + AddFieldProtection(propertyInfo.Name) + ") ");
                }
            }

            var selectSql = selectCmd + resultFiled + " from " + AddFieldProtection(GetType().Name);

            if (where.IsNullOrEmpty())
            {
                return selectSql;
            }
            return selectSql + " where " + @where;
        }

        #endregion

        #region SelectSql

        /// <summary>
        /// 获取Insert语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetSelectSql(string where = null)
        {
            const string selectCmd = "select ";

            var resultFiled = new StringBuilder();
            var properties = GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + ",");
            }

            var selectSql = selectCmd + resultFiled + " from " + AddFieldProtection(GetType().Name);

            if (where.IsNullOrEmpty())
            {
                return selectSql;
            }
            return selectSql + " where " + where;
        }

        /// <summary>
        /// 获取Insert语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetSelectSqlByPrimary(object primaryValue = null)
        {
            const string selectCmd = "select ";

            var resultFiled = new StringBuilder();
            string searchText = string.Empty;
            var properties = GetType().GetProperties();
            var primarykeyName = GetPrimarykeyName();
            primarykeyName = primarykeyName.IsNullOrEmpty() ? "" : primarykeyName.ToLower();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.Name.ToLower() == primarykeyName)
                {
                    if (primaryValue == null)
                    {
                        var type = propertyInfo.PropertyType.UnderlyingSystemType;
                        if (type == typeof(int))
                        {
                            searchText = " where {0} = {1} ".FormatValue(AddFieldProtection(propertyInfo.Name), propertyInfo.GetValue(this, null));
                        }
                        else if (type == typeof(DateTime))
                        {
                            searchText = " where {0} = {1} ".FormatValue(AddFieldProtection(propertyInfo.Name), propertyInfo.GetValue(this, null));
                        }
                        else
                        {
                            searchText = " where {0} = '{1}' ".FormatValue(AddFieldProtection(propertyInfo.Name), propertyInfo.GetValue(this, null));
                        }
                    }
                    else
                    {
                        if (primaryValue.IsInt())
                        {
                            searchText = " where {0} = {1} ".FormatValue(AddFieldProtection(propertyInfo.Name), primaryValue);
                        }
                        else if (primaryValue.IsDateTime())
                        {
                            searchText = " where {0} = {1} ".FormatValue(AddFieldProtection(propertyInfo.Name), primaryValue);
                        }
                        else
                        {
                            searchText = " where {0} = '{1}' ".FormatValue(AddFieldProtection(propertyInfo.Name), primaryValue);
                        }
                    }
                }
                resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + ",");
            }

            return selectCmd + resultFiled + " from " + AddFieldProtection(GetType().Name) + " " + searchText;
        }

        #endregion

        #region InsertSql

        /// <summary>
        /// 获取Insert语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetInsertSql()
        {
            var insertCmd = "insert into " + GetType().Name + " ";
            var resultFiled = new StringBuilder(" (");
            var resultValue = new StringBuilder(" (");
            var properties = GetType().GetProperties();
            var primarykeyName = GetPrimarykeyName();
            primarykeyName = primarykeyName.IsNullOrEmpty() ? "" : primarykeyName.ToLower();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.Name.ToLower() == primarykeyName)
                {
                    if (propertyInfo.ContainAttribute<AutoIncrement>())
                    {
                        continue;
                    }
                }

                var type = propertyInfo.PropertyType.UnderlyingSystemType;
                //var type = propertyInfo.GetValue(modelChatLog, null).GetType();
                if (type == typeof(int))
                {
                    resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + ",");
                    resultValue.AppendFormat(propertyInfo.GetValue(this, null) + ",");
                }
                else if (type == typeof(DateTime))
                {
                    var val = Convert.ToDateTime(propertyInfo.GetValue(this, null));
                    resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + ",");
                    if (val == Operate.ConstantCollection.NetDefaultDateTime)
                    {
                        val = Operate.ConstantCollection.DbDefaultDateTime;
                    }
                    resultValue.AppendFormat("'" + val.ToString(CultureInfo.InvariantCulture) + "',");
                }
                else
                {
                    resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + ",");
                    resultValue.AppendFormat("'" + propertyInfo.GetValue(this, null) + "',");
                }

            }

            return insertCmd + resultFiled.ToString().Trim(',') + ") values " + resultValue.ToString().Trim(',') + ")";
        }

        #endregion

        #region UpdateSql

        /// <summary>
        /// 获取Update语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetUpdateSql()
        {
            var updateCmd = "update " + GetType().Name + " set ";
            var resultFiled = new StringBuilder();
            var properties = GetType().GetProperties();
            int primaryValue = -1;
            var primarykeyName = GetPrimarykeyName();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.Name.ToLower() != primarykeyName.ToLower())
                {
                    var type = propertyInfo.PropertyType.UnderlyingSystemType;
                    //var type = propertyInfo.GetValue(modelChatLog, null).GetType();
                    if (type == typeof(int))
                    {
                        resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + "=" + propertyInfo.GetValue(this, null) + ",");
                    }
                    else if (type == typeof(DateTime))
                    {
                        var val = Convert.ToDateTime(propertyInfo.GetValue(this, null));
                        if (val == Operate.ConstantCollection.NetDefaultDateTime)
                        {
                            val = Operate.ConstantCollection.DbDefaultDateTime;
                        }
                        resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + "='" + val.ToString(CultureInfo.InvariantCulture) + "',");
                    }
                    else
                    {
                        resultFiled.AppendFormat(AddFieldProtection(propertyInfo.Name) + "='" + propertyInfo.GetValue(this, null) + "',");
                    }
                }
                else
                {
                    primaryValue = Convert.ToInt32(propertyInfo.GetValue(this, null));
                }
            }

            return updateCmd + resultFiled.ToString().Trim(',') + " where " + primarykeyName + "=" + primaryValue;
        }

        #endregion

        #region DeleteSql

        /// <summary>
        /// 获取Delete语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetDeleteSql(string where = null)
        {
            var deleteCmd = "delete from " + GetType().Name;
            var properties = GetType().GetProperties();
            int primaryValue = -1;
            var primarykeyName = GetPrimarykeyName();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.Name.ToLower() == primarykeyName.ToLower())
                {
                    primaryValue = Convert.ToInt32(propertyInfo.GetValue(this, null));
                }
            }

            if (where.IsNullOrEmpty())
            {
                return deleteCmd + " where " + primarykeyName + "=" + primaryValue;
            }
            return deleteCmd + " where " + where;
        }

        /// <summary>
        /// 获取Delete语句
        /// </summary>
        /// <returns></returns>
        public virtual string GetDeleteSqlByPrimary(object primaryValue)
        {
            if (primaryValue == null) throw new ArgumentNullException("primaryValue");
            var deleteCmd = "delete from " + AddFieldProtection(GetType().Name);
            string searchText = string.Empty;
            var properties = GetType().GetProperties();
            var primarykeyName = GetPrimarykeyName();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.Name.ToLower() == primarykeyName)
                {
                    primaryValue = propertyInfo.GetValue(this, null);

                    if (primaryValue.IsInt())
                    {
                        searchText = " where {0} = {1} ".FormatValue(AddFieldProtection(propertyInfo.Name), primaryValue);
                    }
                    else if (primaryValue.IsDateTime())
                    {
                        searchText = " where {0} = {1} ".FormatValue(AddFieldProtection(propertyInfo.Name), primaryValue);
                    }
                    else
                    {
                        searchText = " where {0} = '{1}' ".FormatValue(AddFieldProtection(propertyInfo.Name), primaryValue);
                    }
                }
            }
            return deleteCmd + searchText;
        }

        #endregion

        #region FilterSql

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="propertyNameList"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params string[] propertyNameList)
        {
            var result = new StringBuilder();
            var entityType = GetType();

            foreach (var cf in propertyNameList)
            {
                var type = entityType.GetProperty(cf).PropertyType;
                var typeCode = Type.GetTypeCode(type);
                if (typeCode == TypeCode.String)
                {
                    result.AppendFormat(" {0} = '{1}' and", cf, GetType().GetProperty(cf).GetValue(this, null));
                }

                if (typeCode == TypeCode.Int16 || typeCode == TypeCode.Int32 || typeCode == TypeCode.Int64 || typeCode == TypeCode.Decimal || typeCode == TypeCode.Double || typeCode == TypeCode.Single || typeCode == TypeCode.UInt16 || typeCode == TypeCode.UInt32 || typeCode == TypeCode.UInt64 || typeCode == TypeCode.SByte || typeCode == TypeCode.Byte)
                {
                    result.AppendFormat(" {0} = {1} and", cf, GetType().GetProperty(cf).GetValue(this, null));
                }
            }

            var r = result.ToString();
            r = r.TrimEnd("and".ToCharArray());

            return r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string GetMultiFilterString<T>(ICollection<T> list, params string[] propertyNameList) where T : BaseEntity
        {
            var result = new StringBuilder();

            if (list == null || list.Count == 0)
            {
                return " 1 = 1 ";
            }

            T first = null;
            foreach (var item in list)
            {
                first = item;
                break;
            }

            if (list.Count == 1)
            {
                return first.GetFilterString(propertyNameList);
            }

            Type entityType = first.GetType();
            result.Append(" ");
            if (propertyNameList.Length == 1)
            {
                var cf = propertyNameList[0];

                var valueList = new List<object>();
                foreach (var item in list)
                {
                  var v=  entityType.GetProperty(cf).GetValue(item, null);
                    valueList.Add(v);
                }

                var InWhere = "";
                var type = entityType.GetProperty(cf).PropertyType;
                var typeCode = Type.GetTypeCode(type);
                if (typeCode == TypeCode.String)
                {
                    InWhere ="("+ valueList.Join(",","'{0}'")+")";
                }

                if (typeCode == TypeCode.Int16 || typeCode == TypeCode.Int32 || typeCode == TypeCode.Int64 || typeCode == TypeCode.Decimal || typeCode == TypeCode.Double || typeCode == TypeCode.Single || typeCode == TypeCode.UInt16 || typeCode == TypeCode.UInt32 || typeCode == TypeCode.UInt64 || typeCode == TypeCode.SByte || typeCode == TypeCode.Byte)
                {
                    InWhere = "(" + valueList.Join(",", "{0}") + ")";
                }

                result.AppendFormat("{0} In {1}", cf, InWhere);
            }
            else
            {
                var orWhereList = new List<string>();
                foreach (var item in list)
                {
                    var whereItem = item.GetFilterString(propertyNameList);
                    orWhereList.Add(whereItem);
                }

                result.AppendFormat(orWhereList.Join(" Or ","({0})"));
            }
            result.Append(" ");
            var r = result.ToString();
            return r;
        }

        #endregion

        #region MappingXml

        /// <summary>
        /// CreateMappingXml
        /// </summary>
        /// <returns></returns>
        public virtual string CreateMappingXml()
        {
            string result = MappingTemplate.XmlTemplate;
            var type = GetType();

            var primaryMapping = "";
            var propertiesMapping = "";

            var properties = type.GetProperties();
            foreach (var p in properties)
            {
                var kettype = p.GetCustomAttributes(typeof(KeyAttribute), false);
                if (kettype.Length != 0)
                {
                    var comment = GetAttribute<CommentAttribute>(p);
                    primaryMapping = MappingTemplate.BuildKeyMapping(p.Name, p.PropertyType.Name, comment);
                }
                else
                {
                    var comment = GetAttribute<CommentAttribute>(p);
                    var sqltype = GetAttribute<SqlTypeAttribute>(p);
                    var length = GetAttribute<LengthAttribute>(p);
                    var precision = GetAttribute<PrecisionAttribute>(p);
                    var scale = GetAttribute<ScaleAttribute>(p);
                    propertiesMapping += MappingTemplate.BuildPropertyMapping(p.Name, p.PropertyType.Name, comment, sqltype, length, precision, scale);
                }
            }

            result = MappingTemplate.ReplaceTemplate(result, primaryMapping, propertiesMapping, type.Assembly.ManifestModule.Name.Replace(".dll", ""), type.Namespace, type.FullName, type.Name);

            return result;
        }

        #endregion

        #region Attribute

        /// <summary>
        /// GetAttribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p"></param>
        /// <returns></returns>
        private string GetAttribute<T>(PropertyInfo p) where T : BaseAttribute
        {
            var attribute = p.GetCustomAttributes(typeof(T), false)[0] as T;
            var name = attribute == null ? "" : attribute.Name;
            return name;
        }

        #endregion

        #region FieldProtection

        /// <summary>
        /// AddFieldProtection
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        private string AddFieldProtection(string columnName)
        {
            return FieldProtectionLeft + columnName + FieldProtectionRight;
        }

        #endregion

        #endregion
    }
}
