﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/


using System;
using Newtonsoft.Json;

namespace Operate.DataBase.SqlDatabase.Model
{
    /// <summary>
    /// BaseModel
    /// </summary>
    public class BaseModel : Object
    {
        /// <summary>
        /// 获取类名
        /// </summary>
        /// <returns></returns>
        public virtual string GetClassName()
        {
            return GetType().Name;
        }

        /// <summary>
        /// 转换为Json字符串
        /// </summary>
        /// <returns></returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// 获取深度复制副本
        /// </summary>
        /// <typeparam name="T">输出类型</typeparam>
        /// <returns></returns>
        public virtual T MegerDefault<T>() where T : BaseModel, new()
        {
            var defaultModel = new T();
            var properties = GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                var v = propertyInfo.GetValue(this, null);
                if (v == null)
                {
                    var value = propertyInfo.GetValue(defaultModel, null);
                    propertyInfo.SetValue(this, value, null);
                }
            }
            return this as T;
        }
    }
}
