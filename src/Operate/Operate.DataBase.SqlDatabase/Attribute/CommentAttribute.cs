﻿namespace Operate.DataBase.SqlDatabase.Attribute
{
    /// <summary>
    /// 注释
    /// </summary>
    public class CommentAttribute : BaseAttribute
    {
        /// <summary>
        /// 初始化CommentAttribute
        /// </summary>
        /// <param name="name"></param>
        public CommentAttribute(string name)
        {
            Name = name;
        }
    }
}
