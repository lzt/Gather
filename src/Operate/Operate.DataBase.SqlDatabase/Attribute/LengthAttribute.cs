﻿namespace Operate.DataBase.SqlDatabase.Attribute
{
    /// <summary>
    /// 
    /// </summary>
    public class LengthAttribute : BaseAttribute
    {
        /// <summary>
        /// LengthAttribute
        /// </summary>
        /// <param name="name"></param>
        public LengthAttribute(string name)
        {
            Name = name;
        }
    }
}
