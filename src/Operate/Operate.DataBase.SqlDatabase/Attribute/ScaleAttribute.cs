﻿namespace Operate.DataBase.SqlDatabase.Attribute
{
    /// <summary>
    /// ScaleAttribute
    /// </summary>
    public class ScaleAttribute : BaseAttribute
    {
        /// <summary>
        /// SqlTypeAttribute
        /// </summary>
        /// <param name="name"></param>
        public ScaleAttribute(string name)
        {
            Name = name;
        }
    }
}
