﻿namespace Operate.DataBase.SqlDatabase.Attribute
{
    /// <summary>
    /// SqlTypeAttribute
    /// </summary>
    public class SqlTypeAttribute : BaseAttribute
    {
        /// <summary>
        /// SqlTypeAttribute
        /// </summary>
        /// <param name="name"></param>
        public SqlTypeAttribute(string name)
        {
            Name = name;
        }
    }
}
