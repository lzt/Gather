﻿namespace Operate.DataBase.SqlDatabase.Attribute
{
    /// <summary>
    /// PrecisionAttribute
    /// </summary>
    public class PrecisionAttribute : BaseAttribute
    {
        /// <summary>
        /// SqlTypeAttribute
        /// </summary>
        /// <param name="name"></param>
        public PrecisionAttribute(string name)
        {
            Name = name;
        }
    }
}
