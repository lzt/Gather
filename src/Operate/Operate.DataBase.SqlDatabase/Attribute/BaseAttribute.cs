﻿namespace Operate.DataBase.SqlDatabase.Attribute
{
    /// <summary>
    /// BaseAttribute
    /// </summary>
    public class BaseAttribute : System.Attribute
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
