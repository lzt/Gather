﻿using System;
using System.Collections.Generic;
using ServiceStack.Redis;

namespace Operate.DataBase.NoSqlDataBase.RedisAssistant.ServiceStackRedis
{
    /// <summary>
    /// Redis辅助
    /// </summary>
    public class ServiceStackRedisAssistant : IRedisAssistant
    {
        private const int DefaultPooledRedisConnectionCount = 5;

        #region Properties

        /// <summary>
        /// 连接池连接数
        /// </summary>
        public int PooledRedisConnectionCount { get; set; }

        /// <summary>
        /// WriteHosts
        /// </summary>
        public List<string> WriteHosts { get; set; }

        /// <summary>
        /// ReadHosts
        /// </summary>
        public List<string> ReadHosts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PooledRedisClientManager Prcm { get; private set; }

        #endregion

        #region Method

        #region Constructor

        /// <summary>
        /// RedisAssistant
        /// </summary>
        /// <param name="readWriteHosts"></param>
        /// <param name="readOnlyHosts"></param>
        /// <param name="pooledRedisConnectionCount"></param>
        public ServiceStackRedisAssistant(List<string> readWriteHosts, List<string> readOnlyHosts, int pooledRedisConnectionCount = DefaultPooledRedisConnectionCount)
        {
            WriteHosts = readWriteHosts;
            ReadHosts = readOnlyHosts;
            Prcm = CreateRedisManager(WriteHosts.ToArray(), ReadHosts.ToArray());
            Prcm.ConnectTimeout = 500;
            PooledRedisConnectionCount = pooledRedisConnectionCount;
        }

        #endregion

        #region Function

        /// <summary>
        /// 创建Redis连接池管理对象
        /// </summary>
        private PooledRedisClientManager CreateRedisManager(string[] readWriteHosts, string[] readOnlyHosts)
        {
            //支持读写分离，均衡负载
            return new PooledRedisClientManager(readWriteHosts, readOnlyHosts, new RedisClientManagerConfig
            {
                MaxWritePoolSize = readWriteHosts.Length > PooledRedisConnectionCount ? readWriteHosts.Length * 2 : PooledRedisConnectionCount,
                MaxReadPoolSize = readOnlyHosts.Length > PooledRedisConnectionCount ? readOnlyHosts.Length * 2 : PooledRedisConnectionCount,
                AutoStart = true,
            });
        }

        #endregion

        #region List

        #region Get

        /// <summary>
        /// GetAllItemsFromList
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public List<string> GetAllItemsFromList(string listId)
        {
            using (IRedisClient client = Prcm.GetReadOnlyClient())
            {
                return client.GetAllItemsFromList(listId);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// AddItemToList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="value"></param>
        public void AddItemToList(string listId, string value)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                client.AddItemToList(listId, value);
            }
        }

        /// <summary>
        /// AddRangeToList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="values"></param>
        public void AddRangeToList(string listId, List<string> values)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                client.AddRangeToList(listId, values);
            }
        }

        #endregion

        #region Remove

        /// <summary>
        /// RemoveItemFromList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long RemoveItemFromList(string listId, string value)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                return client.RemoveItemFromList(listId, value);
            }
        }

        /// <summary>
        /// RemoveItemFromList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="value"></param>
        /// <param name="noOfMatches"></param>
        /// <returns></returns>
        public long RemoveItemFromList(string listId, string value, int noOfMatches)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                return client.RemoveItemFromList(listId, value, noOfMatches);
            }
        }

        /// <summary>
        /// RemoveAllFromList
        /// </summary>
        /// <param name="listId"></param>
        public void RemoveAllFromList(string listId)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                using (var trans = client.CreateTransaction())
                {
                    try
                    {
                        client.RemoveAllFromList(listId);

                        //经测试发现当list中只有一项的时候，ServiceStack.Redis中的RemoveAllFromList方法无效，故加入此判断
                        var list = client.GetAllItemsFromList(listId);
                        if (list.Count > 0)
                        {
                            foreach (var item in list)
                            {
                                client.RemoveItemFromList(listId, item);
                            }
                        }
                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Get

        /// <summary>
        /// 获取缓存的对象
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            using (IRedisClient client = Prcm.GetReadOnlyClient())
            {
                return client.Get<T>(key);
            }
        }

        #endregion

        #region Set

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        public bool Set<T>(string key, T value)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                return client.Set(key, value);
            }
        }

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="expiresAt">过期</param>
        /// <returns></returns>
        public bool Set<T>(string key, T value, DateTime expiresAt)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                return client.Set(key, value, expiresAt);
            }
        }

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="expiresIn">过期</param>
        /// <returns></returns>
        public bool Set<T>(string key, T value, TimeSpan expiresIn)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                return client.Set(key, value, expiresIn);
            }
        }

        #endregion

        #region Remove

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">键</param>
        public bool Remove(string key)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                return client.Remove(key);
            }
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="ketList">键集合</param>
        public void Remove(List<string> ketList)
        {
            using (IRedisClient client = Prcm.GetClient())
            {
                client.RemoveAll(ketList);
            }
        }

        #endregion

        #endregion
    }
}
