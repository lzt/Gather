﻿using System.Collections.Generic;

namespace Operate.DataBase.NoSqlDataBase.RedisAssistant
{
    /// <summary>
    /// Redis辅助
    /// </summary>
    public interface IRedisAssistant : ICacheAssistant
    {
        #region Method

        #region List

        #region Get

        /// <summary>
        /// GetAllItemsFromList
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        List<string> GetAllItemsFromList(string listId);

        #endregion

        #region Add

        /// <summary>
        /// AddItemToList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="value"></param>
         void AddItemToList(string listId, string value);

        /// <summary>
        /// AddRangeToList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="values"></param>
        void AddRangeToList(string listId, List<string> values);

        #endregion

        #region Remove

        /// <summary>
        /// RemoveItemFromList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        long RemoveItemFromList(string listId, string value);

        /// <summary>
        /// RemoveItemFromList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="value"></param>
        /// <param name="noOfMatches"></param>
        /// <returns></returns>
        long RemoveItemFromList(string listId, string value, int noOfMatches);

        /// <summary>
        /// RemoveAllFromList
        /// </summary>
        /// <param name="listId"></param>
        void RemoveAllFromList(string listId);

        #endregion

        #endregion

        #region Remove

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">键</param>
        bool Remove(string key);

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="ketList">键集合</param>
        void Remove(List<string> ketList);

        #endregion

        #endregion
    }
}
