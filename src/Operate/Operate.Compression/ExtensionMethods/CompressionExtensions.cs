﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Diagnostics.CodeAnalysis;

using System.IO;
using System.IO.Compression;
using System.Text;
using Operate.Compression.ExtensionMethods.Enums;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Compression.ExtensionMethods
{
    /// <summary>
    /// Extension methods dealing with compression
    /// </summary>
    public static class CompressionExtensions
    {
        #region Functions

        #region Compress

        /// <summary>
        /// Compresses the data using the specified compression type
        /// </summary>
        /// <param name="data">Data to compress</param>
        /// <param name="compressionType">Compression type</param>
        /// <returns>The compressed data</returns>
        [SuppressMessage("Microsoft.Usage","CA2202:DoNotDisposeObjectsMultipleTimes")]
        public static byte[] Compress(this byte[] data, CompressionType compressionType = CompressionType.Deflate)
        {
            if (data.IsNull()) { throw new ArgumentNullException("data"); }
            using (var stream = new MemoryStream())
            {
                using (var zipStream = GetStream(stream, CompressionMode.Compress, compressionType))
                {
                    zipStream.Write(data, 0, data.Length);
                    zipStream.Close();
                    return stream.ToArray();
                }
            }
        }

        /// <summary>
        /// Compresses a string of data
        /// </summary>
        /// <param name="data">Data to Compress</param>
        /// <param name="encodingUsing">Encoding that the data uses (defaults to UTF8)</param>
        /// <param name="compressionType">The compression type used</param>
        /// <returns>The data Compressed</returns>
        public static string Compress(this string data, Encoding encodingUsing = null, CompressionType compressionType = CompressionType.Deflate)
        {
            if (data.IsNullOrEmpty()) { throw new ArgumentNullException("data"); }
            return data.ToByteArray(encodingUsing).Compress(compressionType).ToString(Base64FormattingOptions.None);
        }

        #endregion

        #region Decompress

        /// <summary>
        /// Decompresses the byte array that is sent in
        /// </summary>
        /// <param name="data">Data to decompress</param>
        /// <param name="compressionType">The compression type used</param>
        /// <returns>The data decompressed</returns>
        [SuppressMessage("Microsoft.Usage", "CA2202:DoNotDisposeObjectsMultipleTimes")]
        public static byte[] Decompress(this byte[] data, CompressionType compressionType = CompressionType.Deflate)
        {
            if (data.IsNull()) { throw new ArgumentNullException("data"); }
            using (var stream = new MemoryStream())
            {
                using (var dataStream = new MemoryStream(data))
                {
                    using (Stream zipStream = GetStream(dataStream, CompressionMode.Decompress, compressionType))
                    {
                        var buffer = new byte[4096];
                        while (true)
                        {
                            int size = zipStream.Read(buffer, 0, buffer.Length);
                            if (size > 0) stream.Write(buffer, 0, size);
                            else break;
                        }
                        zipStream.Close();
                        return stream.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Decompresses a string of data
        /// </summary>
        /// <param name="data">Data to decompress</param>
        /// <param name="encodingUsing">Encoding that the result should use (defaults to UTF8)</param>
        /// <param name="compressionType">The compression type used</param>
        /// <returns>The data decompressed</returns>
        public static string Decompress(this string data, Encoding encodingUsing = null, CompressionType compressionType = CompressionType.Deflate)
        {
            if (data.IsNullOrEmpty()) { throw new ArgumentNullException("data"); }
            return data.FromBase64().Decompress(compressionType).ToString(encodingUsing);
        }

        #endregion

        #region GetStream

        private static Stream GetStream(MemoryStream stream, CompressionMode mode, CompressionType compressionType)
        {
            if (compressionType == CompressionType.Deflate)
                return new DeflateStream(stream, mode, true);
            return new GZipStream(stream, mode, true);
        }

        #endregion

        #endregion
    }
}