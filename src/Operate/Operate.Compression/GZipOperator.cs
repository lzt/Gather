﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Compression;

namespace Operate.Compression
{
    /// <summary>
    /// GZip处理类
    /// </summary>
   public class GZipOperator
    {
        /// <summary>
        /// 解压
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DataSet GetDatasetByString(string value)
        {
            var ds = new DataSet();
            string cc = GZipDecompressString(value);
            var sr = new StringReader(cc);
            ds.ReadXml(sr);
            return ds;
        }

        /// <summary>
        /// 根据DATASET压缩字符串
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static string GetStringByDataset(string ds)
        {
            return GZipCompressString(ds);
        }

        /// <summary>
        /// 将传入字符串以GZip算法压缩后，返回Base64编码字符
        /// </summary>
        /// <param name="rawString">需要压缩的字符串</param>
        /// <returns>压缩后的Base64编码的字符串</returns>
        public static string GZipCompressString(string rawString)
        {
            if (string.IsNullOrEmpty(rawString) || rawString.Length == 0)
            {
                return "";
            }
            byte[] rawData = System.Text.Encoding.UTF8.GetBytes(rawString.ToString(CultureInfo.InvariantCulture));
            byte[] zippedData = Compress(rawData);
            return Convert.ToBase64String(zippedData);
        }

        /// <summary>
        /// GZip压缩
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        static byte[] Compress(byte[] rawData)
        {
            var ms = new MemoryStream();
            var compressedzipStream = new GZipStream(ms, CompressionMode.Compress, true);
            compressedzipStream.Write(rawData, 0, rawData.Length);
            compressedzipStream.Close();
            return ms.ToArray();
        }


        /// <summary>
        /// 将传入的二进制字符串资料以GZip算法解压缩
        /// </summary>
        /// <param name="zippedString">经GZip压缩后的二进制字符串</param>
        /// <returns>原始未压缩字符串</returns>
        public static string GZipDecompressString(string zippedString)
        {
            if (string.IsNullOrEmpty(zippedString) || zippedString.Length == 0)
            {
                return "";
            }
            byte[] zippedData = Convert.FromBase64String(zippedString);
            return System.Text.Encoding.UTF8.GetString(Decompress(zippedData));
        }


        /// <summary>
        /// ZIP解压
        /// </summary>
        /// <param name="zippedData"></param>
        /// <returns></returns>
        public static byte[] Decompress(byte[] zippedData)
        {
            var ms = new MemoryStream(zippedData);
            var compressedzipStream = new GZipStream(ms, CompressionMode.Decompress);
            var outBuffer = new MemoryStream();
            var block = new byte[1024];
            while (true)
            {
                int bytesRead = compressedzipStream.Read(block, 0, block.Length);
                if (bytesRead <= 0)
                    break;
                outBuffer.Write(block, 0, bytesRead);
            }
            compressedzipStream.Close();
            return outBuffer.ToArray();
        }

        //public static string Zip(string value)
        //{
        //    //Transform string into byte[]  
        //    byte[] byteArray = new byte[value.Length];
        //    int indexBA = 0;
        //    foreach (char item in value.ToCharArray())
        //    {
        //        byteArray[indexBA++] = (byte)item;
        //    }

        //    //Prepare for compress
        //    System.IO.MemoryStream ms = new System.IO.MemoryStream();
        //    System.IO.Compression.GZipStream sw = new System.IO.Compression.GZipStream(ms,
        //        System.IO.Compression.CompressionMode.Compress);

        //    //Compress
        //    sw.Write(byteArray, 0, byteArray.Length);
        //    //Close, DO NOT FLUSH cause bytes will go missing...
        //    sw.Close();

        //    //Transform byte[] zip data to string
        //    byteArray = ms.ToArray();
        //    System.Text.StringBuilder sB = new System.Text.StringBuilder(byteArray.Length);
        //    foreach (byte item in byteArray)
        //    {
        //        sB.Append((char)item);
        //    }
        //    ms.Close();
        //    sw.Dispose();
        //    ms.Dispose();
        //    return sB.ToString();
        //}

        //public static string UnZip(string value)
        //{
        //    //Transform string into byte[]
        //    byte[] byteArray = new byte[value.Length];
        //    int indexBA = 0;
        //    foreach (char item in value.ToCharArray())
        //    {
        //        byteArray[indexBA++] = (byte)item;
        //    }

        //    //Prepare for decompress
        //    System.IO.MemoryStream ms = new System.IO.MemoryStream(byteArray);
        //    System.IO.Compression.GZipStream sr = new System.IO.Compression.GZipStream(ms,
        //        System.IO.Compression.CompressionMode.Decompress);

        //    //Reset variable to collect uncompressed result
        //    byteArray = new byte[byteArray.Length];

        //    //Decompress
        //    int rByte = sr.Read(byteArray, 0, byteArray.Length);

        //    //Transform byte[] unzip data to string
        //    System.Text.StringBuilder sB = new System.Text.StringBuilder(rByte);
        //    //Read the number of bytes GZipStream red and do not a for each bytes in
        //    //resultByteArray;
        //    for (int i = 0; i < rByte; i++)
        //    {
        //        sB.Append((char)byteArray[i]);
        //    }
        //    sr.Close();
        //    ms.Close();
        //    sr.Dispose();
        //    ms.Dispose();
        //    return sB.ToString();
        //}
    }
}
