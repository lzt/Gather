﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Globalization;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Environment
{
    /// <summary>
    /// Parses the args variables of an application
    /// </summary>
    public sealed class ArgsParser
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="optionStarter">The text to determine where an option starts</param>
        public ArgsParser(string optionStarter = "/")
        {
            if (optionStarter.IsNullOrEmpty()) { throw new ArgumentNullException("optionStarter"); }
            OptionStarter = optionStarter;
            OptionRegex = new Regex(string.Format(CultureInfo.InvariantCulture, @"(?<Command>{0}[^\s]+)[\s|\S|$](?<Parameter>""[^""]*""|[^""{0}]*)", optionStarter));
        }

        #endregion

        #region Functions

        /// <summary>
        /// Parses the args into individual options
        /// </summary>
        /// <param name="args">Args to parse</param>
        /// <returns>A list of options</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public IEnumerable<Option> Parse(string[] args)
        {
            if (args == null)
                return new List<Option>();
            var result = new List<Option>();
            string text = "";
            string splitter = "";
            foreach (string arg in args)
            {
                text += splitter + arg;
                splitter = " ";
            }
            MatchCollection matches = OptionRegex.Matches(text);
            string option = "";
            foreach (Match optionMatch in matches)
            {
                if (optionMatch.Value.StartsWith(OptionStarter, StringComparison.InvariantCulture) && !string.IsNullOrEmpty(option))
                {
                    result.Add(new Option(option, OptionStarter));
                    option = "";
                }
                option += optionMatch.Value + " ";
            }
            result.Add(new Option(option, OptionStarter));
            return result;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Option regex
        /// </summary>
        private Regex OptionRegex { get; set; }

        /// <summary>
        /// String that starts an option
        /// </summary>
        private string OptionStarter { get; set; }

        #endregion
    }
}
