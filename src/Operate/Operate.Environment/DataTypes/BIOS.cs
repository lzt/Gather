﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Management;

#endregion

namespace Operate.Environment.DataTypes
{
    /// <summary>
    /// Represents BIOS information
    /// </summary>
    public sealed class BIOS
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Computer Name</param>
        /// <param name="password">Password</param>
        /// <param name="userName">User Name</param>
        public BIOS(string name = "", string userName = "", string password = "")
        {
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            LoadBIOS(name, userName, password);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Serial Number
        /// </summary>
        public string SerialNumber { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Loads the BIOS info
        /// </summary>
        /// <param name="name">Computer name</param>
        /// <param name="userName">User name</param>
        /// <param name="password">Password</param>
        // ReSharper disable InconsistentNaming
        private void LoadBIOS(string name, string userName, string password)
        // ReSharper restore InconsistentNaming
        {
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            ManagementScope scope;
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                var options = new ConnectionOptions();
                options.Username = userName;
                options.Password = password;
                scope = new ManagementScope("\\\\" + name + "\\root\\cimv2", options);
            }
            else
            {
                scope = new ManagementScope("\\\\" + name + "\\root\\cimv2");
            }
            scope.Connect();
            var query = new ObjectQuery("SELECT * FROM Win32_BIOS");
            using (var searcher = new ManagementObjectSearcher(scope, query))
            {
                using (ManagementObjectCollection collection = searcher.Get())
                {
                    foreach (ManagementObject tempBios in collection)
                    {
                        SerialNumber = tempBios.Properties["Serialnumber"].Value.ToString();
                    }
                }
            }
        }

        #endregion
    }
}