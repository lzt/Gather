﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Management;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Environment.DataTypes
{
    /// <summary>
    /// Helper class for searching for files (will be modified later)
    /// </summary>
    public static class Files
    {
        #region Public Static Functions

        /// <summary>
        /// Gets a list of files on the machine with a specific extension
        /// </summary>
        /// <param name="computer">Computer to search</param>
        /// <param name="userName">User name (if not local)</param>
        /// <param name="password">Password (if not local)</param>
        /// <param name="extension">File extension to look for</param>
        /// <returns>List of files that are found to have the specified extension</returns>
        public static IEnumerable<string> GetFilesWithExtension(string computer, string userName, string password, string extension)
        {
            if (computer.IsNullOrEmpty()) { throw new ArgumentNullException("computer"); }
            if (extension.IsNullOrEmpty()) { throw new ArgumentNullException("extension"); }
            var files = new List<string>();
            ManagementScope scope;
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                var options = new ConnectionOptions { Username = userName, Password = password };
                scope = new ManagementScope("\\\\" + computer + "\\root\\cimv2", options);
            }
            else
            {
                scope = new ManagementScope("\\\\" + computer + "\\root\\cimv2");
            }
            scope.Connect();
            var query = new ObjectQuery("SELECT * FROM CIM_DataFile where Extension='" + extension + "'");
            using (var searcher = new ManagementObjectSearcher(scope, query))
            {
                using (ManagementObjectCollection collection = searcher.Get())
                {
                    // ReSharper disable LoopCanBeConvertedToQuery
                    foreach (var o in collection)
                    // ReSharper restore LoopCanBeConvertedToQuery
                    {
                        var tempBIOS = (ManagementObject) o;
                        files.Add(tempBIOS.Properties["Drive"].Value + tempBIOS.Properties["Path"].Value.ToString() + tempBIOS.Properties["FileName"].Value);
                    }
                }
            }
            return files;
        }

        #endregion
    }
}
