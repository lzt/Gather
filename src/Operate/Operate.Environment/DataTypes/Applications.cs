﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections;
using System.Collections.Generic;

using System.Management;

#endregion

namespace Operate.Environment.DataTypes
{
    /// <summary>
    /// Application list
    /// </summary>
    public sealed class Applications : IEnumerable<Application>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Computer Name</param>
        /// <param name="password">Password</param>
        /// <param name="userName">User Name</param>
        public Applications(string name = "", string userName = "", string password = "")
        {
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            LoadApplications(name, userName, password);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Application list
        /// </summary>
        public ICollection<Application> ApplicationList { get; private set; }

        #endregion

        #region Functions

        /// <summary>
        /// Loads applications
        /// </summary>
        /// <param name="name">Computer name</param>
        /// <param name="userName">User name</param>
        /// <param name="password">Password</param>
        private void LoadApplications(string name, string userName, string password)
        {
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            ApplicationList = new List<Application>();
            ManagementScope scope;
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                var options = new ConnectionOptions {Username = userName, Password = password};
                scope = new ManagementScope("\\\\" + name + "\\root\\default", options);
            }
            else
            {
                scope = new ManagementScope("\\\\" + name + "\\root\\default");
            }
            var path = new ManagementPath("StdRegProv");
            using (var registry = new ManagementClass(scope, path, null))
            {
                // ReSharper disable InconsistentNaming
                const uint HKEY_LOCAL_MACHINE = unchecked(0x80000002);
                // ReSharper restore InconsistentNaming
                var args = new object[] { HKEY_LOCAL_MACHINE, @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", null };
                registry.InvokeMethod("EnumKey", args);
                var keys = args[2] as String[];
                using (ManagementBaseObject methodParams = registry.GetMethodParameters("GetStringValue"))
                {
                    methodParams["hDefKey"] = HKEY_LOCAL_MACHINE;
                    if (keys != null)
                        foreach (string subKey in keys)
                        {
                            methodParams["sSubKeyName"] = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" + subKey;
                            methodParams["sValueName"] = "DisplayName";
                            using (ManagementBaseObject results = registry.InvokeMethod("GetStringValue", methodParams, null))
                            {
                                if (results != null && (uint)results["ReturnValue"] == 0)
                                {
                                    var temp = new Application {Name = results["sValue"].ToString()};
                                    ApplicationList.Add(temp);
                                }
                            }
                        }
                }
            }
        }

        #endregion

        #region IEnumerable

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator GetEnumerator()
        {
            if (ApplicationList == null)
                throw new InvalidOperationException("ApplicationList");
            return ApplicationList.GetEnumerator();
        }

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        IEnumerator<Application> IEnumerable<Application>.GetEnumerator()
        {
            if (ApplicationList == null)
                throw new InvalidOperationException("ApplicationList");
            return ApplicationList.GetEnumerator();
        }

        #endregion
    }
}