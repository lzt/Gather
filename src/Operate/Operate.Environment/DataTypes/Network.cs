﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using  Operate.ExtensionMethods;
using System.Management;
using System.Net;

#endregion

namespace Operate.Environment.DataTypes
{
    /// <summary>
    /// Represents network info
    /// </summary>
    public sealed class Network
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Computer name</param>
        /// <param name="password">Password</param>
        /// <param name="userName">Username</param>
        public Network(string name = "", string userName = "", string password = "")
        {
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            GetNetworkInfo(name);
            GetNetworkAdapterInfo(name, userName, password);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Network addresses (IP Addresses, etc.)
        /// </summary>
        public ICollection<NetworkAddress> NetworkAddresses { get; private set; }

        /// <summary>
        /// MAC Address 
        /// </summary>
        public ICollection<NetworkAdapter> MacAddresses { get; private set; }

        #endregion

        #region Functions

        /// <summary>
        /// Gets the network info
        /// </summary>
        /// <param name="name">Computer name</param>
        private void GetNetworkInfo(string name)
        {
            if (name.IsNullOrEmpty()) { throw new ArgumentNullException("name"); }
            NetworkAddresses = new List<NetworkAddress>();
            IPHostEntry hostEntry = Dns.GetHostEntry(name);
            foreach (IPAddress address in hostEntry.AddressList)
            {
                var tempAddress = new NetworkAddress
                    {
                        Type = address.AddressFamily.ToString(),
                        Address = address.ToString()
                    };
                NetworkAddresses.Add(tempAddress);
            }
        }

        /// <summary>
        /// Gets the network adapter info
        /// </summary>
        /// <param name="name">Computer name</param>
        /// <param name="userName">User name</param>
        /// <param name="password">Password</param>
        private void GetNetworkAdapterInfo(string name, string userName, string password)
        {
            if (name.IsNullOrEmpty()) { throw new ArgumentNullException("name"); }
            MacAddresses = new List<NetworkAdapter>();
            ManagementScope scope;
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                var options = new ConnectionOptions {Username = userName, Password = password};
                scope = new ManagementScope("\\\\" + name + "\\root\\cimv2", options);
            }
            else
            {
                scope = new ManagementScope("\\\\" + name + "\\root\\cimv2");
            }
            scope.Connect();
            var query = new ObjectQuery("SELECT * FROM Win32_NetworkAdapterConfiguration");
            using (var searcher = new ManagementObjectSearcher(scope, query))
            {
                using (ManagementObjectCollection collection = searcher.Get())
                {
                    foreach (ManagementObject tempNetworkAdapter in collection)
                    {
                        if (tempNetworkAdapter.Properties["MACAddress"].Value != null)
                        {
                            var adapter = new NetworkAdapter
                                {
                                    Description = tempNetworkAdapter.Properties["Description"].Value.ToString(),
                                    MacAddress = tempNetworkAdapter.Properties["MACAddress"].Value.ToString()
                                };
                            MacAddresses.Add(adapter);
                        }
                    }
                }
            }
        }

        #endregion
    }
}