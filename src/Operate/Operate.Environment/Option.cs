﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Environment
{
    /// <summary>
    /// Contains an individual option
    /// </summary>
    public class Option
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">Option text</param>
        /// <param name="optionStarter">Starter text for an option ("/", "-", etc.)</param>
        public Option(string text, string optionStarter)
        {
            if (text.IsNullOrEmpty()) { throw new ArgumentNullException("text"); }
            if (optionStarter.IsNullOrEmpty()) { throw new ArgumentNullException("optionStarter"); }
            var commandParser = new Regex(string.Format(CultureInfo.InvariantCulture, @"{0}(?<Command>[^\s]*)\s(?<Parameters>.*)", optionStarter));
            var parameterParser = new Regex("(?<Parameter>\"[^\"]*\")[\\s]?|(?<Parameter>[^\\s]*)[\\s]?");
            Parameters = new List<string>();
            Match commandMatch = commandParser.Match(text);
            Command = commandMatch.Groups["Command"].Value;
            text = commandMatch.Groups["Parameters"].Value;
            parameterParser.Matches(text)
                           .Where(x => !string.IsNullOrEmpty(x.Value))
                           .ForEach(x => Parameters.Add(x.Groups["Parameter"].Value));
        }

        #endregion

        #region Functions

        /// <summary>
        /// Converts the options into a string
        /// </summary>
        /// <returns>The string representation of the option</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("Command: ")
                   .Append(Command)
                   .Append("\n")
                   .Append("Parameters: ");
            Parameters.ForEach(x => builder.Append(x).Append(" "));
            return builder.Append("\n")
                          .ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Command string
        /// </summary>
        public string Command { get; set; }

        /// <summary>
        /// List of parameters found
        /// </summary>
        public ICollection<string> Parameters { get;private set; }

        #endregion
    }
}
