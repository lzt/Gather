﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;

using System.Text;
using System.Threading;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.Environment.ExtensionMethods
{
    /// <summary>
    /// Process extensions
    /// </summary>
    public static class ProcessExtensions
    {
        #region Functions

        #region KillProcessAsync

        /// <summary>
        /// Kills a process
        /// </summary>
        /// <param name="process">Process that should be killed</param>
        /// <param name="timeToKill">Amount of time (in ms) until the process is killed.</param>
        public static void KillProcessAsync(this Process process, int timeToKill = 0)
        {
            if (process.IsNull()) { throw new ArgumentNullException("process"); }
            ThreadPool.QueueUserWorkItem(delegate { KillProcessAsyncHelper(process, timeToKill); });
        }

        /// <summary>
        /// Kills a list of processes
        /// </summary>
        /// <param name="processes">Processes that should be killed</param>
        /// <param name="timeToKill">Amount of time (in ms) until the processes are killed.</param>
        public static void KillProcessAsync(this IEnumerable<Process> processes, int timeToKill = 0)
        {
            if (processes.IsNull()) { throw new ArgumentNullException("processes"); }
            processes.ForEach(x => ThreadPool.QueueUserWorkItem(delegate { KillProcessAsyncHelper(x, timeToKill); }));
        }

        #endregion

        #region GetInformation

        /// <summary>
        /// Gets information about all processes and returns it in an HTML formatted string
        /// </summary>
        /// <param name="process">Process to get information about</param>
        /// <param name="htmlFormat">Should this be HTML formatted?</param>
        /// <returns>An HTML formatted string</returns>
        public static string GetInformation(this Process process, bool htmlFormat = true)
        {
            var builder = new StringBuilder();
            return builder.Append(htmlFormat ? "<strong>" : "")
                   .Append(process.ProcessName)
                   .Append(" Information")
                   .Append(htmlFormat ? "</strong><br />" : "\n")
                   .Append(process.ToString(htmlFormat))
                   .Append(htmlFormat ? "<br />" : "\n")
                   .ToString();
        }

        /// <summary>
        /// Gets information about all processes and returns it in an HTML formatted string
        /// </summary>
        /// <param name="processes">Processes to get information about</param>
        /// <param name="htmlFormat">Should this be HTML formatted?</param>
        /// <returns>An HTML formatted string</returns>
        public static string GetInformation(this IEnumerable<Process> processes, bool htmlFormat = true)
        {
            var builder = new StringBuilder();
            processes.ForEach(x => builder.Append(x.GetInformation(htmlFormat)));
            return builder.ToString();
        }

        #endregion

        #endregion

        #region Private Static Functions

        /// <summary>
        /// Kills a process asyncronously
        /// </summary>
        /// <param name="process">Process to kill</param>
        /// <param name="timeToKill">Amount of time until the process is killed</param>
        private static void KillProcessAsyncHelper(Process process, int timeToKill)
        {
            if (timeToKill > 0)
                Thread.Sleep(timeToKill);
            process.Kill();
        }

        #endregion
    }
}