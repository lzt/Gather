﻿namespace Operate.Exception.Base
{
    /// <summary>
    /// BaseException
    /// </summary>
    public class BaseException:System.Exception
    {
        /// <summary>
        /// BaseException
        /// </summary>
        /// <param name="message"></param>
        public BaseException(string message):base(message)
        {
        }
    }
}
