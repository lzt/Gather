﻿using Operate.Exception.Base;
using Operate.ExtensionMethods;

namespace Operate.Exception
{
    /// <summary>
    /// this virtual method need to rewrite.
    /// </summary>
    public class NeedRewriteException:BaseException
    {
        /// <summary>
        /// NeedRewriteException
        /// </summary>
        /// <param name="methodName"></param>
        public NeedRewriteException(string methodName)
            : base("方法\"{0}\"需要重写以实现其功能".FormatValue(methodName))
        {
        }
    }
}
