﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Operate.Exception.Base;
using Operate.ExtensionMethods;

namespace Operate.Exception
{
    /// <summary>
    /// 未知的错误
    /// </summary>
    public class UnknownException : BaseException
    {
        /// <summary>
        /// UnknownException
        /// </summary>
        /// <param name="message"></param>
        public UnknownException(string message = "") : base("未知的错误{0}".FormatValue(string.IsNullOrWhiteSpace(message) ? "" : " -> " + message))
        {
        }
    }
}
