﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Operate.Exception.Base;

namespace Operate.Exception
{
    /// <summary>
    /// 存在null值或无意义的值,在这里不被允许
    /// </summary>
    public class EmptyException:BaseException
    {
        /// <summary>
        /// EmptyException
        /// </summary>
        public EmptyException()
            : base("存在null值或无意义的值,在这里不被允许")
        {
        }
    }
}
