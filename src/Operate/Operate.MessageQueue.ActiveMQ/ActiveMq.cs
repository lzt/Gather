using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.ActiveMQ.Commands;
using Operate.Threading;

namespace Operate.MessageQueue.ActiveMQ
{
    public class ActiveMQ : MessageQueueBase, IMessageSender, IMessageReceiver, IDisposable
    {
        private readonly IConnection _connection;
        private readonly ISession _session;
        private readonly IMessageProducer _producer;
        private readonly IMessageConsumer _consumer;

        /// <summary>
        /// ActiveMQConnection
        /// </summary>
        public ActiveMQConnection Connection { get; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsStarted
        {
            get
            {
                // ReSharper disable once ConvertPropertyToExpressionBody
                return _connection.IsStarted;
            }
        }

        /// <summary>
        /// ActiveMq
        /// </summary>
        /// <param name="conn"></param>
        public ActiveMQ(ActiveMQConnection conn)
        {
            Connection = conn;
            var factory = new NMSConnectionFactory(conn.BrokerUri);
            _connection = factory.CreateConnection(conn.User, conn.Password);
            _connection.Start();
            _session = _connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
            IDestination dest = _session.GetTopic(conn.Destination);
            _producer = _session.CreateProducer(dest);
            _producer.DeliveryMode = MsgDeliveryMode.NonPersistent;

            _consumer = _session.CreateConsumer(dest);

            TaskScheduler = null;
            TaskList = new List<Task>();

            if (Connection.MaxThread <= 1)
            {
                Connection.ThreadMode = ThreadMode.SingleThread;
                Connection.MaxThread = MQConnectionBase.DefaultMaxThread;
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskScheduler = new OperateTaskScheduler(Connection.MaxThread);
            }
        }

        protected override bool GetUserLogger()
        {
            return Connection.UseLogger;
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(byte[] message)
        {
            var msg = new ActiveMQBytesMessage { Content = message };
            Send(msg);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(string message)
        {
            var msg = new ActiveMQTextMessage { Text = message };
            Send(msg);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(IMessage message)
        {
            Send<IMessage>(message);
        }

        /// <summary>
        ///Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(IBytesMessage message)
        {
            Send<IBytesMessage>(message);
        }

        /// <summary>
        ///Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(ITextMessage message)
        {
            Send<ITextMessage>(message);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(IMapMessage message)
        {
            Send<IMapMessage>(message);
        }

        /// <summary>
        /// TrySend（仅使用单线程）
        /// </summary>
        /// <param name="message"></param>
        public void Send(IStreamMessage message)
        {
            Send<IStreamMessage>(message);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void Send<T>(T message) where T : class, IMessage
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                bool result;
                TrySend(message, out result);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(SendCallByAsync, message);

                if (TaskList.Count >= Connection.MaxThread*2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        /// <summary>
        /// TrySend（仅使用单线程）
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void TrySend(string message, out bool result)
        {
            var msg = new ActiveMQTextMessage { Text = message };
            TrySend(msg, out result);
        }

        /// <summary>
        /// TrySend（仅使用单线程）
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void TrySend(byte[] message, out bool result)
        {
            var msg = new ActiveMQBytesMessage { Content = message };
            TrySend(msg, out result);
        }

        private void SendCallByAsync<T>(T message) where T : class, IMessage
        {
            bool result;
            TrySend(message, out result);
            Thread.Sleep(50);
        }

        /// <summary>
        /// TrySend（仅使用单线程）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public void TrySend<T>(T message, out bool result) where T : IMessage
        {
            result = false;
            try
            {
                {
                    var objMessage = message as ITextMessage;
                    if (objMessage != null)
                    {
                        _producer.Send(objMessage);

                        result = true;
                    }
                }

                {
                    var objMessage = message as IMapMessage;
                    if (objMessage != null)
                    {
                        _producer.Send(objMessage);

                        result = true;
                    }
                }

                {
                    var objMessage = message as IStreamMessage;
                    if (objMessage != null)
                    {
                        _producer.Send(objMessage);

                        result = true;
                    }
                }

                {
                    var objMessage = message as IBytesMessage;
                    if (objMessage != null)
                    {
                        _producer.Send(objMessage);

                        result = true;
                    }
                }

                {
                    var objMessage = message as IMessage;
                    if (objMessage != null)
                    {
                        _producer.Send(objMessage);

                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                if (Connection.UseLogger)
                {
                    Logger.RecordException(ex);
                }
            }
        }

        /// <summary>
        /// ReceiveMessage
        /// </summary>
        /// <param name="messageReceiver"></param>
        public void ReceiveMessage(Action<object> messageReceiver)
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                var obj = GetReceiveMessage();
                messageReceiver(obj);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(messageReceiver, GetReceiveMessage());

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        /// <summary>
        /// ReceiveMessage
        /// </summary>
        /// <param name="messageReceiver"></param>
        public void ReceiveMessage<T>(Action<T> messageReceiver) where T : class, IMessage
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                var obj = GetReceiveMessage<T>();
                messageReceiver(obj);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(messageReceiver, GetReceiveMessage<T>());

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        /// <summary>
        /// GetReceiveMessage
        /// </summary>
        /// <returns></returns>
        public object GetReceiveMessage()
        {
            IMessage msg = _consumer.Receive();
            return msg;
        }

        /// <summary>
        /// 接受类型限制为ITextMessage，IMapMessage，IStreamMessage，IMessage
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public T GetReceiveMessage<T>() where T : class, IMessage
        {
            return GetReceiveMessage() as T;
        }

        private bool _disposed;

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (_disposed) return;

            // ReSharper disable once UseNullPropagation
            if (TaskScheduler != null)
            {
                TaskScheduler.Dispose();
            }

            _producer.Close();
            _producer.Dispose();

            if (_consumer != null)
            {
                _consumer.Close();
                _consumer.Dispose();
            }

            _session.Close();
            _session.Dispose();

            _connection.Stop();
            _connection.Close();
            _connection.Dispose();

            _disposed = true;
        }
    }
}