﻿using System;
using System.Collections.Generic;

namespace Operate.MessageQueue.ActiveMQ
{
    public class ActiveMQConnection: MQConnectionBase
    {
        #region Properties

        public string User { get; private set; }

        public string Password { get; private set; }

        public string Host { get; }

        public int Port { get; }

        public string Destination { get; private set; }

        public int Messages { get; private set; }

        public int Size { get; private set; }

        public string BrokerUri { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// ActiveMQConnection
        /// Max Messages:1000000,Max Size:2048
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <param name="port"></param>
        public ActiveMQConnection(string user, string password, string host, string port)
            : this(user, password, host, port, "event", 1000000, 2048)
        {
        }

        /// <summary>
        /// ActiveMQConnection
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <param name="port"></param>
        ///  /// <param name="destination"></param>
        /// <param name="messages"></param>
        /// <param name="size"></param>
        public ActiveMQConnection(string user, string password, string host, string port, string destination, int messages, int size)
        {
            User = Env("ACTIVEMQ_USER", user);
            Password = Env("ACTIVEMQ_PASSWORD", password);
            Host = Env("ACTIVEMQ_HOST", host);
            Port = int.Parse(Env("ACTIVEMQ_PORT", port));

            Destination = Arg(new List<string>(), 0, destination);

            Messages = messages;
            Size = size;

            BrokerUri = "activemq:tcp://" + Host + ":" + Port;

            ThreadMode = ThreadMode.SingleThread;
            MaxThread = DefaultMaxThread;
            UseLogger = false;
        }

        #endregion

        #region Private Methods

        private string Env(string key, string defaultValue)
        {
            string rc = Environment.GetEnvironmentVariable(key);
            if (rc == null)
            {
                return defaultValue;
            }
            return rc;
        }

        private static string Arg(IList<string> args, int index, String defaultValue)
        {
            if (index < args.Count)
            {
                return args[index];
            }
            return defaultValue;
        }

        #endregion
    }
}
