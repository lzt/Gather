﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Operate.DataMapper.Interfaces;
using Operate.ExtensionMethods;

#endregion

namespace Operate.DataMapper
{
    /// <summary>
    /// Maps two types together
    /// </summary>
    /// <typeparam name="TLeft">Left type</typeparam>
    /// <typeparam name="TRight">Right type</typeparam>
    public class TypeMapping<TLeft, TRight> : ITypeMapping
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public TypeMapping() { Mappings = new List<Mapping<TLeft, TRight>>(); }

        #endregion

        #region Properties

        /// <summary>
        /// List of mappings
        /// </summary>
        protected ICollection<Mapping<TLeft, TRight>> Mappings { get; private set; }

        #endregion

        #region Functions

        /// <summary>
        /// Automatically maps properties that are named the same thing
        /// </summary>
        public virtual TypeMapping<TLeft, TRight> AutoMap()
        {
            PropertyInfo[] properties = typeof(TLeft).GetProperties();
            Type destinationType = typeof(TRight);
            foreach (PropertyInfo t in properties)
            {
                PropertyInfo destinationProperty = destinationType.GetProperty(t.Name);
                if (destinationProperty != null)
                {
                    Expression<Func<TLeft, object>> leftGet = t.PropertyGetter<TLeft>();
                    Expression<Action<TLeft, object>> leftSet = leftGet.PropertySetter();
                    Expression<Func<TRight, object>> rightGet = destinationProperty.PropertyGetter<TRight>();
                    Expression<Action<TRight, object>> rightSet = rightGet.PropertySetter();
                    AddMapping(leftGet.Compile(), leftSet.Compile(), rightGet.Compile(), rightSet.Compile());
                }
            }
            return this;
        }

        /// <summary>
        /// Adds a mapping
        /// </summary>
        /// <param name="leftExpression">Left expression</param>
        /// <param name="rightExpression">Right expression</param>
        public virtual TypeMapping<TLeft, TRight> AddMapping(Expression<Func<TLeft, object>> leftExpression, Expression<Func<TRight, object>> rightExpression)
        {
            Mappings.Add(new Mapping<TLeft, TRight>(leftExpression, rightExpression));
            return this;
        }

        /// <summary>
        /// Adds a mapping
        /// </summary>
        /// <param name="leftGet">Left get function</param>
        /// <param name="leftSet">Left set action</param>
        /// <param name="rightExpression">Right expression</param>
        public virtual TypeMapping<TLeft, TRight> AddMapping(Func<TLeft, object> leftGet, Action<TLeft, object> leftSet, Expression<Func<TRight, object>> rightExpression)
        {
            Mappings.Add(new Mapping<TLeft, TRight>(leftGet, leftSet, rightExpression));
            return this;
        }

        /// <summary>
        /// Adds a mapping
        /// </summary>
        /// <param name="leftExpression">Left expression</param>
        /// <param name="rightGet">Right get function</param>
        /// <param name="rightSet">Right set function</param>
        public virtual TypeMapping<TLeft, TRight> AddMapping(Expression<Func<TLeft, object>> leftExpression, Func<TRight, object> rightGet, Action<TRight, object> rightSet)
        {
            Mappings.Add(new Mapping<TLeft, TRight>(leftExpression, rightGet, rightSet));
            return this;
        }

        /// <summary>
        /// Adds a mapping
        /// </summary>
        /// <param name="leftGet">Left get function</param>
        /// <param name="leftSet">Left set function</param>
        /// <param name="rightGet">Right get function</param>
        /// <param name="rightSet">Right set function</param>
        public virtual TypeMapping<TLeft, TRight> AddMapping(Func<TLeft, object> leftGet, Action<TLeft, object> leftSet, Func<TRight, object> rightGet, Action<TRight, object> rightSet)
        {
            Mappings.Add(new Mapping<TLeft, TRight>(leftGet, leftSet, rightGet, rightSet));
            return this;
        }

        /// <summary>
        /// Copies from the source to the destination
        /// </summary>
        /// <param name="source">Source object</param>
        /// <param name="destination">Destination object</param>
        public void Copy(TLeft source, TRight destination)
        {
            foreach (Mapping<TLeft, TRight> mapping in Mappings)
            {
                mapping.Copy(source, destination);
            }
        }

        /// <summary>
        /// Copies from the source to the destination
        /// </summary>
        /// <param name="source">Source object</param>
        /// <param name="destination">Destination object</param>
        public void Copy(TRight source, TLeft destination)
        {
            foreach (Mapping<TLeft, TRight> mapping in Mappings)
            {
                mapping.Copy(source, destination);
            }
        }

        /// <summary>
        /// Copies from the source to the destination (used in 
        /// instances when both Left and Right are the same type
        /// and thus Copy is ambiguous)
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="destination">Destination</param>
        public void CopyLeftToRight(TLeft source, TRight destination)
        {
            foreach (Mapping<TLeft, TRight> mapping in Mappings)
            {
                mapping.CopyLeftToRight(source, destination);
            }
        }

        /// <summary>
        /// Copies from the source to the destination (used in 
        /// instances when both Left and Right are the same type
        /// and thus Copy is ambiguous)
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="destination">Destination</param>
        public void CopyRightToLeft(TRight source, TLeft destination)
        {
            foreach (Mapping<TLeft, TRight> mapping in Mappings)
            {
                mapping.CopyRightToLeft(source, destination);
            }
        }

        #endregion
    }
}