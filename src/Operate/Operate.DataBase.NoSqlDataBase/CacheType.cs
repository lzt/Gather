﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Operate.DataBase.NoSqlDataBase
{
    /// <summary>
    /// 缓存类型
    /// </summary>
    public enum CacheType
    {
        /// <summary>
        /// 无缓存
        /// </summary>
        [Description("无缓存")]
        NoCache=0,

        /// <summary>
        /// NetFrameWork
        /// </summary>
        [Description("NetFrameWork")]
        NetFrameWork = 1,

        /// <summary>
        /// Memcached
        /// </summary>
        [Description("Memcached")]
        Memcached =2,

        /// <summary>
        /// Redis
        /// </summary>
        [Description("Redis")]
        Redis = 3,
    }
}
