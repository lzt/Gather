﻿using System;

namespace Operate.DataBase.NoSqlDataBase
{
    /// <summary>
    /// ICacheAssistant
    /// </summary>
    public interface ICacheAssistant
    {
        #region Properties

        /// <summary>
        /// CacheType
        /// </summary>
        CacheType CacheType { get; }

        #endregion

        #region Get

        /// <summary>
        /// 获取缓存的对象
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <returns></returns>
        T Get<T>(string key);

        #endregion

        #region Set

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        bool Set<T>(string key, T value);

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="expiresAt">过期</param>
        /// <returns></returns>
        bool Set<T>(string key, T value, DateTime expiresAt);

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="expiresIn">过期</param>
        /// <returns></returns>
        bool Set<T>(string key, T value, TimeSpan expiresIn);

        #endregion
    }
}
