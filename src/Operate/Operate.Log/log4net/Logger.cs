﻿using System;
using System.IO;
using log4net;
using log4net.Config;

namespace Operate.Log.log4net
{
    /// <summary>
    /// 在运行时记录日志信息
    /// 通过Logger.Current随时获取ILog对象
    /// ILog的使用方法请参考log4net的文档
    /// </summary>
    public class Logger: ILogger
    {
        #region 字段

        static ILog _current;

        #endregion

        #region 属性

        /// <summary>
        /// CurrentLog
        /// </summary>
        public static ILog Current
        {
            get
            {
                if (_current == null)
                {
                    XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "Config/Log4net.config")); 
                    _current = LogManager.GetLogger("ViolationReminder");
                }
                return _current;
            }
        }

        /// <summary>
        /// RecordException
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ex"></param>
        public void RecordException<T>(T ex) where T : Exception
        {
            Current.Error("异常：", ex);
        }

        #endregion
    }
}