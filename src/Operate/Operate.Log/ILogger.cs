﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.Log
{
    /// <summary>
    /// ILogger
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// RecordException
        /// </summary>
        /// <param name="ex"></param>
        /// <typeparam name="T"></typeparam>
        void RecordException<T>(T ex) where T : Exception;
    }
}
