﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using System.Linq;

#endregion

namespace Operate
{
    /// <summary>
    /// Holds tabular information
    /// </summary>
    public class Table
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="columnNames">Column names</param>
        public Table(params string[] columnNames)
        {
           ColumnNames = (string[])columnNames.Clone();
           Rows = new List<Row>();
           ColumnNameHash = new Hashtable();
            int x = 0;
            foreach (string columnName in columnNames)
            {
                if (!ColumnNameHash.ContainsKey(columnName))
                    ColumnNameHash.Add(columnName, x++);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="reader">Data reader to get the data from</param>
        public Table(IDataReader reader)
        {
            ColumnNames = new string[reader.FieldCount];
            for (int x = 0; x < reader.FieldCount; ++x)
            {
                ColumnNames[x] = reader.GetName(x);
            }
            ColumnNameHash = new Hashtable();
            int y = 0;
            foreach (string columnName in ColumnNames)
            {
                if (!ColumnNameHash.ContainsKey(columnName))
                    ColumnNameHash.Add(columnName, y++);
            }
            Rows = new List<Row>();
            while (reader.Read())
            {
                var values = new object[ColumnNames.Length];
                for (int x = 0; x < reader.FieldCount; ++x)
                {
                    values[x] = reader[x];
                }
                Rows.Add(new Row(ColumnNameHash, ColumnNames, values));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Column names for the table
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public string[] ColumnNames { get; protected set; }

        /// <summary>
        /// Column Name hash table
        /// </summary>
        public Hashtable ColumnNameHash { get; private set; }

        /// <summary>
        /// Rows within the table
        /// </summary>
        public ICollection<Row> Rows { get; private set; }

        /// <summary>
        /// Gets a specific row
        /// </summary>
        /// <param name="rowNumber">Row number</param>
        /// <returns>The row specified</returns>
        public Row this[int rowNumber]
        {
            get
            {
                return Rows.Count > rowNumber ? Rows.ElementAt(rowNumber) : null;
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Adds a row using the objects passed in
        /// </summary>
        /// <param name="objects">Objects to create the row from</param>
        /// <returns>This</returns>
        public virtual Table AddRow(params object[] objects)
        {
            Rows.Add(new Row(ColumnNameHash, ColumnNames, objects));
            return this;
        }

        #endregion
    }

    /// <summary>
    /// Holds an individual row
    /// </summary>
    public class Row
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="columnNames">Column names</param>
        /// <param name="columnValues">Column values</param>
        /// <param name="columnNameHash">Column name hash</param>
        public Row(Hashtable columnNameHash, string[] columnNames, params object[] columnValues)
        {
            ColumnNameHash = columnNameHash;
            ColumnNames = columnNames;
            ColumnValues = (object[])columnValues.Clone();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Column names
        /// </summary>
        public Hashtable ColumnNameHash { get; private set; }

        /// <summary>
        /// Column names
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public string[] ColumnNames { get; protected set; }

        /// <summary>
        /// Column values
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public object[] ColumnValues { get; protected set; }

        /// <summary>
        /// Returns a column based on the column name specified
        /// </summary>
        /// <param name="columnName">Column name to search for</param>
        /// <returns>The value specified</returns>
        public object this[string columnName]
        {
            get
            {
                var column = (int)ColumnNameHash[columnName];//.PositionOf(ColumnName);
                if (column == -1)
                    throw new ArgumentOutOfRangeException(columnName + " is not present in the row");
                return this[column];
            }
        }

        /// <summary>
        /// Returns a column based on the value specified
        /// </summary>
        /// <param name="column">Column number</param>
        /// <returns>The value specified</returns>
        public object this[int column]
        {
            get
            {
                if (column < 0) { throw new ArgumentOutOfRangeException("column"); }
                if (ColumnValues.Length <= column)
                    return null;
                return ColumnValues[column];
            }
        }

        #endregion
    }
}