﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Operate.ExtensionMethods;

#endregion

namespace Operate
{
    /// <summary>
    /// Dictionary that matches multiple keys to each value
    /// </summary>
    /// <typeparam name="TKey">Key type</typeparam>
    /// <typeparam name="TValue">Value type</typeparam>
    public class TagDictionary<TKey, TValue> : IDictionary<TKey, IEnumerable<TValue>>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public TagDictionary()
        {
            Items = new ConcurrentBag<TaggedItem<TKey, TValue>>();
            KeyList = new List<TKey>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Items in the dictionary
        /// </summary>
        private ConcurrentBag<TaggedItem<TKey, TValue>> Items { get; set; }

        /// <summary>
        /// Gets the values found in the dictionary
        /// </summary>
        public ICollection<IEnumerable<TValue>> Values
        {
            get { return new IEnumerable<TValue>[] { Items.ToArray(x => x.Value) }; }
        }

        /// <summary>
        /// Gets the keys found in the dictionary
        /// </summary>
        public ICollection<TKey> Keys
        {
            get { return KeyList; }
        }

        /// <summary>
        /// List of keys that have been entered
        /// </summary>
        private List<TKey> KeyList { get; set; }

        /// <summary>
        /// Gets the values based on a key
        /// </summary>
        /// <param name="key">Key to get the values of</param>
        /// <returns>The values associated with the key</returns>
        public IEnumerable<TValue> this[TKey key]
        {
            get
            {
                return Items.Where(x => x.Keys.Contains(key)).ToArray(x => x.Value);
            }
            set
            {
                Add(key, value);
            }
        }

        /// <summary>
        /// Number of items in the dictionary
        /// </summary>
        public int Count
        {
            get { return Items.Count(); }
        }

        /// <summary>
        /// Always false
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Adds a list of values to the key
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Values to add</param>
        public void Add(TKey key, IEnumerable<TValue> value)
        {
            value.ToArray(x => new TaggedItem<TKey, TValue>(key, x)).ForEach(x => Items.Add(x));
            KeyList.AddIfUnique(key);
        }

        /// <summary>
        /// Adds a value to the dicionary
        /// </summary>
        /// <param name="value">Value to add</param>
        /// <param name="keyCollection">KeyCollection to associate the value with</param>
        public void Add(TValue value, params TKey[] keyCollection)
        {
            Items.Add(new TaggedItem<TKey, TValue>(keyCollection, value));
            keyCollection.ForEach(x => KeyList.AddIfUnique(x));
        }

        /// <summary>
        /// Determines if a key is in the dictionary
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>True if it exists, false otherwise</returns>
        public bool ContainsKey(TKey key)
        {
            return KeyList.Contains(key);
        }

        /// <summary>
        /// Removes all items that are associated with a key
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Returns true if the key was found, false otherwise</returns>
        public bool Remove(TKey key)
        {
            bool returnValue = ContainsKey(key);
            Items = new ConcurrentBag<TaggedItem<TKey, TValue>>(Items.ToArray(x => x).Where(x => !x.Keys.Contains(key)));
            KeyList.Remove(key);
            return returnValue;
        }

        /// <summary>
        /// Attempts to get the values associated with a key
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Values associated with a key</param>
        /// <returns>True if something is returned, false otherwise</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public bool TryGetValue(TKey key, out IEnumerable<TValue> value)
        {
            value = new List<TValue>();
            try
            {
                value = this[key];
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
            return value.Any();
        }

        /// <summary>
        /// Adds an item to the dictionary
        /// </summary>
        /// <param name="item">item to add</param>
        public void Add(KeyValuePair<TKey, IEnumerable<TValue>> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Clears the dictionary
        /// </summary>
        public void Clear()
        {
            Items = new ConcurrentBag<TaggedItem<TKey, TValue>>();
        }

        /// <summary>
        /// Determines if the dictionary contains the key/value pair
        /// </summary>
        /// <param name="item">item to check</param>
        /// <returns>True if it is, false otherwise</returns>
        public bool Contains(KeyValuePair<TKey, IEnumerable<TValue>> item)
        {
            return ContainsKey(item.Key);
        }

        /// <summary>
        /// Copies itself to an array
        /// </summary>
        /// <param name="array">Array</param>
        /// <param name="arrayIndex">Array index</param>
        public void CopyTo(KeyValuePair<TKey, IEnumerable<TValue>>[] array, int arrayIndex)
        {
            for (int x = 0; x < Keys.Count; ++x)
            {
                array[arrayIndex + x] = new KeyValuePair<TKey, IEnumerable<TValue>>(Keys.ElementAt(x), this[Keys.ElementAt(x)]);
            }
        }

        /// <summary>
        /// Removes a specific key/value pair
        /// </summary>
        /// <param name="item">item to remove</param>
        /// <returns>True if it is removed, false otherwise</returns>
        public bool Remove(KeyValuePair<TKey, IEnumerable<TValue>> item)
        {
            return Remove(item.Key);
        }

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<KeyValuePair<TKey, IEnumerable<TValue>>> GetEnumerator()
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (TKey key in Keys)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                yield return new KeyValuePair<TKey, IEnumerable<TValue>>(key, this[key]);
            }
        }

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (TKey key in Keys)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                yield return this[key];
            }
        }

        #endregion

        #region Internal Classes

        /// <summary>
        /// Holds information about each value
        /// </summary>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
#pragma warning disable 693
        private class TaggedItem<TKey, TValue>
#pragma warning restore 693
        {
            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="keys">KeyCollection</param>
            /// <param name="value">Value</param>
            public TaggedItem(IEnumerable<TKey> keys, TValue value)
            {
                Keys = new ConcurrentBag<TKey>(keys);
                Value = value;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="key">Key</param>
            /// <param name="value">Value</param>
            public TaggedItem(TKey key, TValue value)
            {
                Keys = new ConcurrentBag<TKey>(new[] { key });
                Value = value;
            }

            #endregion

            #region Properties

            /// <summary>
            /// The list of keys associated with the value
            /// </summary>
            public ConcurrentBag<TKey> Keys { get; private set; }

            /// <summary>
            /// Value
            /// </summary>
            public TValue Value { get; private set; }

            #endregion
        }

        #endregion
    }
}