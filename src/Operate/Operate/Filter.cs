﻿using System.Text.RegularExpressions;

namespace Operate
{
    /// <summary>
    /// 过滤工具
    /// </summary>
    class Filter
    {
        /// <summary>
        /// 过滤Html标记，获取文本
        /// </summary>
        /// <param name="html">html内容</param>
        /// <returns></returns>
        public static string FilterHtmlTag(string html)
        {
            var source = html;
            //删除脚本   
            source = Regex.Replace(source, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);

            //删除HTML   
            var regex = new Regex("<.+?>", RegexOptions.IgnoreCase);
            source = regex.Replace(source, "");
            source = Regex.Replace(source, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"-->", "", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"<!--.*", "", RegexOptions.IgnoreCase);

            source = Regex.Replace(source, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(nbsp|#160);", "   ", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            source = Regex.Replace(source, @"&#(\d+);", "", RegexOptions.IgnoreCase);

            //source.Replace("<", "");
            //source.Replace(">", "");
            //source.Replace("\r\n", "");

            return source;
        }
    }
}
