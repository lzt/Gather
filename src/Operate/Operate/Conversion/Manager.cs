﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.IO;
using Operate.Conversion.Interfaces;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Conversion
{
    /// <summary>
    /// Conversion manager
    /// </summary>
    public class Manager
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Manager()
        {
            Converters = new Dictionary<Type, IObjectConverter>();
            new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).LoadAssemblies().ForEach(x => x);
            foreach (Type objectConverter in AppDomain.CurrentDomain.GetAssemblies().Types<IObjectConverter>())
            {
                foreach (Type converter in AppDomain.CurrentDomain.GetAssemblies().Types<IConverter>())
                {
                    Type key = converter;
                    while (key != null)
                    {
                        foreach (Type Interface in converter.GetInterfaces())
                        {
                            if (Interface.GetGenericArguments().Length > 0)
                                AddConverter(Interface.GetGenericArguments()[0], objectConverter);
                        }
                        key = key.BaseType;
                    }
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Converters
        /// </summary>
        protected Dictionary<Type, IObjectConverter> Converters { get; private set; }

        #endregion

        #region Functions

        /// <summary>
        /// Adds a converter to the system
        /// </summary>
        /// <param name="objectConverter">Object converter</param>
        /// <param name="objectType">Object type</param>
        /// <returns>This</returns>
        protected Manager AddConverter(Type objectType, Type objectConverter)
        {
            if (!Converters.ContainsKey(objectType))
            {
                Type finalType = objectConverter.MakeGenericType(objectType);
                Converters.Add(objectType, (IObjectConverter)Activator.CreateInstance(finalType, this));
            }
            return this;
        }

        /// <summary>
        /// Converts item from type T to R
        /// </summary>
        /// <typeparam name="T">Incoming type</typeparam>
        /// <typeparam name="TR">Resulting type</typeparam>
        /// <param name="item">Incoming object</param>
        /// <param name="defaultValue">Default return value if the item is null or can not be converted</param>
        /// <returns>The value converted to the specified type</returns>
        public TR To<T, TR>(T item, TR defaultValue = default(TR))
        {
            return (TR)To(item, typeof(TR), defaultValue);
        }


        /// <summary>
        /// Converts item from type T to R
        /// </summary>
        /// <typeparam name="T">Incoming type</typeparam>
        /// <param name="item">Incoming object</param>
        /// <param name="resultType">Result type</param>
        /// <param name="defaultValue">Default return value if the item is null or can not be converted</param>
        /// <returns>The value converted to the specified type</returns>
        public object To<T>(T item, Type resultType, object defaultValue = null)
        {
            try
            {
                // ReSharper disable CompareNonConstrainedGenericWithNull
                if (item == null || Convert.IsDBNull(item))
                    // ReSharper restore CompareNonConstrainedGenericWithNull
                    return defaultValue;
                Type key = typeof(T);
                while (key != null)
                {
                    if (Converters.ContainsKey(key))
                        return Converters[key].To(item, resultType, defaultValue);
                    foreach (Type Interface in key.GetInterfaces())
                    {
                        if (Converters.ContainsKey(Interface))
                            return Converters[Interface].To(item, resultType, defaultValue);
                    }
                    key = key.BaseType;
                }
                return defaultValue;
            }
            catch { return defaultValue; }
        }

        #endregion
    }
}