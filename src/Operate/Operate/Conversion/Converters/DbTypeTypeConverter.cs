﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Data;

#endregion

namespace Operate.Conversion.Converters
{
    /// <summary>
    /// DbType to Type converter
    /// </summary>
    public class DbTypeTypeConverter : ConverterBase<Type, DbType>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="manager">Manager</param>
        public DbTypeTypeConverter(Manager manager) : base(manager) { }

        /// <summary>
        /// Converts the object to the specified type
        /// </summary>
        /// <param name="item">Object to convert</param>
        /// <param name="returnType">Return type</param>
        /// <returns>The object as the type specified</returns>
        public override object To(Type item, Type returnType)
        {
            if (item.IsEnum) return To(Enum.GetUnderlyingType(item), returnType);
            if (item == typeof(byte)) return DbType.Byte;
            if (item == typeof(sbyte)) return DbType.SByte;
            if (item == typeof(short)) return DbType.Int16;
            if (item == typeof(ushort)) return DbType.UInt16;
            if (item == typeof(int)) return DbType.Int32;
            if (item == typeof(uint)) return DbType.UInt32;
            if (item == typeof(long)) return DbType.Int64;
            if (item == typeof(ulong)) return DbType.UInt64;
            if (item == typeof(float)) return DbType.Single;
            if (item == typeof(double)) return DbType.Double;
            if (item == typeof(decimal)) return DbType.Decimal;
            if (item == typeof(bool)) return DbType.Boolean;
            if (item == typeof(string)) return DbType.String;
            if (item == typeof(char)) return DbType.StringFixedLength;
            if (item == typeof(Guid)) return DbType.Guid;
            if (item == typeof(DateTime)) return DbType.DateTime2;
            if (item == typeof(DateTimeOffset)) return DbType.DateTimeOffset;
            if (item == typeof(byte[])) return DbType.Binary;
            if (item == typeof(byte?)) return DbType.Byte;
            if (item == typeof(sbyte?)) return DbType.SByte;
            if (item == typeof(short?)) return DbType.Int16;
            if (item == typeof(ushort?)) return DbType.UInt16;
            if (item == typeof(int?)) return DbType.Int32;
            if (item == typeof(uint?)) return DbType.UInt32;
            if (item == typeof(long?)) return DbType.Int64;
            if (item == typeof(ulong?)) return DbType.UInt64;
            if (item == typeof(float?)) return DbType.Single;
            if (item == typeof(double?)) return DbType.Double;
            if (item == typeof(decimal?)) return DbType.Decimal;
            if (item == typeof(bool?)) return DbType.Boolean;
            if (item == typeof(char?)) return DbType.StringFixedLength;
            if (item == typeof(Guid?)) return DbType.Guid;
            if (item == typeof(DateTime?)) return DbType.DateTime2;
            if (item == typeof(DateTimeOffset?)) return DbType.DateTimeOffset;
            return DbType.Int32;
        }

        /// <summary>
        /// Converts the object to the specified type
        /// </summary>
        /// <param name="item">Object to convert</param>
        /// <param name="returnType">Return type</param>
        /// <returns>The object as the type specified</returns>
        public override object To(DbType item, Type returnType)
        {
            if (item == DbType.Byte) return typeof(byte);
            if (item == DbType.SByte) return typeof(sbyte);
            if (item == DbType.Int16) return typeof(short);
            if (item == DbType.UInt16) return typeof(ushort);
            if (item == DbType.Int32) return typeof(int);
            if (item == DbType.UInt32) return typeof(uint);
            if (item == DbType.Int64) return typeof(long);
            if (item == DbType.UInt64) return typeof(ulong);
            if (item == DbType.Single) return typeof(float);
            if (item == DbType.Double) return typeof(double);
            if (item == DbType.Decimal) return typeof(decimal);
            if (item == DbType.Boolean) return typeof(bool);
            if (item == DbType.String) return typeof(string);
            if (item == DbType.StringFixedLength) return typeof(char);
            if (item == DbType.Guid) return typeof(Guid);
            if (item == DbType.DateTime2) return typeof(DateTime);
            if (item == DbType.DateTime) return typeof(DateTime);
            if (item == DbType.DateTimeOffset) return typeof(DateTimeOffset);
            if (item == DbType.Binary) return typeof(byte[]);
            return typeof(int);
        }
    }
}