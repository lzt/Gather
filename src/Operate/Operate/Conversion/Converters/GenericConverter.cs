﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Globalization;
using System.Reflection;
using Operate.Conversion.Interfaces;

#endregion

namespace Operate.Conversion.Converters
{
    /// <summary>
    /// Generic converter (last ditch effort class)
    /// </summary>
    public class GenericConverter : IConverter<object>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="manager">Manager object</param>
        public GenericConverter(Manager manager) { Manager = manager; }

        /// <summary>
        /// Manager object
        /// </summary>
        protected Manager Manager { get; private set; }

        /// <summary>
        /// Always returns true as it will attempt any return type
        /// </summary>
        /// <param name="type">Type asking about</param>
        /// <returns>True</returns>
        public bool CanConvert(Type type)
        {
            return true;
        }

        /// <summary>
        /// Converts the object to the specified type
        /// </summary>
        /// <param name="item">Object to convert</param>
        /// <param name="returnType">Return type</param>
        /// <returns>The object as the type specified</returns>
        public object To(object item, Type returnType)
        {
            var objectValue = item as string;
            if (objectValue != null && objectValue.Length == 0)
                return null;
            var objectType = item.GetType();
            if (returnType.IsAssignableFrom(objectType))
                return item;
            if (returnType.IsEnum)
                if (objectValue != null) return Enum.Parse(returnType, objectValue, true);
            if ((item as IConvertible) != null)
                return Convert.ChangeType(item, returnType, CultureInfo.InvariantCulture);
            var converter = TypeDescriptor.GetConverter(objectType);
            if (converter.CanConvertTo(returnType))
                return converter.ConvertTo(item, returnType);
            if (returnType == typeof(ExpandoObject))
            {
                var returnValue = new ExpandoObject();
                var tempType = objectType;
                foreach (PropertyInfo property in tempType.GetProperties())
                {
                    ((ICollection<KeyValuePair<String, Object>>)returnValue).Add(new KeyValuePair<string, object>(property.Name, property.GetValue(item, null)));
                }
                return returnValue;
            }
            return null;
        }
    }
}