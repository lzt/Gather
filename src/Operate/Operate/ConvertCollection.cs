﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Operate.ExtensionMethods;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace Operate
{
    /// <summary>
    /// 转换方法集合
    /// </summary>
    public static class ConvertCollection
    {
        public static T ObjectTo<T>(object value, object defaultValue = null)
        {
            T result;

            if (defaultValue == null)
            {
                defaultValue = default(T);
            }

            result = (T)defaultValue;

            //判断convertsionType类型是否为泛型，因为nullable是泛型类,
            var convertsionType = typeof(T);

            if (convertsionType.IsGenericType &&

                //判断convertsionType是否为nullable泛型类
                convertsionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null || value.ToString().Trim().Length == 0)
                {
                    return result;
                }

                //如果convertsionType为nullable类，声明一个NullableConverter类，该类提供从Nullable类到基础基元类型的转换
                NullableConverter nullableConverter = new NullableConverter(convertsionType);

                //将convertsionType转换为nullable对的基础基元类型
                convertsionType = nullableConverter.UnderlyingType;

                try
                {
                    result = (T)(Convert.ChangeType(value, convertsionType));
                    return result;
                }
                catch (Exception)
                {
                    return result;
                }
            }

            if (value != null)
            {
                result = (T)(Convert.ChangeType(value, typeof(T)));
            }

            return result;
        }

        #region 时间转与UNIX时间戳转换

        #region 将时间转换成UNIX时间戳

        /// <summary>
        /// 获取时间戳（13位 毫秒）
        /// </summary>
        /// <returns></returns>
        public static long GetTimeStamp(DateTime dt)
        {
            TimeSpan ts = dt.ToUniversalTime() - new DateTime(
                1970,
                1,
                1,
                0,
                0,
                0,
                DateTimeKind.Utc
            );
            return Convert.ToInt64(ts.TotalSeconds * 1000);
        }

        /// <summary>
        /// 将时间转换成UNIX时间戳
        /// </summary>
        /// <param name="dt">时间</param>
        /// <returns>UNIX时间戳</returns>
        public static long ConvertToUnixTime(DateTime dt)
        {
            //var starttime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            //var ts = dt.Subtract(starttime);
            //var uiStamp = Convert.ToInt64(ts.TotalSeconds);
            //return uiStamp;

            return (dt.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
        }

        #endregion 将时间转换成UNIX时间戳

        #region 将UNIX时间戳转换成时间

        /// <summary>
        /// 将UNIX时间戳转换成协调世界时（UTC）
        /// </summary>
        /// <param name="unixTime">UNIX时间戳</param>
        /// <returns>时间</returns>
        public static DateTime ConvertToUniversalTime(long unixTime)
        {
            var start = new DateTime(
                1970,
                1,
                1,
                0,
                0,
                0,
                DateTimeKind.Utc
            );
            var date = start.AddSeconds(unixTime).ToUniversalTime();
            return date;
        }

        /// <summary>
        /// 将UNIX时间戳转换成本地时间
        /// </summary>
        /// <param name="unixTime">UNIX时间戳</param>
        /// <returns>时间</returns>
        public static DateTime ConvertToLocalTime(long unixTime)
        {
            var t = ConvertToUniversalTime(unixTime);
            return t.ToLocalTime();
        }

        /// <summary>
        /// 将UNIX时间戳转换成协调世界时（UTC）
        /// </summary>
        /// <param name="unixTimeString">UNIX时间戳</param>
        /// <returns>时间</returns>
        public static DateTime ConvertToUniversalTime(string unixTimeString)
        {
            var unixTime = Convert.ToInt64(unixTimeString);
            return ConvertToUniversalTime(unixTime);
        }

        /// <summary>
        /// 将UNIX时间戳转换成本地时间
        /// </summary>
        /// <param name="unixTimeString">UNIX时间戳</param>
        /// <returns>时间</returns>
        public static DateTime ConvertToLocalTime(string unixTimeString)
        {
            var unixTime = Convert.ToInt64(unixTimeString);
            return ConvertToLocalTime(unixTime);
        }

        #endregion 将UNIX时间戳转换成时间

        #endregion 时间转与UNIX时间戳转换

        /// <summary>
        /// 转换类型名为有好名称
        /// </summary>
        /// <param name="typeCode">       </param>
        /// <param name="changeLongToInt"></param>
        /// <returns></returns>
        public static string ToFriendlyTypeName(TypeCode typeCode, bool changeLongToInt = false)
        {
            string result;
            switch (typeCode)
            {
                case TypeCode.String:
                    result = "string";
                    break;

                case TypeCode.Int16:
                    result = "short";
                    break;

                case TypeCode.Int32:
                    result = "int";
                    break;

                case TypeCode.Int64:
                    result = changeLongToInt ? "int" : "long";
                    break;

                case TypeCode.Boolean:
                    result = "bool";
                    break;

                case TypeCode.Char:
                    result = "char";
                    break;

                case TypeCode.Decimal:
                    result = "decimal";
                    break;

                case TypeCode.Double:
                    result = "double";
                    break;

                case TypeCode.Object:
                    result = "object";
                    break;

                case TypeCode.Single:
                    result = "float";
                    break;

                case TypeCode.UInt16:
                    result = "ushort";
                    break;

                case TypeCode.UInt32:
                    result = "uint";
                    break;

                case TypeCode.UInt64:
                    result = "ulong";
                    break;

                default:
                    result = typeCode.ToString();
                    break;
            }

            return result;
        }

        /// <summary>
        /// JObjectToNameValueCollection
        /// </summary>
        /// <returns></returns>
        public static IDictionary<string, object> DictionaryToExpandoObject(IDictionary<string, object> dic)
        {
            var eo = new ExpandoObject();
            if (dic == null || dic.Count == 0)
            {
                return eo;
            }

            foreach (var d in dic)
            {
                eo.Add(new KeyValuePair<string, object>(d.Key, d.Value));
            }

            return eo;
        }

        /// <summary>
        /// JObjectToNameValueCollection
        /// </summary>
        /// <param name="jo">JObject</param>
        /// <returns></returns>
        public static IDictionary<string, object> JObjectToDictionary(JObject jo)
        {
            var dictionary = new Dictionary<string, object>();
            if (jo.Count == 0)
            {
                return dictionary;
            }

            JToken stopName = jo.First;
            JProperty jp;
            JValue jv;

            if (stopName != null)
            {
                do
                {
                    jp = stopName.Value<JProperty>();
                    jv = jp.Value as JValue;

                    if (jv != null)
                    {
                        dictionary[jp.Name] = jv.Value ?? "";
                    }
                    else
                    {
                        if (jp.Value is JArray jArray)
                        {
                            var list = new List<object>();

                            foreach (var item in jArray)
                            {
                                var v = item as JObject;

                                if (v == null)
                                {
                                    list.Add(item);
                                }
                                else
                                {
                                    list.Add(JObjectToDictionary(item as JObject));
                                }
                            }

                            dictionary[jp.Name] = list;
                        }
                        else
                        {
                            if (jp.Value is JObject j)
                            {
                                dictionary[jp.Name] = JObjectToDictionary(j);
                            }
                        }
                    }

                    stopName = stopName.Next;
                }
                while (stopName != null);
            }

            ////最后一个
            //stopName = jo.Last;

            //jp = stopName.Value<JProperty>();
            //jv = jp.Value as JValue;
            //if (jv != null)
            //{
            //    dictionary[jp.Name] = jv.Value ?? "";
            //}

            return dictionary;
        }

        /// <summary>
        /// JObjectToNameValueCollection
        /// </summary>
        /// <param name="jo">JObject</param>
        /// <returns></returns>
        public static IDictionary<string, object> JObjectToExpandoObject(JObject jo)
        {
            var dic = JObjectToDictionary(jo);
            return DictionaryToExpandoObject(dic);
        }

        /// <summary>
        /// JObjectToNameValueCollection
        /// </summary>
        /// <param name="jo">JObject</param>
        /// <returns></returns>
        public static NameValueCollection JObjectToNameValueCollection(JObject jo)
        {
            var nv = new NameValueCollection();

            var dic = JObjectToDictionary(jo);

            foreach (var key in dic.Keys)
            {
                var o = dic[key];

                var t = o.GetType().GetInterface(typeof(ICollection).Name);

                if (t == null)
                {
                    nv[key] = o.ToString();
                }
                else
                {
                    nv[key] = JsonConvert.SerializeObject(o);
                }
            }

            return nv;
        }

        /// <summary>
        /// NameValueCollectionToJson 如果含有Html，则html格式代码将被清除
        /// </summary>
        /// <param name="nv">NameValueCollection</param>
        /// <returns></returns>
        public static string NameValueCollectionToJson(NameValueCollection nv)
        {
            if (nv != null)
            {
                if (nv.Count > 0)
                {
                    var result = new Hashtable();
                    foreach (var key in nv.AllKeys)
                    {
                        if (!string.IsNullOrWhiteSpace(key))
                        {
                            result[key] = nv[key];
                        }
                    }

                    return JsonConvert.SerializeObject(result);

                    //var list = (from key in nv.AllKeys let value = nv[key] select "\"{0}\":\"{1}\"".FormatValue(key, value.ContainsHtml() ? value.FilterHtmlTag() : value)).ToList();
                    //return "{" + list.Join(",") + "}";
                }
            }

            return "{}";
        }

        /// <summary>
        /// Json字符串转化为NameValueCollection
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static NameValueCollection JsonToNameValueCollection(string json)
        {
            var o = JsonConvert.DeserializeObject<JObject>(json);
            return JObjectToNameValueCollection(o);
        }

        /// <summary>
        /// NameValueCollection转化为Dictionary
        /// </summary>
        /// <param name="nv"></param>
        /// <returns></returns>
        public static IDictionary<string, T> NameValueCollectionToDictionary<T>(NameValueCollection nv) where T : class
        {
            IDictionary<string, T> r = new Dictionary<string, T>();
            if (nv.Count > 0)
            {
                foreach (var key in nv.AllKeys)
                {
                    var v = nv[key];
                    r.Add(key, v as T);
                }
            }

            return r;
        }

        /// <summary>
        /// NameValueCollection转化为Dictionary
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static NameValueCollection DictionaryToNameValueCollection<T>(IDictionary<string, T> dic)
        {
            var r = new NameValueCollection();
            if (dic.Count > 0)
            {
                foreach (var key in dic.Keys)
                {
                    r.Add(key, dic[key].ToString());
                }
            }

            return r;
        }

        /// <summary>
        /// Json字符串转化为Dictionary
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static IDictionary<string, object> JsonToDictionary(string json)
        {
            var o = JsonConvert.DeserializeObject<JObject>(json);
            return JObjectToDictionary(o);
        }

        /// <summary>
        /// Json字符串转化为Dictionary
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static SortedDictionary<string, T> DictionaryToSortedDictionary<T>(IDictionary<string, T> dic)
        {
            var r = new SortedDictionary<string, T>();
            foreach (var item in dic)
            {
                r.Add(item.Key, item.Value);
            }

            return r;
        }

        /// <summary>
        /// 把数组所有元素，按照“参数=参数值”的模式拼接成字符串
        /// </summary>
        /// <param name="dic">需要拼接的数组</param>
        /// <returns>拼接完成以后的字符串</returns>
        public static string DictionaryToLinkString<T>(Dictionary<string, T> dic)
        {
            var prestr = new StringBuilder();
            foreach (KeyValuePair<string, T> temp in dic)
            {
                prestr.Append(temp.Key + "=" + temp.Value + "&");
            }

            //去掉最後一個&字符
            int nLen = prestr.Length;
            prestr.Remove(nLen - 1, 1);

            return prestr.ToString();
        }

        /// <summary>
        /// Convert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="actionT"></param>
        /// <returns></returns>
        public static Action<object> ConvertAction<T>(Action<T> actionT)
        {
            if (actionT == null)
            {
                return null;
            }

            return o => actionT((T)o);
        }

        #region DataBase

        /// <summary>
        /// CreateDataTableFromDataReader
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static DataTable CreateDataTableFromDataReader(IDataReader reader)
        {
            var table = new DataTable();
            int fieldCount = reader.FieldCount;
            for (int i = 0; i < fieldCount; i++)
            {
                table.Columns.Add(reader.GetName(i), reader.GetFieldType(i));
            }

            table.BeginLoadData();
            var values = new object[fieldCount];
            while (reader.Read())
            {
                reader.GetValues(values);
                table.LoadDataRow(values, true);
            }

            table.EndLoadData();

            return table;
        }

        /// <summary>
        /// DataReader转换为dataset
        /// </summary>
        /// <param name="reader">DataReader</param>
        /// <returns></returns>
        public static DataSet DataReaderToDataSet(IDataReader reader)
        {
            var ds = new DataSet();

            ds.Tables.Add(CreateDataTableFromDataReader(reader));

            while (reader.NextResult())
            {
                ds.Tables.Add(CreateDataTableFromDataReader(reader));
            }

            return ds;
        }

        /// <summary>
        /// DataReaderToDataTable
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static DataTable DataReaderToDataTable(IDataReader reader)
        {
            var ds = DataReaderToDataSet(reader);

            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }

            return new DataTable();
        }

        /// <summary>
        /// DataReaderToJson
        /// </summary>
        /// <param name="reader">    </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static string DataReaderToJson(IDataReader reader, bool keyToLower = true)
        {
            var d = DataReaderToDataTable(reader);

            string json;

            if (keyToLower)
            {
                json = JsonConvertAssist.SerializeAndKeyToLower(d);
            }
            else
            {
                json = JsonConvert.SerializeObject(d);
            }

            return json;
        }

        /// <summary>
        /// DataTableToJson
        /// </summary>
        /// <param name="dataTable"> </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static string DataTableToJson(DataTable dataTable, bool keyToLower = true)
        {
            var dr = dataTable.CreateDataReader();

            return DataReaderToJson(dr, keyToLower);
        }

        /// <summary>
        /// DataReaderToDictionaryList
        /// </summary>
        /// <param name="reader">    </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static IList<IDictionary<string, object>> DataReaderToDictionaryList(
            IDataReader reader,
            bool keyToLower = true
        )
        {
            var result = new List<IDictionary<string, object>>();

            string json = DataReaderToJson(reader, keyToLower);
            var list = JsonConvert.DeserializeObject<List<object>>(json);

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var o in list)
            {
                var oJson = JsonConvert.SerializeObject(o);
                var dic = ConvertCollection.JsonToDictionary(oJson);
                result.Add(dic);
            }

            return result;
        }

        /// <summary>
        /// DataTableToDictionaryList
        /// </summary>
        /// <param name="dataTable"> </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static IList<IDictionary<string, object>> DataTableToDictionaryList(
            DataTable dataTable,
            bool keyToLower = true
        )
        {
            var dr = dataTable.CreateDataReader();

            return DataReaderToDictionaryList(dr, keyToLower);
        }

        /// <summary>
        /// DataReaderToExpandoObjectList
        /// </summary>
        /// <param name="reader">    </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static List<ExpandoObject> DataReaderToExpandoObjectList(IDataReader reader, bool keyToLower = true)
        {
            var result = new List<ExpandoObject>();

            var list = DataReaderToDictionaryList(reader);

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var dic in list)
            {
                var eo = new ExpandoObject { dic };
                result.Add(eo);
            }

            return result;
        }

        /// <summary>
        /// DataTableToExpandoObjectList
        /// </summary>
        /// <param name="dataTable"> </param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static List<ExpandoObject> DataTableToExpandoObjectList(DataTable dataTable, bool keyToLower = true)
        {
            if (dataTable.Rows.Count > 0)
            {
                var dr = dataTable.CreateDataReader();

                return DataReaderToExpandoObjectList(dr, keyToLower);
            }

            return new List<ExpandoObject>();
        }

        /// <summary>
        /// 检测值类型
        /// </summary>
        /// <returns></returns>
        private static object CheckValue(TypeCode typeCode, object value, bool longToint = true)
        {
            object result = null;
            switch (typeCode)
            {
                case TypeCode.String:
                    result = value is DBNull ? "" : value;
                    break;

                case TypeCode.Int16:
                    result = value is DBNull ? 0 : value;
                    break;

                case TypeCode.Int32:
                    result = value is DBNull
                        ? 0
                        : (longToint ? (value.IsInt64() ? Convert.ToInt32(value) : value) : value);
                    break;

                case TypeCode.Int64:
                    result = value is DBNull ? 0 : value;
                    break;

                case TypeCode.DateTime:
                    result = value is DBNull ? DateTime.MinValue : value;
                    break;

                case TypeCode.Boolean:
                    result = value is DBNull ? false : value;
                    break;

                case TypeCode.Char:
                    result = value is DBNull ? "" : value;
                    break;

                case TypeCode.Decimal:
                    result = value is DBNull ? 0 : value;
                    break;

                case TypeCode.Double:
                    result = value is DBNull ? 0 : value;
                    break;

                case TypeCode.Object:
                    result = value is DBNull ? null : value;
                    break;

                case TypeCode.Single:
                    result = value is DBNull ? 0 : value;
                    break;

                case TypeCode.UInt16:
                    result = value is DBNull ? 0 : value;
                    break;

                case TypeCode.UInt32:
                    result = value is DBNull
                        ? 0
                        : (longToint ? (value.IsInt64() ? Convert.ToUInt32(value) : value) : value);
                    break;

                case TypeCode.UInt64:
                    result = value is DBNull ? 0 : value;
                    break;
            }

            return result;
        }

        /// <summary>
        /// DataTable转换为Model(忽略不匹配项)
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="table">     要转换的数据表</param>
        /// <param name="longToint"> 遇到long类型自动转换为int</param>
        /// <param name="ignoreCase">忽略大小写(默认忽略大小写)</param>
        /// <returns></returns>
        public static IList<TModel> DataTableToModel<TModel>(
            DataTable table,
            bool longToint = true,
            bool ignoreCase = true
        )
        {
            if (table == null)
            {
                return new List<TModel>();
            }

            var list = new List<TModel>(); //里氏替换原则
            TModel t;
            PropertyInfo[] propertypes;
            string tempName;
            if (!ignoreCase)
            {
                foreach (DataRow row in table.Rows)
                {
                    t = Activator.CreateInstance<TModel>(); ////创建指定类型的实例

                    propertypes = t.GetType().GetProperties(); //得到类的属性
                    foreach (PropertyInfo pro in propertypes)
                    {
                        try
                        {
                            tempName = pro.Name;
                            if (table.Columns.Contains(tempName))
                            {
                                var typecode = Type.GetTypeCode(pro.PropertyType);
                                var value = row[tempName];
                                var realValue = CheckValue(typecode, value);
                                pro.SetValue(
                                    t,
                                    realValue,
                                    null
                                );
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new System.Exception("property {0} has error:{1}".FormatValue(pro.Name, ex.Message));
                        }
                    }

                    list.Add(t);
                }
            }
            else
            {
                var colList = new List<string>();

                foreach (DataColumn dc in table.Columns)
                {
                    colList.Add(dc.ColumnName.ToLower());
                }

                //List<string> colList = (from DataColumn col in table.Columns select col.ColumnName.ToLower()).ToList();

                foreach (DataRow row in table.Rows)
                {
                    t = Activator.CreateInstance<TModel>(); ////创建指定类型的实例

                    propertypes = t.GetType().GetProperties(); //得到类的属性
                    foreach (PropertyInfo pro in propertypes)
                    {
                        try
                        {
                            tempName = pro.Name.ToLower();
                            if (colList.Contains(tempName))
                            {
                                var typecode = Type.GetTypeCode(pro.PropertyType);
                                var value = row[tempName];
                                var realValue = CheckValue(typecode, value);
                                pro.SetValue(
                                    t,
                                    realValue,
                                    null
                                );
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new System.Exception("property {0} has error:{1}".FormatValue(pro.Name, ex.Message));
                        }
                    }

                    list.Add(t);
                }
            }

            return list;
        }

        #endregion DataBase
    }
}