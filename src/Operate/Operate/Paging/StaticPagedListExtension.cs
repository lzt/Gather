﻿using System.Collections.Generic;
using PagedList;

namespace Operate.Paging
{
    /// <summary>
    /// StaticPagedListExtension
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StaticPagedListExtension<T> : StaticPagedList<T>
    {
        private const int DefaultDisplayPageNumber = 5;
        private int _displayPageNumber;

        #region Properties

        /// <summary>
        /// 改制该值为奇数，偶数将自动修正为奇数（即+1）
        /// </summary>
        public int DisplayPageNumber
        {
            get { return _displayPageNumber; }
            set
            {
                //修正值
                var v = value;
                if (v > 1)
                {
                    if (v % 2 == 0)
                    {
                        v = v + 1;
                    }
                }
                else if (v <= 1)
                {
                    v = 1;
                }
                _displayPageNumber = v;
            }
        }

        /// <summary>
        /// DisplayPages
        /// </summary>
        public List<int> DisplayPages
        {
            get
            {
                var list = new List<int>();
                if (DisplayPageNumber == 1)
                {
                    list = new List<int> { PageNumber };
                    return list;
                }

                //总页数小于设置的xian'shi'shu
                if (PageCount < DisplayPageNumber)
                {
                    for (int i = 1; i <= PageCount; i++)
                    {
                        list.Add(i);
                    }
                }
                else
                {
                    int harfDisplayPageNumber = DisplayPageNumber / 2;
                    if (PageNumber < DisplayPageNumber)
                    {
                        for (int i = 1; i <= DisplayPageNumber; i++)
                        {
                            list.Add(i);
                        }
                    }
                    else if (PageNumber > PageCount - DisplayPageNumber + 1)
                    {
                        for (int i = PageCount - DisplayPageNumber + 1; i <= PageCount; i++)
                        {
                            list.Add(i);
                        }

                    }
                    else
                    {
                        for (int i = PageNumber - harfDisplayPageNumber; i <= PageNumber + harfDisplayPageNumber; i++)
                        {
                            list.Add(i);
                        }

                        //for (int i = PageCount - DisplayPageNumber + 1; i <= PageCount; i++)
                        //{
                        //    list.Add(i);
                        //}
                    }
                }
                return list;
            }
        }

        #endregion

        /// <summary>
        /// StaticPagedListExtension
        /// </summary>
        /// <param name="subset"></param>
        /// <param name="metaData"></param>
        public StaticPagedListExtension(IEnumerable<T> subset, IPagedList metaData)
            : base(subset, metaData)
        {
            DisplayPageNumber = DefaultDisplayPageNumber;
        }

        /// <summary>
        /// StaticPagedListExtension
        /// </summary>
        /// <param name="subset"></param>
        /// <param name="metaData"></param>
        public StaticPagedListExtension(IList<T> subset, IPagedList metaData)
            : base(subset, metaData)
        {
            DisplayPageNumber = DefaultDisplayPageNumber;
        }

        /// <summary>
        /// StaticPagedListExtension
        /// </summary>
        /// <param name="subset"></param>
        /// <param name="metaData"></param>
        public StaticPagedListExtension(ICollection<T> subset, IPagedList metaData)
            : base(subset, metaData)
        {
            DisplayPageNumber = DefaultDisplayPageNumber;
        }

        /// <summary>
        /// StaticPagedListExtension
        /// </summary>
        /// <param name="subset"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalItemCount"></param>
        public StaticPagedListExtension(IEnumerable<T> subset, int pageNumber, int pageSize, int totalItemCount)
            : base(subset, pageNumber, pageSize, totalItemCount)
        {
            DisplayPageNumber = DefaultDisplayPageNumber;
        }

        /// <summary>
        /// StaticPagedListExtension
        /// </summary>
        /// <param name="subset"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalItemCount"></param>
        public StaticPagedListExtension(IList<T> subset, int pageNumber, int pageSize, int totalItemCount)
            : base(subset, pageNumber, pageSize, totalItemCount)
        {
            DisplayPageNumber = DefaultDisplayPageNumber;
        }

        /// <summary>
        /// StaticPagedListExtension
        /// </summary>
        /// <param name="subset"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalItemCount"></param>
        public StaticPagedListExtension(ICollection<T> subset, int pageNumber, int pageSize, int totalItemCount)
            : base(subset, pageNumber, pageSize, totalItemCount)
        {
            DisplayPageNumber = DefaultDisplayPageNumber;
        }
    }
}
