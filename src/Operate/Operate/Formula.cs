﻿
using System;
using System.Globalization;

namespace Operate
{
    /// <summary>
    /// EnumFormula
    /// </summary>
    public enum EnumFormula
    {
        /// <summary>
        /// 加号
        /// </summary>
        Add,

        /// <summary>
        /// 减号
        /// </summary>
        Dec,

        /// <summary>
        /// 乘号
        /// </summary>
        Mul,

        /// <summary>
        /// 除号
        /// </summary>
        Div,

        /// <summary>
        /// 正玄
        /// </summary>
        Sin,

        /// <summary>
        /// 余玄
        /// </summary>
        Cos,

        /// <summary>
        /// 正切
        /// </summary>
        Tan,

        /// <summary>
        /// 余切
        /// </summary>
        ATan,

        /// <summary>
        /// 平方根
        /// </summary>
        Sqrt,

        /// <summary>
        /// 求幂
        /// </summary>
        Pow,

        /// <summary>
        /// 无
        /// </summary>
        None,
    }

    /// <summary>
    /// 
    /// </summary>
    public class Formula
    {
        private static double CalculateExpress(string expression)
        {
            while (expression.IndexOf("+", StringComparison.Ordinal) != -1 || expression.IndexOf("-", StringComparison.Ordinal) != -1
            || expression.IndexOf("*", StringComparison.Ordinal) != -1 || expression.IndexOf("/", StringComparison.Ordinal) != -1)
            {
                string strTemp;
                string strTempB;
                string strOne;
                string strTwo;
                double replaceValue;
                if (expression.IndexOf("*", StringComparison.Ordinal) != -1)
                {
                    strTemp = expression.Substring(expression.IndexOf("*", StringComparison.Ordinal) + 1, expression.Length - expression.IndexOf("*", StringComparison.Ordinal) - 1);
                    strTempB = expression.Substring(0, expression.IndexOf("*", StringComparison.Ordinal));
                    strOne = strTempB.Substring(GetPrivorPos(strTempB) + 1, strTempB.Length - GetPrivorPos(strTempB) - 1);

                    strTwo = strTemp.Substring(0, GetNextPos(strTemp));
                    replaceValue = Convert.ToDouble(GetExpType(strOne)) * Convert.ToDouble(GetExpType(strTwo));
                    expression = expression.Replace(strOne + "*" + strTwo, replaceValue.ToString(CultureInfo.InvariantCulture));
                }
                else if (expression.IndexOf("/", StringComparison.Ordinal) != -1)
                {
                    strTemp = expression.Substring(expression.IndexOf("/", StringComparison.Ordinal) + 1, expression.Length - expression.IndexOf("/", StringComparison.Ordinal) - 1);
                    strTempB = expression.Substring(0, expression.IndexOf("/", StringComparison.Ordinal));
                    strOne = strTempB.Substring(GetPrivorPos(strTempB) + 1, strTempB.Length - GetPrivorPos(strTempB) - 1);


                    strTwo = strTemp.Substring(0, GetNextPos(strTemp));
                    replaceValue = Convert.ToDouble(GetExpType(strOne)) / Convert.ToDouble(GetExpType(strTwo));
                    expression = expression.Replace(strOne + "/" + strTwo, replaceValue.ToString(CultureInfo.InvariantCulture));
                }
                else if (expression.IndexOf("+", StringComparison.Ordinal) != -1)
                {
                    strTemp = expression.Substring(expression.IndexOf("+", StringComparison.Ordinal) + 1, expression.Length - expression.IndexOf("+", StringComparison.Ordinal) - 1);
                    strTempB = expression.Substring(0, expression.IndexOf("+", StringComparison.Ordinal));
                    strOne = strTempB.Substring(GetPrivorPos(strTempB) + 1, strTempB.Length - GetPrivorPos(strTempB) - 1);

                    strTwo = strTemp.Substring(0, GetNextPos(strTemp));
                    replaceValue = Convert.ToDouble(GetExpType(strOne)) + Convert.ToDouble(GetExpType(strTwo));
                    expression = expression.Replace(strOne + "+" + strTwo, replaceValue.ToString(CultureInfo.InvariantCulture));
                }
                else if (expression.IndexOf("-", StringComparison.Ordinal) != -1)
                {
                    strTemp = expression.Substring(expression.IndexOf("-", StringComparison.Ordinal) + 1, expression.Length - expression.IndexOf("-", StringComparison.Ordinal) - 1);
                    strTempB = expression.Substring(0, expression.IndexOf("-", StringComparison.Ordinal));
                    strOne = strTempB.Substring(GetPrivorPos(strTempB) + 1, strTempB.Length - GetPrivorPos(strTempB) - 1);


                    strTwo = strTemp.Substring(0, GetNextPos(strTemp));
                    replaceValue = Convert.ToDouble(GetExpType(strOne)) - Convert.ToDouble(GetExpType(strTwo));
                    expression = expression.Replace(strOne + "-" + strTwo, replaceValue.ToString(CultureInfo.InvariantCulture));
                }
            }
            return Convert.ToDouble(expression);
        }

        private static double CalculateExExpress(string expression, EnumFormula expressType)
        {
            double retValue = 0;
            switch (expressType)
            {
                case EnumFormula.Sin:
                    retValue = Math.Sin(Convert.ToDouble(expression));
                    break;
                case EnumFormula.Cos:
                    retValue = Math.Cos(Convert.ToDouble(expression));
                    break;
                case EnumFormula.Tan:
                    retValue = Math.Tan(Convert.ToDouble(expression));
                    break;
                case EnumFormula.ATan:
                    retValue = Math.Atan(Convert.ToDouble(expression));
                    break;
                case EnumFormula.Sqrt:
                    retValue = Math.Sqrt(Convert.ToDouble(expression));
                    break;
                case EnumFormula.Pow:
                    retValue = Math.Pow(Convert.ToDouble(expression), 2);
                    break;
            }
            if (Math.Abs(retValue - 0) >0) return Convert.ToDouble(expression);
            return retValue;
        }


        private static int GetNextPos(string expression)
        {
            var expPos = new int[4];
            expPos[0] = expression.IndexOf("+", StringComparison.Ordinal);
            expPos[1] = expression.IndexOf("-", StringComparison.Ordinal);
            expPos[2] = expression.IndexOf("*", StringComparison.Ordinal);
            expPos[3] = expression.IndexOf("/", StringComparison.Ordinal);
            int tmpMin = expression.Length;
            for (int count = 1; count <= expPos.Length; count++)
            {
                if (tmpMin > expPos[count - 1] && expPos[count - 1] != -1)
                {
                    tmpMin = expPos[count - 1];
                }
            }
            return tmpMin;
        }


        private static int GetPrivorPos(string expression)
        {
            var expPos = new int[4];
            expPos[0] = expression.LastIndexOf("+", StringComparison.Ordinal);
            expPos[1] = expression.LastIndexOf("-", StringComparison.Ordinal);
            expPos[2] = expression.LastIndexOf("*", StringComparison.Ordinal);
            expPos[3] = expression.LastIndexOf("/", StringComparison.Ordinal);
            int tmpMax = -1;
            for (int count = 1; count <= expPos.Length; count++)
            {
                if (tmpMax < expPos[count - 1] && expPos[count - 1] != -1)
                {
                    tmpMax = expPos[count - 1];
                }
            }
            return tmpMax;

        }
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string ExecExpression(string expression)
        {
            while (expression.IndexOf("(", StringComparison.Ordinal) != -1)
            {
                string strTemp = expression.Substring(expression.LastIndexOf("(", StringComparison.Ordinal) + 1, expression.Length - expression.LastIndexOf("(", StringComparison.Ordinal) - 1);
                string strExp = strTemp.Substring(0, strTemp.IndexOf(")", StringComparison.Ordinal));
                expression = expression.Replace("(" + strExp + ")", CalculateExpress(strExp).ToString(CultureInfo.InvariantCulture));
            }
            if (expression.IndexOf("+", StringComparison.Ordinal) != -1 || expression.IndexOf("-", StringComparison.Ordinal) != -1
            || expression.IndexOf("*", StringComparison.Ordinal) != -1 || expression.IndexOf("/", StringComparison.Ordinal) != -1)
            {
                expression = CalculateExpress(expression).ToString(CultureInfo.InvariantCulture);
            }
            return expression;
        }

        private static string GetExpType(string expression)
        {
            expression = expression.ToUpper();
            if (expression.IndexOf("SIN", StringComparison.Ordinal) != -1)
            {
                return CalculateExExpress(expression.Substring(expression.IndexOf("N", StringComparison.Ordinal) + 1, expression.Length - 1 - expression.IndexOf("N", StringComparison.Ordinal)), EnumFormula.Sin).ToString(CultureInfo.InvariantCulture);
            }
            if (expression.IndexOf("COS", StringComparison.Ordinal) != -1)
            {
                return CalculateExExpress(expression.Substring(expression.IndexOf("S", StringComparison.Ordinal) + 1, expression.Length - 1 - expression.IndexOf("S", StringComparison.Ordinal)), EnumFormula.Cos).ToString(CultureInfo.InvariantCulture);
            }
            if (expression.IndexOf("TAN", StringComparison.Ordinal) != -1)
            {
                return CalculateExExpress(expression.Substring(expression.IndexOf("N", StringComparison.Ordinal) + 1, expression.Length - 1 - expression.IndexOf("N", StringComparison.Ordinal)), EnumFormula.Tan).ToString(CultureInfo.InvariantCulture);
            }
            if (expression.IndexOf("ATAN", StringComparison.Ordinal) != -1)
            {
                return CalculateExExpress(expression.Substring(expression.IndexOf("N", StringComparison.Ordinal) + 1, expression.Length - 1 - expression.IndexOf("N", StringComparison.Ordinal)), EnumFormula.ATan).ToString(CultureInfo.InvariantCulture);
            }
            if (expression.IndexOf("SQRT", StringComparison.Ordinal) != -1)
            {
                return CalculateExExpress(expression.Substring(expression.IndexOf("T", StringComparison.Ordinal) + 1, expression.Length - 1 - expression.IndexOf("T", StringComparison.Ordinal)), EnumFormula.Sqrt).ToString(CultureInfo.InvariantCulture);
            }
            if (expression.IndexOf("POW", StringComparison.Ordinal) != -1)
            {
                return CalculateExExpress(expression.Substring(expression.IndexOf("W", StringComparison.Ordinal) + 1, expression.Length - 1 - expression.IndexOf("W", StringComparison.Ordinal)), EnumFormula.Pow).ToString(CultureInfo.InvariantCulture);
            }
            return expression;
        }
    }
}
