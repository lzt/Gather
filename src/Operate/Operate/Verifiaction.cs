﻿using System;

namespace Operate
{
    /// <summary>
    /// 通用检验
    /// </summary>
    public static class Verifiaction
    {
        /// <summary>
        /// 验证是否为null
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsNull(this object source)
        {
            return source == null;
        }

        /// <summary>
        /// 是否为数字（以double类型检验）
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsBoolean(this object source)
        {
            if (source.IsNull())
            {
                return false;
            }

            bool i;
            var result = bool.TryParse(source.ToString(), out i);
            return result;
        }

        /// <summary>
        /// 是否为数字（以double类型检验）
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsNumber(this object source)
        {
            if (source.IsNull())
            {
                return false;
            }

            double i;
            var result = double.TryParse(source.ToString(), out i);
            return result;
        }

        /// <summary>
        /// 是否为 Int32 类型
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsInt(this object source)
        {
            return source.IsInt32();
        }

        /// <summary>
        /// 是否为 Int32 类型
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsInt32(this object source)
        {
            if (source.IsNull())
            {
                return false;
            }

            int i;
            var result = Int32.TryParse(source.ToString(), out i);
            return result;
        }

        /// <summary>
        /// 是否为 Int64 类型
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsInt64(this object source)
        {
            if (source.IsNull())
            {
                return false;
            }

            Int64 i;
            var result = Int64.TryParse(source.ToString(), out i);
            return result;
        }

        /// <summary>
        /// 是否为 Decimal 类型
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsDecimal(this object source)
        {
            if (source.IsNull())
            {
                return false;
            }

            decimal i;
            var result = decimal.TryParse(source.ToString(), out i);
            return result;
        }

        /// <summary>
        /// 是否为Datetime
        /// </summary>
        /// <param name="source">要检验的变量</param>
        /// <returns></returns>
        public static bool IsDateTime(this object source)
        {
            if (source.IsNull())
            {
                return false;
            }

            DateTime i;
            var result = DateTime.TryParse(source.ToString(), out i);
            return result;
        }

        #region Check Double

        /// <summary>
        /// Validate Double
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool CheckDouble(params string[] p)
        {
            var result = true;
            foreach (var one in p)
            {
                if (one == null)
                {
                    result = false;
                    break;
                }

                double v;
                if (!double.TryParse(one, out v))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        #endregion Check Double
    }
}