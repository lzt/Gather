﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate
{
    /// <summary>
    /// 集合辅助类
    /// </summary>
    public class CollectionAssistant
    {
        /// <summary>
        /// 字符串包含于
        /// </summary>
        /// <param name="target"></param>
        /// <param name="array"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool In(string target, string[] array, StringComparison comparison = StringComparison.Ordinal)
        {
            var list = array.ToList();
            return In(target, list, comparison);
        }

        /// <summary>
        /// 字符串包含于
        /// </summary>
        /// <param name="target"></param>
        /// <param name="list"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool In(string target, List<string> list, StringComparison comparison = StringComparison.Ordinal)
        {
            if (comparison != StringComparison.OrdinalIgnoreCase)
            {
                return list.Contains(target);
            }
            var ingoreList = list.Select(item => item.ToLower()).ToList();
            return ingoreList.Contains(target.ToLower());
        }

        /// <summary>
        /// 包含于
        /// </summary>
        /// <param name="target"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static bool In<T>(T target, params T[] array) where T : struct
        {
            var list = array.ToList();
            return In(target, list);
        }

        /// <summary>
        /// 包含于
        /// </summary>
        /// <param name="target"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool In<T>(T target, ICollection<T> list) where T : struct
        {
            if (list == null)
            {
                return false;
            }
            return list.Contains(target);
        }
    }
}