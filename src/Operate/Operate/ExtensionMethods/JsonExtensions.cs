﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// JsonExtensions
    /// </summary>
    public static class JsonExtensions
    {
        /// <summary>
        /// CopyValueTo 将json逆序列化的JObject按照键的名称为目标实体对应的属性赋值（仅限类型可以转换成功的属性）
        /// </summary>
        /// <param name="source">    JObject</param>
        /// <param name="target">    目标实体</param>
        /// <param name="ignoreCase">忽略大小写</param>
        /// <typeparam name="T">目标类型</typeparam>
        public static void CopyValueTo<T>(this JObject source, ref T target, bool ignoreCase = false)
        {
            var targetproperties = target.GetType().GetProperties();

            var stop = source.First;
            foreach (var tp in targetproperties)
            {
                do
                {
                    var c = false;
                    if (ignoreCase)
                    {
                        c = tp.Name.ToLower().Equals(stop.Path.ToLower());
                    }
                    else
                    {
                        c = tp.Name.Equals(stop.Path);
                    }

                    if (c)
                    {
                        var v = stop.Value<JProperty>().Value as JValue; ;
                        if (v != null)
                        {
                            var jsonValue = v.Value;

                            //try
                            //{
                            var realValue = Convert.ChangeType(jsonValue, tp.PropertyType);
                            tp.SetValue(target, realValue, null);
                            //}
                            //catch (Exception)
                            //{
                            //    continue;
                            //}
                        }
                    }
                    stop = stop.Next;

                    if (stop == null)
                    {
                        break;
                    }
                } while (stop != source.Last);
            }
        }

        /// <summary>
        /// ToNameValueCollection 转换Json对象为NameValueCollection
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static NameValueCollection ToNameValueCollection(this JObject source)
        {
            return ConvertCollection.JObjectToNameValueCollection(source);
        }

        /// <summary>
        /// ToDictionary 转换Json对象为IDictionary
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IDictionary<string, object> ToDictionary(this JObject source)
        {
            return ConvertCollection.JObjectToDictionary(source);
        }

        /// <summary>
        /// ToDictionary 转换Json对象为IDictionary
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ExpandoObject ToExpandoObject(this JObject source)
        {
            return ConvertCollection.JObjectToExpandoObject(source) as ExpandoObject;
        }
    }
}