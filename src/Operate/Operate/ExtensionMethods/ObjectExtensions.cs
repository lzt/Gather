﻿using System;

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// ObjectExtensions
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// 转换为目标类型
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T ConvertTo<T>(this object value, object defaultValue = null)
        {
            return ConvertCollection.ObjectTo<T>(value, defaultValue);
        }
    }
}