﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

#endregion Usings

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// Array extensions
    /// </summary>
    public static class ArrayExtensions
    {
        #region Functions

        #region GetPropertyValueCollection

        /// <summary>
        /// 取出特定的属性值的集合
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="distinct">去除重复值</param>
        /// <example></example>
        /// <returns></returns>
        public static IList<TP> GetPropertyValueCollection<T, TP>(this T[] source, string propertyName, bool distinct = true)
        {
            if (source.Length == 0)
            {
                return new List<TP>();
            }

            if (propertyName.IsNullOrEmpty())
            {
                throw new Exception("属性名必须有意义，不能为null或空");
            }

            PropertyInfo p;
            if (source.Any())
            {
                var first = source[0];
                p = first.GetType().GetProperty(propertyName);
            }
            else
            {
                return null;
            }

            if (p != null)
            {
                var t = default(TP);
                var type = t.GetType();
                if (p.PropertyType == type)
                {
                    var result = new List<TP>();

                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in source)
                    {
                        var v = (TP)(p.GetValue(item, null));
                        if (distinct)
                        {
                            if (!result.Contains(v))
                            {
                                result.Add(v);
                            }
                        }
                        else
                        {
                            result.Add(v);
                        }
                    }
                    return result;
                }
                return null;
            }
            return null;
        }

        #endregion GetPropertyValueCollection

        #region Get

        /// <summary>
        /// 根据条件获取对象 满足筛选条件的第一个元素
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example></example>
        /// <returns></returns>
        public static T Get<T>(this T[] source, object filter, string propertyName = null) where T : class
        {
            var r = source.GetAll(filter, propertyName);
            if (r != null)
            {
                if (r.Length > 0)
                {
                    return r[0];
                }
                return null;
            }
            return null;
        }

        /// <summary>
        /// 根据条件获取对象 满足筛选条件的所有元素
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example></example>
        /// <returns></returns>
        public static T[] GetAll<T>(this T[] source, object filter, string propertyName = null) where T : class
        {
            var result = new List<T>();
            if (source.Length == 0)
            {
                return null;
            }
            var filtertypeIsDefinedBaseType = filter.IsDefinedBaseType();
            var first = source[0];
            var firsttypeIsDefinedBaseType = first.IsDefinedBaseType();

            if (filtertypeIsDefinedBaseType && firsttypeIsDefinedBaseType)
            {
                if (filter.GetType().FullName == first.GetType().FullName)
                {
                    if (source.Contains((T)filter))
                    {
                        result.AddRange(source.Where(one => one == filter));
                    }
                }
                return result.ToArray();
            }

            if (!filtertypeIsDefinedBaseType && firsttypeIsDefinedBaseType)
            {
                if (propertyName.IsNullOrEmpty())
                {
                    return null;

                    //throw new Exception("源集合类型为基础类型，判断类型为非基础类型，此情况下属性名必须有意义，propertyName不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = filter.GetType().GetProperty(propertyName);

                if (p.PropertyType.FullName == first.GetType().FullName)
                {
                    var v = (T)(p.GetValue(filter, null));
                    if (source.Contains(v))
                    {
                        result.AddRange(source.Where(one => one == filter));
                    }
                }
                return result.ToArray();
            }

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (!firsttypeIsDefinedBaseType && filtertypeIsDefinedBaseType)
            {
                if (propertyName.IsNullOrEmpty())
                {
                    return null;

                    //throw new Exception("要判断的类型为基础类型，此情况下属性名必须有意义，propertyName不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = first.GetType().GetProperty(propertyName);

                if (p.PropertyType.FullName == filter.GetType().FullName)
                {
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in source)
                    {
                        var v = (p.GetValue(item, null));
                        if (v.Equals(filter))
                        {
                            result.Add(item);
                        }
                    }
                }
                return result.ToArray();
            }

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (!filtertypeIsDefinedBaseType && !firsttypeIsDefinedBaseType)
            {
                if (filter.GetType().FullName == first.GetType().FullName)
                {
                    if (source.Contains((T)filter))
                    {
                        // ReSharper disable once LoopCanBeConvertedToQuery
                        foreach (var item in source)
                        {
                            if (item.EqualsByProperty(filter))
                            {
                                result.Add(item);
                            }
                        }
                    }
                    return result.ToArray();
                }

                if (propertyName.IsNullOrEmpty())
                {
                    return null;

                    //throw new Exception("要判断的类型不同,此时只能判断是否含有相同的属性值，propertyName不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var filterpropertyinfo = filter.GetType().GetProperty(propertyName);
                if (filterpropertyinfo == null)
                {
                    return null;
                }
                var filterpropertyvalue = filterpropertyinfo.GetValue(filter, null);

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = first.GetType().GetProperty(propertyName);
                if (p == null)
                {
                    return null;
                }
                if (p.PropertyType.FullName == filterpropertyinfo.PropertyType.FullName)
                {
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in source)
                    {
                        var v = (p.GetValue(item, null));

                        if (v.IsDefinedBaseType())
                        {
                            if (v.Equals(filterpropertyvalue))
                            {
                                result.Add(item);
                            }
                        }
                        else
                        {
                            if (v.EqualsByProperty(filterpropertyvalue))
                            {
                                result.Add(item);
                            }
                        }
                    }
                }
                return result.ToArray();
            }

            return null;
        }

        #endregion Get

        #region ContainsByProperty

        /// <summary>
        /// 判断对象是否包含
        /// </summary>
        /// <param name="sources">源</param>
        /// <param name="target">筛选对象</param>
        /// <example></example>
        /// <returns></returns>
        public static bool ContainsByProperty<T>(this T[] sources, object target) where T : class
        {
            var result = false;

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in sources)
            {
                if (item.EqualsByProperty(target))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        #endregion ContainsByProperty

        #region ExistByPropertyValue

        /// <summary>
        /// 判断对象是否存在
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example></example>
        /// <returns></returns>
        public static bool ExistByPropertyValue<T>(this T[] source, object filter, string propertyName = null) where T : class
        {
            if (source.Length == 0)
            {
                return false;
            }
            var filtertypeIsDefinedBaseType = filter.IsDefinedBaseType();
            var first = source[0];
            var firsttypeIsDefinedBaseType = first.IsDefinedBaseType();

            if (filtertypeIsDefinedBaseType && firsttypeIsDefinedBaseType)
            {
                if (filter.GetType().FullName == first.GetType().FullName)
                {
                    return source.Contains((T)filter);
                }
                return false;
            }

            if (!filtertypeIsDefinedBaseType && firsttypeIsDefinedBaseType)
            {
                if (propertyName.IsNullOrEmpty())
                {
                    return false;

                    //throw new Exception("源集合类型为基础类型，判断类型为非基础类型，此情况下属性名必须有意义，propertyName不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = filter.GetType().GetProperty(propertyName);

                if (p.PropertyType.FullName == first.GetType().FullName)
                {
                    var v = (T)(p.GetValue(filter, null));
                    return source.Contains(v);
                }
                return false;
            }

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (!firsttypeIsDefinedBaseType && filtertypeIsDefinedBaseType)
            {
                if (propertyName.IsNullOrEmpty())
                {
                    return false;

                    //throw new Exception("要判断的类型为基础类型，此情况下属性名必须有意义，propertyName不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = first.GetType().GetProperty(propertyName);

                if (p.PropertyType.FullName == filter.GetType().FullName)
                {
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in source)
                    {
                        var v = (p.GetValue(item, null));
                        if (v.Equals(filter))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (!filtertypeIsDefinedBaseType && !firsttypeIsDefinedBaseType)
            {
                if (filter.GetType().FullName == first.GetType().FullName)
                {
                    return source.ContainsByProperty(filter);
                }

                if (propertyName.IsNullOrEmpty())
                {
                    return false;

                    //throw new Exception("要判断的类型不同,此时只能判断是否含有相同的属性值，propertyName不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var filterpropertyinfo = filter.GetType().GetProperty(propertyName);
                if (filterpropertyinfo == null)
                {
                    return false;
                }
                var filterpropertyvalue = filterpropertyinfo.GetValue(filter, null);

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = first.GetType().GetProperty(propertyName);
                if (p == null)
                {
                    return false;
                }
                if (p.PropertyType.FullName == filterpropertyinfo.PropertyType.FullName)
                {
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in source)
                    {
                        var v = (p.GetValue(item, null));
                        if (v.Equals(filterpropertyvalue))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }

            return false;
        }

        #endregion ExistByPropertyValue

        #region SortByPropertyValue

        /// <summary>
        /// 按照属性值排序
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="rule">排序规则</param>
        /// <param name="sortcount">针对前几位元素进行排序，默认全部元素</param>
        /// <example></example>
        /// <returns></returns>
        public static T[] SortByPropertyValue<T>(this T[] source, ConstantCollection.SortRule rule = ConstantCollection.SortRule.Asc, string propertyName = null, int? sortcount = null)
        {
            var result = new List<T>();
            if (source.Length == 0)
            {
                return source;
            }

            if (sortcount == 0 || sortcount == null)
            {
                sortcount = source.Length;
            }

            if (sortcount > source.Length)
            {
                sortcount = source.Length;
            }

            var t = source[0];
            var filterisdefinedBaseType = t.IsDefinedBaseType();

            if (filterisdefinedBaseType)
            {
                var valuelist = source.Select(item => (IComparable)item).ToList();
                for (int i = 1; i < sortcount; i++)
                {
                    if (valuelist[i].CompareTo(valuelist[i - 1]) < 0)
                    {               //若第i个元素大于i-1元素，直接插入。小于的话，移动有序表后插入
                        int j = i - 1;
                        var x = valuelist[i];        //复制为哨兵，即存储待排序元素
                        valuelist[i] = valuelist[i - 1];           //先后移一个元素
                        while (j >= 0 && (x.CompareTo(valuelist[j]) < 0))
                        {  //查找在有序表的插入位置
                            valuelist[j + 1] = valuelist[j];
                            j--;         //元素后移
                        }
                        valuelist[j + 1] = x;      //插入到正确位置
                    }
                }
                result = valuelist.ToList().Cast<T>().ToList();

                if (rule == ConstantCollection.SortRule.Desc)
                {
                    var newresult = new List<T>();
                    for (int index = result.Count - 1; index >= 0; index--)
                    {
                        var item = result[index];
                        newresult.Add(item);
                    }
                    return newresult.ToArray();
                }
                return result.ToArray();
            }
            else
            {
                if (propertyName.IsNullOrEmpty())
                {
                    throw new Exception("非基础类型的比较条件，必须指定属性名");
                }

                var type = source[0].GetType();

                // ReSharper disable once AssignNullToNotNullAttribute
                var property = type.GetProperty(propertyName);
                if (property == null)
                {
                    throw new Exception("指定的属性不存在");
                }
                var firstvalue = property.GetValue(source[0], null);
                if (!firstvalue.IsDefinedBaseType())
                {
                    throw new Exception("指定的属性类型只支持基础类型");
                }

                var test = firstvalue as IComparable;
                if (test == null)
                {
                    throw new Exception("指定的属性类型不支持比较");
                }

                var valuelist = source.Select(item => (IComparable)property.GetValue(item, null)).ToList();
                var sortvaluelist = valuelist.SortByPropertyValue(rule, null, sortcount);

                if (sortvaluelist.Count == 0)
                {
                    return source;
                }

                foreach (var item in sortvaluelist)
                {
                    foreach (var one in source)
                    {
                        var v = property.GetValue(one, null);
                        if (v.Equals(item))
                        {
                            if (result.Contains(one))
                            {
                                continue;
                            }
                            result.Add(one);
                        }
                    }
                }

                return result.ToArray();
            }
        }

        #endregion SortByPropertyValue

        #region Join

        /// <summary>
        /// JoinCore 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="connector">连接符</param>
        /// <param name="template">格式化字符串</param>
        /// <param name="distinct">去除重复，默认去重</param>
        /// <returns></returns>
        private static string JoinCore<T>(IEnumerable<T> source, string connector, string template = "", bool distinct = true)
        {
            var resilt = "";
            var loglist = new List<T>();
            foreach (var one in source)
            {
                if (string.IsNullOrWhiteSpace(Convert.ToString(one)))
                {
                    continue;
                }

                if (distinct)
                {
                    if (loglist.Contains(one))
                    {
                        continue;
                    }
                }

                if (template.IsNullOrEmpty())
                {
                    resilt += one + connector;
                }
                else
                {
                    resilt += template.FormatValue(one.ToString()) + connector;
                }
                loglist.Add(one);
            }
            if (!connector.IsNullOrEmpty())
            {
                var connectorlength = connector.Length;
                resilt = resilt.Remove(resilt.Length - connectorlength);
            }
            return resilt;
        }

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new string[3]{"1","2","3"};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this string[] source, string connector, string template = "", bool distinct = true)
        //{
        //    if (source.Length == 0)
        //    {
        //        return "";
        //    }
        //    var sourceobject = source;
        //    return JoinCore(sourceobject, connector, template, distinct);
        //}

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new int[3]{1,2,3};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this int[] source, string connector, string template = "", bool distinct = true)
        //{
        //    if (source.Length == 0)
        //    {
        //        return "";
        //    }
        //    var sourceobject = source.Cast<object>();
        //    return JoinCore(sourceobject, connector, template, distinct);
        //}

        /// <summary>
        /// Join 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔, 如果属性值不是Int,或者string,将会被忽略
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名称(非基础类型必填)</param>
        /// <param name="connector">连接符</param>
        /// <param name="template">格式化字符串</param>
        /// <param name="distinct">去除重复，默认去重</param>
        /// <example></example>
        /// <returns></returns>
        public static string Join<T>(this T[] source, string connector, string template = "", bool distinct = true, string propertyName = null)
        {
            if (source.Length == 0)
            {
                return "";
            }

            var first = source[0];
            var type = first.GetType();
            var c = Type.GetTypeCode(type);
            if (c == TypeCode.Object)
            {
                if (propertyName.IsNullOrEmpty())
                {
                    throw new Exception("属性名必须有意义，不能为null或空");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                var p = first.GetType().GetProperty(propertyName);
                if (p != null)
                {
                    var objlist = new List<object>();

                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in source)
                    {
                        var v = p.GetValue(item, null);
                        objlist.Add(v);
                    }
                    return JoinCore(objlist, connector, template, distinct);
                }
            }
            else
            {
                return JoinCore(source, connector, template, distinct);
            }

            return null;
        }

        #endregion Join

        /// <summary>
        /// 读取数组
        /// </summary>
        /// <param name="src">源数组</param>
        /// <param name="offset">起始位置</param>
        /// <param name="count">复制字节数</param>
        /// <returns>返回复制的总字节数</returns>
        public static Byte[] ReadBytes(this Byte[] src, Int32 offset = 0, Int32 count = 0)
        {
            // 即使是全部，也要复制一份，而不只是返回原数组，因为可能就是为了复制数组
            if (count <= 0) count = src.Length - offset;

            var bts = new Byte[count];
            Buffer.BlockCopy(src, offset, bts, 0, bts.Length);
            return bts;
        }

        /// <summary>
        /// 一个数组是否以另一个数组开头
        /// </summary>
        /// <param name="source"></param>
        /// <param name="buffer">缓冲区</param>
        /// <returns></returns>
        public static Boolean StartsWith(this Byte[] source, Byte[] buffer)
        {
            if (source.Length < buffer.Length) return false;

            // ReSharper disable LoopCanBeConvertedToQuery
            for (int i = 0; i < buffer.Length; i++)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (source[i] != buffer[i]) return false;
            }
            return true;
        }

        /// <summary>
        /// 一个数组是否以另一个数组结尾
        /// </summary>
        /// <param name="source"></param>
        /// <param name="buffer">缓冲区</param>
        /// <returns></returns>
        public static Boolean EndsWith(this Byte[] source, Byte[] buffer)
        {
            if (source.Length < buffer.Length) return false;

            var p = source.Length - buffer.Length;

            // ReSharper disable LoopCanBeConvertedToQuery
            for (int i = 0; i < buffer.Length; i++)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (source[p + i] != buffer[i]) return false;
            }
            return true;
        }

        /// <summary>
        /// 在字节数组中查找另一个字节数组的位置，不存在则返回-1
        /// </summary>
        /// <param name="source">字节数组</param>
        /// <param name="buffer">另一个字节数组</param>
        /// <param name="offset">偏移</param>
        /// <param name="length">查找长度</param>
        /// <returns></returns>
        public static Int64 IndexOf(this Byte[] source, Byte[] buffer, Int64 offset = 0, Int64 length = 0) { return IndexOf(source, 0, 0, buffer, offset, length); }

        /// <summary>
        /// 在字节数组中查找另一个字节数组的位置，不存在则返回-1
        /// </summary>
        /// <param name="source">字节数组</param>
        /// <param name="start">源数组起始位置</param>
        /// <param name="count">查找长度</param>
        /// <param name="buffer">另一个字节数组</param>
        /// <param name="offset">偏移</param>
        /// <param name="length">查找长度</param>
        /// <returns></returns>
        public static Int64 IndexOf(this Byte[] source, Int64 start, Int64 count, Byte[] buffer, Int64 offset = 0, Int64 length = 0)
        {
            if (start < 0) start = 0;
            if (count <= 0 || count > source.Length - start) count = source.Length;
            if (length <= 0 || length > buffer.Length - offset) length = buffer.Length - offset;

            // 已匹配字节数
            Int64 win = 0;
            for (Int64 i = start; i + length - win <= count; i++)
            {
                if (source[i] == buffer[offset + win])
                {
                    win++;

                    // 全部匹配，退出
                    if (win >= length) return i - length + 1 - start;
                }
                else
                {
                    //win = 0; // 只要有一个不匹配，马上清零
                    // 不能直接清零，那样会导致数据丢失，需要逐位探测，窗口一个个字节滑动
                    i = i - win;
                    win = 0;
                }
            }

            return -1;
        }

        /// <summary>
        /// 比较两个字节数组大小。相等返回0，不等则返回不等的位置，如果位置为0，则返回1。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="buffer">缓冲区</param>
        /// <returns></returns>
        public static Int32 CompareTo(this Byte[] source, Byte[] buffer) { return CompareTo(source, 0, 0, buffer); }

        /// <summary>
        /// 比较两个字节数组大小。相等返回0，不等则返回不等的位置，如果位置为0，则返回1。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="start"></param>
        /// <param name="count">数量</param>
        /// <param name="buffer">缓冲区</param>
        /// <param name="offset">偏移</param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Int32 CompareTo(this Byte[] source, Int64 start, Int64 count, Byte[] buffer, Int64 offset = 0, Int64 length = 0)
        {
            if (source == buffer) return 0;

            if (start < 0) start = 0;
            if (count <= 0 || count > source.Length - start) count = source.Length - start;
            if (length <= 0 || length > buffer.Length - offset) length = buffer.Length - offset;

            // 逐字节比较
            for (int i = 0; i < count && i < length; i++)
            {
                Int32 rs = source[start + i].CompareTo(buffer[offset + i]);
                if (rs != 0) return i > 0 ? i : 1;
            }

            // 比较完成。如果长度不相等，则较长者较大
            if (count != length) return count > length ? 1 : -1;

            return 0;
        }

        /// <summary>
        /// 字节数组分割
        /// </summary>
        /// <param name="buf"></param>
        /// <param name="sps"></param>
        /// <returns></returns>
        public static IEnumerable<Byte[]> Split(this Byte[] buf, Byte[] sps)
        {
            int p;
            var idx = 0;
            while (true)
            {
                p = (Int32)buf.IndexOf(idx, 0, sps);
                if (p < 0) break;

                yield return buf.ReadBytes(idx, p);

                idx += p + sps.Length;
            }
            if (idx < buf.Length)
            {
                p = buf.Length - idx;
                yield return buf.ReadBytes(idx, p);
            }
        }

        #region 十六进制编码

        /// <summary>
        /// 把字节数组编码为十六进制字符串
        /// </summary>
        /// <param name="data">字节数组</param>
        /// <param name="offset">偏移</param>
        /// <param name="count">数量</param>
        /// <returns></returns>
        public static String ToHex(this Byte[] data, Int32 offset = 0, Int32 count = 0)
        {
            if (data == null || data.Length < 1) return null;
            if (count <= 0) count = data.Length - offset;

            //return BitConverter.ToString(data).Replace("-", null);
            // 上面的方法要替换-，效率太低
            var cs = new Char[count * 2];

            // 两个索引一起用，避免乘除带来的性能损耗
            for (int i = 0, j = 0; i < count; i++, j += 2)
            {
                Byte b = data[offset + i];
                cs[j] = GetHexValue(b / 0x10);
                cs[j + 1] = GetHexValue(b % 0x10);
            }
            return new String(cs);
        }

        /// <summary>
        /// 把字节数组编码为十六进制字符串，带有分隔符和分组功能
        /// </summary>
        /// <param name="data">字节数组</param>
        /// <param name="separate">分隔符</param>
        /// <param name="groupSize">分组大小，为0时对每个字节应用分隔符，否则对每个分组使用</param>
        /// <returns></returns>
        public static String ToHex(this Byte[] data, String separate, Int32 groupSize = 0)
        {
            if (data == null || data.Length < 1) return null;
            if (groupSize < 0) groupSize = 0;

            if (groupSize == 0)
            {
                // 没有分隔符
                if (String.IsNullOrEmpty(separate)) return data.ToHex();

                // 特殊处理
                if (separate == "-") return BitConverter.ToString(data);
            }

            var count = data.Length;
            var len = count * 2;
            if (!String.IsNullOrEmpty(separate)) len += (count - 1) * separate.Length;
            if (groupSize > 0)
            {
                // 计算分组个数
                var g = (count - 1) / groupSize;
                len += g * 2;

                // 扣除间隔
                if (!String.IsNullOrEmpty(separate)) len -= g * separate.Length;
            }
            var sb = new StringBuilder(len);
            for (int i = 0; i < count; i++)
            {
                if (sb.Length > 0)
                {
                    if (i % groupSize == 0)
                        sb.AppendLine();
                    else
                        sb.Append(separate);
                }

                Byte b = data[i];
                sb.Append(GetHexValue(b / 0x10));
                sb.Append(GetHexValue(b % 0x10));
            }

            return sb.ToString();
        }

        private static char GetHexValue(int i)
        {
            if (i < 10) return (char)(i + 0x30);
            return (char)(i - 10 + 0x41);
        }

        /// <summary>
        /// 把十六进制字符串解码字节数组
        /// </summary>
        /// <param name="data">字节数组</param>
        /// <param name="startIndex">起始位置</param>
        /// <param name="length">长度</param>
        /// <returns></returns>
        [Obsolete("ToHex")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static Byte[] FromHex(this String data, Int32 startIndex = 0, Int32 length = 0) { return ToHex(data, startIndex, length); }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="data">Hex编码的字符串</param>
        /// <param name="startIndex">起始位置</param>
        /// <param name="length">长度</param>
        /// <returns></returns>
        public static Byte[] ToHex(this String data, Int32 startIndex = 0, Int32 length = 0)
        {
            if (String.IsNullOrEmpty(data)) return null;

            // 过滤特殊字符
            data = data.Trim()
                .Replace("-", null)
                .Replace("0x", null)
                .Replace("0X", null)
                .Replace(" ", null)
                .Replace("\r", null)
                .Replace("\n", null)
                .Replace(",", null);

            if (length <= 0) length = data.Length - startIndex;

            var bts = new Byte[length / 2];
            for (int i = 0; i < bts.Length; i++)
            {
                bts[i] = Byte.Parse(data.Substring(startIndex + 2 * i, 2), NumberStyles.HexNumber);
            }
            return bts;
        }

        #endregion 十六进制编码

        /// <summary>
        /// 合并两个数组
        /// </summary>
        /// <param name="src">源数组</param>
        /// <param name="des">目标数组</param>
        /// <param name="offset">起始位置</param>
        /// <param name="count">字节数</param>
        /// <returns></returns>
        public static Byte[] Combine(this Byte[] src, Byte[] des, Int32 offset = 0, Int32 count = 0)
        {
            if (count <= 0) count = src.Length - offset;

            var buf = new Byte[src.Length + count];
            Buffer.BlockCopy(src, 0, buf, 0, src.Length);
            Buffer.BlockCopy(des, offset, buf, src.Length, count);
            return buf;
        }

        #region Clear

        /// <summary>
        /// Clears the array completely
        /// </summary>
        /// <param name="array">Array to clear</param>
        /// <returns>The final array</returns>
        /// <example>
        /// <code>
        ///int[] TestObject = new int[] { 1, 2, 3, 4, 5, 6 };
        ///TestObject.Clear();
        /// </code>
        /// </example>
        public static Array Clear(this Array array)
        {
            if (array == null)
                return null;
            Array.Clear(array, 0, array.Length);
            return array;
        }

        /// <summary>
        /// Clears the array completely
        /// </summary>
        /// <param name="array">Array to clear</param>
        /// <typeparam name="TArrayType">Array type</typeparam>
        /// <returns>The final array</returns>
        /// <example>
        /// <code>
        ///int[] TestObject = new int[] { 1, 2, 3, 4, 5, 6 };
        ///TestObject.Clear();
        /// </code>
        /// </example>
        public static TArrayType[] Clear<TArrayType>(this TArrayType[] array)
        {
            return (TArrayType[])((Array)array).Clear();
        }

        #endregion Clear

        #region Convert

        /// <summary>
        ///转换为List
        /// </summary>
        /// <typeparam name="TArrayType"></typeparam>
        /// <param name="array"></param>
        /// <returns></returns>
        public static List<TArrayType> ToList<TArrayType>(this TArrayType[] array)
        {
            var result = new List<TArrayType>();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var a in array)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                result.Add(a);
            }
            return result;
        }

        #endregion Convert

        #region Concat

        /// <summary>
        /// Combines two arrays and returns a new array containing both values
        /// </summary>
        /// <typeparam name="TArrayType">Type of the data in the array</typeparam>
        /// <param name="array1">Array 1</param>
        /// <param name="additions">Arrays to concat onto the first item</param>
        /// <returns>A new array containing both arrays' values</returns>
        /// <example>
        /// <code>
        ///int[] TestObject1 = new int[] { 1, 2, 3 };
        ///int[] TestObject2 = new int[] { 4, 5, 6 };
        ///int[] TestObject3 = new int[] { 7, 8, 9 };
        ///TestObject1 = TestObject1.Combine(TestObject2, TestObject3);
        /// </code>
        /// </example>
        public static TArrayType[] Concat<TArrayType>(this TArrayType[] array1, params TArrayType[][] additions)
        {
            if (array1.IsNull())
            {
                throw new ArgumentNullException("array1");
            }
            if (additions.IsNull())
            {
                throw new ArgumentNullException("additions");
            }
            var result = new TArrayType[array1.Length + additions.Sum(x => x.Length)];
            int offset = array1.Length;
            Array.Copy(array1, 0, result, 0, array1.Length);
            foreach (var t in additions)
            {
                Array.Copy(t, 0, result, offset, t.Length);
                offset += t.Length;
            }
            return result;
        }

        #endregion Concat

        #endregion Functions
    }
}