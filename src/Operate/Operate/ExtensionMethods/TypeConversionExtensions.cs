﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Operate.Conversion;

#endregion

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// Extensions converting between types, checking if something is null, etc.
    /// </summary>
    public static class TypeConversionExtensions
    {
        #region Functions

        #region FormatToString

        /// <summary>
        /// Calls the object's ToString function passing in the formatting
        /// </summary>
        /// <param name="input">Input object</param>
        /// <param name="format">Format of the output string</param>
        /// <returns>The formatted string</returns>
        public static string FormatToString(this object input, string format)
        {
            if (input == null)
                return "";
            return !string.IsNullOrEmpty(format) ? input.Call<string>("ToString", format) : input.ToString();
        }

        #endregion

        #region ToList

        /// <summary>
        /// Attempts to convert the DataTable to a list of objects
        /// </summary>
        /// <typeparam name="T">Type the objects should be in the list</typeparam>
        /// <param name="data">DataTable to convert</param>
        /// <param name="creator">Function used to create each object</param>
        /// <returns>The DataTable converted to a list of objects</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<T> ToList<T>(this DataTable data, Func<T> creator = null) where T : new()
        {
            if (data == null)
                return new List<T>();
            creator = creator.Check(() => new T());
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties();
            var results = new List<T>();
            for (int x = 0; x < data.Rows.Count; ++x)
            {
                T rowObject = creator();

                for (int y = 0; y < data.Columns.Count; ++y)
                {
                    PropertyInfo property = properties.FirstOrDefault(z => z.Name == data.Columns[y].ColumnName);
                    if (property != null)
                        property.SetValue(rowObject, data.Rows[x][data.Columns[y]].To(property.PropertyType, null), new object[] { });
                }
                results.Add(rowObject);
            }
            return results;
        }

        #endregion
        
        #region To

        /// <summary>
        /// Attempts to convert the object to another type and returns the value
        /// </summary>
        /// <typeparam name="T">Type to convert from</typeparam>
        /// <typeparam name="TR">Return type</typeparam>
        /// <param name="Object">Object to convert</param>
        /// <param name="defaultValue">Default value to return if there is an issue or it can't be converted</param>
        /// <returns>The object converted to the other type or the default value if there is an error or can't be converted</returns>
        public static TR To<T, TR>(this T Object, TR defaultValue = default(TR))
        {
            return Converter.To(Object, defaultValue);
        }

        /// <summary>
        /// Attempts to convert the object to another type and returns the value
        /// </summary>
        /// <typeparam name="T">Type to convert from</typeparam>
        /// <param name="resultType">Result type</param>
        /// <param name="Object">Object to convert</param>
        /// <param name="defaultValue">Default value to return if there is an issue or it can't be converted</param>
        /// <returns>The object converted to the other type or the default value if there is an error or can't be converted</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public static object To<T>(this T Object, Type resultType, object defaultValue = null)
        {
            return Converter.To(Object, resultType, defaultValue);
        }

        #endregion

        #endregion

        #region Private Static Properties

        private static readonly Manager Converter = new Manager();

        #endregion
    }
}