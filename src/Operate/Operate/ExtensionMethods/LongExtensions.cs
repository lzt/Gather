﻿using System.Collections.Generic;

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// LongExtensions
    /// </summary>
    public static class LongExtensions
    {
        #region In

        /// <summary>
        /// 包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static bool In(this long input, params long[] array)
        {
            return CollectionAssistant.In(input, array);
        }

        /// <summary>
        /// 包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool In(this long input, ICollection<long> list)
        {
            return CollectionAssistant.In(input, list);
        }

        #endregion In
    }
}