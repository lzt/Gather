﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// Int扩展
    /// </summary>
    public static class IntExtensions
    {
        #region In

        /// <summary>
        /// 包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static bool In(this int input, params int[] array)
        {
            return CollectionAssistant.In(input, array);
        }

        /// <summary>
        /// 包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool In(this int input, ICollection<int> list)
        {
            return CollectionAssistant.In(input, list);
        }

        #endregion In
    }
}