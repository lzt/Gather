﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using Operate.Formatters;
using Operate.Formatters.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

#endregion Usings

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// </summary>
    public enum SplitOption
    {
        /// <summary>
        /// 不作处理
        /// </summary>
        None,

        /// <summary>
        /// 过滤重复项
        /// </summary>
        FilterSame,
    }

    /// <summary>
    /// String and StringBuilder extensions
    /// </summary>
    public static class StringExtensions
    {
        #region Functions

        //#region

        //public static string InsertString(int Index, char chr, int length);

        //#endregion

        #region CutOut

        /// <summary>
        /// 截取指定个数字符串
        /// </summary>
        /// <param name="source">字符源</param>
        /// <param name="length">保留字符个数</param>
        /// <param name="showellipsis">省去的字符显示的符号，默认"..."</param>
        /// <param name="direction">计数开始方向，默认从左边开始计数</param>
        /// <returns></returns>
        public static string CutOut(
            this string source,
            int length,
            string showellipsis = "...",
            ConstantCollection.StringCutDirection direction = ConstantCollection.StringCutDirection.FromLeft
        )
        {
            if (source.IsNullOrEmpty())
            {
                return source;
            }

            var fulllength = source.Length;
            if (direction == ConstantCollection.StringCutDirection.FromRight)
            {
                if (length >= fulllength)
                {
                    return source;
                }

                return showellipsis + source.Substring(fulllength - length, length);
            }

            if (length >= fulllength)
            {
                return source;
            }

            return source.Substring(0, length) + showellipsis;
        }

        #endregion CutOut

        #region In

        /// <summary>
        /// 字符串包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static bool In(this string input, params string[] array)
        {
            return CollectionAssistant.In(
                input,
                array,
                StringComparison.Ordinal
            );
        }

        /// <summary>
        /// 字符串包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="array"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool In(this string input, string[] array, StringComparison comparison = StringComparison.Ordinal)
        {
            return CollectionAssistant.In(
                input,
                array,
                comparison
            );
        }

        /// <summary>
        /// 字符串包含于
        /// </summary>
        /// <param name="input"></param>
        /// <param name="list"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool In(
            this string input,
            List<string> list,
            StringComparison comparison = StringComparison.Ordinal
        )
        {
            return CollectionAssistant.In(
                input,
                list,
                comparison
            );
        }

        #endregion In

        #region Filter

        /// <summary>
        /// 过滤Html标记，获取文本
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string FilterHtmlTag(this string input)
        {
            return Filter.FilterHtmlTag(input);
        }

        #endregion Filter

        #region AppendLineFormat

        /// <summary>
        /// Does an AppendFormat and then an AppendLine on the StringBuilder
        /// </summary>
        /// <param name="builder">Builder object</param>
        /// <param name="format">Format string</param>
        /// <param name="objects">Objects to format</param>
        /// <returns>The StringBuilder passed in</returns>
        public static StringBuilder AppendLineFormat(this StringBuilder builder, string format, params object[] objects)
        {
            return builder.AppendFormat(
                    CultureInfo.InvariantCulture,
                    format,
                    objects
                )
                .AppendLine();
        }

        #endregion AppendLineFormat

        #region

        /// <summary>
        /// 转全角(SBC case)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToSBC(this string input)
        {
            //半角转全角：
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 32)
                {
                    c[i] = (char)12288;
                    continue;
                }

                if (c[i] < 127)
                {
                    c[i] = (char)(c[i] + 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 转半角(SBC case)
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns></returns>
        public static string ToDBC(string input)
        {
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                {
                    c[i] = (char)(c[i] - 65248);
                }
            }

            return new string(c);
        }

        /// <summary>
        /// 转换首字母大写
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToUpperFirst(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return source;
            }

            if (string.IsNullOrEmpty(source.TrimStart()))
            {
                return source;
            }

            var firstchar = source.GetByIndex(0).ToUpper();
            return source.ReplaceByIndex(0, firstchar);
        }

        /// <summary>
        /// 转换首字母小写
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToLowerFirst(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return source;
            }

            if (string.IsNullOrEmpty(source.TrimStart()))
            {
                return source;
            }

            var firstchar = source.GetByIndex(0).ToLower();
            return source.ReplaceByIndex(0, firstchar);
        }

        /// <summary>
        /// 替换指定的某位字符
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <param name="index">指定起始位置</param>
        /// <param name="newstring">要替换成的字符串</param>
        /// <param name="length">替换的位数</param>
        /// <returns></returns>
        public static string ReplaceByIndex(
            this string source,
            int index,
            string newstring,
            int length = 1
        )
        {
            if (length < 0)
            {
                throw new Exception("替换长度不能小于0");
            }

            if (index + length + 1 > source.Length)
            {
                length = source.Length - index;
            }

            if (index > source.Length - 1)
            {
                return source;
            }

            var replacesreing = "";
            if (index == 0)
            {
                string strTemp = source.Remove(0, length);
                replacesreing = "";
                for (int i = 0; i < length; i++)
                {
                    replacesreing += newstring;
                }

                return replacesreing + strTemp;
            }

            if (index == source.Length - 1)
            {
                string strTemp = source.Remove(index);
                return strTemp + newstring;
            }

            string strTempFirst = source.Remove(index);
            for (int i = 0; i < length; i++)
            {
                replacesreing += newstring;
            }

            string strTempLast = source.Remove(0, index + length);
            return strTempFirst + replacesreing + strTempLast;
        }

        /// <summary>
        /// 获取字符串中指定位
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <param name="index">指定位置</param>
        /// <returns></returns>
        public static string GetByIndex(this string source, int index)
        {
            if (index > source.Length - 1)
            {
                return "";
            }

            if (index == 0)
            {
                if (source.Length == 1)
                {
                    return source;
                }

                string strTemp = source.Remove(1);
                return strTemp;
            }

            if (index == source.Length - 1)
            {
                string strTemp = source.Remove(0, index);
                return strTemp;
            }
            else
            {
                string strTemp = source.Remove(index + 1);
                strTemp = strTemp.Remove(0, index);
                return strTemp;
            }
        }

        #endregion Functions

        #region Get

        #endregion Get

        #region Format

        /// <summary>
        /// 格式化
        /// </summary>
        /// <param name="input">格式化字符串</param>
        /// <param name="param">格式化参数</param>
        /// <returns></returns>
        public static string FormatValue(this string input, params object[] param)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            return string.Format(input, param);
        }

        #endregion Format

        #region Convert

        /// <summary>
        /// 十六进制字符串转换为Byte[]
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return returnBytes;
        }

        /// <summary>
        /// 按照分隔符分割为Array
        /// </summary>
        /// <param name="input">源字符串</param>
        /// <param name="delimiter">分隔符</param>
        /// <param name="option">分割配置</param>
        /// <returns></returns>
        public static string[] SplitToArray(this string input, string delimiter, SplitOption option = SplitOption.None)
        {
            var temp = input.SplitToList(delimiter, option);
            return temp.ToArray();
        }

        /// <summary>
        /// 按照分隔符分割为List
        /// </summary>
        /// <param name="input">源字符串</param>
        /// <param name="delimiter">分隔符</param>
        /// <param name="option">分割配置</param>
        /// <returns></returns>
        public static List<string> SplitToList(
            this string input,
            string delimiter,
            SplitOption option = SplitOption.None
        )
        {
            var result = new List<string>();

            if (input.IsNullOrEmpty())
            {
                return result;
            }

            string[] array = new Regex(delimiter).Split(input);

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var s in array)

                // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (!s.IsEmpty())
                {
                    if (option == SplitOption.FilterSame)
                    {
                        if (result.Contains(s))
                        {
                            continue;
                        }
                    }

                    result.Add(s);
                }
            }

            return result;
        }

        /// <summary>
        /// 在 2，8，10，16进制之间进行转换；
        /// </summary>
        /// <param name="input"></param>
        /// <param name="fromBase"></param>
        /// <param name="toBase"></param>
        /// <returns></returns>
        public static string ToBase(this string input, int fromBase, int toBase)
        {
            if (!input.IsInt())
            {
                return null;
            }

            try
            {
                int intValue = Convert.ToInt32(input, fromBase); //先转成10进制
                string result = Convert.ToString(intValue, toBase); //再转成目标进制
                if (toBase == 2)
                {
                    int resultLength = result.Length; //获取二进制的长度
                    switch (resultLength)
                    {
                        case 7:
                            result = "0" + result;
                            break;

                        case 6:
                            result = "00" + result;
                            break;

                        case 5:
                            result = "000" + result;
                            break;

                        case 4:
                            result = "0000" + result;
                            break;

                        case 3:
                            result = "00000" + result;
                            break;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("转换进制时发生异常:{0}", ex.Message));
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static long ToInt64(this string v)
        {
            return v.IsInt64() ? Convert.ToInt64(v) : 0L;
        }

        #endregion Convert

        #region

        /// <summary>
        /// 对比字符串，忽略大小写
        /// </summary>
        /// <param name="input"></param>
        /// <param name="compare">对比源</param>
        /// <returns></returns>
        public static bool CompareIn(this string input, params string[] compare)
        {
            return input.CompareIn(StringComparison.OrdinalIgnoreCase, compare);
        }

        /// <summary>
        /// 对比字符串
        /// </summary>
        /// <param name="input"></param>
        /// <param name="option">对比规则</param>
        /// <param name="compare">对比源</param>
        /// <returns></returns>
        public static bool CompareIn(this string input, StringComparison option, params string[] compare)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            if (compare.Length == 0)
            {
                return false;
            }

            var result = false;
            foreach (var s in compare)
            {
                result = input.Equals(s, option);
                if (result)
                {
                    break;
                }
            }

            return result;
        }

        #endregion

        #region Verifiaction

        /// <summary>
        /// 验证是否是数字
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumber(this string input)
        {
            return Verifiaction.IsNumber(input);
        }

        /// <summary>
        /// 验证是否是日期
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsDateTime(this string input)
        {
            return Verifiaction.IsDateTime(input);
        }

        /// <summary>
        /// 验证是否是整形
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsInt(this string input)
        {
            return Verifiaction.IsInt(input);
        }

        /// <summary>
        /// 验证是否是Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsDecimal(this string input)
        {
            return Verifiaction.IsDecimal(input);
        }

        /// <summary>
        /// 验证匹配匹配
        /// </summary>
        /// <param name="input">要搜索匹配项的字符串</param>
        /// <param name="pattern">要匹配的正则表达式模式</param>
        /// <param name="options">筛选条件</param>
        /// <returns></returns>
        public static bool IsMatch(this string input, string pattern, RegexOptions options = RegexOptions.IgnoreCase)
        {
            return !input.IsNullOrEmpty() && Regex.IsMatch(
                input,
                pattern,
                options
            );
        }

        /// <summary>
        /// 检验是否为空
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsEmpty(this string input)
        {
            return input == "" || input == string.Empty;
        }

        /// <summary>
        /// 检验是否为null或空
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        /// <summary>
        /// 检验去空格后是否为空
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsTrimEmpty(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return true;
            }

            if (input.Trim().IsNullOrEmpty())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 验证IP地址是否合法
        /// </summary>
        /// <param name="ip">要验证的IP地址</param>
        public static bool IsIP(this string ip)
        {
            //如果为空，认为验证合格
            if (IsNullOrEmpty(ip))
            {
                return true;
            }

            //清除要验证字符串中的空格
            ip = ip.Trim();

            //模式字符串
            const string pattern = @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$";

            //验证
            return ip.IsMatch(pattern);
        }

        /// <summary>
        /// 检验是否为url
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsURL(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var reg = new Regex(
                @"^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.[a-zA-Z]{2,4})(\:[0-9]+)?(/[^/][a-zA-Z0-9\.\,\?\'\\/\+&amp;%\$#\=~_\-@]*)*$"
            );
            return reg.IsMatch(input);
        }

        /// <summary>
        /// 检测是否为中文
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <returns>输出true/false</returns>
        public static bool IsZhCn(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var reg = new Regex(@"[\u4e00-\u9fa5]");
            return reg.IsMatch(input);
        }

        /// <summary>
        /// 检测是否为中文和字母
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <returns>输出true/false</returns>
        public static bool IsZhCnOrLitterOrNumber(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var reg = new Regex(@"^[a-zA-Z0-9_\u4e00-\u9fa5]+$");
            return reg.IsMatch(input);
        }

        /// <summary>
        /// 检测Email格式
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <returns>输出true/false</returns>
        public static bool IsEmail(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var reg = new Regex(@"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$");
            return reg.IsMatch(input);
        }

        /// <summary>
        /// 检测国内电话和手机
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <returns>输出true/false</returns>
        public static bool IsPhoneOrMobile(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var regPhone = new Regex(@"^0?\\d{11}$");
            var regMobile = new Regex(@"^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}(-\d{1,4})?$");

            //Regex regPhone = new Regex(@"((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)");
            return regPhone.IsMatch(input) || regMobile.IsMatch(input);
        }

        /// <summary>
        /// 检测手机
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsMobile(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var regMobile = new Regex(@"^1(3[4-9]|4[7]|5[01256789]|8[0-9])\d{8}$");
            return regMobile.IsMatch(input);
        }

        /// <summary>
        /// 检测QQ号码
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <returns>输出true/false</returns>
        public static bool IsQQ(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            var regQq = new Regex(@"[1-9][0-9]{4,}");
            return regQq.IsMatch(input);
        }

        /// <summary>
        /// 验证身份证是否合法
        /// </summary>
        /// <param name="idCard">要验证的身份证</param>
        public static bool IsIdCard(this string idCard)
        {
            //如果为空，认为验证合格
            if (idCard.IsNullOrEmpty())
            {
                return true;
            }

            //清除要验证字符串中的空格
            idCard = idCard.Trim();

            //模式字符串
            var pattern = new StringBuilder();
            pattern.Append(@"^(11|12|13|14|15|21|22|23|31|32|33|34|35|36|37|41|42|43|44|45|46|");
            pattern.Append(@"50|51|52|53|54|61|62|63|64|65|71|81|82|91)");
            pattern.Append(@"(\d{13}|\d{15}[\dx])$");

            //验证
            return idCard.IsMatch(pattern.ToString());
        }

        /// <summary>
        /// 检测长度
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <param name="length">长度</param>
        /// <returns>输出true/false</returns>
        public static bool LessThan(this string input, int length)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            return input.Length < length;
        }

        /// <summary>
        /// 检测长度
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <param name="length">长度</param>
        /// <returns>输出true/false</returns>
        public static bool MoreThan(this string input, int length)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }

            return input.Length > length;
        }

        #endregion

        #region Contain

        /// <summary>
        /// 检测是否包含数字
        /// </summary>
        /// <param name="input">检测字符串</param>
        /// <returns>输出true/false</returns>
        public static bool ContainsNumber(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            const string regEx = @"/\d/ ";
            return Regex.IsMatch(input, regEx);
        }

        /// <summary>
        /// 判断包含是否包含特定字符
        /// </summary>
        /// <param name="input">需要检测的字符串</param>
        /// <param name="containParam">包含的字符数组</param>
        /// <returns>返回true/flase</returns>
        public static bool Contains(this string input, char[] containParam)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            if (containParam.Length == 0)
            {
                return true;
            }

            var result = false;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (char t in containParam)

                // ReSharper restore LoopCanBeConvertedToQuery
            {
                var index = input.IndexOf(t);
                if (index != -1)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        #endregion

        #region Center

        /// <summary>
        /// Centers the input string (if it's longer than the length) and pads it using the padding
        /// string
        /// </summary>
        /// <param name="input"></param>
        /// <param name="length"></param>
        /// <param name="padding"></param>
        /// <returns>The centered string</returns>
        public static string Center(this string input, int length, string padding = " ")
        {
            if (string.IsNullOrEmpty(input))
            {
                input = "";
            }

            string output = "";
            for (int x = 0; x < (length - input.Length) / 2; ++x)
            {
                output += padding[x % padding.Length];
            }

            output += input;
            for (int x = 0; x < (length - input.Length) / 2; ++x)
            {
                output += padding[x % padding.Length];
            }

            return output;
        }

        #endregion

        #region Encode

        /// <summary>
        /// Converts a string to a string of another encoding
        /// </summary>
        /// <param name="input">input string</param>
        /// <param name="originalEncodingUsing">
        /// The type of encoding the string is currently using (defaults to ASCII)
        /// </param>
        /// <param name="encodingUsing">
        /// The type of encoding the string is converted into (defaults to UTF8)
        /// </param>
        /// <returns>string of the byte array</returns>
        public static string Encode(
            this string input,
            Encoding originalEncodingUsing = null,
            Encoding encodingUsing = null
        )
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            originalEncodingUsing = originalEncodingUsing.Check(new ASCIIEncoding());
            encodingUsing = encodingUsing.Check(new UTF8Encoding());
            return Encoding.Convert(
                    originalEncodingUsing,
                    encodingUsing,
                    input.ToByteArray(originalEncodingUsing)
                )
                .ToString(encodingUsing);
        }

        #endregion

        #region FromBase64

        /// <summary>
        /// Converts base 64 string based on the encoding passed in
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="encodingUsing">
        /// The type of encoding the string is using (defaults to UTF8)
        /// </param>
        /// <returns>string in the encoding format</returns>
        public static string FromBase64(this string input, Encoding encodingUsing)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            byte[] tempArray = Convert.FromBase64String(input);
            return encodingUsing.Check(() => new UTF8Encoding()).GetString(tempArray);
        }

        /// <summary>
        /// Converts base 64 string to a byte array
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>A byte array equivalent of the base 64 string</returns>
        public static byte[] FromBase64(this string input)
        {
            return string.IsNullOrEmpty(input) ? new byte[0] : Convert.FromBase64String(input);
        }

        #endregion

        #region Is

        /// <summary>
        /// Is this value of the specified type
        /// </summary>
        /// <param name="value">Value to compare</param>
        /// <param name="comparisonType">Comparison type</param>
        /// <returns>True if it is of the type specified, false otherwise</returns>
        public static bool Is(this string value, StringCompare comparisonType)
        {
            if (comparisonType == StringCompare.CreditCard)
            {
                long checkSum = 0;
                value = value.Replace("-", "").Reverse();
                for (int x = 0; x < value.Length; ++x)
                {
                    if (!value[x].Is(CharIs.Digit))
                    {
                        return false;
                    }

                    int tempValue = (value[x] - '0') * (x % 2 == 1 ? 2 : 1);
                    while (tempValue > 0)
                    {
                        checkSum += tempValue % 10;
                        tempValue /= 10;
                    }
                }

                return (checkSum % 10) == 0;
            }

            if (comparisonType == StringCompare.Unicode)
            {
                return string.IsNullOrEmpty(value) || Regex.Replace(
                    value,
                    @"[^\u0000-\u007F]",
                    ""
                ) != value;
            }

            return value.Is("", StringCompare.Anagram);
        }

        /// <summary>
        /// Is this value of the specified type
        /// </summary>
        /// <param name="value1">Value 1 to compare</param>
        /// <param name="value2">Value 2 to compare</param>
        /// <param name="comparisonType">Comparison type</param>
        /// <returns>True if it is of the type specified, false otherwise</returns>
        public static bool Is(this string value1, string value2, StringCompare comparisonType)
        {
            if (comparisonType != StringCompare.Anagram)
            {
                return value1.Is(comparisonType);
            }

            return new string(value1.OrderBy(x => x).ToArray()) == new string(value2.OrderBy(x => x).ToArray());
        }

        #endregion

        #region Keep

        /// <summary>
        /// Removes everything that is not in the filter text from the input.
        /// </summary>
        /// <param name="input">Input text</param>
        /// <param name="filter">Regex expression of text to keep</param>
        /// <returns>The input text minus everything not in the filter text.</returns>
        public static string Keep(this string input, string filter)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(filter))
            {
                return "";
            }

            var tempRegex = new Regex(filter);
            var collection = tempRegex.Matches(input);
            var builder = new StringBuilder();
            foreach (Match match in collection)
            {
                builder.Append(match.Value);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Removes everything that is not in the filter text from the input.
        /// </summary>
        /// <param name="input">Input text</param>
        /// <param name="filter">Predefined filter to use (can be combined as they are flags)</param>
        /// <returns>The input text minus everything not in the filter text.</returns>
        public static string Keep(this string input, StringFilter filter)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            string value = BuildFilter(filter);
            return input.Keep(value);
        }

        #endregion

        #region Left

        /// <summary>
        /// Gets the first x number of characters from the left hand side
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="length">x number of characters to return</param>
        /// <returns>The resulting string</returns>
        public static string Left(this string input, int length)
        {
            return string.IsNullOrEmpty(input) ? "" : input.Substring(0, input.Length > length ? length : input.Length);
        }

        #endregion

        #region LevenshteinDistance

        /// <summary>
        /// Calculates the Levenshtein distance
        /// </summary>
        /// <param name="value1">Value 1</param>
        /// <param name="value2">Value 2</param>
        /// <returns>The Levenshtein distance</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Microsoft.Performance",
            "CA1814:PreferJaggedArraysOverMultidimensional",
            MessageId = "Body"
        )]
        public static int LevenshteinDistance(this string value1, string value2)
        {
            var matrix = new int[value1.Length + 1, value2.Length + 1];
            for (int x = 0; x <= value1.Length; ++x)
            {
                matrix[x, 0] = x;
            }

            for (int x = 0; x <= value2.Length; ++x)
            {
                matrix[0, x] = x;
            }

            for (int x = 1; x <= value1.Length; ++x)
            {
                for (int y = 1; y <= value2.Length; ++y)
                {
                    int cost = value1[x - 1] == value2[y - 1] ? 0 : 1;
                    matrix[x, y] =
                        new[] { matrix[x - 1, y] + 1, matrix[x, y - 1] + 1, matrix[x - 1, y - 1] + cost }.Min();
                    if (x > 1 && y > 1 && value1[x - 1] == value2[y - 2] && value1[x - 2] == value2[y - 1])
                    {
                        matrix[x, y] = new[] { matrix[x, y], matrix[x - 2, y - 2] + cost }.Min();
                    }
                }
            }

            return matrix[value1.Length, value2.Length];
        }

        #endregion

        #region MaskLeft

        /// <summary>
        /// Masks characters to the left ending at a specific character
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="endPosition">End position (counting from the left)</param>
        /// <param name="mask">Mask character to use</param>
        /// <returns>The masked string</returns>
        public static string MaskLeft(this string input, int endPosition = 4, char mask = '#')
        {
            string appending = "";
            for (int x = 0; x < endPosition; ++x)
            {
                appending += mask;
            }

            return appending + input.Remove(0, endPosition);
        }

        #endregion

        #region MaskRight

        /// <summary>
        /// Masks characters to the right starting at a specific character
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="startPosition">Start position (counting from the left)</param>
        /// <param name="mask">Mask character to use</param>
        /// <returns>The masked string</returns>
        public static string MaskRight(this string input, int startPosition = 4, char mask = '#')
        {
            if (startPosition > input.Length)
            {
                return input;
            }

            string appending = "";
            for (int x = 0; x < input.Length - startPosition; ++x)
            {
                appending += mask;
            }

            return input.Remove(startPosition) + appending;
        }

        #endregion

        #region NumberTimesOccurs

        /// <summary>
        /// returns the number of times a string occurs within the text
        /// </summary>
        /// <param name="input">input text</param>
        /// <param name="match">The string to match (can be regex)</param>
        /// <returns>The number of times the string occurs</returns>
        public static int NumberTimesOccurs(this string input, string match)
        {
            return string.IsNullOrEmpty(input) ? 0 : new Regex(match).Matches(input).Count;
        }

        #endregion

        #region Pluralize

        /// <summary>
        /// Pluralizes a word
        /// </summary>
        /// <param name="word">Word to pluralize</param>
        /// <param name="culture">
        /// Culture info used to pluralize the word (defaults to current culture)
        /// </param>
        /// <returns>The word pluralized</returns>
        public static string Pluralize(this string word, CultureInfo culture = null)
        {
            if (string.IsNullOrEmpty(word))
            {
                return "";
            }

            culture = culture.Check(CultureInfo.CurrentCulture);
            return PluralizationService.CreateService(culture).Pluralize(word);
        }

        #endregion

        #region Remove

        /// <summary>
        /// 移除复合筛选条件的字符串中的元素并返回移除后的新字符串
        /// </summary>
        /// <param name="input">源字符串</param>
        /// <param name="filter">筛选条件</param>
        /// <param name="ignoringCase">忽略大小写</param>
        /// <returns>返回移除符合筛选条件后的新字符串</returns>
        public static string Remove(this string input, List<char> filter, bool ignoringCase = true)
        {
            var newfilter = new List<char>();
            if (ignoringCase)
            {
                newfilter.AddRange(
                    from chr in filter
                    select chr.ToString().ToLower()
                    into newchar
                    where !newchar.IsTrimEmpty()
                    select Convert.ToChar(newchar)
                );
            }
            else
            {
                newfilter = filter;
            }

            var a = input.ToArray();
            var result =
                (from chr in a
                    let cc = ignoringCase ? Convert.ToChar(chr.ToString(CultureInfo.InvariantCulture).ToLower()) : chr
                    where !newfilter.Contains(cc)
                    select chr).Aggregate("", (current, chr) => current + chr);

            return result;
        }

        /// <summary>
        /// Removes everything that is in the filter text from the input.
        /// </summary>
        /// <param name="input">Input text</param>
        /// <param name="filter">Regex expression of text to remove</param>
        /// <returns>Everything not in the filter text.</returns>
        public static string Remove(this string input, string filter)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(filter))
            {
                return input;
            }

            return new Regex(filter).Replace(input, "");
        }

        /// <summary>
        /// Removes everything that is in the filter text from the input.
        /// </summary>
        /// <param name="input">Input text</param>
        /// <param name="filter">Predefined filter to use (can be combined as they are flags)</param>
        /// <returns>Everything not in the filter text.</returns>
        public static string Remove(this string input, StringFilter filter)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            string value = BuildFilter(filter);
            return input.Remove(value);
        }

        #endregion

        #region Replace

        /// <summary>
        /// Replaces everything that is in the filter text with the value specified.
        /// </summary>
        /// <param name="input">Input text</param>
        /// <param name="value">Value to fill in</param>
        /// <param name="filter">Predefined filter to use (can be combined as they are flags)</param>
        /// <returns>The input text with the various items replaced</returns>
        public static string Replace(this string input, StringFilter filter, string value = "")
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            string filterValue = BuildFilter(filter);
            return new Regex(filterValue).Replace(input, value);
        }

        /// <summary>
        /// 移除复合筛选条件的字符串中的元素并返回移除后的新字符串
        /// </summary>
        /// <param name="input">源字符串</param>
        /// <param name="replacrstring">替换成的字符串</param>
        /// <param name="filter">筛选条件</param>
        /// <param name="ignoringCase">忽略大小写</param>
        /// <returns>返回移除符合筛选条件后的新字符串</returns>
        public static string Replace(
            this string input,
            string replacrstring,
            List<char> filter,
            bool ignoringCase = true
        )
        {
            var newfilter = new List<char>();
            if (ignoringCase)
            {
                newfilter.AddRange(
                    from chr in filter
                    select chr.ToString().ToLower()
                    into newchar
                    where !newchar.IsTrimEmpty()
                    select Convert.ToChar(newchar)
                );
            }
            else
            {
                newfilter = filter;
            }

            var a = input.ToArray();
            var result = "";

            foreach (var chr in a)
            {
                var cc = ignoringCase ? Convert.ToChar(chr.ToString(CultureInfo.InvariantCulture).ToLower()) : chr;
                if (newfilter.Contains(cc))
                {
                    result += replacrstring;
                }
                else
                {
                    result += cc;
                }
            }

            return result;
        }

        #endregion

        #region Reverse

        /// <summary>
        /// Reverses a string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>The reverse of the input string</returns>
        public static string Reverse(this string input)
        {
            return new string(input.Reverse<char>().ToArray());
        }

        #endregion

        #region Right

        /// <summary>
        /// Gets the last x number of characters from the right hand side
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="length">x number of characters to return</param>
        /// <returns>The resulting string</returns>
        public static string Right(this string input, int length)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            length = input.Length > length ? length : input.Length;
            return input.Substring(input.Length - length, length);
        }

        #endregion

        #region Singularize

        /// <summary>
        /// Singularizes a word
        /// </summary>
        /// <param name="word">Word to singularize</param>
        /// <param name="culture">
        /// Culture info used to singularize the word (defaults to current culture)
        /// </param>
        /// <returns>The word singularized</returns>
        public static string Singularize(this string word, CultureInfo culture = null)
        {
            if (string.IsNullOrEmpty(word))
            {
                return "";
            }

            culture = culture.Check(CultureInfo.CurrentCulture);
            return PluralizationService.CreateService(culture).Singularize(word);
        }

        #endregion

        #region StripLeft

        /// <summary>
        /// Strips out any of the characters specified starting on the left side of the input string
        /// (stops when a character not in the list is found)
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="characters">Characters to strip (defaults to a space)</param>
        /// <returns>The Input string with specified characters stripped out</returns>
        public static string StripLeft(this string input, string characters = " ")
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            if (string.IsNullOrEmpty(characters))
            {
                return input;
            }

            return input.SkipWhile(characters.Contains).ToString(x => x.ToString(CultureInfo.InvariantCulture), "");
        }

        #endregion

        #region StripRight

        /// <summary>
        /// Strips out any of the characters specified starting on the right side of the input
        /// string (stops when a character not in the list is found)
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="characters">Characters to strip (defaults to a space)</param>
        /// <returns>The Input string with specified characters stripped out</returns>
        public static string StripRight(this string input, string characters = " ")
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            if (string.IsNullOrEmpty(characters))
            {
                return input;
            }

            int position = input.Length - 1;
            for (int x = input.Length - 1; x >= 0; --x)
            {
                if (!characters.Contains(input[x]))
                {
                    position = x + 1;
                    break;
                }
            }

            return input.Left(position);
        }

        #endregion

        #region StripIllegalXML

        /// <summary>
        /// Strips illegal characters for XML content
        /// </summary>
        /// <param name="content">Content</param>
        /// <returns>The stripped string</returns>
        public static string StripIllegalXml(this string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return "";
            }

            var builder = new StringBuilder();
            foreach (char Char in content)
            {
                if (Char == 0x9
                    || Char == 0xA
                    || Char == 0xD
                    || (Char >= 0x20 && Char <= 0xD7FF)
                    || (Char >= 0xE000 && Char <= 0xFFFD))
                {
                    builder.Append(Char);
                }
            }

            return builder.ToString()
                .Replace('\u2013', '-')
                .Replace('\u2014', '-')
                .Replace('\u2015', '-')
                .Replace('\u2017', '_')
                .Replace('\u2018', '\'')
                .Replace('\u2019', '\'')
                .Replace('\u201a', ',')
                .Replace('\u201b', '\'')
                .Replace('\u201c', '\"')
                .Replace('\u201d', '\"')
                .Replace('\u201e', '\"')
                .Replace("\u2026", "...")
                .Replace('\u2032', '\'')
                .Replace('\u2033', '\"')
                .Replace("`", "\'")
                .Replace("&", "&amp;")
                .Replace("<", "&lt;")
                .Replace(">", "&gt;")
                .Replace("\"", "&quot;")
                .Replace("\'", "&apos;");
        }

        #endregion

        #region ToByteArray

        /// <summary>
        /// Converts a string to a byte array
        /// </summary>
        /// <param name="input">input string</param>
        /// <param name="encodingUsing">
        /// The type of encoding the string is using (defaults to UTF8)
        /// </param>
        /// <returns>the byte array representing the string</returns>
        public static byte[] ToByteArray(this string input, Encoding encodingUsing = null)
        {
            return string.IsNullOrEmpty(input) ? null : encodingUsing.Check(new UTF8Encoding()).GetBytes(input);
        }

        #endregion

        #region ToString

        /// <summary>
        /// Formats the string based on the capitalization specified
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="Case">Capitalization type to use</param>
        /// <returns>Capitalizes the string based on the case specified</returns>
        public static string ToString(this string input, StringCase Case)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            if (Case == StringCase.FirstCharacterUpperCase)
            {
                char[] inputChars = input.ToCharArray();
                for (int x = 0; x < inputChars.Length; ++x)
                {
                    if (inputChars[x] != ' ' && inputChars[x] != '\t')
                    {
                        inputChars[x] = char.ToUpper(inputChars[x], CultureInfo.InvariantCulture);
                        break;
                    }
                }

                return new string(inputChars);
            }

            if (Case == StringCase.SentenceCapitalize)
            {
                string[] seperator = { ".", "?", "!" };
                string[] inputStrings = input.Split(seperator, StringSplitOptions.None);
                for (int x = 0; x < inputStrings.Length; ++x)
                {
                    if (!string.IsNullOrEmpty(inputStrings[x]))
                    {
                        var tempRegex = new Regex(inputStrings[x]);
                        inputStrings[x] = inputStrings[x].ToString(StringCase.FirstCharacterUpperCase);
                        input = tempRegex.Replace(input, inputStrings[x]);
                    }
                }

                return input;
            }

            if (Case == StringCase.TitleCase)
            {
                string[] seperator = { " ", ".", "\t", Environment.NewLine, "!", "?" };
                string[] inputStrings = input.Split(seperator, StringSplitOptions.None);
                for (int x = 0; x < inputStrings.Length; ++x)
                {
                    if (!string.IsNullOrEmpty(inputStrings[x])
                        && inputStrings[x].Length > 3)
                    {
                        var tempRegex = new Regex(
                            inputStrings[x].Replace(")", @"\)").Replace("(", @"\(").Replace("*", @"\*")
                        );
                        inputStrings[x] = inputStrings[x].ToString(StringCase.FirstCharacterUpperCase);
                        input = tempRegex.Replace(input, inputStrings[x]);
                    }
                }

                return input;
            }

            return input;
        }

        /// <summary>
        /// Formats a string based on a format string passed in. The default formatter uses the
        /// following format: # = digits @ = alpha characters \ = escape char
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="format">Format of the output string</param>
        /// <param name="provider">
        /// String formatter provider (defaults to GenericStringFormatter)
        /// </param>
        /// <returns>The formatted string</returns>
        public static string ToString(this string input, string format, IStringFormatter provider = null)
        {
            return provider.Check(new GenericStringFormatter()).Format(input, format);
        }

        /// <summary>
        /// Formats a string based on the object's properties
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="Object">Object to use to format the string</param>
        /// <param name="startSeperator">
        /// Seperator character/string to use to describe the start of the property name
        /// </param>
        /// <param name="endSeperator">
        /// Seperator character/string to use to describe the end of the property name
        /// </param>
        /// <returns>The formatted string</returns>
        public static string ToString(
            this string input,
            object Object,
            string startSeperator = "{",
            string endSeperator = "}"
        )
        {
            if (Object == null)
            {
                return input;
            }

            Object.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.CanRead)
                .ForEach(
                    x =>
                    {
                        var value = x.GetValue(Object, null);
                        input = input.Replace(
                            startSeperator + x.Name + endSeperator,
                            value == null ? "" : value.ToString()
                        );
                    }
                );
            return input;
        }

        /// <summary>
        /// Formats a string based on the key/value pairs that are sent in
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="pairs">
        /// Key/value pairs. Replaces the key with the corresponding value.
        /// </param>
        /// <returns>The string after the changes have been made</returns>
        public static string ToString(this string input, params KeyValuePair<string, string>[] pairs)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (KeyValuePair<string, string> pair in pairs)

                // ReSharper restore LoopCanBeConvertedToQuery
            {
                input = input.Replace(pair.Key, pair.Value);
            }

            return input;
        }

        /// <summary>
        /// Uses a regex to format the input string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="format">Regex string used to</param>
        /// <param name="outputFormat">Output format</param>
        /// <param name="options">Regex options</param>
        /// <returns>The input string formatted by using the regex string</returns>
        public static string ToString(
            this string input,
            string format,
            string outputFormat,
            RegexOptions options = RegexOptions.None
        )
        {
            if (input.IsNullOrEmpty())
            {
                throw new ArgumentNullException("input");
            }

            return Regex.Replace(
                input,
                format,
                outputFormat,
                options
            );
        }

        #endregion

        /// <summary>
        /// 过滤sql危险字符
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string FilterSql(this string source)
        {
            return (source ?? "").Replace("'", "''");
        }

        /// <summary>
        /// 当为空字符串时，用默认值代替
        /// </summary>
        /// <param name="source"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string DefaultWhenNullOrEmpty(this string source, string defaultValue)
        {
            return string.IsNullOrWhiteSpace(source) ? defaultValue : source;
        }

        #endregion

        #region Private Functions

        private static string BuildFilter(StringFilter filter)
        {
            string filterValue = "";
            string separator = "";
            if (filter.HasFlag(StringFilter.Alpha))
            {
                filterValue += separator + "[a-zA-Z]";
                separator = "|";
            }

            if (filter.HasFlag(StringFilter.Numeric))
            {
                filterValue += separator + "[0-9]";
                separator = "|";
            }

            if (filter.HasFlag(StringFilter.FloatNumeric))
            {
                filterValue += separator + @"[0-9\.]";
                separator = "|";
            }

            if (filter.HasFlag(StringFilter.ExtraSpaces))
            {
                filterValue += separator + @"[ ]{2,}";
            }

            return filterValue;
        }

        #endregion
    }

    #region Enums

    /// <summary>
    /// What sort of string capitalization should be used?
    /// </summary>
    public enum StringCase
    {
        /// <summary>
        /// Sentence capitalization
        /// </summary>
        SentenceCapitalize,

        /// <summary>
        /// First character upper case
        /// </summary>
        FirstCharacterUpperCase,

        /// <summary>
        /// Title case
        /// </summary>
        TitleCase
    }

    /// <summary>
    /// What type of string comparison are we doing?
    /// </summary>
    public enum StringCompare
    {
        /// <summary>
        /// Is this a credit card number?
        /// </summary>
        CreditCard,

        /// <summary>
        /// Is this an anagram?
        /// </summary>
        Anagram,

        /// <summary>
        /// Is this Unicode
        /// </summary>
        Unicode
    }

    /// <summary>
    /// Predefined filters
    /// </summary>
    [Flags]
    public enum StringFilter
    {
        /// <summary>
        /// Alpha characters
        /// </summary>
        Alpha = 1,

        /// <summary>
        /// Numeric characters
        /// </summary>
        Numeric = 2,

        /// <summary>
        /// Numbers with period, basically allows for decimal point
        /// </summary>
        FloatNumeric = 4,

        /// <summary>
        /// Multiple spaces
        /// </summary>
        ExtraSpaces = 8
    }

    #endregion
}