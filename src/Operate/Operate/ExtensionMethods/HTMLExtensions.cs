﻿using System.Text.RegularExpressions;

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// Set of HTML related extensions (and HTTP related)
    /// HTML 和 HTTP 相关的扩展
    /// </summary>
    public static class HtmlExtensions
    {
        #region Variables

        /// <summary>
        /// StripHtmlRegex
        /// </summary>
        public static readonly Regex StripHtmlRegex = new Regex("<[^>]*>", RegexOptions.Compiled);
  
        #endregion

        #region ContainsHTML

        /// <summary>
        /// Decides if the string contains HTML
        /// 是否包含HTML字符串
        /// </summary>
        /// <param name="input">Input string to check 检测源</param>
        /// <returns>false if it does not contain HTML, true otherwise</returns>
        public static bool ContainsHtml(this string input)
        {
            return !string.IsNullOrEmpty(input) && StripHtmlRegex.IsMatch(input);
        }

        #endregion

        #region StripHTML

        /// <summary>
        /// Removes HTML elements from a string
        /// </summary>
        /// <param name="html">HTML laiden string</param>
        /// <returns>HTML-less string</returns>
        public static string StripHtml(this string html)
        {
            if (string.IsNullOrEmpty(html))
                return "";
            html = StripHtmlRegex.Replace(html, string.Empty);
            return html.Replace("&nbsp;", " ")
                       .Replace("&#160;", string.Empty);
        }

        #endregion
    }
}
