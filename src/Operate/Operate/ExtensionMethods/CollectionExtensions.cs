﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

#endregion

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// ICollection extensions
    /// </summary>
    public static class CollectionExtensions
    {
        #region Functions

        #region Set 集合

        /// <summary>
        /// 当前集合是否是目标集合target的父集
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool IsTargetSuperset<T>(this ICollection<T> source, ICollection<T> target)
        {
            if (target==null)
            {
                return false;
            }

            var result = true;

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in target)
            {
                if (!source.Contains(item))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// 当前集合是否是目标集合target的子集
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool IsTargetSubset<T>(this ICollection<T> source, ICollection<T> target)
        {
            if (target == null)
            {
                return false;
            }

            var result = true;

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in source)
            {
                if (!target.Contains(item))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// 获取当前集合不在目标集合中的元素集
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ICollection<T> GetListNotInTarget<T>(this ICollection<T> source, ICollection<T> target)
        {
            if (target == null)
            {
                return source;
            }

            var list = new List<T>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in source)
            {
                if (!target.Contains(item))
                {
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// 获取集合交集
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ICollection<T> GetIntersection<T>(this ICollection<T> source, ICollection<T> target)
        {
            var list = new List<T>();

            if (target == null)
            {
                return list;
            }

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in source)
            {
                if (target.Contains(item))
                {
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// 获取两集合全集
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ICollection<T> GetUniversalSet<T>(this ICollection<T> source, ICollection<T> target)
        {
            if (target == null)
            {
                return source;
            }

            var list = new List<T>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in source)
            {
                list.Add(item);
            }

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in target)
            {
                if (!source.Contains(item))
                {
                    list.Add(item);
                }
            }

            return list;
        }

        #endregion

        #region Distinct

        /// <summary>
        /// 去重(public属性值完全重复的函数)
        /// 较适用于基础类型
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ICollection<T> DistinctByProperty<T>(this ICollection<T> source) where T : class
        {
            var result = source.GetType().Create() as ICollection<T>;
            foreach (var item in source)
            {
                if (result != null && !result.ContainsByProperty(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }

        #endregion

        #region Get  Property Value  Collection  取出特定的属性值的集合

        /// <summary>
        /// 取出特定的属性值的集合
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="distinct">去除重复值</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static IList<TP> GetPropertyValueCollection<T, TP>(this ICollection<T> source, string propertyName, bool distinct = true)
        {
            var sourcearray = source.ToArray();
            return sourcearray.GetPropertyValueCollection<T, TP>(propertyName, distinct);
        }

        /// <summary>
        /// 取出特定的属性值的集合
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="distinct">去除重复值</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static IList<TP> GetPropertyValueCollection<T, TP>(this IEnumerable<T> source, string propertyName, bool distinct = true)
        {
            var sourcearray = source.ToArray();
            return sourcearray.GetPropertyValueCollection<T, TP>(propertyName, distinct);
        }

        /// <summary>
        /// 取出特定的属性值的集合
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="distinct">去除重复值</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static IList<TP> GetPropertyValueCollection<T, TP>(this IList<T> source, string propertyName, bool distinct = true)
        {
            var sourcearray = source.ToArray();
            return sourcearray.GetPropertyValueCollection<T, TP>(propertyName, distinct);
        }

        #endregion

        #region Get By Property  根据属性条件获取对象

        #region Get from ICollection

        /// <summary>
        /// 根据属性条件获取对象
        /// 满足筛选条件的第一个元素
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static T Get<T>(this ICollection<T> source, object filter, string propertyName = null) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.Get(filter, propertyName);
        }

        /// <summary>
        /// 根据属性条件获取对象
        /// 满足筛选条件的所有元素
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static ICollection<T> GetAll<T>(this ICollection<T> source, object filter, string propertyName = null) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.GetAll(filter, propertyName);
        }

        #endregion

        #region Get from IEnumerable

        /// <summary>
        /// 根据属性条件获取对象
        /// 满足筛选条件的第一个元素
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static T Get<T>(this IEnumerable<T> source, object filter, string propertyName = null) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.Get(filter, propertyName);
        }

        /// <summary>
        /// 根据属性条件获取对象
        /// 满足筛选条件的所有元素
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static IEnumerable<T> GetAll<T>(this IEnumerable<T> source, object filter, string propertyName = null) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.GetAll(filter, propertyName);
        }

        #endregion

        #endregion

        #region RemoveValue By Property 根据属性条件移除对象

        #region RemoveValue from ICollection

        /// <summary>
        /// 根据属性条件移除对象
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="onlyFirstMatch">只移除第一个匹配项</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static void RemoveValue<T>(this ICollection<T> source, object filter, string propertyName = null, bool onlyFirstMatch = true) where T : class
        {
            if (source.IsReadOnly)
            {
                throw new Exception("该集合已设置为只读，不能删除元素");
            }

            if (onlyFirstMatch)
            {
                var v = source.Get(filter, propertyName);
                source.Remove(v);
            }
            else
            {
                var r = source.GetAll(filter, propertyName);
                foreach (var item in r)
                {
                    source.Remove(item);
                }
            }
        }

        #endregion

        #endregion

        #region ContentsByProperty 判断对象是否被包含

        #region check ContainsByProperty from ICollection

        /// <summary>
        /// 判断对象是否被包含
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static bool ExistBContainsByPropertyyProperty<T>(this ICollection<T> source, object filter) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.ContainsByProperty(filter);
        }

        #endregion

        #region check ExistByProperty from IEnumerable

        /// <summary>
        /// 判断对象是否被包含
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static bool ContainsByProperty<T>(this IEnumerable<T> source, object filter) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.ExistByPropertyValue(filter);
        }

        #endregion

        #endregion

        #region ExistByProperty 判断对象是否存在

        #region check ExistByProperty from ICollection

        /// <summary>
        /// 判断对象是否存在
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static bool ExistByPropertyValue<T>(this ICollection<T> source, object filter, string propertyName) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.ExistByPropertyValue(filter, propertyName);
        }

        #endregion

        #region check ExistByProperty from IEnumerable

        /// <summary>
        /// 判断对象是否存在
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="filter">筛选对象</param>
        /// <param name="propertyName">属性名</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static bool ExistByPropertyValue<T>(this IEnumerable<T> source, object filter, string propertyName) where T : class
        {
            var sourcearray = source.ToArray();
            return sourcearray.ExistByPropertyValue(filter, propertyName);
        }

        #endregion

        #endregion

        #region ExistByProperty 判断对象是否存在

        #region check ExistByProperty from ICollection

        /// <summary>
        /// 判断对象是否存在
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="rule">排序规则</param>
        /// <param name="sortcount">针对前几位元素进行排序，默认全部元素</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static ICollection<T> SortByPropertyValue<T>(this ICollection<T> source, ConstantCollection.SortRule rule = ConstantCollection.SortRule.Asc, string propertyName = null, int? sortcount = null)
        {
            var sourcearray = source.ToArray();
            return sourcearray.SortByPropertyValue(rule, propertyName, sortcount);
        }

        #endregion

        #region check ExistByProperty from IEnumerable

        /// <summary>
        /// 判断对象是否存在
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名</param>
        /// <param name="rule">排序规则</param>
        /// <param name="sortcount">针对前几位元素进行排序，默认全部元素</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static IEnumerable<T> SortByPropertyValue<T>(this IEnumerable<T> source, ConstantCollection.SortRule rule = ConstantCollection.SortRule.Asc, string propertyName = null, int? sortcount = null)
        {
            var sourcearray = source.ToArray();
            return sourcearray.SortByPropertyValue(rule, propertyName, sortcount);
        }

        #endregion

        #endregion

        #region Join

        //#region Join String

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new List&lt;string&gt;{"1","2","3"};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this ICollection<string> source, string connector, string template = "", bool distinct = true)
        //{
        //    var sourceobject = source.ToArray();
        //    return sourceobject.Join(connector, template, distinct);
        //}

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new List&lt;string&gt;{"1","2","3"};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this IList<string> source, string connector, string template = "", bool distinct = true)
        //{
        //    var sourceobject = source.ToArray();
        //    return sourceobject.Join(connector, template, distinct);
        //}

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new List&lt;string&gt;{"1","2","3"};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this IEnumerable<string> source, string connector, string template = "", bool distinct = true)
        //{
        //    var sourceobject = source.ToArray();
        //    return sourceobject.Join(connector, template, distinct);
        //}

        //#endregion

        //#region Join Int

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new List&lt;int&gt;{1,2,3};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this ICollection<int> source, string connector, string template = "", bool distinct = true)
        //{
        //    var sourceobject = source.ToArray();
        //    return sourceobject.Join(connector, template, distinct);
        //}

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new List&lt;int&gt;{1,2,3};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this IList<int> source, string connector, string template = "", bool distinct = true)
        //{
        //    var sourceobject = source.ToArray();
        //    return sourceobject.Join(connector, template, distinct);
        //}

        ///// <summary>
        ///// Join
        ///// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔
        ///// </summary>
        ///// <param name="source">源</param>
        ///// <param name="connector">连接符</param>
        ///// <param name="template">格式化字符串</param>
        ///// <param name="distinct">去除重复，默认去重</param>
        ///// <example>
        ///// var list=new List&lt;int&gt;{1,2,3};
        ///// var result=list.Join("_","value:{0}");
        ///// result的结果为"value:1_value:2_value:3"
        ///// </example>
        ///// <returns></returns>
        //public static string Join(this IEnumerable<int> source, string connector, string template = "", bool distinct = true)
        //{
        //    var sourceobject = source.ToArray();
        //    return sourceobject.Join(connector, template, distinct);
        //}

        //#endregion

        #region Join T

        /// <summary>
        /// Join
        /// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔,
        /// 如果属性值不是Int,或者string,将会被忽略
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名称(非基础类型必填)</param>
        /// <param name="connector">连接符</param>
        /// <param name="template">格式化字符串</param>
        /// <param name="distinct">去除重复，默认去重</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static string Join<T>(this ICollection<T> source, string connector, string template = "", bool distinct = true, string propertyName = null)
        {
            var enumerable = source as T[] ?? source.ToArray();
            return enumerable.Join(connector, template, distinct, propertyName);
        }

        /// <summary>
        /// Join
        /// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔,
        /// 如果属性值不是Int,或者string,将会被忽略
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名称(非基础类型必填)</param>
        /// <param name="connector">连接符</param>
        /// <param name="template">格式化字符串</param>
        /// <param name="distinct">去除重复，默认去重</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static string Join<T>(this IList<T> source, string connector, string template = "", bool distinct = true, string propertyName = null)
        {
            var enumerable = source as T[] ?? source.ToArray();
            return enumerable.Join(connector, template, distinct, propertyName);
        }

        /// <summary>
        /// Join
        /// 用于把数组中的所有元素放入一个字符串,元素是通过指定的分隔符进行分隔,
        /// 如果属性值不是Int,或者string,将会被忽略
        /// </summary>
        /// <param name="source">源</param>
        /// <param name="propertyName">属性名称(非基础类型必填)</param>
        /// <param name="connector">连接符</param>
        /// <param name="template">格式化字符串</param>
        /// <param name="distinct">去除重复，默认去重</param>
        /// <example>
        /// </example>
        /// <returns></returns>
        public static string Join<T>(this IEnumerable<T> source, string connector, string template = "", bool distinct = true, string propertyName = null)
        {
            var enumerable = source as T[] ?? source.ToArray();
            return enumerable.Join(connector, template, distinct, propertyName);
        }

        #endregion

        #endregion

        #region To

        ///// <summary>
        ///// 集合转换为数组
        ///// </summary>
        ///// <typeparam name="T">要转换的类型</typeparam>
        ///// <param name="list">要转换的集合</param>
        ///// <returns></returns>
        //public static T[] ToArray<T>(this ICollection<T> list)
        //{
        //    var array = new T[]{};
        //    foreach (var item in list)
        //    {
        //        array.Add(item);
        //    }
        //    return array;
        //}

        #endregion

        #region Add

        /// <summary>
        /// Adds a list of items to the collection
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection</typeparam>
        /// <param name="collection">Collection</param>
        /// <param name="items">Items to add</param>
        /// <returns>The collection with the added items</returns>
        public static ICollection<T> Add<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            if (items == null)
                return collection;
            items.ForEach(collection.Add);
            return collection;
        }

        /// <summary>
        /// Adds a list of items to the collection
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection</typeparam>
        /// <param name="collection">Collection</param>
        /// <param name="items">Items to add</param>
        /// <returns>The collection with the added items</returns>
        public static ICollection<T> Add<T>(this ICollection<T> collection, params T[] items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            if (items == null)
                return collection;
            items.ForEach(collection.Add);
            return collection;
        }

        #endregion

        #region AddAndReturn

        /// <summary>
        /// Adds an item to a list and returns the item
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        /// <param name="collection">Collection to add to</param>
        /// <param name="item">Item to add to the collection</param>
        /// <returns>The original item</returns>
        public static T AddAndReturn<T>(this ICollection<T> collection, T item)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            if (item.IsNull())
            {
                throw new ArgumentNullException("item");
            }
            collection.Add(item);
            return item;
        }

        #endregion

        #region AddIf

        /// <summary>
        /// Adds items to the collection if it passes the predicate test
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">Collection to add to</param>
        /// <param name="items">Items to add to the collection</param>
        /// <param name="predicate">Predicate that an item needs to satisfy in order to be added</param>
        /// <returns>True if any are added, false otherwise</returns>
        public static bool AddIf<T>(this ICollection<T> collection, Predicate<T> predicate, params T[] items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            if (predicate.IsNull())
            {
                throw new ArgumentNullException("predicate");
            }
            bool returnValue = false;
            foreach (T item in items)
            {
                if (predicate(item))
                {
                    collection.Add(item);
                    returnValue = true;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Adds an item to the collection if it isn't already in the collection
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">Collection to add to</param>
        /// <param name="items">Items to add to the collection</param>
        /// <param name="predicate">Predicate that an item needs to satisfy in order to be added</param>
        /// <returns>True if it is added, false otherwise</returns>
        public static bool AddIf<T>(this ICollection<T> collection, Predicate<T> predicate, IEnumerable<T> items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            if (predicate.IsNull())
            {
                throw new ArgumentNullException("predicate");
            }
            return collection.AddIf(predicate, items.ToArray());
        }

        #endregion

        #region AddIfUnique

        /// <summary>
        /// Adds an item to the collection if it isn't already in the collection
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">Collection to add to</param>
        /// <param name="items">Items to add to the collection</param>
        /// <returns>True if it is added, false otherwise</returns>
        public static bool AddIfUnique<T>(this ICollection<T> collection, params T[] items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            return collection.AddIf(x => !collection.Contains(x), items);
        }

        /// <summary>
        /// Adds an item to the collection if it isn't already in the collection
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">Collection to add to</param>
        /// <param name="items">Items to add to the collection</param>
        /// <returns>True if it is added, false otherwise</returns>
        public static bool AddIfUnique<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            return collection.AddIf(x => !collection.Contains(x), items);
        }

        #endregion

        #region Copy

        ///// <summary>
        ///// 获取集合副本
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="source"></param>
        //public static ICollection<T> GetClone<T>(this ICollection<T> source)
        //{
        //    var result = new Collection<T>();
        //    foreach (var val in source)
        //    {
        //        result.Add(val);
        //    }
        //    return result;
        //}

        /// <summary>
        /// 赋值集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void CopyTo<T>(this ICollection<T> source, ICollection<T> target)
        {
            if (target == null)
            {
                target = new Collection<T>();
            }

            target.Clear();

            foreach (var val in source)
            {
                target.Add(val);
            }
        }

        #endregion

        #region Remove

        /// <summary>
        /// Removes all items that fit the predicate passed in
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection</typeparam>
        /// <param name="collection">Collection to remove items from</param>
        /// <param name="predicate">Predicate used to determine what items to remove</param>
        public static ICollection<T> Remove<T>(this ICollection<T> collection, Func<T, bool> predicate)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            return collection.Where(x => !predicate(x)).ToList();
        }

        /// <summary>
        /// Removes all items in the list from the collection
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection</typeparam>
        /// <param name="collection">Collection</param>
        /// <param name="items">Items to remove</param>
        /// <returns>The collection with the items removed</returns>
        public static ICollection<T> Remove<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (collection.IsNull())
            {
                throw new ArgumentNullException("collection");
            }
            if (items == null)
                return collection;
            return collection.Where(x => !items.Contains(x)).ToList();
        }

        #endregion

        #endregion
    }
}