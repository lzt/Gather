﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.ExtensionMethods
{
    /// <summary>
    /// 转换扩展
    /// </summary>
    public static class ConvertExtensions
    {
        #region ToInt()

        /// <summary>
        /// 转换long为int
        /// </summary>
        /// <param name="source">要转换的变量</param>
        /// <returns></returns>
        public static int ToInt(this long source)
        {
            return (int)source;
        }

        /// <summary>
        /// 转换string为int
        /// </summary>
        /// <param name="source">要转换的变量</param>
        /// <returns></returns>
        public static int ToInt(this string source)
        {
            if (source.IsInt())
            {
                return Convert.ToInt32(source);
            }
            throw new Exception("该字符串不能转换为Int类型");
        }

        #endregion


        #region ToDateTime()

        /// <summary>
        /// 转换string为int
        /// </summary>
        /// <param name="source">要转换的变量</param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string source)
        {
            if (source.IsDateTime())
            {
                return Convert.ToDateTime(source);
            }
            throw new Exception("该字符串不能转换为DateTime类型");
        }

        #endregion
    }
}
