﻿using System;
using System.ComponentModel;

namespace Operate
{
    /// <summary>
    /// 常量集合
    /// </summary>
    public class ConstantCollection
    {
        /// <summary>
        /// 一般用户类型
        /// </summary>
        public enum UserType
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 客户
            /// </summary>
            [Description("客户")]
            Customer = 2,

            /// <summary>
            /// 商户
            /// </summary>
            [Description("商户")]
            Supplier = 3
        }

        /// <summary>
        /// 用户等级
        /// </summary>
        public enum  UserRank
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 一级
            /// </summary>
            [Description("一级")]
            One=1,

            /// <summary>
            /// 二级
            /// </summary>
            [Description("二级")]
            Two = 2,

            /// <summary>
            /// 三级
            /// </summary>
            [Description("三级")]
            Three = 3,

            /// <summary>
            /// 四级
            /// </summary>
            [Description("四级")]
            Four = 4,

            /// <summary>
            /// 五级
            /// </summary>
            [Description("五级")]
            Five = 5,

            /// <summary>
            /// 六级
            /// </summary>
            [Description("六级")]
            Six = 6,

            /// <summary>
            /// 七级
            /// </summary>
            [Description("七级")]
            Seven = 7,

            /// <summary>
            /// 八级
            /// </summary>
            [Description("八级")]
            Eight = 8,

            /// <summary>
            /// 九级
            /// </summary>
            [Description("九级")]
            Nine = 9,
        }

        /// <summary>
        /// 逻辑状态
        /// </summary>
        public enum StatusYesNo
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 否 
            /// </summary>
            [Description("否")]
            No = 0,

            /// <summary>
            /// 是
            /// </summary>
            [Description("是")]
            Yes = 1
        }

        /// <summary>
        /// 审核状态
        /// </summary>
        public enum StatusVerification
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 未审核
            /// </summary>
            [Description("未审核")]
            Unaudited = 0,

            /// <summary>
            /// 已审核
            /// </summary>
            [Description("已审核")]
            Audited = 1,

            /// <summary>
            /// 审核进行中
            /// </summary>
            [Description("审核进行中")]
            Auditing = 2,

            /// <summary>
            /// 拒绝审核
            /// </summary>
            [Description("拒绝审核")]
            DenyAudit = 3,
        }

        /// <summary>
        /// 可用状态
        /// </summary>
        public enum StatusEnabled
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 不可用
            /// </summary>
            [Description("不可用")]
            No = 0,

            /// <summary>
            /// 可用
            /// </summary>
            [Description("可用")]
            Yes = 1,
        }

        /// <summary>
        /// 变化状态
        /// </summary>
        public enum StatusChanged
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 未变更
            /// </summary>
            [Description("未变更")]
            No = 0,

            /// <summary>
            /// 已变更
            /// </summary>
            [Description("已变更")]
            Yes = 1,
        }

        /// <summary>
        /// 资金流向
        /// </summary>
        public enum StatusFundFlow
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 收入
            /// </summary>
            [Description("收入")]
            Income = 1,

            /// <summary>
            /// 支出
            /// </summary>
            [Description("支出")]
            Expenditure = 2,

            /// <summary>
            /// 充值
            /// </summary>
            [Description("充值")]
            Recharge = 3,
        }

        /// <summary>
        /// 阅读状态
        /// </summary>
        public enum StatusRead
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 未读
            /// </summary>
            [Description("未读")]
            No = 0,

            /// <summary>
            /// 已读
            /// </summary>
            [Description("已读")]
            Yes = 1,
        }

        /// <summary>
        /// 删除状态
        /// </summary>
        public enum StatusDelete
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 未读
            /// </summary>
            [Description("未删除")]
            No = 0,

            /// <summary>
            /// 已读
            /// </summary>
            [Description("已删除")]
            Yes = 1,
        }

        /// <summary>
        /// 性别状态
        /// </summary>
        public enum StatusSex
        {
            /// <summary>
            /// 秘密
            /// </summary>
            [Description("秘密")]
            Secret = 0,

            /// <summary>
            /// 未读
            /// </summary>
            [Description("男")]
            Male = 1,

            /// <summary>
            /// 已读
            /// </summary>
            [Description("女")]
            Female = 2,

            /// <summary>
            /// 中性
            /// </summary>
            [Description("中性")]
            Neutral = 3
        }

        /// <summary>
        /// 地区类型
        /// </summary>
        public enum AreaType
        {
            /// <summary>
            /// 不限制
            /// </summary>
            [Description("不限制")]
            Unlimited = -1,

            /// <summary>
            /// 省
            /// </summary>
            [Description("省(直辖市，特别行政区)")]
            Province = 1,

            /// <summary>
            /// 市
            /// </summary>
            [Description("市(直辖市直属区域,特别行政区直属区域)")]
            City = 2,

            /// <summary>
            /// 区县
            /// </summary>
            [Description("区县")]
            Prefecture = 3,
        }

        /// <summary>
        /// 字符剪切方向
        /// </summary>
        public enum StringCutDirection
        {
            /// <summary>
            /// 从左边开始计数
            /// </summary>
            [Description("从左边开始计数")]
            FromLeft = 1,

            /// <summary>
            /// 从左边开始计数
            /// </summary>
            [Description("从右边开始计数")]
            FromRight = 2,
        }

        /// <summary>
        /// 排序方式
        /// </summary>
        public enum SortRule
        {
            /// <summary>
            /// 升序排列
            /// </summary>
            [Description("升序排列")]
            Asc = 1,

            /// <summary>
            /// 降序排列
            /// </summary>
            [Description("降序排列")]
            Desc = 2,
        }

        /// <summary>
        /// .net 默认日期： 0001/1/1 0:00:00
        /// </summary>
        [Description(" .net 默认日期： 0001/1/1 0:00:00")]
        public static DateTime NetDefaultDateTime = Convert.ToDateTime("0001/1/1 0:00:00");

        /// <summary>
        /// Db默认日期： 1970/1/1 0:00:00
        /// </summary>
        [Description("Db默认日期： 1970/1/1 0:00:00")]
        public static DateTime DbDefaultDateTime = Convert.ToDateTime("1970/1/1 0:00:00");

        /// <summary>
        /// 最大日期：9999/12/31 23:59:59
        /// </summary>
        [Description("最大日期：9999/12/31 23:59:59")]
        public static DateTime MaxDateTime = Convert.ToDateTime("9999/12/31 23:59:59");
    }
}
