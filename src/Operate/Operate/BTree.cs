﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using Operate.ExtensionMethods;

#endregion

namespace Operate
{
    /// <summary>
    /// Binary tree
    /// </summary>
    /// <typeparam name="T">The type held by the nodes</typeparam>
    public sealed class BinaryTree<T> : ICollection<T>
        where T : IComparable<T>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        // ReSharper disable RedundantArgumentDefaultValue
        public BinaryTree()
            : this(null)
        // ReSharper restore RedundantArgumentDefaultValue
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="root">Root of the binary tree</param>
        public BinaryTree(TreeNode<T> root = null)
        {
            if (root == null)
            {
                NumberOfNodes = 0;
                return;
            }
            Root = root;
            NumberOfNodes = Traversal(root).Count();
        }

        #endregion

        #region Properties
        /// <summary>
        /// The root value
        /// </summary>
        public TreeNode<T> Root { get; set; }

        /// <summary>
        /// The number of nodes in the tree
        /// </summary>
        private int NumberOfNodes { get; set; }

        /// <summary>
        /// Is the tree empty
        /// </summary>
        public bool IsEmpty { get { return Root == null; } }

        /// <summary>
        /// Gets the minimum value of the tree
        /// </summary>
        public T MinValue
        {
            get
            {
                if (IsEmpty)
                {
                    throw new InvalidOperationException("The tree is empty");
                }
                TreeNode<T> tempNode = Root;
                while (tempNode.Left != null)
                    tempNode = tempNode.Left;
                return tempNode.Value;
            }
        }

        /// <summary>
        /// Gets the maximum value of the tree
        /// </summary>
        public T MaxValue
        {
            get
            {
                if (IsEmpty)
                {
                    throw new InvalidOperationException("The tree is empty");
                }
                TreeNode<T> tempNode = Root;
                while (tempNode.Right != null)
                    tempNode = tempNode.Right;
                return tempNode.Value;
            }
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<T> GetEnumerator()
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (TreeNode<T> tempNode in Traversal(Root))
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                yield return tempNode.Value;
            }
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (TreeNode<T> tempNode in Traversal(Root))
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                yield return tempNode.Value;
            }
        }

        #endregion

        #region ICollection<T> Members

        /// <summary>
        /// Adds an item to a binary tree
        /// </summary>
        /// <param name="item">Item to add</param>
        public void Add(T item)
        {
            if (Root == null)
            {
                Root = new TreeNode<T>(item);
                ++NumberOfNodes;
            }
            else
            {
                Insert(item);
            }
        }

        /// <summary>
        /// Clears all items from the tree
        /// </summary>
        public void Clear()
        {
            Root = null;
            NumberOfNodes = 0;
        }

        /// <summary>
        /// Determines if the tree contains an item
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>True if it is, false otherwise</returns>
        public bool Contains(T item)
        {
            if (IsEmpty)
                return false;

            TreeNode<T> tempNode = Root;
            while (tempNode != null)
            {
                int comparedValue = tempNode.Value.CompareTo(item);
                if (comparedValue == 0)
                    return true;
                tempNode = comparedValue < 0 ? tempNode.Left : tempNode.Right;
            }
            return false;
        }

        /// <summary>
        /// Copies the tree to an array
        /// </summary>
        /// <param name="array">Array to copy to</param>
        /// <param name="arrayIndex">Index to start at</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            var tempArray = new T[NumberOfNodes];
            int counter = 0;
            foreach (T value in this)
            {
                tempArray[counter] = value;
                ++counter;
            }
            Array.Copy(tempArray, 0, array, arrayIndex, NumberOfNodes);
        }

        /// <summary>
        /// Number of items in the tree
        /// </summary>
        public int Count
        {
            get { return NumberOfNodes; }
        }

        /// <summary>
        /// Is this read only?
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes an item from the tree
        /// </summary>
        /// <param name="item">Item to remove</param>
        /// <returns>True if it is removed, false otherwise</returns>
        public bool Remove(T item)
        {
            // ReSharper disable InconsistentNaming
            TreeNode<T> Item = Find(item);
            // ReSharper restore InconsistentNaming
            if (Item == null)
                return false;
            --NumberOfNodes;
            var values = new List<T>();
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (TreeNode<T> tempNode in Traversal(Item.Left))
                // ReSharper restore LoopCanBeConvertedToQuery
                values.Add(tempNode.Value);
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (TreeNode<T> tempNode in Traversal(Item.Right))
                // ReSharper restore LoopCanBeConvertedToQuery
                values.Add(tempNode.Value);
            if (Item.Parent != null)
            {
                if (Item.Parent.Left == Item)
                    Item.Parent.Left = null;
                else
                    Item.Parent.Right = null;
                Item.Parent = null;
            }
            else
            {
                Root = null;
            }
            foreach (T value in values)
                Add(value);
            return true;
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Finds a specific object
        /// </summary>
        /// <param name="item">The item to find</param>
        /// <returns>The node if it is found</returns>
        private TreeNode<T> Find(T item)
        {
            // ReSharper disable InconsistentNaming
            return Traversal(Root).FirstOrDefault(Item => Item.Value.Equals(item));
        }

        /// <summary>
        /// Traverses the list
        /// </summary>
        /// <param name="node">The node to start the search from</param>
        /// <returns>The individual items from the tree</returns>
        private IEnumerable<TreeNode<T>> Traversal(TreeNode<T> node)
        {
            if (node != null)
            {
                if (node.Left != null)
                {
                    foreach (TreeNode<T> leftNode in Traversal(node.Left))
                        yield return leftNode;
                }
                yield return node;
                if (node.Right != null)
                {
                    foreach (TreeNode<T> rightNode in Traversal(node.Right))
                        yield return rightNode;
                }
            }
        }

        /// <summary>
        /// Inserts a value
        /// </summary>
        /// <param name="item">item to insert</param>
        private void Insert(T item)
        {
            TreeNode<T> tempNode = Root;
            while (true)
            {
                int comparedValue = tempNode.Value.CompareTo(item);
                if (comparedValue > 0)
                {
                    if (tempNode.Left == null)
                    {
                        tempNode.Left = new TreeNode<T>(item, tempNode);
                        ++NumberOfNodes;
                        return;
                    }
                    tempNode = tempNode.Left;
                }
                else if (comparedValue < 0)
                {
                    if (tempNode.Right == null)
                    {
                        tempNode.Right = new TreeNode<T>(item, tempNode);
                        ++NumberOfNodes;
                        return;
                    }
                    tempNode = tempNode.Right;
                }
                else
                {
                    tempNode = tempNode.Right;
                }
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Outputs the tree as a string
        /// </summary>
        /// <returns>The string representation of the tree</returns>
        public override string ToString()
        {
            return this.ToString(x => x.ToString(), " ");
        }

        /// <summary>
        /// Converts the object to a string
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <returns>The value as a string</returns>
        public static implicit operator string(BinaryTree<T> value)
        {
            return value.ToString();
        }

        #endregion
    }

    /// <summary>
    /// Node class for the Binary tree
    /// </summary>
    /// <typeparam name="T">The value type</typeparam>
    public sealed class TreeNode<T>
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">Value of the node</param>
        /// <param name="parent">Parent node</param>
        /// <param name="left">Left node</param>
        /// <param name="right">Right node</param>
        public TreeNode(T value = default(T), TreeNode<T> parent = null, TreeNode<T> left = null, TreeNode<T> right = null)
        {
            Value = value;
            Right = right;
            Left = left;
            Parent = parent;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Value of the node
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// Parent node
        /// </summary>
        public TreeNode<T> Parent { get; set; }

        /// <summary>
        /// Left node
        /// </summary>
        public TreeNode<T> Left { get; set; }

        /// <summary>
        /// Right node
        /// </summary>
        public TreeNode<T> Right { get; set; }

        /// <summary>
        /// Is this the root
        /// </summary>
        public bool IsRoot { get { return Parent == null; } }

        /// <summary>
        /// Is this a leaf
        /// </summary>
        public bool IsLeaf { get { return Left == null && Right == null; } }

        /// <summary>
        /// Visited?
        /// </summary>
        internal bool Visited { get; set; }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Returns the node as a string
        /// </summary>
        /// <returns>String representation of the node</returns>
        public override string ToString()
        {
            return Value.ToString();
        }

        #endregion
    }
}