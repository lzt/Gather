﻿using Newtonsoft.Json;
using Operate.ExtensionMethods;
using System;

namespace Operate.JsonConverters
{
    /// <summary>
    /// </summary>
    public class LongConverter : JsonConverter
    {
        /// <summary>
        /// WriteJson
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var typeCode = Type.GetTypeCode(value.GetType());

            if (typeCode == TypeCode.Int64)
            {
                var v = Convert.ToInt64(value);

                if (v > Int32.MaxValue || v < Int32.MinValue)
                {
                    writer.WriteValue(value.ToString());

                    return;
                }
            }
            else if (typeCode == TypeCode.UInt64)
            {
                var v = Convert.ToUInt64(value);

                if (v > UInt32.MaxValue)
                {
                    writer.WriteValue(value.ToString());

                    return;
                }
            }

            writer.WriteValue(value);
        }

        /// <summary>
        /// ReadJson
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            long v = (reader.Value as string).ToInt64();

            return v;
        }

        /// <summary>
        /// CanConvert
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            // 只处理long和ulong两种类型的数据
            switch (objectType.FullName)
            {
                case "System.Int64":
                    return true;

                case "System.UInt64":
                    return true;

                default:
                    return false;
            }
        }
    }
}