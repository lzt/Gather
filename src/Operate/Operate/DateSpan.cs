﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.ExtensionMethods;

#endregion

namespace Operate
{
    /// <summary>
    /// Represents a date span
    /// </summary>
    public class DateSpan
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="start">Start of the date span</param>
        /// <param name="end">End of the date span</param>
        public DateSpan(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new InvalidOperationException("Start is after End");
            }
            Start = start;
            End = end;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Start date
        /// </summary>
        public DateTime Start { get; protected set; }

        /// <summary>
        /// End date
        /// </summary>
        public DateTime End { get; protected set; }

        /// <summary>
        /// Years between the two dates
        /// </summary>
        public int Years { get { return (End - Start).Years(); } }

        /// <summary>
        /// Months between the two dates
        /// </summary>
        public int Months { get { return (End - Start).Months(); } }

        /// <summary>
        /// Days between the two dates
        /// </summary>
        public int Days { get { return (End - Start).DaysRemainder(); } }

        /// <summary>
        /// Hours between the two dates
        /// </summary>
        public int Hours { get { return (End - Start).Hours; } }

        /// <summary>
        /// Minutes between the two dates
        /// </summary>
        public int Minutes { get { return (End - Start).Minutes; } }

        /// <summary>
        /// Seconds between the two dates
        /// </summary>
        public int Seconds { get { return (End - Start).Seconds; } }

        /// <summary>
        /// Milliseconds between the two dates
        /// </summary>
        public int MilliSeconds { get { return (End - Start).Milliseconds; } }

        #endregion

        #region Functions

        /// <summary>
        /// Returns the intersecting time span between the two values
        /// </summary>
        /// <param name="span">Span to use</param>
        /// <returns>The intersection of the two time spans</returns>
        public DateSpan Intersection(DateSpan span)
        {
            if (span==null)
                return null;
            if (!Overlap(span))
                return null;
            DateTime start = span.Start > Start ? span.Start : Start;
            DateTime end = span.End < End ? span.End : End;
            return new DateSpan(start, end);
        }

        /// <summary>
        /// Determines if two DateSpans overlap
        /// </summary>
        /// <param name="span">The span to compare to</param>
        /// <returns>True if they overlap, false otherwise</returns>
        public bool Overlap(DateSpan span)
        {
            return ((Start >= span.Start && Start < span.End) || (End <= span.End && End > span.Start) || (Start <= span.Start && End >= span.End));
        }

        #endregion

        #region Operators

        /// <summary>
        /// Addition operator
        /// </summary>
        /// <param name="span1">Span 1</param>
        /// <param name="span2">Span 2</param>
        /// <returns>The combined date span</returns>
        public static DateSpan operator +(DateSpan span1, DateSpan span2)
        {
            if (span1==null && span2==null)
                return null;
            if (span1==null)
                return new DateSpan(span2.Start, span2.End);
            if (span2==null)
                return new DateSpan(span1.Start, span1.End);
            DateTime start = span1.Start < span2.Start ? span1.Start : span2.Start;
            DateTime end = span1.End > span2.End ? span1.End : span2.End;
            return new DateSpan(start, end);
        }

        /// <summary>
        /// Determines if two DateSpans are equal
        /// </summary>
        /// <param name="span1">Span 1</param>
        /// <param name="span2">Span 2</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator ==(DateSpan span1, DateSpan span2)
        {
            if (span1 == null && span2 == null)
                return true;
            if (span1 == null || span2 == null)
                return false;
            return span1.Start == span2.Start && span1.End == span2.End;
        }

        /// <summary>
        /// Determines if two DateSpans are not equal
        /// </summary>
        /// <param name="span1">Span 1</param>
        /// <param name="span2">Span 2</param>
        /// <returns>True if they are not equal, false otherwise</returns>
        public static bool operator !=(DateSpan span1, DateSpan span2)
        {
            return !(span1 == span2);
        }

        /// <summary>
        /// Converts the object to a string
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <returns>The value as a string</returns>
        public static implicit operator string(DateSpan value)
        {
            return value.ToString();
        }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the DateSpan to a string
        /// </summary>
        /// <returns>The DateSpan as a string</returns>
        public override string ToString()
        {
            return "Start: " + Start + " End: " + End;
        }

        /// <summary>
        /// Determines if two objects are equal
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>True if they are, false otherwise</returns>
        public override bool Equals(object obj)
        {
            var tempobj = obj as DateSpan;
            return tempobj != null && tempobj == this;
        }

        /// <summary>
        /// Gets the hash code for the date span
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return End.GetHashCode() & Start.GetHashCode();
        }

        #endregion
    }
}