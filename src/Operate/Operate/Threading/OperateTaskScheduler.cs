﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Operate.Threading
{
    /// <summary>
    /// 提醒任务调度模块
    /// </summary>
    public class OperateTaskScheduler : TaskScheduler, IDisposable
    {
        //调用Task的线程
        readonly Thread[] _threads;

        //Task Collection
        readonly BlockingCollection<Task> _tasks = new BlockingCollection<Task>();


        /// <summary>
        /// 设置schedule并发
        /// </summary>
        /// <param name="concurrencyLevel"></param>
        public OperateTaskScheduler(int concurrencyLevel)
        {

            _threads = new Thread[concurrencyLevel];
            MaximumConcurrencyLevel = concurrencyLevel;


            for (int i = 0; i < concurrencyLevel; i++)
            {
                _threads[i] = new Thread(() =>
                {
                    foreach (Task task in _tasks.GetConsumingEnumerable())
                        TryExecuteTask(task);

                });

                _threads[i].Start();
            }

        }

        /// <summary>
        /// 任务队列
        /// </summary>
        /// <param name="task"></param>
        protected override void QueueTask(Task task)
        {
            _tasks.Add(task);
        }

        /// <summary>
        /// 尝试执行任务内联
        /// </summary>
        /// <param name="task"></param>
        /// <param name="taskWasPreviouslyQueued"></param>
        /// <returns></returns>
        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {

            if (_threads.Contains(Thread.CurrentThread)) return TryExecuteTask(task);

            return false;
        }

        /// <summary>
        /// 最大并发级别
        /// </summary>
        public override int MaximumConcurrencyLevel { get; }

        /// <summary>
        /// 获取计划中的任务
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return _tasks.ToArray();
        }

        /// <summary>
        /// 释放实例
        /// </summary>
        public void Dispose()
        {
            _tasks.CompleteAdding();
            foreach (Thread t in _threads)
            {
                t.Join();
            }

        }
    }
}
