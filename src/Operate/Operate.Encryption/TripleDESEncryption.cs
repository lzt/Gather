﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Operate.Encryption
{
    /// <summary>
    /// TripleDES加密
    /// </summary>
    public class TripleDesEncryption
    {
        #region

        /// <summary>
        /// 定义密钥
        /// </summary>
        private static readonly byte[] Key = { 42, 16, 93, 156, 78, 4, 218, 32, 15, 167, 44, 80, 26, 20, 155, 112, 2, 94, 11, 204, 119, 35, 184, 197 }; 

        /// <summary>
        /// 定义偏移量
        /// </summary>
        private static readonly byte[] Iv = { 55, 103, 246, 79, 36, 99, 167, 3 };  

        #endregion

        #region TripleDES加密

        /// <summary>
        /// TripleDES加密
        /// </summary>
        public static string Encrypt(string source)
        {
            return Encrypt(source, Key, Iv);
        }

        /// <summary>
        /// TripleDES加密
        /// </summary>
        public static string Encrypt(string source, byte[] key, byte[] iv)
        {
            try
            {
                byte[] bytIn = Encoding.Default.GetBytes(source);
                var tripleDes = new TripleDESCryptoServiceProvider { IV = iv, Key = key };
                ICryptoTransform encrypto = tripleDes.CreateEncryptor();
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);
                cs.Write(bytIn, 0, bytIn.Length);
                cs.FlushFinalBlock();
                byte[] bytOut = ms.ToArray();
                return Convert.ToBase64String(bytOut);
            }
            catch (Exception ex)
            {
                throw new Exception("TripleDES加密错误:" + ex.Message);
            }
        }

        #endregion

        #region TripleDES解密

        /// <summary>
        /// TripleDES解密
        /// </summary>
        public static string Decrypt(string source)
        {
            return Decrypt(source, Key, Iv);
        }

        /// <summary>
        /// TripleDES解密
        /// </summary>
        public static string Decrypt(string source, byte[] key, byte[] iv)
        {
            try
            {
                byte[] bytIn = Convert.FromBase64String(source);
                var tripleDes = new TripleDESCryptoServiceProvider { IV = iv, Key = key };
                ICryptoTransform encrypto = tripleDes.CreateDecryptor();
                var ms = new MemoryStream(bytIn, 0, bytIn.Length);
                var cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Read);
                var strd = new StreamReader(cs, Encoding.Default);
                return strd.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception("TripleDES解密错误:" + ex.Message);
            }
        }

        #endregion
    }
}
