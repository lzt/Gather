﻿using Operate.Encryption.BouncyCastle;
using Operate.Encryption.BouncyCastle.Algorithm.Cipher;
using Operate.Encryption.BouncyCastle.Algorithm.Sign;

namespace Operate.Encryption.RSA.BouncyCastle
{
    /// <summary>
    ///  通过BouncyCastle实现Rsa签名
    /// </summary>
    public class RSASignByBC
    {
        /// <summary>
        /// Rsa默认签名算法
        /// </summary>
        public const string RsaDefaultSignAlgorithm = SignAlgorithm.SHA1WITHRSA;

        /// <summary>
        /// Rsa默认加密算法
        /// </summary>
        public static readonly string RsaDefaultCipherAlgorithm = CipherAlgorithmStructure.Combination(CipherAlgorithm.RSA, CipherMode.ECB, CipherPadding.PKCS1PADDING);

        /// <summary>
        /// 签名
        /// SHA1withRSA
        /// </summary>
        /// <param name="text">要签名的文本</param>
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string Sign(string text, string privateKey, bool isPemPath = false)
        {
            return BouncyCastleCipherAssistant.Sign(text, privateKey, RsaDefaultSignAlgorithm, isPemPath);
        }

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="content">需要验证的内容</param>
        /// <param name="signedString">签名结果</param>
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static bool Verify(string content, string signedString, string publicKey, bool isPemPath = false)
        {
            return BouncyCastleCipherAssistant.Verify(content, signedString, publicKey, RsaDefaultSignAlgorithm, isPemPath);
        }
    }
}
