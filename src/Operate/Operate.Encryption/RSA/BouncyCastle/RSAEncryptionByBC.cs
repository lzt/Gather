﻿using Operate.Encryption.BouncyCastle;
using Operate.Encryption.BouncyCastle.Algorithm.Cipher;

namespace Operate.Encryption.RSA.BouncyCastle
{
    /// <summary>
    /// 通过BouncyCastle实现Rsa加密
    /// </summary>
    public class RSAEncryptionByBC
    {
        /// <summary>
        /// Rsa默认加密算法
        /// </summary>
        public static readonly string RsaDefaultCipherAlgorithm = CipherAlgorithmStructure.Combination(CipherAlgorithm.RSA, CipherMode.ECB, CipherPadding.PKCS1PADDING);

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="content">需要加密的文本</param> CipherAlgorithm.RSA
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string EncryptByPublicKey(string content, string publicKey, bool isPemPath = false)
        {
            return BouncyCastleCipherAssistant.EncryptByPublicKey(content, publicKey, RsaDefaultCipherAlgorithm, isPemPath);
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="content">需要加密的文本</param> CipherAlgorithm.RSA
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string EncryptByPrivateKey(string content, string privateKey, bool isPemPath = false)
        {
            return BouncyCastleCipherAssistant.EncryptByPrivateKey(content, privateKey, RsaDefaultCipherAlgorithm, isPemPath);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="content">需要解密的文本</param>
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string DecryptByPublicKey(string content, string publicKey, bool isPemPath = false)
        {
            return BouncyCastleCipherAssistant.DecryptByPublicKey(content, publicKey, RsaDefaultCipherAlgorithm, isPemPath);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="content">需要解密的文本</param>
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string DecryptByPrivateKey(string content, string privateKey, bool isPemPath = false)
        {
            return BouncyCastleCipherAssistant.DecryptByPrivateKey(content, privateKey, RsaDefaultCipherAlgorithm, isPemPath);
        }
    }
}
