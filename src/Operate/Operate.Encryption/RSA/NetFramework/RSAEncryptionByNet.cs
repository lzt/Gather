﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Operate.Encryption.ExtensionMethods;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Encryption.RSA.NetFramework
{
    /// <summary>
    /// Utility class for doing RSA Encryption
    /// </summary>
    public static class RSAEncryptionByNet
    {
        #region key

        private const string PrivateKey = "<RSAKeyValue><Modulus>rhERrmQ3/vLXVy8qAg2C519mheswMSCDWnAnmVSwTQsjRgnUk9tn5yJu6P75Kg9yIFbCJlbjqpGsuqYFtJ4VrAVy/MgFDMNOvnXFftffVn0ynknexKxGlwUn9qWmahct3xEsoAjb3mKUBpUUxnLOuHaZjK72NVF+jEjX/t/zhQ0=</Modulus><Exponent>AQAB</Exponent><P>1eFeXvjb28g+kJfopQHB6aleg1rC+nuOTeMydxez5xqsAO0KSNHGmv7N80yxkecWQ5NS52PEcriSDJPZ1f0FZw==</P><Q>0FiFf7aU8JirhnfasOimQP3a0RgHreGAr0EdTFPB5u5xvEJ6bIBo7L1ahixH3NlPGbIkXTUTqiLGBfJ99WbFaw==</Q><DP>l7eTsvkDNMe6IeWwYQR7Ip5Dbhg/AWIOExAcZ0CIHGLeKpX7WqZ8JMylGXaI67+qGmtyPrOV0e89ovBqcRJX9w==</DP><DQ>IUC/re6aPvxfBAtFIE9BmcXqkszfDOWdAFvILVKA9DbCeGWz3HVySba/KAMRRTJ56YQBQc8i4FjEelaFvBE3GQ==</DQ><InverseQ>pO/9UdJV8tCtjD2KTR+h5VblvAG2Vcza1w4Zh8ghbl1H2+n6W2XjBu7UqFsUiNKcVao/44vgTsSh9Hfh9FLRWg==</InverseQ><D>GX9ml6UWjsIDyUGfZa2U/096NSO+a3PXyeej5VICgUagZCIMgZwiHDlvBbJTzVV14kbTKcqQjuvH4Y9wRoThp5NLSdrkL3R8Yh99f3oJ5t8wjBlnN7ha/RIvdsKs4BvWxnsHGHmkMXjdJivlJqLxdjq/lmN+SajsPzm1vEPGygE=</D></RSAKeyValue>";

        private const string PublicKey = "<RSAKeyValue><Modulus>rhERrmQ3/vLXVy8qAg2C519mheswMSCDWnAnmVSwTQsjRgnUk9tn5yJu6P75Kg9yIFbCJlbjqpGsuqYFtJ4VrAVy/MgFDMNOvnXFftffVn0ynknexKxGlwUn9qWmahct3xEsoAjb3mKUBpUUxnLOuHaZjK72NVF+jEjX/t/zhQ0=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

        #endregion

        #region Public Static Functions

        /// <summary>
        /// Encrypts a string using RSA
        /// </summary>
        /// <param name="input">Input string (should be small as anything over 128 bytes can not be decrypted)</param>
        /// <param name="key">Key to use for encryption</param>
        /// <param name="encodingUsing">Encoding that the input string uses (defaults to UTF8)</param>
        /// <returns>An encrypted string (64bit string)</returns>
        public static string Encrypt(string input, string key = PublicKey, Encoding encodingUsing = null)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(key);
                byte[] encryptedBytes = rsa.Encrypt(input.ToByteArray(encodingUsing), true);
                rsa.Clear();
                return encryptedBytes.ToString(Base64FormattingOptions.None);
            }
        }

        /// <summary>
        /// Decrypts a string using RSA
        /// </summary>
        /// <param name="input">Input string (should be small as anything over 128 bytes can not be decrypted)</param>
        /// <param name="key">Key to use for decryption</param>
        /// <param name="encodingUsing">Encoding that the result should use (defaults to UTF8)</param>
        /// <returns>A decrypted string</returns>
        public static string Decrypt(string input, string key = PrivateKey, Encoding encodingUsing = null)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(key);
                byte[] encryptedBytes = rsa.Decrypt(input.FromBase64(), true);
                rsa.Clear();
                return encryptedBytes.ToString(encodingUsing);
            }
        }

        /// <summary>
        /// Creates a new set of keys
        /// </summary>
        /// <param name="privatePublic">True if private key should be included, false otherwise</param>
        /// <returns>XML representation of the key information</returns>
        public static string CreateKey(bool privatePublic)
        {
            RSACryptoServiceProvider provider;
            using (provider = new RSACryptoServiceProvider())
            {
                return provider.ToXmlString(privatePublic);
            }
        }

        /// <summary>
        /// 从私钥中提取公钥
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="publicKeyBody"></param>
        public static void ExtractKey(string privateKey, out string publicKeyBody)
        {
            const string pattern = "<RSAKeyValue><Modulus>([\\s\\S]*)</Modulus>";
            var r = Regex.Match(privateKey, pattern);
            publicKeyBody = r.Value.Replace("<RSAKeyValue><Modulus>", "").Replace("</Modulus>","");
        }

        /// <summary>
        /// 通过公钥内容构建公钥
        /// </summary>
        /// <param name="publicKeyBody"></param>
        /// <returns></returns>
        public static string BuildPublicKeyByBody(string publicKeyBody)
        {
            return "<RSAKeyValue><Modulus>" + publicKeyBody + "</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        }

        /// <summary>
        /// Takes a string and creates a signed hash of it
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="key">Key to encrypt/sign with</param>
        /// <param name="hash">This will be filled with the unsigned hash</param>
        /// <param name="encodingUsing">Encoding that the input is using (defaults to UTF8)</param>
        /// <returns>A signed hash of the input (64bit string)</returns>
        public static string SignHash(string input, string key, out string hash, Encoding encodingUsing = null)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(key);
                byte[] hashBytes = input.ToByteArray(encodingUsing).Hash();
                byte[] signedHash = rsa.SignHash(hashBytes, CryptoConfig.MapNameToOID("SHA1"));
                rsa.Clear();
                hash = hashBytes.ToString(Base64FormattingOptions.None);
                return signedHash.ToString(Base64FormattingOptions.None);
            }
        }

        /// <summary>
        /// Verifies a signed hash against the unsigned version
        /// </summary>
        /// <param name="hash">The unsigned hash (should be 64bit string)</param>
        /// <param name="signedHash">The signed hash (should be 64bit string)</param>
        /// <param name="key">The key to use in decryption</param>
        /// <returns>True if it is verified, false otherwise</returns>
        public static bool VerifyHash(string hash, string signedHash, string key)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (hash.IsNullOrEmpty()) { throw new ArgumentNullException("hash"); }
            if (signedHash.IsNullOrEmpty()) { throw new ArgumentNullException("signedHash"); }
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(key);
                byte[] inputArray = signedHash.FromBase64();
                byte[] hashArray = hash.FromBase64();
                bool result = rsa.VerifyHash(hashArray, CryptoConfig.MapNameToOID("SHA1"), inputArray);
                rsa.Clear();
                return result;
            }
        }

        #endregion
    }
}