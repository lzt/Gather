﻿namespace Operate.Encryption.ExtensionMethods
{
    /// <summary>
    /// String扩展类
    /// </summary>
    public static class StringExtensions
    {
        #region Base64

        /// <summary>
        /// 检验是否为Base64编码
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool IsBase64(this string source)
        {
            return Base64Encryption.IsBase64(source);
        }

        /// <summary>
        /// Base64编码
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToBase64(this string source)
        {
            return Base64Encryption.Encode(source);
        }

        /// <summary>
        /// Base64编码
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Base64Encode(this string source)
        {
            return Base64Encryption.Encode(source);
        }

        /// <summary>
        /// Base64解码
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Base64Decode(this string source)
        {
            return Base64Encryption.Decode(source);
        }

        #endregion

        #region Md5

        /// <summary>
        /// 转换为md5字符串
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToMd5(this string source)
        {
            return Md5Encryption.ToMd5(source);
        }

        #endregion
    }
}
