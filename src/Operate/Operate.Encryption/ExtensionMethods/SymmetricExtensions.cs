﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Diagnostics.CodeAnalysis;

using System.IO;
using System.Security.Cryptography;
using System.Text;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods;

#endregion

namespace Operate.Encryption.ExtensionMethods
{
    /// <summary>
    /// Symmetric key extensions
    /// </summary>
    public static class SymmetricExtensions
    {
        #region Functions

        #region Encrypt

        /// <summary>
        /// Encrypts a string
        /// </summary>
        /// <param name="data">Text to be encrypted</param>
        /// <param name="key">Password to encrypt with</param>
        /// <param name="algorithmUsing">Algorithm to use for encryption (defaults to AES)</param>
        /// <param name="salt">Salt to encrypt with</param>
        /// <param name="hashAlgorithm">Can be either SHA1 or MD5</param>
        /// <param name="passwordIterations">Number of iterations to do</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <param name="encodingUsing">Encoding that the original string is using (defaults to UTF8)</param>
        /// <returns>An encrypted string (Base 64 string)</returns>
        public static string Encrypt(this string data, string key,
            Encoding encodingUsing = null,
            SymmetricAlgorithm algorithmUsing = null, string salt = "Kosher",
            string hashAlgorithm = "SHA1", int passwordIterations = 2,
            string initialVector = "OFRna73m*aze01xY", int keySize = 256)
        {
            if (string.IsNullOrEmpty(data))
                return "";
            return data.ToByteArray(encodingUsing)
                       .Encrypt(key, algorithmUsing, salt, hashAlgorithm, passwordIterations, initialVector, keySize)
                       .ToString(Base64FormattingOptions.None);
        }

        /// <summary>
        /// Encrypts a byte array
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="key">Key to use to encrypt the data (can use PasswordDeriveBytes, Rfc2898DeriveBytes, etc. Really anything that implements DeriveBytes)</param>
        /// <param name="algorithmUsing">Algorithm to use for encryption (defaults to AES)</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <param name="encodingUsing">Encoding that the original string is using (defaults to UTF8)</param>
        /// <returns>An encrypted byte array</returns>
        public static string Encrypt(this string data,
            DeriveBytes key,
            Encoding encodingUsing = null,
            SymmetricAlgorithm algorithmUsing = null,
            string initialVector = "OFRna73m*aze01xY",
            int keySize = 256)
        {
            if (string.IsNullOrEmpty(data))
                return "";
            return data.ToByteArray(encodingUsing)
                       .Encrypt(key, algorithmUsing, initialVector, keySize)
                       .ToString(Base64FormattingOptions.None);
        }

        /// <summary>
        /// Encrypts a byte array
        /// </summary>
        /// <param name="data">Data to be encrypted</param>
        /// <param name="key">Password to encrypt with</param>
        /// <param name="algorithmUsing">Algorithm to use for encryption (defaults to AES)</param>
        /// <param name="salt">Salt to encrypt with</param>
        /// <param name="hashAlgorithm">Can be either SHA1 or MD5</param>
        /// <param name="passwordIterations">Number of iterations to do</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <returns>An encrypted byte array</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static byte[] Encrypt(this byte[] data, string key,
            SymmetricAlgorithm algorithmUsing = null, string salt = "Kosher",
            string hashAlgorithm = "SHA1", int passwordIterations = 2,
            string initialVector = "OFRna73m*aze01xY", int keySize = 256)
        {
            return data.Encrypt(new PasswordDeriveBytes(key, salt.ToByteArray(), hashAlgorithm, passwordIterations), algorithmUsing, initialVector, keySize);
        }

        /// <summary>
        /// Encrypts a byte array
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="key">Key to use to encrypt the data (can use PasswordDeriveBytes, Rfc2898DeriveBytes, etc. Really anything that implements DeriveBytes)</param>
        /// <param name="algorithmUsing">Algorithm to use for encryption (defaults to AES)</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <returns>An encrypted byte array</returns>
        [SuppressMessage("Microsoft.Usage", "CA2202:DoNotDisposeObjectsMultipleTimes")]
        public static byte[] Encrypt(this byte[] data,
            DeriveBytes key,
            SymmetricAlgorithm algorithmUsing = null,
            string initialVector = "OFRna73m*aze01xY",
            int keySize = 256)
        {
            if (initialVector.IsNullOrEmpty()) { throw new ArgumentNullException("initialVector"); }
            if (data==null)
                return null;
            algorithmUsing = algorithmUsing.Check(()=>new RijndaelManaged());
            using (DeriveBytes derivedPassword = key)
            {
                using (SymmetricAlgorithm symmetricKey = algorithmUsing)
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    byte[] cipherTextBytes;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(derivedPassword.GetBytes(keySize / 8), initialVector.ToByteArray()))
                    {
                        using (var memStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(data, 0, data.Length);
                                cryptoStream.FlushFinalBlock();
                                cipherTextBytes = memStream.ToArray();
                            }
                        }
                    }
                    symmetricKey.Clear();
                    return cipherTextBytes;
                }
            }
        }

        #endregion

        #region Decrypt

        /// <summary>
        /// Decrypts a string
        /// </summary>
        /// <param name="data">Text to be decrypted (Base 64 string)</param>
        /// <param name="key">Key to use to encrypt the data (can use PasswordDeriveBytes, Rfc2898DeriveBytes, etc. Really anything that implements DeriveBytes)</param>
        /// <param name="encodingUsing">Encoding that the output string should use (defaults to UTF8)</param>
        /// <param name="algorithmUsing">Algorithm to use for decryption (defaults to AES)</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <returns>A decrypted string</returns>
        public static string Decrypt(this string data,
            DeriveBytes key,
            Encoding encodingUsing = null,
            SymmetricAlgorithm algorithmUsing = null,
            string initialVector = "OFRna73m*aze01xY",
            int keySize = 256)
        {
            if (string.IsNullOrEmpty(data))
                return "";
            return Convert.FromBase64String(data)
                          .Decrypt(key, algorithmUsing, initialVector, keySize)
                          .ToString(encodingUsing);
        }

        /// <summary>
        /// Decrypts a string
        /// </summary>
        /// <param name="data">Text to be decrypted (Base 64 string)</param>
        /// <param name="key">Password to decrypt with</param>
        /// <param name="encodingUsing">Encoding that the output string should use (defaults to UTF8)</param>
        /// <param name="algorithmUsing">Algorithm to use for decryption (defaults to AES)</param>
        /// <param name="salt">Salt to decrypt with</param>
        /// <param name="hashAlgorithm">Can be either SHA1 or MD5</param>
        /// <param name="passwordIterations">Number of iterations to do</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <returns>A decrypted string</returns>
        public static string Decrypt(this string data, string key,
            Encoding encodingUsing = null,
            SymmetricAlgorithm algorithmUsing = null, string salt = "Kosher",
            string hashAlgorithm = "SHA1", int passwordIterations = 2,
            string initialVector = "OFRna73m*aze01xY", int keySize = 256)
        {
            if (string.IsNullOrEmpty(data))
                return "";
            return Convert.FromBase64String(data)
                          .Decrypt(key, algorithmUsing, salt, hashAlgorithm, passwordIterations, initialVector, keySize)
                          .ToString(encodingUsing);
        }


        /// <summary>
        /// Decrypts a byte array
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="key">Key to use to encrypt the data (can use PasswordDeriveBytes, Rfc2898DeriveBytes, etc. Really anything that implements DeriveBytes)</param>
        /// <param name="algorithmUsing">Algorithm to use for encryption (defaults to AES)</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <returns>An encrypted byte array</returns>
        [SuppressMessage("Microsoft.Usage", "CA2202:DoNotDisposeObjectsMultipleTimes")]
        public static byte[] Decrypt(this byte[] data,
            DeriveBytes key,
            SymmetricAlgorithm algorithmUsing = null,
            string initialVector = "OFRna73m*aze01xY",
            int keySize = 256)
        {
            if (initialVector.IsNullOrEmpty()) { throw new ArgumentNullException("initialVector"); }
            if (data==null)
                return null;
            algorithmUsing = algorithmUsing.Check(()=>new RijndaelManaged());
            using (DeriveBytes derivedPassword = key)
            {
                using (SymmetricAlgorithm symmetricKey = algorithmUsing)
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    byte[] plainTextBytes;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(derivedPassword.GetBytes(keySize / 8), initialVector.ToByteArray()))
                    {
                        using (var memStream = new MemoryStream(data))
                        {
                            using (var cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read))
                            {
                                plainTextBytes = cryptoStream.ReadAllBinary();
                            }
                        }
                    }
                    symmetricKey.Clear();
                    return plainTextBytes;
                }
            }
        }

        /// <summary>
        /// Decrypts a byte array
        /// </summary>
        /// <param name="data">Data to be decrypted</param>
        /// <param name="key">Password to decrypt with</param>
        /// <param name="algorithmUsing">Algorithm to use for decryption</param>
        /// <param name="salt">Salt to decrypt with</param>
        /// <param name="hashAlgorithm">Can be either SHA1 or MD5</param>
        /// <param name="passwordIterations">Number of iterations to do</param>
        /// <param name="initialVector">Needs to be 16 ASCII characters long</param>
        /// <param name="keySize">Can be 64 (DES only), 128 (AES), 192 (AES and Triple DES), or 256 (AES)</param>
        /// <returns>A decrypted byte array</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static byte[] Decrypt(this byte[] data, string key,
            SymmetricAlgorithm algorithmUsing = null, string salt = "Kosher",
            string hashAlgorithm = "SHA1", int passwordIterations = 2,
            string initialVector = "OFRna73m*aze01xY", int keySize = 256)
        {
            return data.Decrypt(new PasswordDeriveBytes(key, salt.ToByteArray(), hashAlgorithm, passwordIterations), algorithmUsing, initialVector, keySize);
        }

        #endregion

        #endregion
    }
}