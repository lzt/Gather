﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Text;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Encryption.ExtensionMethods
{
    /// <summary>
    /// Extensions that deal with bit xoring
    /// </summary>
    public static class ShiftExtensions
    {
        #region Functions

        #region Encrypt

        /// <summary>
        /// Encrypts the data using a basic xor of the key (not very secure unless doing a one time pad)
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="key">Key to use</param>
        /// <param name="oneTimePad">Is this a one time pad?</param>
        /// <returns>The encrypted data</returns>
        public static byte[] Encrypt(this byte[] data, byte[] key, bool oneTimePad)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (data==null)
                return null;
            if (oneTimePad && key.Length < data.Length)
                throw new ArgumentException("Key is not long enough");
            return Process(data, key);
        }

        /// <summary>
        /// Encrypts the data using a basic xor of the key (not very secure unless doing a one time pad)
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="key">Key to use</param>
        /// <param name="oneTimePad">Is this a one time pad?</param>
        /// <param name="encodingUsing">Encoding that the Data uses (defaults to UTF8)</param>
        /// <returns>The encrypted data</returns>
        public static string Encrypt(this string data, string key, bool oneTimePad, Encoding encodingUsing = null)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (data==null)
                return "";
            return data.ToByteArray(encodingUsing).Encrypt(key.ToByteArray(encodingUsing), oneTimePad).ToString(encodingUsing);
        }

        #endregion

        #region Decrypt

        /// <summary>
        /// Decrypts the data using a basic xor of the key (not very secure unless doing a one time pad)
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="key">Key to use</param>
        /// <param name="oneTimePad">Is this a one time pad?</param>
        /// <returns>The decrypted data</returns>
        public static byte[] Decrypt(this byte[] data, byte[] key, bool oneTimePad)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (!(!oneTimePad || key.Length >= data.Length)) { throw new ArgumentException("Key is not long enough"); }
            if (data==null)
                return null;
            return Process(data, key);
        }

        /// <summary>
        /// Decrypts the data using a basic xor of the key (not very secure unless doing a one time pad)
        /// </summary>
        /// <param name="data">Data to decrypt</param>
        /// <param name="key">Key to use</param>
        /// <param name="oneTimePad">Is this a one time pad?</param>
        /// <param name="encodingUsing">Encoding that the Data uses (defaults to UTF8)</param>
        /// <returns>The encrypted data</returns>
        public static string Decrypt(this string data, string key, bool oneTimePad, Encoding encodingUsing = null)
        {
            if (key.IsNull()) { throw new ArgumentNullException("key"); }
            if (data==null)
                return "";
            return data.ToByteArray(encodingUsing).Decrypt(key.ToByteArray(encodingUsing), oneTimePad).ToString(encodingUsing);
        }

        #endregion

        #region Process

        /// <summary>
        /// Actually does the encryption/decryption
        /// </summary>
        private static byte[] Process(byte[] input, byte[] key)
        {
            var outputArray = new byte[input.Length];
            int position = 0;
            for (int x = 0; x < input.Length; ++x)
            {
                outputArray[x] = (byte)(input[x] ^ key[position]);
                ++position;
                if (position >= key.Length)
                    position = 0;
            }
            return outputArray;
        }

        #endregion

        #region XOr

        /// <summary>
        /// XOrs two strings together, returning the result
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="key">Key to use</param>
        /// <param name="encodingUsing">Encoding that the data uses (defaults to UTF8)</param>
        /// <returns>The XOred string</returns>
        public static string XOr(this string input, string key, Encoding encodingUsing = null)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            if (string.IsNullOrEmpty(key))
                return input;
            byte[] inputArray = input.ToByteArray(encodingUsing);
            byte[] keyArray = key.ToByteArray(encodingUsing);
            var outputArray = new byte[inputArray.Length];
            for (int x = 0; x < inputArray.Length; ++x)
            {
                byte keyByte = x < keyArray.Length ? keyArray[x] : (byte)0;
                outputArray[x] = (byte)(inputArray[x] ^ keyByte);
            }
            return outputArray.ToString(encodingUsing);
        }

        #endregion

        #endregion
    }
}