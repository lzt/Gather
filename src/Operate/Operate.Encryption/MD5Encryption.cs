﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Operate.Encryption
{
    /// <summary>
    /// MD5加密
    /// </summary>
    public static class Md5Encryption
    {
        /// <summary>
        /// 模式
        /// </summary>
        public enum Mode
        {
            /// <summary>
            /// 16位
            /// </summary>
            [Description("16位")]
            Short = 16,

            /// <summary>
            /// 32位
            /// </summary>
            [Description("32位")]
            Normal = 32,
        }

        /// <summary>
        /// 转换为md5字符串
        /// </summary>
        /// <param name="source"></param>
        /// <param name="mode">  </param>
        /// <returns></returns>
        public static string ToMd5(string source, Mode mode = Mode.Normal)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(source));

            if (mode == Mode.Short)
            {
                return BitConverter.ToString(s, 4, 8).Replace("-", "");
            }

            return s.Aggregate("", (current, t) => current + t.ToString("x2")).ToLower();
        }
    }
}