﻿using System;
using System.IO;
using System.Text;
using Operate.Encryption.ExtensionMethods;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;

namespace Operate.Encryption.BouncyCastle
{

    /// <summary>
    /// 通过BouncyCastle实现Rsa
    /// </summary>
    public class BouncyCastleCipherAssistant
    {
        /// <summary>
        /// 获取公钥
        /// </summary>
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        /// <exception cref="IOException"></exception>
        public static AsymmetricKeyParameter GetPublicKey(string publicKey, bool isPemPath = false)
        {
            AsymmetricKeyParameter publicKeyParam;
            if (isPemPath)
            {
                TextReader txtreader = new StreamReader(publicKey);
                var r = new PemReader(txtreader);//载入公钥文件
                Object readObject = r.ReadObject();
                //实例化参数实例
                publicKeyParam = (AsymmetricKeyParameter)readObject;
            }
            else
            {
                var buf = new StringBuilder(publicKey);
                if (buf.Length % 4 != 0)
                {
                    throw new IOException("base64 data appears to be truncated");
                }
                var keycontent = Base64.Decode(buf.ToString());
                publicKeyParam = PublicKeyFactory.CreateKey(keycontent);
            }
            return publicKeyParam;
        }

        /// <summary>
        /// 通过私钥内容获取私钥
        /// </summary>
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        /// <exception cref="IOException"></exception>
        public static AsymmetricKeyParameter GetPrivateKey(string privateKey, bool isPemPath = false)
        {
            var keyPair = GetKeyPair(privateKey, isPemPath);
            return keyPair.Private;
        }

        /// <summary>
        /// 通过私钥内容获取公钥私钥对
        /// </summary>
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        /// <exception cref="IOException"></exception>
        public static AsymmetricCipherKeyPair GetKeyPair(string privateKey, bool isPemPath = false)
        {
            AsymmetricCipherKeyPair keyPair;
            if (isPemPath)
            {
                TextReader txtreader = new StreamReader(privateKey);
                var r = new PemReader(txtreader);//载入公钥文件
                Object readObject = r.ReadObject();

                if (readObject.GetType() == typeof(AsymmetricCipherKeyPair))
                {
                    //实例化参数实例
                    keyPair = (AsymmetricCipherKeyPair)readObject;
                    return keyPair;
                }

                if (readObject.GetType() == typeof(RsaPrivateCrtKeyParameters))
                {
                    var privSpec = (RsaPrivateCrtKeyParameters)readObject;
                    AsymmetricKeyParameter pubSpec = new RsaKeyParameters(false, privSpec.Modulus, privSpec.PublicExponent);

                    //实例化参数实例
                    keyPair = new AsymmetricCipherKeyPair(pubSpec, privSpec);
                    return keyPair;
                }

                throw new Exception("不能识别私钥文件");
            }
            else
            {
                var buf = new StringBuilder(privateKey);
                if (buf.Length % 4 != 0)
                {
                    throw new IOException("base64 data appears to be truncated");
                }
                var keycontent = Base64.Decode(buf.ToString());
                var seq = Asn1Sequence.GetInstance(keycontent);

                if (seq.Count != 9)
                    throw new PemException("malformed sequence in RSA private key");

                var rsa = new RsaPrivateKeyStructure(seq);

                AsymmetricKeyParameter pubSpec = new RsaKeyParameters(false, rsa.Modulus, rsa.PublicExponent);
                AsymmetricKeyParameter privSpec = new RsaPrivateCrtKeyParameters(rsa.Modulus, rsa.PublicExponent, rsa.PrivateExponent, rsa.Prime1, rsa.Prime2, rsa.Exponent1, rsa.Exponent2, rsa.Coefficient);

                keyPair = new AsymmetricCipherKeyPair(pubSpec, privSpec);
            }
            return keyPair;
        }

        /// <summary>
        /// 签名
        /// SHA1withRSA
        /// </summary>
        /// <param name="text">要签名的文本</param>
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="algorithm">算法模式</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string Sign(string text, string privateKey, string algorithm, bool isPemPath = false)
        {
            var txtdata = Encoding.GetEncoding("utf-8").GetBytes(text);
            AsymmetricKeyParameter privateKeyParam = GetPrivateKey(privateKey, isPemPath);
            var signature = SignerUtilities.GetSigner(algorithm);
            signature.Init(true, privateKeyParam);
            signature.BlockUpdate(txtdata, 0, txtdata.Length);
            var signdata = signature.GenerateSignature();
            return Convert.ToBase64String(signdata);
        }

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="content">需要验证的内容</param>
        /// <param name="signedString">签名结果</param>
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="algorithm">算法模式</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static bool Verify(string content, string signedString, string publicKey, string algorithm, bool isPemPath = false)
        {
            byte[] contentData = Encoding.UTF8.GetBytes(content);
            byte[] data = Convert.FromBase64String(signedString);
            AsymmetricKeyParameter publicKeyParam = GetPublicKey(publicKey, isPemPath);
            var signature = SignerUtilities.GetSigner(algorithm);
            signature.Init(false, publicKeyParam);
            signature.BlockUpdate(contentData, 0, contentData.Length);
            return signature.VerifySignature(data);
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="content">需要加密的文本</param> CipherAlgorithm.RSA
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="algorithm">算法模式</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string EncryptByPublicKey(string content, string publicKey, string algorithm, bool isPemPath = false)
        {
            byte[] contentData = Encoding.UTF8.GetBytes(content);
            AsymmetricKeyParameter publicKeyParam = GetPublicKey(publicKey, isPemPath);
            IBufferedCipher c = CipherUtilities.GetCipher(algorithm);
            c.Init(true, publicKeyParam);

            //int blockSize = c.GetBlockSize();
            //c.ProcessBytes(input);

            byte[] outBytes = c.DoFinal(contentData);
            return Convert.ToBase64String(outBytes);
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="content">需要加密的文本</param> CipherAlgorithm.RSA
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="algorithm">算法模式</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string EncryptByPrivateKey(string content, string privateKey, string algorithm, bool isPemPath = false)
        {
            byte[] contentData = Encoding.UTF8.GetBytes(content);
            AsymmetricKeyParameter privateKeyParam = GetPrivateKey(privateKey, isPemPath);
            IBufferedCipher c = CipherUtilities.GetCipher(algorithm);
            c.Init(true, privateKeyParam);

            //int blockSize = c.GetBlockSize();
            //c.ProcessBytes(input);

            byte[] outBytes = c.DoFinal(contentData);
            return Convert.ToBase64String(outBytes);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="content">需要解密的文本</param>
        /// <param name="publicKey">公钥（内容或者pem文件路径）</param>
        /// <param name="algorithm">算法模式</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string DecryptByPublicKey(string content, string publicKey, string algorithm, bool isPemPath = false)
        {
            byte[] contentData = Convert.FromBase64String(content);
            AsymmetricKeyParameter publicKeyParam = GetPublicKey(publicKey, isPemPath);
            IBufferedCipher c = CipherUtilities.GetCipher(algorithm);
            c.Init(false, publicKeyParam);

            //int blockSize = c.GetBlockSize();
            //c.ProcessBytes(input);

            byte[] outBytes = c.DoFinal(contentData);
            return Encoding.UTF8.GetString(outBytes);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="content">需要解密的文本</param>
        /// <param name="privateKey">私钥（内容或者pem文件路径）</param>
        /// <param name="algorithm">算法模式</param>
        /// <param name="isPemPath">是不是pem文件</param>
        /// <returns></returns>
        public static string DecryptByPrivateKey(string content, string privateKey, string algorithm, bool isPemPath = false)
        {
            byte[] contentData = Convert.FromBase64String(content);
            AsymmetricKeyParameter privateKeyParam = GetPrivateKey(privateKey, isPemPath);
            IBufferedCipher c = CipherUtilities.GetCipher(algorithm);
            c.Init(false, privateKeyParam);

            //int blockSize = c.GetBlockSize();
            //c.ProcessBytes(input);

            byte[] outBytes = c.DoFinal(contentData);
            return Encoding.UTF8.GetString(outBytes);
        }
    }
}
