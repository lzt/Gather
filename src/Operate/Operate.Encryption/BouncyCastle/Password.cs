﻿using Org.BouncyCastle.OpenSsl;

namespace Operate.Encryption.BouncyCastle
{
    /// <summary>
    /// BouncyCastle 密钥类
    /// </summary>
    public class Password : IPasswordFinder
    {
        private readonly char[] _password;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        public Password(char[] word)
        {
            _password = (char[])word.Clone();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public char[] GetPassword()
        {
            return (char[])_password.Clone();
        }
    }
}
