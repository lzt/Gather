﻿namespace Operate.Encryption.BouncyCastle.Algorithm.Cipher
{
    /// <summary>
    /// 
    /// </summary>
    public enum CipherMode
    {
        ECB,
        NONE,
        CBC,
        CCM,
        CFB,
        CTR,
        CTS,
        EAX,
        GCM,
        GOFB,
        OFB,
        OPENPGPCFB,
        SIC
    };
}
