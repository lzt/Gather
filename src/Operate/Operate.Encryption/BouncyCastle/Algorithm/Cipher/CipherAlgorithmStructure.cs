﻿namespace Operate.Encryption.BouncyCastle.Algorithm.Cipher
{
    /// <summary>
    /// 算法集合
    /// </summary>
    public class CipherAlgorithmStructure
    {
        /// <summary>
        /// 构造加密算法
        /// </summary>
        /// <param name="algorithm"></param>
        /// <returns></returns>
        public static string Combination(CipherAlgorithm algorithm)
        {
            return algorithm.ToString();
        }

        /// <summary>
        /// 构造加密算法
        /// </summary>
        /// <param name="algorithm"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string Combination(CipherAlgorithm algorithm, CipherMode mode)
        {
            return algorithm + "/" + mode;
        }

        /// <summary>
        /// 构造加密算法
        /// </summary>
        /// <param name="algorithm"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <returns></returns>
        public static string Combination(CipherAlgorithm algorithm, CipherMode mode, CipherPadding padding)
        {
            return algorithm + "/" + mode + "/" + padding;
        }
    }
}
