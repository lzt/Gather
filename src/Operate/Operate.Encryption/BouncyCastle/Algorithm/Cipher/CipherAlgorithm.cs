﻿namespace Operate.Encryption.BouncyCastle.Algorithm.Cipher
{
    /// <summary>
    /// 
    /// </summary>
    public enum CipherAlgorithm
    {
        AES,
        ARC4,
        BLOWFISH,
        CAMELLIA,
        CAST5,
        CAST6,
        DES,
        DESEDE,
        ELGAMAL,
        GOST28147,
        HC128,
        HC256,
        IDEA,
        NOEKEON,
        PBEWITHSHAAND128BITRC4,
        PBEWITHSHAAND40BITRC4,
        RC2,
        RC5,
        RC5_64,
        RC6,
        RIJNDAEL,
        RSA,
        SALSA20,
        SEED,
        SERPENT,
        SKIPJACK,
        TEA,
        TWOFISH,
        VMPC,
        VMPC_KSA3,
        XTEA,
    };
}
