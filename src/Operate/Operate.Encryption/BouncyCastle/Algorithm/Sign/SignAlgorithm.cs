﻿namespace Operate.Encryption.BouncyCastle.Algorithm.Sign
{
    /// <summary>
    /// 签名算法
    /// </summary>
    public class SignAlgorithm
    {
        /// <summary>
        /// MD2WITHRSA or MD2WITHRSAENCRYPTION
        /// </summary>
        public const string MD2WITHRSA = "MD2WITHRSA";

        /// <summary>
        /// MD4WITHRSA or MD4WITHRSAENCRYPTION
        /// </summary>
        public const string MD4WITHRSA = "MD4WITHRSA";

        /// <summary>
        /// MD5WITHRSA or MD5WITHRSAENCRYPTION
        /// </summary>
        public const string MD5WITHRSA = "MD5WITHRSA";

        /// <summary>
        /// SHA1WITHRSA or SHA1WITHRSAENCRYPTION or SHA-1WITHRSA
        /// </summary>
        public const string SHA1WITHRSA = "SHA1WITHRSA";

        /// <summary>
        /// SHA224WITHRSA or SHA224WITHRSAENCRYPTION or SHA-224WITHRSA
        /// </summary>
        public const string SHA224WITHRSA = "SHA224WITHRSA";

        /// <summary>
        /// SHA256WITHRSA or SHA256WITHRSAENCRYPTION or SHA-256WITHRSA
        /// </summary>
        public const string SHA256WITHRSA = "SHA256WITHRSA";

        /// <summary>
        /// SHA384WITHRSA or SHA384WITHRSAENCRYPTION or SHA-384WITHRSA
        /// </summary>
        public const string SHA384WITHRSA = "SHA384WITHRSA";

        /// <summary>
        /// SHA512WITHRSA or SHA512WITHRSAENCRYPTION or SHA-512WITHRSA
        /// </summary>
        public const string SHA512WITHRSA = "SHA512WITHRSA";

        /// <summary>
        /// PSSWITHRSA or RSASSA-PSS or RSAPSS
        /// </summary>
        public const string PSSWITHRSA = "PSSWITHRSA";

        /// <summary>
        /// SHA1WITHRSAANDMGF1 or SHA-1WITHRSAANDMGF1 or SHA1WITHRSA/PSS or SHA-1WITHRSA/PSS
        /// </summary>
        public const string SHA1WITHRSAANDMGF1 = "SHA1WITHRSAANDMGF1";

        /// <summary>
        /// SHA224WITHRSAANDMGF1 or SHA-224WITHRSAANDMGF1 or SHA224WITHRSA/PSS or SHA-224WITHRSA/PSS
        /// </summary>
        public const string SHA224WITHRSAANDMGF1 = "SHA224WITHRSAANDMGF1";

        /// <summary>
        /// SHA256WITHRSAANDMGF1 or SHA-256WITHRSAANDMGF1 or SHA256WITHRSA/PSS or SHA-256WITHRSA/PSS
        /// </summary>
        public const string SHA256WITHRSAANDMGF1 = "SHA256WITHRSAANDMGF1";

        /// <summary>
        /// SHA384WITHRSAANDMGF1 or SHA-384WITHRSAANDMGF1 or SHA384WITHRSA/PSS or SHA-384WITHRSA/PSS
        /// </summary>
        public const string SHA384WITHRSAANDMGF1 = "SHA384WITHRSAANDMGF1";

        /// <summary>
        /// SHA512WITHRSAANDMGF1 or SHA-512WITHRSAANDMGF1 or SHA512WITHRSA/PSS or SHA-512WITHRSA/PSS
        /// </summary>
        public const string SHA512WITHRSAANDMGF1 = "SHA512WITHRSAANDMGF1";

        /// <summary>
        /// RIPEMD128WITHRSA or RIPEMD128WITHRSAENCRYPTION
        /// </summary>
        public const string RIPEMD128WITHRSA = "RIPEMD128WITHRSA";

        /// <summary>
        /// RIPEMD160WITHRSA or RIPEMD160WITHRSAENCRYPTION
        /// </summary>
        public const string RIPEMD160WITHRSA = "RIPEMD160WITHRSA";

        /// <summary>
        /// RIPEMD256WITHRSA or RIPEMD256WITHRSAENCRYPTION
        /// </summary>
        public const string RIPEMD256WITHRSA = "RIPEMD256WITHRSA";

        /// <summary>
        /// NONEWITHRSA or RSAWITHNONE or RAWRSA
        /// </summary>
        public const string NONEWITHRSA = "NONEWITHRSA";

        /// <summary>
        /// RAWRSAPSS or NONEWITHRSAPSS or NONEWITHRSASSA-PSS
        /// </summary>
        public const string RAWRSAPSS = "RAWRSAPSS";

        /// <summary>
        /// NONEWITHDSA or DSAWITHNONE or RAWDSA
        /// </summary>
        public const string NONEWITHDSA = "NONEWITHDSA";

        /// <summary>
        /// DSA or DSAWITHSHA1 or DSAWITHSHA-1 or SHA/DSA or SHA1/DSA or SHA-1/DSA or SHA1WITHDSA or SHA-1WITHDSA
        /// </summary>
        public const string DSA = "DSA";

        /// <summary>
        /// DSAWITHSHA224 or DSAWITHSHA-224 or SHA224/DSA or SHA-224/DSA or SHA224WITHDSA or SHA-224WITHDSA
        /// </summary>
        public const string DSAWITHSHA224 = "DSAWITHSHA224";

        /// <summary>
        /// DSAWITHSHA256 or DSAWITHSHA-256 or SHA256/DSA or SHA-256/DSA or SHA256WITHDSA or SHA-256WITHDSA
        /// </summary>
        public const string DSAWITHSHA256 = "DSAWITHSHA256";

        /// <summary>
        /// DSAWITHSHA384 or DSAWITHSHA-384 or SHA384/DSA or SHA-384/DSA or SHA384WITHDSA or SHA-384WITHDSA
        /// </summary>
        public const string DSAWITHSHA384 = "DSAWITHSHA384";

        /// <summary>
        /// DSAWITHSHA512 or DSAWITHSHA-512 or SHA512/DSA or SHA-512/DSA or SHA512WITHDSA or SHA-512WITHDSA
        /// </summary>
        public const string DSAWITHSHA512 = "DSAWITHSHA512";

        /// <summary>
        /// NONEWITHECDSA or ECDSAWITHNONE
        /// </summary>
        public const string NONEWITHECDSA = "NONEWITHECDSA";

        /// <summary>
        /// ECDSA or SHA1/ECDSA or SHA-1/ECDSA or ECDSAWITHSHA1 or ECDSAWITHSHA-1 or SHA1WITHECDSA or SHA-1WITHECDSA
        /// </summary>
        public const string ECDSA = "ECDSA";

        /// <summary>
        /// SHA224WITHECDSA or SHA224/ECDSA or SHA-224/ECDSA or ECDSAWITHSHA224 or ECDSAWITHSHA-224 or SHA-224WITHECDSA
        /// </summary>
        public const string SHA224WITHECDSA = "SHA224WITHECDSA";

        /// <summary>
        /// SHA256WITHECDSA or SHA256/ECDSA or SHA-256/ECDSA or ECDSAWITHSHA256 or ECDSAWITHSHA-256 or SHA-256WITHECDSA
        /// </summary>
        public const string SHA256WITHECDSA = "SHA256WITHECDSA";

        /// <summary>
        /// SHA384WITHECDSA or SHA384/ECDSA or SHA-384/ECDSA or ECDSAWITHSHA384 or ECDSAWITHSHA-384 or SHA-384WITHECDSA
        /// </summary>
        public const string SHA384WITHECDSA = "SHA384WITHECDSA";

        /// <summary>
        /// SHA512WITHECDSA or SHA512/ECDSA or SHA-512/ECDSA or ECDSAWITHSHA512 or ECDSAWITHSHA-512 or SHA-512WITHECDSA
        /// </summary>
        public const string SHA512WITHECDSA = "SHA512WITHECDSA";

        /// <summary>
        /// RIPEMD160WITHECDSA or RIPEMD160/ECDSA or SHA-512/ECDSA or ECDSAWITHRIPEMD160 or ECDSAWITHRIPEMD160 or 
        /// </summary>
        public const string RIPEMD160WITHECDSA = "RIPEMD160WITHECDSA";

        /// <summary>
        /// GOST3411WITHGOST3410 or GOST-3410 or GOST-3410-94
        /// </summary>
        public const string GOST3411WITHGOST3410 = "GOST3411WITHGOST3410";

        /// <summary>
        /// GOST3411WITHECGOST3410 or ECGOST-3410 or ECGOST-3410-2001
        /// </summary>
        public const string GOST3411WITHECGOST3410 = "GOST3411WITHECGOST3410";
    }
}
