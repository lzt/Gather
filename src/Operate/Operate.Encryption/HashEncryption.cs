﻿using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace Operate.Encryption
{
    /// <summary>
    /// Hash加密
    /// </summary>
    public class HashEncryption
    {
        #region

        private const string Key =
            "126O82O232O80O229O246O8O111O255O199O29O49O56O255O55O112O183O205O69O126O1O136O199O79O123O234O234O47O203O251O113O125O7O21O110O235O93O202O39O220O187O84O240O137O190O206O252O234O249O49O244O35O68O151O19O109O72O201O186O237O191O201O237O22O";

        #endregion

        /// <summary>
        /// 得到随机哈希加密字符串
        /// </summary>
        /// <returns></returns>
        public static string GetSecurity()
        {
            string security = HashEncoding(GetRandomValue());
            return security;
        }

        /// <summary>
        /// 得到一个随机数值
        /// </summary>
        /// <returns></returns>
        public static string GetRandomValue()
        {
            var seed = new System.Random();
            string randomVaule = seed.Next(1, int.MaxValue).ToString(CultureInfo.InvariantCulture);
            return randomVaule;
        }

        /// <summary>
        /// 哈希加密一个字符串
        /// </summary>
        /// <param name="security"></param>
        /// <returns></returns>
        public static string HashEncoding(string security = Key)
        {
            var code = new UnicodeEncoding();
            byte[] message = code.GetBytes(security);
            var arithmetic = new SHA512Managed();
            byte[] value = arithmetic.ComputeHash(message);
            security = "";
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (byte o in value)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                security += (int)o + "O";
            }
            return security;
        }
    }
}
