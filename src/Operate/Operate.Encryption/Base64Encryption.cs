﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;

namespace Operate.Encryption
{
    /// <summary>
    /// Base64操作
    /// </summary>
    public class Base64Encryption
    {
        #region Base64编码

        /// <summary>
        /// 检验是否为Base64编码
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool IsBase64(string source)
        {
            if ((source.Length % 4) != 0)
            {
                return false;
            }
            if (!Regex.IsMatch(source, "^[A-Z0-9/+=]*$", RegexOptions.IgnoreCase))
            {
                return false;
            }
            return true;
        }

        //#region ToBase64 另一种算法

        ///// <summary>
        ///// Converts from the specified encoding to a base 64 string
        ///// </summary>
        ///// <param name="input">Input string</param>
        ///// <param name="originalEncodingUsing">The type of encoding the string is using (defaults to UTF8)</param>
        ///// <returns>Bas64 string</returns>
        //public static string ToBase64(string input, Encoding originalEncodingUsing = null)
        //{
        //    if (string.IsNullOrEmpty(input))
        //        return "";
        //    byte[] tempArray = originalEncodingUsing.Check(new UTF8Encoding()).GetBytes(input);
        //    return Convert.ToBase64String(tempArray);
        //}

        //#endregion

        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="source">要加密的字符串</param>
        /// <returns></returns>
        public static string Encode(string source)
        {
            //如果字符串为空，则返回
            if (string.IsNullOrEmpty(source))
            {
                return "";
            }

            try
            {
                var base64Code = new[]{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T',
											'U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n',
											'o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7',
											'8','9','+','/','='};
                const byte empty = (byte)0;
                var byteMessage = new ArrayList(Encoding.Default.GetBytes(source));
                int messageLen = byteMessage.Count;
                int page = messageLen / 3;
                int use;
                if ((use = messageLen % 3) > 0)
                {
                    for (int i = 0; i < 3 - use; i++)
                        byteMessage.Add(empty);
                    page++;
                }
                var outmessage = new StringBuilder(page * 4);
                for (int i = 0; i < page; i++)
                {
                    var instr = new byte[3];
                    instr[0] = (byte)byteMessage[i * 3];
                    instr[1] = (byte)byteMessage[i * 3 + 1];
                    instr[2] = (byte)byteMessage[i * 3 + 2];
                    var outstr = new int[4];
                    outstr[0] = instr[0] >> 2;
                    outstr[1] = ((instr[0] & 0x03) << 4) ^ (instr[1] >> 4);
                    if (!instr[1].Equals(empty))
                        outstr[2] = ((instr[1] & 0x0f) << 2) ^ (instr[2] >> 6);
                    else
                        outstr[2] = 64;
                    if (!instr[2].Equals(empty))
                        outstr[3] = (instr[2] & 0x3f);
                    else
                        outstr[3] = 64;
                    outmessage.Append(base64Code[outstr[0]]);
                    outmessage.Append(base64Code[outstr[1]]);
                    outmessage.Append(base64Code[outstr[2]]);
                    outmessage.Append(base64Code[outstr[3]]);
                }
                return outmessage.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("base64加密异常：" + ex.Message);
            }
        }
        #endregion

        #region Base64解码

        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="source">要解密的字符串</param>
        public static string Decode(string source)
        {
            //如果字符串为空，则返回
            if (string.IsNullOrEmpty(source))
            {
                return "";
            }

            //将空格替换为加号
            source = source.Replace(" ", "+");

            try
            {
                if ((source.Length % 4) != 0)
                {
                    return "包含不正确的BASE64编码";
                }
                if (!Regex.IsMatch(source, "^[A-Z0-9/+=]*$", RegexOptions.IgnoreCase))
                {
                    return "包含不正确的BASE64编码";
                }
                const string base64Code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
                int page = source.Length / 4;
                var outMessage = new ArrayList(page * 3);
                char[] message = source.ToCharArray();
                for (int i = 0; i < page; i++)
                {
                    var instr = new byte[4];
                    instr[0] = (byte)base64Code.IndexOf(message[i * 4]);
                    instr[1] = (byte)base64Code.IndexOf(message[i * 4 + 1]);
                    instr[2] = (byte)base64Code.IndexOf(message[i * 4 + 2]);
                    instr[3] = (byte)base64Code.IndexOf(message[i * 4 + 3]);
                    var outstr = new byte[3];
                    outstr[0] = (byte)((instr[0] << 2) ^ ((instr[1] & 0x30) >> 4));
                    if (instr[2] != 64)
                    {
                        outstr[1] = (byte)((instr[1] << 4) ^ ((instr[2] & 0x3c) >> 2));
                    }
                    else
                    {
                        outstr[2] = 0;
                    }
                    if (instr[3] != 64)
                    {
                        outstr[2] = (byte)((instr[2] << 6) ^ instr[3]);
                    }
                    else
                    {
                        outstr[2] = 0;
                    }
                    outMessage.Add(outstr[0]);
                    if (outstr[1] != 0)
                        outMessage.Add(outstr[1]);
                    if (outstr[2] != 0)
                        outMessage.Add(outstr[2]);
                }
                var outbyte = (byte[])outMessage.ToArray(Byte.MaxValue.GetType());
                return Encoding.Default.GetString(outbyte);
            }
            catch (Exception ex)
            {
                throw new Exception("base64解密异常：" + ex.Message);
            }
        }
        #endregion
    }
}
