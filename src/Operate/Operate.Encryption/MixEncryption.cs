﻿using System;

namespace Operate.Encryption
{
    /// <summary>
    /// 混合加密
    /// </summary>
   public class MixEncryption
    {
        const string Key = "ikjuoperk5e87f45768s98eda1ioec5x";

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Encrypt(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new Exception("空字符串不允许加密");
            }

            var c = new Media.Image.Captcha { LetterCount = 4 };
            source = c.GetRandomString() + source + Random.Random.CreateRandomCode(4);
            var result = DesEncryption.Encrypt(source, Key);
            result = result.Replace("+", "-").Replace("/", "@").Replace("=", "");

            return result;
        }

        /// <summary>
        /// 携带时间信息进行加密
        /// </summary>
        /// <param name="source"></param>
        /// <param name="timeSpan"></param>
        /// <returns></returns>
        public static string EncryptWithExpirationTime(string source, TimeSpan timeSpan)
        {
            var time = DateTime.Now.Add(timeSpan);
            return EncryptWithExpirationTime(source, time);
        }

        /// <summary>
        /// 携带时间信息进行加密
        /// </summary>
        /// <param name="source"></param>
        /// <param name="expirationtime"></param>
        /// <returns></returns>
        public static string EncryptWithExpirationTime(string source, DateTime expirationtime)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new Exception("空字符串不允许加密");
            }

            var c = new Media.Image.Captcha { LetterCount = 4 };
            source = c.GetRandomString() + expirationtime.ToString("yyyy-MM-dd HH:mm:ss") + source + Random.Random.CreateRandomCode(4);
            var result = DesEncryption.Encrypt(source, Key);
            result = result.Replace("+", "-").Replace("/", "@").Replace("=", "");

            return result;
        }

        /// <summary>
        /// 揭解数据
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string Decrypt(string target)
        {
            var result = "";

            if (!string.IsNullOrWhiteSpace(target))
            {
                var residue = target.Length % 4;

                if (residue > 0)
                {
                    var complement = 4 - residue;

                    for (int i = 0; i < complement; i++)
                    {
                        target += "=";
                    }
                }

                target = target.Replace("-", "+").Replace("@", "/");
                result = DesEncryption.Decrypt(target, Key);
                result = result.Substring(0, result.Length - 4);
                result = result.Substring(4);
            }

            return result;
        }

        /// <summary>
        /// 解密携带时间的数据
        /// </summary>
        /// <param name="target"></param>
        /// <param name="expired"></param>
        /// <returns></returns>
        public static string DecryptWithExpirationTime(string target, out bool expired)
        {
            var result = "";
            expired = false;
            if (!string.IsNullOrWhiteSpace(target))
            {
                var residue = target.Length % 4;

                if (residue > 0)
                {
                    var complement = 4 - residue;

                    for (int i = 0; i < complement; i++)
                    {
                        target += "=";
                    }
                }

                target = target.Replace("-", "+").Replace("@", "/");
                result = DesEncryption.Decrypt(target, Key);
                result = result.Substring(0, result.Length - 4);
                result = result.Substring(4);

                var timeString = result.Substring(0, 19);
                DateTime time;
                if (DateTime.TryParse(timeString, out time))
                {
                    if (time.Subtract(DateTime.Now).Milliseconds < 0)
                    {
                        expired = true;
                    }
                }
                else
                {
                    expired = true;
                }

                result = result.Substring(19);
            }

            return result;
        }

    }
}
