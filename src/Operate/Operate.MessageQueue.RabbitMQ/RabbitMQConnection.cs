﻿using System.Collections.Generic;
using RabbitMQ.Client;

namespace Operate.MessageQueue.RabbitMQ
{
    public class RabbitMQConnection : MQConnectionBase
    {
        public const string DefaultExchange = "DefaultExchange";

        public const string DefaultRoutingKey = "DefaultRoutingKey";

        #region Properties

        public string QueueName { get; }

        public string UserName { get; }

        public string Password { get; }

        public string Host { get; }

        public string VirtualHost { get; }

        public int? Port { get; set; }

        public bool Durable { get; set; }

        public bool Exclusive { get; set; }

        public bool AutoDelete { get; set; }

        public IDictionary<string, object> Arguments { get; set; }

        public string Exchange { get; }

        public string Type { get; }

        public string RoutingKey { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// RabbitMQConnection
        /// </summary>
        /// <param name="host"></param>
        public RabbitMQConnection(string host) : this(host, "currentQueue")
        {
        }

        /// <summary>
        /// RabbitMQConnection
        /// </summary>
        /// <param name="host"></param>
        /// <param name="queueName"></param>
        public RabbitMQConnection(string host, string queueName)
            : this(ConnectionFactory.DefaultUser, ConnectionFactory.DefaultPass, host, null, queueName)
        {
        }

        /// <summary>
        /// RabbitMQConnection
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <param name="queueName"></param>
        public RabbitMQConnection(string userName, string password, string host, string queueName)
            : this(userName, password, host, null, queueName)
        {
        }

        /// <summary>
        /// RabbitMQConnection
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="queueName"></param>
        public RabbitMQConnection(string userName, string password, string host, int? port, string queueName)
            : this(userName, password, host, port, ConnectionFactory.DefaultVHost, queueName)
        {
        }


        /// <summary>
        /// ActiveMQConnection
        /// </summary>
        public RabbitMQConnection(string userName, string password, string host, int? port, string virtualHost, string queueName)
        {
            Host = host;
            Port = port;
            VirtualHost = virtualHost;
            QueueName = queueName;
            Password = password;
            UserName = userName;

            Durable = false;
            Exclusive = false;
            AutoDelete = true;
            Arguments = null;

            ThreadMode = ThreadMode.SingleThread;
            MaxThread = DefaultMaxThread;

            UseLogger = false;

            Exchange = DefaultExchange;
            RoutingKey = DefaultRoutingKey;
            Type = ExchangeType.Topic;
        }

        #endregion
    }
}
