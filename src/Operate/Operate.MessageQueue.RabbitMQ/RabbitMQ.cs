﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Operate.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Operate.MessageQueue.RabbitMQ
{
    public class RabbitMQ : MessageQueueBase, IMessageSender, IMessageReceiver, IDisposable
    {
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _senderConnection;
        private readonly IModel _sender;

        public bool IsOpen
        {
            get
            {
                // ReSharper disable once ConvertPropertyToExpressionBody
                return _senderConnection.IsOpen;
            }
        }

        public RabbitMQConnection Connection { get; }

        public RabbitMQ(RabbitMQConnection rabbitMQConnection)
        {
            Connection = rabbitMQConnection;
            _connectionFactory = new ConnectionFactory
            {
                HostName = rabbitMQConnection.Host,
                UserName = rabbitMQConnection.UserName,
                Password = rabbitMQConnection.Password,
                VirtualHost = rabbitMQConnection.VirtualHost,
            };

            if (rabbitMQConnection.Port != null)
            {
                _connectionFactory.Port = rabbitMQConnection.Port.Value;
            }
            else
            {
                rabbitMQConnection.Port = _connectionFactory.Port;
            }

            TaskScheduler = null;
            TaskList = new List<Task>();

            if (Connection.MaxThread <= 1)
            {
                Connection.ThreadMode = ThreadMode.SingleThread;
                Connection.MaxThread = MQConnectionBase.DefaultMaxThread;
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskScheduler = new OperateTaskScheduler(Connection.MaxThread);
            }

            _senderConnection = _connectionFactory.CreateConnection();

            _sender = _senderConnection.CreateModel();
            _sender.ExchangeDeclare(Connection.Exchange, Connection.Type, Connection.Durable, Connection.AutoDelete, Connection.Arguments);
        }

        public RabbitMQ(ConnectionFactory connectionFactory, string queue, bool durable, bool exclusive, bool autoDelete, IDictionary<string, object> arguments)
        {
            Connection = new RabbitMQConnection(
                    connectionFactory.UserName,
                    connectionFactory.Password,
                    connectionFactory.HostName,
                    connectionFactory.Port,
                    connectionFactory.VirtualHost,
                    queue
                )
            {
                Durable = durable,
                Exclusive = exclusive,
                AutoDelete = autoDelete,
                Arguments = arguments
            };

            TaskScheduler = null;
            TaskList = new List<Task>();

            if (Connection.MaxThread <= 1)
            {
                Connection.ThreadMode = ThreadMode.SingleThread;
                Connection.MaxThread = MQConnectionBase.DefaultMaxThread;
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskScheduler = new OperateTaskScheduler(Connection.MaxThread);
            }

            _senderConnection = connectionFactory.CreateConnection();

            _sender = _senderConnection.CreateModel();
            _sender.ExchangeDeclare(Connection.Exchange, Connection.Type, Connection.Durable, Connection.AutoDelete, Connection.Arguments);
            //_sender.QueueDeclare(Connection.QueueName, Connection.Durable, Connection.Exclusive, Connection.AutoDelete, Connection.Arguments);
        }

        protected override bool GetUserLogger()
        {
            return Connection.UseLogger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Send(byte[] message)
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                bool result;
                TrySend(message, out result);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(SendCallByAsync, message);

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        public void SendCallByAsync(byte[] message)
        {
            bool result;
            TrySend(message, out result);
            Thread.Sleep(50);
        }

        /// <summary>
        /// Send
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void TrySend(byte[] message, out bool result)
        {
            try
            {
                _sender.BasicPublish(Connection.Exchange, Connection.RoutingKey, null, message);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                if (Connection.UseLogger)
                {
                    Logger.RecordException(ex);
                }
            }
        }

        public void TrySend(string message, out bool result)
        {
            var bytes = Encoding.UTF8.GetBytes(message);
            TrySend(bytes, out result);
        }

        /// <summary>
        /// Send
        /// </summary>
        /// <param name="message"></param>
        public void Send(string message)
        {
            var bytes = Encoding.UTF8.GetBytes(message);
            Send(bytes);
        }

        public void ReceiveMessage(Action<byte[]> messageReceiver)
        {
            using (var connection = _connectionFactory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(Connection.Exchange, Connection.Type, Connection.Durable, Connection.AutoDelete, Connection.Arguments);
                    //channel.ExchangeDeclare(EXCHANGE_NAME, ExchangeType.Topic, false, true, null);

                    string queueName = channel.QueueDeclare();

                    EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (o, e) =>
                    {
                        if (Connection.ThreadMode == ThreadMode.SingleThread)
                        {
                            messageReceiver(e.Body);
                        }

                        if (Connection.ThreadMode == ThreadMode.ThreadPool)
                        {
                            TaskExec(messageReceiver, e.Body);

                            if (TaskList.Count >= Connection.MaxThread * 2)
                            {
                                try
                                {
                                    //等待所有线程全部运行结束
                                    Task.WaitAll(TaskList.ToArray());
                                }
                                catch (AggregateException ex)
                                {
                                    if (Connection.UseLogger)
                                    {
                                        //.NET4 Task的统一异常处理机制
                                        foreach (Exception inner in ex.InnerExceptions)
                                        {
                                            Logger.RecordException(inner);
                                        }
                                    }
                                }

                                TaskList.Clear();
                            }
                        }
                    };

                    string consumerTag = channel.BasicConsume(queueName, true, consumer);

                    channel.QueueBind(queueName, Connection.Exchange, Connection.RoutingKey);

                    Console.WriteLine("Listening press ENTER to quit");
                    Console.ReadLine();

                    channel.QueueUnbind(queueName, Connection.Exchange, Connection.RoutingKey, null);
                }
            }

            return;
            //using (var connection = _connectionFactory.CreateConnection())
            //{
            //    using (IModel channel = connection.CreateModel())
            //    {
            //var receiverConnection = _connectionFactory.CreateConnection();
            //var channel = receiverConnection.CreateModel();
            //channel.ExchangeDeclare(Connection.Exchange, Connection.Type, Connection.Durable, Connection.AutoDelete, Connection.Arguments);

            //string queueName = channel.QueueDeclare();

            //EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            //consumer.Received += (o, e) =>
            //{
            //    if (Connection.ThreadMode == ThreadMode.SingleThread)
            //    {
            //        messageReceiver(e.Body);
            //    }

            //    if (Connection.ThreadMode == ThreadMode.ThreadPool)
            //    {
            //        TaskExec(messageReceiver, e.Body);

            //        if (_taskList.Count >= Connection.MaxThread * 2)
            //        {
            //            try
            //            {
            //                //等待所有线程全部运行结束
            //                Task.WaitAll(_taskList.ToArray());
            //            }
            //            catch (AggregateException ex)
            //            {
            //                if (Connection.UserLogger)
            //                {
            //                    //.NET4 Task的统一异常处理机制
            //                    foreach (Exception inner in ex.InnerExceptions)
            //                    {
            //                        Logger.Current.Error("异常：", inner);
            //                    }
            //                }
            //            }

            //            _taskList.Clear();
            //        }
            //    }
            //};

            //string consumerTag = channel.BasicConsume(queueName, true, consumer);

            //channel.QueueBind(queueName, Connection.Exchange, Connection.RoutingKey);

            //Console.WriteLine("Listening press ENTER to quit");
            //Console.ReadLine();

            //channel.QueueUnbind(Connection.QueueName, Connection.Exchange, Connection.RoutingKey, null);
            //    }
            //}

            //_serverThread = new Thread(() =>
            //{
            //    //using (var receiverConnection = _connectionFactory.CreateConnection())
            //    //{
            //    //    using (var receiver = receiverConnection.CreateModel())
            //    //    {
            //    var receiverConnection = _connectionFactory.CreateConnection();
            //    var receiver = receiverConnection.CreateModel();
            //    receiver.QueueDeclare(Connection.QueueName, Connection.Durable, Connection.Exclusive, Connection.AutoDelete, Connection.Arguments);

            //    var consumer = new EventingBasicConsumer(receiver);
            //    consumer.Received += (o, e) =>
            //    {
            //        if (Connection.ThreadMode == ThreadMode.SingleThread)
            //        {
            //            messageReceiver(e.Body);
            //        }

            //        if (Connection.ThreadMode == ThreadMode.ThreadPool)
            //        {
            //            TaskExec(messageReceiver, e.Body);

            //            if (_taskList.Count >= Connection.MaxThread * 2)
            //            {
            //                try
            //                {
            //                    //等待所有线程全部运行结束
            //                    Task.WaitAll(_taskList.ToArray());
            //                }
            //                catch (AggregateException ex)
            //                {
            //                    if (Connection.UserLogger)
            //                    {
            //                        //.NET4 Task的统一异常处理机制
            //                        foreach (Exception inner in ex.InnerExceptions)
            //                        {
            //                            Logger.Current.Error("异常：", inner);
            //                        }
            //                    }
            //                }

            //                _taskList.Clear();
            //            }
            //        }
            //    };

            //    receiver.BasicConsume(Connection.QueueName, true, consumer);
            //    //    }
            //    //}
            //});
            //_serverThread.Start();
        }

        private bool _disposed;
        public void Dispose()
        {
            if (_disposed) return;

            // ReSharper disable once UseNullPropagation
            if (TaskScheduler != null)
            {
                TaskScheduler.Dispose();
            }

            _sender.Close();
            _sender.Dispose();

            _senderConnection.Close();
            _senderConnection.Dispose();

            _disposed = true;
        }
    }
}
