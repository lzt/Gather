﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Operate.Caching.Interfaces;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Caching
{
    /// <summary>
    /// Acts as a cache
    /// </summary>
    public class Cache<TKeyType> : ICache<TKeyType>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Cache()
        {
            if (InternalCache == null)
                InternalCache = new ConcurrentDictionary<TKeyType, object>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Internal cache
        /// </summary>
        protected ConcurrentDictionary<TKeyType, object> InternalCache { get; private set; }

        /// <summary>
        /// Collection of keys
        /// </summary>
        public virtual ICollection<TKeyType> Keys
        {
            get
            {
                return InternalCache.Keys;
            }
        }

        /// <summary>
        /// The number of items in the cache
        /// </summary>
        public virtual int Count { get { return InternalCache.Count; } }

        /// <summary>
        /// Gets the item associated with the key
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>The item associated with the key</returns>
        public virtual object this[TKeyType key]
        {
            get
            {
                return Get<object>(key);
            }
            set
            {
                Add(key, value);
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<object> GetEnumerator()
        {
            return InternalCache.Keys.Select(key => InternalCache[key]).GetEnumerator();
        }

        /// <summary>
        /// Gets the enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return InternalCache.Keys.Select(key => InternalCache[key]).GetEnumerator();
        }

        /// <summary>
        /// Clears the cache
        /// </summary>
        public virtual void Clear()
        {
            InternalCache.Clear();
        }

        /// <summary>
        /// Removes an item from the cache
        /// </summary>
        /// <param name="key">Key associated with the item to remove</param>
        public virtual void Remove(TKeyType key)
        {
            if (Exists(key))
            {
                object tempItem;
                InternalCache.TryRemove(key, out tempItem);
            }
        }

        /// <summary>
        /// Determines if the item exists
        /// </summary>
        /// <param name="key">The key associated with the item</param>
        /// <returns>True if it does, false otherwise</returns>
        public virtual bool Exists(TKeyType key)
        {
            return InternalCache.ContainsKey(key);
        }

        /// <summary>
        /// Adds an item to the cache
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public virtual void Add(TKeyType key, object value)
        {
            InternalCache.AddOrUpdate(key, value, (x, y) => value);
        }

        /// <summary>
        /// Gets an item from the cache
        /// </summary>
        /// <typeparam name="TValueType">Item type</typeparam>
        /// <param name="key">Key to search for</param>
        /// <returns>The item associated with the key</returns>
        public virtual TValueType Get<TValueType>(TKeyType key)
        {
            object tempItem;
            return InternalCache.TryGetValue(key, out tempItem) ? tempItem.To(default(TValueType)) : default(TValueType);
        }

        #endregion
    }
}