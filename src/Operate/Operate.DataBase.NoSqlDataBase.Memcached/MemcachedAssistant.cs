﻿using System;
using System.Collections.Generic;
using System.Net;
using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using Newtonsoft.Json;

namespace Operate.DataBase.NoSqlDataBase.Memcached
{
    /// <summary>
    /// MemcachedAssistant
    /// </summary>
    public class MemcachedAssistant : ICacheAssistant
    {
        #region Fields

        private readonly MemcachedClient _cache;

        #endregion

        #region Properties

        /// <summary>
        /// CacheType
        /// </summary>
        public CacheType CacheType { get; }

        #endregion

        private MemcachedAssistant()
        {
            CacheType = CacheType.Memcached;
            var config = GetMemcachedClientConfiguration();
            _cache = new MemcachedClient(config);
        }

        static MemcachedAssistant()
        {
            // //初始化memcache服务器池
            //SockIOPool pool = SockIOPool.GetInstance();
            ////设置Memcache池连接点服务器端。
            //pool.SetServers(serverlist);
            ////其他参数根据需要进行配置

            //pool.InitConnections = 3;
            //pool.MinConnections = 3;
            //pool.MaxConnections = 5;

            //pool.SocketConnectTimeout = 1000;
            //pool.SocketTimeout = 3000;

            //pool.MaintenanceSleep = 30;
            //pool.Failover = true;

            //pool.Nagle = false;
            //pool.Initialize();

            //cache = new MemcachedClient();
            //cache.EnableCompression = false;

            //config.Authentication.Type = typeof(PlainTextAuthenticator);
            //config.Authentication.Parameters["userName"] = "username";
            //config.Authentication.Parameters["password"] = "password";
            //config.Authentication.Parameters["zone"] = "zone";//domain?   ——Jeffrey 2015.10.20
            var config = GetMemcachedClientConfiguration();
            var cache = new MemcachedClient(config);

            var testKey = Guid.NewGuid().ToString();
            var testValue = "test";
            cache.Store(StoreMode.Set, testKey, testValue);

            object storeValue;
            var result = cache.TryGet(testKey, out storeValue);
            if (result)
            {
                if (storeValue as string != testValue)
                {
                    throw new Exception("Memcached失效，没有计入缓存！");
                }

                cache.Remove(testKey);
            }
            else
            {
                throw new Exception("读取缓存失败！");
            }
        }

        #region 单例

        //静态SearchCache
        public static MemcachedAssistant Instance
        {
            get
            {
                return Nested.instance;//返回Nested类中的静态成员instance
            }
        }

        private class Nested
        {
            static Nested()
            {
            }
            //将instance设为一个初始化的BaseCacheStrategy新实例
            public static readonly MemcachedAssistant instance = new MemcachedAssistant();
        }

        #endregion

        private static MemcachedClientConfiguration GetMemcachedClientConfiguration()
        {
            //每次都要新建
            var config = new MemcachedClientConfiguration();
            foreach (var server in MemcachedManager.Serverlist)
            {
                config.Servers.Add(new IPEndPoint(IPAddress.Parse(server.Key), server.Value));
            }
            config.Protocol = MemcachedProtocol.Binary;

            return config;
        }

        /// <summary>
        /// GetCount
        /// </summary>
        /// <returns></returns>
        public long GetCount()
        {
            throw new NotImplementedException();//TODO:需要定义二级缓存键，从池中获取
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            var v = _cache.Get(key);

            if (v != null)
            {
                return JsonConvert.DeserializeObject<T>(v.ToString());
            }

            return default(T);
        }

        /// <summary>
        /// Set，过期时间1天
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Set<T>(string key, T value)
        {
            if (value == null)
            {
                return false;
            }

            //TODO：加了绝对过期时间就会立即失效（再次获取后为null），memcache低版本的bug
            return Set(key, value, DateTime.Now.AddDays(1));
        }

        /// <summary>
        /// Set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiresAt"></param>
        /// <returns></returns>
        public bool Set<T>(string key, T value, DateTime expiresAt)
        {
            if (value == null)
            {
                return false;
            }

            //TODO：加了绝对过期时间就会立即失效（再次获取后为null），memcache低版本的bug
            var expiresIn = expiresAt.Subtract(DateTime.Now);

            if (expiresIn.TotalSeconds >= MemcachedManager.MaxExpireSecond)
            {
                expiresIn = new TimeSpan(MemcachedManager.MaxExpireSecond);
            }
            return Set(key, value, expiresIn);
        }

        /// <summary>
        /// Set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiresIn"></param>
        /// <returns></returns>
        public bool Set<T>(string key, T value, TimeSpan expiresIn)
        {
            if (value == null)
            {
                return false;
            }

            var json = JsonConvert.SerializeObject(value);
            //TODO：加了绝对过期时间就会立即失效（再次获取后为null），memcache低版本的bug
            var r = _cache.Store(StoreMode.Set, key, json, expiresIn);
            _cache.Get(key);
            return r;
        }

        /// <summary>
        /// Remove
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            return _cache.Remove(key);
        }

        public void Remove(List<string> ketList)
        {
            throw new NotImplementedException();
        }
    }
}
