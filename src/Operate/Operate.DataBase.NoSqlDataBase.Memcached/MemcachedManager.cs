﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.DataBase.NoSqlDataBase.Memcached
{
    /// <summary>
    /// MemcachedManager
    /// </summary>
    public class MemcachedManager
    {
        #region Properties

        /// <summary>
        /// Serverlist key:Ip ，value:port
        /// </summary>
        public static Dictionary<string, int> Serverlist;

        /// <summary>
        /// MaxExpireSecond
        /// </summary>
        public const int MaxExpireSecond = 2592000;

        #endregion

        static MemcachedManager()
        {
            Serverlist = new Dictionary<string, int>();
        }
    }
}
