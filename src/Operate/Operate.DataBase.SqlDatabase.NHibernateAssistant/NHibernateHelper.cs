﻿/*
* Code By 卢志涛
*/

using System.Collections.Specialized;
using System.IO;
using System.Xml;
using NHibernate;
using NHibernate.Cfg;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant
{
    /// <summary>
    /// NHibernate辅助类
    /// </summary>
    public class NHibernateHelper
    {
        private ISessionFactory _sessionFactory;

        private readonly Configuration _conf;

        /// <summary>
        /// Nibernate配置文件
        /// </summary>
        public NibernateConfig Config { get; set; }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="config">配置文件</param>
        public NHibernateHelper(string config) : this(config, new NameValueCollection())
        {
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="config">配置文件</param>
        /// <param name="mappingList"></param>
        public NHibernateHelper(string config, NameValueCollection mappingList)
        {
            Config = new NibernateConfig(config);
            var textreader = new StringReader(config);
            var xmlreader = XmlReader.Create(textreader);
            _conf = (new Configuration()).Configure(xmlreader);

            foreach (var mapkey in mappingList.AllKeys)
            {
                if (!mappingList[mapkey].IsNullOrEmpty())
                {
                    var filter = new { EntityName = mapkey };
                    var o = _conf.ClassMappings.Get(filter, "EntityName");
                    if (o == null)
                    {
                        var xml = mappingList[mapkey];
                        _conf.AddXml(xml, mapkey);
                    }
                }
            }

            BuildSessionFactory();

            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
            Config.ConfigInfo.AssemblyChanged += ConfigInfo_AssemblyChanged;
        }

        void ConfigInfo_AssemblyChanged(object sender, System.EventArgs e)
        {
            _conf.AddAssembly(Config.ConfigInfo.Assembly);
            BuildSessionFactory();
        }

        void ConfigInfo_ConnectionStringChanged(object sender, System.EventArgs e)
        {
            _conf.Properties.Remove(Environment.ConnectionString);
            _conf.Properties.Add(Environment.ConnectionString, Config.ConnectionString);
            BuildSessionFactory();
        }

        private void BuildSessionFactory()
        {
            _sessionFactory = _conf.BuildSessionFactory();
        }

        /// <summary>
        ///获取Session
        /// </summary>
        /// <returns></returns>
        public ISession GetSession()
        {
            return _sessionFactory == null ? null : _sessionFactory.OpenSession();
        }

        ///// <summary>
        ///// CheckMappingExist
        ///// </summary>
        ///// <param name="cfg"></param>
        ///// <param name="mappingName">映射名称</param>
        ///// <returns></returns>
        //private bool CheckMappingExist(Configuration cfg, string mappingname)
        //{
        //    bool result = cfg.ClassMappings.Any(cm => cm.MappedClass.Name == mappingname);

        //    return result;
        //}

        /// <summary>
        ///获取cfg
        /// </summary>
        /// <returns></returns>
        public Configuration GetCfg()
        {
            return _conf;
        }

        /// <summary>
        /// CloseSession
        /// </summary>
        public void CloseSessionCreator()
        {
            _sessionFactory.Close();
            _sessionFactory.Dispose();
        }
    }
}
