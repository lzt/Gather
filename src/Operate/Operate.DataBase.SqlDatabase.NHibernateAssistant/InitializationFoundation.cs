﻿using System.Collections.Specialized;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant
{
    /// <summary>
    /// 初始配置
    /// </summary>
    public class InitializationFoundation
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public static NameValueCollection MappingList { get; set; }

        /// <summary>
        /// 数据库类型
        /// </summary>
        static InitializationFoundation()
        {
            MappingList=new NameValueCollection();
        }
    }
}
