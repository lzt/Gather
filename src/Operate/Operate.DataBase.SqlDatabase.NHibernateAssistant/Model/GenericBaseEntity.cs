﻿using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Model
{
    /// <summary>
    /// GenericBaseEntity
    /// </summary>
    /// <typeparam name="TDatatable"></typeparam>
    public class GenericBaseEntity<TDatatable> : BaseEntity where TDatatable : BaseEntity, new()
    {
        /// <summary>
        /// 与默认值合并
        /// </summary>
        /// <returns></returns>
        public virtual TDatatable MegerDefault()
        {
            return MegerDefault<TDatatable>();
        }
    }
}
