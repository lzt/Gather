﻿using System;
using System.Data;
using System.Data.OleDb;
using Operate.DataBase.SqlDatabase.Model;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter
{
    /// <summary>
    /// SybaseAdaptiveServer操作基础类
    /// </summary>
    public class SybaseAdaptiveServerBaseDal : SybaseAdaptiveServerBaseDal<BaseEntity>
    {
        #region Constructors

        /// <summary>
        /// AccessBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SybaseAdaptiveServerBaseDal(string hibernateCfgFilePath, string mappingName = null, string mappingXml = null)
            : base(hibernateCfgFilePath, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SybaseAdaptiveServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName = null, string mappingXml = null)
            : base(configContent, configFileType, mappingName, mappingXml)
        {
        }

        #endregion
    }

    /// <summary>
    /// SybaseAdaptiveServer操作基础类
    /// </summary>
    public class SybaseAdaptiveServerBaseDal<T> : NhBaseDal<T> where T : BaseEntity, new()
    {
        #region Constructors

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        public SybaseAdaptiveServerBaseDal(string hibernateCfgFilePath)
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SybaseAdaptiveServerBaseDal(string hibernateCfgFilePath, string mappingName , string mappingXml )
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        public SybaseAdaptiveServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SybaseAdaptiveServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName , string mappingXml )
            : base(configContent, configFileType, mappingName, mappingXml)
        {
        }

        #endregion

        #region Script

        /// <summary>
        /// 获取创建脚本
        /// </summary>
        /// <returns></returns>
        [Obsolete("暂不支持", true)]
        public override string GetCreateScript(SqlDatabase.ConstantCollection.DataEntityType type)
        {
            throw new System.Exception("暂不支持");
        }

        #endregion

        #region DataBaseParam

        /// <summary>
        /// CreateNewParam
        /// </summary>
        /// <returns></returns>
        public override IDataParameter CreateNewParam()
        {
            return new OleDbParameter();
        }

        #endregion
    }
}
