﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Event;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace;
using Operate.DataBase.SqlDatabase.DataBaseAdapter.Generic;
using Operate.Exception;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Generic
{
    /// <summary>
    /// NhGenericDal
    /// </summary>
    public class NhGenericDal : GenericDal, INhGenericDal
    {
        #region Event

        /// <summary>
        /// Session执行代码之前之后
        /// </summary>
        public event SessionBeforeRunHandler SessionBeforeRun;

        /// <summary>
        /// OnSessionBeforeRun
        /// </summary>
        protected virtual void OnSessionBeforeRun()
        {
            SessionBeforeRunHandler handler = SessionBeforeRun;
            if (handler != null) handler(this, System.EventArgs.Empty);
        }

        /// <summary>
        /// Session关闭之后
        /// </summary>
        public event SessionClosedHandler SessionClosed;

        /// <summary>
        /// OnSessionClosed
        /// </summary>
        protected virtual void OnSessionClosed()
        {
            SessionClosedHandler handler = SessionClosed;
            if (handler != null) handler(this, System.EventArgs.Empty);
        }

        #endregion

        #region Method

        #region Insert

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelItem">数据模型</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void Insert<TModel>(TModel modelItem, ref ISession session, bool closeSession = true)
        {
            using (ITransaction trans = session.BeginTransaction())
            {
                try
                {
                    session.Save(modelItem);
                    session.Flush();
                    trans.Commit();

                    session.Clear();
                    if (closeSession)
                    {
                        session.Close();
                    }
                }
                catch (HibernateException)
                {
                    trans.Rollback();

                    session.Clear();
                    session.Close();
                    throw;
                }
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelItemList"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void Insert<TModel>(List<TModel> modelItemList, ref ISession session, bool closeSession = true)
        {
            OnSessionBeforeRun();
            using (ITransaction trans = session.BeginTransaction())
            {
                try
                {
                    foreach (var model in modelItemList)
                    {
                        session.Save(model);
                    }
                    session.Flush();
                    trans.Commit();

                    session.Clear();
                    if (closeSession)
                    {
                        session.Close();
                    }
                }
                catch (HibernateException)
                {
                    trans.Rollback();

                    session.Clear();
                    session.Close();
                    throw;
                }
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelItem">数据模型</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void Update<TModel>(TModel modelItem, ref ISession session, bool closeSession = true)
        {
            OnSessionBeforeRun();
            using (ITransaction trans = session.BeginTransaction())
            {
                try
                {
                    session.Merge(modelItem);
                    //Session.Update(modelItem);
                    session.Flush();
                    trans.Commit();

                    session.Clear();
                    if (closeSession)
                    {
                        session.Close();
                    }
                }
                catch (HibernateException)
                {
                    trans.Rollback();

                    session.Clear();
                    session.Close();
                    throw;
                }
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelItemList"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void Update<TModel>(List<TModel> modelItemList, ref ISession session, bool closeSession = true)
        {
            foreach (var m in modelItemList)
            {
                Update(m, ref session, false);
            }
            if (closeSession)
            {
                session.Close();
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelItem">数据模型</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void Delete<TModel>(TModel modelItem, ref ISession session, bool closeSession = true)
        {
            OnSessionBeforeRun();
            using (ITransaction trans = session.BeginTransaction())
            {
                try
                {
                    session.Delete(modelItem);
                    session.Flush();
                    trans.Commit();

                    session.Clear();
                    if (closeSession)
                    {
                        session.Close();
                    }
                }
                catch (HibernateException)
                {
                    trans.Rollback();

                    session.Clear();
                    session.Close();
                    throw;
                }
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelItemList"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void Delete<TModel>(List<TModel> modelItemList, ref ISession session, bool closeSession = true)
        {
            foreach (var m in modelItemList)
            {
                Delete(m, ref session, false);
            }
            if (closeSession)
            {
                session.Close();
            }
        }

        /// <summary>
        /// DeleteByPrimarykey
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="primaryValue"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void DeleteByPrimarykey<TModel>(object primaryValue, ref ISession session, bool closeSession = true)
        {
            var m = GetByPrimarykey<TModel>(primaryValue, ref session, false);
            Delete(m, ref session, false);
            if (closeSession)
            {
                session.Close();
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        /// <param name="primaryValueList">即将要删除的数据主键集合</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        public override void DeleteByPrimarykeyList<TModel>(List<object> primaryValueList, ref ISession session, bool closeSession = true)
        {
            var list = GetList<TModel>(primaryValueList, ref session, false);
            if (list.Count > 0)
            {
                Delete(list.ToList(), ref session, false);
            }
            if (closeSession)
            {
                session.Close();
            }
        }

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public override void DeleteByWhere(string where, ref ISession session, bool closeSession = true)
        {
            if (string.IsNullOrEmpty(where))
            {
                session.Clear();
                session.Close();
                throw new EmptyException();
            }
            using (ITransaction trans = session.BeginTransaction())
            {
                try
                {
                    var hql = string.Format("from  {0} Where  {1} ", EntityName, where);
                    //var result = 
                    session.Delete(hql);
                    session.Flush();
                    trans.Commit();

                    session.Clear();
                    if (closeSession)
                    {
                        session.Close();
                    }
                }
                catch (HibernateException)
                {
                    trans.Rollback();

                    session.Clear();
                    session.Close();
                    throw;
                }
            }
        }

        /// <summary>
        /// GetByPrimarykey
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="primaryValue"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public override TModel GetByPrimarykey<TModel>(object primaryValue, ref ISession session, bool closeSession = true)
        {
            TModel result;
            try
            {
                result = session.Get<TModel>(primaryValue);

                session.Clear();
                if (closeSession)
                {
                    session.Close();
                }
            }
            catch (System.Exception)
            {
                session.Clear();
                session.Close();
                throw;
            }
            return result;
        }

        #endregion

        #region GetList

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public override IList<TModel> GetList<TModel>(int currentPage, int pagesize, out long totalPages, out long totalRecords, string @where, string sort, string @group, ref ISession session, bool closeSession = true)
        {
            IList<TModel> result;
            try
            {
                string hql;
                if (currentPage == -1 && pagesize == -1)
                {
                    //不分页
                    //hql
                    hql = string.Format("select count(*) from  {0} {1} ", EntityName, string.IsNullOrEmpty(where) ? "" : " where " + where);
                    var hqlTotlaResordQuery = session.CreateQuery(hql);
                    totalRecords = hqlTotlaResordQuery.UniqueResult<long>();
                    totalPages = totalRecords > 0 ? 1 : 0;

                    //hql
                    hql = string.Format("from  {0} {1} {2} {3}", EntityName, string.IsNullOrEmpty(where) ? "" : " where " + where, string.IsNullOrEmpty(group) ? "" : " group by " + group, string.IsNullOrEmpty(sort) ? "" : " order by " + sort);
                    var hqlQuery = session.CreateQuery(hql);
                    result = hqlQuery.List<TModel>();
                }
                else
                {
                    //分业获取
                    //hql
                    hql = string.Format("select count(*) from  {0} {1} ", EntityName, string.IsNullOrEmpty(where) ? "" : " where " + where);
                    var hqlTotlaResordQuery = session.CreateQuery(hql);
                    totalRecords = hqlTotlaResordQuery.UniqueResult<long>();
                    totalPages = (totalRecords % pagesize == 0 ? totalRecords / pagesize : totalRecords / pagesize + 1);

                    //hql
                    hql = string.Format("from  {0} {1} {2} {3}", EntityName, string.IsNullOrEmpty(where) ? "" : " where " + where, string.IsNullOrEmpty(group) ? "" : " group by " + group, string.IsNullOrEmpty(sort) ? "" : " order by " + sort);
                    var hqlQuery = session.CreateQuery(hql).SetFirstResult((currentPage - 1) * pagesize).SetMaxResults(pagesize);
                    result = hqlQuery.List<TModel>();
                }
                if (closeSession)
                {
                    session.Close();
                }
            }
            catch (System.Exception)
            {
                session.Clear();
                session.Close();
                throw;
            }
            return result;
        }

        #endregion

        #region Count

        /// <summary>
        /// GetCount
        /// </summary>
        /// <param name="where"></param>
        /// <param name="session"></param>
        /// <param name="closeSession"></param>
        /// <returns></returns>
        public override int GetCount(string where, ref ISession session, bool closeSession = true)
        {
            int result;
            try
            {
                //hql
                var hql = string.Format("select count(*) from  {0} {1} ", EntityName,
                    string.IsNullOrEmpty(where) ? "" : " where " + where);
                var hqlTotlaResordQuery = session.CreateQuery(hql);
                var totalRecords = hqlTotlaResordQuery.UniqueResult<long>();
                 result = Convert.ToInt32(totalRecords);

                 session.Clear();
                 if (closeSession)
                 {
                     session.Close();
                 }
            }
            catch (System.Exception)
            {
                session.Clear();
                session.Close();
                throw;
            }

            return result;
        }

        #endregion

        #endregion
    }
}
