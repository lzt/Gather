﻿/*
* Code By 卢志涛
*/

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Generic;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace;
using Operate.DataBase.SqlDatabase.DataBaseAdapter.Base;
using Operate.DataBase.SqlDatabase.Model;
using Operate.Exception;
using Operate.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Base
{
    /// <summary>
    /// NhibernateDal基类
    /// </summary>
    public abstract class NhBaseDal<T> : BaseDal<T>, INhBaseDal<T> where T : BaseEntity, new()
    {
        #region Field

        private bool _disposed;

        /// <summary>
        /// Session
        /// </summary>
        protected ISession Session;

        #endregion

        #region properties

        /// <summary>
        /// AssemblyPath
        /// </summary>
        public string AssemblyPath { get; set; }

        /// <summary>
        /// SqliteAssembly
        /// </summary>
        public Assembly DataBaseOperateAssembly { get; set; }

        /// <summary>
        /// 实例化NHibernate数据操作基础类
        /// </summary>
        public NHibernateHelper NhibernateHelper { get; set; }

        /// <summary>
        /// 当前数据库
        /// </summary>
        public override string Database
        {
            get { return Session != null ? Session.Connection.Database : ""; }
        }

        /// <summary>
        /// 配置文件路径
        /// </summary>
        public string NhibernateCfgFilePath { get; set; }

        /// <summary>
        /// Nibernate配置文件
        /// </summary>
        public NibernateConfig Config
        {
            get { return NhibernateHelper.Config; }
            set
            {
                NhibernateHelper.Config = value;
            }
        }

        /// <summary>
        /// MappingCollection
        /// </summary>
        public NameValueCollection MappingCollection { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        protected NhBaseDal(object configContent, ConstantCollection.ConfigFileType configFileType):base()
        {
            MappingCollection = new NameValueCollection();
            if (ModeType.FullName!=typeof(BaseEntity).FullName)
            {
                var typelist = ModeType.Assembly.GetTypes();
                foreach (var type in typelist)
                {
                    // ReSharper disable once UseNullPropagation
                    if (type.BaseType != null)
                    {
                        // ReSharper disable once UseNullPropagation
                        if (type.BaseType.BaseType != null)
                        {
                            if (type.BaseType.BaseType.BaseType != null)
                            {
                                if (type.BaseType.BaseType.BaseType.Name == "BaseEntity")
                                {
                                    var o = (BaseEntity)(type.Create());
                                    MappingCollection.Add(type.Name, o.CreateMappingXml());
                                }
                            }
                        }
                    }
                }
            }

            Init(configContent, configFileType);
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        protected NhBaseDal(object configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
        {
            MappingCollection = new NameValueCollection();
            if (mappingName != null && mappingXml != null)
            {
                MappingCollection.Add(mappingName, mappingXml);
            }

            Init(configContent, configFileType);
        }

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="configContent"></param>
        /// <param name="configFileType"></param>
        private void Init(object configContent, ConstantCollection.ConfigFileType configFileType)
        {
            var configCon = "";
            switch (configFileType)
            {
                case ConstantCollection.ConfigFileType.ConfigContent:
                    configCon = configContent as string;
                    break;
                case ConstantCollection.ConfigFileType.OutXml:
                    var configPath = configContent as string;
                    configCon = LoadConfigFromXmlDocument(configPath);
                    break;
            }
            if (!configCon.IsNullOrEmpty())
            {
                SetConfig(configCon);
            }

            FieldProtectionLeft = Config.ConfigInfo.FieldProtectionLeft;
            FieldProtectionRight = Config.ConfigInfo.FieldProtectionLeft;

            GenericDalExecutor = new NhGenericDal();
        }

        #endregion

        #region Method

        #region Function

        /// <summary>
        /// 填充配置文件
        /// </summary>
        /// <param name="config">配置文件内容</param>
        private void SetConfig(string config)
        {
            if (config.IsNullOrEmpty())
            {
                throw new EmptyException();
            }

            NhibernateHelper = new NHibernateHelper(config, MappingCollection);
            Session = NhibernateHelper.GetSession();

            Config.ConfigInfo.ConnectionStringChanged += ConfigInfo_ConnectionStringChanged;
            Config.ConfigInfo.AssemblyChanged += ConfigInfo_AssemblyChanged;
        }

        /// <summary>
        /// ConfigInfo_AssemblyChanged
        /// 需完善
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConfigInfo_AssemblyChanged(object sender, System.EventArgs e)
        {
            Session = NhibernateHelper.GetSession();
        }

        /// <summary>
        /// ConfigInfo_ConnectionStringChanged
        /// 需完善
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConfigInfo_ConnectionStringChanged(object sender, System.EventArgs e)
        {
            Session = NhibernateHelper.GetSession();
        }

        /// <summary>
        /// 关闭操纵类当前Session
        /// </summary>
        /// <returns></returns>
        public void CloseCurrentSession()
        {
            if (Session.IsOpen)
            {
                Session.Clear();
                Session.Close();
            }
        }

        /// <summary>
        /// 获取操纵类当前Session
        /// </summary>
        /// <returns></returns>
        public ISession GetCurrentSession()
        {
            if (!Session.IsOpen)
            {
                Session.Dispose();
                return NhibernateHelper.GetSession();
            }
            return Session;
        }

        #endregion

        #region GetOne

        /// <summary>
        /// 根据主键获取数据实体
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>数据实体</returns>
        public virtual T GetByPrimarykey(object primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetByPrimarykey<T>(primaryValue, ref Session);
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                return GetByPrimarykeyBySql(primaryValue);
            }
        }

        /// <summary>
        /// 通过条件获取
        /// </summary>
        /// <param name="where"></param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual T GetByWhere(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetByWhere<T>(where, ref Session);
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                return GetByWhereBySql(where);
            }
        }

        #endregion

        #region GetList

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <returns>实体列表</returns>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql();
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="listId">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(List<object> listId, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(listId, ref Session, true, mode);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                var objlist = listId.ToList();
                return GetListBySql(objlist, mode);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, List<object> listId, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, listId, ref Session, true, mode);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                var objlist = listId.ToList();
                return GetListBySql(currentPage, pagesize, objlist, mode);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalRecords">总计路数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="totalPages">总页面数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, out totalPages, out totalRecords, listId, ref Session, true, mode);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                var objlist = listId.ToList();
                return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, objlist, mode);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        public virtual IList<T> GetList(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(where, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(where);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        public virtual IList<T> GetList(List<object> listId, string where, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(listId, where, ref Session, true, mode);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                var objlist = listId.ToList();
                return GetListBySql(objlist, where, mode);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, List<object> listId, string where, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, listId, where, ref Session, true, mode);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                var objlist = listId.ToList();
                return GetListBySql(currentPage, pagesize, objlist, where, mode);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalPages">总页数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, string where, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, out totalPages, out totalRecords, listId, where, ref Session, true, mode);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                var objlist = listId.ToList();
                return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, objlist, where, mode);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="sort">排序语句</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        public virtual IList<T> GetListBySort(string sort, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(" 1=1 ", sort, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(" 1=1 ", sort);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="sort">排序语句</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        public virtual IList<T> GetList(string where, string sort, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(where, sort, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(where, sort);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(currentPage, pagesize);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="where"></param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, string where, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, where, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(currentPage, pagesize, where);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, out totalPages, out totalRecords, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 返回分页列表页
        ///  </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, out totalPages, out totalRecords, where, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, @where);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, out totalPages, out totalRecords, where, sort, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, @where, sort);
            }
            throw new UnknownException();
        }

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, string group, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetList<T>(currentPage, pagesize, out totalPages, out totalRecords, where, sort, group, ref Session);
            }
            if (operateMode == DataOperateMode.CustomSql)
            {
                return GetListBySql(currentPage, pagesize, out totalPages, out totalRecords, @where, sort, @group);
            }
            throw new UnknownException();
        }

        #endregion

        #region Insert

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItem">将要插入的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Insert(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.Insert(modelItem, ref Session);
            }
            else if (operateMode == DataOperateMode.CustomSql)
            {
                InsertBySql(modelItem);
            }

            OnAysncDeleteFinished(ar);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelJson">将要插入的数据实体Json</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Insert(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.Insert<T>(modelJson, ref Session);
            }
            else if (operateMode == DataOperateMode.CustomSql)
            {
                InsertBySql(modelJson);
            }

            OnAysncDeleteFinished(ar);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Insert(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            foreach (var model in modelItemList)
            {
                Insert(model, operateMode, ar);
            }
            //Session = GetCurrentSession();
            //if (operateMode == DataOperateMode.Nhibernate)
            //{
            //    GenericDalExecutor.Insert(modelItemList, ref  Session);
            //}
            //else if (operateMode == DataOperateMode.CustomSql)
            //{
            //    InsertBySql(modelItemList);
            //}

            //OnAysncDeleteFinished(ar);
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Update(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.Update(modelItem, ref Session);
            }
            else
            {
                UpdateBySql(modelItem);
            }

            OnUpdateFinished(ar);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据Json</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Update(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.Update<T>(modelJson, ref Session);
            }
            else
            {
                UpdateBySql(modelJson);
            }

            OnUpdateFinished(ar);
        }

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Update(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            foreach (var model in modelItemList)
            {
                Update(model, operateMode, ar);
            }
            //Session = GetCurrentSession();
            //if (operateMode == DataOperateMode.Nhibernate)
            //{
            //    GenericDalExecutor.Update(modelItemList, ref  Session);
            //}
            //else if (operateMode == DataOperateMode.CustomSql)
            //{
            //    UpdateBySql(modelItemList);
            //}

            //OnUpdateFinished(ar);
        }

        #endregion

        #region Delete

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="primaryValue">即将要删除的数据主键</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void DeleteByPrimarykey(object primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.DeleteByPrimarykey<T>(primaryValue, ref Session);
            }
            else
            {
                DeleteByPrimarykeyBySql(primaryValue);
            }

            OnDeleteFinished(ar);
        }

        /// <summary>
        /// 通过主键批量删除
        /// </summary>
        /// <param name="primaryValueList">主键值集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual void DeleteByPrimarykeyList(List<object> primaryValueList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            foreach (var val in primaryValueList)
            {
                DeleteByPrimarykey(val, operateMode, ar);
            }

            //Session = GetCurrentSession();
            //if (operateMode == DataOperateMode.Nhibernate)
            //{
            //    GenericDalExecutor.DeleteByPrimarykeyList<T>(primaryValueList, ref  Session);
            //    return true;
            //}
            //else
            //{
            //    return DeleteByPrimarykeyListBySql(primaryValueList);
            //}
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Delete(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.Delete(modelItem, ref Session);
            }
            else
            {
                DeleteBySql(modelItem);
            }

            OnDeleteFinished(ar);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelJson">即将要删除的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Delete(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.Delete<T>(modelJson, ref Session);
            }
            else
            {
                DeleteBySql(modelJson);
            }

            OnDeleteFinished(ar);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItemList">将要删除的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        public virtual void Delete(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            foreach (var model in modelItemList)
            {
                Delete(model, operateMode, ar);
            }
        }

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public virtual void DeleteByWhere(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                GenericDalExecutor.DeleteByWhere(where, ref Session);
            }
            else
            {
                DeleteByWhereBySql(where);
            }

            OnCustomSqlFinished(ar);
        }

        #endregion

        #region Count

        /// <summary>
        /// 根据条件获取结果数
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        public virtual int GetCount(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.GetCount(where, ref Session);
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                return GetCountBySql(where);
            }
        }


        #endregion

        #region ExistByProperty

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>数据实体</returns>
        public virtual bool Exist(int primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.Exist<T>(primaryValue, ref Session);
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                return ExistBySql(primaryValue);
            }
        }

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>数据实体</returns>
        public virtual bool Exist(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate)
        {
            Session = GetCurrentSession();
            if (operateMode == DataOperateMode.Nhibernate)
            {
                return GenericDalExecutor.Exist<T>(where, ref Session);
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                return ExistBySql(where);
            }
        }

        #endregion

        #region RunSql

        /// <summary>
        /// 执行Sql语句 , 返回第一行第一列
        /// </summary>
        /// <param name="sql">存储过程名</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        public override object ExecuteSqlScalar(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            using (ITransaction transaction = Session.BeginTransaction())
            {
                try
                {
                    var command = Session.Connection.CreateCommand();
                    transaction.Enlist(command); //注意此处要把command添加到事物中
                    command.CommandText = sql;
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    var result = command.ExecuteScalar();
                    transaction.Commit();

                    OnCustomSqlFinished(ar);

                    return result;
                }
                catch (System.Exception)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    Session.Clear();
                    Session.Close();
                }
            }
        }

        /// <summary>
        /// 执行Sql语句  无返回结果
        /// </summary>
        /// <param name="sql">存储过程名</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        public override void ExecuteSqlNonQuery(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            using (ITransaction transaction = Session.BeginTransaction())
            {
                try
                {
                    var command = Session.Connection.CreateCommand();
                    transaction.Enlist(command); //注意此处要把command添加到事物中
                    command.CommandText = sql;
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    command.ExecuteNonQuery();
                    transaction.Commit();

                    OnCustomSqlFinished(ar);
                }
                catch (System.Exception)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    Session.Clear();
                    Session.Close();
                }
            }
        }

        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            DataSet ds;
            IDbCommand command = null;
            try
            {
                command = Session.Connection.CreateCommand();
                command.CommandText = sql;
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        command.Parameters.Clear();
                        foreach (var dataParameter in parameters)
                        {
                            if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                continue;
                            command.Parameters.Add(dataParameter);
                        }
                    }
                }
                var dr = command.ExecuteReader();
                if (dr.Read())
                {
                    ds = DataConvert.ConvertToDataSet(dr); //定义一个DataSet  对象接受执行结果
                    dr.Close();
                }
                else
                {
                    ds = null;
                }

                if (command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }

                OnCustomSqlFinished(ar);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if (command != null && command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }
                Session.Clear();
                Session.Close();
            }

            return ds;
        }

        #endregion

        #region RunProcess

        /// <summary>
        /// 执行存储过程（附带参数）  无返回结果
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        public override void ExecuteProcessNonQuery(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            using (ITransaction transaction = Session.BeginTransaction())
            {
                try
                {
                    var command = Session.Connection.CreateCommand();
                    transaction.Enlist(command); //注意此处要把command添加到事物中
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = processName;
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    command.ExecuteNonQuery();
                    transaction.Commit();

                    OnProcessFinished(ar);
                }
                catch (System.Exception)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    Session.Clear();
                    Session.Close();
                }
            }
        }

        /// <summary>
        /// 执行存储过程（附带参数）  无返回结果
        /// </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        public override object ExecuteProcessScalar(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            using (ITransaction transaction = Session.BeginTransaction())
            {
                try
                {
                    var command = Session.Connection.CreateCommand();
                    transaction.Enlist(command); //注意此处要把command添加到事物中
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = processName;
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    var result = command.ExecuteScalar();
                    transaction.Commit();

                    OnProcessFinished(ar);

                    return result;
                }
                catch (System.Exception)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    Session.Clear();
                    Session.Close();
                }
            }
        }

        #endregion

        #region Script

        /// <summary>
        /// 通过SchemaExport获取创建脚本
        /// </summary>
        /// <returns></returns>
        public string GetCreateScriptBySchemaExport(SqlDatabase.ConstantCollection.DataEntityType type)
        {
            var cfg = NhibernateHelper.GetCfg();
            var export = new SchemaExport(cfg);
            var session = GetCurrentSession();

            var bu = new StringBuilder();
            var tw = new StringWriter(bu);
            export.Execute(true, true, false, session.Connection, tw);
            var contentSql = bu.ToString();
            tw.Close();

            Session.Clear();
            Session.Close();

            return contentSql;
        }

        #endregion

        #endregion

        #region Dispose

        /// <summary>
        /// 释放资源
        /// 可以被客户直接调用
        /// </summary>
        public void Dispose()
        {
            //必须以Dispose(true)方式调用,以true告诉Dispose(bool disposing)函数是被客户直接调用的
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose
        /// 首先检查是否被dispose过了，接下来判断是用户调用，还是系统调用
        ///如果 disposing 是 true, 那么这个方法是被客户调用的,那么托管的,和非托管的资源都可以释放
        ///如果 disposing 是 false, 那么函数是从垃圾回收器在调用Finalize时调用的,此时不应当引用其他托管对象所以,只能释放非托管资源
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                // 那么这个方法是被客户直接调用的,那么托管的,和非托管的资源都可以释放
                if (disposing)
                {
                }

                //释放非托管资源
                if (NhibernateHelper != null)
                {
                    NhibernateHelper.CloseSessionCreator();
                }

                _disposed = true;
            }
        }

        #endregion

        #region Destructor

        /// <summary>
        /// 析构函数自动生成 Finalize 方法和对基类的 Finalize 方法的调用.默认情况下,  
        /// 一个类是没有析构函数的,也就是说,对象被垃圾回收时不会被调用Finalize方法   
        /// </summary>
        ~NhBaseDal()
        {
            // 为了保持代码的可读性性和可维护性,千万不要在这里写释放非托管资源的代码
            // 必须以Dispose(false)方式调用,以false告诉Dispose(bool disposing)函数是从垃圾回收器在调用Finalize时调用的
            Dispose(false);
        }

        #endregion
    }
}
