﻿/*
* Code By 卢志涛
* 版本1.5.1.1
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Base;
using Operate.DataBase.SqlDatabase.Model;
using Operate.Exception;
using Operate.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter
{
    /// <summary>
    /// SqlServer基础处理类
    /// </summary>
    public class SqlServerBaseDal : SqlServerBaseDal<BaseEntity>
    {
        #region Constructors

        /// <summary>
        /// 实例化SqlServerBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        public SqlServerBaseDal(string hibernateCfgFilePath)
            : base(hibernateCfgFilePath)
        {
        }

        /// <summary>
        /// 实例化SqlServerBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqlServerBaseDal(string hibernateCfgFilePath, string mappingName , string mappingXml )
            : base(hibernateCfgFilePath, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// 实例化SqlServerBaseDal
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        public SqlServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
        }

        /// <summary>
        /// 实例化SqlServerBaseDal
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqlServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName , string mappingXml )
            : base(configContent, configFileType, mappingName, mappingXml)
        {
        }

        #endregion
    }

    /// <summary>
    /// SqlServer基础处理类
    /// </summary>
    public class SqlServerBaseDal<T> : NhBaseDal<T> where T : BaseEntity, new()
    {
        #region Field

        //// 已经被处理过的标记
        //private bool _alreadyDisposed;

        #endregion

        #region Constructors

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        public SqlServerBaseDal(string hibernateCfgFilePath)
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqlServerBaseDal(string hibernateCfgFilePath, string mappingName , string mappingXml )
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// 实例化SqlServerBaseDal
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        public SqlServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
        }

        /// <summary>
        /// 实例化SqlServerBaseDal
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqlServerBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName , string mappingXml )
            : base(configContent, configFileType, mappingName, mappingXml)
        {
        }

        #endregion

        #region Insert

        /// <summary>
        ///  使用sql语句插入信息
        /// </summary>
        /// <param name="modelItem">数据模型</param>
        /// <param name="ar"></param>
        protected override void InsertBySql(T modelItem, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var builder = new StringBuilder();
            //builder.AppendLine("SET IDENTITY_INSERT {0} ON ".FormatValue(EntityName));
            builder.AppendLine(modelItem.GetInsertSql());
            builder.AppendLine("SELECT @@IDENTITY");
            // builder.AppendLine("SET IDENTITY_INSERT {0} OFF".FormatValue(EntityName));

            var id = Convert.ToInt32(ExecuteSqlScalar(builder.ToString(), null, ar));
            // ReSharper disable once RedundantAssignment
            modelItem = GetByPrimarykey(id);
        }

        #endregion

        #region Update

        /// <summary>
        /// 使用sql语句更新信息
        /// </summary>
        /// <param name="modelItem">数据模型</param>
        /// <param name="ar"></param>
        protected override void UpdateBySql(T modelItem, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var builder = new StringBuilder();
            //builder.AppendLine("SET IDENTITY_INSERT {0} ON ".FormatValue(EntityName));
            builder.AppendLine(modelItem.GetUpdateSql());
            builder.AppendLine("SELECT @@ROWCOUNT ");
            // builder.AppendLine("SET IDENTITY_INSERT {0} OFF".FormatValue(EntityName));

            var id = Convert.ToInt32(ExecuteSqlScalar(builder.ToString(), null, ar));
            // ReSharper disable once RedundantAssignment
            if (id == 0)
            {
                throw new System.Exception("更新操作未成功");
            }
        }

        #endregion

        #region GetList

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <returns></returns>
        protected override IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, string group)
        {
            //Session = GetCurrentSession();
            throw new NeedRewriteException("protected override IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, string group)");
        }

        #endregion

        #region RunSql

        /// <summary>
        /// CreateDbCommand
        /// </summary>
        /// <returns></returns>
        private IDbCommand CreateDbCommand()
        {
            Session = GetCurrentSession();
            IDbCommand command;
            if (Session == null)
            {
                if (string.IsNullOrEmpty(Config.ConnectionString))
                {
                    throw new System.Exception("ConnectionString is null");
                }
                command = new SqlCommand { Connection = new SqlConnection(Config.ConnectionString) };
            }
            else
            {
                command = Session.Connection.CreateCommand();
            }
            return command;
        }

        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            using (IDbCommand command = CreateDbCommand())
            {
                try
                {
                    var ds = new DataSet();
                    command.CommandText = sql;
                    // ReSharper disable once UseNullPropagation
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    var da = new SqlDataAdapter(command as SqlCommand);
                    da.Fill(ds);
                    command.Connection.Close();

                    OnCustomSqlFinished(ar);

                    return ds;
                }
                catch (System.Exception)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                    throw;
                }
                finally
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                    Session.Clear();
                    Session.Close();
                }
            }
        }

        #endregion

        #region RunProcess

        ///  <summary>
        ///  执行存储过程（附带参数）DataSet
        ///  </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DataSet ExecuteProcessToDataset(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            using (IDbCommand command = CreateDbCommand())
            {
                try
                {
                    var ds = new DataSet();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = processName;
                    // ReSharper disable once UseNullPropagation
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    var da = new SqlDataAdapter(command as SqlCommand);
                    da.Fill(ds);
                    command.Connection.Close();

                    OnProcessFinished(ar);

                    return ds;
                }
                catch (System.Exception)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                    throw;
                }
                finally
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                    Session.Clear();
                    Session.Close();
                }
            }
        }

        #endregion

        #region Async

        /// <summary>
        /// 描述：异步执行Insert
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="modelChatLog">数据模型</param>
        /// <param name="ar"></param>
        public override bool AsyncInsert(T modelChatLog, IAsyncResult ar = null)
        {
            return AsyncInsert(modelChatLog, false, ar);
        }

        /// <summary>
        /// 描述：异步执行Insert
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="modelChatLog">数据模型</param>
        /// <param name="executeAysncInsertFinished">是否执行AysncInsertFinished</param>
        /// <param name="ar"></param>
        public override bool AsyncInsert(T modelChatLog, bool executeAysncInsertFinished, IAsyncResult ar = null)
        {
            var cmsString = modelChatLog.GetInsertSql();
            var sqlCon = new SqlConnection(Config.ConnectionString);
            var sqlCmdOrders = new SqlCommand(cmsString, sqlCon);
            sqlCon.Open();

            if (executeAysncInsertFinished)
            {
                sqlCmdOrders.BeginExecuteReader(OnAysncInsertFinished, sqlCmdOrders, CommandBehavior.CloseConnection);
            }
            else
            {
                sqlCmdOrders.BeginExecuteReader(CommandBehavior.CloseConnection);
            }
            return true;
        }

        /// <summary>
        /// 描述：异步执行Update
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="modelChatLog">数据模型</param>
        /// <param name="ar"></param>
        public override bool AsyncUpdate(T modelChatLog, IAsyncResult ar = null)
        {
            return AsyncUpdate(modelChatLog, false, ar);
        }

        /// <summary>
        /// 描述：异步执行Update
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="modelChatLog">数据模型</param>
        /// <param name="executeAysncUpdateFinished">是否执行AysncUpdateFinished</param>
        /// <param name="ar"></param>
        public override bool AsyncUpdate(T modelChatLog, bool executeAysncUpdateFinished, IAsyncResult ar = null)
        {
            var cmsString = modelChatLog.GetUpdateSql();
            var sqlCon = new SqlConnection(Config.ConnectionString);
            var sqlCmdOrders = new SqlCommand(cmsString, sqlCon);
            sqlCon.Open();

            if (executeAysncUpdateFinished)
            {
                sqlCmdOrders.BeginExecuteReader(OnAysncUpdateFinished, sqlCmdOrders, CommandBehavior.CloseConnection);
            }
            else
            {
                sqlCmdOrders.BeginExecuteReader(CommandBehavior.CloseConnection);
            }
            return true;
        }

        /// <summary>
        /// 描述：异步执行Delete
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="modelChatLog">数据模型</param>
        /// <param name="ar"></param>
        public override bool AsyncDelete(T modelChatLog, IAsyncResult ar = null)
        {
            return AsyncDelete(modelChatLog, false, ar);
        }

        /// <summary>
        /// 描述：异步执行Delete
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="modelChatLog">数据模型</param>
        /// <param name="executeAysncDeleteFinished">是否执行AysncUpdateFinished</param>
        /// <param name="ar"></param>
        public override bool AsyncDelete(T modelChatLog, bool executeAysncDeleteFinished, IAsyncResult ar = null)
        {
            var cmsString = modelChatLog.GetDeleteSql();
            var sqlCon = new SqlConnection(Config.ConnectionString);
            var sqlCmdOrders = new SqlCommand(cmsString, sqlCon);
            sqlCon.Open();

            if (executeAysncDeleteFinished)
            {
                sqlCmdOrders.BeginExecuteReader(OnAysncDeleteFinished, sqlCmdOrders, CommandBehavior.CloseConnection);
            }
            else
            {
                sqlCmdOrders.BeginExecuteReader(CommandBehavior.CloseConnection);
            }
            return true;
        }

        /// <summary>
        /// 描述：异步执行sql语句
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        public override bool AsyncCustom(string sql, IDataParameter[] parameters, IAsyncResult ar = null)
        {
            return AsyncCustom(sql, false, parameters, ar);
        }

        /// <summary>
        /// 描述：异步执行sql语句
        /// 要点：使用 ADO.NET 2.0 的异步进程 ,  数据库连接字符串要添加 Asynchronous Processing=true
        ///           如果执行多条命令连接字符串还要添加 MultipleActiveResultSets=true
        /// </summary>
        /// <param name="sql">自定义sql语句</param>
        /// <param name="executeAysncCustomFinished">是否执行AysncCustomFinished</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        public override bool AsyncCustom(string sql, bool executeAysncCustomFinished, IDataParameter[] parameters, IAsyncResult ar = null)
        {
            var cmsString = sql;
            var sqlCon = new SqlConnection(Config.ConnectionString);
            var sqlCmdOrders = new SqlCommand(cmsString, sqlCon);
            // ReSharper disable once UseNullPropagation
            if (parameters != null)
            {
                if (parameters.Length > 0)
                {
                    sqlCmdOrders.Parameters.Clear();
                    foreach (var dataParameter in parameters)
                    {
                        if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input) continue;
                        sqlCmdOrders.Parameters.Add(dataParameter);
                    }
                }
            }
            sqlCon.Open();

            if (executeAysncCustomFinished)
            {
                sqlCmdOrders.BeginExecuteReader(OnAysncCustomFinished, sqlCmdOrders, CommandBehavior.CloseConnection);
            }
            else
            {
                sqlCmdOrders.BeginExecuteReader(CommandBehavior.CloseConnection);
            }
            return true;
        }

        #endregion

        #region Script

        /// <summary>
        /// 获取创建脚本
        /// </summary>
        /// <returns></returns>
        public override string GetCreateScript(SqlDatabase.ConstantCollection.DataEntityType type)
        {
            var typeValue = "";
            if (type == SqlDatabase.ConstantCollection.DataEntityType.DataTable)
            {
                typeValue = "U";
            }

            if (type == SqlDatabase.ConstantCollection.DataEntityType.DataView)
            {
                typeValue = "V";
            }

            if (typeValue == "")
            {
                throw new System.Exception("暂时不支持除表和视图之外的其他实体");
            }
            var sql = "select text From syscomments  Where id in (select id from sysobjects  where  xtype='{0}' And name = '{1}')".FormatValue(typeValue, EntityName);

            var result = Convert.ToString(ExecuteSqlScalar(sql));
            return result;
        }

        #endregion

        #region DataBaseParam

        /// <summary>
        /// CreateNewParam
        /// </summary>
        /// <returns></returns>
        public override IDataParameter CreateNewParam()
        {
            return new SqlParameter();
        }

        #endregion
    }
}
