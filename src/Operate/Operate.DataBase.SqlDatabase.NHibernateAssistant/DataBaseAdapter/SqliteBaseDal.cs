﻿/*
* Code By 卢志涛
* 版本1.5.1.1
*/

using System;
using System.Data;
using System.IO;
using System.Reflection;
using Operate.DataBase.SqlDatabase.Model;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Base;
using Operate.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter
{
    /// <summary>
    /// Sqlite基础处类
    /// </summary>
    public class SqliteBaseDal : SqliteBaseDal<BaseEntity>
    {
        #region Constructors

        /// <summary>
        /// SqliteBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        public SqliteBaseDal(string hibernateCfgFilePath)
            : base(hibernateCfgFilePath)
        {
        }

        /// <summary>
        /// SqliteBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqliteBaseDal(string hibernateCfgFilePath, string mappingName, string mappingXml)
            : base(hibernateCfgFilePath, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// SqliteBaseDal
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        public SqliteBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
        }

        /// <summary>
        /// SqliteBaseDal
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqliteBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
            : base(configContent, configFileType, mappingName, mappingXml)
        {
        }

        #endregion
    }

    /// <summary>
    /// Sqlite基础处类
    /// </summary>
    public class SqliteBaseDal<T> : NhBaseDal<T> where T : BaseEntity, new()
    {
        #region properties

        /// <summary>
        /// 当前数据库
        /// </summary>
        public override string Database
        {
            get
            {
                //var ccc = new System.Data.SQLite.SQLiteConnectionStringBuilder(Session.Connection.ConnectionString);
                //var dataSourceString = ccc.DataSource;
                //var a = dataSourceString.Split('\\');
                //if (a.Length>0)
                //{
                //    return  a[a.Length - 1];
                //}
                //return "";

                //var ccc = new System.Data.SQLite.SQLiteConnection(Session.Connection.ConnectionString);

                var connectionStringBuilder = DataBaseOperateAssembly.CreateInstance("System.Data.SQLite.SQLiteConnectionStringBuilder", false, BindingFlags.CreateInstance, null, new object[] { Session.Connection.ConnectionString }, null, null);
                // ReSharper disable once MergeConditionalExpression
                var dataSourceString = connectionStringBuilder != null ? connectionStringBuilder.GetType().GetProperty("DataSource").GetValue(connectionStringBuilder, null).ToString() : "";
                if (dataSourceString != "")
                {
                    var a = dataSourceString.Split('\\');
                    if (a.Length > 0)
                    {
                        var dbname = a[a.Length - 1].ToUpperFirst();
                        if (dbname.IndexOf('.') >= 0)
                        {
                            dbname = dbname.Remove(dbname.IndexOf('.'));
                        }
                        return dbname;
                    }
                }

                return dataSourceString;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        public SqliteBaseDal(string hibernateCfgFilePath)
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqliteBaseDal(string hibernateCfgFilePath, string mappingName, string mappingXml)
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        public SqliteBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configFileType == ConstantCollection.ConfigFileType.ConfigContent ? HandlerConfigContent(configContent) : configContent, configFileType)
        {
            var assemblyPath = GetAssemblyPath() + "System.Data.SQLite.dll";
            DataBaseOperateAssembly = Assembly.LoadFrom(assemblyPath);
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public SqliteBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
            : base(configFileType == ConstantCollection.ConfigFileType.ConfigContent ? HandlerConfigContent(configContent) : configContent, configFileType, mappingName, mappingXml)
        {
            var assemblyPath = GetAssemblyPath() + "System.Data.SQLite.dll";
            DataBaseOperateAssembly = Assembly.LoadFrom(assemblyPath);
        }

        #endregion

        #region HandlerConfigContent

        private static string HandlerConfigContent(string configContent)
        {
            var nhibernateHelper = new NHibernateHelper(configContent);
            var connList = nhibernateHelper.Config.ConnectionString.Split(';');

            for (int index = 0; index < connList.Length; index++)
            {
                var item = connList[index];
                if (item.ToLower().Contains("data source"))
                {
                    var pArray = item.Split('=');
                    if (pArray.Length == 2)
                    {
                        var path = Path.GetFullPath(pArray[1]);
                        path = Path.GetFullPath(path);
                        connList[index] = pArray[0] + "=" + path;
                    }
                }
            }

            return NibernateConfig.ChangeConnectionToXmlConfig(configContent, connList.Join(";"));
        }

        #endregion

        #region Insert

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="modelItem">数据模型</param>
        /// <param name="ar"></param>
        protected override void InsertBySql(T modelItem, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var insertSql = modelItem.GetInsertSql();
            insertSql += " ; select last_insert_rowid() as no";
            var table = ExecuteSqlToDatatable(insertSql, null, ar);
            var id = Convert.ToInt32(table.Rows[0]["no"]);
            // ReSharper disable once RedundantAssignment
            modelItem = GetByPrimarykey(id);
        }

        #endregion

        #region RunProcess

        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters">参数集合</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var ds = new DataSet();
            IDbCommand command = null;
            try
            {
                command = Session.Connection.CreateCommand();
                command.CommandText = sql;
                // ReSharper disable once UseNullPropagation
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        command.Parameters.Clear();
                        foreach (var dataParameter in parameters)
                        {
                            if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                continue;
                            command.Parameters.Add(dataParameter);
                        }
                    }
                }
                var da = DataBaseOperateAssembly.CreateInstance("System.Data.SQLite.SQLiteDataAdapter", false, BindingFlags.CreateInstance,
                    null, new object[] { command }, null, null);
                if (da != null)
                {
                    var t = da.GetType();
                    MethodInfo mi = t.GetMethod("Fill", new Type[] { typeof(DataSet) }); //方法的名称
                    mi.Invoke(da, new object[] { ds });
                }
                if (command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }

                OnCustomSqlFinished(ar);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                // ReSharper disable once MergeSequentialChecks
                if (command != null && command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }
            }

            return ds;
        }

        #endregion

        #region RunProcess

        ///  <summary>
        ///  执行存储过程（附带参数）DataSet
        ///  </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DataSet ExecuteProcessToDataset(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var ds = new DataSet();
            IDbCommand command = null;
            try
            {
                command = Session.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = processName;
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        command.Parameters.Clear();
                        foreach (var dataParameter in parameters)
                        {
                            if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                continue;
                            command.Parameters.Add(dataParameter);
                        }
                    }
                }
                var da = DataBaseOperateAssembly.CreateInstance("System.Data.SQLite.SQLiteDataAdapter", false, BindingFlags.CreateInstance,
                    null, new object[] { command }, null, null);
                if (da != null)
                {
                    var t = da.GetType();
                    MethodInfo mi = t.GetMethod("Fill", new Type[] { typeof(DataSet) }); //方法的名称
                    mi.Invoke(da, new object[] { ds });
                }
                // ReSharper disable once UseNullPropagation
                if (command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }

                OnProcessFinished(ar);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                // ReSharper disable once MergeSequentialChecks
                if (command != null && command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }
            }

            return ds;
        }

        #endregion

        #region Script

        /// <summary>
        /// 获取创建脚本
        /// </summary>
        /// <returns></returns>

        public override string GetCreateScript(SqlDatabase.ConstantCollection.DataEntityType type)
        {
            var typeValue = "";
            if (type == SqlDatabase.ConstantCollection.DataEntityType.DataTable)
            {
                typeValue = "table";
            }

            if (type == SqlDatabase.ConstantCollection.DataEntityType.DataView)
            {
                typeValue = "view";
            }

            if (typeValue == "")
            {
                throw new System.Exception("暂时不支持除表和视图之外的其他实体");
            }
            var sql = "select sql From SQLITE_MASTER Where type = '{0}' and name = '{1}'".FormatValue(typeValue, EntityName);

            var result = Convert.ToString(ExecuteSqlScalar(sql));
            return result;
        }

        #endregion

        #region DataBaseParam

        /// <summary>
        /// CreateNewParam
        /// </summary>
        /// <returns></returns>
        public override IDataParameter CreateNewParam()
        {
            return (IDataParameter)DataBaseOperateAssembly.CreateInstance("System.Data.SQLite.SQLiteParameter");
        }

        #endregion
    }
}
