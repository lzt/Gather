﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using MySql.Data.MySqlClient;
using Operate.DataBase.SqlDatabase.Model;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter.Base;
using Operate.ExtensionMethods;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.DataBaseAdapter
{
    /// <summary>
    /// MySql
    /// </summary>
    public class MySqlBaseDal : MySqlBaseDal<BaseEntity>
    {
        #region Constructors

        /// <summary>
        /// MySqlBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        public MySqlBaseDal(string hibernateCfgFilePath)
            : base(hibernateCfgFilePath)
        {
        }

        /// <summary>
        /// MySqlBaseDal
        /// </summary>
        /// <param name="hibernateCfgFilePath">配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public MySqlBaseDal(string hibernateCfgFilePath, string mappingName, string mappingXml)
            : base(hibernateCfgFilePath, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// MySqlBaseDal
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        public MySqlBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
        }

        /// <summary>
        /// MySqlBaseDal
        /// </summary>
        /// <param name="configContent">配置内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public MySqlBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
            : base(configContent, configFileType, mappingName, mappingXml)
        {
        }

        #endregion
    }

    /// <summary>
    /// MySql
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MySqlBaseDal<T> : NhBaseDal<T> where T : BaseEntity, new()
    {
        #region Constructors

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        public MySqlBaseDal(string hibernateCfgFilePath)
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="hibernateCfgFilePath">hibernate配置文件路径</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public MySqlBaseDal(string hibernateCfgFilePath, string mappingName, string mappingXml)
            : this(hibernateCfgFilePath, ConstantCollection.ConfigFileType.OutXml, mappingName, mappingXml)
        {
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        public MySqlBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType)
            : base(configContent, configFileType)
        {
            var assemblyPath = GetAssemblyPath() + "MySql.Data.dll";
            DataBaseOperateAssembly = Assembly.LoadFrom(assemblyPath);
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="configContent">配置文件内容</param>
        /// <param name="configFileType">配置类型</param>
        /// <param name="mappingName">映射名称</param>
        /// <param name="mappingXml">映射文档</param>
        public MySqlBaseDal(string configContent, ConstantCollection.ConfigFileType configFileType, string mappingName, string mappingXml)
            : base(configContent, configFileType, mappingName, mappingXml)
        {
            var assemblyPath = GetAssemblyPath() + "MySql.Data.dll";
            DataBaseOperateAssembly = Assembly.LoadFrom(assemblyPath);
        }

        #endregion

        #region GetList

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <returns></returns>
        protected override IList<T> GetListBySql(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, string group)
        {
            Session = GetCurrentSession();
            try
            {
                //获取粗略分页，在大量数据下起作用
                long probablyRecordCount = 0;
                var table = ExecuteSqlToDatatable("show table status from grrb where name='{0}'".FormatValue(EntityName));
                if (table.Rows.Count > 0)
                {
                    var datatow = table.Rows[0];
                    if (datatow["auto_increment"] != DBNull.Value)
                    {
                        probablyRecordCount = Convert.ToInt64(datatow["auto_increment"]);
                    }
                }

                string hql;
                IList<T> result;
                if (currentPage == -1 && pagesize == -1)
                {
                    if (probablyRecordCount < 10000)
                    {
                        //不分页
                        //hql
                        hql = string.Format("select count(*) from  {0} {1} ", EntityName,
                            string.IsNullOrEmpty(where) ? "" : " where " + where);
                        var hqlTotlaResordQuery = Session.CreateQuery(hql);
                        totalRecords = hqlTotlaResordQuery.UniqueResult<long>();
                    }
                    else
                    {
                        totalRecords = probablyRecordCount;
                    }
                    totalPages = totalRecords > 0 ? 1 : 0;

                    //hql
                    hql = string.Format("from  {0} {1} {2} {3}", EntityName,
                        string.IsNullOrEmpty(where) ? "" : " where " + where,
                        string.IsNullOrEmpty(group) ? "" : " group by " + group,
                        string.IsNullOrEmpty(sort) ? "" : " order by " + sort);
                    var hqlQuery = Session.CreateQuery(hql);
                    result = hqlQuery.List<T>();
                }
                else
                {
                    if (probablyRecordCount < 10000)
                    {
                        //分业获取
                        //hql
                        hql = string.Format("select count(*) from  {0} {1} ", EntityName,
                            string.IsNullOrEmpty(where) ? "" : " where " + where);
                        var hqlTotlaResordQuery = Session.CreateQuery(hql);
                        totalRecords = hqlTotlaResordQuery.UniqueResult<long>();
                    }
                    else
                    {
                        totalRecords = probablyRecordCount;
                    }
                    totalPages = (totalRecords % pagesize == 0 ? totalRecords / pagesize : totalRecords / pagesize + 1);


                    //var sqlQueue =
                    //    Session.CreateSQLQuery(
                    //        "SELECT * FROM {0} WHERE {1} >= (SELECT temp.{1} FROM {0} temp {2} {5} {6} LIMIT {3}, 1) LIMIT {4}"
                    //            .FormatValue(EntityName, PrimarykeyName,
                    //                string.IsNullOrEmpty(where) ? "" : " where " + where, currentPage*pagesize, pagesize,
                    //                string.IsNullOrEmpty(group) ? "" : " group by " + group,
                    //                string.IsNullOrEmpty(sort) ? "" : " order by " + sort));
                    //sqlQueue.AddEntity(typeof(T));
                    //return sqlQueue.List<T>();

                    //hql
                    hql = string.Format("from  {0} {1} {2} {3}", EntityName,
                        string.IsNullOrEmpty(where) ? "" : " where " + where,
                        string.IsNullOrEmpty(group) ? "" : " group by " + group,
                        string.IsNullOrEmpty(sort) ? "" : " order by " + sort);
                    var hqlQuery =
                        Session.CreateQuery(hql).SetFirstResult((currentPage - 1) * pagesize).SetMaxResults(pagesize);
                    result = hqlQuery.List<T>();
                }

                return result;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                Session.Clear();
                Session.Close();
            }
        }

        #endregion

        #region RunSql

        ///  <summary>
        /// 执行Sql语句 返回DataSet
        ///  </summary>
        ///  <param name="sql">sql语句</param>
        /// <param name="parameters">参数集合</param>
        /// <returns></returns>
        public override DataSet ExecuteSqlToDataset(string sql, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var ds = new DataSet();
            IDbCommand command = null;
            try
            {
                if (Session == null)
                {
                    if (string.IsNullOrEmpty(Config.ConnectionString))
                    {
                        throw new System.Exception("ConnectionString is null");
                    }
                    command = (IDbCommand)DataBaseOperateAssembly.CreateInstance("MySqlCommand");
                    if (command != null)
                        command.Connection =
                            (IDbConnection)
                                DataBaseOperateAssembly.CreateInstance("MySqlConnection", true,
                                    BindingFlags.CreateInstance, null, new object[] { Config.ConnectionString }, null,
                                    null);
                }
                else
                {
                    command = Session.Connection.CreateCommand();
                }

                if (command != null)
                {
                    command.CommandText = sql;
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }

                    var adapter = new MySqlDataAdapter((MySqlCommand)command);
                    try
                    {
                        adapter.Fill(ds);
                        if (command.Connection != null)
                        {
                            if (command.Connection.State == ConnectionState.Open)
                            {
                                command.Connection.Close();
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        if (command.Connection != null)
                        {
                            if (command.Connection.State == ConnectionState.Open)
                            {
                                command.Connection.Close();
                            }
                        }
                        throw ex.InnerException;
                    }
                }

                OnCustomSqlFinished(ar);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if (command != null && command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }
                if (Session != null)
                {
                    Session.Clear();
                    Session.Close();
                }
            }
            return ds;
        }

        #endregion

        #region RunProcess

        ///  <summary>
        ///  执行存储过程（附带参数）DataSet
        ///  </summary>
        /// <param name="processName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DataSet ExecuteProcessToDataset(string processName, IDataParameter[] parameters = null, IAsyncResult ar = null)
        {
            Session = GetCurrentSession();
            var ds = new DataSet();
            IDbCommand command = null;
            try
            {
                if (Session == null)
                {
                    if (string.IsNullOrEmpty(Config.ConnectionString))
                    {
                        throw new System.Exception("ConnectionString is null");
                    }
                    command = (IDbCommand)DataBaseOperateAssembly.CreateInstance("MySqlCommand");
                    if (command != null)
                        command.Connection =
                            (IDbConnection)
                                DataBaseOperateAssembly.CreateInstance("MySqlConnection", true,
                                    BindingFlags.CreateInstance, null, new object[] { Config.ConnectionString }, null,
                                    null);
                }
                else
                {
                    command = Session.Connection.CreateCommand();
                }
                if (command != null)
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = processName;
                    if (parameters != null)
                    {
                        if (parameters.Length > 0)
                        {
                            command.Parameters.Clear();
                            foreach (var dataParameter in parameters)
                            {
                                if (dataParameter.Value == null && dataParameter.Direction == ParameterDirection.Input)
                                    continue;
                                command.Parameters.Add(dataParameter);
                            }
                        }
                    }
                    var da = DataBaseOperateAssembly.CreateInstance("MySql.Data.MySqlClient.MySqlDataAdapter", true,
                        BindingFlags.CreateInstance, null, new object[] { command }, null, null);
                    if (da != null)
                    {
                        MethodInfo mi = da.GetType().GetMethod("Fill", new[] { typeof(DataSet) }); //方法的名称
                        try
                        {
                            mi.Invoke(da, new object[] { ds });
                            if (command.Connection != null)
                            {
                                if (command.Connection.State == ConnectionState.Open)
                                {
                                    command.Connection.Close();
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            if (command.Connection != null)
                            {
                                if (command.Connection.State == ConnectionState.Open)
                                {
                                    command.Connection.Close();
                                }
                            }
                            throw ex.InnerException;
                        }
                    }
                }

                OnProcessFinished(ar);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if (command != null && command.Connection != null)
                {
                    if (command.Connection.State == ConnectionState.Open)
                    {
                        command.Connection.Close();
                    }
                }
                if (Session != null)
                {
                    Session.Clear();
                    Session.Close();
                }
            }

            return ds;
        }

        #endregion

        #region Script

        /// <summary>
        /// 获取创建脚本
        /// </summary>
        /// <returns></returns>
        [Obsolete("暂不支持", true)]
        public override string GetCreateScript(SqlDatabase.ConstantCollection.DataEntityType type)
        {
            throw new System.Exception("暂不支持");
        }

        #endregion

        #region DataBaseParam

        /// <summary>
        /// CreateNewParam
        /// </summary>
        /// <returns></returns>
        public override IDataParameter CreateNewParam()
        {
            return (IDataParameter)DataBaseOperateAssembly.CreateInstance("MySqlParameter");
        }

        #endregion
    }
}
