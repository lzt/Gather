﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Event
{
    /// <summary>
    /// SessionBeforeRunHandler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SessionBeforeRunHandler(object sender, System.EventArgs e);

    /// <summary>
    /// SessionClosedHandler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SessionClosedHandler(object sender, System.EventArgs e);
}
