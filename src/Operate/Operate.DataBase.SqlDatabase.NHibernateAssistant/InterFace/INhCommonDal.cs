﻿using NHibernate;
using Operate.DataBase.SqlDatabase.InterFace;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using System;
using System.Reflection;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace
{
    /// <summary>
    /// INhCommonDal
    /// </summary>
    public interface INhCommonDal : ICommon, IDisposable
    {
        #region properties

        /// <summary>
        /// AssemblyPath
        /// </summary>
        string AssemblyPath { get; set; }

        /// <summary>
        /// Assembly
        /// </summary>
        Assembly DataBaseOperateAssembly { get; set; }

        /// <summary>
        /// 实例化NHibernate数据操作基础类
        /// </summary>
        NHibernateHelper NhibernateHelper { get; set; }

        /// <summary>
        /// 配置文件路径
        /// </summary>
        string NhibernateCfgFilePath { get; set; }

        /// <summary>
        /// Nibernate配置文件
        /// </summary>
        NibernateConfig Config { get; set; }

        /// <summary>
        /// 实体名称
        /// </summary>
        string EntityName { get; set; }

        #endregion

        #region Method

        #region Insert

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelJson">将要插入的数据实体Json</param>
        /// <param name="operateMode"></param>
        /// <param name="ar"></param>
        void Insert(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        #endregion

        #region Update

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据Json</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Update(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        #endregion

        #region Delete

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="primaryValue">即将要删除的数据主键</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void DeleteByPrimarykey(object primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelJson">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Delete(string modelJson, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        /// <summary>
        /// 条件删除
        /// </summary>
        /// <param name="where">条件（不需要附带Where关键字）</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        /// <returns></returns>
        void DeleteByWhere(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        #endregion

        /// <summary>
        /// 关闭操纵类当前Session
        /// </summary>
        /// <returns></returns>
        void CloseCurrentSession();

        /// <summary>
        /// 获取操纵类当前Session
        /// </summary>
        /// <returns></returns>
        ISession GetCurrentSession();

        #endregion

        #region Script

        /// <summary>
        /// 通过SchemaExport获取创建脚本
        /// </summary>
        /// <returns></returns>
        string GetCreateScriptBySchemaExport(SqlDatabase.ConstantCollection.DataEntityType type);

        #endregion
    }
}
