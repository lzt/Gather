﻿using System;
using System.Collections.Generic;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.General;
using Operate.DataBase.SqlDatabase.InterFace;
using Operate.DataBase.SqlDatabase.Model;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace
{
    /// <summary>
    /// INhBaseDal
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface INhBaseDal<T> : IDal<T>, INhCommonDal where T : BaseEntity, new()
    {
        #region Method

        #region GetOne

        /// <summary>
        /// 根据主键获取数据实体
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>数据实体</returns>
        T GetByPrimarykey(object primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 通过条件获取
        /// </summary>
        /// <param name="where"></param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        T GetByWhere(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        #endregion

        #region GetList

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <returns>实体列表</returns>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="listId">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(List<object> listId, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, List<object> listId, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 根据主键集合获取列表
        /// </summary>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalRecords">总计路数</param>
        /// <param name="listId">主键列表</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">页码</param>
        /// <param name="totalPages">总页面数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        IList<T> GetList(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        IList<T> GetList(List<object> listId, string where, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        IList<T> GetList(int currentPage, int pagesize, List<object> listId, string where, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="totalRecords">总记录数</param>
        /// <param name="listId">主键集合</param>
        /// <param name="where">条件语句</param>
        /// <param name="mode">筛选模式</param>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pagesize">页面条数</param>
        /// <param name="totalPages">总页数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, List<object> listId, string where, DataBase.ConstantCollection.ListMode mode = DataBase.ConstantCollection.ListMode.Include, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="sort">排序语句</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        IList<T> GetListBySort(string sort, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="where">条件语句</param>
        /// <param name="sort">排序语句</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>实体列表</returns>
        IList<T> GetList(string where, string sort, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="where"></param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, string where, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 返回分页列表页
        ///  </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 返回分页列表页
        /// </summary>
        /// <param name="currentPage">当前页码（注意从零开始）</param>
        /// <param name="pagesize">每页条目数</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="where">条件</param>
        /// <param name="sort">排序</param>
        /// <param name="group">分组</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns></returns>
        IList<T> GetList(int currentPage, int pagesize, out long totalPages, out long totalRecords, string where, string sort, string group, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        #endregion

        #region Insert

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItem">将要插入的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Insert(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Insert(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        #endregion

        #region Update

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Update(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        /// <summary>
        /// 向数据库插入新的数据
        /// </summary>
        /// <param name="modelItemList">将要插入的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Update(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        #endregion

        #region Delete

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItem">即将要更新的数据实体</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Delete(T modelItem, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="modelItemList">将要删除的数据实体集合</param>
        /// <param name="operateMode">执行方式</param>
        /// <param name="ar"></param>
        void Delete(List<T> modelItemList, DataOperateMode operateMode = DataOperateMode.Nhibernate, IAsyncResult ar = null);

        #endregion

        #region ExistByProperty

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="primaryValue">主键值</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>数据实体</returns>
        bool Exist(int primaryValue, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        /// <summary>
        /// 根据主键判断数据是否存在
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="operateMode">执行方式</param>
        /// <returns>数据实体</returns>
        bool Exist(string where, DataOperateMode operateMode = DataOperateMode.Nhibernate);

        #endregion

        #endregion
    }
}
