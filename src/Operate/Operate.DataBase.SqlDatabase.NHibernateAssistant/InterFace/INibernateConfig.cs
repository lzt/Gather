﻿using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;
using Operate.DataBase.SqlDatabase.InterFace;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace
{
    /// <summary>
    /// Nibernate配置接口
    /// </summary>
    public interface INibernateConfig : IConfig
    {
        /// <summary>
        /// Config配置实体
        /// </summary>
        ConfigEntity ConfigInfo { get; set; }

        /// <summary>
        /// 加载配置文件
        /// </summary>
        /// <param name="xmlFilePath"></param>
        void Load(string xmlFilePath);
    }
}
