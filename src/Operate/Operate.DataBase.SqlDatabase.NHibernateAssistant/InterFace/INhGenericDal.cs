﻿/*
* Code By 卢志涛
*/

using Operate.DataBase.SqlDatabase.InterFace;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace
{
    /// <summary>
    /// NHerbernate数据处理接口
    /// </summary>
    public interface INhGenericDal : IGenericDal
    {
        #region properties

        /// <summary>
        /// 实体名称
        /// </summary>
        string EntityName { get; set; }

        #endregion
    }
}
