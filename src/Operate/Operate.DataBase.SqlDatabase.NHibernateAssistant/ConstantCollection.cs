﻿/*
* Code By 卢志涛
*/

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant
{
    /// <summary>
    /// 常量集合类
    /// </summary>
    public class ConstantCollection :SqlDatabase.ConstantCollection
    {
        /// <summary>
        /// Nhibernate的XML配置文档的名字空间
        /// </summary>
        public const string NhibernateConfigXmlNameSpace = "urn:nhibernate-configuration-2.2";

        /// <summary>
        ///  Nhibernate的ConnectionProvider 
        /// </summary>
        public const string NhibernateConfigConnectionProvider = "NHibernate.Connection.DriverConnectionProvider";

        /// <summary>
        /// Nhibernate的ProxyfactoryFactoryClass
        /// </summary>
        public const string NhibernateConfigProxyfactoryFactoryClass = "NHibernate.Bytecode.DefaultProxyFactoryFactory,NHibernate";

        /// <summary>
        /// Nhibernate的ShowSql 
        /// </summary>
        public const string NhibernateConfigShowSql = "false";

        /// <summary>
        /// Nhibernate的CommandTimeout
        /// </summary>
        public const string NhibernateConfigCommandTimeout= "10";

        /// <summary>
        /// Nhbernate中指定用ADO.Net的批量更新的数量
        /// </summary>
        public const string NhibernateConfigBatchSize = "10";

        /// <summary>
        /// Nhbernate中将NHibernate查询中的符号映射到SQL查询中的符号 (符号可能是函数名或常量名字).
        /// </summary>
        public const string NhibernateConfigQuerySubstitutions = "true 1, false 0, yes 'Y', no 'N'";


        /// <summary>
        /// 配置文件类型
        /// </summary>
        public enum ConfigFileType
        {
            /// <summary>
            /// 传入的Xml文本
            /// </summary>
            ConfigContent,

            /// <summary>
            /// 内嵌资源
            /// </summary>
            EmberResource,

            /// <summary>
            /// 程序集内嵌
            /// </summary>
            EmberAssembly,

            /// <summary>
            /// 外置Xml文件
            /// </summary>
            OutXml,
        }
    }
}
