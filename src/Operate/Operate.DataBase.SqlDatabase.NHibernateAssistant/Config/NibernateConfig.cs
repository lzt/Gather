﻿using System;
using System.Globalization;
using System.Text;
using System.Xml;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.InterFace;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config
{
    /// <summary>
    /// Nibernate配置
    /// </summary>
    public class NibernateConfig : INibernateConfig
    {
        #region Properties

        /// <summary>
        /// 配置xml
        /// </summary>
        private XmlDocument _configXml;

        /// <summary>
        /// 获取配置Xml
        /// </summary>
        public string ConfigXml
        {
            // ReSharper disable once MergeConditionalExpression
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _configXml == null ? "" : _configXml.OuterXml; }
        }

        /// <summary>
        /// 配置xml的名字控件管理器
        /// </summary>
        private XmlNamespaceManager _nsMgr;

        /// <summary>
        /// 名字控件键
        /// </summary>
        private const string Prefxkey = "nh";

        /// <summary>
        /// 配置实体
        /// </summary>
        public ConfigEntity ConfigInfo { get; set; }

        /// <summary>
        ///数据库连接字符串
        /// </summary>
        public string ConnectionString
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get{ return ConfigInfo.ConnectionString; }
        }

        #endregion

        #region Function

        /// <summary>
        /// 改变载入流中的配置文件的连接字符串
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="newConn"></param>
        public static string ChangeConnectionToXmlConfig(string xml, string newConn)
        {
            var configXml = new XmlDocument();
            configXml.LoadXml(xml);
            var nsMgr = new XmlNamespaceManager(configXml.NameTable);
            nsMgr.AddNamespace(Prefxkey, ConstantCollection.NhibernateConfigXmlNameSpace);
            var selectSingleNode = configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='connection.connection_string']", Prefxkey), nsMgr);
            if (selectSingleNode != null)
            {
                selectSingleNode.InnerText = newConn;
            }
            return configXml.InnerXml;
        }

        private string GetDialect()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='dialect']", Prefxkey), _nsMgr);
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;
        }

        private string GetConnectionProvider()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='connection.provider']", Prefxkey), _nsMgr);
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;
        }

        private string GetConnectionDriverClass()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='connection.driver_class']", Prefxkey), _nsMgr);
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;
        }

        private string GetConnectionString()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='connection.connection_string']", Prefxkey), _nsMgr);
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;
        }

        private string GetProxyFactoryFactoryClass()
        {

            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='proxyfactory.factory_class']", Prefxkey), _nsMgr);
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;

        }

        private bool GetShowSql()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='show_sql']", Prefxkey), _nsMgr);
            if (selectSingleNode != null)
            {
                return selectSingleNode.InnerText.ToLower() == "true";
            }
            return false;
        }

        private int GetCommandTimeout()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='command_timeout']", Prefxkey), _nsMgr);
            if (selectSingleNode != null)
            {
                int check;
                if (int.TryParse(selectSingleNode.InnerText, out check))
                {
                    return Convert.ToInt32(selectSingleNode.InnerText);
                }
                return -1;
            }
            return -1;
        }

        private int GetAdonetBatchSize()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='adonet.batch_size']", Prefxkey), _nsMgr);
            if (selectSingleNode != null)
            {
                int check;
                if (int.TryParse(selectSingleNode.InnerText, out check))
                {
                    return Convert.ToInt32(selectSingleNode.InnerText);
                }
                return -1;
            }
            return -1;
        }

        /// <summary>
        /// 将NHibernate查询中的符号映射到SQL查询中的符号 (符号可能是函数名或常量名字).
        /// </summary>
        /// <returns></returns>
        public string GetQuerySubstitutions()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:property[@name='query.substitutions']", Prefxkey), _nsMgr);
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;
        }

        private string GetAssembly()
        {
            var selectSingleNode = _configXml.SelectSingleNode(string.Format("/{0}:hibernate-configuration/{0}:session-factory/{0}:mapping", Prefxkey), _nsMgr);
            if (selectSingleNode != null)
                if (selectSingleNode.Attributes != null) return selectSingleNode.Attributes["assembly"].Value;
            return string.Empty;
        }

        private void SetConfigEntity()
        {
            ConfigInfo.Dialect = GetDialect();
            ConfigInfo.ConnectionProvider = GetConnectionProvider();
            ConfigInfo.ConnectionDriverClass = GetConnectionDriverClass();
            ConfigInfo.ConnectionString = GetConnectionString();
            ConfigInfo.ProxyfactoryFactoryClass = GetProxyFactoryFactoryClass();
            ConfigInfo.ShowSql = GetShowSql().ToString().ToLower();
            ConfigInfo.CommandTimeout = GetCommandTimeout().ToString(CultureInfo.InvariantCulture);
            ConfigInfo.BatchSize = GetAdonetBatchSize().ToString(CultureInfo.InvariantCulture);
            ConfigInfo.Assembly = GetAssembly();
        }

        #endregion


        #region Constructor

        /// <summary>
        ///指定要替换的关健字
        /// </summary>
        public NibernateConfig()
        {
            _configXml = null;
            ConfigInfo = new ConfigEntity("");
        }

        /// <summary>
        /// 指定要替换的关健字
        /// </summary>
        /// <param name="xml"></param>
        public NibernateConfig(string xml)
        {
            ConfigInfo = new ConfigEntity("");
            _configXml = new XmlDocument();
            _configXml.LoadXml(xml);
            _nsMgr = new XmlNamespaceManager(_configXml.NameTable);
            _nsMgr.AddNamespace(Prefxkey, ConfigInfo.Xmlns);
            SetConfigEntity();
        }

        #endregion

        #region Function

        /// <summary>
        /// 加载xml配置文件
        /// </summary>
        /// <param name="xmlFilePath"></param>
        public void Load(string xmlFilePath)
        {
            _configXml = new XmlDocument();
            _configXml.Load(xmlFilePath);
            _nsMgr = new XmlNamespaceManager(_configXml.NameTable);
            _nsMgr.AddNamespace(Prefxkey, ConfigInfo.Xmlns);
            SetConfigEntity();
        }

        /// <summary>
        /// 构建默认实体配置XML
        /// </summary>
        /// <param name="configEntity">配置实体类</param>
        /// <returns></returns>
        public static string BuilderDefaultConfig(ConfigEntity configEntity)
        {
            var builder = new StringBuilder();
            builder.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            builder.Append("<hibernate-configuration xmlns=\"" + configEntity.Xmlns + "\">");
            builder.Append("	<session-factory>");
            builder.Append("		<property name=\"dialect\">" + configEntity.Dialect + "</property>");
            if (!string.IsNullOrEmpty(configEntity.DefaultSchema))
            {
                builder.Append("		<property name=\"default_schema\">" + configEntity.DefaultSchema + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.UseOuterJoin))
            {
                builder.Append("		<property name=\"use_outer_join\">" + configEntity.UseOuterJoin + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.MaxFetchDepth))
            {
                builder.Append("		<property name=\"max_fetch_depth\">" + configEntity.MaxFetchDepth + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.UseReflectionOptimizer))
            {
                builder.Append("		<property name=\"use_reflection_optimizer\">" + configEntity.UseReflectionOptimizer + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.BytecodeProvider))
            {
                builder.Append("		<property name=\"bytecode.provider\">" + configEntity.BytecodeProvider + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.CacheProviderClass))
            {
                builder.Append("		<property name=\"cache.provider_class\">" + configEntity.CacheProviderClass + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.CacheUseMinimalPuts))
            {
                builder.Append("		<property name=\"cache.use_minimal_puts\">" + configEntity.CacheUseMinimalPuts + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.CacheUseQueryCache))
            {
                builder.Append("		<property name=\"cache.use_query_cache\">" + configEntity.CacheUseQueryCache + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.CacheQueryCacheFactory))
            {
                builder.Append("		<property name=\"cache.query_cache_factory\">" + configEntity.CacheQueryCacheFactory + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.CacheRegionPrefix))
            {
                builder.Append("		<property name=\"cache.region_prefix\">" + configEntity.CacheRegionPrefix + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.Hbm2DdlAuto))
            {
                builder.Append("		<property name=\"hbm2ddl.auto\">" + configEntity.Hbm2DdlAuto + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.UseProxyValidator))
            {
                builder.Append("		<property name=\"use_proxy_validator\">" + configEntity.UseProxyValidator + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.TransactionFactoryClass))
            {
                builder.Append("		<property name=\"transaction.factory_class\">" + configEntity.TransactionFactoryClass + "</property>");
            }
            builder.Append("		<property name=\"connection.provider\">" + configEntity.ConnectionProvider + "</property>");
            builder.Append("		<property name=\"connection.driver_class\">" + configEntity.ConnectionDriverClass + "</property>");
            if (!string.IsNullOrEmpty(configEntity.ConnectionStringName))
            {
                builder.Append("		<property name=\"connection.connection_string\">" + configEntity.ConnectionStringName + "</property>");
            }
            else
            {
                builder.Append("		<property name=\"connection.connection_string\"><![CDATA[" + configEntity.ConnectionString + "]]></property>");
            }
            if (!string.IsNullOrEmpty(configEntity.ConnectionIsolation))
            {
                builder.Append("		<property name=\"connection.isolation\">" + configEntity.ConnectionIsolation + "</property>");
            }
            if (!string.IsNullOrEmpty(configEntity.ConnectionReleaseMode))
            {
                builder.Append("		<property name=\"connection.release_mode\">" + configEntity.ConnectionReleaseMode + "</property>");
            }
            builder.Append("		<property name=\"proxyfactory.factory_class\">" + configEntity.ProxyfactoryFactoryClass + "</property>");
            builder.Append("		<property name=\"show_sql\">" + configEntity.ShowSql + "</property>");
            builder.Append("		<property name=\"command_timeout\">" + configEntity.CommandTimeout + "</property>");
            builder.Append("		<property name=\"adonet.batch_size\">" + configEntity.BatchSize + "</property>");
            builder.Append("		<property name=\"query.substitutions\">" + configEntity.QuerySubstitutions + "</property>");
            builder.Append("		<mapping assembly=\"" + configEntity.Assembly + "\" />");
            builder.Append("	</session-factory>");
            builder.Append("</hibernate-configuration>");

            return builder.ToString();
        }

        #endregion
    }
}
