﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// Access版本
    /// </summary>
    public enum AccessVersion
    {
        /// <summary>
        /// Access
        /// </summary>
        Access
    }

    /// <summary>
    /// Access配置实体
    /// </summary>
    public class AccessConfigEntity : ConfigEntity
    {
                /// <summary>
        ///  Firebird配置实体
        /// </summary>
        /// <param name="connection">连接字符串</param>
        public AccessConfigEntity(string connection) : this(connection, AccessVersion.Access)
        {
        }

        /// <summary>
        /// Access配置实体
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="version"></param>
        public AccessConfigEntity(string connection, AccessVersion version)
            : base(connection)
        {
            switch (version)
            {
                case AccessVersion.Access:
                    Dialect = "NHibernate.JetDriver.JetDialect, NHibernate.JetDriver";
                    ConnectionDriverClass = "NHibernate.JetDriver.JetDriver, NHibernate.JetDriver";
                    ConnectionReleaseMode = "on_close";
                    break;
            }

            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";

            DataBase = DataBaseType.Access;
        }
    }
}
