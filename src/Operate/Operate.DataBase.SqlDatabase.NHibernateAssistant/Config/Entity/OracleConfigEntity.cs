﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// Oracle版本
    /// </summary>
    public enum OracleVersion
    {
        /// <summary>
        /// Oracle (any version)
        /// </summary>
        Oracle,

        /// <summary>
        /// Oracle 9/10g
        /// </summary>
        Oracle910G
    }

    /// <summary>
    /// Oracle配置实体
    /// </summary>
    public class OracleConfigEntity : ConfigEntity
    {
        /// <summary>
        /// Oracle配置实体
        /// </summary>
        /// <param name="connection">链接字符串</param>
        /// <param name="version">版本</param>
        public OracleConfigEntity(string connection, OracleVersion version)
            : base("","",connection)
        {
            switch (version)
            {
                case OracleVersion.Oracle:
                    Dialect = "NHibernate.Dialect.OracleDialect";
                    break;
                case OracleVersion.Oracle910G:
                    Dialect = "NHibernate.Dialect.Oracle9Dialect";
                    break;
            }
            DataBase = DataBaseType.Oracle;
        }
    }
}
