﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// Sqlite版本
    /// </summary>
    public enum SqliteVersion
    {
        /// <summary>
        /// Sqlite
        /// 把driver_class设置为NHibernate.Driver.SQLite20Driver启用System.Data.SQLite provider for .NET 2.0。
        /// </summary>
        Sqlite
    }

    /// <summary>
    /// Sqlite配置实体
    /// </summary>
    public class SqliteConfigEntity : ConfigEntity
    {
        /// <summary>
        /// Sqlite配置实体
        /// </summary>
        /// <param name="connection">链接字符串</param>
        public SqliteConfigEntity(string connection) : this(connection, SqliteVersion.Sqlite)
        {
        }

        /// <summary>
        /// Sqlite配置实体
        /// </summary>
        /// <param name="connection">链接字符串</param>
        /// <param name="version">版本</param>
        public SqliteConfigEntity(string connection, SqliteVersion version)
            : base("", "NHibernate.Driver.SQLite20Driver", connection)
        {
            switch (version)
            {
                case SqliteVersion.Sqlite:
                    Dialect = "NHibernate.Dialect.SQLiteDialect";
                    break;
            }

            DataBase = DataBaseType.Sqlite;
        }
    }
}
