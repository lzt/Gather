﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// DB2版本
    /// </summary>
    public enum Db2Version
    {
        /// <summary>
        /// Db2
        /// </summary>
        Db2,

        /// <summary>
        /// DB2 for iSeries (OS/400)
        /// </summary>
        Db2ForISeries
    }

    /// <summary>
    /// DB2链接实体
    /// </summary>
    public class Db2ConfigEntity : ConfigEntity
    {
        /// <summary>
        /// DB2链接实体
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="version"></param>
        public Db2ConfigEntity(string connection, Db2Version version)
            : base(connection)
        {
            switch (version)
            {
                case Db2Version.Db2:
                    Dialect = "NHibernate.Dialect.DB2Dialect";
                    break;
                case Db2Version.Db2ForISeries:
                    Dialect = "NHibernate.Dialect.DB2400Dialect";
                    break;
            }
            DataBase = DataBaseType.Db2;
        }
    }
}
