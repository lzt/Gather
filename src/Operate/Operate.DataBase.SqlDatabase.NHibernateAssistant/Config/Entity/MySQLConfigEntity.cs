﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    ///  MySql版本
    /// </summary>
    public enum MySqlVersion
    {
        /// <summary>
        /// MySQL 3 or 4
        /// </summary>
        MySql3Or4,

        /// <summary>
        /// MySQL 5
        /// </summary>
        MySql5
    }

    /// <summary>
    /// MySql配置实体
    /// </summary>
    public class MySqlConfigEntity : ConfigEntity
    {
        /// <summary>
        ///  MySql配置实体
        /// </summary>
        /// <param name="connection">链接字符串</param>
        /// <param name="version">版本</param>
        public MySqlConfigEntity(string connection, MySqlVersion version)
            : base("", "NHibernate.Driver.MySqlDataDriver", connection)
        {
            switch (version)
            {
                case MySqlVersion.MySql3Or4:
                    Dialect = "NHibernate.Dialect.MySQLDialect";
                    break;
                case MySqlVersion.MySql5:
                    Dialect = "NHibernate.Dialect.MySQL5Dialect";
                    break;
            }

            FieldProtectionLeft = "`";
            FieldProtectionRight = "`";

            DataBase = DataBaseType.MySql;
        }
    }
}
