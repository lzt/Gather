﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// SqlServer版本
    /// </summary>
    public enum SqlServerVersion
    {
        /// <summary>
        /// Microsoft SQL Server 2000
        /// </summary>
        Server2000,

        /// <summary>
        /// Microsoft SQL Server 2005 or Later
        /// </summary>
        Server2005OrLater,

        /// <summary>
        /// Microsoft SQL Server 2005 Everywhere Edition
        /// </summary>
        Server2005EverywhereEdition,

        /// <summary>
        /// Microsoft SQL Server 7
        /// </summary>
        Server7
    }

    /// <summary>
    /// SqlServer配置实体
    /// </summary>
    public class SqlServerConfigEntity : ConfigEntity
    {
        /// <summary>
        /// SqlServer配置实体
        /// </summary>
        /// <param name="connection">连接字符串</param>
        /// <param name="version">版本</param>
        public SqlServerConfigEntity(string connection, SqlServerVersion version)
            : base("", "NHibernate.Driver.SqlClientDriver", connection)
        {
            switch (version)
            {
                case SqlServerVersion.Server2000:
                    Dialect = "NHibernate.Dialect.MsSql2000Dialect";
                    break;
                case SqlServerVersion.Server2005OrLater:
                    Dialect = "NHibernate.Dialect.MsSql2005Dialect";
                    break;
                case SqlServerVersion.Server2005EverywhereEdition:
                    Dialect = "NHibernate.Dialect.MsSqlCeDialect";
                    break;
                case SqlServerVersion.Server7:
                    Dialect = "NHibernate.Dialect.MsSqlCeDialect";
                    break;
            }

            FieldProtectionLeft = "[";
            FieldProtectionRight = "]";

            DataBase = DataBaseType.SqlServer;
        }
    }
}
