﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// Firebird版本
    /// </summary>
    public enum FirebirdVersion
    {
        /// <summary>
        /// Firebird
        /// 把driver_class设置为 NHibernate.Driver.FirebirdClientDriver启用Firebird provider for .NET 2.0。
        /// </summary>
        Firebird
    }

    /// <summary>
    /// Firebird配置实体
    /// </summary>
    public class FirebirdConfigEntity : ConfigEntity
    {
        /// <summary>
        ///  Firebird配置实体
        /// </summary>
        /// <param name="connection">连接字符串</param>
        public FirebirdConfigEntity(string connection) : this(connection, FirebirdVersion.Firebird)
        {
        }

        /// <summary>
        /// Firebird配置实体
        /// </summary>
        /// <param name="connection">连接字符串</param>
        /// <param name="version">版本</param>
        public FirebirdConfigEntity(string connection, FirebirdVersion version)
            : base("", "NHibernate.Driver.FirebirdClientDriver", connection)
        {
            switch (version)
            {
                case FirebirdVersion.Firebird:
                    Dialect = "NHibernate.Dialect.FirebirdDialect";
                    break;
            }

            DataBase = DataBaseType.Firebird;
        }
    }
}
