﻿using Operate.DataBase.SqlDatabase.EnumCollection;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base
{
    /// <summary>
    /// 配置实体
    /// </summary>
    public class ConfigEntity
    {
        #region Event

        /// <summary>
        /// 委托事件：连接字符串改变
        /// </summary>
        public event ConnectionStringChangedHandler ConnectionStringChanged;

        /// <summary>
        /// 触发连接字符串改变事件
        /// </summary>
        protected virtual void OnConnectionStringChanged()
        {
            var handler = ConnectionStringChanged;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        /// <summary>
        /// 委托事件：数据模型发生改变
        /// </summary>
        public event AssemblyChangedHandler AssemblyChanged;

        /// <summary>
        ///触发数据模型发生改变事件
        /// </summary>
        protected virtual void OnAssemblyChanged()
        {
            var handler = AssemblyChanged;
            var e = new System.EventArgs();
            if (handler != null) handler(this, e);
        }

        #endregion

        #region Properties

        /// <summary>
        /// 数据库类型
        /// </summary>
        public DataBaseType DataBase { get; set; }

        /// <summary>
        /// 字段保护符号左
        /// </summary>
        public string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        public string FieldProtectionRight { get; set; }

        /// <summary>
        /// 名字空间 与NHibernate版本有关  
        /// 默认urn:nhibernate-configuration-2.2
        /// </summary>
        public string Xmlns { get; set; }

        /// <summary>
        /// 设置NHibernate的Dialect类名 - 允许NHibernate针对特定的关系数据库生成优化的SQL
        /// 可用值： full.classname.of.Dialect, assembly
        /// </summary>
        public string Dialect { get; set; }

        /// <summary>
        /// 在生成的SQL中, 将给定的schema/tablespace附加于非全限定名的表名上. 
        /// 默认为空
        /// 可用值： SCHEMA_NAME
        /// </summary>
        public string DefaultSchema { get; set; }

        /// <summary>
        /// 允许外连接抓取，已弃用，请使用max_fetch_depth。
        /// 默认为空
        /// 可用值： true | false
        /// </summary>
        public string UseOuterJoin { get; set; }

        /// <summary>
        /// 为单向关联(一对一, 多对一)的外连接抓取（outer join fetch）树设置最大深度. 值为0意味着将关闭默认的外连接抓取
        /// 默认为空
        /// 可用值：建议在0 到3之间取值
        /// </summary>
        public string MaxFetchDepth { get; set; }

        /// <summary>
        /// 开启运行时代码动态生成来替代运行时反射机制(系统级属性). 使用这种方式的话程序在启动会耗费一定的性能，但是在程序运行期性能会有更好的提升. 注意即使关闭这个优化, Hibernate还是需要CGLIB. 你不能在hibernate.cfg.xml中设置此属性. 这个属性不能在hibernate.cfg.xml或者是应用程序配置文件 hibernate-configuration  配置节中设置。
        /// 默认为空
        /// 可用值： true | false
        /// </summary>
        public string UseReflectionOptimizer { get; set; }

        /// <summary>
        /// 指定字节码provider用于优化NHibernate反射性能。 null代表完全关闭性能优化, lcg用于轻量级的代码动态生成,codedom基于CodeDOM代码动态生成。
        /// 默认为空
        /// 可用值： null | lcg | codedom
        /// </summary>
        public string BytecodeProvider { get; set; }

        /// <summary>
        /// 设置缓存实现类（实现ICacheProvider接口的类）
        /// 默认为空
        /// 可用值： classname.of.CacheProvider, assembly
        /// </summary>
        public string CacheProviderClass { get; set; }

        /// <summary>
        /// 以频繁的读操作为代价, 优化二级缓存来最小化写操作（对群集缓存有效）。
        /// 默认为空
        /// 可用值： true | false
        /// </summary>
        public string CacheUseMinimalPuts { get; set; }

        /// <summary>
        /// 允许查询缓存, 个别查询仍然需要被设置为可缓存的.
        /// 默认为空
        /// 可用值： true | false
        /// </summary>
        public string CacheUseQueryCache { get; set; }

        /// <summary>
        /// 自定义实现IQueryCacheFactory接口的类名, 默认为内建的StandardQueryCacheFactory。
        /// 默认为空
        /// 可用值： classname.of.QueryCacheFactory, assembly
        /// </summary>
        public string CacheQueryCacheFactory { get; set; }

        /// <summary>
        /// 二级缓存区域名的前缀。
        /// 默认为空
        /// 可用值： prefix
        /// </summary>
        public string CacheRegionPrefix { get; set; }

        /// <summary>
        /// 在ISessionFactory创建时，自动检查数据库结构，或者将数据库schema的DDL导出到数据库. 使用 create-drop时,在显式关闭ISessionFactory时，将drop掉数据库schema.
        /// 默认为空
        /// 可用值： create | create-drop
        /// </summary>
        public string Hbm2DdlAuto { get; set; }

        /// <summary>
        /// 是否启用验证接口或者是类是否可以使用代理，默认开启。（检查实体类的属性或者是方法是否被设置为Virtual）
        /// 默认为空
        /// 可用值： true | false
        /// </summary>
        public string UseProxyValidator { get; set; }

        /// <summary>
        /// 自定义ITransactionFactory的实现，默认为NHibernate内建的AdoNetTransactionFactory。
        /// 默认为空
        /// 可用值： classname.of.TransactionFactory, assembly
        /// </summary>
        public string TransactionFactoryClass { get; set; }

        /// <summary>
        /// 实现IConnectionProvider接口的类型。
        /// 例如： （如果是NHibernate内部的IConnectionProvider实现类）full.classname.of.ConnectionProvider，（如果是自定义的NHibernate外部的IConnectionProvider实现类 ）full.classname.of.ConnectionProvider, assembly。
        /// </summary>
        public string ConnectionProvider { get; set; }

        /// <summary>
        /// 如果使用DriverConnectionProvider，实现IDriver接口的类型。
        /// (如果是NHibernate内部的实现的IDriver类型)full.classname.of.Driver （如果是自定义的NHibernate外部的IDriver实现类 ）full.classname.of.Driver, assembly。
        /// 这个配置通常是不需要配置的，如果设置好了hibernate.dialect一般会自动选好对应的IDriver。 详细的dialect与IDriver的默认对应关系可以查看API文档。
        /// </summary>
        public string ConnectionDriverClass { get; set; }

        // 数据库连接字符串
        private string _connectionString;

        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public string ConnectionString
        {
            get { return _connectionString; }
            set
            {
                var oldConn = _connectionString;
                _connectionString = value;
                //如果值发生变化则调用事件触发函数
                if (value != oldConn)
                {
                    OnConnectionStringChanged();
                }
            }
        }

        /// <summary>
        /// 数据库连接字符串名称(定义在.Net配置文件 connectionStrings 配置节里面的连接字符串名。
        /// </summary>
        public string ConnectionStringName { get; set; }

        /// <summary>
        /// ADO.NET事务隔离级别， 查看System.Data.IsolationLevel类来了解各个值的具体意义, 但请注意多数数据库都不支持所有的隔离级别.
        /// 例如： Chaos, ReadCommitted, ReadUncommitted, RepeatableRead, Serializable, Unspecified
        /// </summary>
        public string ConnectionIsolation { get; set; }

        /// <summary>
        /// 指定ADO.NET何时释放数据库连接。
        /// 例如： auto (默认) | on_close | after_transaction
        /// 注意,这些设置仅对通过ISessionFactory.OpenSession得到的ISession起作用。对于通过ISessionFactory.GetCurrentSession得到的ISession， 所配置的ICurrentSessionContext实现控制这些ISession的连接释放模式。
        /// </summary>
        public string ConnectionReleaseMode { get; set; }

        /// <summary>
        /// IoC框架动态代理方式，分别为：Castle框架、LinFu框 架、Spring.Net框架
        /// 如果使 用Castle.DynamicProxy2动态代理，引用NHibernate.ByteCode.Castle.dll程序集并配置 proxyfactory.factory_class节点为property name="proxyfactory.factory_class" NHibernate.ByteCode.Castle.ProxyFactoryFactory,NHi bernate.ByteCode.Castle/property 
        ///
        /// 如 果使用LinFu.DynamicProxy动态代理，引用NHibernate.ByteCode.LinFu.dll程序集并配置 proxyfactory.factory_class节点为property name="proxyfactory.factory_class" NHibernate.ByteCode.LinFu.ProxyFactoryFactory,NHib ernate.ByteCode.LinFu/property 
        ///
        /// 如果使用Spring.Aop动态代理，引用NHibernate.ByteCode.Spring.dll程序集并配置 proxyfactory.factory_class节点为property name="proxyfactory.factory_class" NHibernate.ByteCode.Spring.ProxyFactoryFactory,NHi bernate.ByteCode.Spring/property 
        /// </summary>
        public string ProxyfactoryFactoryClass { get; set; }

        /// <summary>
        /// 输出所有SQL语句到控制台.
        /// 可用值： true | false
        /// </summary>
        public string ShowSql { get; set; }

        /// <summary>
        /// 指定NHibernate生成的IDbCommands对象的超时时间。 
        /// </summary>
        public string CommandTimeout { get; set; }

        /// <summary>
        /// 指定用ADO.Net的批量更新的数量，默认设置为0（不启用该功能）。
        /// </summary>
        public string BatchSize { get; set; }

        /// <summary>
        /// 将NHibernate查询中的符号映射到SQL查询中的符号 (符号可能是函数名或常量名字).
        /// 可用值： hqlLiteral=SQL_LITERAL, hqlFunction=SQLFUNC
        /// </summary>
        public string QuerySubstitutions { get; set; }

        private string _assembly;

        /// <summary>
        /// 实体映射程序集
        /// </summary>
        public string Assembly
        {
            get { return _assembly; }
            set
            {
                var oldassembly = _assembly;
                _assembly = value;
                //如果值发生变化则调用事件触发函数
                if (value != oldassembly)
                {
                    OnAssemblyChanged();
                }
            }
        }

        #endregion

        #region Constructor

        private ConfigEntity()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 根据参数创建实例
        /// </summary>
        /// <param name="connection">连接字符串</param>
        public ConfigEntity(string connection) : this()
        {
            Xmlns = ConstantCollection.NhibernateConfigXmlNameSpace;
            Dialect = "";
            ConnectionProvider = ConstantCollection.NhibernateConfigConnectionProvider;
            ConnectionDriverClass = "";
            ConnectionString = connection;
            ProxyfactoryFactoryClass = ConstantCollection.NhibernateConfigProxyfactoryFactoryClass;
            ShowSql = ConstantCollection.NhibernateConfigShowSql;
            CommandTimeout = ConstantCollection.NhibernateConfigCommandTimeout;
            BatchSize = ConstantCollection.NhibernateConfigBatchSize;
            QuerySubstitutions = ConstantCollection.NhibernateConfigQuerySubstitutions;
            Assembly = "";
        }

        /// <summary>
        /// 根据参数创建实例
        /// </summary>
        /// <param name="dialect"> 设置NHibernate的Dialect类名 - 允许NHibernate针对特定的关系数据库生成优化的SQL 可用值： full.classname.of.Dialect, assembly</param>
        /// <param name="driverClass"> 如果使用DriverConnectionProvider，实现IDriver接口的类型</param>
        /// <param name="connection">连接字符串</param>
        public ConfigEntity(string dialect, string driverClass, string connection) : this()
        {
            Xmlns = ConstantCollection.NhibernateConfigXmlNameSpace;
            Dialect = dialect;
            ConnectionProvider = ConstantCollection.NhibernateConfigConnectionProvider;
            ConnectionDriverClass = driverClass;
            ConnectionString = connection;
            ProxyfactoryFactoryClass = ConstantCollection.NhibernateConfigProxyfactoryFactoryClass;
            ShowSql = ConstantCollection.NhibernateConfigShowSql;
            CommandTimeout = ConstantCollection.NhibernateConfigCommandTimeout;
            BatchSize = ConstantCollection.NhibernateConfigBatchSize;
            QuerySubstitutions = ConstantCollection.NhibernateConfigQuerySubstitutions;
            Assembly = "";
        }

        /// <summary>
        /// 根据参数创建实例
        /// </summary>
        /// <param name="dialect"> 设置NHibernate的Dialect类名 - 允许NHibernate针对特定的关系数据库生成优化的SQL 可用值： full.classname.of.Dialect, assembly</param>
        /// <param name="driverClass"> 如果使用DriverConnectionProvider，实现IDriver接口的类型</param>
        /// <param name="connection">连接字符串</param>
        /// <param name="assembly">实体映射程序集</param>
        public ConfigEntity(string dialect, string driverClass, string connection, string assembly) : this()
        {
            Xmlns = ConstantCollection.NhibernateConfigXmlNameSpace;
            Dialect = dialect;
            ConnectionProvider = ConstantCollection.NhibernateConfigConnectionProvider;
            ConnectionDriverClass = driverClass;
            ConnectionString = connection;
            ProxyfactoryFactoryClass = ConstantCollection.NhibernateConfigProxyfactoryFactoryClass;
            ShowSql = ConstantCollection.NhibernateConfigShowSql;
            CommandTimeout = ConstantCollection.NhibernateConfigCommandTimeout;
            BatchSize = ConstantCollection.NhibernateConfigBatchSize;
            QuerySubstitutions = ConstantCollection.NhibernateConfigQuerySubstitutions;
            Assembly = assembly;
        }

        /// <summary>
        /// 根据参数创建实例
        /// </summary>
        /// <param name="xmlns">名字空间</param>
        /// <param name="dialect">设置NHibernate的Dialect类名 - 允许NHibernate针对特定的关系数据库生成优化的SQL 可用值： full.classname.of.Dialect, assembly</param>
        /// <param name="provider"> 如果使用DriverConnectionProvider，实现IDriver接口的类型</param>
        /// <param name="driverClass">如果使用DriverConnectionProvider，实现IDriver接口的类型</param>
        /// <param name="connection">连接字符串</param>
        /// <param name="factoryClass"> IoC框架动态代理方式，分别为：Castle框架、LinFu框 架、Spring.Net框架</param>
        /// <param name="showSql">输出所有SQL语句到控制台</param>
        /// <param name="commandTimeout"> 指定NHibernate生成的IDbCommands对象的超时时间</param>
        /// <param name="batchSize">指定用ADO.Net的批量更新的数量</param>
        /// <param name="substitutions">将NHibernate查询中的符号映射到SQL查询中的符号 (符号可能是函数名或常量名字).可用值： hqlLiteral=SQL_LITERAL, hqlFunction=SQLFUNC</param>
        /// <param name="assembly">实体映射程序集</param>
        public ConfigEntity(string xmlns, string dialect, string provider, string driverClass,
                                     string connection, string factoryClass, string showSql, string commandTimeout,
                                     string batchSize, string substitutions, string assembly) : this()
        {
            Xmlns = xmlns;
            Dialect = dialect;
            ConnectionProvider = provider;
            ConnectionDriverClass = driverClass;
            ConnectionString = connection;
            ProxyfactoryFactoryClass = factoryClass;
            ShowSql = showSql;
            CommandTimeout = commandTimeout;
            BatchSize = batchSize;
            QuerySubstitutions = substitutions;
            Assembly = assembly;
        }

        #endregion
    }
}
