﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    ///  Ingres版本
    /// </summary>
    public enum IngresVersion
    {
        /// <summary>
        /// Ingres
        /// </summary>
        Ingres,

        /// <summary>
        /// Ingres 3.0
        /// </summary>
        Ingres3
    }
    
    /// <summary>
    /// Ingres配置实体
    /// </summary>
    public class IngresConfigEntity : ConfigEntity
    {
        /// <summary>
        ///  Ingres配置实体
        /// </summary>
        /// <param name="connection">连接字符串</param>
        /// <param name="version">版本</param>
        public IngresConfigEntity(string connection, IngresVersion version)
            : base(connection)
        {
            switch (version)
            {
                case IngresVersion.Ingres:
                    Dialect = "NHibernate.Dialect.IngresDialect";
                    break;
                case IngresVersion.Ingres3:
                    Dialect = "NHibernate.Dialect.IngresDialect";
                    break;
            }

            DataBase = DataBaseType.Ingres;
        }
    }
}
