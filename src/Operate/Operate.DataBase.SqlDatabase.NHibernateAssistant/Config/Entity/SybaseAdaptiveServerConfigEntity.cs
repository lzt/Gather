﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// SybaseAdaptiveServer版本
    /// </summary>
    public enum SybaseAdaptiveServerVersion
    {
        /// <summary>
        /// Sybase Adaptive Server Enterprise
        /// </summary>
        Enterprise,

        /// <summary>
        /// Sybase Adaptive Server Anywhere
        /// </summary>
        Anywhere
    }
    
    /// <summary>
    /// SybaseAdaptiveServer配置实体
    /// </summary>
    public class SybaseAdaptiveServerConfigEntity : ConfigEntity
    {
        /// <summary>
        /// SybaseAdaptiveServer配置实体
        /// </summary>
        /// <param name="connection">链接字符串</param>
        /// <param name="version">版本</param>
        public SybaseAdaptiveServerConfigEntity(string connection, SybaseAdaptiveServerVersion version)
            : base(connection)
        {
            switch (version)
            {
                case SybaseAdaptiveServerVersion.Enterprise:
                    Dialect = "NHibernate.Dialect.SybaseDialect";
                    break;
                case SybaseAdaptiveServerVersion.Anywhere:
                    Dialect = "NHibernate.Dialect.SybaseAnywhereDialect";
                    break;
            }

            DataBase = DataBaseType.Sybase;
        }
    }
}
