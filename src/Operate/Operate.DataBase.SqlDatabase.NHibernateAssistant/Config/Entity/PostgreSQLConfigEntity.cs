﻿using Operate.DataBase.SqlDatabase.EnumCollection;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity.Base;

namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// PostgreSql版本
    /// </summary>
    public enum PostgreSqlVersion
    {
        /// <summary>
        /// PostgreSQL
        /// </summary>
        PostgreSql,

        /// <summary>
        /// PostgreSQL 8.1
        /// 方言在PostgreSQL8.1中现在支持FOR UPDATE NOWAIT
        /// </summary>
        PostgreSql81,

        /// <summary>
        /// PostgreSQL 8.2
        /// 方言在PostgreSQL8.2中现在支持在 DROP TABLE和DROP SEQUENCE中使用IF EXISTS关键字
        /// </summary>
        PostgreSql82
    }

    /// <summary>
    /// PostgreSql配置实体
    /// </summary>
   public class PostgreSqlConfigEntity : ConfigEntity
    {
       /// <summary>
        /// PostgreSql配置实体
       /// </summary>
       /// <param name="connection">链接字符串</param>
       /// <param name="version">版本</param>
       public PostgreSqlConfigEntity(string connection, PostgreSqlVersion version)
            : base(connection)
        {
            switch (version)
            {
                case PostgreSqlVersion.PostgreSql:
                    Dialect = "NHibernate.Dialect.PostgreSQLDialect";
                    break;
                case PostgreSqlVersion.PostgreSql81:
                    Dialect = "NHibernate.Dialect.PostgreSQL81Dialect";
                    break;
                case PostgreSqlVersion.PostgreSql82:
                    Dialect = "NHibernate.Dialect.PostgreSQL82Dialect";
                    break;
            }
            DataBase = DataBaseType.PostgreSql;
        }
    }
}
