﻿namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.Config.Entity
{
    /// <summary>
    /// 链接字符串变更完毕时触发
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ConnectionStringChangedHandler(object sender, System.EventArgs e);

    /// <summary>
    /// 实体模型变更完毕时触发
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void AssemblyChangedHandler(object sender, System.EventArgs e);
}
