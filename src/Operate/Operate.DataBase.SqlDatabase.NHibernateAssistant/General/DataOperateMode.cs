﻿namespace Operate.DataBase.SqlDatabase.NHibernateAssistant.General
{
    /// <summary>
    /// 数据操作模式
    /// </summary>
    public enum DataOperateMode
    {
        /// <summary>
        /// Nhibernate
        /// </summary>
        Nhibernate = 0,

        /// <summary>
        /// CustomSql
        /// </summary>
        CustomSql = 1
    }
}
