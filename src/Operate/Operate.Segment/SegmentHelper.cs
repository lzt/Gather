﻿using System.Collections;

namespace Operate.Segment
{
    /// <summary>
    /// 分词辅助类
    /// </summary>
    public class SegmentHelper
    {
        /// <summary>
        /// 最大长度
        /// </summary>
        public int MaxLength;
        private readonly ArrayList _seg;

        /// <summary>
        /// 数量
        /// </summary>
        public int Count
        {
            get
            {
                return _seg.Count;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public SegmentHelper()
        {
            _seg = new ArrayList();
            MaxLength = 0;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="obj"></param>
        public void Add(object obj)
        {
            _seg.Add(obj);
            if (MaxLength < obj.ToString().Length)
            {
                MaxLength = obj.ToString().Length;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public object GetElem(int i)
        {
            return i < Count ? _seg[i] : null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="i"></param>
        /// <param name="obj"></param>
        public void SetElem(int i, object obj)
        {
            _seg[i] = obj;
        }

        /// <summary>
        /// 包含
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Contains(object obj)
        {
            return _seg.Contains(obj);
        }

        /// <summary>
        /// 按长度排序
        /// </summary>
        public void Sort()
        {
            Sort(this);
        }

        /// <summary>
        /// 按长度排序
        /// </summary>
        public void Sort(SegmentHelper list)
        {
            for (int i = 0; i < list.Count - 1; ++i)
            {
                int max = i;
                for (int j = i + 1; j < list.Count; ++j)
                {

                    string str1 = list.GetElem(j).ToString();
                    string str2 = list.GetElem(max).ToString();
                    int l1 = str1 == "null" ? 0 : str1.Length;

                    int l2 = str2 == "null" ? 0 : str2.Length;

                    if (l1 > l2)
                        max = j;
                }
                object o = list.GetElem(max);
                list.SetElem(max, list.GetElem(i));
                list.SetElem(i, o);
            }
        }
    }
}
