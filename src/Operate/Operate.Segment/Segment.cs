﻿using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using Operate.Caching;

namespace Operate.Segment
{
    /// <summary>
    /// 分词
    /// </summary>
    public class Segment
    {
        #region 私有字段

        private const String NameSpace = "Operate.Segment.";

        private Hashtable _hashtableWords=new Hashtable();
        private ArrayList _arrayNoise=new ArrayList();
        private ArrayList _arrayNumber=new ArrayList();
        private ArrayList _arrayWord=new ArrayList();
        private ArrayList _arrayPrefix=new ArrayList();
        private double _eventTime;
        /// <summary>
        /// 存储盒
        /// </summary>
        public static Cache<string> CacheBox = new Cache<string>();

        /// <summary>
        /// 分隔符
        /// </summary>
        private string _separator = " ";

        /// <summary>
        /// 用于验证汉字的正则表达式
        /// </summary>
        private const string Chinese = "[\u4e00-\u9fa5]";

        #endregion

        #region 公有属性

        /// <summary>
        /// 基本词典路径
        /// </summary>
        public string Dic { get; set; }

        /// <summary>
        /// 数据缓存函数
        /// </summary>
        /// <param name="key">索引键</param>
        /// <param name="val">缓存的数据</param>
        private static void SetCache(string key, object val)
        {
            if (val == null) val = " ";
            lock (CacheBox)
            {
                CacheBox.Add(key, val);
            }
        }

        /// <summary>
        /// 读取缓存
        /// </summary>
        private static object GetCache(string key)
        {
            return CacheBox.Get<object>(key);
        }

        /// <summary>
        /// 暂时无用
        /// </summary>
        public string Noise { get; set; }

        /// <summary>
        /// 数字词典路径
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 字母词典路径
        /// </summary>
        public string Word { get; set; }

        /// <summary>
        /// 姓名前缀字典 用于纠错姓名
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// 是否开启姓名纠错功能
        /// </summary>
        public bool EnablePrefix
        {
            get
            {
                return _arrayPrefix.Count != 0;
            }
            set
            {
                _arrayPrefix = value ? LoadWords(Prefix, _arrayPrefix) : new ArrayList();
            }
        }

        /// <summary>
        /// 用时每次进行加载或分词动作后改属性表示为上一次动作所用时间
        /// 已精确到毫秒但分词操作在字符串较短时可能为0
        /// </summary>
        public double EventTime
        {
            get
            {
                return _eventTime;
            }
        }

        /// <summary>
        /// 分隔符,默认为空格
        /// </summary>
        public string Separator
        {
            get
            {
                return _separator;
            }
            set
            {
                if (!string.IsNullOrEmpty(value)) _separator = value;
            }
        }

        /// <summary>
        /// 可用状态
        /// </summary>
        public bool EmbeddedMode { get;private set; }

        #endregion

        #region 构造方法
        /// <summary>
        /// 构造方法
        /// </summary>
        public Segment()
        {
            Dic = NameSpace + "Dictionary.Dict.dic";
            Noise = NameSpace + "Dictionary.Noise.dic";
            Number = NameSpace + "Dictionary.Number.dic";
            Word = NameSpace + "Dictionary.Word.dic";
            Prefix = NameSpace + "Dictionary.Prefix.dic";
            EmbeddedMode = true;
            InitWordDics();
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        public Segment(string dic, string noise, string number, string word, string prefx)
        {
            Dic = dic;
            Noise = noise;
            Number = number;
            Word = word;
            Prefix = prefx;
            EmbeddedMode = false;
            InitWordDics(false);
        }
        #endregion

        #region 公有方法

        /// <summary>
        /// 加载词列表
        /// </summary>
        public void InitWordDics(bool isEmbedded = true)
        {
            DateTime start = DateTime.Now;
#pragma warning disable 219
            long i = 0;
#pragma warning restore 219
            if (GetCache("jcms_dict") == null)
            {
                _hashtableWords = new Hashtable();
                Stream stream = null;
                if (isEmbedded)
                {
                    stream = GetType().Assembly.GetManifestResourceStream(Dic);
                }
                StreamReader reader;
                if (isEmbedded)
                {
                    if (stream != null)
                    {
                        reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    }
                    else
                    {
                        throw new Exception("加载字典是出现异常,未能正确加载");
                    }
                }
                else
                {
                    reader = new StreamReader(Dic, System.Text.Encoding.UTF8);
                }
                string strline = reader.ReadLine();

                while (strline != null && strline.Trim() != "")
                {
                    i++;
                    string strChar1 = strline.Substring(0, 1);
                    string strChar2 = strline.Substring(1, 1);
                    Hashtable father;
                    if (!_hashtableWords.ContainsKey(strChar1))
                    {
                        father = new Hashtable();
                        _hashtableWords.Add(strChar1, father);
                    }
                    else
                    {
                        father = (Hashtable)_hashtableWords[strChar1];
                    }

                    SegmentHelper list;
                    if (!father.ContainsKey(strChar2))
                    {
                        list = new SegmentHelper();
                        list.Add(strline.Length > 2 ? strline.Substring(2) : "null");
                        father.Add(strChar2, list);
                    }
                    else
                    {
                        list = (SegmentHelper)father[strChar2];
                        list.Add(strline.Length > 2 ? strline.Substring(2) : "null");
                        father[strChar2] = list;
                    }
                    _hashtableWords[strChar1] = father;
                    strline = reader.ReadLine();
                }
                try
                {
                    reader.Close();
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                { }
                SetCache("jcms_dict", _hashtableWords);
            }
            _hashtableWords = (Hashtable)GetCache("jcms_dict");

            _arrayNoise = LoadWords(Noise, _arrayNoise,isEmbedded);
            _arrayNumber = LoadWords(Number, _arrayNumber, isEmbedded);
            _arrayWord = LoadWords(Word, _arrayWord, isEmbedded);
            _arrayPrefix = LoadWords(Prefix, _arrayPrefix, isEmbedded);

            TimeSpan duration = DateTime.Now - start;
            _eventTime = duration.TotalMilliseconds;
        }

        /// <summary>
        /// 加载文本词组到ArrayList
        /// </summary>
        public ArrayList LoadWords(string strPath, ArrayList list, bool isEmbedded=true)
        {
            if (list == null) throw new ArgumentNullException("list");
            Stream stream = null;
            if (isEmbedded)
            {
                stream = GetType().Assembly.GetManifestResourceStream(Dic);
            }
            StreamReader reader;
            if (isEmbedded)
            {
                if (stream != null)
                {
                    reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                }
                else
                {
                    throw new Exception("加载字典是出现异常,未能正确加载");
                }
            }
            else
            {
                reader = new StreamReader(Dic, System.Text.Encoding.UTF8);
            }
            list = new ArrayList();
            string strline = reader.ReadLine();
            while (strline != null)
            {
                list.Add(strline);
                strline = reader.ReadLine();
            }
            try
            {
                reader.Close();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            { }
            return list;
        }

        /// <summary>
        /// 输出词列表
        /// </summary>
        public void OutWords()
        {
            IDictionaryEnumerator idEnumerator1 = _hashtableWords.GetEnumerator();
            while (idEnumerator1.MoveNext())
            {
                IDictionaryEnumerator idEnumerator2 = ((Hashtable)idEnumerator1.Value).GetEnumerator();
                while (idEnumerator2.MoveNext())
                {
                    var aa = (SegmentHelper)idEnumerator2.Value;
                    for (int i = 0; i < aa.Count; i++)
                    {
                        Console.WriteLine(idEnumerator1.Key + idEnumerator2.Key.ToString() + aa.GetElem(i));
                    }
                }
            }
        }

        /// <summary>
        /// 输出ArrayList
        /// </summary>
        public void OutArrayList(ArrayList list)
        {
            if (list == null) return;
            foreach (object t in list)
            {
                Console.WriteLine(t.ToString());
            }
        }

        /// <summary>
        /// 分词过程,不支持回车 
        /// </summary>
        /// <param name="strText">要分词的文本</param>
        /// <returns>分词后的文本</returns>
        public string SegmentText(string strText)
        {
            strText = (strText + "$").Trim();
            if (_hashtableWords == null) return strText;
            if (strText.Length < 3) return strText;
            DateTime start = DateTime.Now;
            int length = 0;
            int preFix = 0;
            bool word = false;
            bool number = false;
            string reText = "";
            string strPrefix = "";
            string strLastChar = "";

            for (int i = 0; i < strText.Length - 1; i++)
            {
                #region 对于每一个字的处理过程
                string strChar1 = strText.Substring(i, 1);
                string strChar2 = strText.Substring(i + 1, 1).Trim();
                bool yes;

                if (reText.Length > 0) strLastChar = reText.Substring(reText.Length - 1);

                if (strChar1 == " ")
                {
                    if ((number || word) && strLastChar != Separator) reText += Separator;
                    yes = true;
                }
                else
                    yes = false;

                int charType = GetCharType(strChar1);
                string strLastWords;
                switch (charType)
                {
                    case 1:
                        #region  如果是数字，如果数字的上一位是字母要和后面的数字分开
                        if (word)
                        {
                            reText += Separator;
                        }
                        word = false;
                        number = true;
                        strLastWords = "";
                        break;
                        #endregion
                    case 2:
                    case 5:
                        #region 如果是字母
                        strLastWords = number ? Separator : "";

                        word = true;
                        number = false;
                        break;
                        #endregion
                    case 3:
                    case 4:
                        #region 第一级哈希表是否包含关键字，假如包含处理第二级哈希表
                        //上一个字是否为字母
                        if (word) reText += Separator;

                        #region 检测上一个是否是数字，这个过程是用于修正数字后的量词的

                        SegmentHelper l;
                        Hashtable h;
                        if (number && charType != 4)
                        {
                            h = (Hashtable)_hashtableWords["n"];
                            if (h.ContainsKey(strChar1))
                            {
                                l = (SegmentHelper)h[strChar1];
                                if (l.Contains(strChar2))
                                {
                                    reText += strChar1 + strChar2 + Separator;
                                    yes = true;
                                    i++;
                                }
                                else if (l.Contains("null"))
                                {
                                    reText += strChar1 + Separator;
                                    yes = true;
                                }
                            }
                            else
                                reText += Separator;
                        }
                        #endregion

                        //非汉字数字的汉字
                        if (charType == 3)
                        {
                            word = false;
                            number = false;
                            strLastWords = Separator;
                        }
                        else
                        {
                            word = false;
                            number = true;
                            strLastWords = "";
                        }

                        //第二级哈希表取出
                        h = (Hashtable)_hashtableWords[strChar1];

                        //第二级哈希表是否包含关键字
                        if (h.ContainsKey(strChar2))
                        {
                            #region  第二级包含关键字
                            //取出ArrayList对象
                            l = (SegmentHelper)h[strChar2];

                            //遍历每一个对象 看是否能组合成词
                            for (int j = 0; j < l.Count; j++)
                            {
                                string strChar3 = l.GetElem(j).ToString();

                                //对于每一个取出的词进行检测,看是否匹配，长度保护
                                if ((strChar3.Length + i + 2) < strText.Length)
                                {
                                    //向i+2后取出m长度的字
                                    string strChar = strText.Substring(i + 2, strChar3.Length).Trim();
                                    if (strChar3 == strChar && !yes)
                                    {
                                        if (strPrefix != "")
                                        {
                                            reText += strPrefix + Separator;
                                            strPrefix = "";
                                            preFix = 0;
                                        }
                                        reText += strChar1 + strChar2 + strChar;
                                        i += strChar3.Length + 1;
                                        yes = true;
                                        break;
                                    }
                                }
                                else if ((strChar3.Length + i + 2) == strText.Length)
                                {
                                    string strChar = strText.Substring(i + 2).Trim();
                                    if (strChar3 == strChar && !yes)
                                    {
                                        if (strPrefix != "")
                                        {
                                            reText += strPrefix + Separator;
                                            strPrefix = "";
                                            preFix = 0;
                                        }
                                        reText += strChar1 + strChar2 + strChar;
                                        i += strChar3.Length + 1;
                                        yes = true;
                                        break;
                                    }
                                }

                                if (j == l.Count - 1 && l.Contains("null") && !yes)
                                {
                                    if (preFix == 1)
                                    {
                                        reText += strPrefix + strChar1 + strChar2;
                                        strPrefix = "";
                                        preFix = 0;
                                    }
                                    else if (preFix > 1)
                                    {
                                        reText += strPrefix + strLastWords + strChar1 + strChar2;
                                        strPrefix = "";
                                        preFix = 0;
                                    }
                                    else
                                    {
                                        if (charType == 4) reText += strChar1 + strChar2;
                                        else reText += strChar1 + strChar2;
                                        strLastWords = Separator;
                                        number = false;
                                    }
                                    i++;
                                    yes = true;
                                    break;
                                }
                            }
                            #endregion

                            //如果没有匹配还可能有一种情况，这个词语只有两个字，以这两个字开头的词语不存在
                            if (!yes && l.Contains("null"))
                            {
                                if (preFix == 1)
                                {
                                    reText += strPrefix + strChar1 + strChar2;
                                    strPrefix = "";
                                    preFix = 0;
                                }
                                else if (preFix > 1)
                                {
                                    reText += strPrefix + strLastWords + strChar1 + strChar2;
                                    strPrefix = "";
                                    preFix = 0;
                                }
                                else
                                {
                                    if (charType == 4) reText += strChar1 + strChar2;
                                    else reText += strChar1 + strChar2;
                                    strLastWords = Separator;
                                    number = false;
                                }
                                i++;
                                yes = true;
                            }
                            if (reText.Length > 0) strLastChar = reText.Substring(reText.Length - 1);
                            if (charType == 4 && GetCharType(strLastChar) == 4)
                            {
                                number = true;
                            }
                            else if (strLastChar != Separator) reText += Separator;
                        }
                        #endregion
                        break;
                    default:
                        #region 未知字符,可能是生僻字,也可能是标点符合之类
                        if (word && !yes)
                        {
                            reText += Separator;
                        }
                        else if (number && !yes)
                        {
                            reText += Separator;
                        }
                        number = false;
                        word = false;
                        strLastWords = Separator;
                        break;
                        #endregion
                }
                if (!yes && number || !yes && word)
                {
                    reText += strChar1;
                    yes = true;
                }
                if (!yes)
                {
                    #region 处理姓名问题
                    if (preFix == 0)
                    {
                        if (_arrayPrefix.Contains(strChar1 + strChar2))
                        {
                            i++;
                            strPrefix = strChar1 + strChar2;
                            preFix++;
                        }
                        else if (_arrayPrefix.Contains(strChar1))
                        {
                            strPrefix = strChar1;
                            preFix++;
                        }
                        else
                        {
                            if (preFix == 3)
                            {
                                reText += strPrefix + Separator + strChar1 + Separator;
                                strPrefix = "";
                                preFix = 0;
                            }
                            else if (preFix > 0)
                            {
                                if (Regex.IsMatch(strChar1, Chinese))
                                {
                                    strPrefix += strChar1;
                                    preFix++;
                                }
                                else
                                {
                                    reText += strPrefix + Separator + strChar1 + Separator;
                                    strPrefix = "";
                                    preFix = 0;
                                }
                            }
                            else
                            {
                                reText += strChar1 + strLastWords;
                            }
                        }
                    }
                    else
                    {
                        if (preFix == 3)
                        {
                            reText += strPrefix + Separator + strChar1 + Separator;
                            strPrefix = "";
                            preFix = 0;
                        }
                        else if (preFix > 0)
                        {
                            if (Regex.IsMatch(strChar1, Chinese))
                            {
                                strPrefix += strChar1;
                                preFix++;
                            }
                            else
                            {
                                reText += strPrefix + Separator + strChar1 + Separator;
                                strPrefix = "";
                                preFix = 0;
                            }
                        }
                        else
                        {
                            reText += strChar1 + strLastWords;
                        }
                    }
                    #endregion
                }
                length = i;
                #endregion
            }

            #region 最后防止最后一个字的丢失
            if (length < strText.Length - 1)
            {
                string strLastChar1 = strText.Substring(strText.Length - 1).Trim();
                string strLastChar2 = strText.Substring(strText.Length - 2).Trim();

                if (reText.Length > 0) strLastChar = reText.Substring(reText.Length - 1);
                if (preFix != 0)
                {
                    reText += strPrefix + strLastChar1;
                }
                else
                {
                    switch (GetCharType(strLastChar1))
                    {
                        case 1:
                            if (strLastChar1 != "." && strLastChar1 != "．")
                                reText += strLastChar1;
                            else
                                reText += Separator + strLastChar1;
                            break;
                        case 2:
                        case 5:
                            if (_arrayWord.Contains(strLastChar2))
                                reText += strLastChar1;
                            break;
                        case 3:
                        case 4:
                            if ((number || word) && strLastChar != Separator)
                                reText += Separator + strLastChar1;
                            else
                                reText += strLastChar1;
                            break;
                        default:
                            if (strLastChar != Separator)
                                reText += Separator + strLastChar1;
                            else
                                reText += strLastChar1;
                            break;
                    }
                }
                if (reText.Length > 0) strLastChar = (reText.Substring(reText.Length - 1));
                if (strLastChar != Separator) reText += Separator;
            }
            #endregion

            TimeSpan duration = DateTime.Now - start;
            _eventTime = duration.TotalMilliseconds;
            return reText.Replace(" $", ""); //这里包含一个字的，则去掉
        }

        /// <summary>
        /// 重载分词过程,支持回车
        /// </summary>
        public string SegmentText(string strText, bool enter)
        {
            if (enter)
            {
                DateTime start = DateTime.Now;
                string[] strArr = strText.Split('\n');

                string reText = "";
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (string t in strArr)
                {
                    reText += SegmentText(t) + "\r\n";
                }

                TimeSpan duration = DateTime.Now - start;
                _eventTime = duration.TotalMilliseconds;
                return reText;
            }
            return SegmentText(strText);
        }

        #region 判断字符类型
        /// <summary>
        /// 判断字符类型,0为未知,1为数字,2为字母,3为汉字,4为汉字数字
        /// </summary>
        private int GetCharType(string pChar)
        {
            int charType = 0;
            if (_arrayNumber.Contains(pChar)) charType = 1;
            if (_arrayWord.Contains(pChar)) charType = 2;
            if (_hashtableWords.ContainsKey(pChar)) charType += 3;
            return charType;
        }
        #endregion

        #region 对加载的词典排序并重新写入
        /// <summary>
        /// 对加载的词典排序并重新写入
        /// </summary>
        public void SortDic()
        {
            SortDic(false);
        }

        /// <summary>
        /// 对加载的词典排序并重新写入
        /// </summary>
        /// <param name="reload">是否重新加载</param>
        public void SortDic(bool reload)
        {
            DateTime start = DateTime.Now;
            var sw = new StreamWriter(Dic, false, System.Text.Encoding.UTF8);

            IDictionaryEnumerator idEnumerator1 = _hashtableWords.GetEnumerator();
            while (idEnumerator1.MoveNext())
            {
                IDictionaryEnumerator idEnumerator2 = ((Hashtable)idEnumerator1.Value).GetEnumerator();
                while (idEnumerator2.MoveNext())
                {
                    var aa = (SegmentHelper)idEnumerator2.Value;
                    aa.Sort();
                    for (int i = 0; i < aa.Count; i++)
                    {
                        if (aa.GetElem(i).ToString() == "null")
                            sw.WriteLine(idEnumerator1.Key + idEnumerator2.Key.ToString());
                        else
                            sw.WriteLine(idEnumerator1.Key + idEnumerator2.Key.ToString() + aa.GetElem(i));
                    }
                }
            }
            sw.Close();

            if (reload) InitWordDics();

            TimeSpan duration = DateTime.Now - start;
            _eventTime = duration.TotalMilliseconds;
        }
        #endregion

        /// <summary>
        /// 删除两行完全相同的词,暂时无用!
        /// </summary>
        /// <returns>相同词条个数</returns>
        public int Optimize()
        {
            int l = 0;
            DateTime start = DateTime.Now;

            var htOptimize = new Hashtable();
            var reader = new StreamReader(Dic, System.Text.Encoding.UTF8);
            string strline = reader.ReadLine();
            while (strline != null && strline.Trim() != "")
            {
                if (!htOptimize.ContainsKey(strline))
                    htOptimize.Add(strline, null);
                else
                    l++;
            }
            Console.WriteLine("ready");
            try
            {
                reader.Close();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
            var sw = new StreamWriter(Dic, false, System.Text.Encoding.UTF8);
            IDictionaryEnumerator ide = htOptimize.GetEnumerator();
            while (ide.MoveNext())
                sw.WriteLine(ide.Key.ToString());
            try
            {
                sw.Close();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
            TimeSpan duration = DateTime.Now - start;
            _eventTime = duration.TotalMilliseconds;
            return l;
        }
        #endregion
    }
}
