﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;
using Operate.Validation.Rules.Enums;

#endregion

namespace Operate.Validation.Rules
{
    /// <summary>
    /// Is attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class IsAttribute : ValidationAttribute
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type">Validation type enum</param>
        /// <param name="errorMessage">Error message</param>
        public IsAttribute(IsValid type, string errorMessage = "")
            : base(string.IsNullOrEmpty(errorMessage) ? "{0} is not {1}" : errorMessage)
        {
            Type = type;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Type of validation to do
        /// </summary>
        public IsValid Type { get;private set; }

        #endregion

        #region Functions

        /// <summary>
        /// Formats the error message
        /// </summary>
        /// <param name="name">Property name</param>
        /// <returns>The formatted string</returns>
        public override string FormatErrorMessage(string name)
        {
            string comparisonString = "";
            if (Type == Enums.IsValid.CreditCard)
                comparisonString = "a credit card";
            else if (Type == Enums.IsValid.Decimal)
                comparisonString = "a decimal";
            else if (Type == Enums.IsValid.Domain)
                comparisonString = "a domain";
            else if (Type == Enums.IsValid.Integer)
                comparisonString = "an integer";
            return string.Format(CultureInfo.InvariantCulture, ErrorMessageString, name, comparisonString);
        }

        /// <summary>
        /// Determines if the property is valid
        /// </summary>
        /// <param name="value">Value to check</param>
        /// <param name="validationContext">Validation context</param>
        /// <returns>The validation result</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var tempvalue = value as string;
            if(Type==Enums.IsValid.CreditCard)
                return tempvalue.Is(StringCompare.CreditCard) ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            if(Type==Enums.IsValid.Decimal)
                return tempvalue != null && Regex.IsMatch(tempvalue, @"^(\d+)+(\.\d+)?$|^(\d+)?(\.\d+)+$") ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            if (Type == Enums.IsValid.Domain)
                return tempvalue != null && Regex.IsMatch(tempvalue, @"^(http|https|ftp)://([a-zA-Z0-9_-]*(?:\.[a-zA-Z0-9_-]*)+):?([0-9]+)?/?") ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            if (Type == Enums.IsValid.Integer)
                return tempvalue != null && Regex.IsMatch(tempvalue, @"^\d+$") ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            return ValidationResult.Success;
        }

        ///// <summary>
        ///// Gets the client side validation rules
        ///// </summary>
        ///// <param name="metadata">Model meta data</param>
        ///// <param name="context">Controller context</param>
        ///// <returns>The list of client side validation rules</returns>
        //public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        //{
        //    var rule = new ModelClientValidationRule {ErrorMessage = FormatErrorMessage(metadata.GetDisplayName())};
        //    rule.ValidationParameters.Add("Type", Type.ToString());
        //    rule.ValidationType = "Is";
        //    return new[] { rule };
        //}

        #endregion
    }
}