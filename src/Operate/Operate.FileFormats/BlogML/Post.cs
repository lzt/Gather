﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Globalization;
using System.Text;
using System.Xml.Linq;

#endregion

namespace Operate.FileFormats.BlogML
{
    /// <summary>
    /// Post class
    /// </summary>
    public sealed class Post
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Post()
        {
            Authors = new Authors();
            Categories = new Categories();
            Tags = new Tags();
            Comments = new Comments();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="element">Element containing the post info</param>
        public Post(XElement element)
        {
              if (element.IsNull()) { throw new ArgumentNullException("element"); }
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
            ID = element.Attribute("id") != null ? element.Attribute("id").Value : "";
            PostURL = element.Attribute("post-url") != null ? element.Attribute("post-url").Value : "";
            DateCreated = element.Attribute("date-created") != null ? DateTime.Parse(element.Attribute("date-created").Value, CultureInfo.InvariantCulture) : DateTime.MinValue;
            DateModified = element.Attribute("date-modified") != null ? DateTime.Parse(element.Attribute("date-modified").Value, CultureInfo.InvariantCulture) : DateCreated;
            if (element.Element("title") != null)
            {
                var xElement = element.Element("title");
                if (xElement != null) Title = xElement.Value;
            }
            if (element.Element("content") != null)
            {
                var xElement1 = element.Element("content");
                if (xElement1 != null) Content = xElement1.Value;
            }
            if (element.Element("post-name") != null)
            {
                var element1 = element.Element("post-name");
                if (element1 != null) PostName = element1.Value;
            }
            if (element.Element("excerpt") != null)
            {
                var xElement2 = element.Element("excerpt");
                if (xElement2 != null) Excerpt = xElement2.Value;
            }
            if (element.Element("authors") != null)
                Authors = new Authors(element.Element("authors"));
            if (element.Element("categories") != null)
                Categories = new Categories(element.Element("categories"));
            if (element.Element("tags") != null)
                Tags = new Tags(element.Element("tags"));
            if (element.Element("comments") != null)
                Comments = new Comments(element.Element("comments"));
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// ID of the post
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// URL of the post
        /// </summary>
        public string PostURL { get; set; }

        /// <summary>
        /// Title of the post
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Content of the post
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Excerpt of the post
        /// </summary>
        public string Excerpt { get; set; }

        /// <summary>
        /// Name of the post
        /// </summary>
        public string PostName { get; set; }

        /// <summary>
        /// Date the post was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Date the post was modified
        /// </summary>
        public DateTime DateModified { get; set; }

        /// <summary>
        /// Authors of the post
        /// </summary>
        public Authors Authors { get; set; }

        /// <summary>
        /// Categories associated with the post
        /// </summary>
        public Categories Categories { get; set; }

        /// <summary>
        /// Tags associated with the post
        /// </summary>
        public Tags Tags { get; set; }

        /// <summary>
        /// Comments associated with the post
        /// </summary>
        public Comments Comments { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the object to a string
        /// </summary>
        /// <returns>The object as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendFormat(CultureInfo.InvariantCulture, "<post id=\"{0}\" date-created=\"{1}\" date-modified=\"{2}\" approved=\"true\" post-url=\"{3}\" type=\"normal\" hasexcerpt=\"true\" views=\"0\" is-published=\"True\">\n", ID, DateCreated.ToString("yyyy-MM-ddThh:mm:ss", CultureInfo.InvariantCulture), DateModified.ToString("yyyy-MM-ddThh:mm:ss", CultureInfo.InvariantCulture), PostURL);
            builder.AppendFormat(CultureInfo.InvariantCulture, "<title type=\"text\"><![CDATA[{0}]]></title>\n", Title);
            builder.AppendFormat(CultureInfo.InvariantCulture, "<content type=\"text\"><![CDATA[{0}]]></content>\n", Content);
            builder.AppendFormat(CultureInfo.InvariantCulture, "<post-name type=\"text\"><![CDATA[{0}]]></post-name>\n", PostName);
            builder.AppendFormat(CultureInfo.InvariantCulture, "<excerpt type=\"text\"><![CDATA[{0}]]></excerpt>\n", Excerpt);
            builder.AppendLine(Authors.ToString());
            builder.AppendLine(Categories.ToString());
            builder.AppendLine(Tags.ToString());
            builder.AppendLine(Comments.ToString());
            builder.AppendLine("<trackbacks />");
            builder.AppendLine("</post>");
            return builder.ToString();
        }

        #endregion
    }
}