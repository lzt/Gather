﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using Operate.FileFormats.BaseClasses;

#endregion

namespace Operate.FileFormats.BlogML
{
    /// <summary>
    /// BlogML class
    /// </summary>
    public sealed class BlogML:StringFormatBase<BlogML>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BlogML()
        {
            Authors = new Authors();
            Categories = new Categories();
            Posts = new Posts();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="location">Location of the XML file</param>
        public BlogML(string location)
        {
            InternalLoad(location);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Title of the blog
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Sub title of the blog
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// Authors of the blog
        /// </summary>
        public Authors Authors { get; set; }

        /// <summary>
        /// Categories of the blog (doesn't include tags)
        /// </summary>
        public Categories Categories { get; set; }

        /// <summary>
        /// Posts of the blog
        /// </summary>
        public Posts Posts { get; set; }

        /// <summary>
        /// Date created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Root URL
        /// </summary>
        public string RootURL { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected override void LoadFromData(string data)
        {
            XDocument document = XDocument.Parse(data);
            foreach (XElement blog in document.Elements("blog"))
            {
                DateCreated = blog.Attribute("date-created") != null ? DateTime.Parse(blog.Attribute("date-created").Value, CultureInfo.InvariantCulture) : DateTime.Now;
                RootURL = blog.Attribute("root-url") != null ? blog.Attribute("root-url").Value : "";
                if (blog.Element("title") != null)
                {
                    var xElement = blog.Element("title");
                    if (xElement != null) Title = xElement.Value;
                }
                if (blog.Element("sub-title") != null)
                {
                    var element = blog.Element("sub-title");
                    if (element != null) SubTitle = element.Value;
                }
                if (blog.Element("authors") != null)
                    Authors = new Authors(blog.Element("authors"));
                if (blog.Element("categories") != null)
                    Categories = new Categories(blog.Element("categories"));
                if (blog.Element("posts") != null)
                    Posts = new Posts(blog.Element("posts"));
            }
        }

        /// <summary>
        /// Converts the object to a string
        /// </summary>
        /// <returns>The object as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>")
                .AppendFormat("<blog root-url=\"{0}\" date-created=\"{1}\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.blogml.com/2006/09/BlogML\">\n", RootURL, DateCreated.ToString("yyyy-MM-ddThh:mm:ss", CultureInfo.InvariantCulture))
                .AppendFormat("<title type=\"text\"><![CDATA[{0}]]></title>\n", Title)
                .AppendFormat("<sub-title type=\"text\"><![CDATA[{0}]]></sub-title>\n", SubTitle)
                .Append(Authors)
                .Append(Categories)
                .Append(Posts)
                .AppendLine("</blog>");
            return builder.ToString();
        }

        #endregion
    }
}