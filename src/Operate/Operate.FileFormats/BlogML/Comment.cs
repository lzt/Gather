﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Globalization;
using System.Text;
using System.Xml.Linq;

#endregion

namespace Operate.FileFormats.BlogML
{
    /// <summary>
    /// Comment class
    /// </summary>
    public sealed class Comment
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Comment()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="element">Element containing the post info</param>
        public Comment(XElement element)
        {
              if (element.IsNull()) { throw new ArgumentNullException("element"); }
            DateCreated = element.Attribute("date-created") != null ? DateTime.Parse(element.Attribute("date-created").Value, CultureInfo.InvariantCulture) : DateTime.MinValue;
            Approved = element.Attribute("approved") != null && bool.Parse(element.Attribute("approved").Value);
            UserName = element.Attribute("user-name") != null ? element.Attribute("user-name").Value : "";
            UserEmail = element.Attribute("user-email") != null ? element.Attribute("user-email").Value : "";
            UserIP = element.Attribute("user-ip") != null ? element.Attribute("user-ip").Value : "";
            UserURL = element.Attribute("user-url") != null ? element.Attribute("user-url").Value : "";
            ID = element.Attribute("id") != null ? element.Attribute("id").Value : "";
            if (element.Element("title") != null)
            {
                var xElement = element.Element("title");
                if (xElement != null) Title = xElement.Value;
            }
            if (element.Element("content") != null)
            {
                var xElement1 = element.Element("content");
                if (xElement1 != null) Content = xElement1.Value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Title of the comment
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Actual content of the comment
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Date created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Determines if the comment is approved
        /// </summary>
        public bool Approved { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// User IP
        /// </summary>
        public string UserIP { get; set; }

        /// <summary>
        /// User URL
        /// </summary>
        public string UserURL { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the object to a string
        /// </summary>
        /// <returns>The object as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendFormat(CultureInfo.InvariantCulture, "<comment id=\"{0}\" parentid=\"00000000-0000-0000-0000-000000000000\" date-created=\"{1}\" date-modified=\"{1}\" approved=\"true\" user-name=\"{2}\" user-email=\"{3}\" user-ip=\"{4}\" user-url=\"{5}\">\n", ID, DateCreated.ToString("yyyy-MM-ddThh:mm:ss", CultureInfo.InvariantCulture), UserName, UserEmail, UserIP, UserURL);
            builder.AppendFormat(CultureInfo.InvariantCulture, "<title type=\"text\"><![CDATA[{0}]]></title>\n<content type=\"text\"><![CDATA[{1}]]></content>\n</comment>\n", Title, Content);
            return builder.ToString();
        }

        #endregion
    }
}