﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Globalization;
using System.Text;
using System.Xml.Linq;

#endregion

namespace Operate.FileFormats.BlogML
{
    /// <summary>
    /// Individual author
    /// </summary>
    public sealed class Author
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Author()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="element">XML element containing the author info</param>
        public Author(XElement element)
        {
              if (element.IsNull()) { throw new ArgumentNullException("element"); }
            ID = element.Attribute("id") != null ? element.Attribute("id").Value : "";
            Email = element.Attribute("email") != null ? element.Attribute("email").Value : "";
            REF = element.Attribute("ref") != null ? element.Attribute("ref").Value : "";
            DateCreated = element.Attribute("date-created") != null ? DateTime.Parse(element.Attribute("date-created").Value, CultureInfo.InvariantCulture) : DateTime.Now;
            DateModified = element.Attribute("date-modified") != null ? DateTime.Parse(element.Attribute("date-modified").Value, CultureInfo.InvariantCulture) : DateTime.Now;
            if (element.Element("title") != null)
            {
                var xElement = element.Element("title");
                if (xElement != null) Title = xElement.Value;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// ID of the author
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Email address of the author
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The person's title (most likely their name)
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Determines if this should be a reference to an author
        /// </summary>
        public string REF { get; set; }

        /// <summary>
        /// Date created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Date modified
        /// </summary>
        public DateTime DateModified { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the author to a string
        /// </summary>
        /// <returns>The author as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("<author ");
            if (string.IsNullOrEmpty(REF))
            {
                builder.AppendFormat(CultureInfo.InvariantCulture, "id=\"{0}\" date-created=\"{1}\" date-modified=\"{2}\" approved=\"true\" email=\"{3}\">\n", ID, DateCreated.ToString("yyyy-MM-ddThh:mm:ss", CultureInfo.InvariantCulture), DateModified.ToString("yyyy-MM-ddThh:mm:ss", CultureInfo.InvariantCulture), Email);
                builder.AppendFormat(CultureInfo.InvariantCulture, "<title type=\"text\"><![CDATA[{0}]]></title>\n", Title);
                builder.AppendLine("</author>");
            }
            else
            {
                builder.AppendFormat("ref=\"{0}\" />\n", REF);
            }
            return builder.ToString();
        }

        #endregion
    }
}