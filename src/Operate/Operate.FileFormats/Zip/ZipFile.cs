﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.IO;
using System.IO.Packaging;
using System.Linq;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods;

#endregion

namespace Operate.FileFormats.Zip
{
    /// <summary>
    /// Helper class for dealing with zip files
    /// </summary>
    public sealed class ZipFile : IDisposable
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath">Path to the zip file</param>
        /// <param name="overwrite">Should the zip file be overwritten?</param>
        public ZipFile(string filePath, bool overwrite = true)
        {
            if (filePath.IsNullOrEmpty()) { throw new ArgumentNullException("filePath"); }
            ZipFileStream = new FileStream(filePath, overwrite ? FileMode.Create : FileMode.OpenOrCreate);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Zip file's FileStream
        /// </summary>
        private FileStream ZipFileStream { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Adds a folder to the zip file
        /// </summary>
        /// <param name="folder">Folder to add</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.EndsWith(System.String,System.StringComparison)")]
        public void AddFolder(string folder)
        {
            if (folder.IsNullOrEmpty()) { throw new ArgumentNullException("folder"); }
            folder = new DirectoryInfo(folder).FullName;
            if (folder.EndsWith(@"\", StringComparison.InvariantCulture))
                folder = folder.Remove(folder.Length - 1);
            using (Package package = Package.Open(ZipFileStream, FileMode.OpenOrCreate))
            {
                new DirectoryInfo(folder)
                    .GetFiles()
                    // ReSharper disable once AccessToDisposedClosure
                    .ForEach(x => AddFile(x.FullName.Replace(folder, ""), x, package));
            }
        }

        /// <summary>
        /// Adds a file to the zip file
        /// </summary>
        /// <param name="file">File to add</param>
        public void AddFile(string file)
        {
            if (file.IsNullOrEmpty()) { throw new ArgumentNullException("file"); }
            var tempFileInfo = new FileInfo(file);
            if (!tempFileInfo.Exists)
                throw new ArgumentException("File does not exist");
            using (Package package = Package.Open(ZipFileStream, FileMode.OpenOrCreate))
            {
                AddFile(tempFileInfo.Name, tempFileInfo, package);
            }
        }

        /// <summary>
        /// Uncompresses the zip file to the specified folder
        /// </summary>
        /// <param name="folder">Folder to uncompress the file in</param>
        public void UncompressFile(string folder)
        {
            if (folder.IsNullOrEmpty()) { throw new ArgumentNullException("folder"); }
            new DirectoryInfo(folder).Create();
            folder = new DirectoryInfo(folder).FullName;
            using (Package package = Package.Open(ZipFileStream, FileMode.Open, FileAccess.Read))
            {
                foreach (PackageRelationship relationship in package.GetRelationshipsByType("http://schemas.microsoft.com/opc/2006/sample/document"))
                {
                    Uri uriTarget = PackUriHelper.ResolvePartUri(new Uri("/", UriKind.Relative), relationship.TargetUri);
                    PackagePart document = package.GetPart(uriTarget);
                    Extract(document, folder);
                }
                if (File.Exists(folder + @"\[Content_Types].xml"))
                    File.Delete(folder + @"\[Content_Types].xml");
            }
        }

        /// <summary>
        /// Extracts an individual file
        /// </summary>
        /// <param name="document">Document to extract</param>
        /// <param name="folder">Folder to extract it into</param>
        private void Extract(PackagePart document, string folder)
        {
            if (folder.IsNullOrEmpty()) { throw new ArgumentNullException("folder"); }
            var urlDecode = System.Web.HttpUtility.UrlDecode(document.Uri.ToString());
            if (urlDecode != null)
            {
                string location = folder + urlDecode.Replace('\\', '/');
                var dirName = Path.GetDirectoryName(location);
                if (dirName != null) new DirectoryInfo(dirName).Create();
                var data = new byte[1024];
                using (var fileStream = new FileStream(location, FileMode.Create))
                {
                    Stream documentStream = document.GetStream();
                    while (true)
                    {
                        int size = documentStream.Read(data, 0, 1024);
                        fileStream.Write(data, 0, size);
                        if (size != 1024)
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Adds a file to the zip file
        /// </summary>
        /// <param name="file">File to add</param>
        /// <param name="fileInfo">File information</param>
        /// <param name="package">Package to add the file to</param>
        private void AddFile(string file, FileInfo fileInfo, Package package)
        {
            if (file.IsNullOrEmpty()) { throw new ArgumentNullException("file"); }
            if (!fileInfo.Exists) { throw new FileNotFoundException("FileInfo does not exist"); }
            Uri uriPath = PackUriHelper.CreatePartUri(new Uri(file, UriKind.Relative));
            PackagePart packagePart = package.CreatePart(uriPath, System.Net.Mime.MediaTypeNames.Text.Xml, CompressionOption.Maximum);
            byte[] data = fileInfo.ReadBinary();
            if (packagePart != null)
            {
                packagePart.GetStream().Write(data, 0, data.Count());
                package.CreateRelationship(packagePart.Uri, TargetMode.Internal, "http://schemas.microsoft.com/opc/2006/sample/document");
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Disposes the object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of the objects
        /// </summary>
        /// <param name="disposing">True to dispose of all resources, false only disposes of native resources</param>
        // ReSharper disable UnusedParameter.Local
        private void Dispose(bool disposing)
        // ReSharper restore UnusedParameter.Local
        {
            if (ZipFileStream != null)
            {
                ZipFileStream.Close();
                ZipFileStream.Dispose();
                ZipFileStream = null;
            }
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~ZipFile()
        {
            Dispose(false);
        }

        #endregion
    }
}