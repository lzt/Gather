﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Text;
using System.Xml.Linq;
using Operate.ExtensionMethods;
using Operate.FileFormats.BaseClasses;

#endregion

namespace Operate.FileFormats.RSD
{
    /// <summary>
    /// Basic helper for RSD
    /// </summary>
    public sealed class RSD : StringFormatBase<RSD>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public RSD()
        {
            APIs = new APIs();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileContent">Content of the RSD file</param>
        public RSD(string fileContent)
        {
            if (fileContent.IsNullOrEmpty()) { throw new ArgumentNullException("fileContent"); }
            LoadContent(fileContent);
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Engine name
        /// </summary>
        public string EngineName { get; set; }

        /// <summary>
        /// Link to the engine
        /// </summary>
        public string EngineLink { get; set; }

        /// <summary>
        /// Link to the home page
        /// </summary>
        public string HomePageLink { get; set; }

        /// <summary>
        /// API definitions
        /// </summary>
        public APIs APIs { get; set; }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected override void LoadFromData(string data)
        {
            LoadContent(data);
        }

        private void LoadContent(string content)
        {
            XDocument document = XDocument.Parse(content);
            foreach (XElement service in document.Elements("RSD").Elements("service"))
            {
                if (service.Element("engineName") != null)
                {
                    var xElement = service.Element("engineName");
                    if (xElement != null) EngineName = xElement.Value;
                }
                if (service.Element("engineLink") != null)
                {
                    var element = service.Element("engineLink");
                    if (element != null) EngineLink = element.Value;
                }
                if (service.Element("homePageLink") != null)
                {
                    var xElement1 = service.Element("homePageLink");
                    if (xElement1 != null) HomePageLink = xElement1.Value;
                }
                if (service.Element("apis") != null)
                    APIs = new APIs(service.Element("apis"));
            }
        }

        /// <summary>
        /// Outputs the RSD file
        /// </summary>
        /// <returns>return the properly formatted RSD file</returns>
        public override string ToString()
        {
            var builder = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?><rsd version=\"1.0\"><service><engineName>");
            builder.Append(EngineName).Append("</engineName><engineLink>").Append(EngineLink).Append("</engineLink><homePageLink>")
                .Append(HomePageLink).Append("</homePageLink>");
            builder.Append(APIs);
            builder.Append("</service></rsd>");
            return builder.ToString();
        }

        #endregion
    }
}