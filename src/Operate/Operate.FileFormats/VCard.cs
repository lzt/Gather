﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Operate.FileFormats.BaseClasses;

#endregion

namespace Operate.FileFormats
{
    /// <summary>
    /// Class for creating vCards
    /// </summary>
    public class VCard : StringFormatBase<VCard>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public VCard()
        {
            Relationships = new List<Relationship>();
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Gets the vCard
        /// </summary>
        /// <returns>A vCard in string format</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public virtual string GetVCard()
        {
            var builder = new StringBuilder();
            builder.Append("BEGIN:VCARD\r\nVERSION:2.1\r\n");
            builder.Append("FN:");
            if (!string.IsNullOrEmpty(Prefix))
            {
                builder.Append(Prefix).Append(" ");
            }
            builder.Append(FirstName + " ");
            if (!string.IsNullOrEmpty(MiddleName))
            {
                builder.Append(MiddleName).Append(" ");
            }
            builder.Append(LastName);
            if (!string.IsNullOrEmpty(Suffix))
            {
                builder.Append(" ").Append(Suffix);
            }
            builder.Append("\r\n");

            builder.Append("N:");
            builder.Append(LastName).Append(";").Append(FirstName).Append(";")
                .Append(MiddleName).Append(";").Append(Prefix).Append(";")
                .Append(Suffix).Append("\r\n");
            builder.Append("TEL;WORK:").Append(DirectDial).Append("\r\n");
            builder.Append("EMAIL;INTERNET:").Append(StripHTML(Email)).Append("\r\n");
            builder.Append("TITLE:").Append(Title).Append("\r\n");
            builder.Append("ORG:").Append(Organization).Append("\r\n");
            builder.Append("END:VCARD\r\n");
            return builder.ToString();
        }

        /// <summary>
        /// Gets the hCard version of the vCard
        /// </summary>
        /// <returns>A hCard in string format</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public virtual string GetHCard()
        {
            var builder = new StringBuilder();
            builder.Append("<div class=\"vcard\">");
            if (string.IsNullOrEmpty(Url))
            {
                builder.Append("<div class=\"fn\">");
                if (!string.IsNullOrEmpty(Prefix))
                {
                    builder.Append(Prefix).Append(" ");
                }
                builder.Append(FirstName).Append(" ");
                if (!string.IsNullOrEmpty(MiddleName))
                {
                    builder.Append(MiddleName).Append(" ");
                }
                builder.Append(LastName);
                if (!string.IsNullOrEmpty(Suffix))
                {
                    builder.Append(" ").Append(Suffix);
                }
                builder.Append("</div>");
            }
            else
            {
                builder.Append("<a class=\"fn url\" href=\"").Append(Url).Append("\"");
                if (Relationships.Count > 0)
                {
                    builder.Append(" rel=\"");
                    foreach (Relationship relationship in Relationships)
                    {
                        builder.Append(relationship.ToString()).Append(" ");
                    }
                    builder.Append("\"");
                }
                builder.Append(">");
                if (!string.IsNullOrEmpty(Prefix))
                {
                    builder.Append(Prefix).Append(" ");
                }
                builder.Append(FirstName).Append(" ");
                if (!string.IsNullOrEmpty(MiddleName))
                {
                    builder.Append(MiddleName).Append(" ");
                }
                builder.Append(LastName);
                if (!string.IsNullOrEmpty(Suffix))
                {
                    builder.Append(" ").Append(Suffix);
                }
                builder.Append("</a>");
            }
            builder.Append("<span class=\"n\" style=\"display:none;\"><span class=\"family-name\">").Append(LastName).Append("</span><span class=\"given-name\">").Append(FirstName).Append("</span></span>");
            if (!string.IsNullOrEmpty(DirectDial))
            {
                builder.Append("<div class=\"tel\"><span class=\"type\">Work</span> ").Append(DirectDial).Append("</div>");
            }
            if (!string.IsNullOrEmpty(Email))
            {
                builder.Append("<div>Email: <a class=\"email\" href=\"mailto:").Append(StripHTML(Email)).Append("\">").Append(StripHTML(Email)).Append("</a></div>");
            }
            if (!string.IsNullOrEmpty(Organization))
            {
                builder.Append("<div>Organization: <span class=\"org\">").Append(Organization).Append("</span></div>");
            }
            if (!string.IsNullOrEmpty(Title))
            {
                builder.Append("<div>Title: <span class=\"title\">").Append(Title).Append("</span></div>");
            }
            builder.Append("</div>");
            return builder.ToString();
        }

        /// <summary>
        /// Gets the VCard as a string
        /// </summary>
        /// <returns>VCard as a string</returns>
        public override string ToString()
        {
            return GetVCard();
        }

        #endregion

        #region Private/Protected Functions

        private static string StripHTML(string html)
        {
            if (string.IsNullOrEmpty(html))
                return string.Empty;

            html = STRIP_HTML_REGEX.Replace(html, string.Empty);
            html = html.Replace("&nbsp;", " ");
            return html.Replace("&#160;", string.Empty);
        }

        // ReSharper disable InconsistentNaming
        private static readonly Regex STRIP_HTML_REGEX = new Regex("<[^>]*>", RegexOptions.Compiled);
        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected override void LoadFromData(string data)
        {
            string content = data;
            foreach (Match tempMatch in Regex.Matches(content, "(?<Title>[^:]+):(?<Value>.*)"))
            {
                if (tempMatch.Groups["Title"].Value.ToUpperInvariant() == "N")
                {
                    string[] name = tempMatch.Groups["Value"].Value.Split(';');
                    if (name.Length > 0)
                    {
                        LastName = name[0];
                        if (name.Length > 1)
                            FirstName = name[1];
                        if (name.Length > 2)
                            MiddleName = name[2];
                        if (name.Length > 3)
                            Prefix = name[3];
                        if (name.Length > 4)
                            Suffix = name[4];
                    }
                }
                else if (tempMatch.Groups["Title"].Value.ToUpperInvariant() == "TEL;WORK")
                {
                    DirectDial = tempMatch.Groups["Value"].Value;
                }
                else if (tempMatch.Groups["Title"].Value.ToUpperInvariant() == "EMAIL;INTERNET")
                {
                    Email = tempMatch.Groups["Value"].Value;
                }
                else if (tempMatch.Groups["Title"].Value.ToUpperInvariant() == "TITLE")
                {
                    Title = tempMatch.Groups["Value"].Value;
                }
                else if (tempMatch.Groups["Title"].Value.ToUpperInvariant() == "ORG")
                {
                    Organization = tempMatch.Groups["Value"].Value;
                }
            }
        }

        #endregion

        #region Properties
        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Prefix
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Suffix
        /// </summary>
        public string Suffix { get; set; }

        /// <summary>
        /// Work phone number of the individual
        /// </summary>
        public string DirectDial { get; set; }

        /// <summary>
        /// Email of the individual
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Title of the person
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Organization the person belongs to
        /// </summary>
        public string Organization { get; set; }

        /// <summary>
        /// Relationship to the person (uses XFN)
        /// </summary>
        public ICollection<Relationship> Relationships { get; private set; }

        /// <summary>
        /// Url to the person's site
        /// </summary>
        public string Url { get; set; }

        #endregion
    }

    #region Enums
    /// <summary>
    /// Enum defining relationships (used for XFN markup)
    /// </summary>
    public enum Relationship
    {
        /// <summary>
        /// Friend
        /// </summary>
        Friend,
        /// <summary>
        /// Acquaintance
        /// </summary>
        Acquaintance,
        /// <summary>
        /// Contact
        /// </summary>
        Contact,
        /// <summary>
        /// Met
        /// </summary>
        Met,
        /// <summary>
        /// Coworker
        /// </summary>
        CoWorker,
        /// <summary>
        /// Colleague
        /// </summary>
        Colleague,
        /// <summary>
        /// Coresident
        /// </summary>
        CoResident,
        /// <summary>
        /// Neighbor
        /// </summary>
        Neighbor,
        /// <summary>
        /// Child
        /// </summary>
        Child,
        /// <summary>
        /// Parent
        /// </summary>
        Parent,
        /// <summary>
        /// Sibling
        /// </summary>
        Sibling,
        /// <summary>
        /// Spouse
        /// </summary>
        Spouse,
        /// <summary>
        /// Kin
        /// </summary>
        Kin,
        /// <summary>
        /// Muse
        /// </summary>
        Muse,
        /// <summary>
        /// Crush
        /// </summary>
        Crush,
        /// <summary>
        /// Date
        /// </summary>
        Date,
        /// <summary>
        /// Sweetheart
        /// </summary>
        Sweetheart,
        /// <summary>
        /// Me
        /// </summary>
        Me
    }
    #endregion
}