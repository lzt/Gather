﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Generic;
using System.Data;
using Operate.ExtensionMethods;
using Operate.FileFormats.Delimited;

#endregion

namespace Operate.FileFormats.ExtensionMethods
{
    /// <summary>
    /// Extension methods pertaining to file formats
    /// </summary>
    public static class ExtensionMethods
    {
        #region Functions

        #region ToCSV
        
        /// <summary>
        /// Converts an IEnumerable to a CSV file
        /// </summary>
        /// <typeparam name="T">Type of the items within the list</typeparam>
        /// <param name="list">The list to convert</param>
        /// <returns>The CSV file containing the list</returns>
        public static CSV.CSV ToCSV<T>(this IEnumerable<T> list)
        {
            return (CSV.CSV)list.ToDataTable();
        }

        #endregion

        #region ToDelimitedFile

        /// <summary>
        /// Converts an IEnumerable to a delimited file
        /// </summary>
        /// <typeparam name="T">Type of the items within the list</typeparam>
        /// <param name="list">The list to convert</param>
        /// <param name="delimiter">Delimiter to use</param>
        /// <returns>The delimited file containing the list</returns>
        public static GenericDelimited.GenericDelimited ToDelimitedFile<T>(this IEnumerable<T> list, string delimiter = "\t")
        {
            return list.ToDataTable().ToDelimitedFile(delimiter);
        }

        /// <summary>
        /// Converts an IEnumerable to a delimited file
        /// </summary>
        /// <param name="data">The DataTable to convert</param>
        /// <param name="delimiter">Delimiter to use</param>
        /// <returns>The delimited file containing the list</returns>
        public static GenericDelimited.GenericDelimited ToDelimitedFile(this DataTable data, string delimiter = "\t")
        {
            var returnValue = new GenericDelimited.GenericDelimited(delimiter);
            if (data==null)
                return returnValue;
            var tempRow = new Delimited.Row(delimiter);
            foreach (DataColumn column in data.Columns)
            {
                tempRow.Add(new Cell(column.ColumnName));
            }
            returnValue.Add(tempRow);
            foreach (DataRow row in data.Rows)
            {
                tempRow = new Delimited.Row(delimiter);
                for (int x = 0; x < data.Columns.Count; ++x)
                {
                    tempRow.Add(new Cell(row.ItemArray[x].ToString()));
                }
                returnValue.Add(tempRow);
            }
            return returnValue;
        }

        #endregion

        #endregion
    }
}
