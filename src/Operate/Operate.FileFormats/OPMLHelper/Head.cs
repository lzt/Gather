﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Globalization;
using System.Text;
using System.Xml.Linq;

#endregion

namespace Operate.FileFormats.OPMLHelper
{
    /// <summary>
    /// Head class
    /// </summary>
    public sealed class Head
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public Head()
        {
            Docs = "http://www.opml.org/spec2";
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="element">XmlElement containing the header information</param>
        public Head(XElement element)
        {
              if (element.IsNull()) { throw new ArgumentNullException("element"); }
            if (element.Element("title") != null)
            {
                var xElement = element.Element("title");
                if (xElement != null) Title = xElement.Value;
            }
            if (element.Element("ownerName") != null)
            {
                var xElement = element.Element("ownerName");
                if (xElement != null) OwnerName = xElement.Value;
            }
            if (element.Element("ownerEmail") != null)
            {
                var element1 = element.Element("ownerEmail");
                if (element1 != null) OwnerEmail = element1.Value;
            }
            if (element.Element("dateCreated") != null)
            {
                var xElement2 = element.Element("dateCreated");
                if (xElement2 != null)
                    DateCreated = DateTime.Parse(xElement2.Value, CultureInfo.InvariantCulture);
            }
            if (element.Element("dateModified") != null)
            {
                var element2 = element.Element("dateModified");
                if (element2 != null)
                    DateModified = DateTime.Parse(element2.Value, CultureInfo.InvariantCulture);
            }
            if (element.Element("docs") != null)
            {
                var xElement3 = element.Element("docs");
                if (xElement3 != null) Docs = xElement3.Value;
            }
        }

        #endregion

        #region Properties
        /// <summary>
        /// Title of the OPML document
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Date it was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Date it was last modified
        /// </summary>
        public DateTime DateModified { get; set; }

        /// <summary>
        /// Owner of the file
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// Owner's email address
        /// </summary>
        public string OwnerEmail { get; set; }

        /// <summary>
        /// Location of the OPML spec
        /// </summary>
        public string Docs { get; set; }
        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the head to a string
        /// </summary>
        /// <returns>The head as a string</returns>
        public override string ToString()
        {
            var headString = new StringBuilder();
            headString.Append("<head>");
            headString.Append("<title>" + Title + "</title>\r\n");
            headString.Append("<dateCreated>" + DateCreated.ToString("R", CultureInfo.InvariantCulture) + "</dateCreated>\r\n");
            headString.Append("<dateModified>" + DateModified.ToString("R", CultureInfo.InvariantCulture) + "</dateModified>\r\n");
            headString.Append("<ownerName>" + OwnerName + "</ownerName>\r\n");
            headString.Append("<ownerEmail>" + OwnerEmail + "</ownerEmail>\r\n");
            headString.Append("<docs>" + Docs + "</docs>\r\n");
            headString.Append("</head>\r\n");
            return headString.ToString();
        }

        #endregion
    }
}