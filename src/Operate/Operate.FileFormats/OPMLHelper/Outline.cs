﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Text;
using System.Xml.Linq;

#endregion

namespace Operate.FileFormats.OPMLHelper
{
    /// <summary>
    /// Outline class
    /// </summary>
    public class Outline
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Outline()
        {
            Outlines = new List<Outline>();
        }

        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="element">Element containing outline information</param>
        public Outline(XElement element)
        {
              if (element.IsNull()) { throw new ArgumentNullException("element"); }
            if (element.Attribute("text") != null)
            {
                Text = element.Attribute("text").Value;
            }
            if (element.Attribute("description") != null)
            {
                Description = element.Attribute("description").Value;
            }
            if (element.Attribute("htmlUrl") != null)
            {
                HTMLUrl = element.Attribute("htmlUrl").Value;
            }
            if (element.Attribute("type") != null)
            {
                Type = element.Attribute("type").Value;
            }
            if (element.Attribute("language") != null)
            {
                Language = element.Attribute("language").Value;
            }
            if (element.Attribute("title") != null)
            {
                Title = element.Attribute("title").Value;
            }
            if (element.Attribute("version") != null)
            {
                Version = element.Attribute("version").Value;
            }
            if (element.Attribute("xmlUrl") != null)
            {
                XMLUrl = element.Attribute("xmlUrl").Value;
            }

            foreach (XElement child in element.Elements("outline"))
            {
                Outlines.Add(new Outline(child));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Outline list
        /// </summary>
        public ICollection<Outline> Outlines { get; private set; }

        /// <summary>
        /// Url of the XML file
        /// </summary>
        public string XMLUrl { get; set; }

        /// <summary>
        /// Version number
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Title of the item
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Language used
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// HTML Url
        /// </summary>
        public string HTMLUrl { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the outline to a string
        /// </summary>
        /// <returns>The outline as a string</returns>
        public override string ToString()
        {
            var outlineString = new StringBuilder();
            outlineString.Append("<outline text=\"" + Text + "\"");
            if (!string.IsNullOrEmpty(XMLUrl))
            {
                outlineString.Append(" xmlUrl=\"" + XMLUrl + "\"");
            }
            if (!string.IsNullOrEmpty(Version))
            {
                outlineString.Append(" version=\"" + Version + "\"");
            }
            if (!string.IsNullOrEmpty(Title))
            {
                outlineString.Append(" title=\"" + Title + "\"");
            }
            if (!string.IsNullOrEmpty(Language))
            {
                outlineString.Append(" language=\"" + Language + "\"");
            }
            if (!string.IsNullOrEmpty(Type))
            {
                outlineString.Append(" type=\"" + Type + "\"");
            }
            if (!string.IsNullOrEmpty(HTMLUrl))
            {
                outlineString.Append(" htmlUrl=\"" + HTMLUrl + "\"");
            }
            if (!string.IsNullOrEmpty(Text))
            {
                outlineString.Append(" text=\"" + Text + "\"");
            }
            if (!string.IsNullOrEmpty(Description))
            {
                outlineString.Append(" description=\"" + Description + "\"");
            }
            if (Outlines.Count > 0)
            {
                outlineString.Append(">\r\n");
                foreach (Outline outline in Outlines)
                {
                    outlineString.Append(outline);
                }
                outlineString.Append("</outline>\r\n");
            }
            else
            {
                outlineString.Append(" />\r\n");
            }
            return outlineString.ToString();
        }

        #endregion
    }
}