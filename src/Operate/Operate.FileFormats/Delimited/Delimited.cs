﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Data;

using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;
using Operate.FileFormats.BaseClasses;

#endregion

namespace Operate.FileFormats.Delimited
{
    /// <summary>
    /// Base classs for delimited files (CSV, etc.)
    /// </summary>
    /// <typeparam name="T">Delimited</typeparam>
    public abstract class Delimited<T> : StringListFormatBase<T, Row>
        where T : Delimited<T>, new()
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        protected Delimited()
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileContent">File content</param>
        protected Delimited(string fileContent)
        {
            Parse(fileContent);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The delimiter used to seperate values (must be overridden)
        /// </summary>
        protected abstract string Delimiter { get; }

        #endregion

        #region Functions

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected override void LoadFromData(string data)
        {
            Parse(data);
        }

        /// <summary>
        /// Parses file content and adds it to the delimited file
        /// </summary>
        /// <param name="fileContent">File content</param>
        public void Parse(string fileContent)
        {
            if (fileContent.IsNullOrEmpty()) { throw new ArgumentNullException("fileContent"); }
            var tempSplitter = new Regex("[^\"\r\n]*(\r\n|\n|$)|(([^\"\r\n]*)(\"[^\"]*\")([^\"\r\n]*))*(\r\n|\n|$)");
            MatchCollection matches = tempSplitter.Matches(fileContent);
            matches.Where(x => !string.IsNullOrEmpty(x.Value))
                    .ForEach(x => Records.Add(new Row(x.Value, Delimiter)));
        }

        /// <summary>
        /// Converts the delimited file to a DataTable
        /// </summary>
        /// <param name="firstRowIsHeader">Determines if the first row should be treated as a header or not</param>
        /// <param name="headers">Headers for the columns if the first row is not a header</param>
        /// <returns>The delimited file as a DataTable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public DataTable ToDataTable(bool firstRowIsHeader = true, params string[] headers)
        {
            var returnValue = new DataTable {Locale = CultureInfo.CurrentCulture};
            if (firstRowIsHeader)
            {
                if (Records.Count > 0)
                    foreach (Cell cell in Records[0])
                        returnValue.Columns.Add(cell.Value);
            }
            else
            {
                foreach (string headerValue in headers)
                    returnValue.Columns.Add(headerValue);
            }
            for (int y = firstRowIsHeader ? 1 : 0; y < Records.Count; ++y)
            {
                var tempRow = new object[returnValue.Columns.Count];
                for (int x = 0; x < Records[y].Count; ++x)
                {
                    tempRow[x] = Records[y][x].Value;
                }
                returnValue.Rows.Add(tempRow);
            }
            return returnValue;
        }

        /// <summary>
        /// To string function
        /// </summary>
        /// <returns>A string containing the file information</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            Records.ForEach<Row>(x => builder.Append(x.ToString()));
            return builder.ToString();
        }


        /// <summary>
        /// Converts the string to the format specified
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <returns>The string as an object</returns>
        public static implicit operator Delimited<T>(DataTable value)
        {
            var returnValue = new T();
            if (value == null)
                return returnValue;
            var tempRow = new Row(returnValue.Delimiter);
            foreach (DataColumn column in value.Columns)
            {
                tempRow.Add(new Cell(column.ColumnName));
            }
            returnValue.Add(tempRow);
            foreach (DataRow row in value.Rows)
            {
                tempRow = new Row(returnValue.Delimiter);
                foreach (object t in row.ItemArray)
                {
                    tempRow.Add(new Cell(t.ToString()));
                }
                returnValue.Records.Add(tempRow);
            }
            return returnValue;
        }

        #endregion
    }
}