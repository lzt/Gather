﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Text;

#endregion

namespace Operate.FileFormats.Cisco
{
    /// <summary>
    /// Input item
    /// </summary>
    public class InputItem
    {
        #region Constructor

        #endregion

        #region Properties

        /// <summary>
        /// Display name
        /// </summary>
        public virtual string DisplayName { get; set; }

        /// <summary>
        /// Query string parameter
        /// </summary>
        public virtual string QueryStringParam { get; set; }

        /// <summary>
        /// Default value
        /// </summary>
        public virtual string DefaultValue { get; set; }

        /// <summary>
        /// input flags
        /// </summary>
        public virtual InputFlag InputFlags { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Exports the item as a properly formatted string
        /// </summary>
        /// <returns>The properly formatted string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("<InputItem><DisplayName>").Append(DisplayName).Append("</DisplayName><QueryStringParam>")
                .Append(QueryStringParam).Append("</QueryStringParam><DefaultValue>").Append(DefaultValue)
                .Append("</DefaultValue><InputFlags>");
            if (InputFlags == InputFlag.ASCII)
            {
                builder.Append("A");
            }
            else if (InputFlags == InputFlag.TelephoneNumber)
            {
                builder.Append("T");
            }
            else if (InputFlags == InputFlag.Numeric)
            {
                builder.Append("N");
            }
            else if (InputFlags == InputFlag.Equation)
            {
                builder.Append("E");
            }
            else if (InputFlags == InputFlag.Uppercase)
            {
                builder.Append("U");
            }
            else if (InputFlags == InputFlag.Lowercase)
            {
                builder.Append("L");
            }
            else if (InputFlags == InputFlag.Password)
            {
                builder.Append("P");
            }
            builder.Append("</InputFlags></InputItem>");
            return builder.ToString();
        }

        #endregion
    }
}