﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Globalization;
using System.Text;
using System.Xml;

#endregion

namespace Operate.FileFormats.RSSHelper
{
    /// <summary>
    /// Channel item for RSS feeds
    /// </summary>
    public sealed class Channel
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Channel()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="element">XML representation of the channel</param>

        public Channel(XmlElement element)
        {
            if (element.IsNull()) { throw new ArgumentNullException("element"); }
            if (!element.Name.Equals("channel", StringComparison.CurrentCultureIgnoreCase)) { throw new ArgumentException("Element is not a channel"); }
            if (element.OwnerDocument != null)
            {
                var namespaceManager = new XmlNamespaceManager(element.OwnerDocument.NameTable);
                var node = element.SelectSingleNode("./title", namespaceManager);
                if (node != null)
                {
                    Title = node.InnerText;
                }
                node = element.SelectSingleNode("./link", namespaceManager);
                if (node != null)
                {
                    Link = node.InnerText;
                }
                node = element.SelectSingleNode("./description", namespaceManager);
                if (node != null)
                {
                    Description = node.InnerText;
                }
                node = element.SelectSingleNode("./copyright", namespaceManager);
                if (node != null)
                {
                    Copyright = node.InnerText;
                }
                node = element.SelectSingleNode("./language", namespaceManager);
                if (node != null)
                {
                    Language = node.InnerText;
                }
                node = element.SelectSingleNode("./webmaster", namespaceManager);
                if (node != null)
                {
                    WebMaster = node.InnerText;
                }
                node = element.SelectSingleNode("./pubdate", namespaceManager);
                if (node != null)
                {
                    PubDate = DateTime.Parse(node.InnerText, CultureInfo.InvariantCulture);
                }
                XmlNodeList nodes = element.SelectNodes("./category", namespaceManager);
                if (nodes != null)
                    foreach (XmlNode tempNode in nodes)
                    {
                        Categories.Add(RSS.StripIllegalCharacters(tempNode.InnerText));
                    }
                node = element.SelectSingleNode("./docs", namespaceManager);
                if (node != null)
                {
                    Docs = node.InnerText;
                }
                node = element.SelectSingleNode("./ttl", namespaceManager);
                if (node != null)
                {
                    TTL = int.Parse(node.InnerText, CultureInfo.InvariantCulture);
                }
                node = element.SelectSingleNode("./image/url", namespaceManager);
                if (node != null)
                {
                    ImageUrl = node.InnerText;
                }
                nodes = element.SelectNodes("./item", namespaceManager);
                if (nodes != null)
                    foreach (XmlNode tempNode in nodes)
                    {
                        Items.Add(new Item((XmlElement)tempNode));
                    }
            }
        }

        #endregion

        #region Private Variables
        private string _title = string.Empty;
        private string _link = string.Empty;
        private string _description = string.Empty;
        private string _copyright = "Copyright " + DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture) + ". All rights reserved.";
        private string _language = "en-us";
        private string _webMaster = string.Empty;
        private DateTime _pubDate = DateTime.Now;
        private ICollection<string> _categories;
        private string _docs = "http://blogs.law.harvard.edu/tech/rss";
        private string _cloud = string.Empty;
        private int _ttl = 5;
        private string _imageUrl = string.Empty;
        private ICollection<Item> _items;

        #endregion

        #region Properties

        /// <summary>
        /// Determines if this is explicit or not
        /// </summary>
        public bool Explicit { get; set; }

        /// <summary>
        /// Items for this channel
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<Item> Items
        {
            get { return _items ?? (_items = new List<Item>()); }
            set { _items = value; }
        }
        /// <summary>
        /// Title of the channel
        /// </summary>
        public string Title
        {
            get { return _title; }
            set { _title = RSS.StripIllegalCharacters(value); }
        }

        /// <summary>
        /// Link to the website
        /// </summary>
        public string Link
        {
            get { return _link; }
            set { _link = value; }
        }

        /// <summary>
        /// Description of the channel
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = RSS.StripIllegalCharacters(value); }
        }

        /// <summary>
        /// Copyright info
        /// </summary>
        public string Copyright
        {
            get { return _copyright; }
            set { _copyright = value; }
        }

        /// <summary>
        /// Language it is in
        /// </summary>
        public string Language
        {
            get { return _language; }
            set { _language = value; }
        }

        /// <summary>
        /// Web Master info
        /// </summary>
        public string WebMaster
        {
            get { return _webMaster; }
            set { _webMaster = value; }
        }

        /// <summary>
        /// Date the channel was published
        /// </summary>
        public DateTime PubDate
        {
            get { return _pubDate; }
            set { _pubDate = value; }
        }

        /// <summary>
        /// Categories associated with this channel
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<string> Categories
        {
            get { return _categories ?? (_categories = new List<string>()); }
            set { _categories = value; }
        }

        /// <summary>
        /// Document describing the file format
        /// </summary>
        public string Docs
        {
            get { return _docs; }
            set { _docs = value; }
        }

        /// <summary>
        /// Cloud information
        /// </summary>
        public string Cloud
        {
            get { return _cloud; }
            set { _cloud = value; }
        }

        /// <summary>
        /// Time to live... Amount of time between updates.
        /// </summary>
        public int TTL
        {
            get { return _ttl; }
            set { _ttl = value; }
        }

        /// <summary>
        /// Url pointing to the image/logo associated with the channel
        /// </summary>
        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; }
        }
        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the channel to a string
        /// </summary>
        /// <returns>The channel as a string</returns>
        public override string ToString()
        {
            var channelString = new StringBuilder();
            channelString.Append("<channel>");
            channelString.Append("<title>").Append(Title).Append("</title>\r\n");
            channelString.Append("<link>").Append(Link).Append("</link>\r\n");
            channelString.Append("<atom:link xmlns:atom=\"http://www.w3.org/2005/Atom\" rel=\"self\" href=\"").Append(Link).Append("\" type=\"application/rss+xml\" />");

            channelString.Append("<description><![CDATA[").Append(Description).Append("]]></description>\r\n");
            channelString.Append("<language>").Append(Language).Append("</language>\r\n");
            channelString.Append("<copyright>").Append(Copyright).Append("</copyright>\r\n");
            channelString.Append("<webMaster>").Append(WebMaster).Append("</webMaster>\r\n");
            channelString.Append("<pubDate>").Append(PubDate.ToString("Ddd, dd MMM yyyy HH':'mm':'ss", CultureInfo.InvariantCulture)).Append("</pubDate>\r\n");
            channelString.Append("<itunes:explicit>").Append((Explicit ? "yes" : "no")).Append("</itunes:explicit>");
            channelString.Append("<itunes:subtitle>").Append(Title).Append("</itunes:subtitle>");
            channelString.Append("<itunes:summary><![CDATA[").Append(Description).Append("]]></itunes:summary>");

            foreach (string category in Categories)
            {
                channelString.Append("<category>").Append(category).Append("</category>\r\n");
                channelString.Append("<itunes:category text=\"").Append(category).Append("\" />\r\n");
            }
            channelString.Append("<docs>").Append(Docs).Append("</docs>\r\n");
            channelString.Append("<ttl>").Append(TTL.ToString(CultureInfo.InvariantCulture)).Append("</ttl>\r\n");
            if (!string.IsNullOrEmpty(ImageUrl))
            {
                channelString.Append("<image><url>").Append(ImageUrl).Append("</url>\r\n<title>").Append(Title).Append("</title>\r\n<link>").Append(Link).Append("</link>\r\n</image>\r\n");
            }
            foreach (Item currentItem in Items)
            {
                channelString.Append(currentItem);
            }
            channelString.Append("</channel>\r\n");
            return channelString.ToString();
        }

        #endregion
    }
}