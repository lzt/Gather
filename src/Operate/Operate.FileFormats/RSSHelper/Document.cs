﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Xml;
using Operate.ExtensionMethods;
using Operate.FileFormats.BaseClasses;

#endregion

namespace Operate.FileFormats.RSSHelper
{
    /// <summary>
    /// RSS document class
    /// </summary>
    public sealed class Document : StringFormatBase<Document>
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Document()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="location">Location of the RSS feed to load</param>
        public Document(string location)
        {
            if (location.IsNullOrEmpty()) { throw new ArgumentNullException("location"); }
            InternalLoad(location);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="document">XML document containing an RSS feed</param>

        public Document(XmlDocument document)
        {
            if (document.IsNull()) { throw new ArgumentNullException("document"); }
            Load(document);
        }

        #endregion

        #region Private Variables

        private ICollection<Channel> _channels;

        #endregion

        #region Properties

        /// <summary>
        /// Channels for the RSS feed
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<Channel> Channels
        {
            get { return _channels ?? (_channels = new List<Channel>()); }
            set { _channels = value; }
        }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// string representation of the RSS feed.
        /// </summary>
        /// <returns>An rss formatted string</returns>
        public override string ToString()
        {
            var documentString = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<rss xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\" xmlns:media=\"http://search.yahoo.com/mrss/\" version=\"2.0\">\r\n");
            foreach (Channel currentChannel in Channels)
            {
                documentString.Append(currentChannel);
            }
            documentString.Append("</rss>");
            return documentString.ToString();
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Copies one document's channels to another
        /// </summary>
        /// <param name="copyFrom">RSS document to copy from</param>
        public void Copy(Document copyFrom)
        {
            if (copyFrom.IsNull()) { throw new ArgumentNullException("copyFrom"); }
            foreach (Channel currentChannel in copyFrom.Channels)
            {
                Channels.Add(currentChannel);
            }
        }

        #endregion

        #region Private Functions

        private void Load(XmlDocument document)
        {
            if (document.IsNull()) { throw new ArgumentNullException("document"); }
            var namespaceManager = new XmlNamespaceManager(document.NameTable);
            if (document.DocumentElement != null)
            {
                XmlNodeList nodes = document.DocumentElement.SelectNodes("./channel", namespaceManager);
                if (nodes != null)
                {
                    foreach (XmlNode element in nodes)
                    {
                        Channels.Add(new Channel((XmlElement)element));
                    }
                    if (Channels.Count == 0)
                    {
                        nodes = document.DocumentElement.SelectNodes(".//channel", namespaceManager);
                        if (nodes != null)
                            foreach (XmlNode element in nodes)
                            {
                                Channels.Add(new Channel((XmlElement)element));
                            }
                        var items = new List<Item>();
                        nodes = document.DocumentElement.SelectNodes(".//item", namespaceManager);
                        if (nodes != null)
                            // ReSharper disable LoopCanBeConvertedToQuery
                            foreach (XmlNode element in nodes)
                            // ReSharper restore LoopCanBeConvertedToQuery
                            {
                                items.Add(new Item((XmlElement)element));
                            }
                        if (Channels.Count > 0)
                        {
                            var firstOrDefault = Channels.FirstOrDefault();
                            if (firstOrDefault != null) firstOrDefault.Items = items;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected override void LoadFromData(string data)
        {
            var document = new XmlDocument();
            document.LoadXml(data);
            Load(document);
        }

        #endregion
    }
}