﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Globalization;
using System.Text;
using System.Xml;

#endregion

namespace Operate.FileFormats.RSSHelper
{
    /// <summary>
    /// Item class for RSS feeds
    /// </summary>
    public sealed class Item
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Item()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="element">XML element containing the item content</param>

        public Item(XmlElement element)
        {
            if (element.IsNull()) { throw new ArgumentNullException("element"); }
            if (!element.Name.Equals("item", StringComparison.CurrentCultureIgnoreCase)) { throw new ArgumentException("Element is not a item"); }
            if (element.OwnerDocument != null)
            {
                var namespaceManager = new XmlNamespaceManager(element.OwnerDocument.NameTable);
                namespaceManager.AddNamespace("media", "http://search.yahoo.com/mrss/");
                XmlNode node = element.SelectSingleNode("./title", namespaceManager);
                if (node != null)
                {
                    Title = node.InnerText;
                }
                node = element.SelectSingleNode("./link", namespaceManager);
                if (node != null)
                {
                    Link = node.InnerText;
                }
                node = element.SelectSingleNode("./description", namespaceManager);
                if (node != null)
                {
                    Description = node.InnerText;
                }
                node = element.SelectSingleNode("./author", namespaceManager);
                if (node != null)
                {
                    Author = node.InnerText;
                }
                XmlNodeList nodes = element.SelectNodes("./category", namespaceManager);
                if (nodes != null)
                    foreach (XmlNode tempNode in nodes)
                    {
                        Categories.Add(RSS.StripIllegalCharacters(tempNode.InnerText));
                    }
                node = element.SelectSingleNode("./enclosure", namespaceManager);
                if (node != null)
                {
                    Enclosure = new Enclosure((XmlElement)node);
                }
                node = element.SelectSingleNode("./pubdate", namespaceManager);
                if (node != null)
                {
                    PubDate = DateTime.Parse(node.InnerText, CultureInfo.InvariantCulture);
                }
                node = element.SelectSingleNode("./media:thumbnail", namespaceManager);
                if (node != null)
                {
                    Thumbnail = new Thumbnail((XmlElement)node);
                }
                node = element.SelectSingleNode("./guid", namespaceManager);
                if (node != null)
                {
                    GUID = new GUID((XmlElement)node);
                }
            }
        }

        #endregion

        #region Private Variables
        private string _title = string.Empty;
        private string _link = string.Empty;
        private string _description = string.Empty;
        private string _author = string.Empty;
        private Thumbnail _thumbnail;
        private ICollection<string> _categories;
        private DateTime _pubDate = DateTime.Now;
        private Enclosure _enclosure;
        #endregion

        #region Properties

        /// <summary>
        /// GUID for the item
        /// </summary>
        public GUID GUID { get; set; }

        /// <summary>
        /// Thumbnail information
        /// </summary>
        public Thumbnail Thumbnail
        {
            get { return _thumbnail ?? (_thumbnail = new Thumbnail()); }
            set { _thumbnail = value; }
        }

        /// <summary>
        /// Title of the item
        /// </summary>
        public string Title
        {
            get { return _title; }
            set { _title = RSS.StripIllegalCharacters(value); }
        }

        /// <summary>
        /// Link to its location
        /// </summary>
        public string Link
        {
            get { return _link; }
            set { _link = value; }
        }

        /// <summary>
        /// Description of the item
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = RSS.StripIllegalCharacters(value); }
        }

        /// <summary>
        /// Author of the item
        /// </summary>
        public string Author
        {
            get { return _author; }
            set { _author = RSS.StripIllegalCharacters(value); }
        }

        /// <summary>
        /// Categories associated with the item
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<string> Categories
        {
            get { return _categories ?? (_categories = new List<string>()); }
            set { _categories = value; }
        }

        /// <summary>
        /// Date it was published
        /// </summary>
        public DateTime PubDate
        {
            get { return _pubDate; }
            set { _pubDate = value; }
        }

        /// <summary>
        /// Enclosure (used for podcasting)
        /// </summary>
        public Enclosure Enclosure
        {
            get { return _enclosure ?? (_enclosure = new Enclosure()); }
            set { _enclosure = value; }
        }
        #endregion

        #region Public Overridden Function

        /// <summary>
        /// Outputs a string ready for RSS
        /// </summary>
        /// <returns>A string formatted for RSS</returns>
        public override string ToString()
        {
            var itemString = new StringBuilder();
            itemString.Append("<item><title>").Append(Title).Append("</title>\r\n<link>")
                .Append(Link).Append("</link>\r\n<author>").Append(Author)
                .Append("</author>\r\n");
            foreach (string category in Categories)
            {
                itemString.Append("<category>").Append(category).Append("</category>\r\n");
            }
            itemString.Append("<pubDate>").Append(PubDate.ToString("r", CultureInfo.InvariantCulture)).Append("</pubDate>\r\n");
            if (Enclosure != null)
                itemString.Append(Enclosure);
            if (Thumbnail != null)
                itemString.Append(Thumbnail);
            itemString.Append("<description><![CDATA[").Append(Description).Append("]]></description>\r\n");
            if (GUID != null)
                itemString.Append(GUID);
            itemString.Append("<itunes:subtitle>").Append(Title).Append("</itunes:subtitle>");
            itemString.Append("<itunes:summary><![CDATA[").Append(Description).Append("]]></itunes:summary>");
            itemString.Append("</item>\r\n");
            return itemString.ToString();
        }

        #endregion
    }
}