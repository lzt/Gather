﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Operate.FileFormats.BaseClasses;

#endregion

namespace Operate.FileFormats.INI
{
    /// <summary>
    /// Class for helping with INI files
    /// </summary>
    public sealed class INI : StringFormatBase<INI>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public INI()
        {
            InternalLoad("");
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">Name of the file</param>
        public INI(string fileName)
        {
            FileName = fileName;
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Writes a change to an INI file
        /// </summary>
        /// <param name="section">Section</param>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void WriteToINI(string section, string key, string value)
        {
            if (FileContents.Keys.Contains(section))
            {
                if (FileContents[section].Keys.Contains(key))
                {
                    FileContents[section][key] = value;
                }
                else
                {
                    FileContents[section].Add(key, value);
                }
            }
            else
            {
                var tempDictionary = new Dictionary<string, string> {{key, value}};
                FileContents.Add(section, tempDictionary);
            }
            Save(_fileName);
        }

        /// <summary>
        /// Reads a value from an INI file
        /// </summary>
        /// <param name="section">Section</param>
        /// <param name="key">Key</param>
        /// <param name="defaultValue">Default value if it does not exist</param>
        public string ReadFromINI(string section, string key, string defaultValue = "")
        {
            if (FileContents.Keys.Contains(section) && FileContents[section].Keys.Contains(key))
                return FileContents[section][key];
            return defaultValue;
        }

        /// <summary>
        /// Returns an XML representation of the INI file
        /// </summary>
        /// <returns>An XML representation of the INI file</returns>
        public string ToXML()
        {
            if (string.IsNullOrEmpty(FileName))
                return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<INI>\r\n</INI>";
            var builder = new StringBuilder();
            builder.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            builder.Append("<INI>\r\n");
            foreach (string header in FileContents.Keys)
            {
                builder.Append("<section name=\"" + header + "\">\r\n");
                foreach (string key in FileContents[header].Keys)
                {
                    builder.Append("<key name=\"" + key + "\">" + FileContents[header][key] + "</key>\r\n");
                }
                builder.Append("</section>\r\n");
            }
            builder.Append("</INI>");
            return builder.ToString();
        }

        /// <summary>
        /// Deletes a section from the INI file
        /// </summary>
        /// <param name="section">Section to remove</param>
        /// <returns>True if it is removed, false otherwise</returns>
        public bool DeleteFromINI(string section)
        {
            bool returnValue = false;
            if (FileContents.ContainsKey(section))
            {
                returnValue = FileContents.Remove(section);
                Save(_fileName);
            }
            return returnValue;
        }

        /// <summary>
        /// Deletes a key from the INI file
        /// </summary>
        /// <param name="section">Section the key is under</param>
        /// <param name="key">Key to remove</param>
        /// <returns>True if it is removed, false otherwise</returns>
        public bool DeleteFromINI(string section, string key)
        {
            bool returnValue = false;
            if (FileContents.ContainsKey(section) && FileContents[section].ContainsKey(key))
            {
                returnValue = FileContents[section].Remove(key);
                Save(_fileName);
            }
            return returnValue;
        }

        /// <summary>
        /// Convert the INI to a string
        /// </summary>
        /// <returns>The INI file as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (string header in FileContents.Keys)
            {
                builder.Append("[" + header + "]\r\n");
                foreach (string key in FileContents[header].Keys)
                    builder.Append(key + "=" + FileContents[header][key] + "\r\n");
            }
            return builder.ToString();
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected override void LoadFromData(string data)
        {
            FileContents = new Dictionary<string, Dictionary<string, string>>();
            string contents = data;
            var section = new Regex("[" + Regex.Escape(" ") + "\t]*" + Regex.Escape("[") + ".*" + Regex.Escape("]\r\n"));
            string[] sections = section.Split(contents);
            MatchCollection sectionHeaders = section.Matches(contents);
            int counter = 1;
            foreach (Match sectionHeader in sectionHeaders)
            {
                string[] splitter = { "\r\n" };
                string[] splitter2 = { "=" };
                string[] items = sections[counter].Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                var sectionValues = new Dictionary<string, string>();
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (string item in items)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    sectionValues.Add(item.Split(splitter2, StringSplitOptions.None)[0], item.Split(splitter2, StringSplitOptions.None)[1]);
                }
                FileContents.Add(sectionHeader.Value.Replace("[", "").Replace("]\r\n", ""), sectionValues);
                ++counter;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Name of the file
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; InternalLoad(_fileName); }
        }

        private Dictionary<string, Dictionary<string, string>> FileContents { get; set; }

        private string _fileName = string.Empty;

        #endregion
    }
}