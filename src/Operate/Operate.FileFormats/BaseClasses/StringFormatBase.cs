﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.IO;
using Operate.IO.ExtensionMethods;

#endregion

namespace Operate.FileFormats.BaseClasses
{
    /// <summary>
    /// Format base class for objects that are string based
    /// </summary>
    public abstract class StringFormatBase<TFormatType> : FormatBase<TFormatType, string>
        where TFormatType : StringFormatBase<TFormatType>, new()
    {
        #region Constructor

        #endregion

        #region Functions

        /// <summary>
        /// Compares the object to another object
        /// </summary>
        /// <param name="other">Object to compare to</param>
        /// <returns>0 if they are equal, -1 if this is smaller, 1 if it is larger</returns>
        public override int CompareTo(TFormatType other)
        {
            return String.Compare(other.ToString(), ToString(), StringComparison.Ordinal);
        }
        
        /// <summary>
        /// Saves the object
        /// </summary>
        /// <param name="location">Location to save it to</param>
        /// <returns>This</returns>
        public override TFormatType Save(string location)
        {
            new FileInfo(location).Save(ToString());
            return (TFormatType)this;
        }

        /// <summary>
        /// Determines if the objects are equal
        /// </summary>
        /// <param name="other">Other object to compare to</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public override bool Equals(TFormatType other)
        {
            return ToString().Equals(other.ToString(), StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Loads the object from the location specified
        /// </summary>
        /// <param name="location">Location of the file to load</param>
        /// <returns>This</returns>
        protected override TFormatType InternalLoad(string location)
        {
            LoadFromData(new FileInfo(location).Read());
            return (TFormatType)this;
        }

        /// <summary>
        /// Loads the object from the data specified
        /// </summary>
        /// <param name="data">Data to load into the object</param>
        protected abstract void LoadFromData(string data);

        /// <summary>
        /// Clones the object
        /// </summary>
        /// <returns>A newly cloned object</returns>
        public override object Clone()
        {
            return (TFormatType)ToString();
        }

        #endregion

        #region Operators

        /// <summary>
        /// Converts the format to a string
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <returns>The value as a string</returns>
        public static implicit operator string(StringFormatBase<TFormatType> value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Converts the string to the format specified
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <returns>The string as an object</returns>
        public static implicit operator StringFormatBase<TFormatType>(string value)
        {
            var returnValue=new TFormatType();
            returnValue.LoadFromData(value);
            return returnValue;
        }

        #endregion
    }
}