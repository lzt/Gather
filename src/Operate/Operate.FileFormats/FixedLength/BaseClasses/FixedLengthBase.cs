﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Text;
using Operate.FileFormats.BaseClasses;
using Operate.FileFormats.FixedLength.Interfaces;

#endregion

namespace Operate.FileFormats.FixedLength.BaseClasses
{
    /// <summary>
    /// Parses and creates a fixed length file
    /// </summary>
    /// <typeparam name="TObjectType">Object Type</typeparam>
    /// <typeparam name="TFieldType">Field Type</typeparam>
    public abstract class FixedLengthBase<TObjectType, TFieldType> : StringListFormatBase<TObjectType, IRecord>
        where TObjectType : FixedLengthBase<TObjectType, TFieldType>, new()
    {
        #region Constructor

        #endregion
        
        #region Functions

        /// <summary>
        /// Parses the string into fields
        /// </summary>
        /// <param name="value">The string value</param>
        /// <param name="length">Max length for the record</param>
        public abstract void Parse(string value, int length = -1);

        /// <summary>
        /// Converts the file to a string
        /// </summary>
        /// <returns>The file as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (IRecord record in Records)
                builder.Append(record);
            return builder.ToString();
        }

        /// <summary>
        /// Loads the data into the object
        /// </summary>
        /// <param name="data">The data to load</param>
        protected override void LoadFromData(string data)
        {
            Parse(data);
        }

        #endregion
    }
}