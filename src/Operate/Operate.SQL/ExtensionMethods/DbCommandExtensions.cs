﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Operate.Comparison;
using Operate.ExtensionMethods;

#endregion

namespace Operate.SQL.ExtensionMethods
{
    /// <summary>
    /// Extension methods for DbCommand objects
    /// </summary>
    public static class DbCommandExtensions
    {
        #region Functions

        #region AddParameter

        /// <summary>
        /// Adds a parameter to the call (for strings only)
        /// </summary>
        /// <param name="command">Command object</param>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="direction">Direction that the parameter goes (in or out)</param>
        /// <returns>The DbCommand object</returns>
        public static DbCommand AddParameter(this DbCommand command, string id, string value = "",
            ParameterDirection direction = ParameterDirection.Input)
        {
            if (command.IsNull()) { throw new ArgumentNullException("command"); }
            if (id.IsNull()) { throw new ArgumentNullException("id"); }
            int length = string.IsNullOrEmpty(value) ? 1 : value.Length;
            if (direction == ParameterDirection.Output
                || direction == ParameterDirection.InputOutput
                || length > 4000
                || length < -1)
                length = -1;
            DbParameter parameter = command.GetOrCreateParameter(id);
            parameter.Value = string.IsNullOrEmpty(value) ? DBNull.Value : (object)value;
            parameter.IsNullable = string.IsNullOrEmpty(value);
            parameter.DbType = DbType.String;
            parameter.Direction = direction;
            parameter.Size = length;
            return command;
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="direction">Direction that the parameter goes (in or out)</param>
        /// <param name="command">Command object</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <returns>The DbCommand object</returns>
        public static DbCommand AddParameter(this DbCommand command, string id, SqlDbType type,
            object value = null, ParameterDirection direction = ParameterDirection.Input)
        {
            if (command.IsNull()) { throw new ArgumentNullException("command"); }
            if (id.IsNullOrEmpty()) { throw new ArgumentNullException("id"); }
            return command.AddParameter(id, type.To(DbType.Int32), value, direction);
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <typeparam name="TDataType">Data type of the parameter</typeparam>
        /// <param name="id">Name of the parameter</param>
        /// <param name="direction">Direction that the parameter goes (in or out)</param>
        /// <param name="command">Command object</param>
        /// <param name="value">Value to add</param>
        /// <returns>The DbCommand object</returns>
        public static DbCommand AddParameter<TDataType>(this DbCommand command, string id, TDataType value = default(TDataType),
            ParameterDirection direction = ParameterDirection.Input)
        {
            if (command.IsNull()) { throw new ArgumentNullException("command"); }
            if (id.IsNull()) { throw new ArgumentNullException("id"); }
            return command.AddParameter(id,
                new GenericEqualityComparer<TDataType>().Equals(value, default(TDataType)) ? typeof(TDataType).To(DbType.Int32) : value.GetType().To(DbType.Int32),
                value, direction);
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="direction">Direction that the parameter goes (in or out)</param>
        /// <param name="command">Command object</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <returns>The DbCommand object</returns>
        public static DbCommand AddParameter(this DbCommand command, string id, DbType type, object value = null,
            ParameterDirection direction = ParameterDirection.Input)
        {
            if (command.IsNull()) { throw new ArgumentNullException("command"); }
            if (id.IsNull()) { throw new ArgumentNullException("id"); }
            DbParameter parameter = command.GetOrCreateParameter(id);
            parameter.Value = value == null || Convert.IsDBNull(value) ? DBNull.Value : value;
            parameter.IsNullable = value == null || Convert.IsDBNull(value);
            if (type != default(DbType))
                parameter.DbType = type;
            parameter.Direction = direction;
            return command;
        }

        #endregion

        #region BeginTransaction

        /// <summary>
        /// Begins a transaction
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>A transaction object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static DbTransaction BeginTransaction(this DbCommand command)
        {
            if (command == null || command.Connection == null)
                return null;
            command.Open();
            command.Transaction = command.Connection.BeginTransaction();
            return command.Transaction;
        }

        #endregion

        #region ClearParameters

        /// <summary>
        /// Clears the parameters
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>The DBCommand object</returns>
        public static DbCommand ClearParameters(this DbCommand command)
        {
            if (command != null)
                command.Parameters.Clear();
            return command;
        }

        #endregion

        #region Close

        /// <summary>
        /// Closes the connection
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>The DBCommand object</returns>
        public static DbCommand Close(this DbCommand command)
        {
            if (command != null
                && command.Connection != null
                && command.Connection.State != ConnectionState.Closed)
                command.Connection.Close();
            return command;
        }

        #endregion

        #region Commit

        /// <summary>
        /// Commits a transaction
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>The DBCommand object</returns>
        public static DbCommand Commit(this DbCommand command)
        {
            if (command != null && command.Transaction != null)
                command.Transaction.Commit();
            return command;
        }

        #endregion

        #region ExecuteDataSet

        /// <summary>
        /// Executes the query and returns a data set
        /// </summary>
        /// <param name="command">Command object</param>
        /// <param name="factory">DbProviderFactory being used</param>
        /// <returns>A dataset filled with the results of the query</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static DataSet ExecuteDataSet(this DbCommand command, DbProviderFactory factory)
        {
            if (command == null)
                return null;
            var returnSet = new DataSet { Locale = CultureInfo.CurrentCulture };
            command.Open();
            using (DbDataAdapter adapter = factory.CreateDataAdapter())
            {
                if (adapter != null)
                {
                    adapter.SelectCommand = command;
                    adapter.Fill(returnSet);
                }
            }
            return returnSet;
        }

        #endregion

        #region ExecuteScalar

        /// <summary>
        /// Executes the stored procedure as a scalar query
        /// </summary>
        /// <param name="command">Command object</param>
        /// <param name="Default">Default value if there is an issue</param>
        /// <returns>The object of the first row and first column</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static TDataType ExecuteScalar<TDataType>(this DbCommand command, TDataType Default = default(TDataType))
        {
            if (command == null)
                return Default;
            command.Open();
            return command.ExecuteScalar().To(Default);
        }

        #endregion

        #region GetOrCreateParameter

        /// <summary>
        /// Gets a parameter or creates it, if it is not found
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="command">Command object</param>
        /// <returns>The DbParameter associated with the ID</returns>
        public static DbParameter GetOrCreateParameter(this DbCommand command, string id)
        {
            if (command.Parameters.Contains(id))
                return command.Parameters[id];
            DbParameter parameter = command.CreateParameter();
            parameter.ParameterName = id;
            command.Parameters.Add(parameter);
            return parameter;
        }

        #endregion

        #region GetOutputParameter

        /// <summary>
        /// Returns an output parameter's value
        /// </summary>
        /// <typeparam name="TDataType">Data type of the object</typeparam>
        /// <param name="id">Parameter name</param>
        /// <param name="command">Command object</param>
        /// <param name="Default">Default value for the parameter</param>
        /// <returns>if the parameter exists (and isn't null or empty), it returns the parameter's value. Otherwise the default value is returned.</returns>
        public static TDataType GetOutputParameter<TDataType>(this DbCommand command, string id, TDataType Default = default(TDataType))
        {
            return command != null && command.Parameters[id] != null ?
                command.Parameters[id].Value.To(Default) :
                Default;
        }

        #endregion

        #region Open

        /// <summary>
        /// Opens the connection
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>The DBCommand object</returns>
        public static DbCommand Open(this DbCommand command)
        {
            if (command != null
                && command.Connection != null
                && command.Connection.State != ConnectionState.Open)
                command.Connection.Open();
            return command;
        }

        #endregion

        #region Rollback

        /// <summary>
        /// Rolls back a transaction
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>The DBCommand object</returns>
        public static DbCommand Rollback(this DbCommand command)
        {
            if (command != null && command.Transaction != null)
                command.Transaction.Rollback();
            return command;
        }

        #endregion

        #endregion
    }
}