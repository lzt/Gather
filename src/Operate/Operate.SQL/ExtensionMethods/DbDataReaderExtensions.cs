﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Data;
using Operate.ExtensionMethods;

#endregion

namespace Operate.SQL.ExtensionMethods
{
    /// <summary>
    /// Extension methods for DbDataReader objects
    /// </summary>
    public static class DataReaderExtensions
    {
        #region Functions
        
        #region GetParameter

        /// <summary>
        /// Returns a parameter's value
        /// </summary>
        /// <param name="reader">Reader object</param>
        /// <param name="id">Parameter name</param>
        /// <param name="Default">Default value for the parameter</param>
        /// <returns>if the parameter exists (and isn't null or empty), it returns the parameter's value. Otherwise the default value is returned.</returns>
        public static TDataType GetParameter<TDataType>(this IDataRecord reader, string id, TDataType Default = default(TDataType))
        {
            if (reader == null)
                return Default;
            for (int x = 0; x < reader.FieldCount; ++x)
            {
                if (reader.GetName(x) == id)
                    return reader.GetParameter(x, Default);
            }
            return Default;
        }

        /// <summary>
        /// Returns a parameter's value
        /// </summary>
        /// <param name="reader">Reader object</param>
        /// <param name="position">Position in the reader row</param>
        /// <param name="Default">Default value for the parameter</param>
        /// <returns>if the parameter exists (and isn't null or empty), it returns the parameter's value. Otherwise the default value is returned.</returns>
        public static TDataType GetParameter<TDataType>(this IDataRecord reader, int position, TDataType Default = default(TDataType))
        {
            if (reader == null)
                return Default;
            object value = reader[position];
            return (value == null || Convert.IsDBNull(value)) ? Default : value.To(Default);
        }

        #endregion

        #endregion
    }
}
