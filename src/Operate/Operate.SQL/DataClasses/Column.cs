﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;
using System.Data;
using Operate.Comparison;
using Operate.SQL.DataClasses.Interfaces;
#endregion

namespace Operate.SQL.DataClasses
{
    /// <summary>
    /// Column class
    /// </summary>
    /// <typeparam name="T">Data type of the column</typeparam>
    public class Column<T> : IColumn
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Column()
        {
            ForeignKey = new List<IColumn>();
            ForeignKeyColumns = new List<string>();
            ForeignKeyTables = new List<string>();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the column</param>
        /// <param name="columnType">The data type</param>
        /// <param name="length">The data length</param>
        /// <param name="nullable">Is it nullable?</param>
        /// <param name="identity">Is it an identity?</param>
        /// <param name="index">Is it the index?</param>
        /// <param name="primaryKey">Is it the primary key?</param>
        /// <param name="unique">Is it unique?</param>
        /// <param name="foreignKeyTable">Foreign key table</param>
        /// <param name="foreignKeyColumn">Foreign key column</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="parentTable">Parent table</param>
        /// <param name="onDeleteCascade">Cascade on delete</param>
        /// <param name="onDeleteSetNull">Set null on delete</param>
        /// <param name="onUpdateCascade">Cascade on update</param>
        public Column(string name, DbType columnType, int length, bool nullable,
            bool identity, bool index, bool primaryKey, bool unique, string foreignKeyTable,
            string foreignKeyColumn, T defaultValue, bool onDeleteCascade, bool onUpdateCascade,
            bool onDeleteSetNull, ITable parentTable)
        {
            Name = name;
            ForeignKey = new List<IColumn>();
            ForeignKeyColumns = new List<string>();
            ForeignKeyTables = new List<string>();
            ParentTable = parentTable;
            DataType = columnType;
            Length = length;
            Nullable = nullable;
            AutoIncrement = identity;
            Index = index;
            PrimaryKey = primaryKey;
            Unique = unique;
            Default = new GenericEqualityComparer<T>().Equals(defaultValue, default(T)) ? "" : defaultValue.ToString();
            OnDeleteCascade = onDeleteCascade;
            OnUpdateCascade = onUpdateCascade;
            OnDeleteSetNull = onDeleteSetNull;
            AddForeignKey(foreignKeyTable, foreignKeyColumn);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// On Delete Cascade
        /// </summary>
        public bool OnDeleteCascade { get; set; }

        /// <summary>
        /// On Update Cascade
        /// </summary>
        public bool OnUpdateCascade { get; set; }

        /// <summary>
        /// On Delete Set Null
        /// </summary>
        public bool OnDeleteSetNull { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Data type
        /// </summary>
        public DbType DataType { get; set; }

        /// <summary>
        /// Data length
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Foreign keys
        /// </summary>
        public ICollection<IColumn> ForeignKey { get; private set; }

        /// <summary>
        /// Primary key?
        /// </summary>
        public bool PrimaryKey { get; set; }

        /// <summary>
        /// Nullable?
        /// </summary>
        public bool Nullable { get; set; }

        /// <summary>
        /// Unique?
        /// </summary>
        public bool Unique { get; set; }

        /// <summary>
        /// Index?
        /// </summary>
        public bool Index { get; set; }

        /// <summary>
        /// Auto increment?
        /// </summary>
        public bool AutoIncrement { get; set; }

        /// <summary>
        /// Default value
        /// </summary>
        public string Default { get; set; }

        /// <summary>
        /// Parent table
        /// </summary>
        public ITable ParentTable { get; set; }

        #endregion

        #region Private Variables

        private List<string> ForeignKeyTables { get; set; }
        private List<string> ForeignKeyColumns { get; set; }

        #endregion

        #region Public Functions

        /// <summary>
        /// Add foreign key
        /// </summary>
        /// <param name="foreignKeyTable">Table of the foreign key</param>
        /// <param name="foreignKeyColumn">Column of the foreign key</param>
        public virtual void AddForeignKey(string foreignKeyTable, string foreignKeyColumn)
        {
            if (string.IsNullOrEmpty(foreignKeyTable) || string.IsNullOrEmpty(foreignKeyColumn))
                return;
            ForeignKeyColumns.Add(foreignKeyColumn);
            ForeignKeyTables.Add(foreignKeyTable);
        }

        /// <summary>
        /// Sets up the foreign key list
        /// </summary>
        public virtual void SetupForeignKeys()
        {
            for (int x = 0; x < ForeignKeyColumns.Count; ++x)
            {
                Database tempDatabase = ParentTable.ParentDatabase;
                if (tempDatabase != null)
                {
                    foreach (Table table in tempDatabase.Tables)
                    {
                        if (table.Name == ForeignKeyTables[x])
                        {
                            foreach (IColumn column in table.Columns)
                            {
                                if (column.Name == ForeignKeyColumns[x])
                                {
                                    ForeignKey.Add(column);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        #endregion
    }
}