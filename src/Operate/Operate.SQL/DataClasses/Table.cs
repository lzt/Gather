﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Operate.ExtensionMethods;
using Operate.SQL.DataClasses.Enums;
using Operate.SQL.DataClasses.Interfaces;
#endregion

namespace Operate.SQL.DataClasses
{
    /// <summary>
    /// Table class
    /// </summary>
    public class Table : ITable
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="parentDatabase">Parent database</param>
        public Table(string name, Database parentDatabase)
        {
            Name = name;
            ParentDatabase = parentDatabase;
            Columns = new List<IColumn>();
            Triggers = new List<Trigger>();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Table()
        {
            Columns = new List<IColumn>();
            Triggers = new List<Trigger>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Name of the table
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Columns in the table
        /// </summary>
        public ICollection<IColumn> Columns { get; private set; }
        /// <summary>
        /// Parent database
        /// </summary>
        public Database ParentDatabase { get; set; }

        /// <summary>
        /// List of triggers associated with the table
        /// </summary>
        public ICollection<Trigger> Triggers { get; private set; }

        #endregion

        #region Public Functions

        /// <summary>
        /// Adds a column
        /// </summary>
        /// <param name="columnName">Column Name</param>
        /// <param name="columnType">Data type</param>
        /// <param name="length">Data length</param>
        /// <param name="nullable">Nullable?</param>
        /// <param name="identity">Identity?</param>
        /// <param name="index">Index?</param>
        /// <param name="primaryKey">Primary key?</param>
        /// <param name="unique">Unique?</param>
        /// <param name="foreignKeyTable">Foreign key table</param>
        /// <param name="foreignKeyColumn">Foreign key column</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="onDeleteCascade">On Delete Cascade</param>
        /// <param name="onUpdateCascade">On Update Cascade</param>
        /// <param name="onDeleteSetNull">On Delete Set Null</param>
        /// <typeparam name="T">Column type</typeparam>
        public virtual IColumn AddColumn<T>(string columnName, DbType columnType, int length = 0, bool nullable = true,
            bool identity = false, bool index = false, bool primaryKey = false, bool unique = false,
            string foreignKeyTable = "", string foreignKeyColumn = "", T defaultValue = default(T),
            bool onDeleteCascade = false, bool onUpdateCascade = false, bool onDeleteSetNull = false)
        {
            return Columns.AddAndReturn(new Column<T>(columnName, columnType, length, nullable, identity, index, primaryKey, unique, foreignKeyTable, foreignKeyColumn, defaultValue, onDeleteCascade, onUpdateCascade, onDeleteSetNull, this));
        }

        /// <summary>
        /// Determines if a column exists in the table
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <returns>True if it exists, false otherwise</returns>
        public virtual bool ContainsColumn(string columnName)
        {
            return this[columnName] != null;
        }

        /// <summary>
        /// Adds a foreign key
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="foreignKeyTable">Foreign key table</param>
        /// <param name="foreignKeyColumn">Foreign key column</param>
        public virtual void AddForeignKey(string columnName, string foreignKeyTable, string foreignKeyColumn)
        {
            this[columnName].Chain(x => x.AddForeignKey(foreignKeyTable, foreignKeyColumn));
        }

        /// <summary>
        /// Sets up foreign keys
        /// </summary>
        public virtual void SetupForeignKeys()
        {
            Columns.ForEach(x => x.SetupForeignKeys());
        }

        /// <summary>
        /// Adds a trigger
        /// </summary>
        /// <param name="name">Trigger name</param>
        /// <param name="definition">Trigger definition</param>
        /// <param name="type">The trigger type</param>
        public virtual Trigger AddTrigger(string name, string definition, TriggerType type)
        {
            return Triggers.AddAndReturn(new Trigger(name, definition, type, this));
        }

        /// <summary>
        /// Gets the column specified
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns>The specified column</returns>
        public virtual IColumn this[string name]
        {
            get
            {
                return Columns.FirstOrDefault(x => x.Name == name);
            }
        }

        #endregion
    }
}
