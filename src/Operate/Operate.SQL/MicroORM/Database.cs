﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Concurrent;
using System.Configuration;
using Operate.SQL.MicroORM.Interfaces;

#endregion

namespace Operate.SQL.MicroORM
{
    /// <summary>
    /// Holds database information
    /// </summary>
    public class Database
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connection">Connection string (if not specified, uses Name to get the connection string from the configuration manager)</param>
        /// <param name="name">Database name (if not specified, it uses the first connection string it finds in the configuration manager)</param>
        /// <param name="dbType">Database type, based on ADO.Net provider name (will override if it pulls info from the configuration manager)</param>
        /// <param name="parameterPrefix">Parameter prefix to use (if empty, it will override with its best guess based on database type)</param>
        /// <param name="profile">Determines if the calls should be profiled</param>
        /// <param name="readable">Should this database be used to read data?</param>
        /// <param name="writable">Should this database be used to write data?</param>
        public Database(string connection, string name, string dbType = "System.Data.SqlClient",
                        string parameterPrefix = "@", bool profile = false, bool writable = true,
                        bool readable = true)
        {
            Name = string.IsNullOrEmpty(name) ? ConfigurationManager.ConnectionStrings[0].Name : name;
            if (string.IsNullOrEmpty(connection) && ConfigurationManager.ConnectionStrings[Name] != null)
            {
                Connection = ConfigurationManager.ConnectionStrings[Name].ConnectionString;
                DbType = ConfigurationManager.ConnectionStrings[Name].ProviderName;
            }
            else if (string.IsNullOrEmpty(connection))
            {
                Connection = name;
                DbType = dbType;
            }
            else
            {
                Connection = connection;
                DbType = dbType;
            }
            if (string.IsNullOrEmpty(DbType))
            {
                DbType = "System.Data.SqlClient";
            }
            if (string.IsNullOrEmpty(parameterPrefix))
            {
                if (dbType.Contains("MySql"))
                    ParameterPrefix = "?";
                else if (dbType.Contains("Oracle"))
                    ParameterPrefix = ":";
                else
                    ParameterPrefix = "@";
            }
            else
                ParameterPrefix = parameterPrefix;
            Profile = profile;
            Mappings = new ConcurrentDictionary<Type, IMapping>();
            Writable = writable;
            Readable = readable;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Connection string
        /// </summary>
        public string Connection { get; protected set; }

        /// <summary>
        /// Name of the database
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Database type, based on ADO.Net provider name
        /// </summary>
        public string DbType { get; protected set; }

        /// <summary>
        /// Parameter prefix that the database uses
        /// </summary>
        public string ParameterPrefix { get; protected set; }

        /// <summary>
        /// Should calls to this database be profiled?
        /// </summary>
        public bool Profile { get; protected set; }

        /// <summary>
        /// Contains the mappings associated with this database
        /// </summary>
        public ConcurrentDictionary<Type, IMapping> Mappings { get; private set; }

        /// <summary>
        /// Should this database be used to write data?
        /// </summary>
        public bool Writable { get; protected set; }

        /// <summary>
        /// Should this database be used to read data?
        /// </summary>
        public bool Readable { get; protected set; }

        #endregion
    }
}