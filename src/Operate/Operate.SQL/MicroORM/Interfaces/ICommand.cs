﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;
using System.Data;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL.MicroORM.Interfaces
{
    /// <summary>
    /// Command interface
    /// </summary>
    public interface ICommand
    {
        #region Properties

        /// <summary>
        /// Actual SQL command
        /// </summary>
        string SQLCommand { get; }

        /// <summary>
        /// Command type
        /// </summary>
        CommandType CommandType { get; }

        /// <summary>
        /// Parameters associated with the command
        /// </summary>
        ICollection<IParameter> Parameters { get; }

        #endregion

        #region Functions

        #region AddParameter

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="id">Parameter ID</param>
        /// <param name="parameter">Parameter value</param>
        /// <typeparam name="TDataType">Data type of the parameter</typeparam>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        void AddParameter<TDataType>(string id, TDataType parameter, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@");

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="id">Parameter ID</param>
        /// <param name="parameter">Parameter value</param>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        void AddParameter(string id, string parameter, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@");

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="parameters">Parameter values</param>
        /// <param name="parameterStarter">Parameter starter to use</param>
        void AddParameter(string parameterStarter,params object[] parameters);

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        void AddParameter(string id, SqlDbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@");

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        void AddParameter(string id, DbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@");

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="parameters">Parameter values</param>
        void AddParameter(params IParameter[] parameters);

        #endregion

        #region ClearParameters

        /// <summary>
        /// Clears the parameters for the command
        /// </summary>
        void ClearParameters();

        #endregion

        #endregion
    }
}