﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Operate.DataMapper;
using Operate.ExtensionMethods;
using Operate.SQL.MicroORM.Enums;
using Operate.SQL.MicroORM.Interfaces;

#endregion

namespace Operate.SQL.MicroORM
{
    /// <summary>
    /// Class that acts as a mapping within the micro ORM
    /// </summary>
    /// <typeparam name="TClassType">Class type that this will accept</typeparam>
    public class Mapping<TClassType> : IMapping, IMapping<TClassType> where TClassType : class,new()
    {
        #region Constructors

        /// <summary>
        /// Constructor (can be used if supplying own SQLHelper)
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="primaryKey">Primary key</param>
        /// <param name="autoIncrement">Is the primary key set to auto increment?</param>
        public Mapping(string tableName, string primaryKey, bool autoIncrement = true)
        {
            Mappings = new TypeMapping<TClassType, SQLHelper>();
            ParameterNames = new List<string>();
            TableName = tableName;
            PrimaryKey = primaryKey;
            AutoIncrement = autoIncrement;
        }

        /// <summary>
        /// Constructor (used internally to create instance versions of static mappings
        /// </summary>
        /// <param name="mappingToCopyFrom">Mapping to copy from</param>
        public Mapping(Mapping<TClassType> mappingToCopyFrom)
        {
            Mappings = mappingToCopyFrom.Mappings;
            TableName = mappingToCopyFrom.TableName;
            PrimaryKey = mappingToCopyFrom.PrimaryKey;
            AutoIncrement = mappingToCopyFrom.AutoIncrement;
            ParameterNames = mappingToCopyFrom.ParameterNames;
            GetPrimaryKey = mappingToCopyFrom.GetPrimaryKey;
            PrimaryKeyMapping = mappingToCopyFrom.PrimaryKeyMapping;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Mapper used to map properties to SQLHelper
        /// </summary>
        public TypeMapping<TClassType, SQLHelper> Mappings { get; private set; }

        /// <summary>
        /// Table name
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Primary key
        /// </summary>
        public string PrimaryKey { get; set; }

        /// <summary>
        /// Used to get/set primary key from an object
        /// </summary>
        public Mapping<TClassType, object> PrimaryKeyMapping { get; set; }

        /// <summary>
        /// Gets the primary key from an object
        /// </summary>
        public Func<TClassType, object> GetPrimaryKey { get; set; }

        /// <summary>
        /// Auto increment?
        /// </summary>
        public bool AutoIncrement { get; set; }

        /// <summary>
        /// Parameter listing
        /// </summary>
        public ICollection<string> ParameterNames { get; private set; }

        #endregion

        #region Map

        /// <summary>
        /// Maps a property to a database property name (required to actually get data from the database)
        /// </summary>
        /// <typeparam name="TDataType">Data type of the property</typeparam>
        /// <param name="property">Property to add a mapping for</param>
        /// <param name="databasePropertyName">Property name</param>
        /// <param name="mode">This determines if the mapping should have read or write access</param>
        /// <param name="defaultValue">Default value</param>
        /// <returns>This mapping</returns>
        public virtual IMapping<TClassType> Map<TDataType>(Expression<Func<TClassType, TDataType>> property,
            string databasePropertyName = "",
            TDataType defaultValue = default(TDataType),
            Mode mode = Mode.Read|Mode.Write)
        {
            if (property == null)
                throw new ArgumentNullException("property");
            if (Mappings == null)
                // ReSharper disable NotResolvedInText
                throw new ArgumentNullException("Mappings");
            // ReSharper restore NotResolvedInText
            if (string.IsNullOrEmpty(databasePropertyName))
                databasePropertyName = property.PropertyName();
            if (ParameterNames.Contains(databasePropertyName))
                return this;
            Expression convert = Expression.Convert(property.Body, typeof(object));
            Expression<Func<TClassType, object>> propertyExpression = Expression.Lambda<Func<TClassType, object>>(convert, property.Parameters);
            Mappings.AddMapping(propertyExpression,
                ((mode & Mode.Read) == Mode.Read) ? new Func<SQLHelper, object>(x => x.GetParameter(databasePropertyName, defaultValue)) : null,
                ((mode & Mode.Write) == Mode.Write) ? new Action<SQLHelper, object>((x, y) => x.AddParameter(databasePropertyName, y)) : null);
            ParameterNames.Add(databasePropertyName);
            if (databasePropertyName == PrimaryKey)
            {
                // ReSharper disable RedundantAssignment
                PrimaryKeyMapping = new Mapping<TClassType, object>(propertyExpression, x => x, (x, y) => x = y);
                // ReSharper restore RedundantAssignment
                GetPrimaryKey = propertyExpression.Compile();
            }
            return this;
        }

        /// <summary>
        /// Maps a property to a database property name (required to actually get data from the database)
        /// </summary>
        /// <param name="property">Property to add a mapping for</param>
        /// <param name="databasePropertyName">Property name</param>
        /// <param name="mode">This determines if the mapping should have read or write access</param>
        /// <param name="defaultValue">Default value</param>
        /// <returns>This mapping</returns>
        public virtual IMapping<TClassType> Map(Expression<Func<TClassType, string>> property,
            string databasePropertyName = "",
            string defaultValue = "",
            Mode mode = Mode.Read|Mode.Write)
        {
            if (property == null)
                throw new ArgumentNullException("property");
            if (Mappings == null)
                // ReSharper disable NotResolvedInText
                throw new ArgumentNullException("Mappings");
            // ReSharper restore NotResolvedInText
            if (string.IsNullOrEmpty(databasePropertyName))
                databasePropertyName = property.PropertyName();
            if (ParameterNames.Contains(databasePropertyName))
                return this;
            Expression convert = Expression.Convert(property.Body, typeof(object));
            Expression<Func<TClassType, object>> propertyExpression = Expression.Lambda<Func<TClassType, object>>(convert, property.Parameters);
            Mappings.AddMapping(propertyExpression,
                ((mode & Mode.Read) == Mode.Read) ? new Func<SQLHelper, object>(x => x.GetParameter(databasePropertyName, defaultValue)) : null,
                ((mode & Mode.Write) == Mode.Write) ? new Action<SQLHelper, object>(
                // ReSharper disable ImplicitlyCapturedClosure
                                                          (x, y) => x.AddParameter(databasePropertyName, (string)y)) : null);
            // ReSharper restore ImplicitlyCapturedClosure
            ParameterNames.Add(databasePropertyName);
            if (databasePropertyName == PrimaryKey)
            {
                PrimaryKeyMapping = new Mapping<TClassType, object>(propertyExpression, x => x,
                    // ReSharper disable RedundantAssignment
                                                                    delegate(object x, object y) { x = y; });
                // ReSharper restore RedundantAssignment
                GetPrimaryKey = propertyExpression.Compile();
            }
            return this;
        }

        #endregion
    }
}