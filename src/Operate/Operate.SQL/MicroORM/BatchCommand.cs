﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;
using Operate.SQL.MicroORM.Interfaces;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL.MicroORM
{
    /// <summary>
    /// Holds information for a set of commands
    /// </summary>
    public class BatchCommand : ICommand, IBatchCommand
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="commands">Commands that are being merged for batching</param>
        public BatchCommand(params Command[] commands)
        {
            SQLCommand_ = "";
            Parameters_ = new List<IParameter>();
            CommandType = CommandType.Text;
            Commands = new List<ICommand>();
            Batched = false;
            AddCommands(commands);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Number of commands being batched
        /// </summary>
        public virtual int CommandCount { get { return Commands.Count; } }

        /// <summary>
        /// Actual SQL command
        /// </summary>
        public virtual string SQLCommand
        {
            get
            {
                if (!Batched)
                    Batch();
                return SQLCommand_;
            }
        }

        /// <summary>
        /// Batched SQL Command
        /// </summary>
        protected string SQLCommand_ { get; private set; }

        /// <summary>
        /// Command type
        /// </summary>
        public virtual CommandType CommandType { get; protected set; }

        /// <summary>
        /// Parameters associated with the command
        /// </summary>
        public virtual ICollection<IParameter> Parameters
        {
            get
            {
                if (!Batched)
                    Batch();
                return Parameters_;
            }
        }

        /// <summary>
        /// Batched parameter list
        /// </summary>
        protected ICollection<IParameter> Parameters_ { get; private set; }

        /// <summary>
        /// Used to parse SQL commands to find parameters (when batching)
        /// </summary>
        private static Regex ParameterRegex = new Regex(@"[^@](?<ParamStart>[:@?])(?<ParamName>\w+)", RegexOptions.Compiled);

        /// <summary>
        /// Commands to batch
        /// </summary>
        protected ICollection<ICommand> Commands { get; private set; }

        /// <summary>
        /// Has this been batched?
        /// </summary>
        protected bool Batched { get; set; }

        #endregion

        #region Functions

        #region AddCommand

        /// <summary>
        /// Adds a command to be batched
        /// </summary>
        /// <param name="command">Command (SQL or stored procedure) to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameterPrefix">Parameter prefix to use</param>
        /// <param name="parameters">Parameters to add</param>
        /// <returns>This</returns>
        public IBatchCommand AddCommand(string command, CommandType commandType, string parameterPrefix, params object[] parameters)
        {
            return AddCommands(new Command(command, commandType, parameterPrefix, parameters));
        }

        /// <summary>
        /// Adds a command to be batched
        /// </summary>
        /// <param name="command">Command (SQL or stored procedure) to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters to add</param>
        /// <returns>This</returns>
        public IBatchCommand AddCommand(string command, CommandType commandType, params IParameter[] parameters)
        {
            return AddCommands(new Command(command, commandType, parameters));
        }

        #endregion

        #region AddCommands

        /// <summary>
        /// Adds a command to be batched
        /// </summary>
        /// <param name="commands">Commands to add</param>
        /// <returns>This</returns>
        public virtual IBatchCommand AddCommands(params Command[] commands)
        {
            if (commands != null)
            {
                Batched = false;
                Commands.Add(commands);
            }
            return this;
        }

        #endregion

        #region AddParameter

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="id">Parameter ID</param>
        /// <param name="parameter">Parameter value</param>
        /// <typeparam name="TDataType">Data type of the parameter</typeparam>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        public virtual void AddParameter<TDataType>(string id, TDataType parameter, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
        }

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="id">Parameter ID</param>
        /// <param name="parameter">Parameter value</param>
        /// <param name="direction">Parameter direction</param>
        /// <param name="parameterStarter">Parameter starter</param>
        public virtual void AddParameter(string id, string parameter, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
        }

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="parameters">Parameter values</param>
        /// <param name="parameterStarter">Parameter starter</param>
        public virtual void AddParameter(string parameterStarter, params object[] parameters)
        {
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        public virtual void AddParameter(string id, SqlDbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        public virtual void AddParameter(string id, DbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
        }

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="parameters">Parameter values</param>
        public virtual void AddParameter(params IParameter[] parameters)
        {
        }

        #endregion

        #region ClearParameters

        /// <summary>
        /// Clears the parameters for the command
        /// </summary>
        public virtual void ClearParameters()
        {
        }

        #endregion

        #region Batch

        /// <summary>
        /// Batches the data
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        protected virtual void Batch()
        {
            Batched = true;
            Parameters_ = new List<IParameter>();
            SQLCommand_ = "";
            int count = 0;
            if (Commands.Count == 1)
            {
                var firstOrDefault = Commands.FirstOrDefault();
                if (firstOrDefault != null) SQLCommand_ = firstOrDefault.SQLCommand;
                var orDefault = Commands.FirstOrDefault();
                if (orDefault != null) Parameters_ = orDefault.Parameters;
                return;
            }
            foreach (ICommand command in Commands)
            {
                if (command.CommandType == CommandType.Text)
                {
                    int count1 = count;
                    SQLCommand_ += string.IsNullOrEmpty(command.SQLCommand) ?
                                        "" :
                                        ParameterRegex.Replace(command.SQLCommand, x =>
                                        {
                                            if (!x.Value.StartsWith("@@", StringComparison.InvariantCulture))
                                                return x.Value + "Command" + count1.ToString(CultureInfo.InvariantCulture);
                                            return x.Value;
                                        }) + Environment.NewLine;
                    foreach (IParameter parameter in command.Parameters)
                    {
                        Parameters_.Add(parameter.CreateCopy("Command" + count.ToString(CultureInfo.InvariantCulture)));
                    }
                }
                else
                {
                    SQLCommand_ += command.SQLCommand + Environment.NewLine;
                    foreach (IParameter parameter in command.Parameters)
                    {
                        Parameters_.Add(parameter.CreateCopy(""));
                    }
                }
                ++count;
            }
        }

        #endregion

        #region Equals

        /// <summary>
        /// Determines if the objects are equal
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>Determines if the commands are equal</returns>
        public override bool Equals(object obj)
        {
            var otherCommand = obj as Command;
            if (otherCommand == null)
                return false;

            if (otherCommand.SQLCommand != SQLCommand
                || otherCommand.CommandType != CommandType
                || Parameters.Count != otherCommand.Parameters.Count)
                return false;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (IParameter parameter in Parameters)
                // ReSharper restore LoopCanBeConvertedToQuery
                if (!otherCommand.Parameters.Contains(parameter))
                    return false;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (IParameter parameter in otherCommand.Parameters)
                // ReSharper restore LoopCanBeConvertedToQuery
                if (!Parameters.Contains(parameter))
                    return false;

            return true;
        }

        #endregion

        #region GetHashCode

        /// <summary>
        /// Returns the hash code for the command
        /// </summary>
        /// <returns>The hash code for the object</returns>
        public override int GetHashCode()
        {
            int ParameterTotal = Parameters.Sum(x => x.GetHashCode());
            if (ParameterTotal > 0)
                return (SQLCommand.GetHashCode() * 23 + CommandType.GetHashCode()) * 23 + ParameterTotal;
            return SQLCommand.GetHashCode() * 23 + CommandType.GetHashCode();
        }

        #endregion

        #region ToString

        /// <summary>
        /// Returns the string representation of the command
        /// </summary>
        /// <returns>The string representation of the command</returns>
        public override string ToString()
        {
            return SQLCommand;
        }

        #endregion

        #endregion
    }
}