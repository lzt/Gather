﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Operate.ExtensionMethods;
using Operate.SQL.MicroORM.Interfaces;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL.MicroORM
{
    /// <summary>
    /// Holds information for an individual SQL command
    /// </summary>
    public class Command : ICommand
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sqlCommand">Actual SQL command</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        public Command(string sqlCommand, CommandType commandType, string parameterStarter = "@", params object[] parameters)
        {
            CommandType = commandType;
            SQLCommand = sqlCommand;
            Parameters = new List<IParameter>();
            AddParameter(parameterStarter, parameters);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sqlCommand">Actual SQL command</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters</param>
        public Command(string sqlCommand, CommandType commandType, params IParameter[] parameters)
        {
            CommandType = commandType;
            SQLCommand = sqlCommand;
            Parameters = new List<IParameter>();
            AddParameter(parameters);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Actual SQL command
        /// </summary>
        public string SQLCommand { get; protected set; }

        /// <summary>
        /// Command type
        /// </summary>
        public CommandType CommandType { get; protected set; }

        /// <summary>
        /// Parameters associated with the command
        /// </summary>
        public ICollection<IParameter> Parameters { get; private set; }

        #endregion

        #region Functions

        #region AddParameter

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="id">Parameter ID</param>
        /// <param name="parameter">Parameter value</param>
        /// <typeparam name="TDataType">Data type of the parameter</typeparam>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">Parameter prefix</param>
        public virtual void AddParameter<TDataType>(string id, TDataType parameter, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            Parameters.Add(new Parameter<TDataType>(id, parameter, direction, parameterStarter));
        }

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="id">Parameter ID</param>
        /// <param name="parameter">Parameter value</param>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">Parameter starter</param>
        public virtual void AddParameter(string id, string parameter, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            Parameters.Add(new StringParameter(id, parameter, direction, parameterStarter));
        }

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="parameters">Parameter values</param>
        /// <param name="parameterStarter">Parameter starter</param>
        public virtual void AddParameter(string parameterStarter = "@", params object[] parameters)
        {
            if (parameters != null)
            {
                foreach (object parameter in parameters)
                {
                    var tempParameter = parameter as string;
                    if (parameter == null)
                        Parameters.Add(new Parameter<object>(Parameters.Count.ToString(CultureInfo.InvariantCulture), default(DbType), null, ParameterDirection.Input, parameterStarter));
                    else if (tempParameter != null)
                        Parameters.Add(new StringParameter(Parameters.Count.ToString(CultureInfo.InvariantCulture), tempParameter, ParameterDirection.Input, parameterStarter));
                    else
                        Parameters.Add(new Parameter<object>(Parameters.Count.ToString(CultureInfo.InvariantCulture), parameter, ParameterDirection.Input, parameterStarter));
                }
            }
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <param name="parameterStarter">Parameter starter</param>
        public virtual void AddParameter(string id, SqlDbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            AddParameter(id, type.To(DbType.Int32), value, direction, parameterStarter);
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <param name="parameterStarter">Parameter starter</param>
        public virtual void AddParameter(string id, DbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            Parameters.Add(new Parameter<object>(id, type, value, direction, parameterStarter));
        }

        /// <summary>
        /// Adds a parameter to the command
        /// </summary>
        /// <param name="parameters">Parameter values</param>
        public virtual void AddParameter(params IParameter[] parameters)
        {
            if (parameters != null)
                foreach (IParameter parameter in parameters)
                    Parameters.Add(parameter);
        }

        #endregion

        #region ClearParameters

        /// <summary>
        /// Clears the parameters for the command
        /// </summary>
        public virtual void ClearParameters()
        {
            Parameters.Clear();
        }

        #endregion

        #region Equals

        /// <summary>
        /// Determines if the objects are equal
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>Determines if the commands are equal</returns>
        public override bool Equals(object obj)
        {
            var otherCommand = obj as Command;
            if (otherCommand == null)
                return false;

            if (otherCommand.SQLCommand != SQLCommand
                || otherCommand.CommandType != CommandType
                || Parameters.Count != otherCommand.Parameters.Count)
                return false;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (IParameter parameter in Parameters)
                // ReSharper restore LoopCanBeConvertedToQuery
                if (!otherCommand.Parameters.Contains(parameter))
                    return false;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (IParameter parameter in otherCommand.Parameters)
                // ReSharper restore LoopCanBeConvertedToQuery
                if (!Parameters.Contains(parameter))
                    return false;

            return true;
        }

        #endregion

        #region GetHashCode

        /// <summary>
        /// Returns the hash code for the command
        /// </summary>
        /// <returns>The hash code for the object</returns>
        public override int GetHashCode()
        {
            int parameterTotal = Parameters.Sum(x => x.GetHashCode());
            if (parameterTotal > 0)
                return (SQLCommand.GetHashCode() * 23 + CommandType.GetHashCode()) * 23 + parameterTotal;
            return SQLCommand.GetHashCode() * 23 + CommandType.GetHashCode();
        }

        #endregion

        #region ToString

        /// <summary>
        /// Returns the string representation of the command
        /// </summary>
        /// <returns>The string representation of the command</returns>
        public override string ToString()
        {
            return SQLCommand;
        }

        #endregion

        #endregion
    }
}