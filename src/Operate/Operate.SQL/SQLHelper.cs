﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Xml;
using Operate.Caching;
using Operate.Comparison;
using Operate.ExtensionMethods;
using Operate.Patterns;
using Operate.Patterns.BaseClasses;
using Operate.SQL.ExtensionMethods;
using Operate.SQL.MicroORM;
using Operate.SQL.MicroORM.Interfaces;
using Operate.SQL.ParameterTypes;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL
{
    /// <summary>
    /// SQL Helper class
    /// </summary>
    public class SQLHelper : SafeDisposableBaseClass, IFluentInterface
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="database">Database name (can be used later to pull connection information)</param>
        public SQLHelper(string database = "Default")
        {
            Command = new Command("", CommandType.Text);
            DatabaseUsing = GetDatabase(database);
            Factory = DbProviderFactories.GetFactory(DatabaseUsing.DbType);
            Connection = Factory.CreateConnection();
            if (Connection != null) Connection.ConnectionString = DatabaseUsing.Connection;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="command">Command to use</param>
        /// <param name="database">Database name (can be used later to pull connection information)</param>
        public SQLHelper(ICommand command, string database = "Default")
        {
            Command = command;
            DatabaseUsing = GetDatabase(database);
            Factory = DbProviderFactories.GetFactory(DatabaseUsing.DbType);
            Connection = Factory.CreateConnection();
            if (Connection != null) Connection.ConnectionString = DatabaseUsing.Connection;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="command">Stored procedure/SQL Text to use</param>
        /// <param name="commandType">The command type of the command sent in</param>
        /// <param name="database">Database to use</param>
        public SQLHelper(string command, CommandType commandType, string database = "Default")
        {
            DatabaseUsing = GetDatabase(database);
            Command = new Command(command, commandType, DatabaseUsing.ParameterPrefix);
            Factory = DbProviderFactories.GetFactory(DatabaseUsing.DbType);
            Connection = Factory.CreateConnection();
            if (Connection != null) Connection.ConnectionString = DatabaseUsing.Connection;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Db provider factory (creates connections, etc.)
        /// </summary>
        private DbProviderFactory Factory { get; set; }

        /// <summary>
        /// Connection to the database
        /// </summary>
        private DbConnection Connection { get; set; }

        /// <summary>
        /// The executable command
        /// </summary>
        private DbCommand ExecutableCommand { get; set; }

        /// <summary>
        /// The data reader for the query
        /// </summary>
        private IDataReader Reader { get; set; }

        /// <summary>
        /// List of database connections
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        private static readonly ConcurrentDictionary<string, Database> Databases = new ConcurrentDictionary<string, Database>();

        /// <summary>
        /// Cache that is used internally
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        private static readonly Cache<int> Cache = new Cache<int>();

        /// <summary>
        /// Database using
        /// </summary>
        private Database DatabaseUsing { get; set; }

        /// <summary>
        /// Command using
        /// </summary>
        private ICommand Command { get; set; }

        #endregion

        #region Protected Functions

        #region BeginTransaction

        /// <summary>
        /// Begins a transaction
        /// </summary>
        // ReSharper disable UnusedMember.Local
        private SQLHelper BeginTransaction()
        // ReSharper restore UnusedMember.Local
        {
            ExecutableCommand.BeginTransaction();
            return this;
        }

        #endregion

        #region Close

        /// <summary>
        /// Closes the connection
        /// </summary>
        private void Close()
        {
            ExecutableCommand.Close();
        }

        #endregion

        #region Commit

        /// <summary>
        /// Commits a transaction
        /// </summary>
        private void Commit()
        {
            ExecutableCommand.Commit();
        }

        #endregion

        #region GetDatabase

        /// <summary>
        /// Adds a database's info
        /// </summary>
        /// <param name="name">Name to associate with the database (if not specified, it uses the first connection string it finds in the configuration manager)</param>
        /// <returns>The database object specified</returns>
        private Database GetDatabase(string name = "Default")
        {
            Database tempDatabase = Databases.GetValue(name);
            return tempDatabase ?? Database("", name, "", "");
        }

        #endregion

        #region Open

        /// <summary>
        /// Opens the connection
        /// </summary>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private void Open()
        {
            ExecutableCommand.Open();
        }

        #endregion

        #region Rollback

        /// <summary>
        /// Rolls back a transaction
        /// </summary>
        private void Rollback()
        {
            ExecutableCommand.Rollback();
        }

        #endregion

        #region SetupScalarCommand

        /// <summary>
        /// Sets up the scalar command
        /// </summary>
        /// <param name="command">Command to create with</param>
        /// <param name="parameters">Parameter list</param>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The string command</returns>
        private string SetupScalarCommand<TClassType>(string command, Mapping<TClassType> mapping, params IParameter[] parameters)
            where TClassType : class,new()
        {
            string whereCommand = "";
            if (parameters != null && parameters.Length > 0)
            {
                whereCommand += " WHERE ";
                string splitter = "";
                foreach (IParameter parameter in parameters)
                {
                    whereCommand += splitter + parameter;
                    splitter = " AND ";
                }
            }
            return string.Format(CultureInfo.InvariantCulture, "SELECT {0} FROM {1} {2}", command, mapping.TableName, whereCommand);
        }

        #endregion

        #region SetupDeleteCommand

        /// <summary>
        /// Sets up the delete command
        /// </summary>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The command string</returns>
        private string SetupDeleteCommand<TClassType>(Mapping<TClassType> mapping)
            where TClassType : class,new()
        {
            return string.Format(CultureInfo.InvariantCulture, "DELETE FROM {0} WHERE {1}", mapping.TableName, mapping.PrimaryKey + "=" + DatabaseUsing.ParameterPrefix + mapping.PrimaryKey);
        }

        #endregion

        #region SetupInsertCommand

        /// <summary>
        /// Sets up the insert command
        /// </summary>
        /// <param name="parameters">Parameters</param>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The command string</returns>
        private string SetupInsertCommand<TClassType>(Mapping<TClassType> mapping, params IParameter[] parameters)
            where TClassType : class,new()
        {
            string parameterList = "";
            string valueList = "";
            string splitter = "";
            foreach (string name in mapping.ParameterNames)
            {
                if (!mapping.AutoIncrement || name != mapping.PrimaryKey)
                {
                    parameterList += splitter + name;
                    valueList += splitter + DatabaseUsing.ParameterPrefix + name;
                    splitter = ",";
                }
            }
            foreach (IParameter parameter in parameters)
            {
                parameterList += splitter + parameter.ID;
                valueList += splitter + DatabaseUsing.ParameterPrefix + parameter.ID;
                splitter = ",";
            }
            return string.Format(CultureInfo.InvariantCulture, "INSERT INTO {0}({1}) VALUES({2}) SELECT scope_identity() as [ID]", mapping.TableName, parameterList, valueList);
        }

        #endregion

        #region SetupPageCountCommand

        /// <summary>
        /// Sets up the page count command
        /// </summary>
        /// <param name="parameters">Parameter list</param>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The string command</returns>
        private string SetupPageCountCommand<TClassType>(Mapping<TClassType> mapping, params IParameter[] parameters)
            where TClassType : class,new()
        {
            string whereCommand = "";
            if (parameters != null && parameters.Length > 0)
            {
                whereCommand += " WHERE ";
                string splitter = "";
                foreach (IParameter parameter in parameters)
                {
                    whereCommand += splitter + parameter;
                    splitter = " AND ";
                }
            }
            return string.Format(CultureInfo.InvariantCulture, "SELECT COUNT(*) as Total FROM (SELECT {0} FROM {1} {2}) as Query", mapping.PrimaryKey, mapping.TableName, whereCommand);
        }

        /// <summary>
        /// Sets up the page count command
        /// </summary>
        /// <param name="command">Command</param>
        /// <returns>The string command</returns>
        private string SetupPageCountCommand(string command)
        {
            return string.Format(CultureInfo.InvariantCulture, "SELECT COUNT(*) as Total FROM ({0}) as Query", command);
        }

        #endregion

        #region SetupPagedCommand

        /// <summary>
        /// Sets up the paged select command
        /// </summary>
        /// <param name="columns">Columns to return</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page</param>
        /// <param name="parameters">Parameter list</param>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The command string</returns>
        private string SetupPagedCommand<TClassType>(string columns, string orderBy, int pageSize, int currentPage,
            // ReSharper disable MethodOverloadWithOptionalParameter
                                                              Mapping<TClassType> mapping, params IParameter[] parameters)
            // ReSharper restore MethodOverloadWithOptionalParameter
            where TClassType : class,new()
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = mapping.PrimaryKey;

            string whereCommand = "";
            if (parameters != null && parameters.Length > 0)
            {
                whereCommand += " WHERE ";
                string splitter = "";
                foreach (IParameter parameter in parameters)
                {
                    whereCommand += splitter + parameter;
                    splitter = " AND ";
                }
            }
            return SetupPagedCommand(string.Format(CultureInfo.InvariantCulture, "SELECT {0} FROM {1} {2}", columns, mapping.TableName, whereCommand), orderBy, pageSize, currentPage, mapping);
        }

        /// <summary>
        /// Sets up the paged select command
        /// </summary>
        /// <param name="query">Query used in getting the paged data</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page</param>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The command string</returns>
        private string SetupPagedCommand<TClassType>(string query, string orderBy, int pageSize,
                                                              int currentPage, Mapping<TClassType> mapping)
            where TClassType : class,new()
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = mapping.PrimaryKey;
            int pageStart = currentPage * pageSize;
            return string.Format(CultureInfo.InvariantCulture, "SELECT Paged.* FROM (SELECT ROW_NUMBER() OVER (ORDER BY {1}) AS Row, Query.* FROM ({0}) as Query) AS Paged WHERE Row>{2} AND Row<={3}",
                                    query,
                                    orderBy,
                                    pageStart,
                                    pageStart + pageSize);
        }

        #endregion

        #region SetupSelectCommand

        /// <summary>
        /// Sets up the select command
        /// </summary>
        /// <param name="columns">Columns to return</param>
        /// <param name="limit">limit on the number of items to return</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="parameters">Parameter list</param>
        /// <param name="mapping">Mapping information</param>
        /// <returns>The string command</returns>
        private string SetupSelectCommand<TClassType>(string columns, int limit, string orderBy, Mapping<TClassType> mapping,
                                                               params IParameter[] parameters)
            where TClassType : class,new()
        {
            string command = (limit > 0 ? "SELECT TOP " + limit : "SELECT") + " {0} FROM {1}";
            if (parameters != null && parameters.Length > 0)
            {
                command += " WHERE ";
                string splitter = "";
                foreach (IParameter parameter in parameters)
                {
                    command += splitter + parameter;
                    splitter = " AND ";
                }
            }
            if (!string.IsNullOrEmpty(orderBy))
                command += orderBy.Trim().ToUpperInvariant().StartsWith("ORDER BY", StringComparison.CurrentCultureIgnoreCase) ? " " + orderBy : " ORDER BY " + orderBy;
            return string.Format(CultureInfo.InvariantCulture, command, columns, mapping.TableName);
        }

        #endregion

        #region SetupUpdateCommand

        /// <summary>
        /// Sets up the update command
        /// </summary>
        /// <param name="mapping">Mapping information</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>The command string</returns>
        private string SetupUpdateCommand<TClassType>(Mapping<TClassType> mapping, params IParameter[] parameters)
            where TClassType : class,new()
        {
            string parameterList = "";
            string whereCommand = mapping.PrimaryKey + "=" + DatabaseUsing.ParameterPrefix + mapping.PrimaryKey;
            string splitter = "";
            foreach (string name in mapping.ParameterNames)
            {
                if (name != mapping.PrimaryKey)
                {
                    parameterList += splitter + name + "=" + DatabaseUsing.ParameterPrefix + name;
                    splitter = ",";
                }
            }
            foreach (IParameter parameter in parameters)
            {
                parameterList += splitter + parameter;
                splitter = ",";
            }
            return string.Format(CultureInfo.InvariantCulture, "UPDATE {0} SET {1} WHERE {2}", mapping.TableName, parameterList, whereCommand);
        }

        #endregion

        #endregion

        #region Public Functions

        #region AddParameter

        /// <summary>
        /// Adds a parameter to the call (for strings only)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <returns>This</returns>
        public SQLHelper AddParameter(string id, string value = "", ParameterDirection direction = ParameterDirection.Input)
        {
            Command.AddParameter(id, value, direction);
            return this;
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <returns>This</returns>
        public SQLHelper AddParameter(string id, SqlDbType type, object value = null, ParameterDirection direction = ParameterDirection.Input)
        {
            Command.AddParameter(id, type, value, direction);
            return this;
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <typeparam name="TDataType">Data type of the parameter</typeparam>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <returns>This</returns>
        public SQLHelper AddParameter<TDataType>(string id, TDataType value = default(TDataType), ParameterDirection direction = ParameterDirection.Input)
        {
            Command.AddParameter(id, value, direction);
            return this;
        }

        /// <summary>
        /// Adds a parameter to the call (for all types other than strings)
        /// </summary>
        /// <param name="id">Name of the parameter</param>
        /// <param name="value">Value to add</param>
        /// <param name="type">SQL type of the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <returns>This</returns>
        public SQLHelper AddParameter(string id, DbType type, object value = null, ParameterDirection direction = ParameterDirection.Input)
        {
            Command.AddParameter(id, type, value, direction);
            return this;
        }

        /// <summary>
        /// Adds parameters to the call
        /// </summary>
        /// <param name="parameters">Parameters to add</param>
        /// <returns>This</returns>
        public SQLHelper AddParameter(params IParameter[] parameters)
        {
            Command.AddParameter(parameters);
            return this;
        }

        /// <summary>
        /// Adds parameters to the call
        /// </summary>
        /// <param name="parameters">Parameters to add</param>
        /// <returns>This</returns>
        public SQLHelper AddParameter(params object[] parameters)
        {
            Command.AddParameter(DatabaseUsing.ParameterPrefix, parameters);
            return this;
        }

        #endregion

        #region All

        /// <summary>
        /// Gets a list of all objects that meet the specified criteria
        /// </summary>
        /// <param name="command">Command to use (can be an SQL string or stored procedure)</param>
        /// <param name="commandType">Command type</param>
        /// <param name="objects">Objects to modify/addon to (uses primary key to determine)</param>
        /// <param name="objectCreator">Function used to create the indvidual objects</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>A list of all objects that meet the specified criteria</returns>
        public IEnumerable<TClassType> All<TClassType>(string command, CommandType commandType, IEnumerable<TClassType> objects = null,
                                                            Func<TClassType> objectCreator = null, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (command.IsNullOrEmpty()) { throw new ArgumentNullException("command"); }
            if (DatabaseUsing.Mappings.IsNull()) { throw new ArgumentNullException("command"); }
            if (objects != null)
            {
                var classTypes = objects as TClassType[] ?? objects.ToArray();
                var Return = classTypes.ToList();
                objectCreator = objectCreator ?? (() => new TClassType());
                Command = new Command(command, commandType, parameters);
                var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
                ExecuteReader(false, cache);
                while (Read())
                {
                    bool add = false;
                    var currentKey = GetParameter<object>(mappingUsing.PrimaryKey);
                    TClassType temp = classTypes.FirstOrDefault(x => mappingUsing.GetPrimaryKey(x).Equals(currentKey));
                    if (temp == default(TClassType))
                    {
                        temp = objectCreator();
                        add = true;
                    }
                    mappingUsing.Mappings.Copy(this, temp);
                    if (add) Return.Add(temp);
                }
                return Return;
            }
            return null;
        }

        /// <summary>
        /// Gets a list of all objects that meet the specified criteria
        /// </summary>
        /// <param name="columns">Columns to return</param>
        /// <param name="limit">Limit on the number of items to return</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="objects">Objects to modify/addon to (uses primary key to determine)</param>
        /// <param name="objectCreator">Function used to create the individual objects</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>A list of all objects that meet the specified criteria</returns>
        public IEnumerable<TClassType> All<TClassType>(string columns = "*", int limit = 0, string orderBy = "", IEnumerable<TClassType> objects = null,
                                                            Func<TClassType> objectCreator = null, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (columns.IsNullOrEmpty()) { throw new ArgumentNullException("columns"); }
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return All(SetupSelectCommand(columns, limit, orderBy, mappingUsing, parameters), CommandType.Text, objects, objectCreator, cache, parameters);
        }

        #endregion

        #region Any

        /// <summary>
        /// Gets a single object that fits the criteria
        /// </summary>
        /// <param name="columns">Columns to select</param>
        /// <param name="objectToReturn">Object to return (in case the object needs to be created outside this,
        /// or default value is desired in case of nothing found)</param>
        /// <param name="objectCreator">Function used to create the object if the ObjectToReturn is set to null
        /// (if set to null, it just creates a new object using the default constructor)</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>An object fitting the criteria specified or null if none are found</returns>
        public TClassType Any<TClassType>(string columns = "*", TClassType objectToReturn = null, Func<TClassType> objectCreator = null, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (columns.IsNullOrEmpty()) { throw new ArgumentNullException("columns"); }
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return Any(SetupSelectCommand(columns, 1, "", mappingUsing, parameters), CommandType.Text, objectToReturn, objectCreator, cache, parameters);
        }

        /// <summary>
        /// Gets a single object that fits the criteria
        /// </summary>
        /// <param name="command">Command to use (can be an SQL string or stored procedure name)</param>
        /// <param name="commandType">Command type</param>
        /// <param name="objectToReturn">Object to return (in case the object needs to be created outside this,
        /// or default value is desired in case of nothing found)</param>
        /// <param name="objectCreator">Function used to create the object if the ObjectToReturn is set to null
        /// (if set to null, it just creates a new object using the default constructor)</param>
        /// <param name="parameters">Parameters used to search by</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>An object fitting the criteria specified or null if none are found</returns>
        public TClassType Any<TClassType>(string command, CommandType commandType, TClassType objectToReturn = null, Func<TClassType> objectCreator = null,
                                                bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (DatabaseUsing.Mappings.IsNull()) { throw new ArgumentNullException("command"); }
            if (command.IsNullOrEmpty()) { throw new ArgumentNullException("command"); }
            objectCreator = objectCreator ?? (() => new TClassType());
            Command = new Command(command, commandType, parameters);
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            ExecuteReader(false, cache);
            if (Read())
            {
                TClassType Return = objectToReturn ?? objectCreator();
                mappingUsing.Mappings.Copy(this, Return);
                return Return;
            }
            return objectToReturn;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes an object from the database
        /// </summary>
        /// <param name="command">Command to use</param>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="commandType">Command type</param>
        /// <param name="Object">Object to delete</param>
        /// <returns>The number of rows deleted</returns>
        public int Delete<TClassType>(string command, CommandType commandType, TClassType Object)
            where TClassType : class,new()
        {
              if (Object.IsNull()) { throw new ArgumentNullException("Object"); }
            if (command.IsNullOrEmpty()) { throw new ArgumentNullException("command"); }
            Command = new Command(command, commandType, DatabaseUsing.ParameterPrefix);
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            mappingUsing.Mappings.Copy(Object, this);
            return ExecuteNonQuery(true);
        }

        /// <summary>
        /// Deletes an object from the database
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="Object">Object to delete</param>
        /// <returns>The number of rows deleted</returns>
        public int Delete<TClassType>(TClassType Object)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return Delete(SetupDeleteCommand(mappingUsing), CommandType.Text, Object);
        }

        /// <summary>
        /// Deletes a list of objects from the database
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="objects">Objects to delete</param>
        /// <returns>The number of rows deleted</returns>
        public int Delete<TClassType>(IEnumerable<TClassType> objects)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            IBatchCommand commands = Batch();
            foreach (TClassType Object in objects)
            {
                commands.AddCommand(SetupDeleteCommand(mappingUsing), CommandType.Text, DatabaseUsing.ParameterPrefix, mappingUsing.GetPrimaryKey(Object));
            }
            return ExecuteNonQuery(true);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts an object based on the command specified
        /// </summary>
        /// <typeparam name="TDataType">Data type expected to be returned from the query (to get the ID, etc.)</typeparam>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="command">Command to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="Object">Object to insert</param>
        /// <param name="parameters">Parameters sent into the function</param>
        /// <returns>The returned object from the query (usually the newly created row's ID)</returns>
        public TDataType Insert<TClassType, TDataType>(string command, CommandType commandType, TClassType Object, params IParameter[] parameters)
            where TClassType : class,new()
        {
              if (Object.IsNull()) { throw new ArgumentNullException("Object"); }
            if (command.IsNullOrEmpty()) { throw new ArgumentNullException("command"); }
            Command = new Command(command, commandType, parameters);
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            mappingUsing.Mappings.Copy(Object, this);
            return ExecuteScalar<TDataType>(true);
        }

        /// <summary>
        /// Inserts an object into the database
        /// </summary>
        /// <typeparam name="TDataType">Data type expected (should be the same type as the primary key)</typeparam>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="Object">Object to insert</param>
        /// <param name="parameters">Parameters sent into the function</param>
        /// <returns>The returned object from the query (the newly created row's ID)</returns>
        public TDataType Insert<TClassType, TDataType>(TClassType Object, params IParameter[] parameters)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return Insert<TClassType, TDataType>(SetupInsertCommand(mappingUsing, parameters), CommandType.Text, Object, parameters);
        }

        #endregion

        #region PageCount

        /// <summary>
        /// Gets the number of pages based on the specified 
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>The number of pages that the table contains for the specified page size</returns>
        public int PageCount<TClassType>(int pageSize = 25, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            Command = new Command(SetupPageCountCommand(mappingUsing, parameters), CommandType.Text, parameters);
            ExecuteReader(false, cache);
            if (Read())
            {
                int total = GetParameter("Total", 0);
                return total % pageSize == 0 ? total / pageSize : (total / pageSize) + 1;
            }
            return 0;
        }

        /// <summary>
        /// Gets the number of pages based on the specified 
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>The number of pages that the table contains for the specified page size</returns>
        public virtual int PageCount<TClassType>(string command, int pageSize = 25, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            Command = new Command(SetupPageCountCommand(command), CommandType.Text, parameters);
            ExecuteReader(false, cache);
            if (Read())
            {
                int total = GetParameter("Total", 0);
                return total % pageSize == 0 ? total / pageSize : (total / pageSize) + 1;
            }
            return 0;
        }

        #endregion

        #region Paged

        /// <summary>
        /// Gets a paged list of objects fitting the specified criteria
        /// </summary>
        /// <param name="columns">Columns to return</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">The current page (starting at 0)</param>
        /// <param name="objects">Objects to modify/addon to (uses primary key to determine)</param>
        /// <param name="objectCreator">Function used to create the individual objects (if set to null, it uses the default constructor)</param>
        /// <param name="cache">Should the item be cached</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <returns>A list of objects that fit the specified criteria</returns>
        public IEnumerable<TClassType> Paged<TClassType>(string columns = "*", string orderBy = "", int pageSize = 25, int currentPage = 0,
                                                               IEnumerable<TClassType> objects = null, Func<TClassType> objectCreator = null,
                                                               bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (columns.IsNullOrEmpty()) { throw new ArgumentNullException("columns"); }
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return All(SetupPagedCommand(columns, orderBy, pageSize, currentPage, mappingUsing, parameters), CommandType.Text, objects, objectCreator, cache, parameters);
        }

        #endregion

        #region PagedCommand

        /// <summary>
        /// Gets a paged list of objects fitting the specified criteria
        /// </summary>
        /// <param name="command">Command to return data from</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">The current page (starting at 0)</param>
        /// <param name="objects">Objects to modify/addon to (uses primary key to determine)</param>
        /// <param name="cache">Should the item be cached</param>
        /// <param name="objectCreator">Function used to create the individual objects (if set to null, it uses the default constructor)</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <returns>A list of objects that fit the specified criteria</returns>
        public IEnumerable<TClassType> PagedCommand<TClassType>(string command, string orderBy = "", int pageSize = 25, int currentPage = 0,
                                                                      IEnumerable<TClassType> objects = null, Func<TClassType> objectCreator = null,
                                                                      bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (command.IsNullOrEmpty()) { throw new ArgumentNullException("command"); }
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return All(SetupPagedCommand(command, orderBy, pageSize, currentPage, mappingUsing), CommandType.Text, objects, objectCreator, cache, parameters);
        }

        #endregion

        #region Save

        /// <summary>
        /// Saves (inserts/updates) an object based on the following criteria:
        /// 1) If autoincrement is set to true and the primary key is the default value, it inserts
        /// 2) If autoincrement is set to true and the primary key is not the default value, it updates
        /// 3) If autoincrement is set to false and the primary key is the default value, it inserts
        /// 4) If autoincrement is set to false and the primary key is not the default value,
        /// it does an Any call to see if the item is already in the database. If it is, it does an
        /// update. Otherwise it does an insert.
        /// On an insert, the primary key property is updated with the resulting value of the insert.
        /// </summary>
        /// <param name="Object">Object to save</param>
        /// <param name="parameters">Extra parameters to be added to the insert/update function</param>
        public void Save<TClassType, TPrimaryKeyType>(TClassType Object, params IParameter[] parameters)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            TPrimaryKeyType primaryKeyVal = mappingUsing.GetPrimaryKey(Object).To(default(TPrimaryKeyType));
            var comparer = new GenericEqualityComparer<TPrimaryKeyType>();
            IParameter param1;
            if (comparer.Equals(primaryKeyVal, default(TPrimaryKeyType)))
            {
                primaryKeyVal = Insert<TClassType, TPrimaryKeyType>(Object, parameters);
                mappingUsing.PrimaryKeyMapping.CopyRightToLeft(primaryKeyVal, Object);
                return;
            }
            if (mappingUsing.AutoIncrement)
            {
                Update(Object, parameters);
                return;
            }
            if (typeof(TPrimaryKeyType).Is(typeof(string)))
                param1 = new StringEqualParameter(primaryKeyVal.ToString(), mappingUsing.PrimaryKey, primaryKeyVal.ToString().Length, DatabaseUsing.ParameterPrefix);
            else
                param1 = new EqualParameter<TPrimaryKeyType>(primaryKeyVal, mappingUsing.PrimaryKey, DatabaseUsing.ParameterPrefix);
            var tempVal = Any<TClassType>(mappingUsing.PrimaryKey, null, null, false, param1);
            if (tempVal == null)
            {
                Insert<TClassType, TPrimaryKeyType>(Object, parameters);
                return;
            }
            Update(Object, parameters);
        }

        /// <summary>
        /// Saves (inserts/updates) a list of objects based on the following criteria:
        /// 1) If autoincrement is set to true and the primary key is the default value, it inserts
        /// 2) If autoincrement is set to true and the primary key is not the default value, it updates
        /// 3) If autoincrement is set to false and the primary key is the default value, it inserts
        /// 4) If autoincrement is set to false and the primary key is not the default value,
        /// it does an Any call to see if the item is already in the database. If it is, it does an
        /// update. Otherwise it does an insert.
        /// On an insert, the primary key property is updated with the resulting value of the insert.
        /// </summary>
        /// <param name="objects">Objects to save</param>
        /// <param name="parameters">Extra parameters to be added to the insert/update function</param>
        public void Save<TClassType, TPrimaryKeyType>(IEnumerable<TClassType> objects, params IParameter[] parameters)
            where TClassType : class,new()
        {
            foreach (TClassType Object in objects)
            {
                Save<TClassType, TPrimaryKeyType>(Object, parameters);
            }
        }

        #endregion

        #region Scalar

        /// <summary>
        /// Runs a supplied scalar function and returns the result
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <param name="cache">Should the item be cached</param>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <returns>The scalar value returned by the command</returns>
        public virtual TDataType Scalar<TClassType, TDataType>(string command, CommandType commandType, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            Command = new Command(command, CommandType.Text, parameters);
            return ExecuteScalar<TDataType>(false, cache);
        }

        /// <summary>
        /// Runs a scalar command using the specified aggregate function
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="aggregateFunction">Aggregate function</param>
        /// <param name="parameters">Parameters</param>
        /// <param name="cache">Should the item be cached</param>
        /// <returns>The scalar value returned by the command</returns>
        public TDataType Scalar<TClassType, TDataType>(string aggregateFunction, bool cache = false, params IParameter[] parameters)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            Command = new Command(SetupScalarCommand(aggregateFunction, mappingUsing, parameters), CommandType.Text, parameters);
            return ExecuteScalar<TDataType>(false, cache);
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates an object in the database
        /// </summary>
        /// <param name="command">Command to use</param>
        /// <param name="commandType">Command type</param>
        /// <param name="Object">Object to update</param>
        /// <param name="parameters">Parameters sent into the function</param>
        /// <returns>The number of rows updated</returns>
        public int Update<TClassType>(string command, CommandType commandType, TClassType Object, params IParameter[] parameters)
            where TClassType : class,new()
        {
            if (command.IsNullOrEmpty()) { throw new ArgumentNullException("command"); }
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            Command = new Command(command, CommandType.Text, parameters);
            mappingUsing.Mappings.Copy(Object, this);
            return ExecuteNonQuery(true);
        }

        /// <summary>
        /// Updates an object in the database
        /// </summary>
        /// <param name="Object">Object to update</param>
        /// <param name="parameters">Parameters sent into the function</param>
        /// <returns>The number of rows updated</returns>
        public int Update<TClassType>(TClassType Object, params IParameter[] parameters)
            where TClassType : class,new()
        {
            var mappingUsing = (Mapping<TClassType>)DatabaseUsing.Mappings[typeof(TClassType)];
            return Update(SetupUpdateCommand(mappingUsing, parameters), CommandType.Text, Object, parameters);
        }

        #endregion

        #region Batch

        /// <summary>
        /// Used to help batch various commands together
        /// </summary>
        /// <returns>The batch object</returns>
        public IBatchCommand Batch()
        {
            Command = new BatchCommand();
            return (IBatchCommand)Command;
        }

        #endregion

        #region Execute

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cache"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> Execute(bool cache = false)
        {
            try
            {
                Setup(true);
                if (cache && Cache.Exists(Command.GetHashCode()))
                    return Cache.Get<List<ExpandoObject>>(Command.GetHashCode());
                if (ExecutableCommand != null && !string.IsNullOrEmpty(ExecutableCommand.CommandText))
                {
                    Reader = ExecutableCommand.ExecuteReader();
                    var returnValues = new List<ExpandoObject>();
                    while (Reader.Read())
                    {
                        IDictionary<string, object> temp = new ExpandoObject();
                        for (int x = 0; x < Reader.FieldCount; ++x)
                        {
                            temp.Add(Reader.GetName(x), Reader[x] == DBNull.Value ? null : Reader[x]);
                        }
                        returnValues.Add((ExpandoObject)temp);
                    }
                    if (cache)
                        Cache.Add(Command.GetHashCode(), returnValues);
                    return returnValues;
                }
            }
            catch { Rollback(); throw; }
            return new List<ExpandoObject>();
        }

        #endregion

        #region ClearParameters

        /// <summary>
        /// Clears the parameters
        /// </summary>
        public SQLHelper ClearParameters()
        {
            Command.ClearParameters();
            return this;
        }

        #endregion

        #region ExecuteBulkCopy

        /// <summary>
        /// Does a bulk copy of the data (only usable on SQL Server)
        /// </summary>
        /// <param name="data">Data to copy over</param>
        /// <param name="destinationTable">Table to copy the data to</param>
        /// <param name="options">Options used during the copy</param>
        /// <param name="createTransaction">Create transaction</param>
        /// <returns>Returns this SQLHelper object</returns>
        public SQLHelper ExecuteBulkCopy<T>(IEnumerable<T> data, string destinationTable, SqlBulkCopyOptions options = SqlBulkCopyOptions.Default,
                                                    bool createTransaction = false)
        {
            using (DataTable table = data.ToDataTable())
            {
                return ExecuteBulkCopy(table, destinationTable, options, createTransaction);
            }
        }

        /// <summary>
        /// Does a bulk copy of the data (only usable on SQL Server)
        /// </summary>
        /// <param name="data">Data to copy over</param>
        /// <param name="destinationTable">Table to copy the data to</param>
        /// <param name="options">Options used during the copy</param>
        /// <param name="createTransaction">Create transaction</param>
        /// <returns>Returns this SQLHelper object</returns>
        public SQLHelper ExecuteBulkCopy(DataTable data, string destinationTable, SqlBulkCopyOptions options = SqlBulkCopyOptions.Default,
                                                 bool createTransaction = false)
        {
            if (DatabaseUsing.Profile)
            {
                // ReSharper disable CoVariantArrayConversion
                using (new Profiler.SQLProfiler("ExecuteBulkCopy", Command.SQLCommand, Command.Parameters.ToArray()))
                // ReSharper restore CoVariantArrayConversion
                {
                    BulkCopy(data, destinationTable, options, createTransaction);
                    return this;
                }
            }
            BulkCopy(data, destinationTable, options, createTransaction);
            return this;
        }

        /// <summary>
        /// Does the actual bulk copy
        /// </summary>
        /// <param name="data">Data to copy over</param>
        /// <param name="destinationTable">Table to copy the data to</param>
        /// <param name="options">Options used during the copy</param>
        /// <param name="createTransaction">Create transaction</param>
        private void BulkCopy(DataTable data, string destinationTable, SqlBulkCopyOptions options, bool createTransaction)
        {
            try
            {
                Setup(createTransaction);
                using (var copier = new SqlBulkCopy(Connection.ConnectionString, options))
                {
                    foreach (DataColumn column in data.Columns)
                        copier.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                    copier.DestinationTableName = destinationTable;
                    copier.WriteToServer(data);
                }
                Commit();
            }
            catch { Rollback(); throw; }
        }

        #endregion

        #region ExecuteDataSet

        /// <summary>
        /// Executes the query and returns a data set
        /// </summary>
        /// <param name="createTransaction">Create transaction</param>
        /// <param name="cache">Determines if the query should be cached for future queries</param>
        /// <returns>A dataset filled with the results of the query</returns>
        public DataSet ExecuteDataSet(bool createTransaction = false, bool cache = false)
        {
            Setup(createTransaction);
            if (DatabaseUsing.Profile)
            {
                // ReSharper disable CoVariantArrayConversion
                using (new Profiler.SQLProfiler("ExecuteDataSet", Command.SQLCommand, Command.Parameters.ToArray()))
                // ReSharper restore CoVariantArrayConversion
                {
                    return ExecuteDataSet(cache, null);
                }
            }
            return ExecuteDataSet(cache, null);
        }

        /// <summary>
        /// Executes the query and returns a data set
        /// </summary>
        /// <param name="cache">Determines if the query should be cached for future queries</param>
        /// <param name="returnValue">DataSet</param>
        /// <returns>A dataset filled with the results of the query</returns>
        private DataSet ExecuteDataSet(bool cache, DataSet returnValue)
        {
            try
            {
                if (cache && Cache.Exists(Command.GetHashCode()))
                    returnValue = Cache.Get<DataSet>(Command.GetHashCode());
                else if (ExecutableCommand != null && !string.IsNullOrEmpty(ExecutableCommand.CommandText))
                {
                    returnValue = ExecutableCommand.ExecuteDataSet(Factory);
                    if (cache)
                        Cache.Add(Command.GetHashCode(), returnValue);
                    Commit();
                }
                return returnValue;
            }
            catch { Rollback(); throw; }
        }

        #endregion

        #region ExecuteNonQuery

        /// <summary>
        /// Executes the stored procedure as a non query
        /// </summary>
        /// <param name="createTransaction">Creates a transaction for this command</param>
        /// <returns>Number of rows effected</returns>
        public int ExecuteNonQuery(bool createTransaction = false)
        {
            Setup(createTransaction);
            if (ExecutableCommand == null || string.IsNullOrEmpty(ExecutableCommand.CommandText))
                return 0;
            if (DatabaseUsing.Profile)
            {
                // ReSharper disable CoVariantArrayConversion
                using (new Profiler.SQLProfiler("ExecuteNonQuery", Command.SQLCommand, Command.Parameters.ToArray()))
                // ReSharper restore CoVariantArrayConversion
                {
                    return ExecuteNonQuery(createTransaction, 0);
                }
            }
            return ExecuteNonQuery(createTransaction, 0);
        }

        /// <summary>
        /// Executes the stored procedure as a non query
        /// </summary>
        /// <param name="createTransaction">Creates a transaction for this command</param>
        /// <param name="returnValue">Not used</param>
        /// <returns>Number of rows effected</returns>
        // ReSharper disable RedundantAssignment
        // ReSharper disable UnusedParameter.Local
        private int ExecuteNonQuery(bool createTransaction, int returnValue)
        // ReSharper restore UnusedParameter.Local
        // ReSharper restore RedundantAssignment
        {
            try
            {
                returnValue = ExecutableCommand.ExecuteNonQuery();
                Commit();
                return returnValue;
            }
            catch { Rollback(); throw; }
        }

        #endregion

        #region ExecuteReader

        /// <summary>
        /// Executes the stored procedure and returns a reader object
        /// </summary>
        /// <param name="createTransaction">Create transaction</param>
        /// <param name="cache">Determines if the query should be cached for future queries</param>
        /// <returns>this</returns>
        public SQLHelper ExecuteReader(bool createTransaction = false, bool cache = false)
        {
            Setup(createTransaction);
            if (DatabaseUsing.Profile)
            {
                // ReSharper disable CoVariantArrayConversion
                using (new Profiler.SQLProfiler("ExecuteReader", Command.SQLCommand, Command.Parameters.ToArray()))
                // ReSharper restore CoVariantArrayConversion
                {
                    return ExecuteReader(createTransaction, cache, false);
                }
            }
            return ExecuteReader(createTransaction, cache, false);
        }

        /// <summary>
        /// Executes the stored procedure/sql command and returns a reader object
        /// </summary>
        /// <param name="createTransaction">Create transaction</param>
        /// <param name="cache">Determines if the query should be cached for future queries</param>
        /// <param name="notUsed">Not used</param>
        /// <returns>this</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        // ReSharper disable UnusedParameter.Local
        private SQLHelper ExecuteReader(bool createTransaction, bool cache, bool notUsed)
        // ReSharper restore UnusedParameter.Local
        {
            try
            {
                if (cache && Cache.Exists(Command.GetHashCode()))
                    Reader = new CacheTables(Cache.Get<IDataReader>(Command.GetHashCode()));
                else if (ExecutableCommand != null && !string.IsNullOrEmpty(ExecutableCommand.CommandText))
                {
                    using (DbDataReader tempReader = ExecutableCommand.ExecuteReader())
                    {
                        Reader = new CacheTables(tempReader);
                    }
                    if (cache)
                        Cache.Add(Command.GetHashCode(), new CacheTables(Reader));
                    Commit();
                }
                return this;
            }
            catch { Rollback(); throw; }
        }

        #endregion

        #region ExecuteScalar

        /// <summary>
        /// Executes the stored procedure as a scalar query
        /// </summary>
        /// <typeparam name="TDataType">Data type to return</typeparam>
        /// <param name="createTransaction">Create transaction</param>
        /// <param name="cache">Determines if the query should be cached for future queries</param>
        /// <returns>The object of the first row and first column</returns>
        public TDataType ExecuteScalar<TDataType>(bool createTransaction = false, bool cache = false)
        {
            Setup(createTransaction);
            if (DatabaseUsing.Profile)
            {
                // ReSharper disable CoVariantArrayConversion
                using (new Profiler.SQLProfiler("ExecuteScalar", Command.SQLCommand, Command.Parameters.ToArray()))
                // ReSharper restore CoVariantArrayConversion
                {
                    return ExecuteScalar<TDataType>(createTransaction, cache, false);
                }
            }
            return ExecuteScalar<TDataType>(createTransaction, cache, false);
        }

        /// <summary>
        /// Executes the stored procedure as a scalar query
        /// </summary>
        /// <typeparam name="TDataType">Data type to return</typeparam>
        /// <param name="createTransaction">Create transaction</param>
        /// <param name="cache">Determines if the query should be cached for future queries</param>
        /// <param name="notUsed">Not used</param>
        /// <returns>The object of the first row and first column</returns>
        // ReSharper disable UnusedParameter.Local
        private TDataType ExecuteScalar<TDataType>(bool createTransaction, bool cache, bool notUsed)
        // ReSharper restore UnusedParameter.Local
        {
            try
            {
                TDataType returnValue = default(TDataType);
                if (cache && Cache.Exists(Command.GetHashCode()))
                    returnValue = Cache.Get<TDataType>(Command.GetHashCode());
                else if (ExecutableCommand != null && !string.IsNullOrEmpty(ExecutableCommand.CommandText))
                {
                    returnValue = ExecutableCommand.ExecuteScalar<TDataType>();
                    if (cache)
                        Cache.Add(Command.GetHashCode(), returnValue);
                    Commit();
                }
                return returnValue;
            }
            catch { Rollback(); throw; }
        }

        #endregion

        #region ExecuteXmlReader

        /// <summary>
        /// Executes the query and returns an XmlReader
        /// </summary>
        /// <param name="createTransaction">Create transaction</param>
        /// <returns>The XmlReader filled with the data from the query</returns>
        public XmlReader ExecuteXmlReader(bool createTransaction = false)
        {
            Setup(createTransaction);
            if (DatabaseUsing.Profile)
            {
                // ReSharper disable CoVariantArrayConversion
                using (new Profiler.SQLProfiler("ExecuteXmlReader", Command.SQLCommand, Command.Parameters.ToArray()))
                // ReSharper restore CoVariantArrayConversion
                {
                    return ExecuteXmlReader(createTransaction, false);
                }
            }
            return ExecuteXmlReader(createTransaction, false);
        }

        /// <summary>
        /// Executes the query and returns an XmlReader
        /// </summary>
        /// <param name="createTransaction">Create transaction</param>
        /// <param name="notUsed">Not used</param>
        /// <returns>The XmlReader filled with the data from the query</returns>
        // ReSharper disable UnusedParameter.Local
        private XmlReader ExecuteXmlReader(bool createTransaction, bool notUsed)
        // ReSharper restore UnusedParameter.Local
        {
            try
            {
                XmlReader returnValue = null;
                if (ExecutableCommand is SqlCommand && !string.IsNullOrEmpty(ExecutableCommand.CommandText))
                    returnValue = ((SqlCommand)ExecutableCommand).ExecuteXmlReader();
                Commit();
                return returnValue;
            }
            catch { Rollback(); throw; }
        }

        #endregion

        #region GetParameter

        /// <summary>
        /// Returns a parameter's value
        /// </summary>
        /// <typeparam name="TDataType">Data type of the object</typeparam>
        /// <param name="id">Parameter name</param>
        /// <param name="Default">Default value for the parameter</param>
        /// <param name="direction">Parameter direction (defaults to input)</param>
        /// <returns>if the parameter exists (and isn't null or empty), it returns the parameter's value. Otherwise the default value is returned.</returns>
        public TDataType GetParameter<TDataType>(string id, TDataType Default = default(TDataType), ParameterDirection direction = ParameterDirection.Input)
        {
            if (direction == ParameterDirection.Output)
                return ExecutableCommand.GetOutputParameter(id, Default);
            return Reader.GetParameter(id, Default);
        }

        /// <summary>
        /// Returns a parameter's value
        /// </summary>
        /// <typeparam name="TDataType">Data type of the object</typeparam>
        /// <param name="position">Position in the row</param>
        /// <param name="Default">Default value for the parameter</param>
        /// <returns>if the parameter exists (and isn't null or empty), it returns the parameter's value. Otherwise the default value is returned.</returns>
        public TDataType GetParameter<TDataType>(int position, TDataType Default = default(TDataType))
        {
            return Reader.GetParameter(position, Default);
        }

        #endregion

        #region NextResult

        /// <summary>
        /// Goes to the next result set (used if multiple queries are sent in)
        /// </summary>
        public SQLHelper NextResult()
        {
            if (Reader != null)
                Reader.NextResult();
            return this;
        }

        #endregion

        #region Read

        /// <summary>
        /// Is there more information?
        /// </summary>
        /// <returns>True if there is more rows, false otherwise</returns>
        public bool Read()
        {
            return Reader != null && Reader.Read();
        }

        #endregion

        #endregion

        #region Private Functions

        #region Setup

        /// <summary>
        /// Sets up the info for 
        /// </summary>
        /// <param name="transaction">Should a transaction be set up for this call?</param>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        private void Setup(bool transaction)
        {
            if (Reader != null)
            {
                Reader.Close();
                Reader.Dispose();
                Reader = null;
            }
            if (ExecutableCommand != null)
            {
                if (ExecutableCommand.Transaction != null)
                {
                    ExecutableCommand.Transaction.Dispose();
                    ExecutableCommand.Transaction = null;
                }
                ExecutableCommand.Parameters.Clear();
            }
            else
            {
                ExecutableCommand = Factory.CreateCommand();
            }
            if (ExecutableCommand != null)
            {
                ExecutableCommand.CommandText = Command.SQLCommand;
                ExecutableCommand.Connection = Connection;
                ExecutableCommand.CommandType = Command.CommandType;
                Command.Parameters.ForEach(x => x.AddParameter(ExecutableCommand));
                if (transaction)
                    ExecutableCommand.BeginTransaction();
            }
            Open();
        }

        #endregion

        #endregion

        #region Static Functions

        #region ClearAllMappings

        /// <summary>
        /// Clears all database objects of all mappings
        /// </summary>
        public static void ClearAllMappings()
        {
            foreach (string database in Databases.Keys)
                ClearMappings(database);
        }

        #endregion

        #region ClearCache

        /// <summary>
        /// Clears the cache
        /// </summary>
        public static void ClearCache()
        {
            Cache.Clear();
        }

        #endregion

        #region ClearMappings

        /// <summary>
        /// Clears a database object of all mappings
        /// </summary>
        /// <param name="database">Database object to clear</param>
        public static void ClearMappings(string database = "Default")
        {
            if (Databases.ContainsKey(database))
                Databases[database].Mappings.Clear();
        }

        #endregion

        #region Database

        /// <summary>
        /// Adds a database's info
        /// </summary>
        /// <param name="connectionString">Connection string to use for this database (if not specified, uses Name to get the connection string from the configuration manager)</param>
        /// <param name="name">Name to associate with the database (if not specified, it uses the first connection string it finds in the configuration manager)</param>
        /// <param name="dbType">Database type, based on ADO.Net provider name (will override if it pulls info from the configuration manager)</param>
        /// <param name="profile">Determines if the commands should be profiled (if empty, it will override with its best guess based on database type)</param>
        /// <param name="parameterPrefix">Parameter prefix</param>
        /// <param name="readable">Should this database be used to read data?</param>
        /// <param name="writable">Should this database be used to write data?</param>
        /// <returns>The database object specified</returns>
        public static Database Database(string connectionString, string name = "Default", string dbType = "System.Data.SqlClient",
                                        string parameterPrefix = "@", bool profile = false, bool readable = true, bool writable = true)
        {
            return Databases.AddOrUpdate(name,
                                        new Database(connectionString, name, dbType, parameterPrefix, profile, readable, writable),
                                        (x, y) => new Database(connectionString, name, dbType, parameterPrefix, profile, writable, readable));
        }

        #endregion

        #region Map

        /// <summary>
        /// Creates a mapping (or returns an already created one if it exists)
        /// </summary>
        /// <typeparam name="TClassType">Class type to map</typeparam>
        /// <param name="tableName">Table name</param>
        /// <param name="primaryKey">Primary key</param>
        /// <param name="autoIncrement">Auto incrementing primar key</param>
        /// <param name="database">Database to use</param>
        /// <returns>The created mapping (or an already created one if it exists)</returns>
        public static Mapping<TClassType> Map<TClassType>(string tableName, string primaryKey, bool autoIncrement = true, string database = "Default")
            where TClassType : class,new()
        {
            return (Mapping<TClassType>)Databases.GetOrAdd(database, new Database("", database))
                                                .Mappings
                                                .GetOrAdd(typeof(TClassType), new Mapping<TClassType>(tableName, primaryKey, autoIncrement));
        }

        #endregion

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Disposes of the objects
        /// </summary>
        /// <param name="managed">True to dispose of all resources, false only disposes of native resources</param>
        protected override void Dispose(bool managed)
        {
            Close();
            if (Reader != null)
            {
                Reader.Dispose();
                Reader = null;
            }
            if (ExecutableCommand != null)
            {
                if (ExecutableCommand.Transaction != null)
                {
                    ExecutableCommand.Transaction.Commit();
                    ExecutableCommand.Transaction.Dispose();
                    ExecutableCommand.Transaction = null;
                }
                ExecutableCommand.Dispose();
                ExecutableCommand = null;
            }
            if (Connection != null)
            {
                Connection.Dispose();
                Connection = null;
            }
        }

        #endregion
    }
}