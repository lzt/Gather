﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Data;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;
using Operate.SQL.DataClasses;
using Operate.SQL.DataClasses.Enums;
using Operate.SQL.DataClasses.Interfaces;
using Operate.SQL.MicroORM.Interfaces;
#endregion

namespace Operate.SQL.SQLServer
{
    /// <summary>
    /// Functions helpful for SQL Server
    /// </summary>
    public static class SQLServer
    {
        #region Public Static Functions

        /// <summary>
        /// Checks if a database exists
        /// </summary>
        /// <param name="database">Name of the database</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns>True if it exists, false otherwise</returns>
        public static bool DoesDatabaseExist(string database, string connectionString)
        {
            return CheckExists("SELECT * FROM Master.sys.Databases WHERE name=@Name", database, connectionString);
        }

        /// <summary>
        /// Checks if a table exists
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns>True if it exists, false otherwise</returns>
        public static bool DoesTableExist(string table, string connectionString)
        {
            return CheckExists("SELECT * FROM sys.Tables WHERE name=@Name", table, connectionString);
        }

        /// <summary>
        /// Checks if a view exists
        /// </summary>
        /// <param name="view">View name</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns>True if it exists, false otherwise</returns>
        public static bool DoesViewExist(string view, string connectionString)
        {
            return CheckExists("SELECT * FROM sys.views WHERE name=@Name", view, connectionString);
        }

        /// <summary>
        /// Checks if stored procedure exists
        /// </summary>
        /// <param name="storedProcedure">Stored procedure's name</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns>True if it exists, false otherwise</returns>
        public static bool DoesStoredProcedureExist(string storedProcedure, string connectionString)
        {
            return CheckExists("SELECT * FROM sys.Procedures WHERE name=@Name", storedProcedure, connectionString);
        }

        /// <summary>
        /// Checks if trigger exists
        /// </summary>
        /// <param name="trigger">Trigger's name</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns>True if it exists, false otherwise</returns>
        public static bool DoesTriggerExist(string trigger, string connectionString)
        {
            return CheckExists("SELECT * FROM sys.triggers WHERE name=@Name", trigger, connectionString);
        }

        /// <summary>
        /// Creates a database out of the structure it is given
        /// </summary>
        /// <param name="database">Database structure</param>
        /// <param name="connectionString">The connection string to the database's location</param>
        public static void CreateDatabase(Database database, string connectionString)
        {
            string command = BuildCommands(database);
            string[] splitter = { "\n" };
            string[] commands = command.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            connectionString = Regex.Replace(connectionString, "Pooling=(.*?;)", "", RegexOptions.IgnoreCase) + ";Pooling=false;";
            string databaseConnectionString = Regex.Replace(connectionString, "Initial Catalog=(.*?;)", "", RegexOptions.IgnoreCase);
            using (var helper = new SQLHelper(commands[0], CommandType.Text, databaseConnectionString))
            {
                helper.ExecuteNonQuery();
            }
            using (var helper = new SQLHelper("", CommandType.Text, connectionString))
            {
                IBatchCommand batcher = helper.Batch();
                for (int x = 1; x < commands.Length; ++x)
                {
                    if (commands[x].Contains("CREATE TRIGGER") || commands[x].Contains("CREATE FUNCTION"))
                    {
                        if (batcher.CommandCount > 0)
                        {
                            helper.ExecuteNonQuery();
                            batcher = helper.Batch();
                        }
                        batcher.AddCommand(commands[x], CommandType.Text);
                        if (x < commands.Length - 1)
                        {
                            helper.ExecuteNonQuery();
                            batcher = helper.Batch();
                        }
                    }
                    else
                    {
                        batcher.AddCommand(commands[x], CommandType.Text);
                    }
                }
                helper.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Updates a database (only adds new fields, tables, etc. does not delete old fields)
        /// </summary>
        /// <param name="desiredDatabase">The desired structure of the database</param>
        /// <param name="currentDatabase">The current database structure</param>
        /// <param name="connectionString">Connection string to the database</param>
        public static void UpdateDatabase(Database desiredDatabase, Database currentDatabase, string connectionString)
        {
            if (currentDatabase == null)
            {
                CreateDatabase(desiredDatabase, connectionString);
                return;
            }
            string command = BuildCommands(desiredDatabase, currentDatabase);
            string[] splitter = { "\n" };
            string[] commands = command.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            connectionString = Regex.Replace(connectionString, "Pooling=(.*?;)", "", RegexOptions.IgnoreCase) + ";Pooling=false;";
            using (var helper = new SQLHelper("", CommandType.Text, connectionString))
            {
                IBatchCommand batcher = helper.Batch();
                for (int x = 0; x < commands.Length; ++x)
                {
                    if (commands[x].Contains("CREATE TRIGGER") || commands[x].Contains("CREATE FUNCTION"))
                    {
                        if (batcher.CommandCount > 0)
                        {
                            helper.ExecuteNonQuery();
                            batcher = helper.Batch();
                        }
                        batcher.AddCommand(commands[x], CommandType.Text);
                        if (x < commands.Length - 1)
                        {
                            helper.ExecuteNonQuery();
                            batcher = helper.Batch();
                        }
                    }
                    else
                    {
                        batcher.AddCommand(commands[x], CommandType.Text);
                    }
                }
                helper.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Gets the structure of a database
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <returns>The database structure</returns>
        public static Database GetDatabaseStructure(string connectionString)
        {
            string databaseName = Regex.Match(connectionString, "Initial Catalog=(.*?;)").Value.Replace("Initial Catalog=", "").Replace(";", "");
            if (!DoesDatabaseExist(databaseName, connectionString))
                return null;
            var temp = new Database(databaseName);
            GetTables(connectionString, temp);
            SetupTables(connectionString, temp);
            SetupViews(connectionString, temp);
            SetupStoredProcedures(connectionString, temp);
            SetupFunctions(connectionString, temp);
            return temp;
        }

        #endregion

        #region Private Static Functions

        /// <summary>
        /// Builds a list of commands for a datatbase
        /// </summary>
        /// <param name="desiredDatabase">Desired database structure</param>
        /// <param name="currentDatabase">Current database structure</param>
        /// <returns>A list of commands</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private static string BuildCommands(Database desiredDatabase, Database currentDatabase)
        {
            var builder = new StringBuilder();

            foreach (DataClasses.Table table in desiredDatabase.Tables)
            {
                DataClasses.Table currentTable = currentDatabase[table.Name];
                builder.Append(currentTable == null ? GetTableCommand(table) : GetAlterTableCommand(table, currentTable));
            }
            foreach (DataClasses.Table table in desiredDatabase.Tables)
            {
                DataClasses.Table currentTable = currentDatabase[table.Name];
                if (currentTable == null)
                {
                    builder.Append(GetForeignKeyCommand(table));
                }
            }
            foreach (DataClasses.Table table in desiredDatabase.Tables)
            {
                DataClasses.Table currentTable = currentDatabase[table.Name];
                builder.Append(currentTable == null
                                   ? GetTriggerCommand(table)
                                   : GetAlterTriggerCommand(table, currentTable));
            }
            foreach (Function function in desiredDatabase.Functions)
            {
                bool found = false;
                foreach (Function currentFunction in currentDatabase.Functions)
                {
                    if (currentFunction.Name == function.Name)
                    {
                        builder.Append(GetAlterFunctionCommand(function, currentFunction));
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    builder.Append(GetFunctionCommand(function));
                }
            }
            foreach (View view in desiredDatabase.Views)
            {
                bool found = false;
                foreach (View currentView in currentDatabase.Views)
                {
                    if (currentView.Name == view.Name)
                    {
                        builder.Append(GetAlterViewCommand(view, currentView));
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    builder.Append(GetViewCommand(view));
                }
            }
            foreach (StoredProcedure storedProcedure in desiredDatabase.StoredProcedures)
            {
                bool found = false;
                foreach (StoredProcedure currentStoredProcedure in currentDatabase.StoredProcedures)
                {
                    if (storedProcedure.Name == currentStoredProcedure.Name)
                    {
                        builder.Append(GetAlterStoredProcedure(storedProcedure, currentStoredProcedure));
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    builder.Append(GetStoredProcedure(storedProcedure));
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets a list of alter commands for a stored procedure
        /// </summary>
        /// <param name="storedProcedure">Desired stored procedure</param>
        /// <param name="currentStoredProcedure">Current stored procedure</param>
        /// <returns>A list of commands in a string</returns>
        private static string GetAlterStoredProcedure(StoredProcedure storedProcedure, StoredProcedure currentStoredProcedure)
        {
            var builder = new StringBuilder();
            if (storedProcedure.Definition != currentStoredProcedure.Definition)
            {
                builder.Append("EXEC dbo.sp_executesql @statement = N'DROP PROCEDURE ").Append(storedProcedure.Name).Append("'\n");
                builder.Append(GetStoredProcedure(storedProcedure));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets a list of alter commands for a view
        /// </summary>
        /// <param name="view">Desired view structure</param>
        /// <param name="currentView">Current view structure</param>
        /// <returns>A list of commands in a string</returns>
        private static string GetAlterViewCommand(View view, View currentView)
        {
            var builder = new StringBuilder();
            if (view.Definition != currentView.Definition)
            {
                builder.Append("EXEC dbo.sp_executesql @statement = N'DROP VIEW ").Append(view.Name).Append("'\n");
                builder.Append(GetViewCommand(view));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets a list of alter commands for a function
        /// </summary>
        /// <param name="function">Desired function structure</param>
        /// <param name="currentFunction">Current function structure</param>
        /// <returns>A list of commands in a string</returns>
        private static string GetAlterFunctionCommand(Function function, Function currentFunction)
        {
            var builder = new StringBuilder();
            if (function.Definition != currentFunction.Definition)
            {
                builder.Append("EXEC dbo.sp_executesql @statement = N'DROP FUNCTION ").Append(function.Name).Append("'\n");
                builder.Append(GetFunctionCommand(function));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets a list of commands altering triggers
        /// </summary>
        /// <param name="table">Desired table containing the triggers</param>
        /// <param name="currentTable">Current table containing the current triggers</param>
        /// <returns>A string containing a list of commands</returns>
        private static string GetAlterTriggerCommand(DataClasses.Table table, DataClasses.Table currentTable)
        {
            var builder = new StringBuilder();
            foreach (Trigger trigger in table.Triggers)
            {
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (Trigger trigger2 in currentTable.Triggers)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    if (trigger.Name == trigger2.Name && trigger.Definition != trigger2.Definition)
                    {
                        builder.Append("EXEC dbo.sp_executesql @statement = N'DROP TRIGGER ").Append(trigger.Name).Append("'\n");
                        string definition = Regex.Replace(trigger.Definition, "-- (.*)", "");
                        builder.Append(definition.Replace("\n", " ").Replace("\r", " ") + "\n");
                        break;
                    }
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets alter commands for a table
        /// </summary>
        /// <param name="desiredTable">Desired table structure</param>
        /// <param name="currentTable">Current table structure</param>
        /// <returns>A string containing a list of commands</returns>
        private static string GetAlterTableCommand(DataClasses.Table desiredTable, DataClasses.Table currentTable)
        {
            var builder = new StringBuilder();
            foreach (IColumn column in desiredTable.Columns)
            {
                IColumn currentColumn = currentTable[column.Name];
                if (currentColumn == null)
                {
                    builder.Append("EXEC dbo.sp_executesql @statement = N'ALTER TABLE ").Append(desiredTable.Name)
                        .Append(" ADD ").Append(column.Name).Append(" ").Append(column.DataType.To(SqlDbType.Int).ToString());
                    if (column.DataType == SqlDbType.VarChar.To(DbType.Int32) || column.DataType == SqlDbType.NVarChar.To(DbType.Int32))
                    {
                        if (column.Length < 0 || column.Length >= 4000)
                        {
                            builder.Append("(MAX)");
                        }
                        else
                        {
                            builder.Append("(").Append(column.Length.ToString(CultureInfo.InvariantCulture)).Append(")");
                        }
                    }
                    builder.Append("'\n");
                    foreach (IColumn foreignKey in column.ForeignKey)
                    {
                        builder.Append("EXEC dbo.sp_executesql @statement = N'ALTER TABLE ").Append(desiredTable.Name)
                            .Append(" ADD FOREIGN KEY (").Append(column.Name).Append(") REFERENCES ")
                            .Append(foreignKey.ParentTable.Name).Append("(").Append(foreignKey.Name).Append(")");
                        if (column.OnDeleteCascade)
                            builder.Append(" ON DELETE CASCADE");
                        if (column.OnUpdateCascade)
                            builder.Append(" ON UPDATE CASCADE");
                        if (column.OnDeleteSetNull)
                            builder.Append(" ON DELETE SET NULL");
                        builder.Append("'\n");
                    }
                }
                else if (currentColumn.DataType != column.DataType
                    || (currentColumn.DataType == column.DataType
                        && currentColumn.DataType == SqlDbType.NVarChar.To(DbType.Int32)
                        && (currentColumn.Length != column.Length * 2 && currentColumn.Length != column.Length)
                        && ((currentColumn.Length < 4000 && currentColumn.Length >= 0)
                                && (column.Length < 4000 && column.Length >= 0))))
                {
                    builder.Append("EXEC dbo.sp_executesql @statement = N'ALTER TABLE ").Append(desiredTable.Name)
                        .Append(" ALTER COLUMN ").Append(column.Name).Append(" ").Append(column.DataType.To(SqlDbType.Int).ToString());
                    if (column.DataType == SqlDbType.VarChar.To(DbType.Int32) || column.DataType == SqlDbType.NVarChar.To(DbType.Int32))
                    {
                        if (column.Length < 0 || column.Length >= 4000)
                        {
                            builder.Append("(MAX)");
                        }
                        else
                        {
                            builder.Append("(").Append(column.Length.ToString(CultureInfo.InvariantCulture)).Append(")");
                        }
                    }
                    builder.Append("'\n");
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Builds the list of commands to build the database
        /// </summary>
        /// <param name="database">Database object</param>
        /// <returns>The commands needed to  build the database</returns>
        private static string BuildCommands(Database database)
        {
            var builder = new StringBuilder();
            builder.Append("EXEC dbo.sp_executesql @statement = N'CREATE DATABASE ").Append(database.Name).Append("'\n");
            foreach (DataClasses.Table table in database.Tables)
            {
                builder.Append(GetTableCommand(table));
            }
            foreach (DataClasses.Table table in database.Tables)
            {
                builder.Append(GetForeignKeyCommand(table));
            }
            foreach (DataClasses.Table table in database.Tables)
            {
                builder.Append(GetTriggerCommand(table));
            }
            foreach (Function function in database.Functions)
            {
                builder.Append(GetFunctionCommand(function));
            }
            foreach (View view in database.Views)
            {
                builder.Append(GetViewCommand(view));
            }
            foreach (StoredProcedure storedProcedure in database.StoredProcedures)
            {
                builder.Append(GetStoredProcedure(storedProcedure));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets the list of triggers associated with the table
        /// </summary>
        /// <param name="table">Table object</param>
        /// <returns>The string containing the various creation commands</returns>
        private static string GetTriggerCommand(DataClasses.Table table)
        {
            var builder = new StringBuilder();
            foreach (Trigger trigger in table.Triggers)
            {
                string definition = Regex.Replace(trigger.Definition, "-- (.*)", "");
                builder.Append(definition.Replace("\n", " ").Replace("\r", " ") + "\n");
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets the foreign keys creation command
        /// </summary>
        /// <param name="table">Table object</param>
        /// <returns>The string creating the foreign keys</returns>
        private static string GetForeignKeyCommand(DataClasses.Table table)
        {
            var builder = new StringBuilder();
            foreach (IColumn column in table.Columns)
            {
                if (column.ForeignKey.Count > 0)
                {
                    foreach (IColumn foreignKey in column.ForeignKey)
                    {
                        builder.Append("EXEC dbo.sp_executesql @statement = N'ALTER TABLE ");
                        builder.Append(column.ParentTable.Name).Append(" ADD FOREIGN KEY (");
                        builder.Append(column.Name).Append(") REFERENCES ").Append(foreignKey.ParentTable.Name);
                        builder.Append("(").Append(foreignKey.Name).Append(")");
                        if (column.OnDeleteCascade)
                            builder.Append(" ON DELETE CASCADE");
                        if (column.OnUpdateCascade)
                            builder.Append(" ON UPDATE CASCADE");
                        if (column.OnDeleteSetNull)
                            builder.Append(" ON DELETE SET NULL");
                        builder.Append("'\n");
                    }
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Gets the stored procedure creation command
        /// </summary>
        /// <param name="storedProcedure">The stored procedure object</param>
        /// <returns>The string creating the stored procedure</returns>
        private static string GetStoredProcedure(StoredProcedure storedProcedure)
        {
            string definition = Regex.Replace(storedProcedure.Definition, "-- (.*)", "");
            return definition.Replace("\n", " ").Replace("\r", " ") + "\n";
        }

        /// <summary>
        /// Gets the view creation command
        /// </summary>
        /// <param name="view">The view object</param>
        /// <returns>The string creating the view</returns>
        private static string GetViewCommand(View view)
        {
            string definition = Regex.Replace(view.Definition, "-- (.*)", "");
            return definition.Replace("\n", " ").Replace("\r", " ") + "\n";
        }

        /// <summary>
        /// Gets the function command
        /// </summary>
        /// <param name="function">The function object</param>
        /// <returns>The string creating the function</returns>
        private static string GetFunctionCommand(Function function)
        {
            string definition = Regex.Replace(function.Definition, "-- (.*)", "");
            return definition.Replace("\n", " ").Replace("\r", " ") + "\n";
        }

        /// <summary>
        /// Gets the table creation commands
        /// </summary>
        /// <param name="table">Table object</param>
        /// <returns>The string containing the creation commands</returns>
        private static string GetTableCommand(DataClasses.Table table)
        {
            var builder = new StringBuilder();
            builder.Append("EXEC dbo.sp_executesql @statement = N'CREATE TABLE ").Append(table.Name).Append("(");
            string splitter = "";
            foreach (IColumn column in table.Columns)
            {
                builder.Append(splitter).Append(column.Name).Append(" ").Append(column.DataType.To(SqlDbType.Int).ToString());
                if (column.DataType == SqlDbType.VarChar.To(DbType.Int32) || column.DataType == SqlDbType.NVarChar.To(DbType.Int32))
                {
                    if (column.Length < 0 || column.Length >= 4000)
                    {
                        builder.Append("(MAX)");
                    }
                    else
                    {
                        builder.Append("(").Append(column.Length.ToString(CultureInfo.InvariantCulture)).Append(")");
                    }
                }
                if (!column.Nullable)
                {
                    builder.Append(" NOT NULL");
                }
                if (column.Unique)
                {
                    builder.Append(" UNIQUE");
                }
                if (column.PrimaryKey)
                {
                    builder.Append(" PRIMARY KEY");
                }
                if (!string.IsNullOrEmpty(column.Default))
                {
                    builder.Append(" DEFAULT ").Append(column.Default.Replace("(", "").Replace(")", "").Replace("'", "''"));
                }
                if (column.AutoIncrement)
                {
                    builder.Append(" IDENTITY");
                }
                splitter = ",";
            }
            builder.Append(")'\n");
            int counter = 0;
            foreach (IColumn column in table.Columns)
            {
                if (column.Index && column.Unique)
                {
                    builder.Append("EXEC dbo.sp_executesql @statement = N'CREATE UNIQUE INDEX ");
                    builder.Append("Index_").Append(column.Name).Append(counter.ToString(CultureInfo.InvariantCulture)).Append(" ON ");
                    builder.Append(column.ParentTable.Name).Append("(").Append(column.Name).Append(")");
                    builder.Append("'\n");
                }
                else if (column.Index)
                {
                    builder.Append("EXEC dbo.sp_executesql @statement = N'CREATE INDEX ");
                    builder.Append("Index_").Append(column.Name).Append(counter.ToString(CultureInfo.InvariantCulture)).Append(" ON ");
                    builder.Append(column.ParentTable.Name).Append("(").Append(column.Name).Append(")");
                    builder.Append("'\n");
                }
                ++counter;
            }
            return builder.ToString();
        }

        /// <summary>
        /// Sets up the functions
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="temp">Database object</param>
        private static void SetupFunctions(string connectionString, Database temp)
        {
            const string command = "SELECT SPECIFIC_NAME as NAME,ROUTINE_DEFINITION as DEFINITION FROM INFORMATION_SCHEMA.ROUTINES WHERE INFORMATION_SCHEMA.ROUTINES.ROUTINE_TYPE='FUNCTION'";
            using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
            {
                helper.ExecuteReader();
                while (helper.Read())
                {
                    var name = helper.GetParameter("NAME", "");
                    var definition = helper.GetParameter("DEFINITION", "");
                    temp.AddFunction(name, definition);
                }
            }
        }

        /// <summary>
        /// Sets up stored procedures
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="temp">Database object</param>
        private static void SetupStoredProcedures(string connectionString, Database temp)
        {
            string command = "SELECT sys.procedures.name as NAME,OBJECT_DEFINITION(sys.procedures.object_id) as DEFINITION FROM sys.procedures";
            using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
            {
                helper.ExecuteReader();
                while (helper.Read())
                {
                    string procedureName = helper.GetParameter("NAME", "");
                    string definition = helper.GetParameter("DEFINITION", "");
                    temp.AddStoredProcedure(procedureName, definition);
                }
            }
            foreach (StoredProcedure procedure in temp.StoredProcedures)
            {
                command = "SELECT sys.systypes.name as TYPE,sys.parameters.name as NAME,sys.parameters.max_length as LENGTH,sys.parameters.default_value as [DEFAULT VALUE] FROM sys.procedures INNER JOIN sys.parameters on sys.procedures.object_id=sys.parameters.object_id INNER JOIN sys.systypes on sys.systypes.xusertype=sys.parameters.system_type_id WHERE sys.procedures.name=@ProcedureName AND (sys.systypes.xusertype <> 256)";
                using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
                {
                    helper.AddParameter("@ProcedureName", procedure.Name)
                          .ExecuteReader();
                    while (helper.Read())
                    {
                        string type = helper.GetParameter("TYPE", "");
                        string name = helper.GetParameter("NAME", "");
                        int length = helper.GetParameter("LENGTH", 0);
                        if (type == "nvarchar")
                            length /= 2;
                        string Default = helper.GetParameter("DEFAULT VALUE", "");
                        procedure.AddColumn(name, type.To<string, SqlDbType>().To(DbType.Int32), length, Default);
                    }
                }
            }
        }

        /// <summary>
        /// Sets up the views
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="temp">Database object</param>
        private static void SetupViews(string connectionString, Database temp)
        {
            foreach (View view in temp.Views)
            {
                string command = "SELECT OBJECT_DEFINITION(sys.views.object_id) as Definition FROM sys.views WHERE sys.views.name=@ViewName";
                using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
                {
                    helper.AddParameter("@ViewName", view.Name)
                    .ExecuteReader();
                    if (helper.Read())
                    {
                        view.Definition = helper.GetParameter("Definition", "");
                    }
                }
                command = "SELECT sys.columns.name AS [Column], sys.systypes.name AS [COLUMN TYPE], sys.columns.max_length as [MAX LENGTH], sys.columns.is_nullable as [IS NULLABLE] FROM sys.views INNER JOIN sys.columns on sys.columns.object_id=sys.views.object_id INNER JOIN sys.systypes ON sys.systypes.xtype = sys.columns.system_type_id WHERE (sys.views.name = @ViewName) AND (sys.systypes.xusertype <> 256)";
                using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
                {
                    helper.AddParameter("@ViewName", view.Name)
                          .ExecuteReader();
                    while (helper.Read())
                    {
                        string columnName = helper.GetParameter("Column", "");
                        string columnType = helper.GetParameter("COLUMN TYPE", "");
                        int maxLength = helper.GetParameter("MAX LENGTH", 0);
                        if (columnType == "nvarchar")
                            maxLength /= 2;
                        bool nullable = helper.GetParameter("IS NULLABLE", false);
                        view.AddColumn<string>(columnName, columnType.To<string, SqlDbType>().To(DbType.Int32), maxLength, nullable);
                    }
                }
            }
        }

        /// <summary>
        /// Sets up the tables (pulls columns, etc.)
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="temp">Database object</param>
        private static void SetupTables(string connectionString, Database temp)
        {
            foreach (DataClasses.Table table in temp.Tables)
            {
                string command = "SELECT sys.columns.name AS [Column], sys.systypes.name AS [COLUMN TYPE], sys.columns.max_length as [MAX LENGTH], sys.columns.is_nullable as [IS NULLABLE], sys.columns.is_identity as [IS IDENTITY], sys.index_columns.index_id as [IS INDEX], key_constraints.name as [PRIMARY KEY], key_constraints_1.name as [UNIQUE], tables_1.name as [FOREIGN KEY TABLE], columns_1.name as [FOREIGN KEY COLUMN], sys.default_constraints.definition as [DEFAULT VALUE] FROM sys.tables INNER JOIN sys.columns on sys.columns.object_id=sys.tables.object_id INNER JOIN sys.systypes ON sys.systypes.xtype = sys.columns.system_type_id LEFT OUTER JOIN sys.index_columns on sys.index_columns.object_id=sys.tables.object_id and sys.index_columns.column_id=sys.columns.column_id LEFT OUTER JOIN sys.key_constraints on sys.key_constraints.parent_object_id=sys.tables.object_id and sys.key_constraints.parent_object_id=sys.index_columns.object_id and sys.index_columns.index_id=sys.key_constraints.unique_index_id and sys.key_constraints.type='PK' LEFT OUTER JOIN sys.foreign_key_columns on sys.foreign_key_columns.parent_object_id=sys.tables.object_id and sys.foreign_key_columns.parent_column_id=sys.columns.column_id LEFT OUTER JOIN sys.tables as tables_1 on tables_1.object_id=sys.foreign_key_columns.referenced_object_id LEFT OUTER JOIN sys.columns as columns_1 on columns_1.column_id=sys.foreign_key_columns.referenced_column_id and columns_1.object_id=tables_1.object_id LEFT OUTER JOIN sys.key_constraints as key_constraints_1 on key_constraints_1.parent_object_id=sys.tables.object_id and key_constraints_1.parent_object_id=sys.index_columns.object_id and sys.index_columns.index_id=key_constraints_1.unique_index_id and key_constraints_1.type='UQ' LEFT OUTER JOIN sys.default_constraints on sys.default_constraints.object_id=sys.columns.default_object_id WHERE (sys.tables.name = @TableName) AND (sys.systypes.xusertype <> 256)";
                using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
                {
                    helper.AddParameter("@TableName", table.Name)
                    .ExecuteReader();
                    while (helper.Read())
                    {
                        string columnName = helper.GetParameter("Column", "");
                        string columnType = helper.GetParameter("COLUMN TYPE", "");
                        int maxLength = helper.GetParameter("MAX LENGTH", 0);
                        if (columnType == "nvarchar")
                            maxLength /= 2;
                        bool nullable = helper.GetParameter("IS NULLABLE", false);
                        bool identity = helper.GetParameter("IS IDENTITY", false);
                        bool index = helper.GetParameter("IS INDEX", 0) != 0;
                        bool primaryKey = !string.IsNullOrEmpty(helper.GetParameter("PRIMARY KEY", ""));
                        bool unique = !string.IsNullOrEmpty(helper.GetParameter("UNIQUE", ""));
                        string foreignKeyTable = helper.GetParameter("FOREIGN KEY TABLE", "");
                        string foreignKeyColumn = helper.GetParameter("FOREIGN KEY COLUMN", "");
                        string defaultValue = helper.GetParameter("DEFAULT VALUE", "");
                        if (table.ContainsColumn(columnName))
                        {
                            table.AddForeignKey(columnName, foreignKeyTable, foreignKeyColumn);
                        }
                        else
                        {
                            table.AddColumn(columnName, columnType.To<string, SqlDbType>().To(DbType.Int32), maxLength, nullable, identity, index, primaryKey, unique, foreignKeyTable, foreignKeyColumn, defaultValue);
                        }
                    }
                }
                command = "SELECT sys.triggers.name as Name,sys.trigger_events.type as Type,OBJECT_DEFINITION(sys.triggers.object_id) as Definition FROM sys.triggers INNER JOIN sys.trigger_events ON sys.triggers.object_id=sys.trigger_events.object_id INNER JOIN sys.tables on sys.triggers.parent_id=sys.tables.object_id where sys.tables.name=@TableName";
                using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
                {
                    helper.AddParameter("@TableName", table.Name)
                        .ExecuteReader();
                    while (helper.Read())
                    {
                        string name = helper.GetParameter("Name", "");
                        int type = helper.GetParameter("Type", 0);
                        string definition = helper.GetParameter("Definition", "");
                        table.AddTrigger(name, definition, type.ToString(CultureInfo.InvariantCulture).To<string, TriggerType>());
                    }
                }
            }
            foreach (DataClasses.Table table in temp.Tables)
            {
                table.SetupForeignKeys();
            }
        }

        /// <summary>
        /// Gets the tables for a database
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="temp">The database object</param>
        private static void GetTables(string connectionString, Database temp)
        {
            const string command = "SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES";
            using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
            {
                helper.ExecuteReader();
                while (helper.Read())
                {
                    string tableName = helper.GetParameter("TABLE_NAME", "");
                    string tableType = helper.GetParameter("TABLE_TYPE", "");
                    if (tableType == "BASE TABLE")
                    {
                        temp.AddTable(tableName);
                    }
                    else if (tableType == "VIEW")
                    {
                        temp.AddView(tableName);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if something exists
        /// </summary>
        /// <param name="command">Command to run</param>
        /// <param name="name">Name of the item</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns>True if it exists, false otherwise</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static bool CheckExists(string command, string name, string connectionString)
        {
            using (var helper = new SQLHelper(command, CommandType.Text, connectionString))
            {
                try
                {
                    return helper.AddParameter("@Name", name)
                        .ExecuteReader()
                        .Read();
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch { }
                // ReSharper restore EmptyGeneralCatchClause
            }
            return false;
        }

        #endregion
    }
}