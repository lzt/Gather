﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Operate.ExtensionMethods;
using Operate.Patterns;
using Operate.SQL.MicroORM;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL
{
    /// <summary>
    /// SQL command builder
    /// </summary>
    public class SQLCommand : IFluentInterface
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="columns">Columns</param>
        /// <param name="table">Table name</param>
        protected SQLCommand(string commandType, string table, params string[] columns)
        {
            CommandType = commandType;
            Columns = new List<string>();
            if (columns == null || columns.Length == 0)
                Columns.Add("*");
            else
                Columns.Add(columns);
            Joins = new List<SQLJoin>();
            Parameters = new List<IParameter>();
            OrderByColumns = new List<string>();
            IsDistinct = false;
            TopNumber = -1;
            Table = table;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Command type (SELECT, DELETE, UPDATE, INSERT)
        /// </summary>
        protected string CommandType { get; set; }

        /// <summary>
        /// Columns
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string> Columns { get; private set; }

        /// <summary>
        /// Joins
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<SQLJoin> Joins { get; private set; }

        /// <summary>
        /// Base table name
        /// </summary>
        protected string Table { get; set; }

        /// <summary>
        /// Parameters
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<IParameter> Parameters { get; private set; }

        /// <summary>
        /// Where clause
        /// </summary>
        protected string WhereClause { get; set; }

        /// <summary>
        /// Order by
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string> OrderByColumns { get; private set; }

        /// <summary>
        /// Limits the results to the top X amount
        /// </summary>
        protected int TopNumber { get; set; }

        /// <summary>
        /// Determines if the items should be distinct
        /// </summary>
        protected bool IsDistinct { get; set; }

        #endregion

        #region Static Functions

        #region Select

        /// <summary>
        /// Creates a select command
        /// </summary>
        /// <param name="columns">Columns to return</param>
        /// <param name="table">Base table name</param>
        /// <returns>An SQLCommand object</returns>
        public static SQLCommand Select(string table, params string[] columns)
        {
            return new SQLCommand("SELECT", table, columns);
        }

        #endregion

        #endregion

        #region Functions

        #region Build

        /// <summary>
        /// Builds a command
        /// </summary>
        /// <returns>The resulting command</returns>
        public Command Build()
        {
            string command = (TopNumber > 0 ? "SELECT TOP " + TopNumber : "SELECT") + (IsDistinct ? " DISTINCT" : "") + " {0} FROM {1} {2} {3} {4}";
            string where = "";
            string orderBy = "";
            if (!string.IsNullOrEmpty(WhereClause))
                where += WhereClause.Trim().ToUpperInvariant().StartsWith("WHERE", StringComparison.CurrentCulture) ? WhereClause.Trim() : "WHERE " + WhereClause.Trim();
            else if (Parameters != null && Parameters.Count > 0)
            {
                where += "WHERE ";
                where += Parameters[0];
                for (int x = 1; x < Parameters.Count; ++x)
                {
                    where += " AND " + Parameters[x];
                }
            }
            if (OrderByColumns != null && OrderByColumns.Count > 0)
            {
                orderBy += " ORDER BY " + OrderByColumns[0];
                for (int x = 1; x < OrderByColumns.Count; ++x)
                {
                    orderBy += "," + OrderByColumns[1];
                }
            }
            if (Parameters != null)
                return new Command(string.Format(CultureInfo.InvariantCulture, command, Columns.ToString(x => x), Table, Joins.ToString(x => x.ToString(), " "), @where, orderBy).Trim(), System.Data.CommandType.Text, Parameters.ToArray());
            return null;
        }

        #endregion

        #region Distinct

        /// <summary>
        /// Sets the command to return distinct rows
        /// </summary>
        /// <returns>this</returns>
        public SQLCommand Distinct()
        {
            IsDistinct = true;
            return this;
        }

        #endregion

        #region Join

        /// <summary>
        /// Joins another table to the command
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="type">Type of join</param>
        /// <param name="onClause">ON Clause</param>
        /// <returns>this</returns>
        public SQLCommand Join(string table, string type, string onClause)
        {
            Joins.Add(new SQLJoin(table, type, onClause));
            return this;
        }

        #endregion

        #region OrderBy

        /// <summary>
        /// Sets up the order by clause
        /// </summary>
        /// <param name="columns">Columns to order by</param>
        /// <returns>this</returns>
        public SQLCommand OrderBy(params string[] columns)
        {
            OrderByColumns.Add(columns);
            return this;
        }

        #endregion

        #region Top

        /// <summary>
        /// Limits the number of items to the top X items
        /// </summary>
        /// <param name="amount">The number of items to limit it to</param>
        /// <returns>this</returns>
        public SQLCommand Top(int amount)
        {
            TopNumber = amount;
            return this;
        }

        #endregion

        #region Where

        /// <summary>
        /// Generates the where clause based on the parameters passed in
        /// </summary>
        /// <param name="parameters">Parameters to use</param>
        /// <returns>this</returns>
        public SQLCommand Where(params IParameter[] parameters)
        {
            WhereClause = "";
            Parameters.Add(parameters);
            return this;
        }

        /// <summary>
        /// Sets the where clause
        /// </summary>
        /// <param name="whereClause">Where clause</param>
        /// <param name="parameters">Parameters to use</param>
        /// <returns>this</returns>
        public SQLCommand Where(string whereClause, params IParameter[] parameters)
        {
            WhereClause = whereClause;
            Parameters.Add(parameters);
            return this;
        }

        /// <summary>
        /// Sets the where clause
        /// </summary>
        /// <param name="whereClause">Where clause</param>
        /// <param name="parameterStarter">Parameter starter</param>
        /// <param name="parameters">Parameters to use</param>
        /// <returns>this</returns>
        public SQLCommand Where(string whereClause, string parameterStarter, params object[] parameters)
        {
            WhereClause = whereClause;
            foreach (object parameter in parameters)
            {
                var tempParameter = parameter as string;
                if (parameter == null)
                    Parameters.Add(new Parameter<object>(Parameters.Count.ToString(CultureInfo.InvariantCulture), default(DbType), null, ParameterDirection.Input, parameterStarter));
                else if (tempParameter != null)
                    Parameters.Add(new StringParameter(Parameters.Count.ToString(CultureInfo.InvariantCulture), tempParameter, ParameterDirection.Input, parameterStarter));
                else
                    Parameters.Add(new Parameter<object>(Parameters.Count.ToString(CultureInfo.InvariantCulture), parameter, ParameterDirection.Input, parameterStarter));
            }
            return this;
        }

        #endregion

        #endregion

        #region Classes

        #region SQLJoin

        /// <summary>
        /// Handles join information
        /// </summary>
        protected class SQLJoin
        {
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="table">Table name</param>
            /// <param name="type">Type of join</param>
            /// <param name="onClause">ON Clause</param>
            public SQLJoin(string table, string type, string onClause)
            {
                Table = table;
                Type = type;
                ONClause = onClause;
            }

            /// <summary>
            /// Table name
            /// </summary>
            public string Table { get; set; }

            /// <summary>
            /// Join type
            /// </summary>
            public string Type { get; set; }

            /// <summary>
            /// ON Clause
            /// </summary>
            public string ONClause { get; set; }

            /// <summary>
            /// returns the join as a string
            /// </summary>
            /// <returns>The join as a string</returns>
            public override string ToString()
            {
                return Type + " " + Table + (ONClause.Trim().ToUpperInvariant().StartsWith("ON", StringComparison.CurrentCulture) ? " " + ONClause : " ON " + ONClause);
            }
        }

        #endregion

        #endregion
    }
}
