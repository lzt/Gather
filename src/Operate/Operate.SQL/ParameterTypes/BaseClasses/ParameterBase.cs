﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Data;
using System.Data.Common;
using Operate.Comparison;
using Operate.ExtensionMethods;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL.ParameterTypes.BaseClasses
{
    /// <summary>
    /// Parameter base class
    /// </summary>
    /// <typeparam name="TDataType">Data type of the parameter</typeparam>
    public abstract class ParameterBase<TDataType> : IParameter
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">ID of the parameter</param>
        /// <param name="value">Value of the parameter</param>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">What the database expects as the
        /// parameter starting string ("@" for SQL Server, ":" for Oracle, etc.)</param>
        protected ParameterBase(string id, TDataType value, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            ID = id;
            Value = value;
            Direction = direction;
            DatabaseType = value == null ? typeof(TDataType).To(DbType.Int32) : value.GetType().To(DbType.Int32);
            BatchID = id;
            ParameterStarter = parameterStarter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">ID of the parameter</param>
        /// <param name="type">Database type</param>
        /// <param name="value">Value of the parameter</param>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">What the database expects as the
        /// parameter starting string ("@" for SQL Server, ":" for Oracle, etc.)</param>
        protected ParameterBase(string id, SqlDbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            ID = id;
            Value = (TDataType)value;
            DatabaseType = type.To(DbType.Int32);
            Direction = direction;
            BatchID = id;
            ParameterStarter = parameterStarter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">ID of the parameter</param>
        /// <param name="type">Database type</param>
        /// <param name="value">Value of the parameter</param>
        /// <param name="direction">Direction of the parameter</param>
        /// <param name="parameterStarter">What the database expects as the
        /// parameter starting string ("@" for SQL Server, ":" for Oracle, etc.)</param>
        protected ParameterBase(string id, DbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, string parameterStarter = "@")
        {
            ID = id;
            Value = (TDataType)value;
            DatabaseType = type;
            Direction = direction;
            BatchID = id;
            ParameterStarter = parameterStarter;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The Name that the parameter goes by
        /// </summary>
        public virtual string ID { get; set; }

        /// <summary>
        /// Batch ID
        /// </summary>
        protected virtual string BatchID { get; set; }

        /// <summary>
        /// Parameter value
        /// </summary>
        public virtual TDataType Value { get; set; }

        /// <summary>
        /// Direction of the parameter
        /// </summary>
        public virtual ParameterDirection Direction { get; set; }

        /// <summary>
        /// Database type
        /// </summary>
        public virtual DbType DatabaseType { get; set; }

        /// <summary>
        /// Starting string of the parameter
        /// </summary>
        public string ParameterStarter { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Adds this parameter to the SQLHelper
        /// </summary>
        /// <param name="helper">SQLHelper</param>
        public abstract void AddParameter(DbCommand helper);

        /// <summary>
        /// Creates a copy of the parameter
        /// </summary>
        /// <param name="suffix">Suffix to add to the parameter (for batching purposes)</param>
        /// <returns>A copy of the parameter</returns>
        public abstract IParameter CreateCopy(string suffix);

        /// <summary>
        /// Determines if the objects are equal
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public override bool Equals(object obj)
        {
            var otherParameter = obj as ParameterBase<TDataType>;
            return (otherParameter != null
                && otherParameter.DatabaseType == DatabaseType
                && otherParameter.Direction == Direction
                && otherParameter.ID == ID
                && new GenericEqualityComparer<TDataType>().Equals(otherParameter.Value, Value));
        }

        /// <summary>
        /// Gets the hash code for the object
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return (ID.GetHashCode() * 23 + Value.GetHashCode()) * 23 + DatabaseType.GetHashCode();
        }

        /// <summary>
        /// Returns the string version of the parameter
        /// </summary>
        /// <returns>The string representation of the parameter</returns>
        public override string ToString()
        {
            return ParameterStarter + ID;
        }

        /// <summary>
        /// The == operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>true if the first and second item are the same, false otherwise</returns>
        public static bool operator ==(ParameterBase<TDataType> first, ParameterBase<TDataType> second)
        {
            if (ReferenceEquals(first, second))
                return true;

            if ((object)first == null || (object)second == null)
                return false;

            return first.GetHashCode() == second.GetHashCode();
        }

        /// <summary>
        /// != operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>returns true if they are not equal, false otherwise</returns>
        public static bool operator !=(ParameterBase<TDataType> first, ParameterBase<TDataType> second)
        {
            return !(first == second);
        }

        #endregion
    }
}