﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Data.Common;
using Operate.SQL.ExtensionMethods;
using Operate.SQL.ParameterTypes.BaseClasses;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.SQL.ParameterTypes
{
    /// <summary>
    /// Parameter class that checks for equality
    /// </summary>
    /// <typeparam name="TDataType">Type of the parameter</typeparam>
    public class EqualParameter<TDataType> : ParameterBase<TDataType>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">Value of the parameter</param>
        /// <param name="id">Name of the parameter</param>
        /// <param name="parameterStarter">What the database expects as the
        /// parameter starting string ("@" for SQL Server, ":" for Oracle, etc.)</param>
        public EqualParameter(TDataType value, string id, string parameterStarter = "@")
            : base(id, value, System.Data.ParameterDirection.Input, parameterStarter)
        {
            Value = value;
            ID = id;
            ParameterStarter = parameterStarter;
        }

        #endregion

        #region Functions

        /// <summary>
        /// Adds the parameter to the SQLHelper
        /// </summary>
        /// <param name="helper">SQLHelper to add the parameter to</param>
        public override void AddParameter(DbCommand helper)
        {
            helper.AddParameter(ID, Value);
        }

        /// <summary>
        /// Creates a copy of the parameter
        /// </summary>
        /// <param name="suffix">Suffix to add to the parameter (for batching purposes)</param>
        /// <returns>A copy of the parameter</returns>
        public override IParameter CreateCopy(string suffix)
        {
            return new EqualParameter<TDataType>(Value, ID + suffix, ParameterStarter);
        }

        /// <summary>
        /// Outputs the param as a string
        /// </summary>
        /// <returns>The param as a string</returns>
        public override string ToString() { return ID + "=" + ParameterStarter + ID; }

        #endregion
    }
}