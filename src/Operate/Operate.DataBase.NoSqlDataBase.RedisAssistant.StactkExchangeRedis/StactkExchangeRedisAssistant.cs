﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Operate.DataBase.NoSqlDataBase.RedisAssistant.StactkExchangeRedis
{
    /// <summary>
    /// StactkExchangeRedisAssistant
    /// </summary>
    public sealed class StactkExchangeRedisAssistant : IRedisAssistant
    {
        #region Fields

        private readonly ConnectionMultiplexer _client;
        private readonly IDatabase _cache;

        #endregion

        #region Properties

        /// <summary>
        /// CacheType
        /// </summary>
        public CacheType CacheType { get; }

        #endregion

        #region Constructor

        #region 单例

        //静态SearchCache
        /// <summary>
        /// Instance
        /// </summary>
        public static StactkExchangeRedisAssistant Instance
        {
            get
            {
                // ReSharper disable once ConvertPropertyToExpressionBody
                return Nested.StactkExchangeRedisAssistant;//返回Nested类中的静态成员instance
            }
        }

        internal class Nested
        {
            static Nested()
            {
            }
            //将instance设为一个初始化的BaseCacheStrategy新实例
            internal static readonly StactkExchangeRedisAssistant StactkExchangeRedisAssistant = new StactkExchangeRedisAssistant();
        }

        #endregion

        /// <summary>
        /// RedisAssistant
        /// </summary>
        public StactkExchangeRedisAssistant()
        {
            _client = RedisManager.Manager;
            _cache = _client.GetDatabase();

            var testKey = Guid.NewGuid().ToString();
            var testValue = Guid.NewGuid().ToString();
            _cache.StringSet(testKey, testValue);
            var storeValue = _cache.StringGet(testKey);
            if (storeValue != testValue)
            {
                throw new Exception("Redis失效，没有计入缓存！");
            }
            _cache.StringSet(testKey, (string)null);

            CacheType = CacheType.Redis;
        }

        #endregion

        #region Methods

        public List<string> GetAllItemsFromList(string listId)
        {
            throw new NotImplementedException();
        }

        public void AddItemToList(string listId, string value)
        {
            throw new NotImplementedException();
        }

        public void AddRangeToList(string listId, List<string> values)
        {
            throw new NotImplementedException();
        }

        public long RemoveItemFromList(string listId, string value)
        {
            throw new NotImplementedException();
        }

        public long RemoveItemFromList(string listId, string value, int noOfMatches)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllFromList(string listId)
        {
            throw new NotImplementedException();
        }

        private IServer GetServer()
        {
            //https://github.com/StackExchange/StackExchange.Redis/blob/master/Docs/KeysScan.md
            var server = _client.GetServer(_client.GetEndPoints()[0]);
            return server;
        }

        /// <summary>
        /// GetCount
        /// </summary>
        /// <returns></returns>
        public long GetCount()
        {
            var count = GetServer().Keys().Count();
            return count;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            var v = (string)_cache.StringGet(key);
            if (v!=null)
            {
                return JsonConvert.DeserializeObject<T>(v);
            }

            return default(T);
        }

        /// <summary>
        /// Set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Set<T>(string key, T value)
        {
            var json = JsonConvert.SerializeObject(value);
            return _cache.StringSet(key, json);
        }

        /// <summary>
        /// Set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiresAt"></param>
        /// <returns></returns>
        public bool Set<T>(string key, T value, DateTime expiresAt)
        {
            var timespan = new TimeSpan(expiresAt.Ticks);
            return Set(key, value, timespan);
        }

        public bool Set<T>(string key, T value, TimeSpan expiresIn)
        {
            var json = JsonConvert.SerializeObject(value);
            return _cache.StringSet(key, json, expiresIn);
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void Remove(List<string> ketList)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
