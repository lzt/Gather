﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Linq;

#endregion

namespace Operate.Math.ExtensionMethods
{
    /// <summary>
    /// Permutation extensions
    /// </summary>
    public static class PermutationExtensions
    {
        #region Functions

        #region Permute

        /// <summary>
        /// Finds all permutations of the items within the list
        /// </summary>
        /// <typeparam name="T">Object type in the list</typeparam>
        /// <param name="input">Input list</param>
        /// <returns>The list of permutations</returns>
        public static ListMapping<int, T> Permute<T>(this IEnumerable<T> input)
        {
            if (input.IsNull()) { throw new ArgumentNullException("input"); }
            var current = new List<T>();
            var collection = input as T[] ?? input.ToArray();
            current.AddRange(collection);
            var returnValue = new ListMapping<int, T>();
            int max = (collection.Count() - 1).Factorial();
            int currentValue = 0;
            for (int x = 0; x < collection.Count(); ++x)
            {
                int z = 0;
                while (z < max)
                {
                    int y = collection.Count() - 1;
                    while (y > 1)
                    {
                        T tempHolder = current[y - 1];
                        current[y - 1] = current[y];
                        current[y] = tempHolder;
                        --y;
                        foreach (T item in current)
                            returnValue.Add(currentValue, item);
                        ++z;
                        ++currentValue;
                        if (z == max)
                            break;
                    }
                }
                if (x + 1 != collection.Count())
                {
                    current.Clear();
                    current.AddRange(collection);
                    T tempHolder2 = current[0];
                    current[0] = current[x + 1];
                    current[x + 1] = tempHolder2;
                }
            }
            return returnValue;
        }

        #endregion

        #endregion
    }
}
