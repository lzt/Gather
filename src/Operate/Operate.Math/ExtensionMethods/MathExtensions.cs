﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;

#endregion


namespace Operate.Math.ExtensionMethods
{
    /// <summary>
    /// Extension methods that add basic math functions
    /// </summary>
    public static class MathExtensions
    {
        #region Public Static Functions

        #region Absolute

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The absolute value</returns>
        public static decimal Absolute(this decimal value)
        {
            return System.Math.Abs(value);
        }

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The absolute value</returns>
        public static double Absolute(this double value)
        {
            return System.Math.Abs(value);
        }

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The absolute value</returns>
        public static float Absolute(this float value)
        {
            return System.Math.Abs(value);
        }

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The absolute value</returns>
        public static int Absolute(this int value)
        {
            return System.Math.Abs(value);
        }

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The absolute value</returns>
        public static long Absolute(this long value)
        {
            return System.Math.Abs(value);
        }

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The absolute value</returns>
        public static short Absolute(this short value)
        {
            return System.Math.Abs(value);
        }

        #endregion

        #region Exp

        /// <summary>
        /// Returns E raised to the specified power
        /// </summary>
        /// <param name="value">Power to raise E by</param>
        /// <returns>E raised to the specified power</returns>
        public static double Exp(this double value)
        {
            return System.Math.Exp(value);
        }

        #endregion

        #region Factorial

        /// <summary>
        /// Calculates the factorial for a number
        /// </summary>
        /// <param name="input">Input value (N!)</param>
        /// <returns>The factorial specified</returns>
        public static int Factorial(this int input)
        {
            int value1 = 1;
            for (int x = 2; x <= input; ++x)
                value1 = value1 * x;
            return value1;
        }

        #endregion

        #region GreatestCommonDenominator

        /// <summary>
        /// Returns the greatest common denominator between value1 and value2
        /// </summary>
        /// <param name="value1">Value 1</param>
        /// <param name="value2">Value 2</param>
        /// <returns>The greatest common denominator if one exists</returns>
        public static int GreatestCommonDenominator(this int value1, int value2)
        {
            value1 = value1.Absolute();
            value2 = value2.Absolute();
            while (value1 != 0 && value2 != 0)
            {
                if (value1 > value2)
                    value1 %= value2;
                else
                    value2 %= value1;
            }
            return value1 == 0 ? value2 : value1;
        }

        /// <summary>
        /// Returns the greatest common denominator between value1 and value2
        /// </summary>
        /// <param name="value1">Value 1</param>
        /// <param name="value2">Value 2</param>
        /// <returns>The greatest common denominator if one exists</returns>
        public static int GreatestCommonDenominator(this int value1, uint value2)
        {
            return value1.GreatestCommonDenominator((int)value2);
        }

        /// <summary>
        /// Returns the greatest common denominator between value1 and value2
        /// </summary>
        /// <param name="value1">Value 1</param>
        /// <param name="value2">Value 2</param>
        /// <returns>The greatest common denominator if one exists</returns>
        public static int GreatestCommonDenominator(this uint value1, uint value2)
        {
            return ((int)value1).GreatestCommonDenominator((int)value2);
        }

        #endregion

        #region Log

        /// <summary>
        /// Returns the natural (base e) logarithm of a specified number
        /// </summary>
        /// <param name="value">Specified number</param>
        /// <returns>The natural logarithm of the specified number</returns>
        public static double Log(this double value)
        {
            return System.Math.Log(value);
        }

        /// <summary>
        /// Returns the logarithm of a specified number in a specified base
        /// </summary>
        /// <param name="value">Value</param>
        /// <param name="Base">Base</param>
        /// <returns>The logarithm of a specified number in a specified base</returns>
        public static double Log(this double value, double Base)
        {
            return System.Math.Log(value, Base);
        }

        #endregion

        #region Log10

        /// <summary>
        /// Returns the base 10 logarithm of a specified number
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>The base 10 logarithm of the specified number</returns>
        public static double Log10(this double value)
        {
            return System.Math.Log10(value);
        }

        #endregion

        #region Median

        /// <summary>
        /// Gets the median from the list
        /// </summary>
        /// <typeparam name="T">The data type of the list</typeparam>
        /// <param name="values">The list of values</param>
        /// <returns>The median value</returns>
        public static T Median<T>(this IEnumerable<T> values)
        {
            if (values == null)
                return default(T);
            var enumerable = values as T[] ?? values.ToArray();
            if (enumerable.Count() == 0)
                return default(T);
            values = enumerable.OrderBy(x => x);
            return values.ElementAt((values.Count() / 2));
        }

        #endregion

        #region Mode

        /// <summary>
        /// Gets the mode (item that occurs the most) from the list
        /// </summary>
        /// <typeparam name="T">The data type of the list</typeparam>
        /// <param name="values">The list of values</param>
        /// <returns>The mode value</returns>
        public static T Mode<T>(this IEnumerable<T> values)
        {
            if (values == null)
                return default(T);
            var enumerable = values as T[] ?? values.ToArray();
            if (enumerable.Count() == 0)
                return default(T);
            var items = new Bag<T>();
            foreach (T value in enumerable)
                items.Add(value);
            int maxValue = 0;
            T maxIndex = default(T);
            foreach (T key in items)
            {
                if (items[key] > maxValue)
                {
                    maxValue = items[key];
                    maxIndex = key;
                }
            }
            return maxIndex;
        }

        #endregion

        #region Pow

        /// <summary>
        /// Raises Value to the power of Power
        /// </summary>
        /// <param name="value">Value to raise</param>
        /// <param name="power">Power</param>
        /// <returns>The resulting value</returns>
        public static double Pow(this double value, double power)
        {
            return System.Math.Pow(value, power);
        }

        #endregion

        #region Round

        /// <summary>
        /// Rounds the value to the number of digits
        /// </summary>
        /// <param name="value">Value to round</param>
        /// <param name="digits">Digits to round to</param>
        /// <param name="rounding">Rounding mode to use</param>
        /// <returns></returns>
        public static double Round(this double value, int digits = 2, MidpointRounding rounding = MidpointRounding.AwayFromZero)
        {
            return System.Math.Round(value, digits, rounding);
        }

        #endregion

        #region StandardDeviation

        /// <summary>
        /// Gets the standard deviation
        /// </summary>
        /// <param name="values">List of values</param>
        /// <returns>The standard deviation</returns>
        public static double StandardDeviation(this IEnumerable<double> values)
        {
            return values.Variance().Sqrt();
        }

        #endregion

        #region Sqrt

        /// <summary>
        /// Returns the square root of a value
        /// </summary>
        /// <param name="value">Value to take the square root of</param>
        /// <returns>The square root</returns>
        public static double Sqrt(this double value)
        {
            return System.Math.Sqrt(value);
        }

        #endregion

        #region Variance

        /// <summary>
        /// Calculates the variance of a list of values
        /// </summary>
        /// <param name="values">List of values</param>
        /// <returns>The variance</returns>
        public static double Variance(this IEnumerable<double> values)
        {
            var enumerable = values as double[] ?? values.ToArray();
            if (values == null || enumerable.Count() == 0)
                return 0;
            double meanValue = enumerable.Average();
            double sum = 0;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (double value in enumerable)
                // ReSharper restore LoopCanBeConvertedToQuery
                sum += (value - meanValue).Pow(2);
            return sum / enumerable.Count();
        }

        /// <summary>
        /// Calculates the variance of a list of values
        /// </summary>
        /// <param name="values">List of values</param>
        /// <returns>The variance</returns>
        public static double Variance(this IEnumerable<int> values)
        {
            var enumerable = values as int[] ?? values.ToArray();
            if (values == null || enumerable.Count() == 0)
                return 0;
            double meanValue = enumerable.Average();
            double sum = 0;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (int value in enumerable)
                // ReSharper restore LoopCanBeConvertedToQuery
                sum += (value - meanValue).Pow(2);
            return sum / enumerable.Count();
        }

        /// <summary>
        /// Calculates the variance of a list of values
        /// </summary>
        /// <param name="values">List of values</param>
        /// <returns>The variance</returns>
        public static double Variance(this IEnumerable<float> values)
        {
            var enumerable = values as float[] ?? values.ToArray();
            if (values == null || enumerable.Count() == 0)
                return 0;
            double meanValue = enumerable.Average();
            double sum = 0;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var value in enumerable)
                // ReSharper restore LoopCanBeConvertedToQuery
                sum += (value - meanValue).Pow(2);
            return sum / enumerable.Count();
        }

        #endregion

        #endregion
    }
}