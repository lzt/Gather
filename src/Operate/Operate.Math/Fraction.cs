﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Globalization;
using Operate.Math.ExtensionMethods;

#endregion

namespace Operate.Math
{
    /// <summary>
    /// Represents a fraction
    /// </summary>
    public class Fraction
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numerator">Numerator</param>
        /// <param name="denominator">Denominator</param>
        public Fraction(int numerator, int denominator)
        {
            Numerator = numerator;
            Denominator = denominator;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numerator">Numerator</param>
        /// <param name="denominator">Denominator</param>
        public Fraction(double numerator, double denominator)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            while (numerator != System.Math.Round(numerator, MidpointRounding.AwayFromZero)
                || denominator != System.Math.Round(denominator, MidpointRounding.AwayFromZero))
            // ReSharper restore CompareOfFloatsByEqualityOperator
            {
                numerator *= 10;
                denominator *= 10;
            }
            Numerator = (int)numerator;
            Denominator = (int)denominator;
            Reduce();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numerator">Numerator</param>
        /// <param name="denominator">Denominator</param>
        public Fraction(decimal numerator, decimal denominator)
        {
            while (numerator != System.Math.Round(numerator, MidpointRounding.AwayFromZero)
                || denominator != System.Math.Round(denominator, MidpointRounding.AwayFromZero))
            {
                numerator *= 10;
                denominator *= 10;
            }
            Numerator = (int)numerator;
            Denominator = (int)denominator;
            Reduce();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numerator">Numerator</param>
        /// <param name="denominator">Denominator</param>
        public Fraction(float numerator, float denominator)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            while (numerator != System.Math.Round(numerator, MidpointRounding.AwayFromZero)
                || denominator != System.Math.Round(denominator, MidpointRounding.AwayFromZero))
            // ReSharper restore CompareOfFloatsByEqualityOperator
            {
                numerator *= 10;
                denominator *= 10;
            }
            Numerator = (int)numerator;
            Denominator = (int)denominator;
            Reduce();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Numerator of the faction
        /// </summary>
        public int Numerator { get; set; }

        /// <summary>
        /// Denominator of the fraction
        /// </summary>
        public int Denominator { get; set; }

        #endregion

        #region Functions

        #region ToString

        /// <summary>
        /// Displays the fraction as a string
        /// </summary>
        /// <returns>The fraction as a string</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}/{1}", Numerator, Denominator);
        }

        #endregion

        #region GetHashCode

        /// <summary>
        /// Gets the hash code of the fraction
        /// </summary>
        /// <returns>The hash code of the fraction</returns>
        public override int GetHashCode()
        {
            return Numerator.GetHashCode() % Denominator.GetHashCode();
        }

        #endregion

        #region Equals

        /// <summary>
        /// Determines if the fractions are equal
        /// </summary>
        /// <param name="obj">object to check</param>
        /// <returns>True if they are, false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Fraction))
                return false;
            var other = obj as Fraction;
            decimal value1 = this;
            decimal value2 = other;
            return value1 == value2;
        }

        #endregion

        #region Reduce

        /// <summary>
        /// Reduces the fraction (finds the greatest common denominator and divides the numerator/denominator by it).
        /// </summary>
        public void Reduce()
        {
            int gcd = Numerator.GreatestCommonDenominator(Denominator);
            if (gcd != 0)
            {
                Numerator /= gcd;
                Denominator /= gcd;
            }
        }

        #endregion

        #region Inverse

        /// <summary>
        /// Returns the inverse of the fraction
        /// </summary>
        /// <returns>The inverse</returns>
        public Fraction Inverse()
        {
            return new Fraction(Denominator, Numerator);
        }

        #endregion

        #endregion

        #region Operators

        #region Equals

        /// <summary>
        /// Equals operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator ==(Fraction first, Fraction second)
        {
            return first != null && first.Equals(second);
        }

        /// <summary>
        /// Equals operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator ==(Fraction first, double second)
        {
            // ReSharper disable SuspiciousTypeConversion.Global
            return first != null && first.Equals(second);
            // ReSharper restore SuspiciousTypeConversion.Global
        }

        /// <summary>
        /// Equals operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator ==(double first, Fraction second)
        {
            // ReSharper disable SuspiciousTypeConversion.Global
            return second != null && second.Equals(first);
            // ReSharper restore SuspiciousTypeConversion.Global
        }

        #endregion

        #region Not Equals

        /// <summary>
        /// Not equals operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator !=(Fraction first, Fraction second)
        {
            return !(first == second);
        }

        /// <summary>
        /// Not equals operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator !=(Fraction first, double second)
        {
            return !(first == second);
        }

        /// <summary>
        /// Not equals operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator !=(double first, Fraction second)
        {
            return !(first == second);
        }

        #endregion

        #region ToDouble

        /// <summary>
        /// Converts the fraction to a double
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The fraction as a double</returns>
        public static implicit operator double(Fraction fraction)
        {
            return (fraction.Numerator / (double)fraction.Denominator);
        }

        #endregion

        #region ToDecimal

        /// <summary>
        /// Converts the fraction to a decimal
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The fraction as a decimal</returns>
        public static implicit operator decimal(Fraction fraction)
        {
            return (fraction.Numerator / (decimal)fraction.Denominator);
        }

        #endregion

        #region ToFloat

        /// <summary>
        /// Converts the fraction to a float
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The fraction as a float</returns>
        public static implicit operator float(Fraction fraction)
        {
            return (fraction.Numerator / (float)fraction.Denominator);
        }

        #endregion

        #region FromDouble

        /// <summary>
        /// Converts the double to a fraction
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The double as a fraction</returns>
        public static implicit operator Fraction(double fraction)
        {
            return new Fraction(fraction, 1.0);
        }

        #endregion

        #region FromDecimal

        /// <summary>
        /// Converts the decimal to a fraction
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The decimal as a fraction</returns>
        public static implicit operator Fraction(decimal fraction)
        {
            return new Fraction(fraction, 1.0m);
        }

        #endregion

        #region FromFloat

        /// <summary>
        /// Converts the float to a fraction
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The float as a fraction</returns>
        public static implicit operator Fraction(float fraction)
        {
            return new Fraction(fraction, 1.0);
        }

        #endregion

        #region FromInt

        /// <summary>
        /// Converts the int to a fraction
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The int as a fraction</returns>
        public static implicit operator Fraction(int fraction)
        {
            return new Fraction(fraction, 1);
        }

        #endregion

        #region FromUInt

        /// <summary>
        /// Converts the uint to a fraction
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The uint as a fraction</returns>
        public static implicit operator Fraction(uint fraction)
        {
            return new Fraction((int)fraction, 1);
        }

        #endregion

        #region ToString

        /// <summary>
        /// Converts the fraction to a string
        /// </summary>
        /// <param name="fraction">Fraction</param>
        /// <returns>The fraction as a string</returns>
        public static implicit operator string(Fraction fraction)
        {
            return fraction.ToString();
        }

        #endregion

        #region Multiplication

        /// <summary>
        /// Multiplication
        /// </summary>
        /// <param name="first">First fraction</param>
        /// <param name="second">Second fraction</param>
        /// <returns>The resulting fraction</returns>
        public static Fraction operator *(Fraction first, Fraction second)
        {
            var result = new Fraction(first.Numerator * second.Numerator, first.Denominator * second.Denominator);
            result.Reduce();
            return result;
        }

        #endregion

        #region Addition

        /// <summary>
        /// Addition
        /// </summary>
        /// <param name="first">First fraction</param>
        /// <param name="second">Second fraction</param>
        /// <returns>The added fraction</returns>
        public static Fraction operator +(Fraction first, Fraction second)
        {
            var value1 = new Fraction(first.Numerator * second.Denominator, first.Denominator * second.Denominator);
            var value2 = new Fraction(second.Numerator * first.Denominator, second.Denominator * first.Denominator);
            var result = new Fraction(value1.Numerator + value2.Numerator, value1.Denominator);
            result.Reduce();
            return result;
        }

        #endregion

        #region Subtraction

        /// <summary>
        /// Subtraction
        /// </summary>
        /// <param name="first">First fraction</param>
        /// <param name="second">Second fraction</param>
        /// <returns>The subtracted fraction</returns>
        public static Fraction operator -(Fraction first, Fraction second)
        {
            var value1 = new Fraction(first.Numerator * second.Denominator, first.Denominator * second.Denominator);
            var value2 = new Fraction(second.Numerator * first.Denominator, second.Denominator * first.Denominator);
            var result = new Fraction(value1.Numerator - value2.Numerator, value1.Denominator);
            result.Reduce();
            return result;
        }

        #endregion

        #region Division

        /// <summary>
        /// Division
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>The divided fraction</returns>
        public static Fraction operator /(Fraction first, Fraction second)
        {
            return first * second.Inverse();
        }

        #endregion

        #region Negation

        /// <summary>
        /// Negation of the fraction
        /// </summary>
        /// <param name="first">Fraction to negate</param>
        /// <returns>The negated fraction</returns>
        public static Fraction operator -(Fraction first)
        {
            return new Fraction(-first.Numerator, first.Denominator);
        }

        #endregion

        #endregion
    }
}