﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Text;
using System.Xml.Serialization;

#endregion

namespace Operate.Math
{
    /// <summary>
    /// Matrix used in linear algebra
    /// </summary>
    [Serializable]
    public class Matrix
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width">Width of the matrix</param>
        /// <param name="height">Height of the matrix</param>
        /// <param name="values">Values to use in the matrix</param>
        public Matrix(int width, int height, double[,] values = null)
        {
            _width = width;
            _height = height;
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            Values = values ?? new double[width, height];
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Width of the matrix
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body"), XmlElement]
        public virtual int Width
        {
            get { return _width; }
            set { _width = value; Values = new double[Width, Height]; }
        }

        /// <summary>
        /// Height of the matrix
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body"), XmlElement]
        public virtual int Height
        {
            get { return _height; }
            set { _height = value; Values = new double[Width, Height]; }
        }

        /// <summary>
        /// Sets the values of the matrix
        /// </summary>
        /// <param name="x">x position</param>
        /// <param name="y">Y position</param>
        /// <returns>the value at a point in the matrix</returns>
        public virtual double this[int x, int y]
        {
            get
            {
                if (!(x >= 0 && x <= Width)) { throw new ArgumentOutOfRangeException("x"); }
                if (!(y >= 0 && y <= Height)) { throw new ArgumentOutOfRangeException("y"); }
                return Values[x, y];
            }

            set
            {
                if (!(x >= 0 && x <= Width)) { throw new ArgumentOutOfRangeException("x"); }
                if (!(y >= 0 && y <= Height)) { throw new ArgumentOutOfRangeException("y"); }
                Values[x, y] = value;
            }
        }

        /// <summary>
        /// Values for the matrix
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member"), XmlElement]
        public virtual double[,] Values { get; set; }

        #endregion

        #region Private Variables
        private int _width = 1;
        private int _height = 1;
        #endregion

        #region Operators

        /// <summary>
        /// Determines if two matrices are equal
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="m2">Matrix 2</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public static bool operator ==(Matrix m1, Matrix m2)
        {
            if ((object)m1 == null && (object)m2 == null)
                return true;
            if ((object)m1 == null)
                return false;
            if ((object)m2 == null)
                return false;
            if (m1.Width != m2.Width || m1.Height != m2.Height)
                return false;
            for (int x = 0; x <= m1.Width; ++x)
                for (int y = 0; y <= m1.Height; ++y)
                    // ReSharper disable CompareOfFloatsByEqualityOperator
                    if (m1[x, y] != m2[x, y])
                        // ReSharper restore CompareOfFloatsByEqualityOperator
                        return false;
            return true;
        }

        /// <summary>
        /// Determines if two matrices are unequal
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="m2">Matrix 2</param>
        /// <returns>True if they are not equal, false otherwise</returns>
        public static bool operator !=(Matrix m1, Matrix m2)
        {
            return !(m1 == m2);
        }

        /// <summary>
        /// Adds two matrices
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="m2">Matrix 2</param>
        /// <returns>The result</returns>
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            if (m2.IsNull()) { throw new ArgumentNullException("m2"); }
            if (!(m1.Width == m2.Width && m1.Height == m2.Height)) { throw new ArgumentException("Both matrices must be the same dimensions."); }
            var tempMatrix = new Matrix(m1.Width, m1.Height);
            for (int x = 0; x < m1.Width; ++x)
                for (int y = 0; y < m1.Height; ++y)
                    tempMatrix[x, y] = m1[x, y] + m2[x, y];
            return tempMatrix;
        }

        /// <summary>
        /// Subtracts two matrices
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="m2">Matrix 2</param>
        /// <returns>The result</returns>
        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            if (m2.IsNull()) { throw new ArgumentNullException("m2"); }
            if (!(m1.Width == m2.Width && m1.Height == m2.Height)) { throw new ArgumentException("Both matrices must be the same dimensions."); }
            var tempMatrix = new Matrix(m1.Width, m1.Height);
            for (int x = 0; x < m1.Width; ++x)
                for (int y = 0; y < m1.Height; ++y)
                    tempMatrix[x, y] = m1[x, y] - m2[x, y];
            return tempMatrix;
        }

        /// <summary>
        /// Negates a matrix
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <returns>The result</returns>
        public static Matrix operator -(Matrix m1)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            var tempMatrix = new Matrix(m1.Width, m1.Height);
            for (int x = 0; x < m1.Width; ++x)
                for (int y = 0; y < m1.Height; ++y)
                    tempMatrix[x, y] = -m1[x, y];
            return tempMatrix;
        }

        /// <summary>
        /// Multiplies two matrices
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="m2">Matrix 2</param>
        /// <returns>The result</returns>
        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            if (m2.IsNull()) { throw new ArgumentNullException("m2"); }
            if (!(m1.Width == m2.Width && m1.Height == m2.Height)) { throw new ArgumentException("Both matrices must be the same dimensions."); }
            var tempMatrix = new Matrix(m2.Width, m1.Height);
            for (int x = 0; x < m2.Width; ++x)
            {
                for (int y = 0; y < m1.Height; ++y)
                {
                    tempMatrix[x, y] = 0.0;
                    for (int i = 0; i < m1.Width; ++i)
                        for (int j = 0; j < m2.Height; ++j)
                            tempMatrix[x, y] += (m1[i, y] * m2[x, j]);
                }
            }
            return tempMatrix;
        }

        /// <summary>
        /// Multiplies a matrix by a value
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="d">Value to multiply by</param>
        /// <returns>The result</returns>
        public static Matrix operator *(Matrix m1, double d)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            var tempMatrix = new Matrix(m1.Width, m1.Height);
            for (int x = 0; x < m1.Width; ++x)
                for (int y = 0; y < m1.Height; ++y)
                    tempMatrix[x, y] = m1[x, y] * d;
            return tempMatrix;
        }

        /// <summary>
        /// Multiplies a matrix by a value
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="d">Value to multiply by</param>
        /// <returns>The result</returns>
        public static Matrix operator *(double d, Matrix m1)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            var tempMatrix = new Matrix(m1.Width, m1.Height);
            for (int x = 0; x < m1.Width; ++x)
                for (int y = 0; y < m1.Height; ++y)
                    tempMatrix[x, y] = m1[x, y] * d;
            return tempMatrix;
        }

        /// <summary>
        /// Divides a matrix by a value
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="d">Value to divide by</param>
        /// <returns>The result</returns>
        public static Matrix operator /(Matrix m1, double d)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            return m1 * (1 / d);
        }

        /// <summary>
        /// Divides a matrix by a value
        /// </summary>
        /// <param name="m1">Matrix 1</param>
        /// <param name="d">Value to divide by</param>
        /// <returns>The result</returns>
        public static Matrix operator /(double d, Matrix m1)
        {
            if (m1.IsNull()) { throw new ArgumentNullException("m1"); }
            return m1 * (1 / d);
        }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Determines if the objects are equal
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>True if they are, false otherwise</returns>
        public override bool Equals(object obj)
        {
            var tempobj = obj as Matrix;
            return tempobj != null && this == tempobj;
        }

        /// <summary>
        /// Gets the hash code for the object
        /// </summary>
        /// <returns>The hash code for the object</returns>
        public override int GetHashCode()
        {
            double hash = 0;
            for (int x = 0; x < Width; ++x)
                for (int y = 0; y < Height; ++y)
                    hash += this[x, y];
            return (int)hash;
        }

        /// <summary>
        /// Gets the string representation of the matrix
        /// </summary>
        /// <returns>The matrix as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            string seperator = "";
            builder.Append("{").Append(Environment.NewLine);
            for (int x = 0; x < Width; ++x)
            {
                builder.Append("{");
                for (int y = 0; y < Height; ++y)
                {
                    builder.Append(seperator).Append(this[x, y]);
                    seperator = ",";
                }
                builder.Append("}").Append(Environment.NewLine);
                seperator = "";
            }
            builder.Append("}");
            return builder.ToString();
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Transposes the matrix
        /// </summary>
        /// <returns>Returns a new transposed matrix</returns>
        public virtual Matrix Transpose()
        {
            var tempValues = new Matrix(Height, Width);
            for (int x = 0; x < Width; ++x)
                for (int y = 0; y < Height; ++y)
                    tempValues[y, x] = Values[x, y];
            return tempValues;
        }

        /// <summary>
        /// Gets the determinant of a square matrix
        /// </summary>
        /// <returns>The determinant of a square matrix</returns>
        public virtual double Determinant()
        {
            if (Width != Height) { throw new ArgumentException("The determinant can not be calculated for a non square matrix"); }
            if (Width == 2)
                return (this[0, 0] * this[1, 1]) - (this[0, 1] * this[1, 0]);
            double answer = 0.0;
            for (int x = 0; x < Width; ++x)
            {
                var tempMatrix = new Matrix(Width - 1, Height - 1);
                int widthCounter = 0;
                for (int y = 0; y < Width; ++y)
                {
                    if (y != x)
                    {
                        for (int z = 1; z < Height; ++z)
                            tempMatrix[widthCounter, z - 1] = this[y, z];
                        ++widthCounter;
                    }
                }
                if (x % 2 == 0)
                {
                    answer += tempMatrix.Determinant();
                }
                else
                {
                    answer -= tempMatrix.Determinant();
                }
            }
            return answer;
        }

        #endregion
    }
}