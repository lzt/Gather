﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Xml.Serialization;
using Operate.Math.ExtensionMethods;

#endregion

namespace Operate.Math
{
    /// <summary>
    /// Vector class (holds three items)
    /// </summary>
    [Serializable]
    public class Vector3
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">x direction</param>
        /// <param name="y">Y direction</param>
        /// <param name="z">Z direction</param>
        public Vector3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Normalizes the vector
        /// </summary>
        public virtual void Normalize()
        {
            double normal = Magnitude;
            if (normal > 0)
            {
                normal = 1 / normal;
                X *= normal;
                Y *= normal;
                Z *= normal;
            }
        }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// To string function
        /// </summary>
        /// <returns>String representation of the vector</returns>
        public override string ToString()
        {
            return "(" + X + "," + Y + "," + Z + ")";
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return (int)(X + Y + Z) % Int32.MaxValue;
        }

        /// <summary>
        /// Determines if the items are equal
        /// </summary>
        /// <param name="obj">Object to compare</param>
        /// <returns>true if they are, false otherwise</returns>
        public override bool Equals(object obj)
        {
            var tempobj = obj as Vector3;
            return tempobj != null && this == tempobj;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Used for converting this to an array and back
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public virtual double[] Array
        {
            get { return new[] { X, Y, Z }; }
            set
            {
                if (value.Length == 3)
                {
                    X = value[0];
                    Y = value[1];
                    Z = value[2];
                }
            }
        }

        /// <summary>
        /// Returns the magnitude of the vector
        /// </summary>
        public virtual double Magnitude
        {
            get { return ((X * X) + (Y * Y) + (Z * Z)).Sqrt(); }
        }

        /// <summary>
        /// x value
        /// </summary>
        [XmlElement]
        public virtual double X { get; set; }

        /// <summary>
        /// Y Value
        /// </summary>
        [XmlElement]
        public virtual double Y { get; set; }

        /// <summary>
        /// Z value
        /// </summary>
        [XmlElement]
        public virtual double Z { get; set; }

        #endregion

        #region Public Static Functions

        /// <summary>
        /// Addition
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        /// <summary>
        /// Subtraction
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        /// <summary>
        /// Negation
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator -(Vector3 v1)
        {
            return new Vector3(-v1.X, -v1.Y, -v1.Z);
        }

        /// <summary>
        /// Less than
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static bool operator <(Vector3 v1, Vector3 v2)
        {
            return v1.Magnitude < v2.Magnitude;
        }

        /// <summary>
        /// Less than or equal
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static bool operator <=(Vector3 v1, Vector3 v2)
        {
            return v1.Magnitude <= v2.Magnitude;
        }

        /// <summary>
        /// Greater than
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static bool operator >(Vector3 v1, Vector3 v2)
        {
            return v1.Magnitude > v2.Magnitude;
        }

        /// <summary>
        /// Greater than or equal
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static bool operator >=(Vector3 v1, Vector3 v2)
        {
            return v1.Magnitude >= v2.Magnitude;
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static bool operator ==(Vector3 v1, Vector3 v2)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            return v1 != null && (v2 != null && (v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z));
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        /// <summary>
        /// Not equals
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static bool operator !=(Vector3 v1, Vector3 v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Division
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="d">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator /(Vector3 v1, double d)
        {
            return new Vector3(v1.X / d, v1.Y / d, v1.Z / d);
        }

        /// <summary>
        /// Multiplication
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="d">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator *(Vector3 v1, double d)
        {
            return new Vector3(v1.X * d, v1.Y * d, v1.Z * d);
        }

        /// <summary>
        /// Multiplication
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="d">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator *(double d, Vector3 v1)
        {
            return new Vector3(v1.X * d, v1.Y * d, v1.Z * d);
        }

        /// <summary>
        /// Does a cross product
        /// </summary>
        /// <param name="v1">Item 1</param>
        /// <param name="v2">Item 2</param>
        /// <returns>The resulting vector</returns>
        public static Vector3 operator *(Vector3 v1, Vector3 v2)
        {
            var tempVector = new Vector3(0.0, 0.0, 0.0)
                {
                    X = (v1.Y*v2.Z) - (v1.Z*v2.Y),
                    Y = (v1.Z*v2.X) - (v1.X*v2.Z),
                    Z = (v1.X*v2.Y) - (v1.Y*v2.X)
                };
            return tempVector;
        }

        /// <summary>
        /// Does a dot product
        /// </summary>
        /// <param name="v1">Vector 1</param>
        /// <param name="v2">Vector 2</param>
        /// <returns>a dot product</returns>
        public static double DotProduct(Vector3 v1, Vector3 v2)
        {
            return (v1.X * v2.X) + (v1.Y * v2.Y) + (v1.Z * v2.Z);
        }

        /// <summary>
        /// Interpolates between the vectors
        /// </summary>
        /// <param name="v1">Vector 1</param>
        /// <param name="v2">Vector 2</param>
        /// <param name="control">Percent to move between 1 and 2</param>
        /// <returns>The interpolated vector</returns>
        public static Vector3 Interpolate(Vector3 v1, Vector3 v2, double control)
        {
            var tempVector = new Vector3(0.0, 0.0, 0.0)
                {
                    X = (v1.X*(1 - control)) + (v2.X*control),
                    Y = (v1.Y*(1 - control)) + (v2.Y*control),
                    Z = (v1.Z*(1 - control)) - (v2.Z*control)
                };
            return tempVector;
        }

        /// <summary>
        /// The distance between two vectors
        /// </summary>
        /// <param name="v1">Vector 1</param>
        /// <param name="v2">Vector 2</param>
        /// <returns>Distance between the vectors</returns>
        public static double Distance(Vector3 v1, Vector3 v2)
        {
            return (((v1.X - v2.X) * (v1.X - v2.X)) + ((v1.Y - v2.Y) * (v1.Y - v2.Y)) + ((v1.Z - v2.Z) * (v1.Z - v2.Z))).Sqrt();
        }

        /// <summary>
        /// Determines the angle between the vectors
        /// </summary>
        /// <param name="v1">Vector 1</param>
        /// <param name="v2">Vector 2</param>
        /// <returns>Angle between the vectors</returns>
        public static double Angle(Vector3 v1, Vector3 v2)
        {
            v1.Normalize();
            v2.Normalize();
            return System.Math.Acos(Vector3.DotProduct(v1, v2));
        }

        #endregion
    }
}