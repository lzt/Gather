﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Text;

#endregion

namespace Operate.Math
{
    /// <summary>
    /// Class to be used for sets of data
    /// </summary>
    /// <typeparam name="T">Type that the set holds</typeparam>
    public class Set<T> : List<T>
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Set()
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="initialSize">Initial size</param>
        public Set(int initialSize)
            : base(initialSize)
        {
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Used to tell if this set contains the other
        /// </summary>
        /// <param name="set">Set to check against</param>
        /// <returns>True if it is, false otherwise</returns>
        public virtual bool Contains(Set<T> set)
        {
            return set.IsSubset(this);
        }

        /// <summary>
        /// Used to tell if this is a subset of the other
        /// </summary>
        /// <param name="set">Set to check against</param>
        /// <returns>True if it is, false otherwise</returns>
        public virtual bool IsSubset(Set<T> set)
        {
            if (set == null || Count > set.Count)
                return false;

            for (int x = 0; x < Count; ++x)
                if (!set.Contains(this[x]))
                    return false;
            return true;
        }

        /// <summary>
        /// Determines if the sets intersect
        /// </summary>
        /// <param name="set">Set to check against</param>
        /// <returns>True if they do, false otherwise</returns>
        public virtual bool Intersect(Set<T> set)
        {
            if (set == null)
                return false;
            for (int x = 0; x < Count; ++x)
                if (set.Contains(this[x]))
                    return true;
            return false;
        }

        #endregion

        #region Public Static Functions

        /// <summary>
        /// Gets the intersection of set 1 and set 2
        /// </summary>
        /// <param name="set1">Set 1</param>
        /// <param name="set2">Set 2</param>
        /// <returns>The intersection of the two sets</returns>
        public static Set<T> GetIntersection(Set<T> set1, Set<T> set2)
        {
            if (set1 == null || set2 == null || !set1.Intersect(set2))
                return null;
            var returnValue = new Set<T>();
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (T t in set1)
                if (set2.Contains(t))
                    returnValue.Add(t);

            foreach (T t in set2)
                if (set1.Contains(t))
                    returnValue.Add(t);
            // ReSharper restore LoopCanBeConvertedToQuery

            return returnValue;
        }

        /// <summary>
        /// Adds two sets together
        /// </summary>
        /// <param name="set1">Set 1</param>
        /// <param name="set2">Set 2</param>
        /// <returns>The joined sets</returns>
        public static Set<T> operator +(Set<T> set1, Set<T> set2)
        {
            if (set1.IsNull()) { throw new ArgumentNullException("set1"); }
            if (set2.IsNull()) { throw new ArgumentNullException("set2"); }

            var returnValue = new Set<T>();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (T t in set1)
                returnValue.Add(t);
            foreach (T t in set2)
                returnValue.Add(t);
            // ReSharper restore LoopCanBeConvertedToQuery

            return returnValue;
        }

        /// <summary>
        /// Removes items from set 2 from set 1
        /// </summary>
        /// <param name="set1">Set 1</param>
        /// <param name="set2">Set 2</param>
        /// <returns>The resulting set</returns>
        public static Set<T> operator -(Set<T> set1, Set<T> set2)
        {
            if (set1.IsNull()) { throw new ArgumentNullException("set1"); }
            if (set2.IsNull()) { throw new ArgumentNullException("set2"); }

            var returnValue = new Set<T>();
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (T t in set1)
                // ReSharper restore LoopCanBeConvertedToQuery
                if (!set2.Contains(t))
                    returnValue.Add(t);
            return returnValue;
        }

        /// <summary>
        /// Determines if the two sets are equivalent
        /// </summary>
        /// <param name="set1">Set 1</param>
        /// <param name="set2">Set 2</param>
        /// <returns>True if they are, false otherwise</returns>
        public static bool operator ==(Set<T> set1, Set<T> set2)
        {
            if (((object)set1) == null && ((object)set2) == null)
                return true;
            if (((object)set1) == null || ((object)set2) == null)
                return false;
            return set1.Contains(set2) && set2.Contains(set1);
        }

        /// <summary>
        /// Determines if the two sets are not equivalent
        /// </summary>
        /// <param name="set1">Set 1</param>
        /// <param name="set2">Set 2</param>
        /// <returns>False if they are, true otherwise</returns>
        public static bool operator !=(Set<T> set1, Set<T> set2)
        {
            return !(set1 == set2);
        }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Returns the hash code for the object
        /// </summary>
        /// <returns>The hash code for the object</returns>
        public override int GetHashCode()
        {
            // ReSharper disable BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
            // ReSharper restore BaseObjectGetHashCodeCallInGetHashCode
        }

        /// <summary>
        /// Determines if the two sets are equivalent
        /// </summary>
        /// <param name="obj">The object to compare to</param>
        /// <returns>True if they are, false otherwise</returns>
        public override bool Equals(object obj)
        {
            return this == (obj as Set<T>);
        }

        /// <summary>
        /// Returns the set as a string
        /// </summary>
        /// <returns>The set as a string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("{ ");
            string splitter = "";
            for (int x = 0; x < Count; ++x)
            {
                builder.Append(splitter);
                builder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "{0}", this[x]);
                splitter = ",  ";
            }
            builder.Append(" }");
            return builder.ToString();
        }

        #endregion
    }
}