﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.Math.ExtensionMethods;

#endregion

namespace Operate.Math.Geometry.BaseClasses
{
    /// <summary>
    /// Base shape class
    /// </summary>
    public abstract class Shape
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="max">Max X,Y value</param>
        /// <param name="min">Min X,Y value</param>
        /// <param name="center">Center of the shape</param>
        protected Shape(Point min, Point max, Point center)
        {
            Min = min;
            Max = max;
            Center = center;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Min X,Y value
        /// </summary>
        public Point Min { get; protected set; }

        /// <summary>
        /// Max X,Y value
        /// </summary>
        public Point Max { get; protected set; }

        /// <summary>
        /// Center coordinate
        /// </summary>
        public Point Center { get; protected set; }

        /// <summary>
        /// The predicate/set that can be used to determine if a point is within the shape
        /// </summary>
        public virtual Predicate<Point> Set { get { return x => x.X >= Min.X && x.X <= Max.X && x.Y >= Min.Y && x.Y <= Max.Y; } }

        #endregion

        #region Functions

        /// <summary>
        /// Determines if the point is within the shape and returns true if it is, false otherwise
        /// </summary>
        /// <param name="x">X Coordinate</param>
        /// <param name="y">Y Coordinate</param>
        /// <returns>True if it is contained, false otherwise</returns>
        public bool Contains(double x, double y)
        {
            return Set(new Point(x, y));
        }

        /// <summary>
        /// Determines the Euclidean distance between two points
        /// </summary>
        /// <param name="x1">X1 coordinate</param>
        /// <param name="y1">Y1 coordinate</param>
        /// <param name="x2">X2 coordinate</param>
        /// <param name="y2">Y2 coordinate</param>
        /// <returns>The Euclidean distance between the two points</returns>
        protected static double EuclideanDistance(double x1, double y1, double x2, double y2)
        {
            return ((x1 - x2).Pow(2) + (y1 - y2).Pow(2)).Sqrt();
        }

        #endregion
    }
}