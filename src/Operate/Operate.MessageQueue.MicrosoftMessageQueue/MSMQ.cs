﻿using System;
using System.Collections.Generic;
using System.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.Threading;

namespace Operate.MessageQueue.MicrosoftMessageQueue
{
    public sealed class MSMQ : MessageQueueBase, IMessageSender, IMessageReceiver, IDisposable
    {
        private readonly System.Messaging.MessageQueue _mq;

        /// <summary>
        /// MSMQConnection
        /// </summary>
        public MSMQConnection Connection { get; }

        public bool IsOpen { get; }

        public MSMQ(MSMQConnection conn)
        {
            Connection = conn;
            IsOpen = false;

            try
            {
                _mq = new System.Messaging.MessageQueue(Connection.ConnectionString);
                IsOpen = true;

                TaskScheduler = null;
                TaskList = new List<Task>();

                if (Connection.MaxThread <= 1)
                {
                    Connection.ThreadMode = ThreadMode.SingleThread;
                    Connection.MaxThread = MQConnectionBase.DefaultMaxThread;
                }

                if (Connection.ThreadMode == ThreadMode.ThreadPool)
                {
                    TaskScheduler = new OperateTaskScheduler(Connection.MaxThread);
                }
            }
            catch (Exception ex)
            {
                IsOpen = false;

                if (Connection.UseLogger)
                {
                    Logger.RecordException(ex);
                }
            }
        }

        protected override bool GetUserLogger()
        {
            return Connection.UseLogger;
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void Send<T>(T message) 
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                bool result;
                TrySend(message, out result);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(SendCallByAsync, message);

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        public void Send(string message)
        {
            Send<string>(message);
        }

        public void Send(byte[] message)
        {
            Send<byte[]>(message);
        }

        private void SendCallByAsync<T>(T message) 
        {
            bool result;
            TrySend(message, out result);
            Thread.Sleep(50);
        }

        public void TrySend<T>(T message, out bool result) 
        {
            try
            {
                if (!IsOpen || !_mq.CanWrite)
                {
                    result = false;
                }
                else
                {
                    var label = Connection.MessageLabel;
                    if (string.IsNullOrWhiteSpace(label))
                    {
                        if (message is string)
                        {
                            label = message.ToString().CutOut(6);
                        }
                        else
                        {
                            label = message.GetType().Name;
                        }
                    }

                    var msg = new Message
                    {
                        Recoverable = true,
                        Priority = Connection.MessagePriority,
                        Label = label,
                        Body = message,
                        Formatter = new XmlMessageFormatter(new[] { typeof(string) })
                    };
                    //为了避免存放消息队列的计算机重新启动而丢失消息，可以通过设置消息对象的Recoverable属性为true，
                    //在消息传递过程中将消息保存到磁盘上来保证消息的传递，默认为false。 
                    _mq.Send(msg);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;

                if (Connection.UseLogger)
                {
                    Logger.RecordException(ex);
                }
            }
        }

        /// <summary>
        /// TrySend
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void TrySend(byte[] message, out bool result)
        {
            TrySend<byte[]>(message, out result);
        }

        /// <summary>
        /// TrySend
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void TrySend(string message, out bool result)
        {
            TrySend<string>(message, out result);
        }

        /// <summary>
        /// ReceiveMessage
        /// </summary>
        /// <param name="messageReceiver"></param>
        public void ReceiveMessage(Action<object> messageReceiver)
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                var obj = GetReceiveMessage();
                messageReceiver(obj);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(messageReceiver, GetReceiveMessage());

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        public void ReceiveMessage<T>(Action<T> messageReceiver) where T : class
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                var obj = GetReceiveMessage<T>();
                messageReceiver(obj);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(messageReceiver, GetReceiveMessage<T>());

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        public object GetReceiveMessage()
        {
            if (_mq.CanRead)
            {
                var message = _mq.Receive();
                if (message != null)
                {
                    message.Formatter = new XmlMessageFormatter(new[] { typeof(string) });
                    return message;
                }
            }
            return null;
        }

        public T GetReceiveMessage<T>() where T : class
        {
            return GetReceiveMessage() as T;
        }

        public void ReceiveMessage(Action<Message> messageReceiver)
        {
            var message = _mq.Receive();
            if (message != null)
            {
                message.Formatter = new XmlMessageFormatter(new[] { typeof(string) });
                messageReceiver(message);
            }
        }

        private bool _disposed;

        public void Dispose()
        {
            if (_disposed) return;

            // ReSharper disable once UseNullPropagation
            if (TaskScheduler != null)
            {
                TaskScheduler.Dispose();
            }

            _mq.Close();
            _mq.Dispose();

            _disposed = true;
        }
    }
}
