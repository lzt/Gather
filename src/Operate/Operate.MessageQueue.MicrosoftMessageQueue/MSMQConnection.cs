﻿using System.Messaging;

namespace Operate.MessageQueue.MicrosoftMessageQueue
{
    public class MSMQConnection: MQConnectionBase
    {
        public const string DefaultMessageLabel = "";

        #region Properties

        public string ConnectionString { get; }

        public MessagePriority MessagePriority { get; }

        public string MessageLabel { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// MSMQConnection
        /// </summary>
        /// <param name="connectionString"></param>
        public MSMQConnection(string connectionString) : this(connectionString, MessagePriority.Normal)
        {

        }

        /// <summary>
        /// MSMQConnection
        /// </summary>
        public MSMQConnection(string connectionString, MessagePriority messagePriority)
        {
            ConnectionString = connectionString;
            MessagePriority = messagePriority;
            MessageLabel = DefaultMessageLabel;

            ThreadMode = ThreadMode.SingleThread;
            MaxThread = DefaultMaxThread;

            UseLogger = false;
        }

        #endregion
    }
}
