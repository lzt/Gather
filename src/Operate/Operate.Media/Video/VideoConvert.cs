﻿namespace Operate.Media.Video
{
    /// <summary>
    /// 视频转换
    /// </summary>
    public class VideoConvert
    {
        #region properties

        /// <summary>
        /// FFMpeg转换工具
        /// </summary>
        public static string FFMpegToolsName { get; set; }

        /// <summary>
        /// FFMpeg转换工具
        /// </summary>
        public static string FFMpegToolsPath { get; set; }

        /// <summary>
        /// 视频解码工具
        /// </summary>
        public static string MenCoderTools { get; set; }

        /// <summary>
        /// 存储路径
        /// </summary>
        public static string SaveFile { get; set; }

        /// <summary>
        /// 图片体积
        /// </summary>
        public static string SizeOfImg { get; set; }

        /// <summary>
        /// 宽度
        /// </summary>
        public static string WidthOfFile { get; set; }

        /// <summary>
        /// 高度
        /// </summary>
        public static string HeightOfFile { get; set; }

        #endregion

        #region Function

        #region 视频格式转为Flv

        /// <summary>
        /// 视频格式转为Flv
        /// </summary>
        /// <param name="sourceVideoPath">原视频文件地址</param>
        /// <param name="exportPath">生成后的Flv文件地址</param>
        public bool ToFlv(string sourceVideoPath, string exportPath)
        {
            if ((!System.IO.File.Exists(FFMpegToolsPath)) || (!System.IO.File.Exists(sourceVideoPath)))
            {
                return false;
            }
            string command = " -i \"" + sourceVideoPath + "\" -y -ab 32 -ar 22050 -b 800000 -s  480*360 \"" + exportPath + "\""; //Flv格式     
            var p = new System.Diagnostics.Process
                {
                    StartInfo =
                        {
                            FileName = FFMpegToolsName,
                            Arguments = command,
                            WorkingDirectory = FFMpegToolsPath,
                            UseShellExecute = false,
                            RedirectStandardInput = true,
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            CreateNoWindow = false
                        }
                };
            p.Start();
            p.BeginErrorReadLine();
            p.WaitForExit();
            p.Close();
            p.Dispose();
            return true;
        }

        #endregion

        #region 生成Flv视频的缩略图

        /// <summary>
        /// 生成Flv视频的缩略图
        /// </summary>
        /// <param name="sourceVideoPath">视频文件地址</param>
        public string CatchImg(string sourceVideoPath)
        {
            if ((!System.IO.File.Exists(FFMpegToolsPath)) || (!System.IO.File.Exists(sourceVideoPath))) return "";
            try
            {
                string flvImgP = sourceVideoPath.Substring(0, sourceVideoPath.Length - 4) + ".jpg";
                string command = " -i " + sourceVideoPath + " -y -f image2 -t 0.1 -s " + SizeOfImg + " " + flvImgP;
                var p = new System.Diagnostics.Process
                    {
                        StartInfo =
                            {
                                FileName = FFMpegToolsName,
                                Arguments = command,
                                WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal
                            }
                    };
                try
                {
                    p.Start();
                }
                catch
                {
                    return "";
                }
                finally
                {
                    p.Close();
                    p.Dispose();
                }
                System.Threading.Thread.Sleep(4000);

                //注意:图片截取成功后,数据由内存缓存写到磁盘需要时间较长,大概在3,4秒甚至更长;
                if (System.IO.File.Exists(flvImgP))
                {
                    return flvImgP;
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        #endregion

        //#region 运行FFMpeg的视频解码(绝对路径)
        ///// <summary>
        ///// 转换文件并保存在指定文件夹下
        ///// </summary>
        ///// <param name="fileName">上传视频文件的路径（原文件）</param>
        ///// <param name="playFile">转换后的文件的路径（网络播放文件）</param>
        ///// <param name="imgFile">从视频文件中抓取的图片路径</param>
        ///// <returns>成功:返回图片虚拟地址;失败:返回空字符串</returns>
        //public string ChangeFilePhy(string fileName, string playFile, string imgFile)
        //{
        //    string ffmpeg = Server.MapPath(VideoConvert.ffmpegtool);
        //    if ((!System.IO.File.Exists(ffmpeg)) || (!System.IO.File.Exists(fileName)))
        //    {
        //        return "";
        //    }
        //    string flv_file = System.IO.Path.ChangeExtension(playFile, ".flv");
        //    string FlvImgSize = VideoConvert.sizeOfImg;
        //    System.Diagnostics.ProcessStartInfo FilestartInfo = new System.Diagnostics.ProcessStartInfo(ffmpeg);
        //    FilestartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        //    FilestartInfo.Arguments = " -i " + fileName + " -ab 56 -ar 22050 -b 500 -r 15 -s " + widthOfFile + "x" + heightOfFile + " " + flv_file;
        //    try
        //    {
        //        System.Diagnostics.Process.Start(FilestartInfo);//转换
        //        CatchImg(fileName, imgFile); //截图
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //    return "";
        //}

        //public string CatchImg(string fileName, string imgFile)
        //{
        //    string ffmpeg = Server.MapPath(VideoConvert.ffmpegtool);
        //    string flv_img = imgFile + ".jpg";
        //    string FlvImgSize = VideoConvert.sizeOfImg;
        //    System.Diagnostics.ProcessStartInfo ImgstartInfo = new System.Diagnostics.ProcessStartInfo(ffmpeg);
        //    ImgstartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        //    ImgstartInfo.Arguments = "   -i   " + fileName + "  -y  -f  image2   -ss 2 -vframes 1  -s   " + FlvImgSize + "   " + flv_img;
        //    try
        //    {
        //        System.Diagnostics.Process.Start(ImgstartInfo);
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //    if (System.IO.File.Exists(flv_img))
        //    {
        //        return flv_img;
        //    }
        //    return "";
        //}
        //#endregion

        //#region 运行FFMpeg的视频解码(相对路径)
        ///// <summary>
        ///// 转换文件并保存在指定文件夹下
        ///// </summary>
        ///// <param name="fileName">上传视频文件的路径（原文件）</param>
        ///// <param name="playFile">转换后的文件的路径（网络播放文件）</param>
        ///// <param name="imgFile">从视频文件中抓取的图片路径</param>
        ///// <returns>成功:返回图片虚拟地址;失败:返回空字符串</returns>
        //public string ChangeFileVir(string fileName, string playFile, string imgFile)
        //{
        //    string ffmpeg = Server.MapPath(VideoConvert.ffmpegtool);
        //    if ((!System.IO.File.Exists(ffmpeg)) || (!System.IO.File.Exists(fileName)))
        //    {
        //        return "";
        //    }
        //    string flv_img = System.IO.Path.ChangeExtension(Server.MapPath(imgFile), ".jpg");
        //    string flv_file = System.IO.Path.ChangeExtension(Server.MapPath(playFile), ".flv");
        //    string FlvImgSize = VideoConvert.sizeOfImg;

        //    System.Diagnostics.ProcessStartInfo ImgstartInfo = new System.Diagnostics.ProcessStartInfo(ffmpeg);
        //    ImgstartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        //    ImgstartInfo.Arguments = "   -i   " + fileName + "   -y   -f   image2   -t   0.001   -s   " + FlvImgSize + "   " + flv_img;

        //    System.Diagnostics.ProcessStartInfo FilestartInfo = new System.Diagnostics.ProcessStartInfo(ffmpeg);
        //    FilestartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        //    FilestartInfo.Arguments = " -i " + fileName + " -ab 56 -ar 22050 -b 500 -r 15 -s " + widthOfFile + "x" + heightOfFile + " " + flv_file;
        //    try
        //    {
        //        System.Diagnostics.Process.Start(FilestartInfo);
        //        System.Diagnostics.Process.Start(ImgstartInfo);
        //    }
        //    catch
        //    {
        //        return "";
        //    }

        //    ///注意:图片截取成功后,数据由内存缓存写到磁盘需要时间较长,大概在3,4秒甚至更长;   
        //    ///这儿需要延时后再检测,我服务器延时8秒,即如果超过8秒图片仍不存在,认为截图失败;    
        //    if (System.IO.File.Exists(flv_img))
        //    {
        //        return flv_img;
        //    }
        //    return "";
        //}
        //#endregion

        //#region 运行mencoder的视频解码器转换(绝对路径)
        ///// <summary>
        ///// 运行mencoder的视频解码器转换
        ///// </summary>
        //public string MChangeFilePhy(string vFileName, string playFile, string imgFile)
        //{
        //    string tool = Server.MapPath(VideoConvert.mencodertool);
        //    if ((!System.IO.File.Exists(tool)) || (!System.IO.File.Exists(vFileName)))
        //    {
        //        return "";
        //    }
        //    string flv_file = System.IO.Path.ChangeExtension(playFile, ".flv");
        //    string FlvImgSize = VideoConvert.sizeOfImg;
        //    System.Diagnostics.ProcessStartInfo FilestartInfo = new System.Diagnostics.ProcessStartInfo(tool);
        //    FilestartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        //    FilestartInfo.Arguments = " " + vFileName + " -o " + flv_file + " -of lavf -lavfopts i_certify_that_my_video_stream_does_not_use_b_frames -oac mp3lame -lameopts abr:br=56 -ovc lavc -lavcopts vcodec=flv:vbitrate=200:mbd=2:mv0:trell:v4mv:cbp:last_pred=1:dia=-1:cmp=0:vb_strategy=1 -vf scale=" + widthOfFile + ":" + heightOfFile + " -ofps 12 -srate 22050";
        //    try
        //    {
        //        System.Diagnostics.Process.Start(FilestartInfo);
        //        CatchImg(flv_file, imgFile);
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //    return "";
        //}
        //#endregion

        #endregion
    }
}
