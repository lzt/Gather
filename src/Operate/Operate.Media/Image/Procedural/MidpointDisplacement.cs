﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;
using System.Drawing;

#endregion

namespace Operate.Media.Image.Procedural
{
    /// <summary>
    /// Helper class for generating cracks by midpoint displacement
    /// </summary>
    public static class MidpointDisplacement
    {
        #region Functions

        /// <summary>
        /// Generates an image that contains cracks
        /// </summary>
        /// <param name="width">Image width</param>
        /// <param name="height">Image height</param>
        /// <param name="numberOfCracks">Number of cracks</param>
        /// <param name="iterations">Iterations</param>
        /// <param name="maxChange">Maximum height change of the cracks</param>
        /// <param name="maxLength">Maximum length of the cracks</param>
        /// <param name="seed">Random seed</param>
        /// <returns>An image containing "cracks"</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Generate(int width,int height,int numberOfCracks,int iterations,
            int maxChange,int maxLength,int seed)
        {
            var returnValue = new Bitmap(width, height);
            IEnumerable<Line> lines = GenerateLines(width,height,numberOfCracks, iterations, maxChange, maxLength, seed);
            using (Graphics returnGraphic = Graphics.FromImage(returnValue))
            {
                foreach (Line line in lines)
                {
                    foreach (Line subLine in line.SubLines)
                    {
                        returnGraphic.DrawLine(Pens.White, subLine.X1, subLine.Y1, subLine.X2, subLine.Y2);
                    }
                }
            }
            return returnValue;
        }

        private static IEnumerable<Line> GenerateLines(int width,int height,int numberOfCracks, int iterations, int maxChange, int maxLength, int seed)
        {
            var lines = new List<Line>();
            var generator = new System.Random(seed);
            for (int x = 0; x < numberOfCracks; ++x)
            {
                Line tempLine;
                int lineLength;
                do
                {
                    tempLine = new Line(generator.Next(0, width), generator.Next(0, width),
                        generator.Next(0, height), generator.Next(0, height));
                    lineLength = (int)System.Math.Sqrt((double)((tempLine.X1 - tempLine.X2) * (tempLine.X1 - tempLine.X2))
                        + ((tempLine.Y1 - tempLine.Y2) * (tempLine.Y1 - tempLine.Y2)));
                } while (lineLength > maxLength&&lineLength<=0);
                lines.Add(tempLine);
                var tempLineList = new List<Line> {tempLine};
                for (int y = 0; y < iterations; ++y)
                {
                    Line lineUsing=tempLineList[generator.Next(0,tempLineList.Count)];
                    int xBreak=generator.Next(lineUsing.X1,lineUsing.X2)+generator.Next(-maxChange,maxChange);
                    int yBreak;
                    if (lineUsing.Y1 > lineUsing.Y2)
                    {
                        yBreak = generator.Next(lineUsing.Y2, lineUsing.Y1) + generator.Next(-maxChange, maxChange);
                    }
                    else
                    {
                        yBreak = generator.Next(lineUsing.Y1, lineUsing.Y2) + generator.Next(-maxChange, maxChange);
                    }
                    var lineA=new Line(lineUsing.X1,xBreak,lineUsing.Y1,yBreak);
                    var lineB=new Line(xBreak,lineUsing.X2,yBreak,lineUsing.Y2);
                    tempLineList.Remove(lineUsing);
                    tempLineList.Add(lineA);
                    tempLineList.Add(lineB);
                }
                tempLine.SubLines=tempLineList;
            }
            return lines;
        }

        #endregion
    }

    #region Internal classes

    internal class Line
    {
        public Line(){}

        public Line(int x1,int x2,int y1,int y2)
        {
            if (x1 > x2)
            {
                int holder = x1;
                x1 = x2;
                x2 = holder;
                holder = y1;
                y1 = y2;
                y2 = holder;
            }
            X1=x1;
            X2=x2;
            Y1=y1;
            Y2=y2;
        }

        public int X1;
        public int X2;
        public int Y1;
        public int Y2;
        public List<Line> SubLines = new List<Line>();
    }

    #endregion
}