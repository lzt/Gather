﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Drawing;
using System.Drawing.Imaging;
using Operate.ExtensionMethods;
using Operate.Media.Image.ExtensionMethods;
#endregion

namespace Operate.Media.Image.Procedural
{
    /// <summary>
    /// Helper class for doing fault formations
    /// </summary>
    public static class FaultFormation
    {
        #region Functions
        
        /// <summary>
        /// Generates a number of faults, returning an image
        /// </summary>
        /// <param name="width">Width of the resulting image</param>
        /// <param name="height">Height of the resulting image</param>
        /// <param name="numberFaults">Number of faults</param>
        /// <param name="seed">Random seed</param>
        /// <returns>An image from the resulting faults</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body")]
        public static Bitmap Generate(int width,int height,int numberFaults,int seed)
        {
            var heights = new float[width, height];
            float increaseVal = 0.1f;
            var generator = new System.Random(seed);
            for (int x = 0; x < numberFaults; ++x)
            {
                increaseVal = GenerateFault(width, height, numberFaults, heights, increaseVal, generator);
            }
            var returnValue = new Bitmap(width, height);
            BitmapData imageData = returnValue.LockImage();
            int imagePixelSize = imageData.GetPixelSize();
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    float value = heights[x, y];
                    value = (value * 0.5f) + 0.5f;
                    value *= 255;
                    int rgbValue = ((int)value).Clamp(255, 0);
                    imageData.SetPixel(x, y, Color.FromArgb(rgbValue, rgbValue, rgbValue), imagePixelSize);
                }
            }
            returnValue.UnlockImage(imageData);
            return returnValue;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "3#")]
        private static float GenerateFault(int width, int height, int numberFaults, float[,] heights, float increaseVal, System.Random generator)
        {
            int wall = 0;
            int wall2 = 0;
            while (wall == wall2)
            {
                wall = generator.Next(4);
                wall2 = generator.Next(4);
            }
            int x1 = 0;
            int y1 = 0;
            int x2 = 0;
            int y2 = 0;
            while (x1 == x2 || y1 == y2)
            {
                if (wall == 0)
                {
                    x1 = generator.Next(width);
                    y1 = 0;
                }
                else if (wall == 1)
                {
                    y1 = generator.Next(height);
                    x1 = width;
                }
                else if (wall == 2)
                {
                    x1 = generator.Next(width);
                    y1 = height;
                }
                else
                {
                    x1 = 0;
                    y1 = generator.Next(height);
                }

                if (wall2 == 0)
                {
                    x2 = generator.Next(width);
                    y2 = 0;
                }
                else if (wall2 == 1)
                {
                    y2 = generator.Next(height);
                    x2 = width;
                }
                else if (wall2 == 2)
                {
                    x2 = generator.Next(width);
                    y2 = height;
                }
                else
                {
                    x2 = 0;
                    y2 = generator.Next(height);
                }
            }
            int m = (y1 - y2) / (x1 - x2);
            int b = y1 - (m * x1);
            int side = generator.Next(2);
            int direction = 0;
            while (direction == 0)
                direction = generator.Next(-1, 2);
            float tempIncreaseVal = (float)generator.NextDouble() * increaseVal * direction;
            if (side == 0)
            {
                for (int y = 0; y < width; ++y)
                {
                    int lastY = (m * y) + b;
                    for (int z = 0; z < lastY; ++z)
                    {
                        if (z < height)
                        {
                            heights[y, z] += tempIncreaseVal;
                            if (heights[y, z] > 1.0f)
                                heights[y, z] = 1.0f;
                            else if (heights[y, z] < -1.0f)
                                heights[y, z] = -1.0f;
                        }
                    }
                }
            }
            else
            {
                for (int y = 0; y < width; ++y)
                {
                    int lastY = (m * y) + b;
                    if (lastY < 0)
                        lastY = 0;
                    for (int z = lastY; z < height; ++z)
                    {
                        heights[y, z] += tempIncreaseVal;
                        if (heights[y, z] > 1.0f)
                            heights[y, z] = 1.0f;
                        else if (heights[y, z] < -1.0f)
                            heights[y, z] = -1.0f;
                    }
                }
            }
            increaseVal -= (0.1f / numberFaults);
            return increaseVal;
        }

        #endregion
    }
}