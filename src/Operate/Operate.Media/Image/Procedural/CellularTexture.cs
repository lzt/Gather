﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Drawing;
using System.Drawing.Imaging;
using Operate.ExtensionMethods;
using Operate.Media.Image.ExtensionMethods;
#endregion

namespace Operate.Media.Image.Procedural
{
    /// <summary>
    /// Cellular texture helper
    /// </summary>
    public static class CellularTexture
    {
        #region Functions

        /// <summary>
        /// Generates a cellular texture image
        /// </summary>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        /// <param name="numberOfPoints">Number of points</param>
        /// <param name="seed">Random seed</param>
        /// <returns>Returns an image of a cellular texture</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body")]
        public static Bitmap Generate(int width, int height, int numberOfPoints, int seed)
        {
            var map = new CellularMap(seed, width, height, numberOfPoints);
            float maxDistance = map.MaxDistance;
            float minimumDistance = map.MinDistance;
            float[,] distanceBuffer = map.Distances;
            var returnValue = new Bitmap(width, height);
            BitmapData imageData = returnValue.LockImage();
            int imagePixelSize = imageData.GetPixelSize();
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    float value = GetHeight(x, y, distanceBuffer, minimumDistance, maxDistance);
                    value *= 255;
                    int rgbValue = ((int)value).Clamp(255, 0);
                    imageData.SetPixel(x, y, Color.FromArgb(rgbValue, rgbValue, rgbValue), imagePixelSize);
                }
            }
            returnValue.UnlockImage(imageData);
            return returnValue;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "2#")]
        private static float GetHeight(float x, float y, float[,] distanceBuffer,
            float minimumDistance, float maxDistance)
        {
            return (distanceBuffer[(int)x,(int)y] - minimumDistance) / (maxDistance - minimumDistance);
        }

        #endregion
    }
}