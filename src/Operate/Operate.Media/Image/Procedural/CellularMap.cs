﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;

#endregion

namespace Operate.Media.Image.Procedural
{
    /// <summary>
    /// A cellular map creator
    /// </summary>
    public class CellularMap
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="seed">Seed for random generation</param>
        /// <param name="width">Width of the image</param>
        /// <param name="height">Height of the image</param>
        /// <param name="numberOfPoints">Number of cells</param>
        public CellularMap(int seed, int width, int height, int numberOfPoints)
        {
            _width = width;
            _height = height;
            MinDistance = float.MaxValue;
            MaxDistance = float.MinValue;
            var rand = new Random.Random(seed);
            Distances = new float[width,height];
            ClosestPoint = new int[width,height];
            for (int x = 0; x < numberOfPoints; ++x)
            {
                var tempPoint = new Point {X = rand.Next(0, width), Y = rand.Next(0, height)};
                _points.Add(tempPoint);
            }
            CalculateDistances();
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Calculate the distance between the points
        /// </summary>
        private void CalculateDistances()
        {
            for (int x = 0; x < _width; ++x)
            {
                for (int y = 0; y < _height; ++y)
                {
                    FindClosestPoint(x, y);
                }
            }
        }

        /// <summary>
        /// Finds the closest cell center
        /// </summary>
        /// <param name="x">x axis</param>
        /// <param name="y">y axis</param>
        private void FindClosestPoint(int x, int y)
        {
            float maxDistance = float.MaxValue;
            int index = -1;
            for (int z = 0; z < _points.Count; ++z)
            {
                var distance = (float)System.Math.Sqrt(((_points[z].X - x) * (_points[z].X - x)) + ((_points[z].Y - y) * (_points[z].Y - y)));
                if (distance < maxDistance)
                {
                    maxDistance = distance;
                    index = z;
                }
            }
            ClosestPoint[x,y] = index;
            Distances[x,y] = maxDistance;
            if (maxDistance < MinDistance)
                MinDistance = maxDistance;
            if (maxDistance > MaxDistance)
                MaxDistance = maxDistance;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// List of closest cells
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        public virtual int[,] ClosestPoint { get; set; }

        /// <summary>
        /// Distances to the closest cell
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        public virtual float[,] Distances { get; set; }

        /// <summary>
        /// Minimum distance to a point
        /// </summary>
        public virtual float MinDistance { get; set; }

        /// <summary>
        /// Maximum distance to a point
        /// </summary>
        public virtual float MaxDistance { get; set; }

        #endregion

        #region Private Properties
        private readonly List<Point> _points = new List<Point>();
        private readonly int _width;
        private readonly int _height;
        #endregion
    }

    #region Internal Classes

    /// <summary>
    /// Individual point
    /// </summary>
    internal class Point
    {
        #region Constructor

        #endregion

        #region Public Properties

        /// <summary>
        /// X axis
        /// </summary>
        public virtual int X { get; set; }

        /// <summary>
        /// Y axis
        /// </summary>
        public virtual int Y { get; set; }

        #endregion
    }

    #endregion
}