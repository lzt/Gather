﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Drawing;
using System.Drawing.Imaging;
using Operate.ExtensionMethods;
using Operate.Media.Image.ExtensionMethods;
#endregion

namespace Operate.Media.Image.Procedural
{
    /// <summary>
    /// Perlin noise helper class
    /// </summary>
    public static class PerlinNoise
    {
        #region Functions

        /// <summary>
        /// Generates perlin noise
        /// </summary>
        /// <param name="width">Width of the resulting image</param>
        /// <param name="height">Height of the resulting image</param>
        /// <param name="maxRGBValue">MaxRGBValue</param>
        /// <param name="minRGBValue">MinRGBValue</param>
        /// <param name="frequency">Frequency</param>
        /// <param name="amplitude">Amplitude</param>
        /// <param name="persistance">Persistance</param>
        /// <param name="octaves">Octaves</param>
        /// <param name="seed">Random seed</param>
        /// <returns>An image containing perlin noise</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Generate(int width, int height, int maxRGBValue, int minRGBValue,
            float frequency, float amplitude, float persistance, int octaves, int seed)
        {
            var returnValue = new Bitmap(width, height);
            BitmapData imageData = returnValue.LockImage();
            int imagePixelSize = imageData.GetPixelSize();
            var noise = GenerateNoise(seed, width, height);
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    float value = GetValue(x, y, width, height, frequency, amplitude, persistance, octaves, noise);
                    value = (value * 0.5f) + 0.5f;
                    value *= 255;
                    int rgbValue = ((int)value).Clamp(maxRGBValue, minRGBValue);
                    imageData.SetPixel(x, y, Color.FromArgb(rgbValue, rgbValue, rgbValue), imagePixelSize);
                }
            }
            returnValue.UnlockImage(imageData);
            return returnValue;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "8#")]
        private static float GetValue(int x, int y, int width, int height, float frequency, float amplitude,
            float persistance, int octaves, float[,] noise)
        {
            float finalValue = 0.0f;
            for (int i = 0; i < octaves; ++i)
            {
                finalValue += GetSmoothNoise(x * frequency, y * frequency, width, height, noise) * amplitude;
                frequency *= 2.0f;
                amplitude *= persistance;
            }
            finalValue = finalValue.Clamp(1.0f, -1.0f);
            return finalValue;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "4#")]
        private static float GetSmoothNoise(float x, float y, int width, int height, float[,] noise)
        {
            float fractionX = x - (int)x;
            float fractionY = y - (int)y;
            int x1 = ((int)x + width) % width;
            int y1 = ((int)y + height) % height;
            int x2 = ((int)x + width - 1) % width;
            int y2 = ((int)y + height - 1) % height;

            float finalValue = 0.0f;
            finalValue += fractionX * fractionY * noise[x1, y1];
            finalValue += fractionX * (1 - fractionY) * noise[x1, y2];
            finalValue += (1 - fractionX) * fractionY * noise[x2, y1];
            finalValue += (1 - fractionX) * (1 - fractionY) * noise[x2, y2];

            return finalValue;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return")]
        private static float[,] GenerateNoise(int seed, int width, int height)
        {
            var noise = new float[width, height];
            var randomGenerator = new System.Random(seed);
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    noise[x, y] = ((float)(randomGenerator.NextDouble()) - 0.5f) * 2.0f;
                }
            }
            return noise;
        }

        #endregion
    }
}