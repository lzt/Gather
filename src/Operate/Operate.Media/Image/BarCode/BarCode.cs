﻿/*
Copyright (c) 2010 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using Operate.Media.Image.BarCode.Interfaces;
using Operate.Media.Image.BarCode.Symbologies;
using System.Drawing;
using System.Drawing.Drawing2D;
#endregion

namespace Operate.Media.Image.BarCode
{
    /// <summary>
    /// Class for generating bar codes
    /// </summary>
    public class BarCode
    {
        #region Public Static Functions

        /// <summary>
        /// Encodes and writes a bar code to an image
        /// </summary>
        /// <param name="data">Data string to encode</param>
        /// <param name="width">Desired width of the image</param>
        /// <param name="height">Desired height of the image</param>
        /// <param name="encodingType">Encoding type to use</param>
        /// <returns>An image of a bar code corresponding to the data</returns>
        public static Bitmap Encode(string data, int width, int height, Enums.Type encodingType)
        {
            if (string.IsNullOrEmpty(data))
                throw new ArgumentNullException("Data" + " can't be empty/null");

            string encodedData = GetSymbology(data, encodingType);
            return GenerateImage(encodingType, width, height, encodedData);
        }

        #endregion

        #region Private Static Functions

        private static Bitmap GenerateImage(Enums.Type encodingType, int width, int height, string encodedValue)
        {
            var returnValue = new Bitmap(width, height);
            int barWidth;
            int shiftAdjustment;

            if (encodingType == Enums.Type.ITF14)
            {
                var bearerWidth = (int)(width / 12.05);
                int quietZone = Convert.ToInt32(width * 0.05);
                barWidth = (width - (bearerWidth * 2) - (quietZone * 2)) / encodedValue.Length;
                shiftAdjustment = ((width - (bearerWidth * 2) - (quietZone * 2)) % encodedValue.Length) / 2;

                if (barWidth <= 0 || quietZone <= 0)
                    throw new Exception("Image size is too small");

                using (Graphics graphics = Graphics.FromImage(returnValue))
                {
                    graphics.Clear(Color.White);
                    using (var pen = new Pen(Color.Black, barWidth))
                    {
                        pen.Alignment = PenAlignment.Right;
                        for (int x = 0; x < encodedValue.Length; ++x)
                        {
                            if (encodedValue[x] == '1')
                                graphics.DrawLine(pen,
                                    new Point((x * barWidth) + shiftAdjustment + bearerWidth + quietZone, 0),
                                    new Point((x * barWidth) + shiftAdjustment + bearerWidth + quietZone, height));
                        }
                        // ReSharper disable PossibleLossOfFraction
                        pen.Width = height / 8;
                        // ReSharper restore PossibleLossOfFraction
                        pen.Color = Color.Black;
                        pen.Alignment = PenAlignment.Center;
                        graphics.DrawLine(pen, new Point(0, 0), new Point(width, 0));
                        graphics.DrawLine(pen, new Point(0, height), new Point(width, height));
                        graphics.DrawLine(pen, new Point(0, 0), new Point(0, height));
                        graphics.DrawLine(pen, new Point(width, 0), new Point(width, height));
                    }
                }
                return returnValue;
            }

            barWidth = width / encodedValue.Length;
            int barWidthModifier = 1;

            if (encodingType == Enums.Type.PostNet)
                barWidthModifier = 2;

            shiftAdjustment = (width % encodedValue.Length) / 2;

            if (barWidth <= 0)
                throw new Exception("Image size is too small");

            using (Graphics graphics = Graphics.FromImage(returnValue))
            {
                graphics.Clear(Color.White);
                // ReSharper disable PossibleLossOfFraction
                using (var background = new Pen(Color.White, barWidth / barWidthModifier))
                // ReSharper restore PossibleLossOfFraction
                {
                    // ReSharper disable PossibleLossOfFraction
                    using (var foreground = new Pen(Color.Black, barWidth / barWidthModifier))
                    // ReSharper restore PossibleLossOfFraction
                    {
                        for (int x = 0; x < encodedValue.Length; ++x)
                        {
                            if (encodingType == Enums.Type.PostNet)
                            {
                                if (encodedValue[x] != '1')
                                    graphics.DrawLine(foreground,
                                        new Point(x * barWidth + shiftAdjustment + 1, height),
                                        new Point(x * barWidth + shiftAdjustment + 1, height / 2));
                                graphics.DrawLine(background,
                                    new Point(x * (barWidth * barWidthModifier) + shiftAdjustment + barWidth + 1, 0),
                                    new Point(x * (barWidth * barWidthModifier) + shiftAdjustment + barWidth + 1, height));
                            }

                            if (encodedValue[x] == '1')
                                graphics.DrawLine(foreground,
                                    new Point(x * barWidth + shiftAdjustment + 1, 0),
                                    new Point(x * barWidth + shiftAdjustment + 1, height));
                        }
                    }
                }
            }
            return returnValue;
        }

        private static string GetSymbology(string data, Enums.Type encodingType)
        {
            ISymbology symbologyUsing = null;
            switch (encodingType)
            {
                case Enums.Type.UCC12:
                case Enums.Type.UPCA:
                    symbologyUsing = new UPCA(data);
                    break;
                case Enums.Type.UCC13:
                case Enums.Type.EAN13:
                    symbologyUsing = new EAN13(data);
                    break;
                case Enums.Type.Interleaved2Of5:
                    symbologyUsing = new Interleaved2of5(data);
                    break;
                case Enums.Type.Industrial2Of5:
                case Enums.Type.Standard2Of5:
                    symbologyUsing = new Standard2of5(data);
                    break;
                case Enums.Type.LOGMARS:
                case Enums.Type.CODE39:
                    symbologyUsing = new Code39(data);
                    break;
                case Enums.Type.CODE39Extended:
                    symbologyUsing = new Code39(data);
                    break;
                case Enums.Type.Codabar:
                    symbologyUsing = new Codabar(data);
                    break;
                case Enums.Type.PostNet:
                    symbologyUsing = new Postnet(data);
                    break;
                case Enums.Type.ISBN:
                case Enums.Type.BOOKLAND:
                    symbologyUsing = new ISBN(data);
                    break;
                case Enums.Type.JAN13:
                    symbologyUsing = new JAN13(data);
                    break;
                case Enums.Type.UPC_SUPPLEMENTAL_2DIGIT:
                    symbologyUsing = new UPCSupplement2(data);
                    break;
                case Enums.Type.MSI_Mod10:
                case Enums.Type.MSI_2Mod10:
                case Enums.Type.MSI_Mod11:
                case Enums.Type.MSI_Mod11_Mod10:
                case Enums.Type.ModifiedPlessey:
                    symbologyUsing = new MSI(data);
                    break;
                case Enums.Type.UPC_SUPPLEMENTAL_5DIGIT:
                    symbologyUsing = new UPCSupplement5(data);
                    break;
                case Enums.Type.UPCE:
                    symbologyUsing = new UPCE(data);
                    break;
                case Enums.Type.EAN8:
                    symbologyUsing = new EAN8(data);
                    break;
                case Enums.Type.USD8:
                case Enums.Type.CODE11:
                    symbologyUsing = new Code11(data);
                    break;
                case Enums.Type.CODE128:
                    symbologyUsing = new Code128(data);
                    break;
                case Enums.Type.CODE128A:
                    symbologyUsing = new Code128A(data);
                    break;
                case Enums.Type.CODE128B:
                    symbologyUsing = new Code128B(data);
                    break;
                case Enums.Type.CODE128C:
                    symbologyUsing = new Code128C(data);
                    break;
                case Enums.Type.ITF14:
                    symbologyUsing = new ITF14(data);
                    break;
                case Enums.Type.CODE93:
                    symbologyUsing = new Code93(data);
                    break;
                case Enums.Type.TELEPEN:
                    symbologyUsing = new Telepen(data);
                    break;
            }
            return symbologyUsing==null?null:symbologyUsing.Encode();
        }

        #endregion

        #region Properties

        /// <summary>
        /// bar code encoding
        /// </summary>
        protected Enums.Type Encoding { get; set; }

        /// <summary>
        /// Symbology using
        /// </summary>
        protected ISymbology Symbology { get; set; }

        /// <summary>
        /// The string to encode
        /// </summary>
        protected string Data { get; set; }

        #endregion
    }
}