﻿/*
Copyright (c) 2010 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Globalization;
using Operate.Media.Image.BarCode.Interfaces;
#endregion

namespace Operate.Media.Image.BarCode.Symbologies
{
    /// <summary>
    /// Code 11
    /// </summary>
    public class Code11 : ISymbology
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data">Input data</param>
        public Code11(string data)
        {
            Input = data;
        }

        #endregion

        #region Functions

        public string Encode()
        {
            int weight = 1;
            int total = 0;
            string returnValue = Input;

            for (int x = Input.Length - 1; x >= 0; --x)
            {
                if (weight == 10)
                    weight = 1;
                if (Input[x] != '-')
                    total += Int32.Parse(Input[x].ToString(CultureInfo.InvariantCulture)) * weight++;
                else
                    total += 10 * weight++;
            }
            int checkSum = total % 11;
            returnValue += checkSum.ToString(CultureInfo.InvariantCulture);

            if (Input.Length >= 1)
            {
                weight = 1;
                total = 0;
                for (int x = returnValue.Length - 1; x >= 0; x--)
                {
                    if (weight == 9)
                        weight = 1;
                    if (returnValue[x] != '-')
                        total += Int32.Parse(returnValue[x].ToString(CultureInfo.InvariantCulture)) * weight++;
                    else
                        total += 10 * weight++;
                }
                checkSum = total % 11;
                returnValue += checkSum.ToString(CultureInfo.InvariantCulture);
            }

            const string space = "0";
            string finalResult = _codes[11] + space;

            foreach (char Char in returnValue)
            {
                int index = (Char == '-' ? 10 : Int32.Parse(Char.ToString(CultureInfo.InvariantCulture)));
                finalResult += _codes[index];
                finalResult += space;
            }
            finalResult += _codes[11];
            return finalResult;
        }

        #endregion

        #region Properties

        public string Input { get; set; }

        #endregion

        #region Variables

        private readonly string[] _codes = { "101011", "1101011", "1001011", "1100101", "1011011", "1101101", "1001101", "1010011", "1101001", "110101", "101101", "1011001" };

        #endregion
    }
}