﻿/*
Copyright (c) 2010 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Globalization;
using Operate.Media.Image.BarCode.Interfaces;
#endregion

namespace Operate.Media.Image.BarCode.Symbologies
{
    /// <summary>
    /// Codabar
    /// </summary>
    public class Codabar : ISymbology
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data">Data to encode</param>
        public Codabar(string data)
        {
            Init();
            Input = data.Trim();
        }

        #endregion

        #region Variables
        private readonly System.Collections.Hashtable _table = new System.Collections.Hashtable();
        #endregion

        #region Properties

        public string Input { get; set; }

        #endregion

        #region Functions

        public string Encode()
        {
            if (Input.Length < 2)
                throw new Exception("Input too short");

            switch (Input[0].ToString(CultureInfo.InvariantCulture).ToUpper())
            {
                case "A": break;
                case "B": break;
                case "C": break;
                case "D": break;
                default: throw new Exception("Invalid starting character");
            }

            switch (Input[Input.Trim().Length - 1].ToString(CultureInfo.InvariantCulture).ToUpper())
            {
                case "A": break;
                case "B": break;
                case "C": break;
                case "D": break;
                default: throw new Exception("Invalid ending character");
            }

            string result = "";
            foreach (char Char in Input)
            {
                result += _table[Char];
                result += "0";
            }
            return result.Remove(result.Length - 1);
        }

        private void Init()
        {
            _table.Clear();
            _table.Add('0', "101010011");
            _table.Add('1', "101011001");
            _table.Add('2', "101001011");
            _table.Add('3', "110010101");
            _table.Add('4', "101101001");
            _table.Add('5', "110101001");
            _table.Add('6', "100101011");
            _table.Add('7', "100101101");
            _table.Add('8', "100110101");
            _table.Add('9', "110100101");
            _table.Add('-', "101001101");
            _table.Add('$', "101100101");
            _table.Add(':', "1101011011");
            _table.Add('/', "1101101011");
            _table.Add('.', "1101101101");
            _table.Add('+', "101100110011");
            _table.Add('A', "1011001001");
            _table.Add('B', "1010010011");
            _table.Add('C', "1001001011");
            _table.Add('D', "1010011001");
            _table.Add('a', "1011001001");
            _table.Add('b', "1010010011");
            _table.Add('c', "1001001011");
            _table.Add('d', "1010011001");
        }

        #endregion
    }
}