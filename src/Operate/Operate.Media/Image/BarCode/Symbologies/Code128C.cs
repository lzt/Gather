﻿using System;
using Operate.Media.Image.BarCode.Interfaces;

namespace Operate.Media.Image.BarCode.Symbologies
{
    public class Code128C : ISymbology
    {
        public string Data { get; private set; }

        public Code128C(string data)
        {
            Data = data;
        }

        public string Encode()
        {
            throw new NotImplementedException();
        }

        public string Input
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
