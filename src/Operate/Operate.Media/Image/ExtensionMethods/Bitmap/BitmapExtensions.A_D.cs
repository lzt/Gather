﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.Math.ExtensionMethods;

#endregion

// ReSharper disable CheckNamespace
namespace Operate.Media.Image.ExtensionMethods
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Image extensions
    /// </summary>
    public static partial class BitmapExtensions
    {
        #region Functions

        #region AddNoise

        /// <summary>
        /// adds noise to the image
        /// </summary>
        /// <param name="originalImage">Image to add noise to</param>
        /// <param name="fileName">Location to save the image to (optional)</param>
        /// <param name="amount">Amount of noise to add (defaults to 10)</param>
        /// <returns>New image object with the noise added</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap AddNoise(this Bitmap originalImage, int amount = 10, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int height = newBitmap.Height;
            int width = newBitmap.Width;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color currentPixel = oldData.GetPixel(x, y, oldPixelSize);
                    int r = currentPixel.R + Random.Random.ThreadSafeNext(-amount, amount + 1);
                    int g = currentPixel.G + Random.Random.ThreadSafeNext(-amount, amount + 1);
                    int b = currentPixel.B + Random.Random.ThreadSafeNext(-amount, amount + 1);
                    r = r.Clamp(255, 0);
                    g = g.Clamp(255, 0);
                    b = b.Clamp(255, 0);
                    Color tempValue = Color.FromArgb(r, g, b);
                    newData.SetPixel(x, y, tempValue, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region AdjustBrightness

        /// <summary>
        /// Adjusts the brightness
        /// </summary>
        /// <param name="image">Image to change</param>
        /// <param name="fileName">File to save to</param>
        /// <param name="value">-255 to 255</param>
        /// <returns>A bitmap object</returns>
        public static Bitmap AdjustBrightness(this Bitmap image, int value = 0, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            float finalValue = value / 255.0f;
            var tempMatrix = new ColorMatrix
            {
                Matrix = new[]
                        {
                            new float[] {1, 0, 0, 0, 0},
                            new float[] {0, 1, 0, 0, 0},
                            new float[] {0, 0, 1, 0, 0},
                            new float[] {0, 0, 0, 1, 0},
                            new[] {finalValue, finalValue, finalValue, 1, 1}
                        }
            };
            Bitmap newBitmap = tempMatrix.Apply(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region AdjustContrast

        /// <summary>
        /// Adjusts the Contrast
        /// </summary>
        /// <param name="originalImage">Image to change</param>
        /// <param name="value">Used to set the contrast (-100 to 100)</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap AdjustContrast(this Bitmap originalImage, float value = 0, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            value = (100.0f + value) / 100.0f;
            value *= value;
            int width = newBitmap.Width;
            int height = newBitmap.Height;

            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color pixel = oldData.GetPixel(x, y, oldPixelSize);
                    float red = pixel.R / 255.0f;
                    float green = pixel.G / 255.0f;
                    float blue = pixel.B / 255.0f;
                    red = (((red - 0.5f) * value) + 0.5f) * 255.0f;
                    green = (((green - 0.5f) * value) + 0.5f) * 255.0f;
                    blue = (((blue - 0.5f) * value) + 0.5f) * 255.0f;
                    newData.SetPixel(x, y,
                        Color.FromArgb(((int)red).Clamp(255, 0),
                        ((int)green).Clamp(255, 0),
                        ((int)blue).Clamp(255, 0)),
                        newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region AdjustGamma

        /// <summary>
        /// Adjusts the Gamma
        /// </summary>
        /// <param name="originalImage">Image to change</param>
        /// <param name="value">Used to build the gamma ramp (usually .2 to 5)</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap AdjustGamma(this Bitmap originalImage, float value = 1.0f, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();

            var ramp = new int[256];
            // ReSharper disable ImplicitlyCapturedClosure
            Parallel.For(0, 256,
                         delegate(int x)
                         { ramp[x] = ((int)((255.0 * System.Math.Pow(x / 255.0, 1.0 / value)) + 0.5)).Clamp(255, 0); });
            // ReSharper restore ImplicitlyCapturedClosure
            int width = newBitmap.Width;
            int height = newBitmap.Height;

            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color pixel = oldData.GetPixel(x, y, oldPixelSize);
                    int red = ramp[pixel.R];
                    int green = ramp[pixel.G];
                    int blue = ramp[pixel.B];
                    newData.SetPixel(x, y, Color.FromArgb(red, green, blue), newPixelSize);
                }
            });

            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region And

        /// <summary>
        /// ands two images
        /// </summary>
        /// <param name="image1">Image to manipulate</param>
        /// <param name="image2">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap And(this Bitmap image1, Bitmap image2, string fileName = "")
        {
            if (image1.IsNull()) { throw new ArgumentNullException("image1"); }
            if (image2.IsNull()) { throw new ArgumentNullException("image2"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image1.Width, image1.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData1 = image1.LockImage();
            BitmapData oldData2 = image2.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize1 = oldData1.GetPixelSize();
            int oldPixelSize2 = oldData2.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color pixel1 = oldData1.GetPixel(x, y, oldPixelSize1);
                    Color pixel2 = oldData2.GetPixel(x, y, oldPixelSize2);
                    newData.SetPixel(x, y,
                        Color.FromArgb(pixel1.R & pixel2.R,
                            pixel1.G & pixel2.G,
                            pixel1.B & pixel2.B),
                        newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            image1.UnlockImage(oldData1);
            image2.UnlockImage(oldData2);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region BlackAndWhite

        /// <summary>
        /// Converts an image to black and white
        /// </summary>
        /// <param name="image">Image to change</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object of the black and white image</returns>
        public static Bitmap BlackAndWhite(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempMatrix = new ColorMatrix
            {
                Matrix = new[]
                        {
                            new[] {.3f, .3f, .3f, 0, 0},
                            new[] {.59f, .59f, .59f, 0, 0},
                            new[] {.11f, .11f, .11f, 0, 0},
                            new float[] {0, 0, 0, 1, 0},
                            new float[] {0, 0, 0, 0, 1}
                        }
            };
            Bitmap newBitmap = tempMatrix.Apply(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region BlueFilter

        /// <summary>
        /// Gets the blue filter for an image
        /// </summary>
        /// <param name="image">Image to change</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        public static Bitmap BlueFilter(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempMatrix = new ColorMatrix
            {
                Matrix = new[]
                        {
                            new float[] {0, 0, 0, 0, 0},
                            new float[] {0, 0, 0, 0, 0},
                            new float[] {0, 0, 1, 0, 0},
                            new float[] {0, 0, 0, 1, 0},
                            new float[] {0, 0, 0, 0, 1}
                        }
            };
            var newBitmap = tempMatrix.Apply(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region BoxBlur

        /// <summary>
        /// Does smoothing using a box blur
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="size">Size of the aperture</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        public static Bitmap BoxBlur(this Bitmap image, int size = 3, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempFilter = new Filter(size, size);
            for (int x = 0; x < size; ++x)
                for (int y = 0; y < size; ++y)
                    tempFilter.MyFilter[x, y] = 1;
            Bitmap newBitmap = tempFilter.ApplyFilter(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region BumpMap

        /// <summary>
        /// Creates the bump map
        /// </summary>
        /// <param name="direction">Direction of the bump map</param>
        /// <param name="image">Image to create a bump map from</param>
        /// <param name="invert">Inverts the direction of the bump map</param>
        /// <returns>The resulting bump map</returns>
        public static Bitmap BumpMap(this Bitmap image, Direction direction = Direction.TopBottom, bool invert = false)
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            var edgeDetectionFilter = new Filter();
            if (direction == Direction.TopBottom)
            {
                if (!invert)
                {
                    edgeDetectionFilter.MyFilter[0, 0] = 1;
                    edgeDetectionFilter.MyFilter[1, 0] = 2;
                    edgeDetectionFilter.MyFilter[2, 0] = 1;
                    edgeDetectionFilter.MyFilter[0, 1] = 0;
                    edgeDetectionFilter.MyFilter[1, 1] = 0;
                    edgeDetectionFilter.MyFilter[2, 1] = 0;
                    edgeDetectionFilter.MyFilter[0, 2] = -1;
                    edgeDetectionFilter.MyFilter[1, 2] = -2;
                    edgeDetectionFilter.MyFilter[2, 2] = -1;
                }
                else
                {
                    edgeDetectionFilter.MyFilter[0, 0] = -1;
                    edgeDetectionFilter.MyFilter[1, 0] = -2;
                    edgeDetectionFilter.MyFilter[2, 0] = -1;
                    edgeDetectionFilter.MyFilter[0, 1] = 0;
                    edgeDetectionFilter.MyFilter[1, 1] = 0;
                    edgeDetectionFilter.MyFilter[2, 1] = 0;
                    edgeDetectionFilter.MyFilter[0, 2] = 1;
                    edgeDetectionFilter.MyFilter[1, 2] = 2;
                    edgeDetectionFilter.MyFilter[2, 2] = 1;
                }
            }
            else
            {
                if (!invert)
                {
                    edgeDetectionFilter.MyFilter[0, 0] = -1;
                    edgeDetectionFilter.MyFilter[0, 1] = -2;
                    edgeDetectionFilter.MyFilter[0, 2] = -1;
                    edgeDetectionFilter.MyFilter[1, 0] = 0;
                    edgeDetectionFilter.MyFilter[1, 1] = 0;
                    edgeDetectionFilter.MyFilter[1, 2] = 0;
                    edgeDetectionFilter.MyFilter[2, 0] = 1;
                    edgeDetectionFilter.MyFilter[2, 1] = 2;
                    edgeDetectionFilter.MyFilter[2, 2] = 1;
                }
                else
                {
                    edgeDetectionFilter.MyFilter[0, 0] = 1;
                    edgeDetectionFilter.MyFilter[0, 1] = 2;
                    edgeDetectionFilter.MyFilter[0, 2] = 1;
                    edgeDetectionFilter.MyFilter[1, 0] = 0;
                    edgeDetectionFilter.MyFilter[1, 1] = 0;
                    edgeDetectionFilter.MyFilter[1, 2] = 0;
                    edgeDetectionFilter.MyFilter[2, 0] = -1;
                    edgeDetectionFilter.MyFilter[2, 1] = -2;
                    edgeDetectionFilter.MyFilter[2, 2] = -1;
                }
            }
            edgeDetectionFilter.Offset = 127;
            using (Bitmap tempImage = edgeDetectionFilter.ApplyFilter(image))
            {
                return tempImage.BlackAndWhite();
            }
        }

        #endregion

        #region Colorize

        /// <summary>
        /// Colorizes a black and white image
        /// </summary>
        /// <param name="originalImage">Black and white image</param>
        /// <param name="colors">Color array to use for the image</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>The colorized image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Colorize(this Bitmap originalImage, Color[] colors, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            if (colors.Length < 256)
                return new Bitmap(1, 1);
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = originalImage.Width;
            int height = originalImage.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    int colorUsing = oldData.GetPixel(x, y, oldPixelSize).R;
                    newData.SetPixel(x, y, colors[colorUsing], newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Crop

        /// <summary>
        /// Crops an image
        /// </summary>
        /// <param name="imageUsing">Image to crop</param>
        /// <param name="width">Width of the cropped image</param>
        /// <param name="height">Height of the cropped image</param>
        /// <param name="offsetx">the Offset of X</param>
        /// <param name="offsety">the Offset of Y</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A Bitmap object of the cropped image</returns>
        public static Bitmap Crop(this Bitmap imageUsing, uint width, uint height, uint offsetx = 0, uint offsety = 0, string fileName = "")
        {
            if (imageUsing.IsNull()) { throw new ArgumentNullException("imageUsing"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            Bitmap tempBitmap = imageUsing;
            var tempRectangle = new Rectangle { Height = (int)height, Width = (int)width };

            if (tempBitmap.Height  > offsety)
            {
                tempRectangle.Y =  (int)offsety;
            }
            else
            {
                tempRectangle.Y =0;
            }

            if (tempBitmap.Width  > offsetx)
            {
                tempRectangle.X = (int)offsetx;
            }
            else
            {
                tempRectangle.X = 0;
            }

            Bitmap newBitmap = tempBitmap.Clone(tempRectangle, tempBitmap.PixelFormat);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        /// <summary>
        /// Crops an image
        /// </summary>
        /// <param name="imageUsing">Image to crop</param>
        /// <param name="width">Width of the cropped image</param>
        /// <param name="height">Height of the cropped image</param>
        /// <param name="vAlignment">The verticle alignment of the cropping (top or bottom)</param>
        /// <param name="hAlignment">The horizontal alignment of the cropping (left or right)</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A Bitmap object of the cropped image</returns>
        public static Bitmap Crop(this Bitmap imageUsing, int width, int height, Align vAlignment, Align hAlignment, string fileName = "")
        {
            if (imageUsing.IsNull()) { throw new ArgumentNullException("imageUsing"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            Bitmap tempBitmap = imageUsing;
            var tempRectangle = new Rectangle { Height = height, Width = width };
            if (vAlignment == Align.Top)
            {
                tempRectangle.Y = 0;
            }
            else
            {
                tempRectangle.Y = tempBitmap.Height - height;
                if (tempRectangle.Y < 0)
                    tempRectangle.Y = 0;
            }
            if (hAlignment == Align.Left)
            {
                tempRectangle.X = 0;
            }
            else
            {
                tempRectangle.X = tempBitmap.Width - width;
                if (tempRectangle.X < 0)
                    tempRectangle.X = 0;
            }
            Bitmap newBitmap = tempBitmap.Clone(tempRectangle, tempBitmap.PixelFormat);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Dilate

        /// <summary>
        /// Does dilation
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="size">Size of the aperture</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A Bitmap object of the resulting image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Dilate(this Bitmap originalImage, int size, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int apetureMin = -(size / 2);
            int apetureMax = (size / 2);
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    int rValue = 0;
                    int gValue = 0;
                    int bValue = 0;
                    for (int x2 = apetureMin; x2 < apetureMax; ++x2)
                    {
                        int tempX = x + x2;
                        if (tempX >= 0 && tempX < width)
                        {
                            for (int y2 = apetureMin; y2 < apetureMax; ++y2)
                            {
                                int tempY = y + y2;
                                if (tempY >= 0 && tempY < height)
                                {
                                    Color tempColor = oldData.GetPixel(tempX, tempY, oldPixelSize);
                                    rValue = rValue.Max(tempColor.R);
                                    gValue = gValue.Max(tempColor.G);
                                    bValue = bValue.Max(tempColor.B);
                                }
                            }
                        }
                    }
                    Color tempPixel = Color.FromArgb(rValue, gValue, bValue);
                    newData.SetPixel(x, y, tempPixel, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Distance

        private static double Distance(int r1, int r2, int g1, int g2, int b1, int b2)
        {
            return ((double)(((r1 - r2) * (r1 - r2)) + ((g1 - g2) * (g1 - g2)) + ((b1 - b2) * (b1 - b2)))).Sqrt();
        }

        #endregion

        #region DrawRoundedRectangle

        /// <summary>
        /// Draws a rounded rectangle on a bitmap
        /// </summary>
        /// <param name="image">Image to draw on</param>
        /// <param name="boxColor">The color that the box should be</param>
        /// <param name="xPosition">The upper right corner's x position</param>
        /// <param name="yPosition">The upper right corner's y position</param>
        /// <param name="height">Height of the box</param>
        /// <param name="width">Width of the box</param>
        /// <param name="cornerRadius">Radius of the corners</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>The bitmap with the rounded box on it</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap DrawRoundedRectangle(this Bitmap image, Color boxColor, int xPosition, int yPosition,
            int height, int width, int cornerRadius, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            if (boxColor.IsNull()) { throw new ArgumentNullException("boxColor"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image, image.Width, image.Height);
            using (Graphics newGraphics = Graphics.FromImage(newBitmap))
            {
                using (var boxPen = new Pen(boxColor))
                {
                    using (var path = new GraphicsPath())
                    {
                        path.AddLine(xPosition + cornerRadius, yPosition, xPosition + width - (cornerRadius * 2), yPosition);
                        path.AddArc(xPosition + width - (cornerRadius * 2), yPosition, cornerRadius * 2, cornerRadius * 2, 270, 90);
                        path.AddLine(xPosition + width, yPosition + cornerRadius, xPosition + width, yPosition + height - (cornerRadius * 2));
                        path.AddArc(xPosition + width - (cornerRadius * 2), yPosition + height - (cornerRadius * 2), cornerRadius * 2, cornerRadius * 2, 0, 90);
                        path.AddLine(xPosition + width - (cornerRadius * 2), yPosition + height, xPosition + cornerRadius, yPosition + height);
                        path.AddArc(xPosition, yPosition + height - (cornerRadius * 2), cornerRadius * 2, cornerRadius * 2, 90, 90);
                        path.AddLine(xPosition, yPosition + height - (cornerRadius * 2), xPosition, yPosition + cornerRadius);
                        path.AddArc(xPosition, yPosition, cornerRadius * 2, cornerRadius * 2, 180, 90);
                        path.CloseFigure();
                        newGraphics.DrawPath(boxPen, path);
                    }
                }
            }
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region DrawText

        /// <summary>
        /// Draws text on an image within the bounding box specified.
        /// </summary>
        /// <param name="image">Image to draw on</param>
        /// <param name="textToDraw">The text to draw on the image</param>
        /// <param name="fontToUse">Font in which to draw the text</param>
        /// <param name="brushUsing">Defines the brush using</param>
        /// <param name="boxToDrawWithin">Rectangle to draw the image within</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object with the text drawn on it</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap DrawText(this Bitmap image, string textToDraw,
            Font fontToUse, Brush brushUsing, RectangleF boxToDrawWithin,
            string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            if (fontToUse.IsNull()) { throw new ArgumentNullException("fontToUse"); }
            if (brushUsing.IsNull()) { throw new ArgumentNullException("brushUsing"); }
            if (boxToDrawWithin.IsNull()) { throw new ArgumentNullException("boxToDrawWithin"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image, image.Width, image.Height);
            using (Graphics tempGraphics = Graphics.FromImage(newBitmap))
            {
                tempGraphics.DrawString(textToDraw, fontToUse, brushUsing, boxToDrawWithin);
            }
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #endregion
    }
}
