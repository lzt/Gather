﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.Math.ExtensionMethods;

#endregion

// ReSharper disable CheckNamespace
namespace Operate.Media.Image.ExtensionMethods
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Image extensions
    /// </summary>
    public static partial class BitmapExtensions
    {
        #region Functions

        #region Jitter

        /// <summary>
        /// Causes a "Jitter" effect
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="maxJitter">Maximum number of pixels the item can move</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Jitter(this Bitmap originalImage, int maxJitter = 5, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage, originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    int newX = Random.Random.ThreadSafeNext(-maxJitter, maxJitter);
                    int newY = Random.Random.ThreadSafeNext(-maxJitter, maxJitter);
                    newX += x;
                    newY += y;
                    newX = newX.Clamp(width - 1, 0);
                    newY = newY.Clamp(height - 1, 0);

                    newData.SetPixel(x, y, oldData.GetPixel(newX, newY, oldPixelSize), newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region KuwaharaBlur

        /// <summary>
        /// Does smoothing using a kuwahara blur
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="size">Size of the aperture</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap KuwaharaBlur(this Bitmap originalImage, int size = 3, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int[] apetureMinX = { -(size / 2), 0, -(size / 2), 0 };
            int[] apetureMaxX = { 0, (size / 2), 0, (size / 2) };
            int[] apetureMinY = { -(size / 2), -(size / 2), 0, 0 };
            int[] apetureMaxY = { 0, 0, (size / 2), (size / 2) };
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    int[] rValues = { 0, 0, 0, 0 };
                    int[] gValues = { 0, 0, 0, 0 };
                    int[] bValues = { 0, 0, 0, 0 };
                    int[] numPixels = { 0, 0, 0, 0 };
                    int[] maxRValue = { 0, 0, 0, 0 };
                    int[] maxGValue = { 0, 0, 0, 0 };
                    int[] maxBValue = { 0, 0, 0, 0 };
                    int[] minRValue = { 255, 255, 255, 255 };
                    int[] minGValue = { 255, 255, 255, 255 };
                    int[] minBValue = { 255, 255, 255, 255 };
                    for (int i = 0; i < 4; ++i)
                    {
                        for (int x2 = apetureMinX[i]; x2 < apetureMaxX[i]; ++x2)
                        {
                            int tempX = x + x2;
                            if (tempX >= 0 && tempX < width)
                            {
                                for (int y2 = apetureMinY[i]; y2 < apetureMaxY[i]; ++y2)
                                {
                                    int tempY = y + y2;
                                    if (tempY >= 0 && tempY < height)
                                    {
                                        Color tempColor = oldData.GetPixel(tempX, tempY, oldPixelSize);
                                        rValues[i] += tempColor.R;
                                        gValues[i] += tempColor.G;
                                        bValues[i] += tempColor.B;
                                        if (tempColor.R > maxRValue[i])
                                            maxRValue[i] = tempColor.R;
                                        else if (tempColor.R < minRValue[i])
                                            minRValue[i] = tempColor.R;

                                        if (tempColor.G > maxGValue[i])
                                            maxGValue[i] = tempColor.G;
                                        else if (tempColor.G < minGValue[i])
                                            minGValue[i] = tempColor.G;

                                        if (tempColor.B > maxBValue[i])
                                            maxBValue[i] = tempColor.B;
                                        else if (tempColor.B < minBValue[i])
                                            minBValue[i] = tempColor.B;

                                        ++numPixels[i];
                                    }
                                }
                            }
                        }
                    }
                    int j = 0;
                    int minDifference = 10000;
                    for (int i = 0; i < 4; ++i)
                    {
                        int currentDifference = (maxRValue[i] - minRValue[i]) + (maxGValue[i] - minGValue[i]) + (maxBValue[i] - minBValue[i]);
                        if (currentDifference < minDifference && numPixels[i] > 0)
                        {
                            j = i;
                            minDifference = currentDifference;
                        }
                    }

                    Color meanPixel = Color.FromArgb(rValues[j] / numPixels[j],
                        gValues[j] / numPixels[j],
                        bValues[j] / numPixels[j]);
                    newData.SetPixel(x, y, meanPixel, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region LaplaceEdgeDetection

        /// <summary>
        /// Laplace edge detection function
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        public static Bitmap LaplaceEdgeDetection(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            using (Bitmap tempImage = image.BlackAndWhite())
            {
                var tempFilter = new Filter(5, 5);
                tempFilter.MyFilter[0, 0] = -1;
                tempFilter.MyFilter[0, 1] = -1;
                tempFilter.MyFilter[0, 2] = -1;
                tempFilter.MyFilter[0, 3] = -1;
                tempFilter.MyFilter[0, 4] = -1;
                tempFilter.MyFilter[1, 0] = -1;
                tempFilter.MyFilter[1, 1] = -1;
                tempFilter.MyFilter[1, 2] = -1;
                tempFilter.MyFilter[1, 3] = -1;
                tempFilter.MyFilter[1, 4] = -1;
                tempFilter.MyFilter[2, 0] = -1;
                tempFilter.MyFilter[2, 1] = -1;
                tempFilter.MyFilter[2, 2] = 24;
                tempFilter.MyFilter[2, 3] = -1;
                tempFilter.MyFilter[2, 4] = -1;
                tempFilter.MyFilter[3, 0] = -1;
                tempFilter.MyFilter[3, 1] = -1;
                tempFilter.MyFilter[3, 2] = -1;
                tempFilter.MyFilter[3, 3] = -1;
                tempFilter.MyFilter[3, 4] = -1;
                tempFilter.MyFilter[4, 0] = -1;
                tempFilter.MyFilter[4, 1] = -1;
                tempFilter.MyFilter[4, 2] = -1;
                tempFilter.MyFilter[4, 3] = -1;
                tempFilter.MyFilter[4, 4] = -1;
                using (Bitmap newImage = tempFilter.ApplyFilter(tempImage))
                {
                    Bitmap newBitmap = newImage.Negative();
                    if (!string.IsNullOrEmpty(fileName))
                        newBitmap.Save(fileName, formatUsing);
                    return newBitmap;
                }
            }
        }

        #endregion

        #region LockImage

        /// <summary>
        /// Locks an image
        /// </summary>
        /// <param name="image">Image to lock</param>
        /// <returns>The bitmap data for the image</returns>
        public static BitmapData LockImage(this Bitmap image)
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            return image.LockBits(new Rectangle(0, 0, image.Width, image.Height),
                ImageLockMode.ReadWrite, image.PixelFormat);
        }

        #endregion

        #region Map

        private static int Map(int value, int min, int max)
        {
            double tempVal = (value - min);
            tempVal /= max - min;
            return ((int)(tempVal * 255)).Clamp(255, 0);
        }

        #endregion

        #region MedianFilter

        /// <summary>
        /// Does smoothing using a median filter
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <param name="size">Size of the aperture</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap MedianFilter(this Bitmap originalImage, int size = 3, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int apetureMin = -(size / 2);
            int apetureMax = (size / 2);
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    var rValues = new List<int>();
                    var gValues = new List<int>();
                    var bValues = new List<int>();
                    for (int x2 = apetureMin; x2 < apetureMax; ++x2)
                    {
                        int tempX = x + x2;
                        if (tempX >= 0 && tempX < width)
                        {
                            for (int y2 = apetureMin; y2 < apetureMax; ++y2)
                            {
                                int tempY = y + y2;
                                if (tempY >= 0 && tempY < height)
                                {
                                    Color tempColor = oldData.GetPixel(tempX, tempY, oldPixelSize);
                                    rValues.Add(tempColor.R);
                                    gValues.Add(tempColor.G);
                                    bValues.Add(tempColor.B);
                                }
                            }
                        }
                    }
                    Color medianPixel = Color.FromArgb(rValues.Median(),
                        gValues.Median(),
                        bValues.Median());
                    newData.SetPixel(x, y, medianPixel, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region MotionDetection

        /// <summary>
        /// Runs a simplistic motion detection algorithm
        /// </summary>
        /// <param name="newImage">The "new" frame</param>
        /// <param name="oldImage">The "old" frame</param>
        /// <param name="threshold">The threshold used to detect changes in the image</param>
        /// <param name="detectionColor">Color to display changes in the images as</param>
        /// <returns>A bitmap indicating where changes between frames have occurred overlayed on top of the new image.</returns>
        public static Bitmap MotionDetection(this Bitmap newImage, Bitmap oldImage, int threshold, Color detectionColor)
        {
            if (newImage.IsNull()) { throw new ArgumentNullException("newImage"); }
            if (oldImage.IsNull()) { throw new ArgumentNullException("oldImage"); }
            using (Bitmap newImage1 = newImage.BlackAndWhite())
            {
                using (Bitmap oldImage1 = oldImage.BlackAndWhite())
                {
                    using (Bitmap newImage2 = newImage1.SNNBlur(5))
                    {
                        using (Bitmap oldImage2 = oldImage1.SNNBlur(5))
                        {
                            using (var outputImage = new Bitmap(newImage2, newImage2.Width, newImage2.Height))
                            {
                                using (var overlay = new Bitmap(newImage, newImage.Width, newImage.Height))
                                {
                                    BitmapData newImage2Data = newImage2.LockImage();
                                    int newImage2PixelSize = newImage2Data.GetPixelSize();
                                    BitmapData oldImage2Data = oldImage2.LockImage();
                                    int oldImage2PixelSize = oldImage2Data.GetPixelSize();
                                    BitmapData overlayData = overlay.LockImage();
                                    int overlayPixelSize = overlayData.GetPixelSize();
                                    int width = outputImage.Width;
                                    int height = outputImage.Height;
                                    int height1 = height;
                                    Parallel.For(0, width, x =>
                                    {
                                        for (int y = 0; y < height1; ++y)
                                        {
                                            Color newPixel = newImage2Data.GetPixel(x, y, newImage2PixelSize);
                                            Color oldPixel = oldImage2Data.GetPixel(x, y, oldImage2PixelSize);
                                            overlayData.SetPixel(x, y,
                                                                 System.Math.Pow(newPixel.R - oldPixel.R, 2.0) >
                                                                 threshold
                                                                     ? Color.FromArgb(100, 0, 100)
                                                                     : Color.FromArgb(200, 0, 200), overlayPixelSize);
                                        }
                                    });
                                    overlay.UnlockImage(overlayData);
                                    newImage2.UnlockImage(newImage2Data);
                                    oldImage2.UnlockImage(oldImage2Data);
                                    using (Bitmap overlay2 = overlay.EdgeDetection(25, detectionColor))
                                    {
                                        BitmapData overlay2Data = overlay2.LockImage();
                                        int overlay2PixelSize = overlay2Data.GetPixelSize();
                                        width = outputImage.Width;
                                        height = outputImage.Height;
                                        Parallel.For(0, width, x =>
                                        {
                                            for (int y = 0; y < height; ++y)
                                            {
                                                Color pixel1 = overlay2Data.GetPixel(x, y, overlay2PixelSize);
                                                if (pixel1.R != detectionColor.R || pixel1.G != detectionColor.G || pixel1.B != detectionColor.B)
                                                {
                                                    overlay2Data.SetPixel(x, y, Color.FromArgb(200, 0, 200), overlay2PixelSize);
                                                }
                                            }
                                        });
                                        overlay2.UnlockImage(overlay2Data);
                                        return outputImage.Watermark(overlay2, 1.0f, 0, 0, Color.FromArgb(200, 0, 200));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Negative

        /// <summary>
        /// gets the negative of the image
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Negative(this Bitmap originalImage, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color currentPixel = oldData.GetPixel(x, y, oldPixelSize);
                    Color tempValue = Color.FromArgb(255 - currentPixel.R, 255 - currentPixel.G, 255 - currentPixel.B);
                    newData.SetPixel(x, y, tempValue, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region NormalMap

        /// <summary>
        /// Creates the normal map
        /// </summary>
        /// <param name="imageUsing">Image to create the normal map from</param>
        /// <param name="invertX">Invert the X direction</param>
        /// <param name="invertY">Invert the Y direction</param>
        /// <returns>Returns the resulting normal map</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap NormalMap(this Bitmap imageUsing, bool invertX = false, bool invertY = false)
        {
            if (imageUsing.IsNull()) { throw new ArgumentNullException("imageUsing"); }
            using (Bitmap tempImageX = imageUsing.BumpMap(Direction.LeftRight, invertX))
            {
                using (Bitmap tempImageY = imageUsing.BumpMap(Direction.TopBottom, invertY))
                {
                    var returnImage = new Bitmap(tempImageX.Width, tempImageX.Height);
                    BitmapData tempImageXData = tempImageX.LockImage();
                    BitmapData tempImageYData = tempImageY.LockImage();
                    BitmapData returnImageData = returnImage.LockImage();
                    int tempImageXPixelSize = tempImageXData.GetPixelSize();
                    int tempImageYPixelSize = tempImageYData.GetPixelSize();
                    int returnImagePixelSize = returnImageData.GetPixelSize();
                    int width = tempImageX.Width;
                    int height = tempImageX.Height;
                    Parallel.For(0, height, y =>
                    {
                        var tempVector = new Math.Vector3(0.0, 0.0, 0.0);
                        for (int x = 0; x < width; ++x)
                        {
                            Color tempPixelX = tempImageXData.GetPixel(x, y, tempImageXPixelSize);
                            Color tempPixelY = tempImageYData.GetPixel(x, y, tempImageYPixelSize);
                            tempVector.X = tempPixelX.R / 255.0;
                            tempVector.Y = tempPixelY.R / 255.0;
                            tempVector.Z = 1.0;
                            tempVector.Normalize();
                            tempVector.X = ((tempVector.X + 1.0) / 2.0) * 255.0;
                            tempVector.Y = ((tempVector.Y + 1.0) / 2.0) * 255.0;
                            tempVector.Z = ((tempVector.Z + 1.0) / 2.0) * 255.0;
                            returnImageData.SetPixel(x, y,
                                Color.FromArgb((int)tempVector.X,
                                    (int)tempVector.Y,
                                    (int)tempVector.Z),
                                returnImagePixelSize);
                        }
                    });
                    tempImageX.UnlockImage(tempImageXData);
                    tempImageY.UnlockImage(tempImageYData);
                    returnImage.UnlockImage(returnImageData);
                    return returnImage;
                }
            }
        }

        #endregion

        #endregion
    }
}
