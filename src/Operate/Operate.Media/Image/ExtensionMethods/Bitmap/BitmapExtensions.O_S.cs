﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.Media.Image.Procedural;

#endregion

// ReSharper disable CheckNamespace
namespace Operate.Media.Image.ExtensionMethods
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Image extensions
    /// </summary>
    public static partial class BitmapExtensions
    {
        #region Functions

        #region Or

        /// <summary>
        /// Ors two images
        /// </summary>
        /// <param name="image1">Image to manipulate</param>
        /// <param name="image2">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Or(this Bitmap image1, Bitmap image2, string fileName = "")
        {
            if (image1.IsNull()) { throw new ArgumentNullException("image1"); }
            if (image2.IsNull()) { throw new ArgumentNullException("image2"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image1.Width, image1.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData1 = image1.LockImage();
            BitmapData oldData2 = image2.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize1 = oldData1.GetPixelSize();
            int oldPixelSize2 = oldData2.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color pixel1 = oldData1.GetPixel(x, y, oldPixelSize1);
                    Color pixel2 = oldData2.GetPixel(x, y, oldPixelSize2);
                    newData.SetPixel(x, y,
                        Color.FromArgb(pixel1.R | pixel2.R,
                            pixel1.G | pixel2.G,
                            pixel1.B | pixel2.B),
                        newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            image1.UnlockImage(oldData1);
            image2.UnlockImage(oldData2);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region OilPainting

        /// <summary>
        /// Slow but interesting function that applies an oil painting effect
        /// </summary>
        /// <param name="image">Image to create the oil painting effect from</param>
        /// <param name="seed">Randomization seed</param>
        /// <param name="numberOfPoints">Number of points for the painting</param>
        /// <returns>The resulting bitmap</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap OilPainting(this Bitmap image, int seed, int numberOfPoints = 100)
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            using (var currImage = new Bitmap(image))
            {
                var map = new CellularMap(seed, image.Width, image.Height, numberOfPoints);
                BitmapData imageData = currImage.LockImage();
                int imagePixelSize = imageData.GetPixelSize();
                int width = currImage.Width;
                int height = currImage.Height;
                Parallel.For(0, numberOfPoints, i =>
                    {
                        int red = 0;
                        int green = 0;
                        int blue = 0;
                        int counter = 0;
                        for (int x = 0; x < width; ++x)
                        {
                            for (int y = 0; y < height; ++y)
                            {
                                if (map.ClosestPoint[x, y] == i)
                                {
                                    Color pixel = imageData.GetPixel(x, y, imagePixelSize);
                                    red += pixel.R;
                                    green += pixel.G;
                                    blue += pixel.B;
                                    ++counter;
                                }
                            }
                        }
                        int counter2 = 0;
                        for (int x = 0; x < width; ++x)
                        {
                            for (int y = 0; y < height; ++y)
                            {
                                if (map.ClosestPoint[x, y] == i)
                                {
                                    imageData.SetPixel(x, y, Color.FromArgb(red / counter, green / counter, blue / counter), imagePixelSize);
                                    ++counter2;
                                    if (counter2 == counter)
                                        break;
                                }
                            }
                            if (counter2 == counter)
                                break;
                        }
                    });
                currImage.UnlockImage(imageData);
                return currImage;
            }
        }

        #endregion

        #region Pixelate

        /// <summary>
        /// Pixelates an image
        /// </summary>
        /// <param name="originalImage">Image to pixelate</param>
        /// <param name="pixelSize">Size of the "pixels" in pixels</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Pixelate(this Bitmap originalImage, int pixelSize = 5, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            for (int x = 0; x < newBitmap.Width; x += (pixelSize / 2))
            {
                int minX = (x - (pixelSize / 2)).Clamp(newBitmap.Width, 0);
                int maxX = (x + (pixelSize / 2)).Clamp(newBitmap.Width, 0);
                for (int y = 0; y < newBitmap.Height; y += (pixelSize / 2))
                {
                    int rValue = 0;
                    int gValue = 0;
                    int bValue = 0;
                    int minY = (y - (pixelSize / 2)).Clamp(newBitmap.Height, 0);
                    int maxY = (y + (pixelSize / 2)).Clamp(newBitmap.Height, 0);
                    for (int x2 = minX; x2 < maxX; ++x2)
                    {
                        for (int y2 = minY; y2 < maxY; ++y2)
                        {
                            Color pixel = oldData.GetPixel(x2, y2, oldPixelSize);
                            rValue += pixel.R;
                            gValue += pixel.G;
                            bValue += pixel.B;
                        }
                    }
                    rValue = rValue / (pixelSize * pixelSize);
                    gValue = gValue / (pixelSize * pixelSize);
                    bValue = bValue / (pixelSize * pixelSize);
                    Color tempPixel = Color.FromArgb(rValue, gValue, bValue);
                    Parallel.For(minX, maxX, x2 =>
                    {
                        for (int y2 = minY; y2 < maxY; ++y2)
                        {
                            newData.SetPixel(x2, y2, tempPixel, newPixelSize);
                        }
                    });
                }
            }
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region RedFilter

        /// <summary>
        /// Gets the Red filter for an image
        /// </summary>
        /// <param name="image">Image to change</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        public static Bitmap RedFilter(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
           var tempMatrix = new ColorMatrix
               {
                   Matrix = new[]
                       {
                           new float[] {1, 0, 0, 0, 0},
                           new float[] {0, 0, 0, 0, 0},
                           new float[] {0, 0, 0, 0, 0},
                           new float[] {0, 0, 0, 1, 0},
                           new float[] {0, 0, 0, 0, 1}
                       }
               };
            Bitmap newBitmap = tempMatrix.Apply(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Resize

        /// <summary>
        /// Resizes an image to a certain height
        /// </summary>
        /// <param name="image">Image to resize</param>
        /// <param name="maxSide">Max height/width for the final image</param>
        /// <param name="quality">Quality of the resizing</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object of the resized image</returns>
        public static Bitmap Resize(this Bitmap image, int maxSide, Quality quality = Quality.Low, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }

            int oldWidth = image.Width;
            int oldHeight = image.Height;
            int oldMaxSide = (oldWidth >= oldHeight) ? oldWidth : oldHeight;

            double coefficient = maxSide / (double)oldMaxSide;
            int newWidth = Convert.ToInt32(coefficient * oldWidth);
            int newHeight = Convert.ToInt32(coefficient * oldHeight);
            if (newWidth <= 0)
                newWidth = 1;
            if (newHeight <= 0)
                newHeight = 1;
            return image.Resize(newWidth, newHeight, quality, fileName);
        }

        /// <summary>
        /// Resizes an image to a certain height
        /// </summary>
        /// <param name="image">Image to resize</param>
        /// <param name="width">New width for the final image</param>
        /// <param name="height">New height for the final image</param>
        /// <param name="quality">Quality of the resizing</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object of the resized image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Resize(this Bitmap image, int width, int height, Quality quality = Quality.Low, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(width, height);
            using (Graphics newGraphics = Graphics.FromImage(newBitmap))
            {
                if (quality == Quality.High)
                {
                    newGraphics.CompositingQuality = CompositingQuality.HighQuality;
                    newGraphics.SmoothingMode = SmoothingMode.HighQuality;
                    newGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                }
                else
                {
                    newGraphics.CompositingQuality = CompositingQuality.HighSpeed;
                    newGraphics.SmoothingMode = SmoothingMode.HighSpeed;
                    newGraphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                }
                newGraphics.DrawImage(image, new Rectangle(0, 0, width, height));
            }
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Rotate

        /// <summary>
        /// Rotates an image
        /// </summary>
        /// <param name="image">Image to rotate</param>
        /// <param name="degreesToRotate">Degrees to rotate the image</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object containing the rotated image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Rotate(this Bitmap image, float degreesToRotate, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image.Width, image.Height);
            using (Graphics newGraphics = Graphics.FromImage(newBitmap))
            {
                newGraphics.TranslateTransform(image.Width / 2.0f, image.Height / 2.0f);
                newGraphics.RotateTransform(degreesToRotate);
                newGraphics.TranslateTransform(-(float)image.Width / 2.0f, -(float)image.Height / 2.0f);
                newGraphics.DrawImage(image,
                    new Rectangle(0, 0, image.Width, image.Height),
                    new Rectangle(0, 0, image.Width, image.Height),
                    GraphicsUnit.Pixel);
            }
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region SepiaTone

        /// <summary>
        /// Converts an image to sepia tone
        /// </summary>
        /// <param name="image">Image to change</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object of the sepia tone image</returns>
        public static Bitmap SepiaTone(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempMatrix = new ColorMatrix
            {
                Matrix = new[]
                        {
                            new[] {.393f, .349f, .272f, 0, 0},
                            new[] {.769f, .686f, .534f, 0, 0},
                            new[] {.189f, .168f, .131f, 0, 0},
                            new float[] {0, 0, 0, 1, 0},
                            new float[] {0, 0, 0, 0, 1}
                        }
            };
            Bitmap newBitmap = tempMatrix.Apply(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region SetPixel

        /// <summary>
        /// Sets a pixel at the x,y coords
        /// </summary>
        /// <param name="data">Bitmap data</param>
        /// <param name="x">X coord</param>
        /// <param name="y">Y coord</param>
        /// <param name="pixelColor">Pixel color information</param>
        /// <param name="pixelSizeInBytes">Pixel size in bytes</param>
        public static unsafe void SetPixel(this BitmapData data, int x, int y, Color pixelColor, int pixelSizeInBytes)
        {
            if (data.IsNull()) { throw new ArgumentNullException("data"); }
            var dataPointer = (byte*)data.Scan0;
            dataPointer = dataPointer + (y * data.Stride) + (x * pixelSizeInBytes);
            if (pixelSizeInBytes == 3)
            {
                dataPointer[2] = pixelColor.R;
                dataPointer[1] = pixelColor.G;
                dataPointer[0] = pixelColor.B;
                return;
            }
            dataPointer[3] = pixelColor.A;
            dataPointer[2] = pixelColor.R;
            dataPointer[1] = pixelColor.G;
            dataPointer[0] = pixelColor.B;
        }

        #endregion

        #region Sharpen

        /// <summary>
        /// Sharpens an image
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        public static Bitmap Sharpen(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempFilter = new Filter();
            tempFilter.MyFilter[0, 0] = -1;
            tempFilter.MyFilter[0, 2] = -1;
            tempFilter.MyFilter[2, 0] = -1;
            tempFilter.MyFilter[2, 2] = -1;
            tempFilter.MyFilter[0, 1] = -2;
            tempFilter.MyFilter[1, 0] = -2;
            tempFilter.MyFilter[2, 1] = -2;
            tempFilter.MyFilter[1, 2] = -2;
            tempFilter.MyFilter[1, 1] = 16;
            Bitmap newBitmap = tempFilter.ApplyFilter(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region SharpenLess

        /// <summary>
        /// Sharpens an image
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        public static Bitmap SharpenLess(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempFilter = new Filter();
            tempFilter.MyFilter[0, 0] = -1;
            tempFilter.MyFilter[0, 1] = 0;
            tempFilter.MyFilter[0, 2] = -1;
            tempFilter.MyFilter[1, 0] = 0;
            tempFilter.MyFilter[1, 1] = 7;
            tempFilter.MyFilter[1, 2] = 0;
            tempFilter.MyFilter[2, 0] = -1;
            tempFilter.MyFilter[2, 1] = 0;
            tempFilter.MyFilter[2, 2] = -1;
            Bitmap newBitmap = tempFilter.ApplyFilter(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region SinWave

        /// <summary>
        /// Does a "wave" effect on the image
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="amplitude">Amplitude of the sine wave</param>
        /// <param name="frequency">Frequency of the sine wave</param>
        /// <param name="xDirection">Determines if this should be done in the X direction</param>
        /// <param name="yDirection">Determines if this should be done in the Y direction</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap which has been modified</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap SinWave(this Bitmap originalImage, float amplitude, float frequency, bool xDirection, bool yDirection, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    double value1 = 0;
                    double value2 = 0;
                    if (yDirection)
                        value1 = System.Math.Sin(((x * frequency) * System.Math.PI) / 180.0d) * amplitude;
                    if (xDirection)
                        value2 = System.Math.Sin(((y * frequency) * System.Math.PI) / 180.0d) * amplitude;
                    value1 = y - (int)value1;
                    value2 = x - (int)value2;
                    while (value1 < 0)
                        value1 += height;
                    while (value2 < 0)
                        value2 += width;
                    while (value1 >= height)
                        value1 -= height;
                    while (value2 >= width)
                        value2 -= width;
                    newData.SetPixel(x, y,
                        oldData.GetPixel((int)value2, (int)value1, oldPixelSize),
                        newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region SobelEdgeDetection

        /// <summary>
        /// Sobel edge detection function
        /// </summary>
        /// <param name="input">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        public static Bitmap SobelEdgeDetection(this Bitmap input, string fileName = "")
        {
            if (input.IsNull()) { throw new ArgumentNullException("input"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            using (Bitmap tempImage = input.BlackAndWhite())
            {
                var tempFilter = new Filter();
                tempFilter.MyFilter[0, 0] = -1;
                tempFilter.MyFilter[0, 1] = 0;
                tempFilter.MyFilter[0, 2] = 1;
                tempFilter.MyFilter[1, 0] = -2;
                tempFilter.MyFilter[1, 1] = 0;
                tempFilter.MyFilter[1, 2] = 2;
                tempFilter.MyFilter[2, 0] = -1;
                tempFilter.MyFilter[2, 1] = 0;
                tempFilter.MyFilter[2, 2] = 1;
                tempFilter.Absolute = true;
                using (Bitmap tempImageX = tempFilter.ApplyFilter(tempImage))
                {
                    tempFilter = new Filter();
                    tempFilter.MyFilter[0, 0] = 1;
                    tempFilter.MyFilter[0, 1] = 2;
                    tempFilter.MyFilter[0, 2] = 1;
                    tempFilter.MyFilter[1, 0] = 0;
                    tempFilter.MyFilter[1, 1] = 0;
                    tempFilter.MyFilter[1, 2] = 0;
                    tempFilter.MyFilter[2, 0] = -1;
                    tempFilter.MyFilter[2, 1] = -2;
                    tempFilter.MyFilter[2, 2] = -1;
                    tempFilter.Absolute = true;
                    using (Bitmap tempImageY = tempFilter.ApplyFilter(tempImage))
                    {
                        using (var newBitmap = new Bitmap(tempImage.Width, tempImage.Height))
                        {
                            BitmapData newData = newBitmap.LockImage();
                            BitmapData oldData1 = tempImageX.LockImage();
                            BitmapData oldData2 = tempImageY.LockImage();
                            int newPixelSize = newData.GetPixelSize();
                            int oldPixelSize1 = oldData1.GetPixelSize();
                            int oldPixelSize2 = oldData2.GetPixelSize();
                            int width = newBitmap.Width;
                            int height = newBitmap.Height;
                            Parallel.For(0, width, x =>
                            {
                                for (int y = 0; y < height; ++y)
                                {
                                    Color pixel1 = oldData1.GetPixel(x, y, oldPixelSize1);
                                    Color pixel2 = oldData2.GetPixel(x, y, oldPixelSize2);
                                    newData.SetPixel(x, y,
                                        Color.FromArgb((pixel1.R + pixel2.R).Clamp(255, 0),
                                            (pixel1.G + pixel2.G).Clamp(255, 0),
                                            (pixel1.B + pixel2.B).Clamp(255, 0)),
                                        newPixelSize);
                                }
                            });
                            newBitmap.UnlockImage(newData);
                            tempImageX.UnlockImage(oldData1);
                            tempImageY.UnlockImage(oldData2);
                            Bitmap newBitmap2 = newBitmap.Negative();
                            if (!string.IsNullOrEmpty(fileName))
                                newBitmap2.Save(fileName, formatUsing);
                            return newBitmap2;
                        }
                    }
                }
            }
        }

        #endregion

        #region SobelEmboss

        /// <summary>
        /// Sobel emboss function
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        public static Bitmap SobelEmboss(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempFilter = new Filter();
            tempFilter.MyFilter[0, 0] = -1;
            tempFilter.MyFilter[0, 1] = 0;
            tempFilter.MyFilter[0, 2] = 1;
            tempFilter.MyFilter[1, 0] = -2;
            tempFilter.MyFilter[1, 1] = 0;
            tempFilter.MyFilter[1, 2] = 2;
            tempFilter.MyFilter[2, 0] = -1;
            tempFilter.MyFilter[2, 1] = 0;
            tempFilter.MyFilter[2, 2] = 1;
            tempFilter.Offset = 127;
            Bitmap newBitmap = tempFilter.ApplyFilter(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region SNNBlur

        /// <summary>
        /// Does smoothing using a SNN blur
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <param name="size">Size of the aperture</param>
        /// <returns>The resulting bitmap</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap SNNBlur(this Bitmap originalImage, int size = 3, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int apetureMinX = -(size / 2);
            int apetureMaxX = (size / 2);
            int apetureMinY = -(size / 2);
            int apetureMaxY = (size / 2);
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    int rValue = 0;
                    int gValue = 0;
                    int bValue = 0;
                    int numPixels = 0;
                    for (int x2 = apetureMinX; x2 < apetureMaxX; ++x2)
                    {
                        int tempX1 = x + x2;
                        int tempX2 = x - x2;
                        if (tempX1 >= 0 && tempX1 < width && tempX2 >= 0 && tempX2 < width)
                        {
                            for (int y2 = apetureMinY; y2 < apetureMaxY; ++y2)
                            {
                                int tempY1 = y + y2;
                                int tempY2 = y - y2;
                                if (tempY1 >= 0 && tempY1 < height && tempY2 >= 0 && tempY2 < height)
                                {
                                    Color tempColor = oldData.GetPixel(x, y, oldPixelSize);
                                    Color tempColor2 = oldData.GetPixel(tempX1, tempY1, oldPixelSize);
                                    Color tempColor3 = oldData.GetPixel(tempX2, tempY2, oldPixelSize);
                                    if (Distance(tempColor.R, tempColor2.R, tempColor.G, tempColor2.G, tempColor.B, tempColor2.B) <
                                        Distance(tempColor.R, tempColor3.R, tempColor.G, tempColor3.G, tempColor.B, tempColor3.B))
                                    {
                                        rValue += tempColor2.R;
                                        gValue += tempColor2.G;
                                        bValue += tempColor2.B;
                                    }
                                    else
                                    {
                                        rValue += tempColor3.R;
                                        gValue += tempColor3.G;
                                        bValue += tempColor3.B;
                                    }
                                    ++numPixels;
                                }
                            }
                        }
                    }
                    Color meanPixel = Color.FromArgb(rValue / numPixels,
                        gValue / numPixels,
                        bValue / numPixels);
                    newData.SetPixel(x, y, meanPixel, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region StretchContrast

        /// <summary>
        /// Stretches the contrast
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap StretchContrast(this Bitmap originalImage, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            Color minValue;
            Color maxValue;
            GetMinMaxPixel(out minValue, out maxValue, oldData, oldPixelSize);
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color currentPixel = oldData.GetPixel(x, y, oldPixelSize);
                    Color tempValue = Color.FromArgb(Map(currentPixel.R, minValue.R, maxValue.R),
                        Map(currentPixel.G, minValue.G, maxValue.G),
                        Map(currentPixel.B, minValue.B, maxValue.B));
                    newData.SetPixel(x, y, tempValue, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #endregion
    }
}
