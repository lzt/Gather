﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.Media.Image.Procedural;

#endregion

// ReSharper disable CheckNamespace
namespace Operate.Media.Image.ExtensionMethods
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Image extensions
    /// </summary>
    public static partial class BitmapExtensions
    {
        #region Function

        #region Threshold

        /// <summary>
        /// Does threshold manipulation of the image
        /// </summary>
        /// <param name="originalImage">Image to transform</param>
        /// <param name="threshold">Float defining the threshold at which to set the pixel to black vs white.</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object containing the new image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Threshold(this Bitmap originalImage, float threshold = 0.5f, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color tempColor = oldData.GetPixel(x, y, oldPixelSize);
                    newData.SetPixel(x, y,
                                     (tempColor.R + tempColor.G + tempColor.B)/755.0f > threshold
                                         ? Color.White
                                         : Color.Black, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region ToASCIIArt

        /// <summary>
        /// Converts an image to ASCII art
        /// </summary>
        /// <param name="input">The image you wish to convert</param>
        /// <returns>A string containing the art</returns>
        public static string ToASCIIArt(this Bitmap input)
        {
            if (input.IsNull()) { throw new ArgumentNullException("input"); }
            bool showLine = true;
            using (Bitmap tempImage = input.BlackAndWhite())
            {
                BitmapData oldData = tempImage.LockImage();
                int oldPixelSize = oldData.GetPixelSize();
                var builder = new StringBuilder();
                for (int x = 0; x < tempImage.Height; ++x)
                {
                    for (int y = 0; y < tempImage.Width; ++y)
                    {
                        if (showLine)
                        {
                            Color currentPixel = oldData.GetPixel(y, x, oldPixelSize);
                            builder.Append(ASCIICharacters[((currentPixel.R * ASCIICharacters.Length) / 255)]);
                        }

                    }
                    if (showLine)
                    {
                        builder.Append(Environment.NewLine);
                        showLine = false;
                    }
                    else
                    {
                        showLine = true;
                    }
                }
                tempImage.UnlockImage(oldData);
                return builder.ToString();
            }
        }

        #endregion

        #region ToBase64

        /// <summary>
        /// Converts an image to a base64 string and returns it
        /// </summary>
        /// <param name="image">Image to convert</param>
        /// <param name="desiredFormat">Desired image format (defaults to Jpeg)</param>
        /// <returns>The image in base64 string format</returns>
        public static string ToBase64(this System.Drawing.Image image, ImageFormat desiredFormat = null)
        {
            desiredFormat = desiredFormat.Check(ImageFormat.Jpeg);
            using (var stream = new MemoryStream())
            {
                image.Save(stream, desiredFormat);
                return stream.ToArray().ToString(Base64FormattingOptions.None);
            }
        }

        #endregion

        #region Turbulence

        /// <summary>
        /// Does turbulence manipulation of the image
        /// </summary>
        /// <param name="originalImage">Image to transform</param>
        /// <param name="roughness">Roughness of the movement</param>
        /// <param name="power">How strong the movement is</param>
        /// <param name="seed">Random seed</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object containing the new image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Turbulence(this Bitmap originalImage, int roughness = 8, float power = 5.0f, int seed = 25123864, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            int width = originalImage.Width;
            int height = originalImage.Height;
            BitmapData originalData = originalImage.LockImage();
            int originalPixelSize = originalData.GetPixelSize();
            var newBitmap = new Bitmap(width, height);
            BitmapData returnData = newBitmap.LockImage();
            int returnPixelSize = returnData.GetPixelSize();
            using (Bitmap xNoise = PerlinNoise.Generate(width, height, 255, 0, 0.0625f, 1.0f, 0.5f, roughness, seed))
            {
                BitmapData xNoiseData = xNoise.LockImage();
                int xNoisePixelSize = xNoiseData.GetPixelSize();
                using (Bitmap yNoise = PerlinNoise.Generate(width, height, 255, 0, 0.0625f, 1.0f, 0.5f, roughness, seed * 2))
                {
                    BitmapData yNoiseData = yNoise.LockImage();
                    int yNoisePixelSize = yNoiseData.GetPixelSize();
                    Parallel.For(0, height, y =>
                    {
                        for (int x = 0; x < width; ++x)
                        {
                            float xDistortion = x + (GetHeight(x, y, xNoiseData, xNoisePixelSize) * power);
                            float yDistortion = y + (GetHeight(x, y, yNoiseData, yNoisePixelSize) * power);
                            int x1 = ((int)xDistortion).Clamp(width - 1, 0);
                            int y1 = ((int)yDistortion).Clamp(height - 1, 0);
                            returnData.SetPixel(x, y, originalData.GetPixel(x1, y1, originalPixelSize), returnPixelSize);
                        }
                    });
                    yNoise.UnlockImage(yNoiseData);
                }
                xNoise.UnlockImage(xNoiseData);
            }
            newBitmap.UnlockImage(returnData);
            UnlockImage(originalImage, originalData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region UnlockImage

        /// <summary>
        /// Unlocks the image's data
        /// </summary>
        /// <param name="image">Image to unlock</param>
        /// <param name="imageData">The image data</param>
        /// <returns>Returns the image</returns>
        public static Bitmap UnlockImage(this Bitmap image, BitmapData imageData)
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            if (imageData.IsNull()) { throw new ArgumentNullException("imageData"); }
            image.UnlockBits(imageData);
            return image;
        }

        #endregion

        #region Watermark

        /// <summary>
        /// Adds a watermark to an image
        /// </summary>
        /// <param name="image">image to add the watermark to</param>
        /// <param name="watermarkImage">Watermark image</param>
        /// <param name="opacity">Opacity of the watermark (1.0 to 0.0 with 1 being completely visible and 0 being invisible)</param>
        /// <param name="x">X position in pixels for the watermark</param>
        /// <param name="y">Y position in pixels for the watermark</param>
        /// <param name="keyColor">Transparent color used in watermark image, set to null if not used</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>The results in the form of a bitmap object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Watermark(this Bitmap image, Bitmap watermarkImage, float opacity, int x, int y, Color keyColor, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            if (watermarkImage.IsNull()) { throw new ArgumentNullException("watermarkImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image, image.Width, image.Height);
            using (Graphics newGraphics = Graphics.FromImage(newBitmap))
            {
                float[][] floatColorMatrix ={
                            new float[] {1, 0, 0, 0, 0},
                            new float[] {0, 1, 0, 0, 0},
                            new float[] {0, 0, 1, 0, 0},
                            new[] {0, 0, 0, opacity, 0},
                            new float[] {0, 0, 0, 0, 1}
                        };

                var newColorMatrix = new System.Drawing.Imaging.ColorMatrix(floatColorMatrix);
                using (var attributes = new ImageAttributes())
                {
                    attributes.SetColorMatrix(newColorMatrix);
                    attributes.SetColorKey(keyColor, keyColor);
                    newGraphics.DrawImage(watermarkImage,
                        new Rectangle(x, y, watermarkImage.Width, watermarkImage.Height),
                        0, 0, watermarkImage.Width, watermarkImage.Height,
                        GraphicsUnit.Pixel,
                        attributes);
                }
            }
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Xor

        /// <summary>
        /// Xors two images
        /// </summary>
        /// <param name="image1">Image to manipulate</param>
        /// <param name="image2">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Xor(this Bitmap image1, Bitmap image2, string fileName = "")
        {
                   if (image1.IsNull()) { throw new ArgumentNullException("image1"); }
            if (image2.IsNull()) { throw new ArgumentNullException("image2"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image1.Width, image1.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData1 = image1.LockImage();
            BitmapData oldData2 = image2.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize1 = oldData1.GetPixelSize();
            int oldPixelSize2 = oldData2.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color pixel1 = oldData1.GetPixel(x, y, oldPixelSize1);
                    Color pixel2 = oldData2.GetPixel(x, y, oldPixelSize2);
                    newData.SetPixel(x, y,
                        Color.FromArgb(pixel1.R ^ pixel2.R,
                            pixel1.G ^ pixel2.G,
                            pixel1.B ^ pixel2.B),
                        newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            image1.UnlockImage(oldData1);
            image2.UnlockImage(oldData2);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #endregion
    }
}