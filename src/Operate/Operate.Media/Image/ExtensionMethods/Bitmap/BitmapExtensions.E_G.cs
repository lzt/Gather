﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Operate.ExtensionMethods;

#endregion

// ReSharper disable CheckNamespace
namespace Operate.Media.Image.ExtensionMethods
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Image extensions
    /// </summary>
    public static partial class BitmapExtensions
    {
        #region Functions
        #region EdgeDetection

        /// <summary>
        /// Does basic edge detection on an image
        /// </summary>
        /// <param name="originalImage">Image to do edge detection on</param>
        /// <param name="threshold">Decides what is considered an edge</param>
        /// <param name="edgeColor">Color of the edge</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap which has the edges drawn on it</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap EdgeDetection(this Bitmap originalImage, float threshold, Color edgeColor, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            if (edgeColor.IsNull()) { throw new ArgumentNullException("edgeColor"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage, originalImage.Width, originalImage.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color currentColor = oldData.GetPixel(x, y, oldPixelSize);
                    if (y < height - 1 && x < width - 1)
                    {
                        Color tempColor = oldData.GetPixel(x + 1, y + 1, oldPixelSize);
                        if (Distance(currentColor.R, tempColor.R, currentColor.G, tempColor.G, currentColor.B, tempColor.B) > threshold)
                            newData.SetPixel(x, y, edgeColor, newPixelSize);
                    }
                    else if (y < height - 1)
                    {
                        Color tempColor = oldData.GetPixel(x, y + 1, oldPixelSize);
                        if (Distance(currentColor.R, tempColor.R, currentColor.G, tempColor.G, currentColor.B, tempColor.B) > threshold)
                            newData.SetPixel(x, y, edgeColor, newPixelSize);
                    }
                    else if (x < width - 1)
                    {
                        Color tempColor = oldData.GetPixel(x + 1, y, oldPixelSize);
                        if (Distance(currentColor.R, tempColor.R, currentColor.G, tempColor.G, currentColor.B, tempColor.B) > threshold)
                            newData.SetPixel(x, y, edgeColor, newPixelSize);
                    }
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Emboss

        /// <summary>
        /// Emboss function
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap image</returns>
        public static Bitmap Emboss(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempFilter = new Filter();
            tempFilter.MyFilter[0, 0] = -2;
            tempFilter.MyFilter[0, 1] = -1;
            tempFilter.MyFilter[1, 0] = -1;
            tempFilter.MyFilter[1, 1] = 1;
            tempFilter.MyFilter[2, 1] = 1;
            tempFilter.MyFilter[1, 2] = 1;
            tempFilter.MyFilter[2, 2] = 2;
            tempFilter.MyFilter[0, 2] = 0;
            tempFilter.MyFilter[2, 0] = 0;
            Bitmap newBitmap = tempFilter.ApplyFilter(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Equalize

        /// <summary>
        /// Uses an RGB histogram to equalize the image
        /// </summary>
        /// <param name="originalImage">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>The resulting bitmap image</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Equalize(this Bitmap originalImage, string fileName = "")
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(originalImage.Width, originalImage.Height);
            var tempHistogram = new RGBHistogram(originalImage);
            tempHistogram.Equalize();
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = originalImage.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width = newBitmap.Width;
            int height = newBitmap.Height;
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < height; ++y)
                {
                    Color current = oldData.GetPixel(x, y, oldPixelSize);
                    var newR = (int)tempHistogram.R[current.R];
                    var newG = (int)tempHistogram.G[current.G];
                    var newB = (int)tempHistogram.B[current.B];
                    newR = newR.Clamp(255, 0);
                    newG = newG.Clamp(255, 0);
                    newB = newB.Clamp(255, 0);
                    newData.SetPixel(x, y, Color.FromArgb(newR, newG, newB), newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            originalImage.UnlockImage(oldData);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region Flip

        /// <summary>
        /// Flips an image
        /// </summary>
        /// <param name="image">Image to flip</param>
        /// <param name="flipX">Flips an image along the X axis</param>
        /// <param name="flipY">Flips an image along the Y axis</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap which is flipped</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Flip(this Bitmap image, bool flipX, bool flipY, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var newBitmap = new Bitmap(image, image.Width, image.Height);
            if (flipX && !flipY)
                newBitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
            else if (!flipX && flipY)
                newBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
            else if (flipX)
                newBitmap.RotateFlip(RotateFlipType.RotateNoneFlipXY);

            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region GaussianBlur

        /// <summary>
        /// Does smoothing using a gaussian blur
        /// </summary>
        /// <param name="image">Image to manipulate</param>
        /// <param name="fileName">File to save to</param>
        /// <param name="size">Size of the aperture</param>
        /// <returns>The resulting bitmap</returns>
        public static Bitmap GaussianBlur(this Bitmap image, int size = 3, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            using (Bitmap returnBitmap = image.BoxBlur(size))
            {
                using (Bitmap returnBitmap2 = returnBitmap.BoxBlur(size))
                {
                    Bitmap returnBitmap3 = returnBitmap2.BoxBlur(size);
                    if (!string.IsNullOrEmpty(fileName))
                        returnBitmap3.Save(fileName, formatUsing);
                    return returnBitmap3;
                }
            }
        }

        #endregion

        #region GetHTMLPalette

        /// <summary>
        /// Gets a palette listing in HTML string format
        /// </summary>
        /// <param name="originalImage">Image to get the palette of</param>
        /// <returns>A list containing HTML color values (ex: #041845)</returns>
        public static IEnumerable<string> GetHTMLPalette(this Bitmap originalImage)
        {
            if (originalImage.IsNull()) { throw new ArgumentNullException("originalImage"); }
            var returnArray = new List<string>();
            if (originalImage.Palette.Entries.Length > 0)
            {
                originalImage.Palette.Entries.ForEach(x => returnArray.AddIfUnique(ColorTranslator.ToHtml(x)));
                return returnArray;
            }
            BitmapData imageData = originalImage.LockImage();
            int pixelSize = imageData.GetPixelSize();
            for (int x = 0; x < originalImage.Width; ++x)
            {
                for (int y = 0; y < originalImage.Height; ++y)
                {
                    returnArray.AddIfUnique(ColorTranslator.ToHtml(imageData.GetPixel(x, y, pixelSize)));
                }
            }
            originalImage.UnlockImage(imageData);
            return returnArray;
        }

        #endregion

        #region GreenFilter

        /// <summary>
        /// Gets the Green filter for an image
        /// </summary>
        /// <param name="image">Image to change</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>A bitmap object</returns>
        public static Bitmap GreenFilter(this Bitmap image, string fileName = "")
        {
            if (image.IsNull()) { throw new ArgumentNullException("image"); }
            ImageFormat formatUsing = fileName.GetImageFormat();
            var tempMatrix = new ColorMatrix
            {
                Matrix = new[]
                       {
                           new float[] {0, 0, 0, 0, 0},
                           new float[] {0, 1, 0, 0, 0},
                           new float[] {0, 0, 0, 0, 0},
                           new float[] {0, 0, 0, 1, 0},
                           new float[] {0, 0, 0, 0, 1}
                       }
            };
            Bitmap newBitmap = tempMatrix.Apply(image);
            if (!string.IsNullOrEmpty(fileName))
                newBitmap.Save(fileName, formatUsing);
            return newBitmap;
        }

        #endregion

        #region GetHeight

        private static float GetHeight(int x, int y, BitmapData blackAndWhiteData, int blackAndWhitePixelSize)
        {
            if (blackAndWhiteData.IsNull()) { throw new ArgumentNullException("blackAndWhiteData"); }
            Color tempColor = blackAndWhiteData.GetPixel(x, y, blackAndWhitePixelSize);
            return GetHeight(tempColor);
        }

        private static float GetHeight(Color color)
        {
            return color.R / 255.0f;
        }

        #endregion

        #region GetImageFormat

        /// <summary>
        /// Returns the image format this file is using
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.EndsWith(System.String,System.StringComparison)")]
        public static ImageFormat GetImageFormat(this string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return ImageFormat.Bmp;
            if (fileName.EndsWith("jpg", StringComparison.InvariantCultureIgnoreCase) || fileName.EndsWith("jpeg", StringComparison.InvariantCultureIgnoreCase))
                return ImageFormat.Jpeg;
            if (fileName.EndsWith("png", StringComparison.InvariantCultureIgnoreCase))
                return ImageFormat.Png;
            if (fileName.EndsWith("tiff", StringComparison.InvariantCultureIgnoreCase))
                return ImageFormat.Tiff;
            if (fileName.EndsWith("ico", StringComparison.InvariantCultureIgnoreCase))
                return ImageFormat.Icon;
            if (fileName.EndsWith("gif", StringComparison.InvariantCultureIgnoreCase))
                return ImageFormat.Gif;
            return ImageFormat.Bmp;
        }

        #endregion

        #region GetMinMaxPixel

        private static void GetMinMaxPixel(out Color min, out Color max, BitmapData imageData, int pixelSize)
        {
            if (imageData.IsNull()) { throw new ArgumentNullException("imageData"); }
            int minR = 255, minG = 255, minB = 255;
            int maxR = 0, maxG = 0, maxB = 0;
            for (int x = 0; x < imageData.Width; ++x)
            {
                for (int y = 0; y < imageData.Height; ++y)
                {
                    Color tempImage = imageData.GetPixel(x, y, pixelSize);
                    if (minR > tempImage.R)
                        minR = tempImage.R;
                    if (maxR < tempImage.R)
                        maxR = tempImage.R;

                    if (minG > tempImage.G)
                        minG = tempImage.G;
                    if (maxG < tempImage.G)
                        maxG = tempImage.G;

                    if (minB > tempImage.B)
                        minB = tempImage.B;
                    if (maxB < tempImage.B)
                        maxB = tempImage.B;
                }
            }
            min = Color.FromArgb(minR, minG, minB);
            max = Color.FromArgb(maxR, maxG, maxB);
        }

        #endregion

        #region GetPixelSize

        /// <summary>
        /// Gets the pixel size (in bytes)
        /// </summary>
        /// <param name="data">Bitmap data</param>
        /// <returns>The pixel size (in bytes)</returns>
        public static int GetPixelSize(this BitmapData data)
        {
            if (data.IsNull()) { throw new ArgumentNullException("data"); }
            if (data.PixelFormat == PixelFormat.Format24bppRgb)
                return 3;
            if (data.PixelFormat == PixelFormat.Format32bppArgb
                || data.PixelFormat == PixelFormat.Format32bppPArgb
                || data.PixelFormat == PixelFormat.Format32bppRgb)
                return 4;
            return 0;
        }

        #endregion

        #region GetPixel

        /// <summary>
        /// Gets a pixel from an x,y coordinate
        /// </summary>
        /// <param name="data">Bitmap data</param>
        /// <param name="x">X coord</param>
        /// <param name="y">Y coord</param>
        /// <param name="pixelSizeInBytes">Pixel size in bytes</param>
        /// <returns>The pixel at the x,y coords</returns>
        public static unsafe Color GetPixel(this BitmapData data, int x, int y, int pixelSizeInBytes)
        {
            if (data.IsNull()) { throw new ArgumentNullException("data"); }
            var dataPointer = (byte*)data.Scan0;
            dataPointer = dataPointer + (y * data.Stride) + (x * pixelSizeInBytes);
            return (pixelSizeInBytes == 3) ?
                Color.FromArgb(dataPointer[2], dataPointer[1], dataPointer[0]) :
                Color.FromArgb(dataPointer[3], dataPointer[2], dataPointer[1], dataPointer[0]);
        }

        #endregion

        #endregion
    }
}
