﻿// ReSharper disable CheckNamespace
namespace Operate.Media.Image.ExtensionMethods
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Image extensions
    /// </summary>
    public static partial class BitmapExtensions
    {
        #region Variables

        /// <summary>
        /// Characters used for ASCII art
        /// </summary>
        private static readonly string[] ASCIICharacters = { "#", "#", "@", "%", "=", "+", "*", ":", "-", ".", " " };

        #endregion
    }

    #region Enums

    /// <summary>
    /// Enum defining alignment
    /// </summary>
    public enum Align
    {
        /// <summary>
        /// Top
        /// </summary>
        Top,
        /// <summary>
        /// Bottom
        /// </summary>
        Bottom,
        /// <summary>
        /// Left
        /// </summary>
        Left,
        /// <summary>
        /// Right
        /// </summary>
        Right
    }

    /// <summary>
    /// Enum defining quality
    /// </summary>
    public enum Quality
    {
        /// <summary>
        /// High
        /// </summary>
        High,
        /// <summary>
        /// Low
        /// </summary>
        Low
    }

    /// <summary>
    /// Direction
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Top to bottom
        /// </summary>
        TopBottom = 0,
        /// <summary>
        /// Left to right
        /// </summary>
        LeftRight
    };

    #endregion
}
