﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

#endregion

namespace Operate.Media.Image.ExtensionMethods
{
    /// <summary>
    /// Screen extensions
    /// </summary>
    public static class ScreenExtensions
    {
        #region Functions

        #region TakeScreenShot

        /// <summary>
        /// Takes a screenshot of the screen as a whole
        /// (if multiple screens are attached, it takes an image containing them all)
        /// </summary>
        /// <param name="screen">Screen to get the screenshot from</param>
        /// <param name="fileName">Name of the file to save the screenshot (optional)</param>
        /// <returns>Returns a bitmap containing the screen shot</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public static Bitmap TakeScreenShot(this Screen screen, string fileName = "")
        {
            if (screen.IsNull()) { throw new ArgumentNullException("screen"); }
            var tempBitmap = new Bitmap(screen.Bounds.Width > 1 ? screen.Bounds.Width : 1, screen.Bounds.Height > 1 ? screen.Bounds.Height : 1, PixelFormat.Format32bppArgb);
            try
            {
                if (screen.Bounds.Width > 1 && screen.Bounds.Height > 1)
                {
                    using (Graphics tempGraphics = Graphics.FromImage(tempBitmap))
                    {
                        tempGraphics.CopyFromScreen(screen.Bounds.X, screen.Bounds.Y, 0, 0, screen.Bounds.Size, CopyPixelOperation.SourceCopy);
                    }
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
            if (!string.IsNullOrEmpty(fileName))
                tempBitmap.Save(fileName);
            return tempBitmap;
        }

        /// <summary>
        /// Takes a screenshot of the screen as a whole
        /// (if multiple screens are attached, it takes an image containing them all)
        /// </summary>
        /// <param name="screens">Screens to get the screenshot from</param>
        /// <param name="fileName">Name of the file to save the screenshot (optional)</param>
        /// <returns>Returns a bitmap containing the screen shot</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public static Bitmap TakeScreenShot(this IEnumerable<Screen> screens, string fileName = "")
        {
            if (screens.IsNull()) { throw new ArgumentNullException("screens"); }
            Rectangle totalScreenRect = Rectangle.Empty;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Screen currentScreen in Screen.AllScreens)
                // ReSharper restore LoopCanBeConvertedToQuery
                totalScreenRect = Rectangle.Union(totalScreenRect, currentScreen.Bounds);
            var tempBitmap = new Bitmap(totalScreenRect.Width > 1 ? totalScreenRect.Width : 1, totalScreenRect.Height > 1 ? totalScreenRect.Width : 1, PixelFormat.Format32bppArgb);
            try
            {
                if (totalScreenRect.Width > 1 && totalScreenRect.Height > 1)
                {
                    using (Graphics tempGraphics = Graphics.FromImage(tempBitmap))
                    {
                        tempGraphics.CopyFromScreen(totalScreenRect.X, totalScreenRect.Y, 0, 0, totalScreenRect.Size, CopyPixelOperation.SourceCopy);
                    }
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
            if (!string.IsNullOrEmpty(fileName))
                tempBitmap.Save(fileName);
            return tempBitmap;
        }

        #endregion

        #endregion
    }
}