﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;

using System.Drawing;
using System.Drawing.Imaging;
using Operate.ExtensionMethods;
using Operate.Media.Image.ExtensionMethods;

#endregion

namespace Operate.Media.Image
{
    /// <summary>
    /// Class used to create an RGB Histogram
    /// </summary>
    public class RGBHistogram
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="image">Image to load</param>
        public RGBHistogram(Bitmap image = null)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            R = new float[256];
            G = new float[256];
            B = new float[256];
            if (image != null)
                LoadImage(image);
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Loads an image
        /// </summary>
        /// <param name="imageUsing">Image to load</param>
        public virtual void LoadImage(Bitmap imageUsing)
        {
            if (imageUsing.IsNull()) { throw new ArgumentNullException("imageUsing"); }
            BitmapData oldData = imageUsing.LockImage();
            int pixelSize = oldData.GetPixelSize();
            _width = imageUsing.Width;
            _height = imageUsing.Height;
            R.Clear();
            G.Clear();
            B.Clear();
            for (int x = 0; x < _width; ++x)
            {
                for (int y = 0; y < _height; ++y)
                {
                    Color tempColor = oldData.GetPixel(x, y, pixelSize);
                    ++R[tempColor.R];
                    ++G[tempColor.G];
                    ++B[tempColor.B];
                }
            }
            imageUsing.UnlockImage(oldData);
        }

        /// <summary>
        /// Normalizes the histogram
        /// </summary>
        public virtual void Normalize()
        {
            float totalPixels = _width * _height;
            if (totalPixels <= 0)
                return;
            for (int x = 0; x < 256; ++x)
            {
                R[x] /= totalPixels;
                G[x] /= totalPixels;
                B[x] /= totalPixels;
            }
        }

        /// <summary>
        /// Equalizes the histogram
        /// </summary>
        public virtual void Equalize()
        {
            float totalPixels = _width * _height;
            int rMax = int.MinValue;
            int rMin = int.MaxValue;
            int gMax = int.MinValue;
            int gMin = int.MaxValue;
            int bMax = int.MinValue;
            int bMin = int.MaxValue;
            for (int x = 0; x < 256; ++x)
            {
                if (R[x] > 0f)
                {
                    if (rMax < x)
                        rMax = x;
                    if (rMin > x)
                        rMin = x;
                }
                if (G[x] > 0f)
                {
                    if (gMax < x)
                        gMax = x;
                    if (gMin > x)
                        gMin = x;
                }
                if (B[x] > 0f)
                {
                    if (bMax < x)
                        bMax = x;
                    if (bMin > x)
                        bMin = x;
                }
            }

            float previousR = R[0];
            R[0] = R[0] * 256 / totalPixels;
            float previousG = G[0];
            G[0] = G[0] * 256 / totalPixels;
            float previousB = B[0];
            B[0] = B[0] * 256 / totalPixels;
            for (int x = 1; x < 256; ++x)
            {
                previousR += R[x];
                previousG += G[x];
                previousB += B[x];
                R[x] = ((previousR - R[rMin]) / (totalPixels - R[rMin])) * 255;
                G[x] = ((previousG - G[gMin]) / (totalPixels - G[gMin])) * 255;
                B[x] = ((previousB - B[bMin]) / (totalPixels - B[bMin])) * 255;
            }
            _width = 256;
            _height = 1;
        }

        #endregion

        #region Private Values
        private int _width;
        private int _height;
        #endregion

        #region Public Properties

        /// <summary>
        /// Red values
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public virtual float[] R { get; set; }

        /// <summary>
        /// Green values
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public virtual float[] G { get; set; }

        /// <summary>
        /// Blue values
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public virtual float[] B { get; set; }

        #endregion
    }
}