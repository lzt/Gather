﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Operate.ExtensionMethods;
using Operate.Media.Image.ExtensionMethods;

#endregion

namespace Operate.Media.Image
{
    /// <summary>
    /// Used when applying convolution filters to an image
    /// </summary>
    public class Filter
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        public Filter(int width = 3, int height = 3)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            MyFilter = new int[width, height];
            Width = width;
            Height = height;
            Offset = 0;
            Absolute = false;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The actual filter array
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        public virtual int[,] MyFilter { get; set; }

        /// <summary>
        /// Width of the filter box
        /// </summary>
        public virtual int Width { get; set; }

        /// <summary>
        /// Height of the filter box
        /// </summary>
        public virtual int Height { get; set; }

        /// <summary>
        /// Amount to add to the red, blue, and green values
        /// </summary>
        public virtual int Offset { get; set; }

        /// <summary>
        /// Determines if we should take the absolute value prior to clamping
        /// </summary>
        public virtual bool Absolute { get; set; }

        #endregion

        #region Public Functions

        /// <summary>
        /// Applies the filter to the input image
        /// </summary>
        /// <param name="input">input image</param>
        /// <returns>Returns a separate image with the filter applied</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public virtual Bitmap ApplyFilter(Bitmap input)
        {
            if (input.IsNull()) { throw new ArgumentNullException("input"); }
            var newBitmap = new Bitmap(input.Width, input.Height);
            BitmapData newData = newBitmap.LockImage();
            BitmapData oldData = input.LockImage();
            int newPixelSize = newData.GetPixelSize();
            int oldPixelSize = oldData.GetPixelSize();
            int width2 = input.Width;
            int height2 = input.Height;
            Parallel.For(0, width2, x =>
            {
                for (int y = 0; y < height2; ++y)
                {
                    int rValue = 0;
                    int gValue = 0;
                    int bValue = 0;
                    int weight = 0;
                    int xCurrent = -Width / 2;
                    for (int x2 = 0; x2 < Width; ++x2)
                    {
                        if (xCurrent + x < width2 && xCurrent + x >= 0)
                        {
                            int yCurrent = -Height / 2;
                            for (int y2 = 0; y2 < Height; ++y2)
                            {
                                if (yCurrent + y < height2 && yCurrent + y >= 0)
                                {
                                    Color pixel = oldData.GetPixel(xCurrent + x, yCurrent + y, oldPixelSize);
                                    rValue += MyFilter[x2, y2] * pixel.R;
                                    gValue += MyFilter[x2, y2] * pixel.G;
                                    bValue += MyFilter[x2, y2] * pixel.B;
                                    weight += MyFilter[x2, y2];
                                }
                                ++yCurrent;
                            }
                        }
                        ++xCurrent;
                    }
                    Color meanPixel = oldData.GetPixel(x, y, oldPixelSize);
                    if (weight == 0)
                        weight = 1;
                    if (weight > 0)
                    {
                        if (Absolute)
                        {
                            rValue = System.Math.Abs(rValue);
                            gValue = System.Math.Abs(gValue);
                            bValue = System.Math.Abs(bValue);
                        }
                        rValue = (rValue / weight) + Offset;
                        rValue = rValue.Clamp(255, 0);
                        gValue = (gValue / weight) + Offset;
                        gValue = gValue.Clamp(255, 0);
                        bValue = (bValue / weight) + Offset;
                        bValue = bValue.Clamp(255, 0);
                        meanPixel = Color.FromArgb(rValue, gValue, bValue);
                    }
                    newData.SetPixel(x, y, meanPixel, newPixelSize);
                }
            });
            newBitmap.UnlockImage(newData);
            input.UnlockImage(oldData);
            return newBitmap;
        }

        #endregion
    }
}