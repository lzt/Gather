﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Operate.Media.Audio
{
    public class Mp3Player
    {
        #region - 属性 -

        [DllImport("winmm.dll")]
        private static extern Int32 mciSendString(String command, StringBuilder buffer, Int32 bufferSize,
            IntPtr hwndCallback);

        /// <summary>          
        /// 临时音乐文件存放处        
        /// </summary>         
        private readonly string _musicPath = "";

        /// <summary>         
        /// 父窗体句柄         
        /// </summary>         
        private readonly IntPtr _handle;

        #endregion

        #region - 构造函数 -

        /// <summary>          
        /// 创建Mp3播放类         
        /// </summary>          
        /// <param name="music">嵌入的音乐文件</param>          
        /// <param name="path">临时音乐文件保存路径</param>         
        /// <param name="handle">父窗体句柄</param>          
        public Mp3Player(Byte[] music, string path, IntPtr handle)
        {
            try
            {
                _handle = handle;
                _musicPath = Path.Combine(path, "temp.mp3");
                var fs = new FileStream(_musicPath, FileMode.Create);
                fs.Write(music, 0, music.Length);
                fs.Close();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>          
        /// 创建Mp3播放类        
        /// </summary>         
        /// <param name="musicPath">要播放的mp3文件路径</param>         
        /// <param name="handle">父窗体句柄</param>        
        public Mp3Player(string musicPath, IntPtr handle)
        {
            _musicPath = musicPath;
            _handle = handle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="music"></param>
        /// <param name="handle"></param>
        public Mp3Player(Byte[] music, IntPtr handle) : this(music, @"C:\Windows\", handle)
        {
        }

        #endregion

        #region - 播放音乐 -

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void Open(string path)
        {
            if (path != "")
            {
                try
                {
                    mciSendString("open " + path + " alias media", null, 0, _handle);
                    mciSendString("play media", null, 0, _handle);
                }
                catch (Exception)
                {
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Open()
        {
            Open(_musicPath);
        }

        #endregion

        #region - 停止音乐播放 -

        private void CloseMedia()
        {
            try
            {
                mciSendString("close all", null, 0, _handle);
            }
            catch (Exception)
            {
            }
        }

        #endregion
    }
}
