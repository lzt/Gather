﻿using System.Media;

namespace Operate.Media.Audio
{
    /// <summary>
    /// Wav操作类
    /// </summary>
    public class WavOperator
    {
        /// <summary>
        /// Player
        /// </summary>
        public SoundPlayer Player { get; private set; }

        #region constructor

        /// <summary>
        /// 初始化
        /// </summary>
        public WavOperator()
        {
            Player = new SoundPlayer();
        }

        #endregion

        #region Function

        /// <summary>
        /// 以同步方式播放wav文件
        /// </summary>
        /// <param name="wavFilePath">wav文件的路径</param>
        public void SyncPlay(string wavFilePath)
        {
            //设置wav文件的路径 
            Player.SoundLocation = wavFilePath;

            //使用异步方式加载wav文件
            Player.LoadAsync();

            //使用同步方式播放wav文件
            if (Player.IsLoadCompleted)
            {
                Player.PlaySync();
            }
        }

        /// <summary>
        /// 以异步方式播放wav文件
        /// </summary>
        /// <param name="wavFilePath">wav文件的路径</param>
        public void ASyncPlay(string wavFilePath)
        {
            //设置wav文件的路径 
            Player.SoundLocation = wavFilePath;

            //使用异步方式加载wav文件
            Player.LoadAsync();

            //使用异步方式播放wav文件
            if (Player.IsLoadCompleted)
            {
                Player.Play();
            }
        }

        /// <summary>
        /// 停止播放
        /// </summary>
        public void StopPlay()
        {
            Player.Stop();
        }

        #endregion
    }
}
