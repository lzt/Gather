﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Operate.ExtensionMethods;
using Operate.ORM.Aspect.Interfaces;
using Operate.ORM.Mapping.Interfaces;
using Operate.ORM.QueryProviders.Interfaces;
using Operate.Reflection.ExtensionMethods;
using Operate.SQL;
using Operate.SQL.MicroORM;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion Usings

namespace Operate.ORM.QueryProviders
{
    /// <summary>
    /// Default query provider
    /// </summary>
    public class Default
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="profile">Should profiling be done</param>
        /// <param name="assembliesUsing">Assemblies using</param>
        public Default(bool profile, params Assembly[] assembliesUsing)
        {
            Profile = profile;
            foreach (Assembly assembly in assembliesUsing)
            {
                Setup(assembly);
            }
        }

        #endregion Constructor

        #region Functions

        #region Setup

        /// <summary>
        /// Sets up the system
        /// </summary>
        /// <param name="assemblyUsing">Assembly to set up</param>
        private void Setup(Assembly assemblyUsing)
        {
            if (Databases == null)
                Databases = new List<IDatabase>();
            IEnumerable<Type> types = assemblyUsing.Types(typeof(IDatabase));
            foreach (Type type in types)
            {
                var tempObject = (IDatabase)Activator.CreateInstance(type);
                if (!string.IsNullOrEmpty(tempObject.ConnectionString))
                {
                    SQLHelper.Database(tempObject.ConnectionString, tempObject.Name);
                    Databases.Add(tempObject);
                }
            }
            Manager = new Reflection.AOP.AOPManager();
        }

        #endregion Setup

        #region AddMapping

        /// <summary>
        /// Adds a mapping
        /// </summary>
        /// <param name="mapping">Mapping to add</param>
        public virtual void AddMapping(IMapping mapping)
        {
            if (Mappings == null)
                Mappings = new ListMapping<IDatabase, IMapping>();
            IEnumerable<IDatabase> databases = Databases.Where(x => x.Is(mapping.DatabaseConfigType)
                                                        && !string.IsNullOrEmpty(x.ConnectionString));
            foreach (IDatabase database in databases)
            {
                mapping.AddToQueryProvider(database);
                Mappings.Add(database, mapping);
            }
        }

        #endregion AddMapping

        #region Any

        /// <summary>
        /// Returns any item that matches the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <param name="currentSession">Current session</param>
        /// <param name="returnValue">Return value</param>
        /// <returns>First item matching the criteria</returns>
        public virtual TObjectType Any<TObjectType>(Session currentSession, TObjectType returnValue = null, params IParameter[] parameters) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValue = mapping.AnyCommand == null ? ormObject.Any("*", returnValue, () => Manager.Create<TObjectType>(), false, parameters) : ormObject.Any(mapping.AnyCommand.SQLCommand, mapping.AnyCommand.CommandType, returnValue, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            var tempReturnValue = returnValue as IORMObject;
            if (tempReturnValue != null)
                tempReturnValue.Session0 = currentSession;
            return returnValue;
        }

        /// <summary>
        /// Returns any item that matches the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="columns">Columns to load</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <param name="currentSession">Current session</param>
        /// <param name="returnValue">Return value</param>
        /// <returns>First item that matches the criteria</returns>
        public virtual TObjectType Any<TObjectType>(Session currentSession, string columns, TObjectType returnValue = null, params IParameter[] parameters) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                if (Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType)) != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValue = ormObject.Any(columns, returnValue, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            var tempReturnValue = returnValue as IORMObject;
            if (tempReturnValue != null)
                tempReturnValue.Session0 = currentSession;
            return returnValue;
        }

        /// <summary>
        /// Returns any item that matches the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="command">Command to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <param name="currentSession">Current session</param>
        /// <param name="returnValue">Return value</param>
        /// <returns>First item that matches the criteria</returns>
        public virtual TObjectType Any<TObjectType>(Session currentSession, string command, CommandType commandType, TObjectType returnValue = null, params IParameter[] parameters) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                if (Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType)) != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValue = ormObject.Any(command, commandType, returnValue, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            var tempReturnValue = returnValue as IORMObject;
            if (tempReturnValue != null)
                tempReturnValue.Session0 = currentSession;
            return returnValue;
        }

        #endregion Any

        #region All

        /// <summary>
        /// Returns a list of objects that meet the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="currentSession">Current session</param>
        /// <param name="columns">Columns to load</param>
        /// <param name="limit">Limit on the number of items to return</param>
        /// <param name="orderBy">Order by clause (minus the ORDER BY)</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A list of objects that meet the criteria</returns>
        public virtual IEnumerable<TObjectType> All<TObjectType>(Session currentSession, string columns, int limit, string orderBy, params IParameter[] parameters) where TObjectType : class,new()
        {
            var returnValues = new List<TObjectType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                if (Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType)) != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValues = (List<TObjectType>)ormObject.All(columns, limit, orderBy, returnValues, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            if (returnValues != null)
            {
                // ReSharper disable PossibleMultipleEnumeration
                foreach (TObjectType returnValue in returnValues)
                // ReSharper restore PossibleMultipleEnumeration
                {
                    var tempReturn = returnValue as IORMObject;
                    if (tempReturn != null)
                        tempReturn.Session0 = currentSession;
                }
                // ReSharper disable PossibleMultipleEnumeration
                return returnValues;
                // ReSharper restore PossibleMultipleEnumeration
            }
            return null;
        }

        /// <summary>
        /// Returns a list of objects that meet the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="currentSession">Current session</param>
        /// <param name="command">Command to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A list of objects that meet the criteria</returns>
        public virtual IEnumerable<TObjectType> All<TObjectType>(Session currentSession, string command, CommandType commandType, params IParameter[] parameters) where TObjectType : class,new()
        {
            var returnValues = new List<TObjectType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                if (Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType)) != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValues = (List<TObjectType>)ormObject.All(command, commandType, returnValues, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            if (returnValues != null)
            {
                // ReSharper disable PossibleMultipleEnumeration
                foreach (TObjectType returnValue in returnValues)
                // ReSharper restore PossibleMultipleEnumeration
                {
                    var tempReturn = returnValue as IORMObject;
                    if (tempReturn != null)
                        tempReturn.Session0 = currentSession;
                }
                // ReSharper disable PossibleMultipleEnumeration
                return returnValues;
                // ReSharper restore PossibleMultipleEnumeration
            }
            return null;
        }

        /// <summary>
        /// Returns a list of objects that meet the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="currentSession">Current session</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A list of objects that meet the criteria</returns>
        public virtual IEnumerable<TObjectType> All<TObjectType>(Session currentSession, params IParameter[] parameters) where TObjectType : class,new()
        {
            var returnValues = new List<TObjectType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        if (mapping.AllCommand == null)
                        {
                            returnValues = (List<TObjectType>)ormObject.All("*", 0, "", returnValues, () => Manager.Create<TObjectType>(), false, parameters);
                        }
                        else
                        {
                            returnValues = (List<TObjectType>)ormObject.All(mapping.AllCommand.SQLCommand, mapping.AllCommand.CommandType, returnValues, () => Manager.Create<TObjectType>(), false, parameters);
                        }
                    }
                }
            }
            if (returnValues != null)
            {
                // ReSharper disable PossibleMultipleEnumeration
                foreach (TObjectType returnValue in returnValues)
                // ReSharper restore PossibleMultipleEnumeration
                {
                    var tempReturn = returnValue as IORMObject;
                    if (tempReturn != null)
                        tempReturn.Session0 = currentSession;
                }
                // ReSharper disable PossibleMultipleEnumeration
                return returnValues;
                // ReSharper restore PossibleMultipleEnumeration
            }
            return null;
        }

        #endregion All

        #region Delete

        /// <summary>
        /// Deletes the specified object from the database
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="Object">Object to delete</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public virtual void Delete<TObjectType>(TObjectType Object) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Writable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        var joinCommands = new List<Command>();
                        foreach (IProperty property in mapping.Properties)
                        {
                            if (!property.Cascade && mapping.ObjectType == property.Type)
                            {
                                joinCommands.AddIfUnique(((IProperty<TObjectType>)property).JoinsDelete(Object, ormObject));
                            }
                            if (property.Cascade)
                            {
                                joinCommands.AddIfUnique(((IProperty<TObjectType>)property).CascadeJoinsDelete(Object, ormObject));
                            }
                        }
                        if (joinCommands.Count > 0)
                        {
                            ormObject.Batch().AddCommands(joinCommands.ToArray());
                            ormObject.ExecuteNonQuery();
                        }
                        foreach (IProperty property in mapping.Properties)
                        {
                            if (property.Cascade)
                            {
                                ((IProperty<TObjectType>)property).CascadeDelete(Object, ormObject);
                            }
                        }
                        ormObject.Delete(Object);
                    }
                }
            }
        }

        #endregion Delete

        #region LoadProperties

        /// <summary>
        /// Loads a property
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TDataType">Property type</typeparam>
        /// <param name="currentSession">Current Session</param>
        /// <param name="Object">Object</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="parameters">Extra parameters</param>
        /// <returns>The appropriate property value</returns>
        public virtual IEnumerable<TDataType> LoadProperties<TObjectType, TDataType>(Session currentSession, TObjectType Object, string propertyName, params IParameter[] parameters)
            where TObjectType : class,new()
            where TDataType : class,new()
        {
            var returnValue = new List<TDataType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    IProperty property = mapping.Properties.FirstOrDefault(x => x.Type == typeof(TDataType)
                        && x.Name == propertyName);
                    if (property != null)
                    {
                        using (var ormObject = new SQLHelper(database.Name))
                        {
                            if (property.CommandToLoad == null)
                                return ormObject.All("*", 0, "", returnValue, () => Manager.Create<TDataType>(), false, parameters);
                            returnValue = (List<TDataType>)ormObject.All(property.CommandToLoad.SQLCommand, property.CommandToLoad.CommandType, returnValue, () => Manager.Create<TDataType>(), false, parameters);
                        }
                    }
                }
            }
            if (returnValue != null)
            {
                // ReSharper disable PossibleMultipleEnumeration
                foreach (TDataType item in returnValue)
                // ReSharper restore PossibleMultipleEnumeration
                {
                    var tempItem = item as IORMObject;
                    if (tempItem != null)
                        tempItem.Session0 = currentSession;
                }
                // ReSharper disable PossibleMultipleEnumeration
                return returnValue;
                // ReSharper restore PossibleMultipleEnumeration
            }
            return null;
        }

        #endregion LoadProperties

        #region LoadListProperties

        /// <summary>
        /// Loads a property
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TDataType">Property type</typeparam>
        /// <param name="currentSession">Current Session</param>
        /// <param name="Object">Object</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="parameters">Extra parameters</param>
        /// <returns>The appropriate property value</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public virtual List<TDataType> LoadListProperties<TObjectType, TDataType>(Session currentSession, TObjectType Object, string propertyName, params IParameter[] parameters)
            where TObjectType : class,new()
            where TDataType : class,new()
        {
            var returnValue = new List<TDataType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    IProperty property = mapping.Properties.FirstOrDefault(x => x.Type == typeof(TDataType)
                        && x.Name == propertyName);
                    if (property != null)
                    {
                        using (var ormObject = new SQLHelper(database.Name))
                        {
                            if (property.CommandToLoad == null)
                                returnValue = (List<TDataType>)ormObject.All("*", 0, "", returnValue, () => Manager.Create<TDataType>(), false, parameters);
                            else
                                returnValue = (List<TDataType>)ormObject.All(property.CommandToLoad.SQLCommand, property.CommandToLoad.CommandType, returnValue, () => Manager.Create<TDataType>(), false, parameters);
                        }
                    }
                }
            }
            foreach (TDataType item in returnValue)
            {
                var tempItem = item as IORMObject;
                if (tempItem != null)
                    tempItem.Session0 = currentSession;
            }
            return returnValue;
        }

        #endregion LoadListProperties

        #region LoadProperty

        /// <summary>
        /// Loads a property
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TDataType">Property type</typeparam>
        /// <param name="currentSession">Current session</param>
        /// <param name="Object">Object</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="parameters">Extra parameters</param>
        /// <returns>The appropriate property value</returns>
        public virtual TDataType LoadProperty<TObjectType, TDataType>(Session currentSession, TObjectType Object, string propertyName, params IParameter[] parameters)
            where TObjectType : class,new()
            where TDataType : class,new()
        {
            TDataType returnValue = null;
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    IProperty property = mapping.Properties.FirstOrDefault(x => x.Type == typeof(TDataType) && x.Name == propertyName);
                    if (property != null)
                    {
                        using (var ormObject = new SQLHelper(database.Name))
                        {
                            returnValue = property.CommandToLoad == null ? ormObject.Any("*", returnValue, () => Manager.Create<TDataType>(), false, parameters) : ormObject.Any(property.CommandToLoad.SQLCommand, property.CommandToLoad.CommandType, returnValue, () => Manager.Create<TDataType>(), false, parameters);
                        }
                    }
                }
            }
            var tempReturn = returnValue as IORMObject;
            if (tempReturn != null)
                tempReturn.Session0 = currentSession;
            return returnValue;
        }

        #endregion LoadProperty

        #region Paged

        /// <summary>
        /// Returns a paged list of items
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="columns">Columns to load</param>
        /// <param name="orderBy">Order by clause (minus the ORDER BY part)</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (starting with 0)</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <param name="currentSession">Current session to use in the query</param>
        /// <returns>A paged list of items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> Paged<TObjectType>(Session currentSession, string columns = "*", string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] parameters) where TObjectType : class,new()
        {
            var returnValues = new List<TObjectType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValues = (List<TObjectType>)ormObject.Paged(columns, orderBy, pageSize, currentPage, returnValues, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            if (returnValues != null)
            {
                // ReSharper disable PossibleMultipleEnumeration
                foreach (TObjectType returnValue in returnValues)
                // ReSharper restore PossibleMultipleEnumeration
                {
                    var tempReturn = returnValue as IORMObject;
                    if (tempReturn != null)
                        tempReturn.Session0 = currentSession;
                }
                // ReSharper disable PossibleMultipleEnumeration
                return returnValues;
                // ReSharper restore PossibleMultipleEnumeration
            }
            return null;
        }

        #endregion Paged

        #region PagedCommand

        /// <summary>
        /// Returns a paged list of items
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="command">Command to use</param>
        /// <param name="orderBy">Order by clause (minus the ORDER BY part)</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (starting with 0)</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <param name="currentSession">Current session to use in the query</param>
        /// <returns>A paged list of items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> PagedCommand<TObjectType>(Session currentSession, string command, string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] parameters) where TObjectType : class,new()
        {
            var returnValues = new List<TObjectType>();
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        returnValues = (List<TObjectType>)ormObject.PagedCommand(command, orderBy, pageSize, currentPage, returnValues, () => Manager.Create<TObjectType>(), false, parameters);
                    }
                }
            }
            if (returnValues != null)
            {
                // ReSharper disable PossibleMultipleEnumeration
                foreach (TObjectType returnValue in returnValues)
                // ReSharper restore PossibleMultipleEnumeration
                {
                    var tempReturn = returnValue as IORMObject;
                    if (tempReturn != null)
                        tempReturn.Session0 = currentSession;
                }
                // ReSharper disable PossibleMultipleEnumeration
                return returnValues;
                // ReSharper restore PossibleMultipleEnumeration
            }
            return null;
        }

        #endregion PagedCommand

        #region PageCount

        /// <summary>
        /// Gets the number of pages based on the specified info
        /// </summary>
        /// <param name="currentSession">Current session</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <returns>The number of pages that the table contains for the specified page size</returns>
        public virtual int PageCount<TObjectType>(Session currentSession, int pageSize = 25, params IParameter[] parameters) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        return ormObject.PageCount<TObjectType>(pageSize, false, parameters);
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Gets the number of pages based on the specified info
        /// </summary>
        /// <param name="currentSession">Current session</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <returns>The number of pages that the table contains for the specified page size</returns>
        public virtual int PageCount<TObjectType>(Session currentSession, string command, int pageSize = 25, params IParameter[] parameters) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        return ormObject.PageCount<TObjectType>(command, pageSize, false, parameters);
                    }
                }
            }
            return 0;
        }

        #endregion PageCount

        #region Save

        /// <summary>
        /// Saves an object to the database
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TPrimaryKeyType">Primary key type</typeparam>
        /// <param name="Object">Object to save</param>
        /// <param name="parameters">Extra parameters used in saving the object</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        public virtual void Save<TObjectType, TPrimaryKeyType>(TObjectType Object, params IParameter[] parameters) where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Writable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        foreach (IProperty property in mapping.Properties)
                        {
                            if (property.Cascade)
                            {
                                ((IProperty<TObjectType>)property).CascadeSave(Object, ormObject);
                            }
                        }
                        List<IParameter> Params = parameters.ToList();
                        // ReSharper disable LoopCanBeConvertedToQuery
                        foreach (IProperty property in mapping.Properties)
                        // ReSharper restore LoopCanBeConvertedToQuery
                        {
                            IParameter parameter = ((IProperty<TObjectType>)property).GetAsParameter(Object);
                            if (parameter != null)
                                Params.Add(parameter);
                        }
                        ormObject.Save<TObjectType, TPrimaryKeyType>(Object, Params.ToArray());
                        var joinCommands = new List<Command>();
                        foreach (IProperty property in mapping.Properties)
                        {
                            if (!property.Cascade &&
                                (property is IManyToMany
                                    || property is IManyToOne
                                    || property is IIEnumerableManyToOne
                                    || property is IListManyToMany
                                    || property is IListManyToOne))
                            {
                                joinCommands.AddIfUnique(((IProperty<TObjectType>)property).JoinsDelete(Object, ormObject));
                            }
                            if (property.Cascade)
                            {
                                joinCommands.AddIfUnique(((IProperty<TObjectType>)property).CascadeJoinsDelete(Object, ormObject));
                            }
                        }
                        if (joinCommands.Count > 0)
                        {
                            for (int x = 0; x < (joinCommands.Sum(y => y.Parameters.Count) / 100) + 1; ++x)
                            {
                                ormObject.Batch().AddCommands(joinCommands.ElementsBetween(x * 100, (x * 100) + 100).ToArray());
                                ormObject.ExecuteNonQuery();
                            }
                        }
                        joinCommands = new List<Command>();
                        foreach (IProperty property in mapping.Properties)
                        {
                            if (!property.Cascade &&
                                (property is IManyToMany
                                    || property is IManyToOne
                                    || property is IIEnumerableManyToOne
                                    || property is IListManyToMany
                                    || property is IListManyToOne))
                            {
                                joinCommands.AddIfUnique(((IProperty<TObjectType>)property).JoinsSave(Object, ormObject));
                            }
                            if (property.Cascade)
                            {
                                joinCommands.AddIfUnique(((IProperty<TObjectType>)property).CascadeJoinsSave(Object, ormObject));
                            }
                        }
                        if (joinCommands.Count > 0)
                        {
                            for (int x = 0; x < (joinCommands.Sum(y => y.Parameters.Count) / 100) + 1; ++x)
                            {
                                ormObject.Batch().AddCommands(joinCommands.ElementsBetween(x * 100, (x * 100) + 100).ToArray());
                                ormObject.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }

        #endregion Save

        #region Scalar

        /// <summary>
        /// Runs a supplied scalar function and returns the result
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <param name="currentSession">Current session</param>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <returns>The scalar value returned by the command</returns>
        public virtual TDataType Scalar<TObjectType, TDataType>(Session currentSession, string command, CommandType commandType, params IParameter[] parameters)
            where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        return ormObject.Scalar<TObjectType, TDataType>(command, commandType, false, parameters);
                    }
                }
            }
            return default(TDataType);
        }

        /// <summary>
        /// Runs a scalar command using the specified aggregate function
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="currentSession">Current session</param>
        /// <param name="aggregateFunction">Aggregate function</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>The scalar value returned by the command</returns>
        public virtual TDataType Scalar<TObjectType, TDataType>(Session currentSession, string aggregateFunction, params IParameter[] parameters)
            where TObjectType : class,new()
        {
            foreach (IDatabase database in Mappings.Keys.Where(x => x.Readable).OrderBy(x => x.Order))
            {
                IMapping mapping = Mappings[database].FirstOrDefault(x => x.ObjectType == typeof(TObjectType));
                if (mapping != null)
                {
                    using (var ormObject = new SQLHelper(database.Name))
                    {
                        return ormObject.Scalar<TObjectType, TDataType>(aggregateFunction, false, parameters);
                    }
                }
            }
            return default(TDataType);
        }

        #endregion Scalar

        #endregion Functions

        #region Properties

        /// <summary>
        /// List of database configurations
        /// </summary>
        public ICollection<IDatabase> Databases { get; private set; }

        /// <summary>
        /// AOP manager (used to create objects)
        /// </summary>
        public Reflection.AOP.AOPManager Manager { get; set; }

        /// <summary>
        /// Mappings associated to databases
        /// </summary>
        public ListMapping<IDatabase, IMapping> Mappings { get; private set; }

        /// <summary>
        /// Should the queries be profiled?
        /// </summary>
        public bool Profile { get; set; }

        #endregion Properties
    }
}