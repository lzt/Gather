﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Generic;
using System.Data;
using Operate.ORM.QueryProviders;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.ORM
{
    /// <summary>
    /// Session class
    /// </summary>
    public class Session
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="queryProvider">Query provider</param>
        public Session(Default queryProvider)
        {
            QueryProvider = queryProvider;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Query provider
        /// </summary>
        private Default QueryProvider { get; set; }

        #endregion

        #region Functions

        #region Any

        /// <summary>
        /// Returns a single item matching the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Type of the object</typeparam>
        /// <param name="columns">Columns to load</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A single object matching the criteria</returns>
        public virtual TObjectType Any<TObjectType>(string columns, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.Any<TObjectType>(this, columns, null, parameters);
        }

        /// <summary>
        /// Returns a single item matching the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Type of the object</typeparam>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A single object matching the criteria</returns>
        public virtual TObjectType Any<TObjectType>(params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.Any<TObjectType>(this, null, parameters);
        }

        /// <summary>
        /// Returns a single item matching the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Type of the object</typeparam>
        /// <param name="command">SQL Command to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A single object matching the criteria</returns>
        public virtual TObjectType Any<TObjectType>(string command, CommandType commandType, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.Any<TObjectType>(this, command, commandType, null, parameters);
        }

        #endregion

        #region All

        /// <summary>
        /// Returns all items that match the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Type of the object</typeparam>
        /// <param name="columns">Columns to load</param>
        /// <param name="limit">Limit of the number of items to load</param>
        /// <param name="orderBy">Order by clause (minus the ORDER BY part)</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>All items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> All<TObjectType>(string columns, int limit, string orderBy, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.All<TObjectType>(this, columns, limit, orderBy, parameters);
        }

        /// <summary>
        /// Returns all items that match the criteria
        /// </summary>
        /// <typeparam name="TObjectType">Type of the object</typeparam>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>All items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> All<TObjectType>(params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.All<TObjectType>(this, parameters);
        }

        /// <summary>
        /// /// Returns all items that match the criteria
        /// </summary>
        /// <typeparam name="TObjectType"></typeparam>
        /// <param name="command">SQL Command to run</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>All items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> All<TObjectType>(string command, CommandType commandType, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.All<TObjectType>(this, command, commandType, parameters);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes an object from the database
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="Object">Object to delete</param>
        public virtual void Delete<TObjectType>(TObjectType Object) where TObjectType : class,new()
        {
            QueryProvider.Delete(Object);
        }

        #endregion

        #region LoadProperties

        /// <summary>
        /// Loads a property (primarily used internally for lazy loading)
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="Object">Object</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="parameters">Extra parameters (generally will be the ID of the object)</param>
        /// <returns>The appropriate property value</returns>
        public virtual IEnumerable<TDataType> LoadProperties<TObjectType, TDataType>(TObjectType Object, string propertyName, params IParameter[] parameters)
            where TObjectType : class,new()
            where TDataType : class,new()
        {
            return QueryProvider.LoadProperties<TObjectType, TDataType>(this, Object, propertyName, parameters);
        }

        #endregion

        #region LoadListProperties

        /// <summary>
        /// Loads a property (primarily used internally for lazy loading)
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="Object">Object</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="parameters">Extra parameters (generally will be the ID of the object)</param>
        /// <returns>The appropriate property value</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public virtual List<TDataType> LoadListProperties<TObjectType, TDataType>(TObjectType Object, string propertyName, params IParameter[] parameters)
            where TObjectType : class,new()
            where TDataType : class,new()
        {
            return QueryProvider.LoadListProperties<TObjectType, TDataType>(this, Object, propertyName, parameters);
        }

        #endregion

        #region LoadProperty

        /// <summary>
        /// Loads a property (primarily used internally for lazy loading)
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="Object">Object</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="parameters">Extra parameters (generally will be the ID of the object)</param>
        /// <returns>The appropriate property value</returns>
        public virtual TDataType LoadProperty<TObjectType, TDataType>(TObjectType Object, string propertyName, params IParameter[] parameters)
            where TObjectType : class,new()
            where TDataType : class,new()
        {
            return QueryProvider.LoadProperty<TObjectType, TDataType>(this, Object, propertyName, parameters);
        }

        #endregion

        #region Paged

        /// <summary>
        /// Returns a paged list of items
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="columns">Columns to load</param>
        /// <param name="orderBy">Order by clause (minus the ORDER BY part)</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (starting with 0)</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A paged list of items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> Paged<TObjectType>(string columns = "*", string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.Paged<TObjectType>(this, columns, orderBy, pageSize, currentPage, parameters);
        }

        #endregion

        #region PagedCommand

        /// <summary>
        /// Returns a paged list of items
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="command">Command to call</param>
        /// <param name="orderBy">Order by clause (minus the ORDER BY part)</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (starting with 0)</param>
        /// <param name="parameters">Parameters used in the where clause</param>
        /// <returns>A paged list of items that match the criteria</returns>
        public virtual IEnumerable<TObjectType> PagedCommand<TObjectType>(string command, string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.PagedCommand<TObjectType>(this, command, orderBy, pageSize, currentPage, parameters);
        }

        #endregion

        #region PageCount

        /// <summary>
        /// Gets the number of pages based on the specified 
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <typeparam name="TObjectType">Object type to get the page count of</typeparam>
        /// <returns>The number of pages that the table contains for the specified page size</returns>
        public virtual int PageCount<TObjectType>(int pageSize = 25, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.PageCount<TObjectType>(this, pageSize, parameters);
        }

        /// <summary>
        /// Gets the number of pages based on the specified 
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <typeparam name="TObjectType">Object type to get the page count of</typeparam>
        /// <returns>The number of pages that the table contains for the specified page size</returns>
        public virtual int PageCount<TObjectType>(string command, int pageSize = 25, params IParameter[] parameters) where TObjectType : class,new()
        {
            return QueryProvider.PageCount<TObjectType>(this, command, pageSize, parameters);
        }

        #endregion

        #region Save

        /// <summary>
        /// Saves an object to the database
        /// </summary>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <typeparam name="TPrimaryKeyType">Primary key type</typeparam>
        /// <param name="Object">Object to save</param>
        /// <param name="parameters">Extra parameters to save</param>
        public virtual void Save<TObjectType, TPrimaryKeyType>(TObjectType Object, params IParameter[] parameters) where TObjectType : class,new()
        {
            QueryProvider.Save<TObjectType, TPrimaryKeyType>(Object, parameters);
        }

        #endregion

        #region Scalar

        /// <summary>
        /// Runs a supplied scalar function and returns the result
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <returns>The scalar value returned by the command</returns>
        public virtual TDataType Scalar<TObjectType, TDataType>(string command, CommandType commandType, params IParameter[] parameters)
            where TObjectType : class,new()
        {
            return QueryProvider.Scalar<TObjectType, TDataType>(this, command, commandType, parameters);
        }

        /// <summary>
        /// Runs a scalar command using the specified aggregate function
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <typeparam name="TObjectType">Object type</typeparam>
        /// <param name="aggregateFunction">Aggregate function</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>The scalar value returned by the command</returns>
        public virtual TDataType Scalar<TObjectType, TDataType>(string aggregateFunction, params IParameter[] parameters)
            where TObjectType : class,new()
        {
            return QueryProvider.Scalar<TObjectType, TDataType>(this, aggregateFunction, parameters);
        }

        #endregion

        #endregion
    }
}