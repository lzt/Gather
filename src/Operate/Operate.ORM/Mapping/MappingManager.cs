﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Operate.ExtensionMethods;
using Operate.ORM.Mapping.Interfaces;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.ORM.Mapping
{
    /// <summary>
    /// Mapping manager
    /// </summary>
    public class MappingManager : IMappingManager
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assemblyUsing">Assembly using</param>
        public MappingManager(Assembly assemblyUsing)
        {
            Setup(assemblyUsing);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assembliesUsing">Assemblies using</param>
        public MappingManager(IEnumerable<Assembly> assembliesUsing)
        {
            foreach (Assembly assembly in assembliesUsing)
            {
                Setup(assembly);
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the mappings
        /// </summary>
        /// <param name="assemblyUsing">Assembly using</param>
        private void Setup(Assembly assemblyUsing)
        {
            if (Mappings == null)
                Mappings = new ListMapping<Type, IMapping>();
            IEnumerable<Type> types = assemblyUsing.Types(typeof(IMapping));
            foreach (Type type in types)
            {
                Type baseType = type.BaseType;
                var tempObject = (IMapping)assemblyUsing.CreateInstance(type.FullName);
                if (tempObject != null)
                {
                    tempObject.Manager = this;
                    if (baseType != null) Mappings.Add(baseType.GetGenericArguments()[0], tempObject);
                }
            }
        }

        /// <summary>
        /// Initializes the mappings
        /// </summary>
        public void Initialize()
        {
            var tempMappings = new List<IMapping>();
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Type key in Mappings.Keys)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (IMapping mapping in Mappings[key])
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    tempMappings.Add(mapping);
                }
            }
            foreach (IMapping mapping in tempMappings.OrderBy(x => x.Order))
            {
                mapping.Initialize();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// List of mappings
        /// </summary>
        public ListMapping<Type, IMapping> Mappings { get; private set; }

        #endregion
    }
}