﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using Operate.Patterns;
using Operate.ORM.QueryProviders.Interfaces;
using Operate.SQL;
using Operate.SQL.MicroORM;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.ORM.Mapping.Interfaces
{
    /// <summary>
    /// Property interface
    /// </summary>
    public interface IProperty
    {
        #region Functions

        /// <summary>
        /// Sets up the various load commands
        /// </summary>
        void SetupLoadCommands();

        /// <summary>
        /// Gets the property as an object
        /// </summary>
        /// <param name="Object">Object to get the property from</param>
        /// <returns>The property as an object</returns>
        object GetAsObject(object Object);

        #endregion

        #region Properties

        /// <summary>
        /// Auto increment
        /// </summary>
        bool AutoIncrement { get; }

        /// <summary>
        /// Cascade
        /// </summary>
        bool Cascade { get; }

        /// <summary>
        /// Derived field name
        /// </summary>
        string DerivedFieldName { get; }

        /// <summary>
        /// Field name
        /// </summary>
        string FieldName { get; }

        /// <summary>
        /// Foreign key link
        /// </summary>
        IMapping ForeignKey { get; set; }

        /// <summary>
        /// Index
        /// </summary>
        bool Index { get; }

        /// <summary>
        /// Max length (used in strings)
        /// </summary>
        int MaxLength { get; }

        /// <summary>
        /// Name of the property
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Can the item be null?
        /// </summary>
        bool NotNull { get; }

        /// <summary>
        /// Table name
        /// </summary>
        string TableName { get; }

        /// <summary>
        /// Property type
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// Is this a unique field?
        /// </summary>
        bool Unique { get; }

        /// <summary>
        /// Command used to load the property
        /// </summary>
        Command CommandToLoad { get; }

        #endregion
    }

    /// <summary>
    /// Property interface
    /// </summary>
    /// <typeparam name="TClassType">Class type</typeparam>
    public interface IProperty<TClassType>
        where TClassType : class,new()
    {
        #region Functions

        /// <summary>
        /// Adds to query provider
        /// </summary>
        /// <param name="database">Database associated with mapping</param>
        /// <param name="mapping">Mapping to add the property to</param>
        void AddToQueryProvider(IDatabase database, Mapping<TClassType> mapping);

        /// <summary>
        /// Gets the property as a parameter
        /// </summary>
        /// <param name="Object">Object to get the property from</param>
        /// <returns>The property as a parameter</returns>
        IParameter GetAsParameter(TClassType Object);

        /// <summary>
        /// Gets the property as an object
        /// </summary>
        /// <param name="Object">Object to get the property from</param>
        /// <returns>The property as an object</returns>
        object GetAsObject(TClassType Object);

        /// <summary>
        /// Cascades the save
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM</param>
        void CascadeSave(TClassType Object, SQLHelper microORM);

        /// <summary>
        /// Cascades the delete
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM</param>
        void CascadeDelete(TClassType Object, SQLHelper microORM);

        /// <summary>
        /// Cascade the deleting of joins
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM</param>
        /// <returns>Returns the list of commands</returns>
        IEnumerable<Command> CascadeJoinsDelete(TClassType Object, SQLHelper microORM);

        /// <summary>
        /// Cascade the saving of joins
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">MicroORM</param>
        /// <returns>Returns the list of commands</returns>
        IEnumerable<Command> CascadeJoinsSave(TClassType Object, SQLHelper microORM);

        /// <summary>
        /// Deletes the joins
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM</param>
        /// <returns>Returns the list of commands</returns>
        IEnumerable<Command> JoinsDelete(TClassType Object, SQLHelper microORM);

        /// <summary>
        /// Saves the joins
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">MicroORM</param>
        /// <returns>Returns the list of commands</returns>
        IEnumerable<Command> JoinsSave(TClassType Object, SQLHelper microORM);

        #endregion
    }

    /// <summary>
    /// Property interface
    /// </summary>
    public interface IProperty<TClassType, TDataType>
    {
        #region Properties

        /// <summary>
        /// Default value for this property
        /// </summary>
        Func<TDataType> DefaultValue { get; }

        /// <summary>
        /// Expression pointing to the property
        /// </summary>
        Expression<Func<TClassType, TDataType>> Expression { get; }

        /// <summary>
        /// Compiled version of the expression
        /// </summary>
        Func<TClassType, TDataType> CompiledExpression { get; }

        #endregion
    }

    /// <summary>
    /// Property interface
    /// </summary>
    /// <typeparam name="TClassType">Class type</typeparam>
    /// <typeparam name="TDataType">Data type of the property</typeparam>
    /// <typeparam name="TReturnType">Return type</typeparam>
    // ReSharper disable UnusedTypeParameter
    public interface IProperty<TClassType, in TDataType, out TReturnType> : IFluentInterface
    // ReSharper restore UnusedTypeParameter
    {
        #region Functions

        /// <summary>
        /// Sets the default value of the property
        /// </summary>
        /// <param name="defaultValue">Default value</param>
        /// <returns>This IProperty object</returns>
        TReturnType SetDefaultValue(Func<TDataType> defaultValue);

        /// <summary>
        /// Sets the field to not null
        /// </summary>
        /// <returns>this</returns>
        TReturnType DoNotAllowNullValues();

        /// <summary>
        /// Sets the unique field to true
        /// </summary>
        /// <returns>this</returns>
        TReturnType ThisShouldBeUnique();

        /// <summary>
        /// Turns on indexing for this property
        /// </summary>
        /// <returns>This</returns>
        TReturnType TurnOnIndexing();

        /// <summary>
        /// Turns on autoincrement for this property
        /// </summary>
        /// <returns>This</returns>
        TReturnType TurnOnAutoIncrement();

        /// <summary>
        /// Sets the name of the field in the database
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <returns>this</returns>
        TReturnType SetFieldName(string fieldName);

        /// <summary>
        /// Set database table name
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <returns>this</returns>
        TReturnType SetTableName(string tableName);

        /// <summary>
        /// Turns on cascade for saving/deleting
        /// </summary>
        /// <returns>this</returns>
        TReturnType TurnOnCascade();

        /// <summary>
        /// Sets the max length for the string
        /// </summary>
        /// <param name="maxLength">Max length</param>
        /// <returns>this</returns>
        TReturnType SetMaxLength(int maxLength);

        /// <summary>
        /// Allows you to load a property based on a specified command
        /// </summary>
        /// <param name="command">Command used to load the property</param>
        /// <param name="commandType">Command type</param>
        /// <returns>this</returns>
        TReturnType LoadUsingCommand(string command, CommandType commandType);

        #endregion
    }
}
