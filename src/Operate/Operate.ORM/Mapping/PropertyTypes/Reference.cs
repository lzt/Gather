﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Operate.ORM.Mapping.BaseClasses;
using Operate.ORM.Mapping.Interfaces;
using Operate.ORM.QueryProviders.Interfaces;
using Operate.SQL;
using Operate.SQL.MicroORM;
using Operate.SQL.MicroORM.Enums;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.ORM.Mapping.PropertyTypes
{
    /// <summary>
    /// Reference class
    /// </summary>
    /// <typeparam name="TClassType">Class type</typeparam>
    /// <typeparam name="TDataType">Data type</typeparam>
    public class Reference<TClassType, TDataType> : PropertyBase<TClassType, TDataType, IReference<TClassType, TDataType>>,
        IReference<TClassType, TDataType>
        where TClassType : class,new()
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="expression">Expression pointing to the property</param>
        /// <param name="mapping">Mapping the StringID is added to</param>
        public Reference(Expression<Func<TClassType, TDataType>> expression, IMapping mapping)
            : base(expression, mapping)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            SetDefaultValue(() => default(TDataType));
            SetFieldName(Name + "_");
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the default load commands
        /// </summary>
        public override void SetupLoadCommands()
        {

        }

        /// <summary>
        /// Deletes the object from join tables
        /// </summary>
        /// <param name="Object">Object to remove</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        public override IEnumerable<Command> JoinsDelete(TClassType Object, SQLHelper microORM)
        {
            return new List<Command>();
        }

        /// <summary>
        /// Saves the object to various join tables
        /// </summary>
        /// <param name="Object">Object to add</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        public override IEnumerable<Command> JoinsSave(TClassType Object, SQLHelper microORM)
        {
            return new List<Command>();
        }

        /// <summary>
        /// Deletes the object to from join tables on cascade
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        public override IEnumerable<Command> CascadeJoinsDelete(TClassType Object, SQLHelper microORM)
        {
            return new List<Command>();
        }

        /// <summary>
        /// Saves the object to various join tables on cascade
        /// </summary>
        /// <param name="Object">Object to add</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        public override IEnumerable<Command> CascadeJoinsSave(TClassType Object, SQLHelper microORM)
        {
            return new List<Command>();
        }

        /// <summary>
        /// Deletes the object on cascade
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM object</param>
        public override void CascadeDelete(TClassType Object, SQLHelper microORM)
        {

        }

        /// <summary>
        /// Saves the object on cascade
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM object</param>
        public override void CascadeSave(TClassType Object, SQLHelper microORM)
        {

        }

        /// <summary>
        /// Gets it as a parameter
        /// </summary>
        /// <param name="Object">Object</param>
        /// <returns>The value as a parameter</returns>
        public override IParameter GetAsParameter(TClassType Object)
        {
            return null;
        }

        /// <summary>
        /// Gets it as an object
        /// </summary>
        /// <param name="Object">Object</param>
        /// <returns>The value as an object</returns>
        public override object GetAsObject(TClassType Object)
        {
            return null;
        }

        /// <summary>
        /// Sets the loading command used
        /// </summary>
        /// <param name="command">Command to use</param>
        /// <param name="commandType">Command type</param>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> LoadUsingCommand(string command, System.Data.CommandType commandType)
        {
            CommandToLoad = new Command(command, commandType);
            return this;
        }

        /// <summary>
        /// Add to query provider
        /// </summary>
        /// <param name="database">Database object</param>
        /// <param name="mapping">Mapping object</param>
        public override void AddToQueryProvider(IDatabase database, Mapping<TClassType> mapping)
        {
            var mode = Mode.Neither;
            if (database.Readable)
                mode |= Mode.Read;
            if (database.Writable)
                mode |= Mode.Write;
            mapping.Map(Expression, FieldName, DefaultValue(), mode);
        }

        /// <summary>
        /// Set a default value
        /// </summary>
        /// <param name="defaultValue">Default value</param>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> SetDefaultValue(Func<TDataType> defaultValue)
        {
            DefaultValue = defaultValue;
            return this;
        }

        /// <summary>
        /// Does not allow null values
        /// </summary>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> DoNotAllowNullValues()
        {
            NotNull = true;
            return this;
        }

        /// <summary>
        /// This should be unique
        /// </summary>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> ThisShouldBeUnique()
        {
            Unique = true;
            return this;
        }

        /// <summary>
        /// Turn on indexing
        /// </summary>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> TurnOnIndexing()
        {
            Index = true;
            return this;
        }

        /// <summary>
        /// Turn on auto increment
        /// </summary>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> TurnOnAutoIncrement()
        {
            AutoIncrement = true;
            return this;
        }

        /// <summary>
        /// Set field name
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> SetFieldName(string fieldName)
        {
            FieldName = fieldName;
            return this;
        }

        /// <summary>
        /// Set the table name
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> SetTableName(string tableName)
        {
            TableName = tableName;
            return this;
        }

        /// <summary>
        /// Turn on cascade
        /// </summary>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> TurnOnCascade()
        {
            Cascade = true;
            return this;
        }

        /// <summary>
        /// Set max length
        /// </summary>
        /// <param name="maxLength">Max length</param>
        /// <returns>This</returns>
        public override IReference<TClassType, TDataType> SetMaxLength(int maxLength)
        {
            MaxLength = maxLength;
            return this;
        }

        #endregion
    }
}