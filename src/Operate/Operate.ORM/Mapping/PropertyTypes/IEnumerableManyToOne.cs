﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Operate.ExtensionMethods;
using Operate.ORM.Mapping.BaseClasses;
using Operate.ORM.Mapping.Interfaces;
using Operate.ORM.QueryProviders.Interfaces;
using Operate.SQL;
using Operate.SQL.MicroORM;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.ORM.Mapping.PropertyTypes
{
    /// <summary>
    /// Many to one class
    /// </summary>
    /// <typeparam name="TClassType">Class type</typeparam>
    /// <typeparam name="TDataType">Data type</typeparam>
    public class EnumerableManyToOne<TClassType, TDataType> : PropertyBase<TClassType, IEnumerable<TDataType>, IIEnumerableManyToOne<TClassType, TDataType>>,
        IIEnumerableManyToOne<TClassType, TDataType>
        where TClassType : class,new()
        where TDataType : class,new()
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="expression">Expression pointing to the many to one</param>
        /// <param name="mapping">Mapping the StringID is added to</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Compare(System.String,System.String,System.StringComparison)")]
        public EnumerableManyToOne(Expression<Func<TClassType, IEnumerable<TDataType>>> expression, IMapping mapping)
            : base(expression, mapping)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Type = typeof(TDataType);
            SetDefaultValue(() => new List<TDataType>());
            string class1 = typeof(TClassType).Name;
            string class2 = typeof(TDataType).Name;
            if (string.Compare(class1, class2, StringComparison.InvariantCulture) < 0)
                SetTableName(class1 + "_" + class2);
            else
                SetTableName(class2 + "_" + class1);
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Functions

        /// <summary>
        /// Sets loading command
        /// </summary>
        public override void SetupLoadCommands()
        {
            if (CommandToLoad != null)
                return;
            IMapping foreignMapping = Mapping.Manager.Mappings[typeof(TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
            if (foreignMapping == Mapping)
            {
                if (foreignMapping != null)
                    LoadUsingCommand(@"SELECT " + foreignMapping.TableName + @".*
                                FROM " + foreignMapping.TableName + @"
                                INNER JOIN " + TableName + " ON " + TableName + "." + foreignMapping.TableName + foreignMapping.IDProperty.FieldName + "2=" + foreignMapping.TableName + "." + foreignMapping.IDProperty.FieldName + @"
                                WHERE " + TableName + "." + Mapping.TableName + Mapping.IDProperty.FieldName + "=@ID", CommandType.Text);
            }
            else
            {
                if (foreignMapping != null)
                    LoadUsingCommand(@"SELECT " + foreignMapping.TableName + @".*
                                FROM " + foreignMapping.TableName + @"
                                INNER JOIN " + TableName + " ON " + TableName + "." + foreignMapping.TableName + foreignMapping.IDProperty.FieldName + "=" + foreignMapping.TableName + "." + foreignMapping.IDProperty.FieldName + @"
                                WHERE " + TableName + "." + Mapping.TableName + Mapping.IDProperty.FieldName + "=@ID", CommandType.Text);
            }
        }

        /// <summary>
        /// Deletes the object from join tables
        /// </summary>
        /// <param name="Object">Object to remove</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        public override IEnumerable<Command> JoinsDelete(TClassType Object, SQLHelper microORM)
        {
            if (Object == null)
                return new List<Command>();
            IEnumerable<TDataType> list = CompiledExpression(Object);
            if (list == null)
                return new List<Command>();
            var commands = new List<Command>();
            object currentIDParameter = ((IProperty<TClassType>)Mapping.IDProperty).GetAsObject(Object);
            commands.AddIfUnique(new Command("DELETE FROM " + TableName + " WHERE " + Mapping.TableName + Mapping.IDProperty.FieldName + "=@0",
                    CommandType.Text,
                    "@",
                    currentIDParameter));
            return commands;
        }

        /// <summary>
        /// Saves the object to various join tables
        /// </summary>
        /// <param name="Object">Object to add</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Compare(System.String,System.String,System.StringComparison)")]
        public override IEnumerable<Command> JoinsSave(TClassType Object, SQLHelper microORM)
        {
            if (Object == null)
                return new List<Command>();
            IEnumerable<TDataType> list = CompiledExpression(Object);
            if (list == null)
                return new List<Command>();
            var commands = new List<Command>();
            foreach (TDataType item in list)
            {
                if (item != null)
                {
                    object currentIDParameter = ((IProperty<TClassType>)Mapping.IDProperty).GetAsObject(Object);
                    IMapping foreignMapping = Mapping.Manager.Mappings[typeof(TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (foreignMapping != null)
                    {
                        object foreignIDParameter = ((IProperty<TDataType>)foreignMapping.IDProperty).GetAsObject(item);
                        string parameters;
                        var values = new object[2];
                        if (foreignMapping == Mapping)
                        {
                            parameters = Mapping.TableName + Mapping.IDProperty.FieldName + "," + foreignMapping.TableName + foreignMapping.IDProperty.FieldName + "2";
                            values[0] = currentIDParameter;
                            values[1] = foreignIDParameter;
                        }
                        else if (string.Compare(Mapping.TableName, foreignMapping.TableName, StringComparison.InvariantCulture) <= 0)
                        {
                            parameters = Mapping.TableName + Mapping.IDProperty.FieldName + "," + foreignMapping.TableName + foreignMapping.IDProperty.FieldName;
                            values[0] = currentIDParameter;
                            values[1] = foreignIDParameter;
                        }
                        else
                        {
                            parameters = foreignMapping.TableName + foreignMapping.IDProperty.FieldName + "," + Mapping.TableName + Mapping.IDProperty.FieldName;
                            values[1] = currentIDParameter;
                            values[0] = foreignIDParameter;
                        }
                        commands.AddIfUnique(new Command("INSERT INTO " + TableName + "(" + parameters + ") VALUES (@0,@1)",
                                                         CommandType.Text,
                                                         "@",
                                                         values));
                    }
                }
            }
            return commands;
        }

        /// <summary>
        /// Deletes the object to from join tables on cascade
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public override IEnumerable<Command> CascadeJoinsDelete(TClassType Object, SQLHelper microORM)
        {
            if (Object == null)
                return new List<Command>();
            IEnumerable<TDataType> list = CompiledExpression(Object);
            if (list == null)
                return new List<Command>();
            var commands = new List<Command>();
            foreach (TDataType item in list)
            {
                if (item != null)
                {
                    var firstOrDefault = Mapping.Manager.Mappings[typeof (TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (
                        firstOrDefault != null)
                        foreach (IProperty property in firstOrDefault.Properties)
                        {
                            if (!property.Cascade
                                && (property is IManyToMany
                                    || property is IManyToOne
                                    || property is IIEnumerableManyToOne
                                    || property is IListManyToMany
                                    || property is IListManyToOne))
                            {
                                commands.AddIfUnique(((IProperty<TDataType>)property).JoinsDelete(item, microORM));
                            }
                            if (property.Cascade)
                            {
                                commands.AddIfUnique(((IProperty<TDataType>)property).CascadeJoinsDelete(item, microORM));
                            }
                        }
                }
            }
            commands.AddIfUnique(JoinsDelete(Object, microORM));
            return commands;
        }

        /// <summary>
        /// Saves the object to various join tables on cascade
        /// </summary>
        /// <param name="Object">Object to add</param>
        /// <param name="microORM">Micro ORM object</param>
        /// <returns>The list of commands needed to do this</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public override IEnumerable<Command> CascadeJoinsSave(TClassType Object, SQLHelper microORM)
        {
            if (Object == null)
                return new List<Command>();
            IEnumerable<TDataType> list = CompiledExpression(Object);
            if (list == null)
                return new List<Command>();
            var commands = new List<Command>();
            foreach (TDataType item in list)
            {
                if (item != null)
                {
                    var firstOrDefault = Mapping.Manager.Mappings[typeof (TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (
                        firstOrDefault != null)
                        foreach (IProperty property in firstOrDefault.Properties)
                        {
                            if (!property.Cascade &&
                                (property is IManyToMany
                                 || property is IManyToOne
                                 || property is IIEnumerableManyToOne
                                 || property is IListManyToMany
                                 || property is IListManyToOne))
                            {
                                commands.AddIfUnique(((IProperty<TDataType>)property).JoinsSave(item, microORM));
                            }
                            if (property.Cascade)
                            {
                                commands.AddIfUnique(((IProperty<TDataType>)property).CascadeJoinsSave(item, microORM));
                            }
                        }
                }
            }
            commands.AddIfUnique(JoinsSave(Object, microORM));
            return commands;
        }

        /// <summary>
        /// Deletes the object on cascade
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM object</param>
        public override void CascadeDelete(TClassType Object, SQLHelper microORM)
        {
            if (Object == null)
                return;
            var list = CompiledExpression(Object);
            if (list == null)
                return;
            foreach (TDataType item in list)
            {
                if (item != null)
                {
                    var firstOrDefault = Mapping.Manager.Mappings[typeof (TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (
                        firstOrDefault != null)
                        foreach (IProperty property in firstOrDefault.Properties)
                        {
                            if (property.Cascade)
                                ((IProperty<TDataType>)property).CascadeDelete(item, microORM);
                        }
                    var orDefault = Mapping.Manager.Mappings[typeof (TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (
                        orDefault != null)
                        ((IProperty<TDataType>)orDefault.IDProperty).CascadeDelete(item, microORM);
                }
            }
        }

        /// <summary>
        /// Saves the object on cascade
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="microORM">Micro ORM object</param>
        public override void CascadeSave(TClassType Object, SQLHelper microORM)
        {
            if (Object == null)
                return;
            var list = CompiledExpression(Object);
            if (list == null)
                return;
            foreach (TDataType item in list)
            {
                if (item != null)
                {
                    var firstOrDefault = Mapping.Manager.Mappings[typeof (TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (
                        firstOrDefault != null)
                        foreach (IProperty property in firstOrDefault.Properties)
                        {
                            if (property.Cascade)
                                ((IProperty<TDataType>)property).CascadeSave(item, microORM);
                        }
                    var orDefault = Mapping.Manager.Mappings[typeof (TDataType)].FirstOrDefault(x => x.DatabaseConfigType == Mapping.DatabaseConfigType);
                    if (
                        orDefault != null)
                        ((IProperty<TDataType>)orDefault.IDProperty).CascadeSave(item, microORM);
                }
            }
        }

        /// <summary>
        /// Sets the loading command used
        /// </summary>
        /// <param name="command">Command to use</param>
        /// <param name="commandType">Command type</param>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> LoadUsingCommand(string command, CommandType commandType)
        {
            CommandToLoad = new Command(command, commandType);
            return this;
        }

        /// <summary>
        /// Gets it as a parameter
        /// </summary>
        /// <param name="Object">Object</param>
        /// <returns>The value as a parameter</returns>
        public override IParameter GetAsParameter(TClassType Object)
        {
            return null;
        }

        /// <summary>
        /// Gets it as an object
        /// </summary>
        /// <param name="Object">Object</param>
        /// <returns>The value as an object</returns>
        public override object GetAsObject(TClassType Object)
        {
            return null;
        }

        /// <summary>
        /// Add to query provider
        /// </summary>
        /// <param name="database">Database object</param>
        /// <param name="mapping">Mapping object</param>
        public override void AddToQueryProvider(IDatabase database, Mapping<TClassType> mapping)
        {
        }

        /// <summary>
        /// Set a default value
        /// </summary>
        /// <param name="defaultValue">Default value</param>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> SetDefaultValue(Func<IEnumerable<TDataType>> defaultValue)
        {
            DefaultValue = defaultValue;
            return this;
        }

        /// <summary>
        /// Does not allow null values
        /// </summary>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> DoNotAllowNullValues()
        {
            NotNull = true;
            return this;
        }

        /// <summary>
        /// This should be unique
        /// </summary>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> ThisShouldBeUnique()
        {
            Unique = true;
            return this;
        }

        /// <summary>
        /// Turn on indexing
        /// </summary>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> TurnOnIndexing()
        {
            Index = true;
            return this;
        }

        /// <summary>
        /// Turn on auto increment
        /// </summary>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> TurnOnAutoIncrement()
        {
            AutoIncrement = true;
            return this;
        }

        /// <summary>
        /// Set field name
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> SetFieldName(string fieldName)
        {
            FieldName = fieldName;
            return this;
        }

        /// <summary>
        /// Set the table name
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> SetTableName(string tableName)
        {
            TableName = tableName;
            return this;
        }

        /// <summary>
        /// Turn on cascade
        /// </summary>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> TurnOnCascade()
        {
            Cascade = true;
            return this;
        }

        /// <summary>
        /// Set max length
        /// </summary>
        /// <param name="maxLength">Max length</param>
        /// <returns>This</returns>
        public override IIEnumerableManyToOne<TClassType, TDataType> SetMaxLength(int maxLength)
        {
            MaxLength = maxLength;
            return this;
        }

        #endregion
    }
}