﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Operate.ExtensionMethods;
using Operate.ORM.Mapping.Interfaces;
using Operate.ORM.Mapping.PropertyTypes;
using Operate.ORM.QueryProviders.Interfaces;
using Operate.SQL;
using Operate.SQL.MicroORM;
#endregion

namespace Operate.ORM.Mapping
{
    /// <summary>
    /// Class mapping
    /// </summary>
    /// <typeparam name="TClassType">Class type</typeparam>
    /// <typeparam name="TDatabaseType">Database type</typeparam>
    public abstract class Mapping<TClassType, TDatabaseType> : IMapping<TClassType>, IMapping
        where TDatabaseType : IDatabase
        where TClassType : class,new()
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="suffix">Suffix used to define names of properties/table name</param>
        /// <param name="prefix">Prefix used to define names of properties/table name</param>
        protected Mapping(string tableName = "", string suffix = "_", string prefix = "")
        {
            if (!string.IsNullOrEmpty(tableName))
                TableName = tableName;
            Suffix = suffix;
            Prefix = prefix;
            Setup();
        }

        #endregion

        #region Functions

        #region AddToQueryProvider

        /// <summary>
        /// Add to query provider
        /// </summary>
        /// <param name="database">Database object</param>
        public void AddToQueryProvider(IDatabase database)
        {
            Mapping<TClassType> map = SQLHelper.Map<TClassType>(TableName, IDProperty.Chain(x => x.FieldName), IDProperty.Chain(x => x.AutoIncrement), database.Chain(x => x.Name));
            ((IProperty<TClassType>)IDProperty).Chain(x => x.AddToQueryProvider(database, map));
            foreach (IProperty property in Properties)
                ((IProperty<TClassType>)property).AddToQueryProvider(database, map);
        }

        #endregion

        #region All

        /// <summary>
        /// Sets a command for default all function
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="commandType">Command type</param>
        public virtual void All(string command, System.Data.CommandType commandType)
        {
            AllCommand = new Command(command, commandType);
        }

        #endregion

        #region Any

        /// <summary>
        /// Sets a command for default any function
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="commandType">Command type</param>
        public virtual void Any(string command, System.Data.CommandType commandType)
        {
            AnyCommand = new Command(command, commandType);
        }

        #endregion

        #region Setup

        /// <summary>
        /// Sets up the mapping
        /// </summary>
        private void Setup()
        {
            if (Properties == null)
                Properties = new List<IProperty>();
        }

        #endregion

        #region ID

        /// <summary>
        /// Creates an ID object
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>ID object</returns>
        public IID<TClassType, TDataType> ID<TDataType>(Expression<Func<TClassType, TDataType>> expression)
        {
            if (IDProperty!= null)
                throw new NotSupportedException("Multiple IDs are not currently supported");
            Setup();
            var Return = new ID<TClassType, TDataType>(expression, this);
            IDProperty = Return;
            return Return;
        }

        /// <summary>
        /// Creates an ID object
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <returns>ID object</returns>
        public IID<TClassType, string> ID(Expression<Func<TClassType, string>> expression)
        {
            if (IDProperty != null)
                throw new NotSupportedException("Multiple IDs are not currently supported");
            Setup();
            var Return = new StringID<TClassType>(expression, this);
            IDProperty = Return;
            return Return;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// Should be overwritten to initialize values in the 
        /// database. This is run after the initial setup but prior to
        /// returning to the user.
        /// </summary>
        public virtual void Initialize() { }

        #endregion

        #region Reference

        /// <summary>
        /// Creates a reference object
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>A reference object</returns>
        public IReference<TClassType, TDataType> Reference<TDataType>(Expression<Func<TClassType, TDataType>> expression)
        {
            Setup();
            var Return = new Reference<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        /// <summary>
        /// Creates a reference object
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <returns>A reference object</returns>
        public IReference<TClassType, string> Reference(Expression<Func<TClassType, string>> expression)
        {
            Setup();
            var Return = new StringReference<TClassType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        #endregion

        #region Map

        /// <summary>
        /// Creates a map object
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>The map object</returns>
        public IMap<TClassType, TDataType> Map<TDataType>(Expression<Func<TClassType, TDataType>> expression) where TDataType : class,new()
        {
            Setup();
            var Return = new Map<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        #endregion

        #region ManyToOne

        /// <summary>
        /// Creates a many to one
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>The many to one object</returns>
        public IManyToOne<TClassType, TDataType> ManyToOne<TDataType>(Expression<Func<TClassType, TDataType>> expression) where TDataType : class,new()
        {
            Setup();
            var Return = new ManyToOne<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        /// <summary>
        /// Creates a many to one
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>The many to one object</returns>
        public IIEnumerableManyToOne<TClassType, TDataType> ManyToOne<TDataType>(Expression<Func<TClassType, IEnumerable<TDataType>>> expression) where TDataType : class,new()
        {
            Setup();
            var Return = new EnumerableManyToOne<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        /// <summary>
        /// Creates a many to one
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>The many to one object</returns>
        public IListManyToOne<TClassType, TDataType> ManyToOne<TDataType>(Expression<Func<TClassType, List<TDataType>>> expression) where TDataType : class,new()
        {
            Setup();
            var Return = new ListManyToOne<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        #endregion

        #region ManyToMany

        /// <summary>
        /// Creates a many to many object
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>The many to many object</returns>
        public IManyToMany<TClassType, TDataType> ManyToMany<TDataType>(Expression<Func<TClassType, IEnumerable<TDataType>>> expression) where TDataType : class,new()
        {
            Setup();
            var Return = new ManyToMany<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        /// <summary>
        /// Creates a many to many object
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>The many to many object</returns>
        public IListManyToMany<TClassType, TDataType> ManyToMany<TDataType>(Expression<Func<TClassType, List<TDataType>>> expression) where TDataType : class,new()
        {
            Setup();
            var Return = new ListManyToMany<TClassType, TDataType>(expression, this);
            Properties.Add(Return);
            return Return;
        }

        #endregion

        //#region SetupValidation

        ///// <summary>
        ///// Used to set up validation, using the class used internally by the system
        ///// </summary>
        ///// <param name="Validator">Validator</param>
        //public virtual void SetupValidation(Validator<ClassType> Validator) { }

        //#endregion

        #endregion

        #region Properties

        /// <summary>
        /// Table name
        /// </summary>
        public virtual string TableName
        {
            get
            {
                if (string.IsNullOrEmpty(_tableName))
                {
                    return Prefix + typeof(TClassType).Name + Suffix;
                }
                return _tableName;
            }
            private set { _tableName = value; }
        }

        private string _tableName = "";

        /// <summary>
        /// Suffix used
        /// </summary>
        public virtual string Suffix { get; private set; }

        /// <summary>
        /// Prefix used
        /// </summary>
        public virtual string Prefix { get; private set; }

        /// <summary>
        /// Database config type
        /// </summary>
        public virtual Type DatabaseConfigType { get { return typeof(TDatabaseType); } }

        /// <summary>
        /// List of properties
        /// </summary>
        public virtual ICollection<IProperty> Properties { get; private set; }

        /// <summary>
        /// ID property
        /// </summary>
        public virtual IProperty IDProperty { get; set; }

        /// <summary>
        /// Mapping manager
        /// </summary>
        public virtual IMappingManager Manager { get; set; }

        /// <summary>
        /// Object type
        /// </summary>
        public virtual Type ObjectType { get { return typeof(TClassType); } }

        /// <summary>
        /// Default any command
        /// </summary>
        public virtual Command AnyCommand { get; set; }

        /// <summary>
        /// Default all command
        /// </summary>
        public virtual Command AllCommand { get; set; }

        /// <summary>
        /// The order in which the mappings are initialized
        /// </summary>
        public virtual int Order { get; set; }

        #endregion
    }
}