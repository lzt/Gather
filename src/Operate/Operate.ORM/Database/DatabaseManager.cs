﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Operate.ExtensionMethods;
using Operate.ORM.Mapping.Interfaces;
using Operate.ORM.QueryProviders.Interfaces;
using Operate.SQL.DataClasses.Enums;
#endregion

namespace Operate.ORM.Database
{
    /// <summary>
    /// Database manager (handles generation and mapping of stored procedures)
    /// </summary>
    public class DatabaseManager
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mappings">Mappings associated with databases (provided by query provider)</param>
        public DatabaseManager(ListMapping<IDatabase, IMapping> mappings)
        {
            Mappings = mappings;
            DatabaseStructures = new List<SQL.DataClasses.Database>();
        }

        #endregion

        #region Functions

        #region Setup

        /// <summary>
        /// Sets up the databases
        /// </summary>
        public virtual void Setup()
        {
            foreach (IDatabase key in Mappings.Keys)
            {
                if (key.Update)
                {
                    string databaseName = Regex.Match(key.ConnectionString, "Initial Catalog=(.*?;)").Value.Replace("Initial Catalog=", "").Replace(";", "");
                    var tempDatabase = new SQL.DataClasses.Database(databaseName);
                    SetupFunctions(tempDatabase);
                    SetupTables(key, tempDatabase);
                    SetupJoiningTables(key, tempDatabase);
                    SetupAuditTables(key, tempDatabase);

                    DatabaseStructures.Add(tempDatabase);
                    foreach (var table in tempDatabase.Tables)
                    {
                        table.SetupForeignKeys();
                    }
                    var currentDatabase = SQL.SQLServer.SQLServer.GetDatabaseStructure(key.ConnectionString);
                    SQL.SQLServer.SQLServer.UpdateDatabase(tempDatabase, currentDatabase, key.ConnectionString);

                    foreach (IMapping mapping in Mappings[key])
                    {
                        foreach (IProperty property in mapping.Properties)
                        {
                            property.SetupLoadCommands();
                        }
                    }
                }
            }
        }

        #endregion

        #region SetupAuditTables

        private static void SetupAuditTables(IDatabase key, SQL.DataClasses.Database tempDatabase)
        {
            if (!key.Audit)
                return;
            var tempTables = new List<SQL.DataClasses.Table>();
            foreach (var table in tempDatabase.Tables)
            {
                tempTables.Add(SetupAuditTables(table));
                SetupInsertUpdateTrigger(table);
                SetupDeleteTrigger(table);
            }
            tempDatabase.Tables.Add(tempTables);
        }

        private static SQL.DataClasses.Table SetupAuditTables(SQL.DataClasses.Table table)
        {
            var auditTable = new SQL.DataClasses.Table(table.Name + "Audit", table.ParentDatabase);
            auditTable.AddColumn("ID", DbType.Int32, 0, false, true, true, true, false, "", "", 0);
            auditTable.AddColumn("AuditType", SqlDbType.NVarChar.To(DbType.Int32), 1, false, false, false, false, false, "", "", "");
            foreach (SQL.DataClasses.Interfaces.IColumn column in table.Columns)
                auditTable.AddColumn(column.Name, column.DataType, column.Length, column.Nullable, false, false, false, false, "", "", "");
            return auditTable;
        }

        #endregion

        #region SetupDeleteTrigger

        private static void SetupDeleteTrigger(SQL.DataClasses.Table table)
        {
            var columns = new StringBuilder();
            var builder = new StringBuilder();
            builder.Append("CREATE TRIGGER dbo.").Append(table.Name).Append("_Audit_D ON dbo.")
                .Append(table.Name).Append(" FOR DELETE AS IF @@rowcount=0 RETURN")
                .Append(" INSERT INTO dbo.").Append(table.Name).Append("Audit").Append("(");
            string splitter = "";
            foreach (var column in table.Columns)
            {
                columns.Append(splitter).Append(column.Name);
                splitter = ",";
            }
            builder.Append(columns);
            builder.Append(",AuditType) SELECT ");
            builder.Append(columns);
            builder.Append(",'D' FROM deleted");
            table.AddTrigger(table.Name + "_Audit_D", builder.ToString(), TriggerType.INSERT);
        }

        #endregion

        #region SetupInsertUpdateTrigger

        private static void SetupInsertUpdateTrigger(SQL.DataClasses.Table table)
        {
            var columns = new StringBuilder();
            var builder = new StringBuilder();
            builder.Append("CREATE TRIGGER dbo.").Append(table.Name).Append("_Audit_IU ON dbo.")
                .Append(table.Name).Append(" FOR INSERT,UPDATE AS IF @@rowcount=0 RETURN declare @AuditType")
                .Append(" char(1) declare @DeletedCount int SELECT @DeletedCount=count(*) FROM DELETED IF @DeletedCount=0")
                .Append(" BEGIN SET @AuditType='I' END ELSE BEGIN SET @AuditType='U' END")
                .Append(" INSERT INTO dbo.").Append(table.Name).Append("Audit").Append("(");
            string splitter = "";
            foreach (var column in table.Columns)
            {
                columns.Append(splitter).Append(column.Name);
                splitter = ",";
            }
            builder.Append(columns);
            builder.Append(",AuditType) SELECT ");
            builder.Append(columns);
            builder.Append(",@AuditType FROM inserted");
            table.AddTrigger(table.Name + "_Audit_IU", builder.ToString(), TriggerType.INSERT);
        }

        #endregion

        #region SetupJoiningTables

        private void SetupJoiningTables(IDatabase key, SQL.DataClasses.Database tempDatabase)
        {
            foreach (IMapping mapping in Mappings[key])
            {
                foreach (IProperty property in mapping.Properties)
                {
                    if (property is IMap)
                    {
                        IMapping mapMapping = Mappings[key].FirstOrDefault(x => x.ObjectType == property.Type);
                        if (mapMapping != null)
                            tempDatabase[mapping.TableName].AddColumn(property.FieldName,
                                                                      mapMapping.IDProperty.Type.To(DbType.Int32),
                                                                      mapMapping.IDProperty.MaxLength,
                                                                      !property.NotNull,
                                                                      false,
                                                                      property.Index,
                                                                      false,
                                                                      false,
                                                                      mapMapping.TableName,
                                                                      mapMapping.IDProperty.FieldName,
                                                                      "",
                                                                      false,
                                                                      false,
                                                                      mapping.Properties.Count(x => x.Type == property.Type) == 1 && mapping.ObjectType != property.Type);
                    }
                    else if (property is IManyToOne || property is IManyToMany || property is IIEnumerableManyToOne||property is IListManyToMany||property is IListManyToOne)
                    {
                        SetupJoiningTablesEnumerable(mapping, property, key, tempDatabase);
                    }
                }
            }
        }

        #endregion

        #region SetupJoiningTablesEnumerable

        private void SetupJoiningTablesEnumerable(IMapping mapping, IProperty property, IDatabase key, SQL.DataClasses.Database tempDatabase)
        {
            if (tempDatabase.Tables.FirstOrDefault(x => x.Name == property.TableName) != null)
                return;
            IMapping mapMapping = Mappings[key].FirstOrDefault(x => x.ObjectType == property.Type);
            if (mapMapping == mapping)
            {
                tempDatabase.AddTable(property.TableName);
                tempDatabase[property.TableName].AddColumn("ID_", DbType.Int32, 0, false, true, true, true, false, "", "", "");
                if (mapping != null)
                    tempDatabase[property.TableName].AddColumn(mapping.TableName + mapping.IDProperty.FieldName,
                                                               mapping.IDProperty.Type.To(DbType.Int32),
                                                               mapping.IDProperty.MaxLength,
                                                               false,
                                                               false,
                                                               false,
                                                               false,
                                                               false,
                                                               mapping.TableName,
                                                               mapping.IDProperty.FieldName,
                                                               "");
                if (mapMapping != null)
                    tempDatabase[property.TableName].AddColumn(mapMapping.TableName + mapMapping.IDProperty.FieldName + "2",
                                                               mapMapping.IDProperty.Type.To(DbType.Int32),
                                                               mapMapping.IDProperty.MaxLength,
                                                               false,
                                                               false,
                                                               false,
                                                               false,
                                                               false,
                                                               mapMapping.TableName,
                                                               mapMapping.IDProperty.FieldName,
                                                               "");
            }
            else
            {
                tempDatabase.AddTable(property.TableName);
                tempDatabase[property.TableName].AddColumn("ID_", DbType.Int32, 0, false, true, true, true, false, "", "", "");
                tempDatabase[property.TableName].AddColumn(mapping.TableName + mapping.IDProperty.FieldName,
                    mapping.IDProperty.Type.To(DbType.Int32),
                    mapping.IDProperty.MaxLength,
                    false,
                    false,
                    false,
                    false,
                    false,
                    mapping.TableName,
                    mapping.IDProperty.FieldName,
                    "",
                    true);
                if (mapMapping != null)
                    tempDatabase[property.TableName].AddColumn(mapMapping.TableName + mapMapping.IDProperty.FieldName,
                                                               mapMapping.IDProperty.Type.To(DbType.Int32),
                                                               mapMapping.IDProperty.MaxLength,
                                                               false,
                                                               false,
                                                               false,
                                                               false,
                                                               false,
                                                               mapMapping.TableName,
                                                               mapMapping.IDProperty.FieldName,
                                                               "",
                                                               true);
            }
        }

        #endregion

        #region SetupTables

        private void SetupTables(IDatabase key, SQL.DataClasses.Database tempDatabase)
        {
            foreach (IMapping mapping in Mappings[key])
            {
                tempDatabase.AddTable(mapping.TableName);
                SetupProperties(tempDatabase[mapping.TableName], mapping);
            }
        }

        #endregion

        #region SetupProperties

        private static void SetupProperties(SQL.DataClasses.Table table, IMapping mapping)
        {
            table.AddColumn(mapping.IDProperty.FieldName,
                mapping.IDProperty.Type.To(DbType.Int32),
                mapping.IDProperty.MaxLength,
                mapping.IDProperty.NotNull,
                mapping.IDProperty.AutoIncrement,
                mapping.IDProperty.Index,
                true,
                mapping.IDProperty.Unique,
                "",
                "",
                "");
            foreach (IProperty property in mapping.Properties)
            {
                if (!(property is IManyToMany || property is IManyToOne || property is IMap || property is IIEnumerableManyToOne || property is IListManyToMany || property is IListManyToOne))
                {
                    table.AddColumn(property.FieldName,
                    property.Type.To(DbType.Int32),
                    property.MaxLength,
                    !property.NotNull,
                    property.AutoIncrement,
                    property.Index,
                    false,
                    property.Unique,
                    "",
                    "",
                    "");
                }
            }
        }

        #endregion

        #region SetupFunctions

        private static void SetupFunctions(SQL.DataClasses.Database database)
        {
            database.AddFunction("Split", "CREATE FUNCTION dbo.Split(@List nvarchar(MAX),@SplitOn nvarchar(5)) RETURNS @ReturnValue table(Id int identity(1,1),Value nvarchar(200)) AS  BEGIN While (Charindex(@SplitOn,@List)>0) Begin Insert Into @ReturnValue (value) Select Value = ltrim(rtrim(Substring(@List,1,Charindex(@SplitOn,@List)-1))) Set @List = Substring(@List,Charindex(@SplitOn,@List)+len(@SplitOn),len(@List)) End Insert Into @ReturnValue (Value) Select Value = ltrim(rtrim(@List)) Return END");
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Mappings associated to databases
        /// </summary>
        public ListMapping<IDatabase, IMapping> Mappings { get;private set; }

        /// <summary>
        /// List of database structures
        /// </summary>
        public ICollection<SQL.DataClasses.Database> DatabaseStructures { get;private set; }

        #endregion
    }
}