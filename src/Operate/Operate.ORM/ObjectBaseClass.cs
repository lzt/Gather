﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Operate.EventArgs;
using Operate.ExtensionMethods;
using Operate.ORM.Interfaces;
using Operate.SQL.ParameterTypes.Interfaces;
using Operate.Validation.ExtensionMethods;
#endregion

namespace Operate.ORM
{
    /// <summary>
    /// Object base class helper. This is not required but automatically
    /// sets up basic functions and properties to simplify things a bit.
    /// </summary>
    /// <typeparam name="TIDType">ID type</typeparam>
    /// <typeparam name="TObjectType">Object type (must be the child object type)</typeparam>
    public abstract class ObjectBaseClass<TObjectType, TIDType> : IComparable, IComparable<TObjectType>, IObject<TIDType>
        where TObjectType : ObjectBaseClass<TObjectType, TIDType>, new()
        where TIDType : IComparable
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        protected ObjectBaseClass()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Active = true;
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Static Functions

        #region Any

        /// <summary>
        /// Loads the item based on the ID
        /// </summary>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <returns>The specified item</returns>
        public static TObjectType Any(params IParameter[] Params)
        {
            return Any(ORM.CreateSession(), Params);
        }

        /// <summary>
        /// Loads the item based on the ID
        /// </summary>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <returns>The specified item</returns>
        public static TObjectType Any(Session session, params IParameter[] Params)
        {
            var instance = new TObjectType();
            var e = new LoadingEventArgs { Content = Params };
            instance.OnLoading(e);
            if (!e.Stop)
            {
                instance = session.Any<TObjectType>(Params);
                if (instance != null)
                    instance.OnLoaded(new LoadedEventArgs());
            }
            return instance;
        }

        /// <summary>
        /// Loads the item based on the ID
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="commandType">Command type</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <returns>The specified item</returns>
        public static TObjectType Any(string command, CommandType commandType, params IParameter[] Params)
        {
            return Any(ORM.CreateSession(), command, commandType, Params);
        }

        /// <summary>
        /// Loads the item based on the ID
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="commandType">Command type</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <returns>The specified item</returns>
        public static TObjectType Any(Session session, string command, CommandType commandType, params IParameter[] Params)
        {
            var instance = new TObjectType();
            var e = new LoadingEventArgs { Content = Params };
            instance.OnLoading(e);
            if (!e.Stop)
            {
                instance = session.Any<TObjectType>(command, commandType, Params);
                if (instance != null)
                    instance.OnLoaded(new LoadedEventArgs());
            }
            return instance;
        }

        #endregion

        #region All

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> All(params IParameter[] Params)
        {
            return All(ORM.CreateSession(), Params);
        }

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> All(Session session, params IParameter[] Params)
        {
            IEnumerable<TObjectType> instance = new List<TObjectType>();
            var e = new LoadingEventArgs();
            OnLoading(null, e);
            if (!e.Stop)
            {
                instance = session.All<TObjectType>(Params);
                foreach (TObjectType item in instance)
                {
                    item.OnLoaded(new LoadedEventArgs());
                }
            }
            return instance;
        }

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="commandType">Command type</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> All(string command, CommandType commandType, params IParameter[] Params)
        {
            return All(ORM.CreateSession(), command, commandType, Params);
        }

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="commandType">Command type</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> All(Session session, string command, CommandType commandType, params IParameter[] Params)
        {
            IEnumerable<TObjectType> instance = new List<TObjectType>();
            var e = new LoadingEventArgs();
            OnLoading(null, e);
            if (!e.Stop)
            {
                instance = session.All<TObjectType>(command, commandType, Params);
                foreach (TObjectType item in instance)
                {
                    item.OnLoaded(new LoadedEventArgs());
                }
            }
            return instance;
        }

        #endregion

        #region Paged

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="orderBy">What the data is ordered by</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (0 based)</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> Paged(string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] Params)
        {
            return Paged(ORM.CreateSession(), orderBy, pageSize, currentPage, Params);
        }

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="orderBy">What the data is ordered by</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (0 based)</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> Paged(Session session, string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] Params)
        {
            IEnumerable<TObjectType> instance = new List<TObjectType>();
            var e = new LoadingEventArgs();
            OnLoading(null, e);
            if (!e.Stop)
            {
                instance = session.Paged<TObjectType>("*", orderBy, pageSize, currentPage, Params);
                foreach (TObjectType item in instance)
                {
                    item.OnLoaded(new LoadedEventArgs());
                }
            }
            return instance;
        }

        #endregion

        #region PagedCommand

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="orderBy">What the data is ordered by</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (0 based)</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="command">Command to run</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> PagedCommand(string command, string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] Params)
        {
            return PagedCommand(ORM.CreateSession(), command, orderBy, pageSize, currentPage, Params);
        }

        /// <summary>
        /// Loads the items based on type
        /// </summary>
        /// <param name="orderBy">What the data is ordered by</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="currentPage">Current page (0 based)</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <param name="command">Command to run</param>
        /// <returns>All items that fit the specified query</returns>
        public static IEnumerable<TObjectType> PagedCommand(Session session, string command, string orderBy = "", int pageSize = 25, int currentPage = 0, params IParameter[] Params)
        {
            IEnumerable<TObjectType> instance = new List<TObjectType>();
            var e = new LoadingEventArgs();
            OnLoading(null, e);
            if (!e.Stop)
            {
                instance = session.PagedCommand<TObjectType>(command, orderBy, pageSize, currentPage, Params);
                foreach (TObjectType item in instance)
                {
                    item.OnLoaded(new LoadedEventArgs());
                }
            }
            return instance;
        }

        #endregion

        #region PageCount

        /// <summary>
        /// Gets the page count based on page size
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <returns>All items that fit the specified query</returns>
        public static int PageCount(int pageSize = 25, params IParameter[] Params)
        {
            return PageCount(ORM.CreateSession(), pageSize, Params);
        }

        /// <summary>
        /// Gets the page count based on page size
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <returns>All items that fit the specified query</returns>
        public static int PageCount(Session session, int pageSize = 25, params IParameter[] Params)
        {
            return session.PageCount<TObjectType>(pageSize, Params);
        }

        /// <summary>
        /// Gets the page count based on page size
        /// </summary>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="command">Command to run</param>
        /// <returns>All items that fit the specified query</returns>
        public static int PageCount(string command, params IParameter[] Params)
        {
            return PageCount(command, 0, Params);
        }

        /// <summary>
        /// Gets the page count based on page size
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="command">Command to run</param>
        /// <returns>All items that fit the specified query</returns>
        public static int PageCount(string command, int pageSize = 25, params IParameter[] Params)
        {
            return PageCount(ORM.CreateSession(), command, pageSize, Params);
        }

        /// <summary>
        /// Gets the page count based on page size
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="Params">Parameters used to specify what to load</param>
        /// <param name="session">ORM session variable</param>
        /// <param name="command">Command to run</param>
        /// <returns>All items that fit the specified query</returns>
        public static int PageCount(Session session, string command, int pageSize = 25, params IParameter[] Params)
        {
            return session.PageCount<TObjectType>(command, pageSize, Params);
        }

        #endregion

        #region Save

        /// <summary>
        /// Saves a list of objects
        /// </summary>
        /// <param name="objects">List of objects</param>
        public static void Save(IEnumerable<TObjectType> objects)
        {
            Save(objects, ORM.CreateSession());
        }

        /// <summary>
        /// Saves a list of objects
        /// </summary>
        /// <param name="objects">List of objects</param>
        /// <param name="session">ORM session variable</param>
        public static void Save(IEnumerable<TObjectType> objects, Session session)
        {
            foreach (TObjectType Object in objects)
            {
                Object.SetupObject();
                Object.Validate();
                Object.Save(session);
            }
        }

        #endregion

        #region Scalar

        /// <summary>
        /// Runs a supplied scalar function and returns the result
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <returns>The scalar value returned by the command</returns>
        public static TDataType Scalar<TDataType>(string command, CommandType commandType, params IParameter[] parameters)
        {
            return Scalar<TDataType>(ORM.CreateSession(), command, commandType, parameters);
        }

        /// <summary>
        /// Runs a scalar command using the specified aggregate function
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="aggregateFunction">Aggregate function</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>The scalar value returned by the command</returns>
        public static TDataType Scalar<TDataType>(string aggregateFunction, params IParameter[] parameters)
        {
            return Scalar<TDataType>(ORM.CreateSession(), aggregateFunction, parameters);
        }

        /// <summary>
        /// Runs a supplied scalar function and returns the result
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Parameters to search by</param>
        /// <param name="command">Command to get the page count of</param>
        /// <param name="session">ORM session variable</param>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <returns>The scalar value returned by the command</returns>
        public static TDataType Scalar<TDataType>(Session session, string command, CommandType commandType, params IParameter[] parameters)
        {
            return session.Scalar<TObjectType, TDataType>(command, commandType, parameters);
        }

        /// <summary>
        /// Runs a scalar command using the specified aggregate function
        /// </summary>
        /// <typeparam name="TDataType">Data type</typeparam>
        /// <param name="aggregateFunction">Aggregate function</param>
        /// <param name="parameters">Parameters</param>
        /// <param name="session">Session object</param>
        /// <returns>The scalar value returned by the command</returns>
        public static TDataType Scalar<TDataType>(Session session, string aggregateFunction, params IParameter[] parameters)
        {
            return session.Scalar<TObjectType, TDataType>(aggregateFunction, parameters);
        }

        #endregion

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the object for saving purposes
        /// </summary>
        public virtual void SetupObject()
        {
            DateModified = DateTime.Now;
        }

        /// <summary>
        /// Saves the item (if it already exists, it updates the item.
        /// Otherwise it inserts the item)
        /// </summary>
        public virtual void Save()
        {
            Save(ORM.CreateSession());
        }

        /// <summary>
        /// Deletes the item
        /// </summary>
        public virtual void Delete()
        {
            Delete(ORM.CreateSession());
        }

        /// <summary>
        /// Saves the item (if it already exists, it updates the item.
        /// Otherwise it inserts the item)
        /// </summary>
        /// <param name="session">ORM session variable</param>
        public virtual void Save(Session session)
        {
            var e = new SavingEventArgs();
            OnSaving(e);

            if (!e.Stop)
            {
                SetupObject();
                this.Validate();
                session.Save<TObjectType, TIDType>((TObjectType)this);
                var x = new SavedEventArgs();
                OnSaved(x);
            }
        }

        /// <summary>
        /// Deletes the item
        /// </summary>
        /// <param name="session">ORM session variable</param>
        public virtual void Delete(Session session)
        {
            var e = new DeletingEventArgs();
            OnDeleting(e);
            if (!e.Stop)
            {
                session.Delete((TObjectType)this);
                var x = new DeletedEventArgs();
                OnDeleted(x);
            }
        }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Returns the hash of this item
        /// </summary>
        /// <returns>the int hash of the item</returns>
        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        /// <summary>
        /// Determines if two items are equal
        /// </summary>
        /// <param name="obj">The object to compare this to</param>
        /// <returns>true if they are the same, false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj.GetType() == GetType())
            {
                return obj.GetHashCode() == GetHashCode();
            }

            return false;
        }

        /// <summary>
        /// The &lt; operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if the first item is less than the second, false otherwise</returns>
        public static bool operator <(ObjectBaseClass<TObjectType, TIDType> first, ObjectBaseClass<TObjectType, TIDType> second)
        {
            if (ReferenceEquals(first, second))
                return false;
            if ((object)first == null || (object)second == null)
                return false;
            return first.GetHashCode() < second.GetHashCode();
        }

        /// <summary>
        /// The &gt; operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>True if the first item is greater than the second, false otherwise</returns>
        public static bool operator >(ObjectBaseClass<TObjectType, TIDType> first, ObjectBaseClass<TObjectType, TIDType> second)
        {
            if (ReferenceEquals(first, second))
                return false;
            if ((object)first == null || (object)second == null)
                return false;
            return first.GetHashCode() > second.GetHashCode();
        }

        /// <summary>
        /// The == operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>true if the first and second item are the same, false otherwise</returns>
        public static bool operator ==(ObjectBaseClass<TObjectType, TIDType> first, ObjectBaseClass<TObjectType, TIDType> second)
        {
            if (ReferenceEquals(first, second))
                return true;

            if ((object)first == null || (object)second == null)
                return false;

            return first.GetHashCode() == second.GetHashCode();
        }

        /// <summary>
        /// != operator
        /// </summary>
        /// <param name="first">First item</param>
        /// <param name="second">Second item</param>
        /// <returns>returns true if they are not equal, false otherwise</returns>
        public static bool operator !=(ObjectBaseClass<TObjectType, TIDType> first, ObjectBaseClass<TObjectType, TIDType> second)
        {
            return !(first == second);
        }

        #endregion

        #region Events

        /// <summary>
        /// Called when the object is saved
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static EventHandler<SavedEventArgs> Saved;

        /// <summary>
        /// Called when the item is Saved
        /// </summary>
        /// <param name="e">SavedEventArgs item</param>
        protected virtual void OnSaved(SavedEventArgs e)
        {
            Saved.Raise(this, e);
        }

        /// <summary>
        /// Called when the object is deleted
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static EventHandler<DeletedEventArgs> Deleted;

        /// <summary>
        /// Called when the item is Deleted
        /// </summary>
        /// <param name="e">DeletedEventArgs item</param>
        protected virtual void OnDeleted(DeletedEventArgs e)
        {
            Deleted.Raise(this, e);
        }

        /// <summary>
        /// Called prior to an object is saving
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static EventHandler<SavingEventArgs> Saving;

        /// <summary>
        /// Called when the item is Saving
        /// </summary>
        /// <param name="e">SavingEventArgs item</param>
        protected virtual void OnSaving(SavingEventArgs e)
        {
            Saving.Raise(this, e);
        }

        /// <summary>
        /// Called prior to an object is deleting
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static EventHandler<DeletingEventArgs> Deleting;

        /// <summary>
        /// Called when the item is Deleting
        /// </summary>
        /// <param name="e">DeletingEventArgs item</param>
        protected virtual void OnDeleting(DeletingEventArgs e)
        {
            Deleting.Raise(this, e);
        }

        /// <summary>
        /// Called prior to an object is loading
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static EventHandler<LoadingEventArgs> Loading;

        /// <summary>
        /// Called when the item is Loading
        /// </summary>
        /// <param name="e">LoadingEventArgs item</param>
        protected virtual void OnLoading(LoadingEventArgs e)
        {
            Loading.Raise(this, e);
        }

        /// <summary>
        /// Called when the item is Loading
        /// </summary>
        /// <param name="e">LoadingEventArgs item</param>
        /// <param name="sender">Sender item</param>
        protected static void OnLoading(object sender, LoadingEventArgs e)
        {
            Loading.Raise(sender, e);
        }

        /// <summary>
        /// Called prior to an object being loaded
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static EventHandler<LoadedEventArgs> Loaded;

        /// <summary>
        /// Called when the item is Loaded
        /// </summary>
        /// <param name="e">LoadedEventArgs item</param>
        protected virtual void OnLoaded(LoadedEventArgs e)
        {
            Loaded.Raise(this, e);
        }

        /// <summary>
        /// Called when the item is Loaded
        /// </summary>
        /// <param name="e">LoadedEventArgs item</param>
        /// <param name="sender">Sender item</param>
        protected static void OnLoaded(object sender, LoadedEventArgs e)
        {
            Loaded.Raise(sender, e);
        }

        #endregion

        #region IObject Members

        /// <summary>
        /// ID for the object
        /// </summary>
        public virtual TIDType ID { get; set; }

        /// <summary>
        /// Date last modified
        /// </summary>
        public virtual DateTime DateModified { get; set; }

        /// <summary>
        /// Date object was created
        /// </summary>
        public virtual DateTime DateCreated { get; set; }

        /// <summary>
        /// Is the object active?
        /// </summary>
        public virtual bool Active { get; set; }

        #endregion

        #region IComparable Functions

        /// <summary>
        /// Compares the object to another object
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>0 if they are equal, -1 if this is smaller, 1 if it is larger</returns>
        public int CompareTo(object obj)
        {
            if (obj is ObjectBaseClass<TObjectType, TIDType>)
                return CompareTo((TObjectType)obj);
            return -1;
        }

        /// <summary>
        /// Compares the object to another object
        /// </summary>
        /// <param name="other">Object to compare to</param>
        /// <returns>0 if they are equal, -1 if this is smaller, 1 if it is larger</returns>
        public virtual int CompareTo(TObjectType other)
        {
            return other.ID.CompareTo(ID);
        }

        #endregion
    }
}