﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Operate.ORM.Aspect.Interfaces;
using Operate.ORM.Mapping.Interfaces;
using Operate.Reflection.AOP.Interfaces;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;
using Operate.SQL.ParameterTypes;
using Operate.SQL.ParameterTypes.Interfaces;

#endregion

namespace Operate.ORM.Aspect
{
    /// <summary>
    /// ORM Aspect (used internally)
    /// </summary>
    public class ORMAspect : IAspect
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ORMAspect(ListMapping<Type, IMapping> mappings)
        {
            InterfacesUsing = new List<Type> {typeof (IORMObject)};
            ClassMappings = mappings;
        }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the start method
        /// </summary>
        /// <param name="method">Method builder to use</param>
        /// <param name="baseType">Base type</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public void SetupStartMethod(IMethodBuilder method, Type baseType)
        {
            method.SetCurrentMethod();
            if (ClassMappings.ContainsKey(baseType)
                && method.Name.StartsWith("set_", StringComparison.InvariantCulture))
            {
                foreach (IMapping mapping in ClassMappings[baseType])
                {
                    string propertyName = method.Name.Replace("set_", "");
                    IProperty property = mapping.Properties.FirstOrDefault(x => x.Name == propertyName);
                    if (property != null)
                    {
                        Reflection.Emit.FieldBuilder field = Fields.Find(x => x.Name == property.DerivedFieldName);
                        if (field != null)
                            field.Assign(method.Parameters.ElementAt(1));
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Sets up the code at the end of the method
        /// </summary>
        /// <param name="method">Method builder to use</param>
        /// <param name="baseType">Base type</param>
        /// <param name="returnValue">Return value (if there is one)</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public void SetupEndMethod(IMethodBuilder method, Type baseType, VariableBase returnValue)
        {
            method.SetCurrentMethod();
            if (ClassMappings.ContainsKey(baseType)
                && method.Name.StartsWith("get_", StringComparison.InvariantCulture))
            {
                foreach (IMapping mapping in ClassMappings[baseType])
                {
                    string propertyName = method.Name.Replace("get_", "");
                    IProperty property = mapping.Properties.FirstOrDefault(x => x.Name == propertyName);
                    if (property != null)
                    {
                        if (property is IManyToOne || property is IMap)
                            SetupSingleProperty(method, baseType, returnValue, property, mapping);
                        else if (property is IIEnumerableManyToOne || property is IManyToMany)
                            SetupIEnumerableProperty(method, baseType, returnValue, property, mapping);
                        else if (property is IListManyToMany || property is IListManyToOne)
                            SetupListProperty(method, baseType, returnValue, property, mapping);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Sets up the exception method
        /// </summary>
        /// <param name="method">Method builder</param>
        /// <param name="baseType">Base type</param>
        public void SetupExceptionMethod(IMethodBuilder method, Type baseType)
        {

        }

        /// <summary>
        /// Sets up an object
        /// </summary>
        /// <param name="Object">Object to set up</param>
        public void Setup(object Object)
        {

        }

        /// <summary>
        /// Set up interfaces
        /// </summary>
        /// <param name="typeBuilder">Type builder to use</param>
        public void SetupInterfaces(Reflection.Emit.TypeBuilder typeBuilder)
        {
            Fields = new List<Reflection.Emit.FieldBuilder>();
            SessionField = CreateProperty(typeBuilder, "Session0", typeof(Session));
            CreateConstructor(typeBuilder);
            SetupFields(typeBuilder);
        }

        /// <summary>
        /// Sets up the fields needed to store the data for lazy loading
        /// </summary>
        /// <param name="typeBuilder"></param>
        private void SetupFields(Reflection.Emit.TypeBuilder typeBuilder)
        {
            if (ClassMappings.ContainsKey(typeBuilder.BaseClass))
            {
                foreach (IMapping mapping in ClassMappings[typeBuilder.BaseClass])
                {
                    foreach (IProperty property in mapping.Properties)
                    {
                        if (property is IManyToOne || property is IMap)
                        {
                            if (Fields.FirstOrDefault(x => x.Name == property.DerivedFieldName) == null)
                                Fields.Add(typeBuilder.CreateField(property.DerivedFieldName, property.Type));
                        }
                        else if (property is IIEnumerableManyToOne || property is IManyToMany)
                        {
                            if (Fields.FirstOrDefault(x => x.Name == property.DerivedFieldName) == null)
                                Fields.Add(typeBuilder.CreateField(property.DerivedFieldName, typeof(IEnumerable<>).MakeGenericType(property.Type)));
                        }
                        else if (property is IListManyToOne || property is IListManyToMany)
                        {
                            if (Fields.FirstOrDefault(x => x.Name == property.DerivedFieldName) == null)
                                Fields.Add(typeBuilder.CreateField(property.DerivedFieldName, typeof(List<>).MakeGenericType(property.Type)));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates the default constructor
        /// </summary>
        /// <param name="typeBuilder">Type builder</param>
        private static void CreateConstructor(Reflection.Emit.TypeBuilder typeBuilder)
        {
            IMethodBuilder constructor = typeBuilder.CreateConstructor();
            {
                constructor.SetCurrentMethod();
                constructor.This.Call(typeBuilder.BaseClass.GetConstructor(Type.EmptyTypes));
                constructor.Return();
            }
        }

        /// <summary>
        /// Creates a default property
        /// </summary>
        /// <param name="typeBuilder">Type builder</param>
        /// <param name="name">Name of the property</param>
        /// <param name="propertyType">Property type</param>
        /// <returns>The property builder</returns>
        private static IPropertyBuilder CreateProperty(Reflection.Emit.TypeBuilder typeBuilder, string name, Type propertyType)
        {
            return typeBuilder.CreateDefaultProperty(name, propertyType, PropertyAttributes.SpecialName,
                MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Public,
                MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.Public);
        }

        /// <summary>
        /// Sets up a property (List)
        /// </summary>
        /// <param name="method">Method builder</param>
        /// <param name="baseType">Base type for the object</param>
        /// <param name="returnValue">Return value</param>
        /// <param name="property">Property info</param>
        /// <param name="mapping">Mapping info</param>
        private void SetupListProperty(IMethodBuilder method, Type baseType, VariableBase returnValue, IProperty property, IMapping mapping)
        {
            var field = Fields.Find(x => x.Name == property.DerivedFieldName);
            Reflection.Emit.Commands.If if1 = method.If((VariableBase)SessionField, Reflection.Emit.Enums.Comparison.NotEqual, null);
            {
                var if2 = method.If(field, Reflection.Emit.Enums.Comparison.Equal, null);
                {
                    //Load data
                    VariableBase idValue = method.This.Call(baseType.GetProperty(mapping.IDProperty.Name).GetGetMethod());
                    VariableBase idParameter = method.NewObj(typeof(EqualParameter<>).MakeGenericType(mapping.IDProperty.Type), new object[] { idValue, "ID", "@" });
                    VariableBase propertyList = method.NewObj(typeof(List<IParameter>));
                    propertyList.Call("Add", new object[] { idParameter });
                    MethodInfo loadPropertiesMethod = typeof(Session).GetMethod("LoadListProperties");
                    loadPropertiesMethod = loadPropertiesMethod.MakeGenericMethod(new[] { baseType, field.DataType.GetGenericArguments()[0] });
                    VariableBase returnVal = ((VariableBase)SessionField).Call(loadPropertiesMethod, new object[] { method.This, property.Name, propertyList.Call("ToArray") });
                    field.Assign(returnVal);
                }
                if2.EndIf();
                PropertyInfo countProperty=field.DataType.GetProperty("Count");
                var if4 = method.If(field.Call(countProperty.GetGetMethod()), Reflection.Emit.Enums.Comparison.Equal, method.CreateConstant(0));
                {
                    //Load data
                    VariableBase idValue = method.This.Call(baseType.GetProperty(mapping.IDProperty.Name).GetGetMethod());
                    VariableBase idParameter = method.NewObj(typeof(EqualParameter<>).MakeGenericType(mapping.IDProperty.Type), new object[] { idValue, "ID", "@" });
                    VariableBase propertyList = method.NewObj(typeof(List<IParameter>));
                    propertyList.Call("Add", new object[] { idParameter });
                    MethodInfo loadPropertiesMethod = typeof(Session).GetMethod("LoadProperties");
                    loadPropertiesMethod = loadPropertiesMethod.MakeGenericMethod(new[] { baseType, field.DataType.GetGenericArguments()[0] });
                    VariableBase returnVal = ((VariableBase)SessionField).Call(loadPropertiesMethod, new object[] { method.This, property.Name, propertyList.Call("ToArray") });
                    field.Assign(returnVal);
                }
                if4.EndIf();
                var if3 = method.If(field, Reflection.Emit.Enums.Comparison.Equal, null);
                {
                    field.Assign(method.NewObj(typeof(List<>).MakeGenericType(property.Type).GetConstructor(Type.EmptyTypes)));
                }
                if3.EndIf();
            }
            if1.EndIf();
            returnValue.Assign(field);
        }

        /// <summary>
        /// Sets up a property (IEnumerable)
        /// </summary>
        /// <param name="method">Method builder</param>
        /// <param name="baseType">Base type for the object</param>
        /// <param name="returnValue">Return value</param>
        /// <param name="property">Property info</param>
        /// <param name="mapping">Mapping info</param>
        private void SetupIEnumerableProperty(IMethodBuilder method, Type baseType, VariableBase returnValue, IProperty property, IMapping mapping)
        {
            var field = Fields.Find(x => x.Name == property.DerivedFieldName);
            var if1 = method.If((VariableBase)SessionField, Reflection.Emit.Enums.Comparison.NotEqual, null);
            {
                Reflection.Emit.Commands.If if2 = method.If(field, Reflection.Emit.Enums.Comparison.Equal, null);
                {
                    //Load data
                    VariableBase idValue = method.This.Call(baseType.GetProperty(mapping.IDProperty.Name).GetGetMethod());
                    VariableBase idParameter = method.NewObj(typeof(EqualParameter<>).MakeGenericType(mapping.IDProperty.Type), new object[] { idValue, "ID", "@" });
                    VariableBase propertyList = method.NewObj(typeof(List<IParameter>));
                    propertyList.Call("Add", new object[] { idParameter });
                    MethodInfo loadPropertiesMethod = typeof(Session).GetMethod("LoadProperties");
                    loadPropertiesMethod = loadPropertiesMethod.MakeGenericMethod(new[] { baseType, field.DataType.GetGenericArguments()[0] });
                    VariableBase returnVal = ((VariableBase)SessionField).Call(loadPropertiesMethod, new object[] { method.This, property.Name, propertyList.Call("ToArray") });
                    field.Assign(returnVal);
                }
                if2.EndIf();
                Reflection.Emit.Commands.If if3 = method.If(field, Reflection.Emit.Enums.Comparison.Equal, null);
                {
                    field.Assign(method.NewObj(typeof(List<>).MakeGenericType(property.Type).GetConstructor(Type.EmptyTypes)));
                }
                if3.EndIf();
            }
            if1.EndIf();
            returnValue.Assign(field);
        }

        /// <summary>
        /// Sets up a property (non IEnumerable)
        /// </summary>
        /// <param name="method">Method builder</param>
        /// <param name="baseType">Base type for the object</param>
        /// <param name="returnValue">Return value</param>
        /// <param name="property">Property info</param>
        /// <param name="mapping">Mapping info</param>
        private void SetupSingleProperty(IMethodBuilder method, Type baseType, VariableBase returnValue, IProperty property, IMapping mapping)
        {
            var field = Fields.Find(x => x.Name == property.DerivedFieldName);
            var if1 = method.If((VariableBase)SessionField, Reflection.Emit.Enums.Comparison.NotEqual, null);
            {
                Reflection.Emit.Commands.If if2 = method.If(field, Reflection.Emit.Enums.Comparison.Equal, null);
                {
                    //Load Data
                    VariableBase idValue = method.This.Call(baseType.GetProperty(mapping.IDProperty.Name).GetGetMethod());
                    VariableBase idParameter = method.NewObj(typeof(EqualParameter<>).MakeGenericType(mapping.IDProperty.Type), new object[] { idValue, "ID", "@" });
                    VariableBase propertyList = method.NewObj(typeof(List<IParameter>));
                    propertyList.Call("Add", new object[] { idParameter });
                    MethodInfo loadPropertyMethod = typeof(Session).GetMethod("LoadProperty");
                    loadPropertyMethod = loadPropertyMethod.MakeGenericMethod(new[] { baseType, field.DataType });
                    VariableBase returnVal = ((VariableBase)SessionField).Call(loadPropertyMethod, new object[] { method.This, property.Name, propertyList.Call("ToArray") });
                    field.Assign(returnVal);
                }
                if2.EndIf();
            }
            if1.EndIf();
            returnValue.Assign(field);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Interfaces this aspect is using
        /// </summary>
        public ICollection<Type> InterfacesUsing { get;private set; }

        /// <summary>
        /// Class mappings
        /// </summary>
        private ListMapping<Type, IMapping> ClassMappings { get; set; }

        /// <summary>
        /// Fields used for storing Map, ManyToOne, and ManyToMany properties
        /// </summary>
        private List<Reflection.Emit.FieldBuilder> Fields { get; set; }

        /// <summary>
        /// Field to store the session object
        /// </summary>
        private IPropertyBuilder SessionField { get; set; }

        #endregion
    }
}