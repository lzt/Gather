﻿using System.ComponentModel;

namespace Operate.MessageQueue.ZeroMQ
{
    public enum Mode
    {
        /// <summary>
        /// 服务端
        /// </summary>
        [Description("服务端")]
        Server = 1,

        /// <summary>
        /// 客户端
        /// </summary>
        [Description("客户端")]
        Client = 2,
    }
}
