﻿namespace Operate.MessageQueue.ZeroMQ
{
    public class ZeroMQConnection : MQConnectionBase
    {
        #region Properties

        public string ConnectionString { get; }

        public Mode Mode { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// MSMQConnection
        /// </summary>
        public ZeroMQConnection(string connectionString, Mode mode)
        {
            Mode = mode;
            ConnectionString = connectionString;

            ThreadMode = ThreadMode.SingleThread;
            MaxThread = DefaultMaxThread;

            UseLogger = false;
        }

        #endregion
    }
}
