﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetMQ;
using NetMQ.Sockets;
using Operate.ExtensionMethods;

namespace Operate.MessageQueue.ZeroMQ
{
    public class ZeroMQ : MessageQueueBase, IMessageSender, IMessageReceiver, IDisposable
    {
        private readonly RouterSocket _server;
        private readonly DealerSocket _client;
        private readonly NetMQPoller _poller;

        /// <summary>
        /// ZeroMQConnection
        /// </summary>
        public ZeroMQConnection Connection { get; }

        public ZeroMQ(ZeroMQConnection conn)
        {
            Connection = conn;

            if (string.IsNullOrWhiteSpace(conn.ConnectionString))
            {
                throw new Exception("无效的连接字符串");
            }

            _server = null;
            _client = null;

            if (Connection.Mode == Mode.Server)
            {
                _server = new RouterSocket(conn.ConnectionString);
            }

            if (Connection.Mode == Mode.Client)
            {
                _client = new DealerSocket();
                _client.Options.Identity = Encoding.Unicode.GetBytes(Guid.NewGuid().ToString());
                _client.Connect(conn.ConnectionString);
                _client.ReceiveReady += Client_ReceiveReady;

                _poller = new NetMQPoller { _client };
                _poller.RunAsync();
            }
        }

        protected override bool GetUserLogger()
        {
            return Connection.UseLogger;
        }

        void Client_ReceiveReady(object sender, NetMQSocketEventArgs e)
        {
            var result = e.Socket.ReceiveMultipartMessage();
            Console.WriteLine("REPLY {0}", result);
        }

        public bool TrySend(params byte[][] frames)
        {
            if (Connection.Mode != Mode.Client)
            {
                throw new Exception("请使用客户端模式发送");
            }
            return _client.TrySendMultipartBytes(frames);
        }

        public void TrySend<T>(T message, out bool result)
        {
            if (Connection.Mode != Mode.Client)
            {
                throw new Exception("请使用客户端模式发送");
            }

            result = false;
            try
            {
                if (message is string)
                {
                    result = _client.TrySendFrame(message as string);
                    return;
                }

                if (message is byte[])
                {
                    result = _client.TrySendFrame(message as byte[]);
                    return;
                }

                var data = message as IEnumerable<byte[]>;
                if (data != null)
                {
                    result = _client.TrySendMultipartBytes(data);
                    return;
                }

                if (message is NetMQMessage)
                {
                    result = _client.TrySendMultipartMessage(message as NetMQMessage);
                }
            }
            catch (Exception ex)
            {
                result = false;
                if (Connection.UseLogger)
                {
                    Logger.RecordException(ex);
                }
            }
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void Send<T>(T message) where T : class
        {
            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                bool result;
                TrySend(message, out result);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(SendCallByAsync, message);

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        private void SendCallByAsync<T>(T message) where T : class
        {
            bool result;
            TrySend(message, out result);
            Thread.Sleep(50);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(string message)
        {
            Send<string>(message);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(byte[] message)
        {
            Send<byte[]>(message);
        }

        /// <summary>
        /// Send(支持单线程和线程池两种模式)
        /// </summary>
        /// <param name="message"></param>
        public void Send(NetMQMessage message)
        {
            Send<NetMQMessage>(message);
        }

        /// <summary>
        /// GetReceiveMessage
        /// </summary>
        /// <returns></returns>
        public object GetReceiveMessage()
        {
            var msg = _server.ReceiveMultipartMessage();

            var clientAddress = msg.First;
            var clientOriginalMessage = msg.Last.ConvertToString();
            string response = "{0} back from server {1}".FormatValue(clientOriginalMessage,
                DateTime.Now.ToLongTimeString());
            var messageToClient = new NetMQMessage();
            messageToClient.Append(clientAddress);
            messageToClient.Append(response);
            _server.SendMultipartMessage(messageToClient);

            return msg;
        }

        /// <summary>
        /// GetReceiveMessage
        /// </summary>
        /// <returns></returns>
        public T GetReceiveMessage<T>() where T : NetMQMessage
        {
            return GetReceiveMessage() as T;
        }

        public void ReceiveMessage(Action<object> messageReceiver)
        {
            if (Connection.Mode != Mode.Server)
            {
                throw new Exception("请使用服务端模式接收");
            }

            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                var obj = GetReceiveMessage();
                messageReceiver(obj);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(messageReceiver, GetReceiveMessage());

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }

        /// <summary>
        /// ReceiveMessage
        /// </summary>
        /// <param name="messageReceiver"></param>
        public void ReceiveMessage<T>(Action<T> messageReceiver) where T : NetMQMessage
        {
            if (Connection.Mode != Mode.Server)
            {
                throw new Exception("请使用服务端模式接收");
            }

            if (Connection.ThreadMode == ThreadMode.SingleThread)
            {
                var obj = GetReceiveMessage<T>();
                messageReceiver(obj);
            }

            if (Connection.ThreadMode == ThreadMode.ThreadPool)
            {
                TaskExec(messageReceiver, GetReceiveMessage<T>());

                if (TaskList.Count >= Connection.MaxThread * 2)
                {
                    try
                    {
                        //等待所有线程全部运行结束
                        Task.WaitAll(TaskList.ToArray());
                    }
                    catch (AggregateException ex)
                    {
                        if (Connection.UseLogger)
                        {
                            //.NET4 Task的统一异常处理机制
                            foreach (Exception inner in ex.InnerExceptions)
                            {
                                Logger.RecordException(inner);
                            }
                        }
                    }

                    TaskList.Clear();
                }
            }
        }


        private bool _disposed;

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (_disposed) return;

            // ReSharper disable once UseNullPropagation
            if (TaskScheduler != null)
            {
                TaskScheduler.Dispose();
            }

            if (_server != null)
            {
                _server.Close();
                _server.Dispose();
            }

            if (_client != null)
            {
                _client.Close();
                _client.Dispose();
            }


            // ReSharper disable once UseNullPropagation
            if (_poller != null)
            {
                _poller.Dispose();
            }

            _disposed = true;
        }
    }
}
