﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TextTemplating;
using Operate.IO.ExtensionMethods;

namespace Operate.T4
{
    /// <summary>
    /// 转换器
    /// </summary>
    public class Translater : IDisposable
    {
        #region Field

        private bool _disposed;

        #endregion

        #region Properties

        /// <summary>
        /// OutPath
        /// </summary>
        public string OutFilePath { get; set; }

        /// <summary>
        /// Template
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// TextTemplatingSession
        /// </summary>
        public TextTemplatingSession Session { get; set; }

        /// <summary>
        /// Error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// AssemblyReferences
        /// </summary>
        public List<string> AssemblyReferences { get; set; }

        /// <summary>
        /// Imports
        /// </summary>
        public List<string> Imports { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Translater
        /// </summary>
        public Translater()
        {
            AssemblyReferences = new List<string>();
            Imports = new List<string>();
        }

        #endregion

        #region Method

        /// <summary>
        /// GetConvertResult
        /// </summary>
        /// <returns></returns>
        public string GetConvertResult()
        {
            var host = new CustomTextTemplatingEngineHost
            {
                TemplateFile = "test.tt",
            };

            foreach (var assembly in AssemblyReferences)
            {
                host.AssemblyReferences.Add(assembly);
            }

            foreach (var import in Imports)
            {
                host.Imports.Add(import);
            }
            var engine = new Engine();

            host.Session = Session;
            string output = engine.ProcessTemplate(Template, host);
            Error = host.Errors.Cast<CompilerError>().Aggregate("", (current, error) => current + error.ErrorText);

            host.Errors.Clear();
            host.AssemblyReferences.Clear();
            host.Imports.Clear();
            host.StandardAssemblyReferences.Clear();
            host.StandardImports.Clear();
            host.Session.Clear();
            host.TemplateFile = null;

            return output;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        public void Create()
        {
            var output = GetConvertResult();
            OutFilePath.WriteFile(output);
        }

        #endregion

        #region Despose

        public void Dispose()
        {
            //必须以Dispose(true)方式调用,以true告诉Dispose(bool disposing)函数是被客户直接调用的
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                // 那么这个方法是被客户直接调用的,那么托管的,和非托管的资源都可以释放
                if (disposing)
                {
                }
                Session.Clear();
                _disposed = true;
            }
        }

        #endregion

        #region Destructor

        //析构函数自动生成 Finalize 方法和对基类的 Finalize 方法的调用.默认情况下,  
        //一个类是没有析构函数的,也就是说,对象被垃圾回收时不会被调用Finalize方法   
        ~Translater()
        {
            // 为了保持代码的可读性性和可维护性,千万不要在这里写释放非托管资源的代码
            // 必须以Dispose(false)方式调用,以false告诉Dispose(bool disposing)函数是从垃圾回收器在调用Finalize时调用的
            Dispose(false);
        }

        #endregion
    }
}
