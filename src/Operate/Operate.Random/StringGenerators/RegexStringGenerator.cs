﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Text;
using System.Text.RegularExpressions;
using Operate.Random.BaseClasses;
using Operate.Random.Interfaces;

#endregion

namespace Operate.Random.StringGenerators
{
    /// <summary>
    /// Randomly generates strings based on a Regex
    /// </summary>
    public class RegexStringGenerator : GeneratorAttributeBase, IGenerator<string>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="length">Length of the string to generate</param>
        /// <param name="allowedCharacters">Characters that are allowed</param>
        /// <param name="numberOfNonAlphaNumericsAllowed">Number of non alphanumeric characters to allow</param>
        public RegexStringGenerator(int length, string allowedCharacters = ".", int numberOfNonAlphaNumericsAllowed = int.MaxValue)
            : base("", "")
        {
            Length = length;
            AllowedCharacters = allowedCharacters;
            NumberOfNonAlphaNumericsAllowed = numberOfNonAlphaNumericsAllowed;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Length to generate
        /// </summary>
        public virtual int Length { get; protected set; }

        /// <summary>
        /// Characters allowed
        /// </summary>
        public virtual string AllowedCharacters { get; protected set; }

        /// <summary>
        /// Number of non alpha numeric characters allowed
        /// </summary>
        public virtual int NumberOfNonAlphaNumericsAllowed { get; protected set; }

        #endregion

        #region Functions

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public string Next(System.Random rand)
        {
            if (Length < 1)
                return "";
            var tempBuilder = new StringBuilder();
            var comparer = new Regex(AllowedCharacters);
            var alphaNumbericComparer = new Regex("[0-9a-zA-Z]");
            int counter = 0;
            while (tempBuilder.Length < Length)
            {
                var tempValue = new string(Convert.ToChar(Convert.ToInt32(System.Math.Floor(94 * rand.NextDouble() + 32))), 1);
                if (comparer.IsMatch(tempValue))
                {
                    if (!alphaNumbericComparer.IsMatch(tempValue) && NumberOfNonAlphaNumericsAllowed > counter)
                    {
                        tempBuilder.Append(tempValue);
                        ++counter;
                    }
                    else if (alphaNumbericComparer.IsMatch(tempValue))
                    {
                        tempBuilder.Append(tempValue);
                    }
                }
            }
            return tempBuilder.ToString();
        }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public string Next(System.Random rand, string min, string max)
        {
            return Next(rand);
        }

        /// <summary>
        /// Generates next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return Next(rand);
        }

        #endregion
    }
}