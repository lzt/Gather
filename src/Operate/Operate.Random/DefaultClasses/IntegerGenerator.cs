﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.ExtensionMethods;
using Operate.Random.BaseClasses;
using Operate.Random.Interfaces;

#endregion

namespace Operate.Random.DefaultClasses
{
    /// <summary>
    /// Randomly generates ints
    /// </summary>
    public class IntegerGenerator<T> : IGenerator<T>
    {
        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public T Next(System.Random rand)
        {
            return rand.Next().To(default(T));
        }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public T Next(System.Random rand, T min, T max)
        {
            return rand.Next(min.To(0), max.To(0)).To(default(T));
        }

        /// <summary>
        /// Randomly generates an object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>A randomly generated object</returns>
        public object NextObj(System.Random rand)
        {
            return Next(rand);
        }
    }

    #region Integer generators

    #region ByteGenerator

    /// <summary>
    /// Byte generator
    /// </summary>
    public class ByteGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public ByteGenerator(byte min, byte max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ByteGenerator()
            : base(0, byte.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<byte>().Next(rand, (byte)Min, (byte)Max);
        }
    }

    #endregion

    #region CharGenerator

    /// <summary>
    /// Char generator
    /// </summary>
    public class CharGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public CharGenerator(char min, char max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public CharGenerator()
            : base(0, char.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<char>().Next(rand, (char)Min, (char)Max);
        }
    }

    #endregion

    #region IntGenerator

    /// <summary>
    /// Int generator
    /// </summary>
    public class IntGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public IntGenerator(int min, int max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public IntGenerator()
            : base(0, int.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<int>().Next(rand, (int)Min, (int)Max);
        }
    }

    #endregion

    #region LongGenerator

    /// <summary>
    /// Long generator
    /// </summary>
    public class LongGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public LongGenerator(long min, long max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public LongGenerator()
            : base(0, long.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<long>().Next(rand, (long)Min, (long)Max);
        }
    }

    #endregion

    #region SByteGenerator

    /// <summary>
    /// sbyte generator
    /// </summary>
    public class SByteGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public SByteGenerator(sbyte min, sbyte max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public SByteGenerator()
            : base(0, sbyte.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<sbyte>().Next(rand, (sbyte)Min, (sbyte)Max);
        }
    }

    #endregion

    #region ShortGenerator

    /// <summary>
    /// Short generator
    /// </summary>
    public class ShortGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public ShortGenerator(short min, short max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ShortGenerator()
            : base(0, short.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<short>().Next(rand, (short)Min, (short)Max);
        }
    }

    #endregion

    #region UIntGenerator

    /// <summary>
    /// uint generator
    /// </summary>
    public class UIntGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public UIntGenerator(uint min, uint max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public UIntGenerator()
            : base(0, uint.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<uint>().Next(rand, (uint)Min, (uint)Max);
        }
    }

    #endregion

    #region ULongGenerator

    /// <summary>
    /// ulong generator
    /// </summary>
    public class ULongGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public ULongGenerator(ulong min, ulong max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ULongGenerator()
            : base(0, ulong.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<ulong>().Next(rand, (ulong)Min, (ulong)Max);
        }
    }

    #endregion

    #region UShortGenerator

    /// <summary>
    /// ushort generator
    /// </summary>
    public class UShortGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public UShortGenerator(ushort min, ushort max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public UShortGenerator()
            : base(0, ushort.MaxValue)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new IntegerGenerator<ushort>().Next(rand, (ushort)Min, (ushort)Max);
        }
    }

    #endregion

    #endregion
}