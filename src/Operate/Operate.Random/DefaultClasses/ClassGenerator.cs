﻿using System.Reflection;
using Operate.ExtensionMethods;
using Operate.Random.BaseClasses;
using Operate.Random.Interfaces;
using Operate.Reflection.ExtensionMethods;

namespace Operate.Random.DefaultClasses
{
    /// <summary>
    /// Randomly generates a class
    /// </summary>
    /// <typeparam name="T">Class type to generate</typeparam>
    public class ClassGenerator<T> : IGenerator<T>
        where T : class,new()
    {
        /// <summary>
        /// Generates a random version of the class
        /// </summary>
        /// <param name="rand">Random generator to use</param>
        /// <returns>The randomly generated class</returns>
        public T Next(System.Random rand)
        {
            var returnItem = new T();
            System.Type objectType = typeof(T);
            foreach (PropertyInfo property in objectType.GetProperties())
            {
                var attribute = property.Attribute<GeneratorAttributeBase>();
                if(attribute!=null)
                    returnItem.Property(property, attribute.NextObj(rand));
            }
            return returnItem;
        }

        /// <summary>
        /// Generates a random version of the class
        /// </summary>
        /// <param name="rand">Random generator to use</param>
        /// <param name="min">min value (not used)</param>
        /// <param name="max">max value (not used)</param>
        /// <returns>The randomly generated class</returns>
        public T Next(System.Random rand, T min, T max)
        {
            return new T();
        }

        /// <summary>
        /// Gets a random version of the class
        /// </summary>
        /// <param name="rand">Random generator used</param>
        /// <returns>The randonly generated class</returns>
        public object NextObj(System.Random rand)
        {
            return new T();
        }
    }
}