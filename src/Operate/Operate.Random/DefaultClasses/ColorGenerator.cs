﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Drawing;
using Operate.ExtensionMethods;
using Operate.Random.BaseClasses;
using Operate.Random.Interfaces;

#endregion

namespace Operate.Random.DefaultClasses
{
    /// <summary>
    /// Randomly generates Colors
    /// </summary>
    public class ColorGenerator : GeneratorAttributeBase, IGenerator<Color>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ColorGenerator() : base(Color.Black, Color.White) { }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public Color Next(System.Random rand)
        {
            return Next(rand, Color.Black, Color.White);
        }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public Color Next(System.Random rand, Color min, Color max)
        {
            return Color.FromArgb(rand.Next(min.A.Min((byte)(max.A + 1)), min.A.Max((byte)(max.A + 1))),
                rand.Next(min.R.Min((byte)(max.R + 1)), min.R.Max((byte)(max.R + 1))),
                rand.Next(min.G.Min((byte)(max.G + 1)), min.G.Max((byte)(max.G + 1))),
                rand.Next(min.B.Min((byte)(max.B + 1)), min.B.Max((byte)(max.B + 1))));
        }

        /// <summary>
        /// Generates next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            if ((Color)Min != default(Color) || (Color)Max != default(Color))
                return Next(rand, (Color)Min, (Color)Max);
            return Next(rand);
        }
    }
}