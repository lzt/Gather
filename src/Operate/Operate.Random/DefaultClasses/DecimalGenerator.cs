﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using Operate.ExtensionMethods;
using Operate.Random.BaseClasses;
using Operate.Random.Interfaces;

#endregion

namespace Operate.Random.DefaultClasses
{
    /// <summary>
    /// Randomly generates decimals
    /// </summary>
    public class DecimalGenerator<T> : IGenerator<T>
    {
        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public T Next(System.Random rand)
        {
            return rand.NextDouble().To(default(T));
        }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public T Next(System.Random rand, T min, T max)
        {
            return (min.To(default(double)) + ((max.To(default(double)) - min.To(default(double))) * rand.NextDouble())).To(default(T));
        }

        /// <summary>
        /// Randomly generates an object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>A randomly generated object</returns>
        public object NextObj(System.Random rand)
        {
            return Next(rand);
        }
    }

    #region Decimal Generators

    /// <summary>
    /// Decimal generator
    /// </summary>
    public class DecimalGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public DecimalGenerator(decimal min, decimal max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DecimalGenerator()
            : base(0m, 1m)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new DecimalGenerator<decimal>().Next(rand, (decimal)Min, (decimal)Max);
        }
    }

    /// <summary>
    /// Double generator
    /// </summary>
    public class DoubleGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public DoubleGenerator(double min, double max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DoubleGenerator()
            : base(0d, 1d)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new DecimalGenerator<double>().Next(rand, (double)Min, (double)Max);
        }
    }

    /// <summary>
    /// Float generator
    /// </summary>
    public class FloatGenerator : GeneratorAttributeBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        public FloatGenerator(float min, float max)
            : base(min, max)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public FloatGenerator()
            : base(0f, 1f)
        {
        }

        /// <summary>
        /// Creates the next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return new DecimalGenerator<float>().Next(rand, (float)Min, (float)Max);
        }
    }

    #endregion
}