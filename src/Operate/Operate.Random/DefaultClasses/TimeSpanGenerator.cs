﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using Operate.Random.BaseClasses;
using Operate.Random.Interfaces;

#endregion

namespace Operate.Random.DefaultClasses
{
    /// <summary>
    /// Randomly generates TimeSpans
    /// </summary>
    public class TimeSpanGenerator : GeneratorAttributeBase, IGenerator<TimeSpan>
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public TimeSpanGenerator() : base(TimeSpan.MinValue, TimeSpan.MaxValue) { }

        #endregion

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public TimeSpan Next(System.Random rand)
        {
            return Next(rand, TimeSpan.MinValue, TimeSpan.MaxValue);
        }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public TimeSpan Next(System.Random rand, TimeSpan min, TimeSpan max)
        {
            if (min > max)
                throw new ArgumentException("The minimum value must be less than the maximum value");
            return min + new TimeSpan((long)(new TimeSpan(max.Ticks - min.Ticks).Ticks * rand.NextDouble()));
        }

        /// <summary>
        /// Generates next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            if ((TimeSpan)Min != default(TimeSpan) || (TimeSpan)Max != default(TimeSpan))
                return Next(rand, (TimeSpan)Min, (TimeSpan)Max);
            return Next(rand);
        }
    }
}