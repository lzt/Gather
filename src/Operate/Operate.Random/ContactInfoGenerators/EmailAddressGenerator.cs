﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using Operate.Random.BaseClasses;
using Operate.Random.ExtensionMethods;
using Operate.Random.Interfaces;
using Operate.Random.NameGenerators;

#endregion

namespace Operate.Random.ContactInfoGenerators
{
    /// <summary>
    /// Generates a random email address
    /// </summary>
    public class EmailAddressGenerator : GeneratorAttributeBase, IGenerator<string>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="commonEndings">Common endings to domain names should be used (.com,.org,.net,etc.)</param>
        public EmailAddressGenerator(bool commonEndings = true)
            : base("", "")
        {
            CommonEndings = commonEndings;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Should common domain name endings be used?
        /// </summary>
        public virtual bool CommonEndings { get;private set; }

        #endregion

        #region Functions

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public string Next(System.Random rand)
        {
            string domainName = (rand.Next<bool>()) ? rand.Next(_freeAccounts) + (CommonEndings ? rand.Next(_mostCommonEndings) : rand.Next(_endings)) : new DomainNameGenerator(CommonEndings).Next(rand);
            int addressStyle = rand.Next(1, 6);
            if (addressStyle == 1)
                return new NameGenerator().Next(rand).Replace(" ", ".") + "@" + domainName;
            if (addressStyle == 2)
                return new NameGenerator(false, true).Next(rand).Replace(" ", ".") + "@" + domainName;
            if (addressStyle == 3)
                return rand.Next<char>('a', 'z') + "." + new LastNameGenerator().Next(rand) + "@" + domainName;
            if (addressStyle == 4)
                return new NameGenerator(false, false, false).Next(rand).Replace(" ", ".") + "@" + domainName;
            return rand.Next<char>('a', 'z') + "." + rand.Next<char>('a', 'z') + "." + new LastNameGenerator().Next(rand) + "@" + domainName;
        }

        /// <summary>
        /// Generates a random value of the specified type
        /// </summary>
        /// <param name="rand">Random number generator that it can use</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <returns>A randomly generated object of the specified type</returns>
        public string Next(System.Random rand, string min, string max)
        {
            return Next(rand);
        }

        /// <summary>
        /// Generates next object
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <returns>The next object</returns>
        public override object NextObj(System.Random rand)
        {
            return Next(rand);
        }

        #endregion

        #region Private Variables

        private readonly string[] _freeAccounts = { "gmail", "yahoo", "hotmail" };

        private readonly string[] _endings = { ".ag", ".am", ".as", ".at", ".az", ".be", ".bi", ".bs", ".cc", ".cf", ".cg", ".ch", ".co.at", ".co.ck", ".co.gg", ".co.il", ".co.je", ".co.ma", ".co.mu", ".co.mz", ".co.nz", ".co.pn", ".co.ro", ".co.tt", ".co.uk", ".co.vi", ".co.za", ".com", ".com.ag", ".com.ar", ".com.az", ".com.bs", ".com.dm", ".com.do", ".com.ec", ".com.fj", ".com.gd", ".com.gi", ".com.gt", ".com.gy", ".com.jm", ".com.kh", ".com.kn", ".com.lc", ".com.lk", ".com.lv", ".com.ly", ".com.mx", ".com.nf", ".com.ni", ".com.pa", ".com.pe", ".com.ph", ".com.pl", ".com.pr", ".com.pt", ".com.ro", ".com.ru", ".com.sb", ".com.sc", ".com.tj", ".com.tp", ".com.ua", ".com.ve", ".cx", ".cz", ".dk", ".fm", ".gd", ".gen.tr", ".gg", ".gl", ".gs", ".gy", ".hm", ".io", ".je", ".jp", ".kg", ".kn", ".kz", ".li", ".lk", ".lt", ".lv", ".ly", ".ma", ".md", ".ms", ".mu", ".mw", ".net", ".net.tp", ".nu", ".off.ai", ".org", ".org.tp", ".org.uk", ".ph", ".pl", ".ro", ".ru", ".rw", ".sc", ".sh", ".sn", ".st", ".tc", ".tf", ".tj", ".to", ".tp", ".tt", ".uz", ".vg", ".vu", ".ws" };

        private readonly string[] _mostCommonEndings = { ".com", ".net", ".org" };

        #endregion
    }
}