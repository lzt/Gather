﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Globalization;

#endregion

namespace Operate.Random
{
    /// <summary>
    /// Utility class for handling random
    /// information.
    /// </summary>
    public class Random : System.Random
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Random()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="seed">Seed value</param>
        public Random(int seed)
            : base(seed)
        {
        }

        #endregion

        #region Private Variables

        private static readonly Random GlobalSeed = new Random();

        [ThreadStatic]
        private static Random _local;

        #endregion

        #region Static Functions

        #region ThreadSafeNext

        /// <summary>
        /// A thread safe version of a random number generation
        /// </summary>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        /// <returns>A randomly generated value</returns>
        public static int ThreadSafeNext(int min = int.MinValue, int max = int.MaxValue)
        {
            if (_local == null)
            {
                int seed;
                lock (GlobalSeed)
                    seed = GlobalSeed.Next();
                _local = new Random(seed);
            }
            return _local.Next(min, max);
        }

        #endregion

        #region 生成随机码

        ///  <summary>
        ///  生成随机码
        ///  </summary>
        ///  <param  name="length">随机码个数</param>
        ///  <returns></returns>
        public static string CreateRandomCode(int length)
        {
            string randomcode = String.Empty;
            //生成一定长度的验证码
            var random = new Random();
            for (int i = 0; i < length; i++)
            {
                int rand = random.Next();
                char code;
                if (rand % 3 == 0)
                {
                    code = (char)('A' + (char)(rand % 26));
                }
                else
                {
                    code = (char)('0' + (char)(rand % 10));
                }
                randomcode += code.ToString(CultureInfo.InvariantCulture);
            }
            return randomcode;
        }

        #endregion

        #region 生成随机码

        ///  <summary>
        ///  生成G到Z之间的任意一个字母
        ///  </summary>
        ///  <returns></returns>
        public static string CreateRandomCharFromGtoZ(out int outseed,List<string> filter=null,int? seed=null)
        {
            var check = new List<string> {"A", "B", "C", "D", "E", "F"};
            //生成一定长度的验证码
            Random random;
            if (seed==null)
            {
                random = new Random();
            }
            else
            {
                random = new Random((int)seed);
            }
            outseed = random.Next();
            string code;;
            if (outseed % 3 == 0)
            {
                code = ((char)('A' + (char)(outseed % 26))).ToString(CultureInfo.InvariantCulture).ToUpper();
            }
            else
            {
                code = CreateRandomCharFromGtoZ(out outseed, filter, outseed);
            }

            if (check.Contains(code))
            {
                code = CreateRandomCharFromGtoZ(out outseed, filter, outseed);
            }

            if (filter!=null)
            {
                if (filter.Contains(code))
                {
                    code = CreateRandomCharFromGtoZ(out outseed, filter, outseed);
                }
            }

            return code;
        }

        #endregion

        #endregion
    }
}