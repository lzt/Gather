﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Drawing;
using System.Linq;
using Operate.ExtensionMethods;
using Operate.Random.DefaultClasses;
using Operate.Random.Interfaces;

#endregion

namespace Operate.Random.ExtensionMethods
{
    /// <summary>
    /// Extension methods for the Random class
    /// </summary>
    public static class RandomExtensions
    {
        #region Functions

        #region Next

        /// <summary>
        /// Randomly generates a value of the specified type
        /// </summary>
        /// <typeparam name="T">Type to generate</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static T Next<T>(this System.Random random, IGenerator<T> generator = null)
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            if (generator == null)
            {
                if (!_generators.ContainsKey(typeof(T)))
                    throw new ArgumentOutOfRangeException("The type specified, " + typeof(T).Name + ", does not have a default generator.");
                generator = (IGenerator<T>)_generators[typeof(T)];
            }
            return generator.Next(random);
        }

        /// <summary>
        /// Randomly generates a value of the specified type
        /// </summary>
        /// <typeparam name="T">Type to generate</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static T NextClass<T>(this System.Random random, IGenerator<T> generator = null)
            where T : class,new()
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            if (generator == null)
            {
                generator = new ClassGenerator<T>();
            }
            return generator.Next(random);
        }

        /// <summary>
        /// Randomly generates a value of the specified type
        /// </summary>
        /// <typeparam name="T">Type to generate</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static T Next<T>(this System.Random random, T min, T max, IGenerator<T> generator = null)
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            if (generator == null)
            {
                if (!_generators.ContainsKey(typeof(T)))
                    throw new ArgumentOutOfRangeException("The type specified, " + typeof(T).Name + ", does not have a default generator.");
                generator = (IGenerator<T>)_generators[typeof(T)];
            }
            return generator.Next(random, min, max);
        }

        /// <summary>
        /// Randomly generates a list of values of the specified type
        /// </summary>
        /// <typeparam name="T">Type to the be generated</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="amount">Number of items to generate</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static IEnumerable<T> Next<T>(this System.Random random, int amount, IGenerator<T> generator = null)
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            if (generator == null)
            {
                if (!_generators.ContainsKey(typeof(T)))
                    throw new ArgumentOutOfRangeException("The type specified, " + typeof(T).Name + ", does not have a default generator.");
                generator = (IGenerator<T>)_generators[typeof(T)];
            }
            return amount.Times(x => generator.Next(random));
        }

        /// <summary>
        /// Randomly generates a list of values of the specified type
        /// </summary>
        /// <typeparam name="T">Type to the be generated</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="amount">Number of items to generate</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static IEnumerable<T> NextClass<T>(this System.Random random, int amount, IGenerator<T> generator = null)
            where T : class,new()
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            if (generator == null)
            {
                generator = new ClassGenerator<T>();
            }
            return amount.Times(x => generator.Next(random));
        }

        /// <summary>
        /// Randomly generates a list of values of the specified type
        /// </summary>
        /// <typeparam name="T">Type to the be generated</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="amount">Number of items to generate</param>
        /// <param name="max">Maximum value (inclusive)</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static IEnumerable<T> Next<T>(this System.Random random, int amount, T min, T max, IGenerator<T> generator = null)
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            if (generator == null)
            {
                if (!_generators.ContainsKey(typeof(T)))
                    throw new ArgumentOutOfRangeException("The type specified, " + typeof(T).Name + ", does not have a default generator.");
                generator = (IGenerator<T>)_generators[typeof(T)];
            }
            return amount.Times(x => generator.Next(random, min, max));
        }

        /// <summary>
        /// Picks a random item from the list
        /// </summary>
        /// <typeparam name="T">Type of object in the list</typeparam>
        /// <param name="random">Random number generator</param>
        /// <param name="list">List to pick from</param>
        /// <returns>Item that is returned</returns>
        public static T Next<T>(this System.Random random, IEnumerable<T> list)
        {
            int x = 0;
            var enumerable = list as T[] ?? list.ToArray();
            int position = random.Next(0, enumerable.Count());
            foreach (T item in enumerable)
            {
                if (x == position)
                    return item;
                ++x;
            }
            return default(T);
        }

        #endregion

        #region NextEnum

        /// <summary>
        /// Randomly generates a value of the specified enum type
        /// </summary>
        /// <typeparam name="T">Type to generate</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static T NextEnum<T>(this System.Random random, IGenerator<T> generator = null)
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            generator = generator.Check(new EnumGenerator<T>());
            return generator.Next(random);
        }

        /// <summary>
        /// Randomly generates a list of values of the specified enum type
        /// </summary>
        /// <typeparam name="T">Type to the be generated</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="amount">Number of items to generate</param>
        /// <param name="generator">Generator to be used (if not included, default generator is used)</param>
        /// <returns>The randomly generated value</returns>
        public static IEnumerable<T> NextEnum<T>(this System.Random random, int amount, IGenerator<T> generator = null)
        {
            if (random.IsNull()) { throw new ArgumentNullException("random"); }
            SetupGenerators();
            generator = generator.Check(new EnumGenerator<T>());
            return amount.Times(x => generator.Next(random));
        }

        #endregion

        #region RegisterGenerator

        /// <summary>
        /// Registers a generator with a type
        /// </summary>
        /// <typeparam name="T">Type to associate with the generator</typeparam>
        /// <param name="rand">Random number generator</param>
        /// <param name="generator">Generator to associate with the type</param>
        /// <returns>The random number generator</returns>
        public static System.Random RegisterGenerator<T>(this System.Random rand, IGenerator generator)
        {
            return rand.RegisterGenerator(typeof(T), generator);
        }

        /// <summary>
        /// Registers a generator with a type
        /// </summary>
        /// <param name="rand">Random number generator</param>
        /// <param name="generator">Generator to associate with the type</param>
        /// <param name="type">Type to associate with the generator</param>
        /// <returns>The random number generator</returns>
        public static System.Random RegisterGenerator(this System.Random rand, Type type, IGenerator generator)
        {
            if (_generators.ContainsKey(type))
                _generators[type] = generator;
            else
                _generators.Add(type, generator);
            return rand;
        }

        #endregion

        #region ResetGenerators

        /// <summary>
        /// Resets the generators to the defaults
        /// </summary>
        /// <param name="random">Random object</param>
        /// <returns>The random object sent in</returns>
        public static System.Random ResetGenerators(this System.Random random)
        {
            _generators = null;
            SetupGenerators();
            return random;
        }

        #endregion

        #region Shuffle

        /// <summary>
        /// Shuffles a list randomly
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="random">Random object</param>
        /// <param name="list">List of objects to shuffle</param>
        /// <returns>The shuffled list</returns>
        public static IEnumerable<T> Shuffle<T>(this System.Random random, IEnumerable<T> list)
        {
            var enumerable = list as T[] ?? list.ToArray();
            if (list == null || enumerable.Count() == 0)
                return enumerable;
            return enumerable.OrderBy(x => random.Next());
        }

        #endregion

        #endregion

        #region Private Functions/Variables

        private static void SetupGenerators()
        {
            if (_generators != null)
                return;
            _generators = new Dictionary<Type, IGenerator>
                {
                    {typeof (bool), new BoolGenerator()},
                    {typeof (decimal), new DecimalGenerator<decimal>()},
                    {typeof (double), new DecimalGenerator<double>()},
                    {typeof (float), new DecimalGenerator<float>()},
                    {typeof (byte), new IntegerGenerator<byte>()},
                    {typeof (char), new IntegerGenerator<char>()},
                    {typeof (int), new IntegerGenerator<int>()},
                    {typeof (long), new IntegerGenerator<long>()},
                    {typeof (sbyte), new IntegerGenerator<sbyte>()},
                    {typeof (short), new IntegerGenerator<short>()},
                    {typeof (uint), new IntegerGenerator<uint>()},
                    {typeof (ulong), new IntegerGenerator<ulong>()},
                    {typeof (ushort), new IntegerGenerator<ushort>()},
                    {typeof (DateTime), new DateTimeGenerator()},
                    {typeof (TimeSpan), new TimeSpanGenerator()},
                    {typeof (Color), new ColorGenerator()},
                    {typeof (string), new StringGenerator()}
                };
        }

        private static Dictionary<Type, IGenerator> _generators;

        #endregion
    }
}