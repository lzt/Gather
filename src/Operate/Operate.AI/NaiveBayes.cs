﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Globalization;
using System.Linq;
using Operate.ExtensionMethods;

#endregion

namespace Operate.AI
{
    /// <summary>
    /// Naive bayes classifier
    /// </summary>
    /// <typeparam name="T">The type of the individual tokens</typeparam>
    public class NaiveBayes<T>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aTokenWeight">Weight of each token in set A</param>
        /// <param name="bTokenWeight">Weight of each token in set B</param>
        /// <param name="maxInterestingTokenCount">After sorting, this is the maximum number of tokens that are picked to figure out the final probability</param>
        /// <param name="maxTokenProbability">Maximum token probability</param>
        /// <param name="minTokenProbability">Minimum token probability</param>
        /// <param name="minCountForInclusion">Minimum number of times a token needs to be present for it to be included</param>
        public NaiveBayes(int aTokenWeight = 1, int bTokenWeight = 1,
            double minTokenProbability = 0.01, double maxTokenProbability = 0.999,
            int maxInterestingTokenCount = int.MaxValue,
            int minCountForInclusion = 1)
        {
            SetA = new Bag<T>();
            SetB = new Bag<T>();
            Probabilities = new Dictionary<T, double>();
            Total = 0;
            TotalA = 0;
            TotalB = 0;
           ATokenWeight = aTokenWeight;
           BTokenWeight = bTokenWeight;
           MinCountForInclusion = minCountForInclusion;
           MinTokenProbability = minTokenProbability;
           MaxTokenProbability = maxTokenProbability;
           MaxInterestingTokenCount = maxInterestingTokenCount;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Set A
        /// </summary>
        public Bag<T> SetA { get;private set; }

        /// <summary>
        /// Set B
        /// </summary>
        public Bag<T> SetB { get;private set; }

        /// <summary>
        /// Total number of tokens
        /// </summary>
        protected double Total { get; set; }

        /// <summary>
        /// Total number of tokens in set A
        /// </summary>
        protected double TotalA { get; set; }

        /// <summary>
        /// Total number of tokens in set B
        /// </summary>
        protected double TotalB { get; set; }

        /// <summary>
        /// Dictionary containing probabilities
        /// </summary>
        protected Dictionary<T, double> Probabilities { get;private set; }

        /// <summary>
        /// Weight to give to the probabilities in set A
        /// </summary>
        public int ATokenWeight { get; set; }

        /// <summary>
        /// Weight to give the probabilities in set B
        /// </summary>
        public int BTokenWeight { get; set; }

        /// <summary>
        /// Minimum count that an item needs to be found to be included in final probability
        /// </summary>
        public int MinCountForInclusion { get; set; }

        /// <summary>
        /// Minimum token probability (if less than this amount, it becomes this amount)
        /// </summary>
        public double MinTokenProbability { get; set; }

        /// <summary>
        /// Maximum token probability (if greater than this amount, it becomes this amount)
        /// </summary>
        public double MaxTokenProbability { get; set; }

        /// <summary>
        /// After sorting, this is the maximum number of tokens that are picked to figure out the final probability
        /// </summary>
        public int MaxInterestingTokenCount { get; set; }

        #endregion

        #region Public Functions

        /// <summary>
        /// Loads a set of tokens
        /// </summary>
        /// <param name="setATokens">Set A</param>
        /// <param name="setBTokens">Set B</param>
        public virtual void LoadTokens(IEnumerable<T> setATokens, IEnumerable<T> setBTokens)
        {
            if (setATokens.IsNull()) { throw new ArgumentNullException("setATokens"); }
            if (setBTokens.IsNull()) { throw new ArgumentNullException("setBTokens"); }
            SetA = SetA.Check(()=>new Bag<T>());
            SetB = SetB.Check(()=>new Bag<T>());
            SetA.Add(setATokens);
            SetB.Add(setBTokens);
            TotalA = SetA.Sum(x => SetA[x]);
            TotalB = SetB.Sum(x => SetB[x]);
            Total = TotalA + TotalB;
            Probabilities = new Dictionary<T, double>();
            foreach (T token in SetA)
                Probabilities.Add(token, CalculateProbabilityOfToken(token));
            foreach (T token in SetB)
                if (!Probabilities.ContainsKey(token))
                    Probabilities.Add(token, CalculateProbabilityOfToken(token));
        }

        /// <summary>
        /// Calculates the probability of the list of tokens being in set A
        /// </summary>
        /// <param name="items">List of items</param>
        /// <returns>The probability that the tokens are from set A</returns>
        public virtual double CalculateProbabilityOfTokens(IEnumerable<T> items)
        {
            if (items.IsNull()) { throw new ArgumentNullException("items"); }
            if (Probabilities.IsNull()) { throw new InvalidOperationException("Probabilities has not been initialized"); }
            var sortedProbabilities = new SortedList<string, double>();
            int x = 0;
            foreach(T item in items)
            {
                double tokenProbability = 0.5;
                if (Probabilities.ContainsKey(item))
                    tokenProbability = Probabilities[item];
                string difference = ((0.5 - Math.Abs(0.5 - tokenProbability))).ToString(".0000000", CultureInfo.InvariantCulture) + item + x;
                sortedProbabilities.Add(difference, tokenProbability);
                ++x;
            }
            double totalProbability = 1;
            double negativeTotalProbability = 1;
            int count = 0;
            int maxCount = sortedProbabilities.Count.Min(MaxInterestingTokenCount);
            foreach (string probability in sortedProbabilities.Keys)
            {
                double tokenProbability = sortedProbabilities[probability];
                totalProbability *= tokenProbability;
                negativeTotalProbability *= (1 - tokenProbability);
                ++count;
                if (count >= maxCount)
                    break;
            }
            return totalProbability / (totalProbability + negativeTotalProbability);
        }

        #endregion

        #region Protected Functions

        /// <summary>
        /// Calculates a single items probability of being in set A
        /// </summary>
        /// <param name="item">Item to calculate</param>
        /// <returns>The probability that the token is from set A</returns>
        protected virtual double CalculateProbabilityOfToken(T item)
        {
            if (!(SetA != null && SetB != null)) { throw new ArgumentException("Probabilities have not been initialized"); }
            double probability = 0;
            int aCount = SetA.Contains(item) ? SetA[item] * ATokenWeight : 0;
            int bCount = SetB.Contains(item) ? SetB[item] * BTokenWeight : 0;
            if (aCount + bCount >= MinCountForInclusion)
            {
                double aProbability = (aCount / TotalA).Min(1);
                double bProbability = (bCount / TotalB).Min(1);
                probability = MinTokenProbability.Max(MaxTokenProbability.Min(aProbability / (aProbability + bProbability)));
            }
            return probability;
        }

        #endregion
    }
}