﻿using System.Collections.Generic;
using MongoDB.Driver;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Model.Base;

namespace Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer.Interface
{
    /// <summary>
    /// IGenericDal
    /// </summary>
    public interface IGenericDal<TModel>:IDal where TModel : BaseModel, new()
    {
        #region Method

        #region GetOne

        ///// <summary>
        ///// 获取第一个结果
        ///// </summary>
        ///// <typeparam name="TModel"></typeparam>
        ///// <returns></returns>
        //TModel GetFirst();

        /// <summary>
        /// GetByQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        TModel GetByQuery(IMongoQuery query);

        #endregion

        #region GetList

        /// <summary>
        /// 获取全部结果
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        IList<TModel> GetList();

        #endregion

        #region Insert

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        void Insert(TModel model);

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelsList"></param>
        void Insert(TModel[] modelsList);

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelsList"></param>
        void Insert(List<TModel> modelsList);

        #endregion

        #region Update

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        void Update(TModel model);

        #endregion

        #region Delete

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        void Delete(TModel model);

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="modelList"></param>
        void Delete(List<TModel> modelList);

        /// <summary>
        /// DeleteAll
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        void DeleteAll();

        #endregion

        #region Drop

        /// <summary>
        /// DropCollection
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        CommandResult Drop();

        #endregion

        #endregion
    }
}
