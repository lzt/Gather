﻿using System.Collections.Generic;
using MongoDB.Driver;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Model.Base;

namespace Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer.Interface
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDal
    {
        #region Properties

        /// <summary>
        /// MongoDB连接字符串
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        string DataBaseName { get; set; }

        /// <summary>
        /// DBAssistant
        /// </summary>
        DBAssistant Assistant { get; set; }

        #endregion
    }
}
