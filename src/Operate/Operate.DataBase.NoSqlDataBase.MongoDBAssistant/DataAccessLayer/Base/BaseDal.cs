﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer.Interface;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Model.Base;

namespace Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseDal : IDal
    {
        #region Properties

        /// <summary>
        /// MongoDB连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// DBAssistant
        /// 助理
        /// </summary>
        public DBAssistant Assistant { get; set; }

        #endregion


        #region  Constructors

        /// <summary>
        /// BaseDal
        /// </summary>
        /// <param name="conn"></param>
        public BaseDal(string conn)
        {
            ConnectionString = conn;
            Assistant = new DBAssistant(ConnectionString);
            DataBaseName = Assistant.DataBaseName;
        }

        #endregion
    }
}
