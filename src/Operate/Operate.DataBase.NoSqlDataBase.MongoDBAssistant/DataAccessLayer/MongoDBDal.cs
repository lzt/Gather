﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer.Base;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Model.Base;

namespace Operate.DataBase.NoSqlDataBase.MongoDBAssistant.DataAccessLayer
{
    /// <summary>
    /// Dal
    /// </summary>
    public class MongoDBDal<TModel> : BaseDal where TModel : BaseModel, new()
    {
        #region Field

        /// <summary>
        /// ObjectId的键
        /// </summary>
        private const string ObjectidKey = "_id";

        #endregion

        #region Constructor

        /// <summary>
        /// MongoDBDal
        /// </summary>
        /// <param name="conn"></param>
        public MongoDBDal(string conn): base(conn)
        {
        }

        #endregion

        #region Method

        #region Function

        private string GetTypeName()
        {
            return typeof(TModel).Name;
        }

        /// <summary>
        /// 初始化查询记录 主要当该查询条件为空时 会附加一个恒真的查询条件，防止空查询报错
        /// </summary>
        /// <param name="query">查询的条件</param>
        /// <returns></returns>
        private IMongoQuery InitQuery(IMongoQuery query)
        {
            return query ?? (Query.Exists(ObjectidKey));
        }

        /// <summary>
        /// 初始化排序条件  主要当条件为空时 会默认以ObjectId递增的一个排序
        /// </summary>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        private SortByDocument InitSortBy(SortByDocument sortBy)
        {
            //负数为降序，0以上（包括0）为升序
            return sortBy ?? (new SortByDocument(ObjectidKey, -1));
        }

        #endregion

        #region AggregateExplain

        /// <summary>
        /// Aggregate
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public virtual IEnumerable<BsonDocument> Aggregate(AggregateArgs args)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            var result = col.Aggregate(args);
            return result;
        }

        /// <summary>
        /// AggregateExplain
        /// </summary>
        /// <returns></returns>
        public virtual CommandResult AggregateExplain(AggregateArgs args)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection<BsonDocument>(name);
            var result = col.AggregateExplain(args);
            return result;
        }

        #endregion

        #region Index

        /// <summary>
        /// CreateIndex
        /// </summary>
        /// <returns></returns>
        public virtual WriteConcernResult CreateIndex(params string[] keyNames)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection<BsonDocument>(name);
            var result = col.CreateIndex(keyNames);
            return result;
        }

        /// <summary>
        /// CreateIndex
        /// </summary>
        /// <returns></returns>
        public virtual WriteConcernResult CreateIndex(IMongoIndexKeys keys)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection<BsonDocument>(name);
            var result = col.CreateIndex(keys);
            return result;
        }

        /// <summary>
        /// CreateIndex
        /// </summary>
        /// <returns></returns>
        public virtual WriteConcernResult CreateIndex(IMongoIndexKeys keys, IMongoIndexOptions options)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.CreateIndex(keys, options);
            return result;
        }

        /// <summary>
        /// IndexExists
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public virtual bool IndexExists(IMongoIndexKeys keys)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            return col.IndexExists(keys);
        }

        /// <summary>
        /// IndexExists
        /// </summary>
        /// <param name="keyNames"></param>
        /// <returns></returns>
        public virtual bool IndexExists(params string[] keyNames)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection<BsonDocument>(name);
            return col.IndexExists(keyNames);
        }

        /// <summary>
        /// IndexExistsByName
        /// </summary>
        /// <param name="indexName"></param>
        /// <returns></returns>
        public virtual bool IndexExistsByName(string indexName)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            return col.IndexExists(indexName);
        }

        /// <summary>
        /// ReIndex
        /// </summary>
        /// <returns></returns>
        public virtual CommandResult ReIndex()
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            return col.ReIndex();
        }

        #endregion

        #region Distinct

        /// <summary>
        /// Distinct
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public IEnumerable<TModel> Distinct(DistinctArgs args)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.Distinct<TModel>(args);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<TModel> Distinct(string key)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.Distinct<TModel>(key);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual IEnumerable<TModel> Distinct(string key, IMongoQuery query)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.Distinct<TModel>(key, query);
            return result;
        }

        #endregion

        #region Count

        /// <summary>
        /// GetCount
        /// </summary>
        /// <returns></returns>
        public virtual long GetCount()
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.Count();
            return result;
        }

        /// <summary>
        /// GetCount
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public virtual long GetCount(CountArgs args)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.Count(args);
            return result;
        }

        /// <summary>
        /// GetCount
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual long GetCount(IMongoQuery query)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var result = col.Count(query);
            return result;
        }

        #endregion

        #region ExistByProperty

        /// <summary>
        /// Exists
        /// </summary>
        /// <returns></returns>
        public virtual bool ExistCollection()
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            return col.Exists();
        }

        #endregion

        #region GetOne

        /// <summary>
        /// GetById
        /// </summary>
        /// <returns></returns>
        public virtual TModel GetById(BsonValue id)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            return col.FindOneByIdAs<TModel>(id);
        }

        /// <summary>
        /// GetByQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual TModel GetByQuery(IMongoQuery query)
        {
            var name = GetTypeName();
            MongoCollection<TModel> col = Assistant.DataBase.GetCollection<TModel>(name);
            var result = col.FindOne(query);
            return result;
        }

        /// <summary>
        /// GetByQuery
        /// </summary>
        /// <param name="querydic"></param>
        /// <returns></returns>
        public virtual TModel GetByQuery(Dictionary<string, object> querydic)
        {
            var list=GetList(querydic, null, 1);
            return list == null ? null : (list.Count > 0 ? list[0] : null);
        }

        #endregion

        #region GetList

        /// <summary>
        /// GetListByTop
        /// </summary>
        /// <returns></returns>
        public virtual IList<TModel> GetListByTop(int topCount, Dictionary<string, object> querydic,
            SortByDocument sortBy = null)
        {
            return GetList(querydic, sortBy, topCount);
        }

        /// <summary>
        /// GetListByTop
        /// </summary>
        /// <returns></returns>
        public virtual IList<TModel> GetListByTop(int topCount, IMongoQuery query = null,
            SortByDocument sortBy = null)
        {
            return GetList(query, sortBy, topCount);
        }

        /// <summary>
        /// 获取集合
        /// </summary>
        ///<example>
        /// var sort = new SortByDocument() { { "Id", -1 } }
        /// </example>
        /// <param name="querydic">查询的条件字典</param>
        /// <param name="sortBy">排序的条件(默认_id降序排列)</param>
        /// <param name="topCount"></param>
        /// <returns></returns>
        public virtual IList<TModel> GetList(Dictionary<string, object> querydic, SortByDocument sortBy = null, int topCount=0)
        {
            var searchquery = new QueryDocument();
            searchquery.AddRange(querydic);

            var searchsortBy = InitSortBy(sortBy);
            return GetList(searchquery, searchsortBy, topCount);
        }

        /// <summary>
        /// GetList
        /// </summary>
        /// <returns></returns>
        public virtual IList<TModel> GetList(IMongoQuery query = null, SortByDocument sortBy = null, int topCount=0)
        {
            if (topCount <= 0)
            {
                //最大值限定为10000，防止数据量过大
                return GetList(1, 10000, query, sortBy);
            }
            return GetList(1, topCount, query, sortBy);
        }

        /// <summary>
        /// 分页查询 PageIndex和PageSize模式  在页数PageIndex大的情况下 效率明显变低
        /// </summary>
        /// <param name="query">查询的条件</param>
        /// <param name="pageIndex">当前的页数</param>
        /// <param name="pageSize">当前的尺寸</param>
        /// <param name="sortBy">排序方式</param>
        /// <returns>返回List列表</returns>
        public IList<TModel> GetList(int pageIndex, int pageSize, IMongoQuery query = null, SortByDocument sortBy = null)
        {
            if (pageSize <= 0)
            {
                throw new Exception("pageSize 应该大于0");
            }

            var name = GetTypeName();
            MongoCollection<TModel> col = Assistant.DataBase.GetCollection<TModel>(name);
            var searchquery = InitQuery(query);
            var searchsortBy = InitSortBy(sortBy);

            //如页序号为0时初始化为1
            pageIndex = pageIndex == 0 ? 1 : pageIndex;
            //按条件查询 排序 跳数 取数
            MongoCursor<TModel> mongoCursor = col.Find(searchquery).SetSortOrder(searchsortBy).SetSkip((pageIndex - 1) * pageSize).SetLimit(pageSize);

            return mongoCursor.ToList();
        }

        /// <summary>
        /// 分页查询 PageIndex和PageSize模式  在页数PageIndex大的情况下 效率明显变低
        /// </summary>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <param name="query">查询的条件</param>
        /// <param name="pageIndex">当前的页数</param>
        /// <param name="pageSize">当前的尺寸</param>
        /// <param name="sortBy">排序方式</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <returns>返回List列表</returns>
        public IList<TModel> GetList(int pageIndex, int pageSize, out long totalPages, out long totalRecords, IMongoQuery query = null, SortByDocument sortBy = null)
        {
            if (pageSize<=0)
            {
                throw new Exception("pageSize 应该大于0");
            }

            var name = GetTypeName();
            MongoCollection<TModel> col = Assistant.DataBase.GetCollection<TModel>(name);
            var searchquery = InitQuery(query);
            var searchsortBy = InitSortBy(sortBy);

            totalRecords = col.Find(searchquery).Count();
            totalPages = (totalRecords % pageSize == 0 ? totalRecords / pageSize : totalRecords / pageSize + 1);


            //如页序号为0时初始化为1
            pageIndex = pageIndex == 0 ? 1 : pageIndex;
            //按条件查询 排序 跳数 取数
            MongoCursor<TModel> mongoCursor = col.Find(searchquery).SetSortOrder(searchsortBy).SetSkip((pageIndex - 1) * pageSize).SetLimit(pageSize);

            return mongoCursor.ToList();
        }

        /// <summary>
        /// 分页查询 指定索引最后项-PageSize模式 
        /// </summary>
        /// <param name="query">查询的条件 没有可以为null</param>
        /// <param name="indexName">索引名称</param>
        /// <param name="lastKeyValue">最后索引的值</param>
        /// <param name="pageSize">分页的尺寸</param>
        /// <param name="sortType">排序类型 1升序 -1降序 仅仅针对该索引</param>
        /// <returns>返回一个List列表数据</returns>
        public List<TModel> GetList(IMongoQuery query, string indexName, object lastKeyValue, int pageSize, int sortType)
        {
            if (pageSize <= 0)
            {
                throw new Exception("pageSize 应该大于0");
            }

            var name = GetTypeName();
            MongoCollection<TModel> col = Assistant.DataBase.GetCollection<TModel>(name);
            var searchquery = InitQuery(query);
            MongoCursor<TModel> mongoCursor;
            //判断升降序后进行查询
            if (sortType > 0)
            {
                //升序
                if (lastKeyValue != null)
                {
                    //有上一个主键的值传进来时才添加上一个主键的值的条件
                    searchquery = Query.And(searchquery, Query.GT(indexName, BsonValue.Create(lastKeyValue)));
                }
                //先按条件查询 再排序 再取数
                mongoCursor = col.Find(searchquery).SetSortOrder(new SortByDocument(indexName, 1)).SetLimit(pageSize);
            }
            else
            {
                //降序
                if (lastKeyValue != null)
                {
                    searchquery = Query.And(searchquery, Query.LT(indexName, BsonValue.Create(lastKeyValue)));
                }
                mongoCursor = col.Find(searchquery).SetSortOrder(new SortByDocument(indexName, -1)).SetLimit(pageSize);
            }

            return mongoCursor.ToList();
        }

        /// <summary>
        /// 分页查询 指定索引最后项-PageSize模式 
        /// </summary>
        /// <param name="query">查询的条件 没有可以为null</param>
        /// <param name="indexName">索引名称</param>
        /// <param name="lastKeyValue">最后索引的值</param>
        /// <param name="pageSize">分页的尺寸</param>
        /// <param name="sortType">排序类型 1升序 -1降序 仅仅针对该索引</param>
        /// <param name="totalPages">输出 总页数</param>
        /// <param name="totalRecords">输出 总记录数</param>
        /// <returns>返回一个List列表数据</returns>
        public List<TModel> GetList(IMongoQuery query, string indexName, object lastKeyValue, int pageSize, int sortType, out long totalPages, out long totalRecords )
        {
            if (pageSize <= 0)
            {
                throw new Exception("pageSize 应该大于0");
            }

            var name = GetTypeName();
            MongoCollection<TModel> col = Assistant.DataBase.GetCollection<TModel>(name);
            var searchquery = InitQuery(query);
            MongoCursor<TModel> mongoCursor;

            //判断升降序后进行查询
            if (sortType > 0)
            {
                //升序
                if (lastKeyValue != null)
                {
                    //有上一个主键的值传进来时才添加上一个主键的值的条件
                    searchquery = Query.And(searchquery, Query.GT(indexName, BsonValue.Create(lastKeyValue)));
                }

                totalRecords = col.Find(searchquery).Count();
                totalPages = (totalRecords % pageSize == 0 ? totalRecords / pageSize : totalRecords / pageSize + 1);

                //先按条件查询 再排序 再取数
                mongoCursor = col.Find(searchquery).SetSortOrder(new SortByDocument(indexName, 1)).SetLimit(pageSize);
            }
            else
            {
                //降序
                if (lastKeyValue != null)
                {
                    searchquery = Query.And(searchquery, Query.LT(indexName, BsonValue.Create(lastKeyValue)));
                }

                totalRecords = col.Find(searchquery).Count();
                totalPages = (totalRecords % pageSize == 0 ? totalRecords / pageSize : totalRecords / pageSize + 1);

                mongoCursor = col.Find(searchquery).SetSortOrder(new SortByDocument(indexName, -1)).SetLimit(pageSize);
            }

            return mongoCursor.ToList();
        }

        /// <summary>
        /// 分页查询 指定ObjectId最后项-PageSize模式 
        /// </summary>
        /// <param name="query">查询的条件 没有可以为null</param>
        /// <param name="lastObjectId">上一条记录的ObjectId 没有可以为空</param>
        /// <param name="pageSize">每页尺寸</param>
        /// <param name="sortType">排序类型 1升序 -1降序 仅仅针对_id</param>
        /// <returns>返回一个List列表数据</returns>
        public List<TModel> GetList(IMongoQuery query, string lastObjectId, int pageSize, int sortType)
        {
            return GetList(query, ObjectidKey, new ObjectId(lastObjectId), pageSize, sortType);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="model"></param>
        public virtual void Insert(TModel model)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            col.Insert(model);
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="modelsList"></param>
        public virtual void Insert(TModel[] modelsList)
        {
            var list = modelsList.ToList();
            Insert(list);
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="modelsList"></param>
        public virtual void Insert(List<TModel> modelsList)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            col.InsertBatch(modelsList);
        }

        #endregion

        #region Update

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="model"></param>
        public virtual void Update(TModel model)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);

            var querysearch = model.GetQueryByIdentity();
            var query = new QueryDocument();
            var oldmodel = GetByQuery(querysearch);
            var type = oldmodel.GetType();
            var properties = type.GetProperties();
            foreach (var p in properties)
            {
                if (p.Name == "Id")
                {
                    continue;
                }

                var oldvalue = p.GetValue(oldmodel, null);
                var value = p.GetValue(model, null);

                if ((oldvalue.IsNull() && !value.IsNull()) || (!oldvalue.IsNull() && value.IsNull()))
                {
                    var dic = new Dictionary<string, object> { { p.Name, value } };
                    query.AddRange(dic);
                }
                else
                {
                    if (!oldvalue.Equals(value))
                    {
                        var dic = new Dictionary<string, object> { { p.Name, value } };
                        query.AddRange(dic);
                    }
                }
            }
            if (query.Any())
            {
                var update = new UpdateDocument { { "$set", query } };
                col.Update(querysearch, update);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// DeleteById
        /// </summary>
        /// <returns></returns>
        public virtual void DeleteById(BsonValue id)
        {
            var m = GetById(id);
            if (m!=null)
            {
                Delete(m);
            }
        }

        /// <summary>
        /// DeleteByQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual void DeleteByQuery(IMongoQuery query)
        {
            var m = GetByQuery(query);
            if (m != null)
            {
                Delete(m);
            }
        }

        /// <summary>
        /// DeleteByQuery
        /// </summary>
        /// <param name="querydic"></param>
        /// <returns></returns>
        public virtual void DeleteByQuery(Dictionary<string, object> querydic)
        {
            var m = GetByQuery(querydic);
            if (m != null)
            {
                Delete(m);
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="model"></param>
        public virtual void Delete(TModel model)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            var query = model.GetQueryByIdentity();
            col.Remove(query);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="modelList"></param>
        public virtual void Delete(List<TModel> modelList)
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);

            foreach (var m in modelList)
            {
                var query = m.GetQueryByIdentity();
                col.Remove(query);
            }
        }

        /// <summary>
        /// DeleteAll
        /// </summary>
        public virtual void DeleteAll()
        {
            var name = GetTypeName();
            MongoCollection col = Assistant.DataBase.GetCollection(name);
            col.RemoveAll();
        }

        #endregion

        #region Drop

        /// <summary>
        /// Drop
        /// </summary>
        public virtual CommandResult Drop()
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            return col.Drop();
        }

        /// <summary>
        /// DropAllIndexes
        /// </summary>
        /// <returns></returns>
        public virtual CommandResult DropAllIndexes()
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            return col.DropAllIndexes();
        }

        /// <summary>
        /// DropIndex
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public virtual CommandResult DropIndex(IMongoIndexKeys keys)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            return col.DropIndex(keys);
        }

        /// <summary>
        /// DropIndex
        /// </summary>
        /// <param name="keyNames"></param>
        /// <returns></returns>
        public virtual CommandResult DropIndex(params string[] keyNames)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            return col.DropIndex(keyNames);
        }

        /// <summary>
        /// DropIndexByName
        /// </summary>
        /// <param name="indexName"></param>
        /// <returns></returns>
        public virtual CommandResult DropIndexByName(string indexName)
        {
            var name = GetTypeName();
            var col = Assistant.DataBase.GetCollection(name);
            return col.DropIndexByName(indexName);
        }

        #endregion

        #endregion
    }
}
