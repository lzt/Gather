﻿using System;
using MongoDB.Driver;

namespace Operate.DataBase.NoSqlDataBase.MongoDBAssistant
{
    /// <summary>
    /// MongoDB 辅助操作类
    /// </summary>
    public class DBAssistant
    {
        #region Properties

        /// <summary>
        /// MongoDB连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// MongoDB连接字符串端口
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// Server
        /// </summary>
        public MongoServer Server { get; private set; }

        ///// <summary>
        ///// Client
        ///// </summary>
        //public MongoClient Client { get; private set; }

        /// <summary>
        /// DataBase
        /// </summary>
        public MongoDatabase DataBase { get; private set; }

        /// <summary>
        /// Timeout
        /// </summary>
        public TimeSpan Timeout { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// DBAssistant
        /// </summary>
        /// <param name="conn"></param>
        public DBAssistant(string conn)
        {
            var mangoUrl = new MongoUrl(conn);

            ConnectionString = mangoUrl.Url;
            Port = mangoUrl.Server.Port;
            DataBaseName = mangoUrl.DatabaseName;
            Timeout = mangoUrl.ConnectTimeout;

            //var mongoSetting = new MongoClientSettings()
            //{
            //    Server = mangoUrl.Server
            //};
            //if (Timeout > 0)
            //{
            //    mongoSetting.ConnectTimeout = mangoUrl.ConnectTimeout;
            //}

            //Client = new MongoClient(mangoUrl);
            //Server = new MongoServer(new MongoServerSettings()
            //{
                
            //});

            Server = new MongoServer(new MongoServerSettings()
            {
                Server = mangoUrl.Server,
                ConnectTimeout = mangoUrl.ConnectTimeout
            });
            DataBase = Server.GetDatabase(DataBaseName);
        }

        ///// <summary>
        ///// DBAssistant
        ///// </summary>
        ///// <param name="conn"></param>
        ///// <param name="dbName"></param>
        ///// <param name="port"></param>
        //public DBAssistant(string conn, string dbName, int port = 27017)
        //{
        //    Port = port;
        //    ConnectionString = conn;
        //    DataBaseName = dbName;
        //    Init();
        //}

        ///// <summary>
        ///// DBAssistant
        ///// </summary>
        ///// <param name="conn"></param>
        ///// <param name="dbName"></param>
        ///// <param name="timeout"></param>
        ///// <param name="port"></param>
        //public DBAssistant(string conn, string dbName, int timeout , int port = 27017)
        //{
        //    Port = port;
        //    ConnectionString = conn;
        //    DataBaseName = dbName;
        //    Timeout = timeout;
        //    Init();
        //}

        #endregion

        #region Method

        ///// <summary>
        ///// Init
        ///// </summary>
        //private void Init()
        //{
        //    //设置连接超时时间
        //    //设置数据库服务器
        //    var mongoSetting = new MongoClientSettings()
        //    {
        //        Server = new MongoServerAddress(ConnectionString, Port),
        //    };
        //    if (Timeout > 0)
        //    {
        //        mongoSetting.ConnectTimeout = new TimeSpan(Timeout*TimeSpan.TicksPerSecond);
        //    }

        //    Client = new MongoClient(mongoSetting);
        //    //Server = Client.GetServer();
        //    DataBase = Client.GetDatabase(DataBaseName);
        //}

        #endregion

    }
}
