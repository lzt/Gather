﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Attribute;

namespace Operate.DataBase.NoSqlDataBase.MongoDBAssistant.Model.Base
{
    /// <summary>
    /// BaseModel
    /// </summary>
    public class BaseModel
    {
        #region Field

        private ObjectId _id;

        #endregion

        #region Properties

        /// <summary>
        /// BsonType.ObjectId 这个对应了 MongoDB.Bson.ObjectId 
        /// </summary>
        [Field(true)]
        public ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// GetQueryByIdentity
        /// </summary>
        /// <returns></returns>
        public IMongoQuery GetQueryByIdentity()
        {
            if (Id.IsNull())
            {
                throw new Exception("此_Id没有存在对象");
            }

            var r= Query.EQ("_id", Id);

            return r;
        }

        /// <summary>
        /// 转化为Dictionary
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> ToDictionary()
        {
            var result = new Dictionary<string, object>();
            var type = GetType();
            var properties = type.GetProperties();
            foreach (var p in properties)
            {
                var value = p.GetValue(this, null);
                result.Add(p.Name, value);
            }

            return result;
        }

        #endregion
    }
}
