﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;

namespace Operate.Reflection
{
    /// <summary>
    /// EnumEx
    /// </summary>
    public class EnumEx
    {
        /// <summary>
        /// GetIntValues
        /// 获取枚举值对应的整形值集合
        /// </summary>
        /// <param name="enumType">枚举类型</param>
        /// <returns></returns>
        public static int[] GetIntValues(Type enumType)
        {
            var enumvaluelist = Enum.GetValues(enumType);
            return enumvaluelist.Cast<int>().OrderBy(x=>x).ToArray();
        }

        /// <summary>
        /// GetIntValues
        /// 获取枚举值对应的整形值集合
        /// </summary>
        /// <returns></returns>
        public static int[] GetIntValues<T>()
        {
            return GetIntValues(typeof(T));
        }

        /// <summary>
        /// 验证键存在
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="name">要检测的键</param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool ExistName(Type enumType, string name, StringComparison comparison = StringComparison.Ordinal)
        {
            var result = false;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (string oneName in Enum.GetNames(enumType))
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (oneName.Equals(name, comparison))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// 验证键存在
        /// </summary>
        /// <param name="name">要检测的键</param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool ExistName<T>(string name, StringComparison comparison = StringComparison.Ordinal)
        {
            return ExistName(typeof(T), name, comparison);
        }

        /// <summary>
        /// 验证值存在
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="value">要检测的值</param>
        /// <returns></returns>
        public static bool ExistValue(Type enumType, int value)
        {
            var result = false;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (int oneValue in Enum.GetValues(enumType))
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (oneValue == value)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// 验证值存在
        /// </summary>
        /// <param name="value">要检测的值</param>
        /// <returns></returns>
        public static bool ExistValue<T>(int value)
        {
            return ExistValue(typeof(T), value);
        }

        /// <summary>
        /// 获取键集合
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static List<string> GetNameList(Type enumType)
        {
            var result = new List<string>();
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (string oneName in Enum.GetNames(enumType))
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                result.Add(oneName);
            }
            return result;
        }

        /// <summary>
        /// 获取键集合
        /// </summary>
        /// <returns></returns>
        public static List<string> GetNameList<T>()
        {
            return GetNameList(typeof(T));
        }

        /// <summary>
        /// 通过ID获取枚举中相应的Name
        /// </summary>
        /// <returns></returns>
        public static string GetNameByValue(Type enumType, int enumValue)
        {
            string result = null;
            bool valueExist = false;

            var positionValue = 0;
            foreach (int oneValue in Enum.GetValues(enumType))
            {
                positionValue += 1;
                if (oneValue == enumValue)
                {
                    valueExist = true;
                    break;
                }
            }

            if (!valueExist)
            {
                return null;
            }

            var positionName = 0;
            foreach (string oneName in Enum.GetNames(enumType))
            {
                positionName += 1;
                if (positionName == positionValue)
                {
                    result = oneName;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// 通过ID获取枚举中相应的Name
        /// </summary>
        /// <returns></returns>
        public static string GetNameByValue<T>(int enumValue)
        {
            return GetNameByValue(typeof(T), enumValue);
        }

        /// <summary>
        /// 通过Name获取枚举中相应的值
        /// </summary>
        /// <param name="enumName">枚举中的名称</param>
        /// <param name="enumtype"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static int? GetValueByName(Type enumtype, string enumName, StringComparison comparison = StringComparison.Ordinal)
        {
            int? result = null;
            var valueExist = false;

            var positionName = 0;
            foreach (string oneName in Enum.GetNames(enumtype))
            {
                positionName += 1;
                if (oneName.Equals(enumName, comparison))
                {
                    valueExist = true;
                    break;
                }
            }

            if (!valueExist)
            {
                return null;
            }

            var positionValue = 0;
            foreach (int oneValue in Enum.GetValues(enumtype))
            {
                positionValue += 1;
                if (positionName == positionValue)
                {
                    result = oneValue;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// 通过Name获取枚举中相应的值
        /// </summary>
        /// <param name="enumName">枚举中的名称</param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static int? GetValueByName<T>(string enumName, StringComparison comparison = StringComparison.Ordinal)
        {
            return GetValueByName(typeof(T), enumName, comparison);
        }

        /// <summary>
        /// ConvertEnumToJson
        /// </summary>
        /// <param name="enumtype"></param>
        /// <param name="changeKeyToLowerFirst"></param>
        /// <param name="ignoreList"></param>
        /// <returns></returns>
        public static string ConvertEnumToJson(Type enumtype, bool changeKeyToLowerFirst = false, IEnumerable<object> ignoreList = null)
        {
            var hashlist = new List<Hashtable>();
            var valuelist = GetIntValues(enumtype);
            var ignoreValueList = new List<int>();

            if (ignoreList != null)
            {
                ignoreValueList = ignoreList.Cast<int>().OrderBy(x=>x).ToList();
            }

            foreach (var v in valuelist)
            {
                if (ignoreList != null)
                {
                    if (ignoreValueList.Contains(v))
                    {
                        continue;
                    }
                }

                var h = new Hashtable();
                var s = Enum.GetName(enumtype, v);

                Enum enumItem = Enum.ToObject(enumtype, v) as Enum;
                var descriptionAttribute = enumItem.GetAttribute<DescriptionAttribute>();
                var description = descriptionAttribute == null ? "" : descriptionAttribute.Description;

                if (changeKeyToLowerFirst)
                {
                    h["name"] = s.ToLowerFirst();
                    h["value"] = v;
                    h["internalName"] = s;
                    h["description"] = description;
                }
                else
                {
                    h["name"] = s;
                    h["value"] = v;
                    h["description"] = description;
                }
                hashlist.Add(h);
            }
            return JsonConvert.SerializeObject(hashlist);
        }

        /// <summary>
        /// ConvertEnumToJson
        /// </summary>
        /// <param name="changeKeyToLowerFirst"></param>
        /// <param name="ignoreList"></param>
        /// <returns></returns>
        public static string ConvertEnumToJson<T>(bool changeKeyToLowerFirst = false, IEnumerable<T> ignoreList = null)
        {
            return ConvertEnumToJson(typeof(T), changeKeyToLowerFirst, ignoreList == null ? null : ignoreList.Cast<object>());
        }

        /// <summary>
        /// ConvertEnumToObject
        /// </summary>
        /// <param name="enumtype"></param>
        /// <param name="changeKeyToLowerFirst"></param>
        /// <param name="ignoreList"></param>
        /// <returns></returns>
        public static object ConvertEnumToObject(Type enumtype, bool changeKeyToLowerFirst = false, IEnumerable<object> ignoreList = null)
        {
            var json = ConvertEnumToJson(enumtype, changeKeyToLowerFirst, ignoreList);
            return JsonConvert.DeserializeObject(json);
        }

        /// <summary>
        /// ConvertEnumToObject
        /// </summary>
        /// <param name="changeKeyToLowerFirst"></param>
        /// <param name="ignoreList"></param>
        /// <returns></returns>
        public static object ConvertEnumToObject<T>(bool changeKeyToLowerFirst = false, IEnumerable<T> ignoreList = null)
        {
            return ConvertEnumToObject(typeof(T), changeKeyToLowerFirst, ignoreList == null ? null : ignoreList.Cast<object>());
        }
    }
}
