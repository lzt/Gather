﻿using System.Collections.Generic;
using System.Dynamic;
using Operate.ExtensionMethods;

namespace Operate.Reflection.ExtensionMethods
{
    /// <summary>
    /// ObjectExtensions
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// 转换为Dictionary
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDictionary(this object source)
        {
            var result = new Dictionary<string, object>();

            var type = source.GetType();
            var properties = type.GetProperties();

            foreach (var p in properties)
            {
                result.Add(new KeyValuePair<string, object>(p.Name, p.GetValue(source, null)));
            }

            return result;
        }

        /// <summary>
        /// 转换为ExpandoObject
        /// </summary>
        /// <param name="source"></param>
        /// <param name="keyToLower"></param>
        /// <returns></returns>
        public static ExpandoObject ToExpandoObject(this object source,bool keyFisrtLetterToLower=true)
        {
            var result = new ExpandoObject();

            var type = source.GetType();
            var properties = type.GetProperties();

            foreach (var p in properties)
            {
                result.Add(new KeyValuePair<string, object>(keyFisrtLetterToLower ? p.Name.ToLowerFirst(): p.Name, p.GetValue(source, null)));
            }

            return result;
        }
    }
}