﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Operate.Reflection.Emit;

namespace Operate.Reflection.ExtensionMethods
{
    /// <summary>
    /// AssemblyExtensions
    /// </summary>
    public static class AssemblyExtensions
    {
        /// <summary>
        /// 获取程序集路径
        /// </summary>
        /// <returns></returns>
        public static string GetPath(this System.Reflection.Assembly assembly)
        {
            var assemblyPath = assembly.CodeBase.ToLower().Replace(assembly.ManifestModule.Name.ToLower(), "");
            assemblyPath = assemblyPath.Replace("file:///", "");
            assemblyPath = assemblyPath.Replace("/", "\\");
            return assemblyPath;
        }
    }
}
