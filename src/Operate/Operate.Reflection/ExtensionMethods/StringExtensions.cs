﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.Reflection.ExtensionMethods
{
    /// <summary>
    /// StringExtensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 转换为枚举
        /// </summary>
        /// <param name="source"></param>
        /// <param name="entype"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static int? ToEnum(this string source, Enum entype, StringComparison comparison = StringComparison.Ordinal)
        {
            if (entype.ExistName(source, comparison))
            {
                return entype.GetValueByName(source, comparison);
            }
            throw new Exception("无法将枚举转换为枚举，枚举中该键不存在");
        }
    }
}
