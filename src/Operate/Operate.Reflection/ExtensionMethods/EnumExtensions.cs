﻿using System;
using System.Collections.Generic;
using Operate.ExtensionMethods;

namespace Operate.Reflection.ExtensionMethods
{
    /// <summary>
    /// Enum扩展
    /// </summary>
    public static class EnumExtensions
    {
        #region tramsform Value or key

        /// <summary>
        /// 验证键存在
        /// </summary>
        /// <param name="objType"></param>
        /// <param name="name">要检测的键</param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static bool ExistName(this Enum objType, string name, StringComparison comparison = StringComparison.Ordinal)
        {
            return EnumEx.ExistName(objType.GetType(), name, comparison);
        }

        /// <summary>
        /// 验证值存在
        /// </summary>
        /// <param name="objType"></param>
        /// <param name="value">要检测的值</param>
        /// <returns></returns>
        public static bool ExistValue(this Enum objType, int value)
        {
            return EnumEx.ExistValue(objType.GetType(), value);
        }

        /// <summary>
        /// 获取键集合
        /// </summary>
        /// <param name="objType"></param>
        /// <returns></returns>
        public static List<string> GetNameList(this Enum objType)
        {
            return EnumEx.GetNameList(objType.GetType());
        }

        /// <summary>
        /// 获取值集合
        /// </summary>
        /// <param name="objType"></param>
        /// <returns></returns>
        public static List<int> GetValueList(this Enum objType)
        {
            return EnumEx.GetIntValues(objType.GetType()).ToList();
        }

        /// <summary>
        /// 通过ID获取枚举中相应的Name
        /// </summary>
        /// <returns></returns>
        public static string GetNameByValue(this Enum objType, int enumValue)
        {
            return EnumEx.GetNameByValue(objType.GetType(),enumValue);
        }

        /// <summary>
        /// 通过Name获取枚举中相应的值
        /// </summary>
        /// <param name="enumName">枚举中的名称</param>
        /// <param name="objType"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static int? GetValueByName(this Enum objType, string enumName, StringComparison comparison = StringComparison.Ordinal)
        {
            return EnumEx.GetValueByName(objType.GetType(), enumName, comparison);
        }

        #endregion
    }
}
