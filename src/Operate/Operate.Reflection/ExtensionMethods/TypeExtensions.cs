﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Operate.ExtensionMethods;

namespace Operate.Reflection.ExtensionMethods
{
    /// <summary>
    /// Type扩展
    /// </summary>
    public static class TypeExtensions
    {
        #region Enum

        /// <summary>
        /// 判断是否包含该类型特性
        /// </summary>
        /// <typeparam name="T">目标类型</typeparam>
        /// <returns></returns>
        public static bool ContainAttribute<T>(this Enum source) where T : new()
        {
            var field = source.GetType().GetField(source.ToString());
            return field.ContainAttribute<T>(false, MemberTypes.Field);
        }

        /// <summary>
        /// 获取特性
        /// </summary>
        /// <typeparam name="T">目标类型</typeparam>
        /// <param name="source">调用源</param>
        /// <param name="nameFilter">筛选名称</param>
        /// <returns></returns>
        public static T GetAttribute<T>(this Enum source, string nameFilter = "") where T : new()
        {
            var field = source.GetType().GetField(source.ToString());
            return field.GetAttribute<T>(nameFilter, false, MemberTypes.Field);
        }

        /// <summary>
        /// 获取特性
        /// </summary>
        /// <param name="source">调用源</param>
        /// <param name="nameFilter">筛选名称</param>
        /// <returns></returns>
        public static List<object> GetAttribute(this Enum source, string nameFilter = "")
        {
            var field = source.GetType().GetField(source.ToString());
            return field.GetAttribute(nameFilter, false, MemberTypes.Field);
        }

        #endregion

        #region Object

        /// <summary>
        /// 判断是否包含该类型特性
        /// </summary>
        /// <typeparam name="T">目标类型</typeparam>
        /// <returns></returns>
        public static bool ContainAttribute<T>(this object source, bool inherit = false, MemberTypes memberTypes = MemberTypes.All) where T : new()
        {
            var attr = source.GetAttribute<T>();
            return !attr.IsNull();
        }

        /// <summary>
        /// 获取特性
        /// </summary>
        /// <typeparam name="T">目标类型</typeparam>
        /// <param name="source">调用源</param>
        /// <param name="nameFilter">筛选名称</param>
        /// <param name="inherit">继承</param>
        /// <param name="memberTypes">成员类型</param>
        /// <returns></returns>
        public static T GetAttribute<T>(this object source, string nameFilter = "", bool inherit = false, MemberTypes memberTypes = MemberTypes.All) where T : new()
        {
            var check = new T();
            var result = default(T);
            var list = source.GetAttribute(nameFilter, inherit, memberTypes);
            foreach (var attr in list)
            {
                if (attr.GetType().FullName == check.GetType().FullName)
                {
                    result = (T)attr;
                }
            }
            return result;
        }

        /// <summary>
        /// 获取特性
        /// </summary>
        /// <param name="source">调用源</param>
        /// <param name="nameFilter">筛选名称</param>
        /// <param name="inherit">继承</param>
        /// <param name="memberTypes">成员类型</param>
        /// <returns></returns>
        public static List<object> GetAttribute(this object source, string nameFilter = "", bool inherit = false, MemberTypes memberTypes = MemberTypes.All)
        {
            if ((source as PropertyInfo) != null)
            {
                var tryPropertyInfo = source as PropertyInfo;
                List<object> attrs = tryPropertyInfo.GetCustomAttributes(inherit).ToList();
                return attrs;
            }
            else if ((source as FieldInfo) != null)
            {
                var tryPropertyInfo = source as FieldInfo;
                List<object> attrs = tryPropertyInfo.GetCustomAttributes(inherit).ToList();
                return attrs;
            }
            else if ((source as MemberInfo) != null)
            {
                var tryPropertyInfo = source as MemberInfo;
                List<object> attrs = tryPropertyInfo.GetCustomAttributes(inherit).ToList();
                return attrs;
            }
            else
            {
                Type type = source.GetType();
                var attrs = new List<object>();
                switch (memberTypes)
                {
                    case MemberTypes.All:
                        attrs = type.GetCustomAttributes(inherit).ToList();
                        break;
                    case MemberTypes.Field:
                        var field = type.GetField(nameFilter);
                        attrs = field.IsNull() ? attrs : field.GetCustomAttributes(inherit).ToList();
                        break;
                    case MemberTypes.Method:
                        var method = type.GetMethod(nameFilter);
                        attrs = method.IsNull() ? attrs : method.GetCustomAttributes(inherit).ToList();
                        break;
                    case MemberTypes.Property:
                        var property = type.GetProperty(nameFilter);
                        attrs = property.IsNull() ? attrs : property.GetCustomAttributes(inherit).ToList();
                        break;
                }
                return attrs;
            }
        }

        #endregion
    }
}
