﻿/*
Copyright (c) 2011 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using Operate.ExtensionMethods;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

#endregion Usings

namespace Operate.Reflection
{
    /// <summary>
    /// Utility class that handles various functions dealing with reflection.
    /// </summary>
    public static class Reflection
    {
        #region Public Static Functions

        #region CallMethod

        /// <summary>
        /// Calls a method on an object
        /// </summary>
        /// <param name="methodName">Method name</param>
        /// <param name="Object">Object to call the method on</param>
        /// <param name="inputVariables">(Optional)input variables for the method</param>
        /// <returns>The returned value of the method</returns>
        public static object CallMethod(string methodName, object Object, params object[] inputVariables)
        {
            if (string.IsNullOrEmpty(methodName) || Object == null)
            {
                return null;
            }

            Type objectType = Object.GetType();
            MethodInfo method;
            if (inputVariables != null)
            {
                var methodInputTypes = new Type[inputVariables.Length];
                for (int x = 0; x < inputVariables.Length; ++x)
                {
                    methodInputTypes[x] = inputVariables[x].GetType();
                }
                method = objectType.GetMethod(methodName, methodInputTypes);
                if (method != null)
                {
                    return method.Invoke(Object, inputVariables);
                }
            }
            method = objectType.GetMethod(methodName);
            if (method != null)
            {
                return method.Invoke(Object, null);
            }
            return null;
        }

        #endregion CallMethod

        #region DumpAllAssembliesAndProperties

        /// <summary>
        /// Returns all assemblies and their properties
        /// </summary>
        /// <returns>
        /// An HTML formatted string that contains the assemblies and their information
        /// </returns>
        public static string DumpAllAssembliesAndProperties()
        {
            var builder = new StringBuilder();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                builder.Append("<strong>").Append(assembly.GetName().Name).Append("</strong><br />");
                builder.Append(DumpProperties(assembly)).Append("<br /><br />");
            }
            return builder.ToString();
        }

        #endregion DumpAllAssembliesAndProperties

        #region DumpProperties

        /// <summary>
        /// Dumps the properties names and current values from an object
        /// </summary>
        /// <param name="Object">Object to dunp</param>
        /// <returns>An HTML formatted table containing the information about the object</returns>
        public static string DumpProperties(object Object)
        {
            if (Object == null)
            {
                throw new ArgumentNullException("Object");
            }

            var tempValue = new StringBuilder();
            tempValue.Append("<table><thead><tr><th>Property Name</th><th>Property Value</th></tr></thead><tbody>");
            Type objectType = Object.GetType();
            var properties = objectType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                tempValue.Append("<tr><td>").Append(property.Name).Append("</td><td>");
                var parameters = property.GetIndexParameters();
                if (property.CanRead && parameters.Length == 0)
                {
                    try
                    {
                        object value = property.GetValue(Object, null);
                        tempValue.Append(value == null ? "null" : value.ToString());
                    }

                    // ReSharper disable EmptyGeneralCatchClause
                    catch { }

                    // ReSharper restore EmptyGeneralCatchClause
                }
                tempValue.Append("</td></tr>");
            }
            tempValue.Append("</tbody></table>");
            return tempValue.ToString();
        }

        /// <summary>
        /// Dumps the properties names and current values from an object type (used for static
        /// classes)
        /// </summary>
        /// <param name="objectType">Object type to dunp</param>
        /// <returns>
        /// An HTML formatted table containing the information about the object type
        /// </returns>
        public static string DumpProperties(Type objectType)
        {
            if (objectType == null)
            {
                throw new ArgumentNullException("objectType");
            }

            var tempValue = new StringBuilder();
            tempValue.Append("<table><thead><tr><th>Property Name</th><th>Property Value</th></tr></thead><tbody>");
            PropertyInfo[] properties = objectType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                tempValue.Append("<tr><td>").Append(property.Name).Append("</td><td>");
                if (property.GetIndexParameters().Length == 0)
                {
                    try
                    {
                        tempValue.Append(property.GetValue(null, null) == null ? "null" : property.GetValue(null, null).ToString());
                    }

                    // ReSharper disable EmptyGeneralCatchClause
                    catch { }

                    // ReSharper restore EmptyGeneralCatchClause
                }
                tempValue.Append("</td></tr>");
            }
            tempValue.Append("</tbody></table>");
            return tempValue.ToString();
        }

        #endregion DumpProperties

        #region GetAssembliesFromDirectory

        /// <summary>
        /// Gets a list of assemblies from a directory
        /// </summary>
        /// <param name="directory">The directory to search in</param>
        /// <param name="recursive">Determines whether to search recursively or not</param>
        /// <returns>List of assemblies in the directory</returns>
        public static System.Collections.Generic.List<Assembly> GetAssembliesFromDirectory(string directory, bool recursive = false)
        {
            var returnList = new System.Collections.Generic.List<Assembly>();
            System.Collections.Generic.List<FileInfo> files = new DirectoryInfo(directory).GetFiles("*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).ToList();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (FileInfo file in files)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (file.Extension.Equals(".dll", StringComparison.CurrentCultureIgnoreCase))
                {
                    returnList.Add(Assembly.LoadFile(file.FullName));
                }
            }
            return returnList;
        }

        #endregion GetAssembliesFromDirectory

        #region GetLoadedAssembly

        /// <summary>
        /// Gets an assembly by its name if it is currently loaded
        /// </summary>
        /// <param name="name">Name of the assembly to return</param>
        /// <returns>The assembly specified if it exists, otherwise it returns null</returns>
        public static Assembly GetLoadedAssembly(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var tempAssembly in AppDomain.CurrentDomain.GetAssemblies())

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (tempAssembly.GetName().Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    return tempAssembly;
                }
            }
            return null;
        }

        #endregion GetLoadedAssembly

        #region GetObjectsFromAssembly

        /// <summary>
        /// Returns an instance of all classes that it finds within an assembly that are of the
        /// specified base type/interface.
        /// </summary>
        /// <typeparam name="TClassType">Base type/interface searching for</typeparam>
        /// <param name="assembly">Assembly to search within</param>
        /// <returns>A list of objects that are of the type specified</returns>
        public static System.Collections.Generic.List<TClassType> GetObjectsFromAssembly<TClassType>(Assembly assembly)
        {
            var types = GetTypes(assembly, typeof(TClassType));
            var returnValues = new System.Collections.Generic.List<TClassType>();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Type type in types)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                returnValues.Add((TClassType)Activator.CreateInstance(type));
            }
            return returnValues;
        }

        /// <summary>
        /// Returns an instance of all classes that it finds within an assembly that are of the
        /// specified base type/interface.
        /// </summary>
        /// <typeparam name="TClassType">Base type/interface searching for</typeparam>
        /// <param name="directory">Directory to search within</param>
        /// <returns>A list of objects that are of the type specified</returns>
        public static System.Collections.Generic.List<TClassType> GetObjectsFromAssembly<TClassType>(string directory)
        {
            var returnValues = new System.Collections.Generic.List<TClassType>();
            var assemblies = LoadAssembliesFromDirectory(directory, true);

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Assembly assembly in assemblies)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                System.Collections.Generic.List<Type> types = GetTypes(assembly, typeof(TClassType));

                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (Type type in types)

                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    returnValues.Add((TClassType)Activator.CreateInstance(type));
                }
            }
            return returnValues;
        }

        #endregion GetObjectsFromAssembly

        #region GetPropertyGetter

        /// <summary>
        /// Gets a lambda expression that calls a specific property's getter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <typeparam name="TDataType">Data type expecting</typeparam>
        /// <param name="propertyName">Property name</param>
        /// <returns>A lambda expression that calls a specific property's getter function</returns>
        public static Expression<Func<TClassType, TDataType>> GetPropertyGetter<TClassType, TDataType>(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            string[] splitName = propertyName.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            PropertyInfo property = GetProperty<TClassType>(splitName[0]);
            if (property != null)
            {
                if (property.DeclaringType != null)
                {
                    ParameterExpression objectInstance = Expression.Parameter(property.DeclaringType, "x");
                    MemberExpression propertyGet = Expression.Property(objectInstance, property);
                    for (int x = 1; x < splitName.Length; ++x)
                    {
                        property = GetProperty(property.PropertyType, splitName[x]);
                        propertyGet = Expression.Property(propertyGet, property);
                    }
                    if (property.PropertyType != typeof(TDataType))
                    {
                        UnaryExpression convert = Expression.Convert(propertyGet, typeof(TDataType));
                        return Expression.Lambda<Func<TClassType, TDataType>>(convert, objectInstance);
                    }
                    return Expression.Lambda<Func<TClassType, TDataType>>(propertyGet, objectInstance);
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a lambda expression that calls a specific property's getter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <typeparam name="TDataType">Data type expecting</typeparam>
        /// <param name="property">Property</param>
        /// <returns>A lambda expression that calls a specific property's getter function</returns>
        public static Expression<Func<TClassType, TDataType>> GetPropertyGetter<TClassType, TDataType>(PropertyInfo property)
        {
            if (!IsOfType(property.PropertyType, typeof(TDataType)))
            {
                throw new ArgumentException("Property is not of the type specified");
            }

            if (!IsOfType(property.DeclaringType, typeof(TClassType)))
            {
                throw new ArgumentException("Property is not from the declaring class type specified");
            }

            if (property.DeclaringType != null)
            {
                ParameterExpression objectInstance = Expression.Parameter(property.DeclaringType, "x");
                MemberExpression propertyGet = Expression.Property(objectInstance, property);
                if (property.PropertyType != typeof(TDataType))
                {
                    UnaryExpression convert = Expression.Convert(propertyGet, typeof(TDataType));
                    return Expression.Lambda<Func<TClassType, TDataType>>(convert, objectInstance);
                }
                return Expression.Lambda<Func<TClassType, TDataType>>(propertyGet, objectInstance);
            }
            return null;
        }

        /// <summary>
        /// Gets a lambda expression that calls a specific property's getter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="propertyName">Property name</param>
        /// <returns>A lambda expression that calls a specific property's getter function</returns>
        public static Expression<Func<TClassType, object>> GetPropertyGetter<TClassType>(string propertyName)
        {
            return GetPropertyGetter<TClassType, object>(propertyName);
        }

        /// <summary>
        /// Gets a lambda expression that calls a specific property's getter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="property">Property</param>
        /// <returns>A lambda expression that calls a specific property's getter function</returns>
        public static Expression<Func<TClassType, object>> GetPropertyGetter<TClassType>(PropertyInfo property)
        {
            return GetPropertyGetter<TClassType, object>(property);
        }

        #endregion GetPropertyGetter

        #region GetPropertySetter

        /// <summary>
        /// Gets a lambda expression that calls a specific property's setter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <typeparam name="TDataType">Data type expecting</typeparam>
        /// <param name="propertyName">Property name</param>
        /// <returns>A lambda expression that calls a specific property's setter function</returns>
        public static Expression<Action<TClassType, TDataType>> GetPropertySetter<TClassType, TDataType>(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            string[] splitName = propertyName.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            PropertyInfo property = GetProperty<TClassType>(splitName[0]);
            if (property.DeclaringType != null)
            {
                ParameterExpression objectInstance = Expression.Parameter(property.DeclaringType, "x");
                ParameterExpression propertySet = Expression.Parameter(typeof(TDataType), "y");
                MethodCallExpression setterCall;
                MemberExpression propertyGet = null;
                if (splitName.Length > 1)
                {
                    propertyGet = Expression.Property(objectInstance, property);
                    for (int x = 1; x < splitName.Length - 1; ++x)
                    {
                        property = GetProperty(property.PropertyType, splitName[x]);
                        propertyGet = Expression.Property(propertyGet, property);
                    }
                    property = GetProperty(property.PropertyType, splitName[splitName.Length - 1]);
                }
                if (property.PropertyType != typeof(TDataType))
                {
                    UnaryExpression convert = Expression.Convert(propertySet, property.PropertyType);

                    // ReSharper disable PossiblyMistakenUseOfParamsMethod
                    setterCall = propertyGet == null ? Expression.Call(objectInstance, property.GetSetMethod(), convert) : Expression.Call(propertyGet, property.GetSetMethod(), convert);

                    // ReSharper restore PossiblyMistakenUseOfParamsMethod
                    return Expression.Lambda<Action<TClassType, TDataType>>(setterCall, objectInstance, propertySet);
                }

                // ReSharper disable PossiblyMistakenUseOfParamsMethod
                setterCall = propertyGet == null ? Expression.Call(objectInstance, property.GetSetMethod(), propertySet) : Expression.Call(propertyGet, property.GetSetMethod(), propertySet);

                // ReSharper restore PossiblyMistakenUseOfParamsMethod
                return Expression.Lambda<Action<TClassType, TDataType>>(setterCall, objectInstance, propertySet);
            }
            return null;
        }

        /// <summary>
        /// Gets a lambda expression that calls a specific property's setter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <typeparam name="TDataType">Data type expecting</typeparam>
        /// <returns>A lambda expression that calls a specific property's setter function</returns>
        public static Expression<Action<TClassType, TDataType>> GetPropertySetter<TClassType, TDataType>(PropertyInfo property)
        {
            if (!IsOfType(property.PropertyType, typeof(TDataType)))
            {
                throw new ArgumentException("Property is not of the type specified");
            }

            if (!IsOfType(property.DeclaringType, typeof(TClassType)))
            {
                throw new ArgumentException("Property is not from the declaring class type specified");
            }

            if (property.DeclaringType != null)
            {
                ParameterExpression objectInstance = Expression.Parameter(property.DeclaringType, "x");
                ParameterExpression propertySet = Expression.Parameter(typeof(TDataType), "y");
                MethodCallExpression setterCall;
                if (property.PropertyType != typeof(TDataType))
                {
                    UnaryExpression convert = Expression.Convert(propertySet, property.PropertyType);

                    // ReSharper disable PossiblyMistakenUseOfParamsMethod
                    setterCall = Expression.Call(objectInstance, property.GetSetMethod(), convert);

                    // ReSharper restore PossiblyMistakenUseOfParamsMethod
                    return Expression.Lambda<Action<TClassType, TDataType>>(setterCall, objectInstance, propertySet);
                }

                // ReSharper disable PossiblyMistakenUseOfParamsMethod
                setterCall = Expression.Call(objectInstance, property.GetSetMethod(), propertySet);

                // ReSharper restore PossiblyMistakenUseOfParamsMethod
                return Expression.Lambda<Action<TClassType, TDataType>>(setterCall, objectInstance, propertySet);
            }
            return null;
        }

        /// <summary>
        /// Gets a lambda expression that calls a specific property's setter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <param name="propertyName">Property name</param>
        /// <returns>A lambda expression that calls a specific property's setter function</returns>
        public static Expression<Action<TClassType, object>> GetPropertySetter<TClassType>(string propertyName)
        {
            return GetPropertySetter<TClassType, object>(propertyName);
        }

        /// <summary>
        /// Gets a lambda expression that calls a specific property's setter function
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <returns>A lambda expression that calls a specific property's setter function</returns>
        public static Expression<Action<TClassType, object>> GetPropertySetter<TClassType>(PropertyInfo property)
        {
            return GetPropertySetter<TClassType, object>(property);
        }

        #endregion GetPropertySetter

        #region GetProperty

        /// <summary>
        /// Gets a property based on a path
        /// </summary>
        /// <typeparam name="TSource">Source type</typeparam>
        /// <param name="propertyPath">Path to the property</param>
        /// <returns>The property info</returns>
        public static PropertyInfo GetProperty<TSource>(string propertyPath)
        {
            return GetProperty(typeof(TSource), propertyPath);
        }

        /// <summary>
        /// Gets a property based on a path
        /// </summary>
        /// <param name="sourceType">Source type</param>
        /// <param name="propertyPath">Path to the property</param>
        /// <returns>The property info</returns>
        public static PropertyInfo GetProperty(Type sourceType, string propertyPath)
        {
            if (string.IsNullOrEmpty(propertyPath))
            {
                return null;
            }

            string[] splitter = { "." };
            string[] sourceProperties = propertyPath.Split(splitter, StringSplitOptions.None);
            Type propertyType = sourceType;
            PropertyInfo propertyInfo = propertyType.GetProperty(sourceProperties[0]);
            propertyType = propertyInfo.PropertyType;
            for (int x = 1; x < sourceProperties.Length; ++x)
            {
                propertyInfo = propertyType.GetProperty(sourceProperties[x]);
                propertyType = propertyInfo.PropertyType;
            }
            return propertyInfo;
        }

        #endregion GetProperty

        #region GetPropertyName

        ///// <summary>
        ///// Gets the name of the property held within the expression
        ///// </summary>
        ///// <typeparam name="T">The type of object used in the expression</typeparam>
        ///// <param name="expression">The expression</param>
        ///// <returns>A string containing the name of the property</returns>
        //public static string GetPropertyName<T>(Expression<Func<T, object>> expression)
        //{
        //    if (expression == null)
        //    {
        //        return "";
        //    }

        //    string name;
        //    if (expression.Body.NodeType == ExpressionType.Convert)
        //    {
        //        name = expression.Body.ToString().Replace("Convert(", "").Replace(")", "");
        //        name = name.Remove(0, name.IndexOf(".", StringComparison.Ordinal) + 1);
        //    }
        //    else
        //    {
        //        name = expression.Body.ToString();
        //        name = name.Remove(0, name.IndexOf(".", StringComparison.Ordinal) + 1);
        //    }
        //    return name;
        //}

        /// <summary>
        /// Gets a property name
        /// </summary>
        /// <typeparam name="TClassType">Class type</typeparam>
        /// <typeparam name="TDataType">Data type of the property</typeparam>
        /// <param name="expression">LINQ expression</param>
        /// <returns>The name of the property</returns>
        public static string GetPropertyName<TClassType, TDataType>(Expression<Func<TClassType, TDataType>> expression)
        {
            if (expression.Body is UnaryExpression && expression.Body.NodeType == ExpressionType.Convert)
            {
                var temp = (MemberExpression)((UnaryExpression)expression.Body).Operand;
                return GetPropertyName(temp.Expression) + temp.Member.Name;
            }
            if (!(expression.Body is MemberExpression))
            {
                throw new ArgumentException("Expression.Body is not a MemberExpression");
            }

            return GetPropertyName(((MemberExpression)expression.Body).Expression) + ((MemberExpression)expression.Body).Member.Name;
        }

        private static string GetPropertyName(Expression expression)
        {
            if (!(expression is MemberExpression))
            {
                return "";
            }

            return GetPropertyName(((MemberExpression)expression).Expression) + ((MemberExpression)expression).Member.Name + ".";
        }

        #endregion GetPropertyName

        #region GetPropertyParent

        /// <summary>
        /// Gets a property's parent object
        /// </summary>
        /// <param name="sourceObject">Source object</param>
        /// <param name="propertyPath">
        /// Path of the property (ex: Prop1.Prop2.Prop3 would be the Prop1 of the source object,
        /// which then has a Prop2 on it, which in turn has a Prop3 on it.)
        /// </param>
        /// <param name="propertyInfo">Property info that is sent back</param>
        /// <returns>The property's parent object</returns>
        public static object GetPropertyParent(object sourceObject, string propertyPath, out PropertyInfo propertyInfo)
        {
            if (sourceObject == null)
            {
                propertyInfo = null;
                return null;
            }
            string[] splitter = { "." };
            string[] sourceProperties = propertyPath.Split(splitter, StringSplitOptions.None);
            object tempSourceProperty = sourceObject;
            Type propertyType = sourceObject.GetType();
            propertyInfo = propertyType.GetProperty(sourceProperties[0]);
            propertyType = propertyInfo.PropertyType;
            for (int x = 1; x < sourceProperties.Length; ++x)
            {
                if (tempSourceProperty != null)
                {
                    tempSourceProperty = propertyInfo.GetValue(tempSourceProperty, null);
                }
                propertyInfo = propertyType.GetProperty(sourceProperties[x]);
                propertyType = propertyInfo.PropertyType;
            }
            return tempSourceProperty;
        }

        #endregion GetPropertyParent

        #region GetPropertyValue

        /// <summary>
        /// Gets a property's value
        /// </summary>
        /// <param name="sourceObject">object who contains the property</param>
        /// <param name="propertyPath">
        /// Path of the property (ex: Prop1.Prop2.Prop3 would be the Prop1 of the source object,
        /// which then has a Prop2 on it, which in turn has a Prop3 on it.)
        /// </param>
        /// <returns>
        /// The value contained in the property or null if the property can not be reached
        /// </returns>
        public static object GetPropertyValue(object sourceObject, string propertyPath)
        {
            if (sourceObject == null || string.IsNullOrEmpty(propertyPath))
            {
                return null;
            }

            string[] splitter = { "." };
            string[] sourceProperties = propertyPath.Split(splitter, StringSplitOptions.None);
            object tempSourceProperty = sourceObject;
            Type propertyType = sourceObject.GetType();
            foreach (string t in sourceProperties)
            {
                PropertyInfo sourcePropertyInfo = propertyType.GetProperty(t);
                if (sourcePropertyInfo == null)
                {
                    return null;
                }

                tempSourceProperty = sourcePropertyInfo.GetValue(tempSourceProperty, null);
                if (tempSourceProperty == null)
                {
                    return null;
                }

                propertyType = sourcePropertyInfo.PropertyType;
            }
            return tempSourceProperty;
        }

        #endregion GetPropertyValue

        #region GetPropertyType

        /// <summary>
        /// Gets a property's type
        /// </summary>
        /// <param name="sourceObject">object who contains the property</param>
        /// <param name="propertyPath">
        /// Path of the property (ex: Prop1.Prop2.Prop3 would be the Prop1 of the source object,
        /// which then has a Prop2 on it, which in turn has a Prop3 on it.)
        /// </param>
        /// <returns>The type of the property specified or null if it can not be reached.</returns>
        public static Type GetPropertyType(object sourceObject, string propertyPath)
        {
            if (sourceObject == null || string.IsNullOrEmpty(propertyPath))
            {
                return null;
            }

            string[] splitter = { "." };
            string[] sourceProperties = propertyPath.Split(splitter, StringSplitOptions.None);
            Type propertyType = sourceObject.GetType();
            foreach (string t in sourceProperties)
            {
                PropertyInfo propertyInfo = propertyType.GetProperty(t);
                propertyType = propertyInfo.PropertyType;
            }
            return propertyType;
        }

        /// <summary>
        /// Gets a property's type
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="propertyPath">
        /// Path of the property (ex: Prop1.Prop2.Prop3 would be the Prop1 of the source object,
        /// which then has a Prop2 on it, which in turn has a Prop3 on it.)
        /// </param>
        /// <returns>The type of the property specified or null if it can not be reached.</returns>
        public static Type GetPropertyType<T>(string propertyPath)
        {
            if (string.IsNullOrEmpty(propertyPath))
            {
                return null;
            }

            Type objectType = typeof(T);
            object Object = objectType.Assembly.CreateInstance(objectType.FullName);
            return GetPropertyType(Object, propertyPath);
        }

        #endregion GetPropertyType

        #region GetTypeName

        /// <summary>
        /// Returns the type's name
        /// </summary>
        /// <param name="objectType">object type</param>
        /// <returns>string name of the type</returns>
        public static string GetTypeName(Type objectType)
        {
            if (objectType == null)
            {
                return "";
            }

            var output = new StringBuilder();
            if (objectType.Name == "Void")
            {
                output.Append("void");
            }
            else
            {
                if (objectType.Name.Contains("`"))
                {
                    Type[] genericTypes = objectType.GetGenericArguments();
                    output.Append(objectType.Name.Remove(objectType.Name.IndexOf("`", StringComparison.Ordinal)))
                        .Append("<");
                    string seperator = "";
                    foreach (Type genericType in genericTypes)
                    {
                        output.Append(seperator).Append(GetTypeName(genericType));
                        seperator = ",";
                    }
                    output.Append(">");
                }
                else
                {
                    output.Append(objectType.Name);
                }
            }
            return output.ToString();
        }

        #endregion GetTypeName

        #region GetTypes

        /// <summary>
        /// Gets a list of types based on an interface
        /// </summary>
        /// <param name="assembly">Assembly to check</param>
        /// <param name="Interface">Interface to look for (also checks base class)</param>
        /// <returns>List of types that use the interface</returns>
        public static System.Collections.Generic.List<Type> GetTypes(Assembly assembly, string Interface)
        {
            var returnList = new System.Collections.Generic.List<Type>();
            if (assembly == null)
            {
                return returnList;
            }

            Type[] types = assembly.GetTypes();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Type type in types)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (CheckIsOfInterface(type, Interface))
                {
                    returnList.Add(type);
                }
            }
            return returnList;
        }

        /// <summary>
        /// Gets a list of types based on an interface
        /// </summary>
        /// <param name="assembly">Assembly to check</param>
        /// <param name="Interface">Interface to look for (also checks base class)</param>
        /// <returns>List of types that use the interface</returns>
        public static System.Collections.Generic.List<Type> GetTypes(Assembly assembly, Type Interface)
        {
            var returnList = new System.Collections.Generic.List<Type>();
            if (assembly == null)
            {
                return returnList;
            }

            Type[] types = assembly.GetTypes();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Type type in types)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (CheckIsOfInterface(type, Interface))
                {
                    returnList.Add(type);
                }
            }
            return returnList;
        }

        /// <summary>
        /// Gets a list of types based on an interface
        /// </summary>
        /// <param name="assemblyLocation">Location of the DLL</param>
        /// <param name="Interface">Interface to look for</param>
        /// <param name="assembly">The assembly holding the types</param>
        /// <returns>A list of types that use the interface</returns>
        public static System.Collections.Generic.List<Type> GetTypes(string assemblyLocation, string Interface, out Assembly assembly)
        {
            assembly = Assembly.LoadFile(assemblyLocation);
            return GetTypes(assembly, Interface);
        }

        #endregion GetTypes

        #region GetTypesFromDirectory

        /// <summary>
        /// Gets a list of types based on an interface from all assemblies found in a directory
        /// </summary>
        /// <param name="assemblyDirectory">Directory to search in</param>
        /// <param name="Interface">The interface to look for</param>
        /// <param name="recursive">Determines whether to search recursively or not</param>
        /// <returns>A list mapping using the assembly as the key and a list of types</returns>
        public static ListMapping<Assembly, Type> GetTypesFromDirectory(string assemblyDirectory, string Interface, bool recursive = false)
        {
            var returnList = new ListMapping<Assembly, Type>();
            System.Collections.Generic.List<Assembly> assemblies = GetAssembliesFromDirectory(assemblyDirectory, recursive);
            foreach (Assembly assembly in assemblies)
            {
                Type[] types = assembly.GetTypes();
                foreach (Type type in types)
                {
                    if (type.GetInterface(Interface, true) != null)
                    {
                        returnList.Add(assembly, type);
                    }
                }
            }
            return returnList;
        }

        #endregion GetTypesFromDirectory

        #region IsIEnumerable

        /// <summary>
        /// Simple function to determine if an item is an IEnumerable
        /// </summary>
        /// <param name="objectType">Object type</param>
        /// <returns>True if it is, false otherwise</returns>
        public static bool IsIEnumerable(Type objectType)
        {
            return IsOfType(objectType, typeof(IEnumerable));
        }

        #endregion IsIEnumerable

        #region IsOfType

        /// <summary>
        /// Determines if an object is of a specific type
        /// </summary>
        /// <param name="Object">Object</param>
        /// <param name="type">Type</param>
        /// <returns>True if it is, false otherwise</returns>
        public static bool IsOfType(object Object, Type type)
        {
            if (Object == null)
            {
                throw new ArgumentNullException("Object");
            }

            return IsOfType(Object.GetType(), type);
        }

        /// <summary>
        /// Determines if an object type is of a specific type
        /// </summary>
        /// <param name="objectType">Object type</param>
        /// <param name="type">Base type</param>
        /// <returns>True if it is, false otherwise</returns>
        public static bool IsOfType(Type objectType, Type type)
        {
            if (objectType == null)
            {
                throw new ArgumentException("ObjectType");
            }

            if (type == null)
            {
                throw new ArgumentException("Type");
            }

            return CheckIsOfInterface(objectType, type);
        }

        #endregion IsOfType

        #region LoadAssembly

        /// <summary>
        /// Loads an assembly from a specific location
        /// </summary>
        /// <param name="location">Location of the assembly</param>
        /// <returns>The loaded assembly</returns>
        public static Assembly LoadAssembly(string location)
        {
            AssemblyName name = AssemblyName.GetAssemblyName(location);
            return AppDomain.CurrentDomain.Load(name);
        }

        #endregion LoadAssembly

        #region LoadAssembliesFromDirectory

        /// <summary>
        /// Loads an assembly from a specific location
        /// </summary>
        /// <param name="directory">Directory to search for assemblies</param>
        /// <param name="recursive">Search recursively</param>
        /// <returns>The loaded assemblies</returns>
        public static System.Collections.Generic.List<Assembly> LoadAssembliesFromDirectory(string directory, bool recursive = false)
        {
            var returnValues = new System.Collections.Generic.List<Assembly>();
            var files = new DirectoryInfo(directory).GetFiles("*.dll", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).ToList();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (FileInfo file in files)

            // ReSharper restore LoopCanBeConvertedToQuery
            {
                returnValues.Add(LoadAssembly(file.FullName));
            }
            return returnValues;
        }

        #endregion LoadAssembliesFromDirectory

        #region MakeShallowCopy

        /// <summary>
        /// Makes a shallow copy of the object
        /// </summary>
        /// <param name="Object">Object to copy</param>
        /// <param name="simpleTypesOnly">
        /// If true, it only copies simple types (no classes, only items like int, string, etc.),
        /// false copies everything.
        /// </param>
        /// <returns>A copy of the object</returns>
        public static object MakeShallowCopy(object Object, bool simpleTypesOnly)
        {
            if (Object == null)
            {
                return null;
            }

            Type objectType = Object.GetType();
            PropertyInfo[] properties = objectType.GetProperties();
            FieldInfo[] fields = objectType.GetFields();
            object classInstance = Activator.CreateInstance(objectType);

            foreach (PropertyInfo property in properties)
            {
                try
                {
                    if (property.GetGetMethod() != null && property.GetSetMethod() != null)
                    {
                        if (simpleTypesOnly)
                        {
                            SetPropertyifSimpleType(property, classInstance, Object);
                        }
                        else
                        {
                            SetProperty(property, classInstance, Object);
                        }
                    }
                }

                // ReSharper disable EmptyGeneralCatchClause
                catch { }

                // ReSharper restore EmptyGeneralCatchClause
            }

            foreach (FieldInfo field in fields)
            {
                try
                {
                    if (simpleTypesOnly)
                    {
                        SetFieldifSimpleType(field, classInstance, Object);
                    }
                    else
                    {
                        SetField(field, classInstance, Object);
                    }
                }

                // ReSharper disable EmptyGeneralCatchClause
                catch { }

                // ReSharper restore EmptyGeneralCatchClause
            }

            return classInstance;
        }

        #endregion MakeShallowCopy

        #region MakeShallowCopyInheritedClass

        /// <summary>
        /// Makes a shallow copy of the object to a different class type (inherits from the
        /// original)
        /// </summary>
        /// <param name="derivedType">Derived type</param>
        /// <param name="Object">Object to copy</param>
        /// <param name="simpleTypesOnly">
        /// If true, it only copies simple types (no classes, only items like int, string, etc.),
        /// false copies everything.
        /// </param>
        /// <returns>A copy of the object</returns>
        public static object MakeShallowCopyInheritedClass(Type derivedType, object Object, bool simpleTypesOnly)
        {
            if (derivedType == null)
            {
                return null;
            }

            if (Object == null)
            {
                return null;
            }

            Type objectType = Object.GetType();
            Type returnedObjectType = derivedType;
            PropertyInfo[] properties = objectType.GetProperties();
            FieldInfo[] fields = objectType.GetFields();
            object classInstance = Activator.CreateInstance(returnedObjectType);

            foreach (PropertyInfo property in properties)
            {
                try
                {
                    PropertyInfo childProperty = returnedObjectType.GetProperty(property.Name);
                    if (childProperty != null)
                    {
                        if (simpleTypesOnly)
                        {
                            SetPropertyifSimpleType(childProperty, property, classInstance, Object);
                        }
                        else
                        {
                            SetProperty(childProperty, property, classInstance, Object);
                        }
                    }
                }

                // ReSharper disable EmptyGeneralCatchClause
                catch { }

                // ReSharper restore EmptyGeneralCatchClause
            }

            foreach (FieldInfo field in fields)
            {
                try
                {
                    FieldInfo childField = returnedObjectType.GetField(field.Name);
                    if (childField != null)
                    {
                        if (simpleTypesOnly)
                        {
                            SetFieldifSimpleType(childField, field, classInstance, Object);
                        }
                        else
                        {
                            SetField(childField, field, classInstance, Object);
                        }
                    }
                }

                // ReSharper disable EmptyGeneralCatchClause
                catch { }

                // ReSharper restore EmptyGeneralCatchClause
            }

            return classInstance;
        }

        #endregion MakeShallowCopyInheritedClass

        #region SetValue

        /// <summary>
        /// Sets the value of destination property
        /// </summary>
        /// <param name="sourceValue">The source value</param>
        /// <param name="destinationObject">The destination object</param>
        /// <param name="destinationPropertyInfo">The destination property info</param>
        /// <param name="format">Allows for formatting if the destination is a string</param>
        public static void SetValue(object sourceValue, object destinationObject,
            PropertyInfo destinationPropertyInfo, string format)
        {
            if (destinationObject == null || destinationPropertyInfo == null)
            {
                return;
            }

            Type destinationPropertyType = destinationPropertyInfo.PropertyType;
            destinationPropertyInfo.SetValue(destinationObject,
                Parse(sourceValue, destinationPropertyType, format),
                null);
        }

        /// <summary>
        /// Sets the value of destination property
        /// </summary>
        /// <param name="sourceValue">The source value</param>
        /// <param name="destinationObject">The destination object</param>
        /// <param name="propertyPath">
        /// The path to the property (ex: MyProp.SubProp.FinalProp would look at the MyProp on the
        /// destination object, then find it's SubProp, and finally copy the SourceValue to the
        /// FinalProp property on the destination object)
        /// </param>
        /// <param name="format">Allows for formatting if the destination is a string</param>
        public static void SetValue(object sourceValue, object destinationObject,
            string propertyPath, string format)
        {
            string[] splitter = { "." };
            string[] destinationProperties = propertyPath.Split(splitter, StringSplitOptions.None);
            object tempDestinationProperty = destinationObject;
            Type destinationPropertyType = destinationObject.GetType();
            PropertyInfo destinationProperty;
            for (int x = 0; x < destinationProperties.Length - 1; ++x)
            {
                destinationProperty = destinationPropertyType.GetProperty(destinationProperties[x]);
                destinationPropertyType = destinationProperty.PropertyType;
                tempDestinationProperty = destinationProperty.GetValue(tempDestinationProperty, null);
                if (tempDestinationProperty == null)
                {
                    return;
                }
            }
            destinationProperty = destinationPropertyType.GetProperty(destinationProperties[destinationProperties.Length - 1]);
            SetValue(sourceValue, tempDestinationProperty, destinationProperty, format);
        }

        #endregion SetValue

        #region GetPropertyName

        /// <summary>
        /// GetClassAndPropertyName
        /// </summary>
        /// <param name="propertyLambda"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GetPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            return GetPropertyName(propertyLambda, out Type type, out PropertyInfo propertyInfo);
        }

        /// <summary>
        /// GetClassAndPropertyName
        /// </summary>
        /// <param name="propertyLambda"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GetPropertyName<T>(Expression<Func<T, object>> propertyLambda)
        {
            return GetPropertyName(propertyLambda, out Type type, out PropertyInfo propertyInfo);
        }

        /// <summary>
        /// GetPropertyName
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyLambda"></param>
        /// <param name="type"></param>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static string GetPropertyName<T>(
            Expression<Func<T>> propertyLambda,
            out Type type,
            out PropertyInfo propertyInfo)
        {
            dynamic me;
            propertyInfo = null;

            if (propertyLambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                me = propertyLambda.Body as MemberExpression;

                if (me == null)
                {
                    throw new ArgumentException(
                        "You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
                }

                dynamic classLam = me.Expression;

                type = null;

                if (me.Member != null)
                {
                    if (me.Member.PropertyType != null)
                    {
                        type = classLam.Type;

                        propertyInfo = me.Member as PropertyInfo;

                        if (propertyInfo != null)
                        {
                            return propertyInfo.Name;
                        }

                        throw new ArgumentException("PropertyInfo is null'");
                    }
                }
            }

            if (propertyLambda.Body.NodeType == ExpressionType.Convert)
            {
                if (propertyLambda.Body is UnaryExpression cov)
                {
                    me = cov.Operand as MemberExpression;

                    if (me == null)
                    {
                        throw new ArgumentException(
                            "You must pass a lambda of the form: ' Class=> Class.Property' or 'object => object.Property'");
                    }

                    dynamic classLam = me.Expression;

                    type = null;

                    if (me.Member != null)
                    {
                        if (me.Member.PropertyType != null)
                        {
                            type = classLam.Type;

                            propertyInfo = me.Member as PropertyInfo;

                            if (propertyInfo != null)
                            {
                                return propertyInfo.Name;
                            }

                            throw new ArgumentException("PropertyInfo is null'");
                        }
                    }
                }
            }

            throw new ArgumentException("Cannot analyze type get name ");
        }

        /// <summary>
        /// GetPropertyName
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyLambda"></param>
        /// <param name="type"></param>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static string GetPropertyName<T>(
            Expression<Func<T,object>> propertyLambda,
            out Type type,
            out PropertyInfo propertyInfo)
        {
            dynamic me;
            propertyInfo = null;

            if (propertyLambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                me = propertyLambda.Body as MemberExpression;

                if (me == null)
                {
                    throw new ArgumentException(
                        "You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
                }

                dynamic classLam = me.Expression;

                type = null;

                if (me.Member != null)
                {
                    if (me.Member.PropertyType != null)
                    {
                        type = classLam.Type;

                        propertyInfo = me.Member as PropertyInfo;

                        if (propertyInfo != null)
                        {
                            return propertyInfo.Name;
                        }

                        throw new ArgumentException("PropertyInfo is null'");
                    }
                }
            }

            if (propertyLambda.Body.NodeType == ExpressionType.Convert)
            {
                if (propertyLambda.Body is UnaryExpression cov)
                {
                    me = cov.Operand as MemberExpression;

                    if (me == null)
                    {
                        throw new ArgumentException(
                            "You must pass a lambda of the form: ' Class=> Class.Property' or 'object => object.Property'");
                    }

                    dynamic classLam = me.Expression;

                    type = null;

                    if (me.Member != null)
                    {
                        if (me.Member.PropertyType != null)
                        {
                            type = classLam.Type;

                            propertyInfo = me.Member as PropertyInfo;

                            if (propertyInfo != null)
                            {
                                return propertyInfo.Name;
                            }

                            throw new ArgumentException("PropertyInfo is null'");
                        }
                    }
                }
            }

            throw new ArgumentException("Cannot analyze type get name ");
        }

        #endregion GetPropertyName

        #region GetPropertyName

        /// <summary>
        /// GetClassName
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string GetClassName<T>(T o)
        {
            return o.GetType().Name;
        }

        #endregion GetPropertyName

        #region GetClassAndPropertyName

        ///// <summary>
        ///// GetPropertyName
        ///// </summary>
        ///// <typeparam name="T1"></typeparam>
        ///// <typeparam name="T2"></typeparam>
        ///// <param name="o"></param>
        ///// <param name="propertyLambda"></param>
        ///// <returns></returns>
        //public static string GetClassAndPropertyName<T1,T2>(T1 o, Expression<Func<T2>> propertyLambda)
        //{
        //    var className = GetClassName(o);
        //    var propertyName = GetPropertyName(propertyLambda);

        //    return className + "." + propertyName;
        //}

        /// <summary>
        /// GetClassAndPropertyName
        /// </summary>
        /// <param name="propertyLambda"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GetClassAndPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            return GetClassAndPropertyName(propertyLambda, out Type type, out PropertyInfo propertyInfo);
        }

        /// <summary>
        /// GetClassAndPropertyName
        /// </summary>
        /// <param name="propertyLambda"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GetClassAndPropertyName<T>(Expression<Func<T, object>> propertyLambda)
        {
            return GetClassAndPropertyName(propertyLambda, out Type type, out PropertyInfo propertyInfo);
        }

        /// <summary>
        /// GetClassAndPropertyName
        /// </summary>
        /// <param name="propertyLambda"></param>
        /// <param name="type"></param>
        /// <param name="propertyInfo"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GetClassAndPropertyName<T>(
            Expression<Func<T>> propertyLambda,
            out Type type,
            out PropertyInfo propertyInfo)
        {

            dynamic me;
            propertyInfo = null;

            if (propertyLambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                me = propertyLambda.Body as MemberExpression;

                if (me == null)
                {
                    throw new ArgumentException(
                        "You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
                }

                dynamic classLam = me.Expression;

                type = null;

                if (me.Member != null)
                {
                    if (me.Member.PropertyType != null)
                    {
                        type = classLam.Type;

                        propertyInfo = me.Member as PropertyInfo;

                        if (propertyInfo != null)
                        {
                            return type.Name + "." + propertyInfo.Name;
                        }

                        throw new ArgumentException("PropertyInfo is null'");
                    }
                }
            }

            if (propertyLambda.Body.NodeType == ExpressionType.Convert)
            {
                if (propertyLambda.Body is UnaryExpression cov)
                {
                    me = cov.Operand as MemberExpression;

                    if (me == null)
                    {
                        throw new ArgumentException(
                            "You must pass a lambda of the form: ' Class=> Class.Property' or 'object => object.Property'");
                    }

                    dynamic classLam = me.Expression;

                    type = null;

                    if (me.Member != null)
                    {
                        if (me.Member.PropertyType != null)
                        {
                            type = classLam.Type;

                            propertyInfo = me.Member as PropertyInfo;

                            if (propertyInfo != null)
                            {
                                return type.Name + "." + propertyInfo.Name;
                            }

                            throw new ArgumentException("PropertyInfo is null'");
                        }
                    }
                }
            }

            throw new ArgumentException("Cannot analyze type get name ");
        }

        /// <summary>
        /// GetClassAndPropertyName
        /// </summary>
        /// <param name="propertyLambda"></param>
        /// <param name="type"></param>
        /// <param name="propertyInfo"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GetClassAndPropertyName<T>(
            Expression<Func<T, object>> propertyLambda,
            out Type type,
            out PropertyInfo propertyInfo)
        {
            dynamic me;
            propertyInfo = null;

            if (propertyLambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                me = propertyLambda.Body as MemberExpression;

                if (me == null)
                {
                    throw new ArgumentException(
                        "You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
                }

                dynamic classLam = me.Expression;

                type = null;

                if (me.Member != null)
                {
                    if (me.Member.PropertyType != null)
                    {
                        type = classLam.Type;

                        propertyInfo = me.Member as PropertyInfo;

                        if (propertyInfo != null)
                        {
                            return type.Name + "." + propertyInfo.Name;
                        }

                        throw new ArgumentException("PropertyInfo is null'");
                    }
                }
            }

            if (propertyLambda.Body.NodeType == ExpressionType.Convert)
            {
                if (propertyLambda.Body is UnaryExpression cov)
                {
                    me = cov.Operand as MemberExpression;

                    if (me == null)
                    {
                        throw new ArgumentException(
                            "You must pass a lambda of the form: ' Class=> Class.Property' or 'object => object.Property'");
                    }

                    dynamic classLam = me.Expression;

                    type = null;

                    if (me.Member != null)
                    {
                        if (me.Member.PropertyType != null)
                        {
                            type = classLam.Type;

                            propertyInfo = me.Member as PropertyInfo;

                            if (propertyInfo != null)
                            {
                                return type.Name + "." + propertyInfo.Name;
                            }

                            throw new ArgumentException("PropertyInfo is null'");
                        }
                    }
                }
            }

            throw new ArgumentException("Cannot analyze type get name ");
        }

        #endregion GetClassAndPropertyName

        #endregion Public Static Functions

        #region Private Static Functions

        #region CheckIsOfInterface

        /// <summary>
        /// Checks if the type is of a specific interface type
        /// </summary>
        /// <param name="type">Type to check</param>
        /// <param name="Interface">Interface to check against</param>
        /// <returns>True if it is, false otherwise</returns>
        private static bool CheckIsOfInterface(Type type, Type Interface)
        {
            if (type == null)
            {
                return false;
            }

            if (type == Interface || type.GetInterface(Interface.Name, true) != null)
            {
                return true;
            }

            return CheckIsOfInterface(type.BaseType, Interface);
        }

        /// <summary>
        /// Checks if the type is of a specific interface type
        /// </summary>
        /// <param name="type">Type to check</param>
        /// <param name="Interface">Name of the interface to check against</param>
        /// <returns>True if it is, false otherwise</returns>
        private static bool CheckIsOfInterface(Type type, string Interface)
        {
            if (type == null)
            {
                return false;
            }

            if (type.GetInterface(Interface, true) != null)
            {
                return true;
            }

            if (!string.IsNullOrEmpty(type.FullName) && type.FullName.Contains(Interface))
            {
                return true;
            }

            return CheckIsOfInterface(type.BaseType, Interface);
        }

        #endregion CheckIsOfInterface

        #region DiscoverFormatString

        /// <summary>
        /// Used to find the format string to use
        /// </summary>
        /// <param name="inputType">Input type</param>
        /// <param name="outputType">Output type</param>
        /// <param name="formatString">the string format</param>
        /// <returns>The format string to use</returns>
        private static string DiscoverFormatString(Type inputType,
            Type outputType, string formatString)
        {
            if (inputType == outputType
                || inputType == typeof(string)
                || outputType == typeof(string))
            {
                return formatString;
            }

            if (inputType == typeof(float)
                || inputType == typeof(double)
                || inputType == typeof(decimal)
                || outputType == typeof(float)
                || outputType == typeof(double)
                || outputType == typeof(decimal))
            {
                return "f0";
            }

            return formatString;
        }

        #endregion DiscoverFormatString

        #region Parse

        /// <summary>
        /// Parses the object and turns it into the requested output type
        /// </summary>
        /// <param name="input">Input object</param>
        /// <param name="outputType">Output type</param>
        /// <returns>An object with the requested output type</returns>
        internal static object Parse(object input, Type outputType)
        {
            return Parse(input, outputType, "");
        }

        /// <summary>
        /// Parses the string into the requested output type
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="outputType">Output type</param>
        /// <returns>An object with the requested output type</returns>
        // ReSharper disable UnusedMember.Local
        private static object Parse(string input, Type outputType)

        // ReSharper restore UnusedMember.Local
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            return Parse(input, outputType, "");
        }

        /// <summary>
        /// Parses the object into the requested output type
        /// </summary>
        /// <param name="input">Input object</param>
        /// <param name="outputType">Output object</param>
        /// <param name="format">
        /// format string (may be overridded if the conversion involves a floating point value to
        /// "f0")
        /// </param>
        /// <returns>The object converted to the specified output type</returns>
        private static object Parse(object input, Type outputType, string format)
        {
            if (input == null || outputType == null)
            {
                return null;
            }

            Type inputType = input.GetType();
            Type baseType = inputType;
            while (baseType != outputType)
            {
                baseType = baseType.BaseType;
                if (baseType == null)
                {
                    break;
                }
            }
            if (baseType == outputType)
            {
                return input;
            }
            if (inputType == outputType)
            {
                return input;
            }
            if (outputType == typeof(string))
            {
                return input.FormatToString(format);
            }
            return CallMethod("Parse", outputType.Assembly.CreateInstance(outputType.FullName), input.FormatToString(DiscoverFormatString(inputType, outputType, format)));
        }

        #endregion Parse

        #region SetField

        /// <summary>
        /// Copies a field value
        /// </summary>
        /// <param name="field">Field object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetField(FieldInfo field, object classInstance, object Object)
        {
            try
            {
                SetField(field, field, classInstance, Object);
            }

            // ReSharper disable EmptyGeneralCatchClause
            catch { }

            // ReSharper restore EmptyGeneralCatchClause
        }

        /// <summary>
        /// Copies a field value
        /// </summary>
        /// <param name="childField">Child field object</param>
        /// <param name="field">Field object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetField(FieldInfo childField, FieldInfo field, object classInstance, object Object)
        {
            try
            {
                if (field.IsPublic && childField.IsPublic)
                {
                    childField.SetValue(classInstance, field.GetValue(Object));
                }
            }

            // ReSharper disable EmptyGeneralCatchClause
            catch { }

            // ReSharper restore EmptyGeneralCatchClause
        }

        #endregion SetField

        #region SetFieldIfSimpleType

        /// <summary>
        /// Copies a field value
        /// </summary>
        /// <param name="field">Field object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetFieldifSimpleType(FieldInfo field, object classInstance, object Object)
        {
            try
            {
                SetFieldifSimpleType(field, field, classInstance, Object);
            }

            // ReSharper disable EmptyGeneralCatchClause
            catch { }

            // ReSharper restore EmptyGeneralCatchClause
        }

        /// <summary>
        /// Copies a field value
        /// </summary>
        /// <param name="childField">Child field object</param>
        /// <param name="field">Field object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetFieldifSimpleType(FieldInfo childField, FieldInfo field, object classInstance, object Object)
        {
            Type fieldType = field.FieldType;
            if (field.FieldType.FullName.StartsWith("System.Collections.Generic.List", StringComparison.CurrentCultureIgnoreCase))
            {
                fieldType = field.FieldType.GetGenericArguments()[0];
            }

            if (fieldType.FullName.StartsWith("System"))
            {
                SetField(childField, field, classInstance, Object);
            }
        }

        #endregion SetFieldIfSimpleType

        #region SetProperty

        /// <summary>
        /// Copies a property value
        /// </summary>
        /// <param name="property">Property object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetProperty(PropertyInfo property, object classInstance, object Object)
        {
            try
            {
                if (property.GetGetMethod() != null && property.GetSetMethod() != null)
                {
                    SetProperty(property, property, classInstance, Object);
                }
            }

            // ReSharper disable EmptyGeneralCatchClause
            catch { }

            // ReSharper restore EmptyGeneralCatchClause
        }

        /// <summary>
        /// Copies a property value
        /// </summary>
        /// <param name="childProperty">Child property object</param>
        /// <param name="property">Property object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetProperty(PropertyInfo childProperty, PropertyInfo property, object classInstance, object Object)
        {
            try
            {
                if (childProperty.GetSetMethod() != null && property.GetGetMethod() != null)
                {
                    childProperty.SetValue(classInstance, property.GetValue(Object, null), null);
                }
            }

            // ReSharper disable EmptyGeneralCatchClause
            catch { }

            // ReSharper restore EmptyGeneralCatchClause
        }

        #endregion SetProperty

        #region SetPropertyIfSimpleType

        /// <summary>
        /// Copies a property value
        /// </summary>
        /// <param name="property">Property object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetPropertyifSimpleType(PropertyInfo property, object classInstance, object Object)
        {
            try
            {
                if (property.GetGetMethod() != null && property.GetSetMethod() != null)
                {
                    SetPropertyifSimpleType(property, property, classInstance, Object);
                }
            }

            // ReSharper disable EmptyGeneralCatchClause
            catch { }

            // ReSharper restore EmptyGeneralCatchClause
        }

        /// <summary>
        /// Copies a property value
        /// </summary>
        /// <param name="childProperty">Child property object</param>
        /// <param name="property">Property object</param>
        /// <param name="classInstance">Class to copy to</param>
        /// <param name="Object">Class to copy from</param>
        private static void SetPropertyifSimpleType(PropertyInfo childProperty, PropertyInfo property, object classInstance, object Object)
        {
            Type propertyType = property.PropertyType;
            if (property.PropertyType.FullName.StartsWith("System.Collections.Generic.List", StringComparison.CurrentCultureIgnoreCase))
            {
                propertyType = property.PropertyType.GetGenericArguments()[0];
            }

            if (propertyType.FullName.StartsWith("System"))
            {
                SetProperty(childProperty, property, classInstance, Object);
            }
        }

        #endregion SetPropertyIfSimpleType

        #endregion Private Static Functions
    }
}