﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Linq;
using System.Reflection;
using System.Text;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;
using MethodBase = Operate.Reflection.Emit.BaseClasses.MethodBase;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Helper class for defining a method within a type
    /// </summary>
    public class MethodBuilder : MethodBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="typeBuilder">Type builder</param>
        /// <param name="name">Name of the method</param>
        /// <param name="attributes">Attributes for the method (public, private, etc.)</param>
        /// <param name="parameters">Parameter types for the method</param>
        /// <param name="returnType">Return type for the method</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public MethodBuilder(TypeBuilder typeBuilder, string name,
            MethodAttributes attributes, IEnumerable<Type> parameters, Type returnType)
        {
            if (typeBuilder.IsNull()) { throw new ArgumentNullException("typeBuilder"); }
            if (name.IsNullOrEmpty()) { throw new ArgumentNullException("name"); }
            Name = name;
            Type = typeBuilder;
            Attributes = attributes;
            ReturnType = returnType ?? typeof(void);
            Parameters = new List<ParameterBuilder> { new ParameterBuilder(null, 0) };
            var parameterTypes = parameters as Type[] ?? parameters.ToArray();
            if (parameters != null)
            {
                int x = 1;
                if (name.StartsWith("set_", StringComparison.InvariantCulture))
                    x = -1;
                foreach (Type parameterType in parameterTypes)
                {
                    Parameters.Add(new ParameterBuilder(parameterType, x));
                    ++x;
                }
            }
            Builder = Type.Builder.DefineMethod(name, attributes, returnType,
                (parameters != null && parameterTypes.Any()) ? parameterTypes.ToArray() : System.Type.EmptyTypes);
            Generator = Builder.GetILGenerator();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Method builder
        /// </summary>
        public System.Reflection.Emit.MethodBuilder Builder { get; protected set; }

        /// <summary>
        /// Type builder
        /// </summary>
        private TypeBuilder Type { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Outputs the method to a string
        /// </summary>
        /// <returns>The string representation of the method</returns>
        public override string ToString()
        {
            var output = new StringBuilder();

            output.Append("\n");
            if ((Attributes & MethodAttributes.Public) > 0)
                output.Append("public ");
            else if ((Attributes & MethodAttributes.Private) > 0)
                output.Append("private ");
            if ((Attributes & MethodAttributes.Static) > 0)
                output.Append("static ");
            if ((Attributes & MethodAttributes.Abstract) > 0)
                output.Append("abstract ");
            else if ((Attributes & MethodAttributes.HideBySig) > 0)
                output.Append("override ");
            else if ((Attributes & MethodAttributes.Virtual) > 0)
                output.Append("virtual ");
            output.Append(ReturnType.GetName());
            output.Append(" ").Append(Name).Append("(");

            string splitter = "";
            if (Parameters != null)
            {
                foreach (ParameterBuilder parameter in Parameters)
                {
                    if (parameter.Number != 0)
                    {
                        output.Append(splitter).Append(parameter.GetDefinition());
                        splitter = ",";
                    }
                }
            }
            output.Append(")");
            output.Append("\n{\n");
            Commands.ForEach(x => output.Append(x.ToString()));
            output.Append("}\n\n");

            return output.ToString();
        }

        #endregion
    }
}