﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Reflection.Emit;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Helper class for defining a local variable
    /// </summary>
    public class LocalBuilder : VariableBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="methodBuilder">Method builder</param>
        /// <param name="name">Name of the local</param>
        /// <param name="localType">Type of the local</param>
        public LocalBuilder(IMethodBuilder methodBuilder, string name, Type localType)
        {
            if (methodBuilder.IsNull()) { throw new ArgumentNullException("methodBuilder"); }
            if (name.IsNullOrEmpty()) { throw new ArgumentNullException("name"); }
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Name = name;
            MethodBuilder = methodBuilder;
            DataType = localType;
            Builder = methodBuilder.Generator.DeclareLocal(localType);
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Functions

        /// <summary>
        /// Loads the local object
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Load(ILGenerator generator)
        {
            generator.Emit(OpCodes.Ldloc, Builder);
        }

        /// <summary>
        /// Saves the local object
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Save(ILGenerator generator)
        {
            generator.Emit(OpCodes.Stloc, Builder);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Local builder
        /// </summary>
        public System.Reflection.Emit.LocalBuilder Builder { get; protected set; }

        /// <summary>
        /// Method builder
        /// </summary>
        // ReSharper disable UnusedAutoPropertyAccessor.Local
        private IMethodBuilder MethodBuilder { get; set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Local

        #endregion

        #region Overridden Functions

        /// <summary>
        /// The local item as a string
        /// </summary>
        /// <returns>The local item as a string</returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Operator Functions

        /// <summary>
        /// Increments the local object by one
        /// </summary>
        /// <param name="left">Local object to increment</param>
        /// <returns>The local object</returns>
        public static LocalBuilder operator ++(LocalBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Add(left, 1));
            return left;
        }

        /// <summary>
        /// Decrements the local object by one
        /// </summary>
        /// <param name="left">Local object to decrement</param>
        /// <returns>The local object</returns>
        public static LocalBuilder operator --(LocalBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Subtract(left, 1));
            return left;
        }

        #endregion
    }
}