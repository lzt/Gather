﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Linq;
using System.Reflection;
using System.Text;
using Operate.Reflection.Emit.Interfaces;
using MethodBase = Operate.Reflection.Emit.BaseClasses.MethodBase;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Helper class for defining/creating constructors
    /// </summary>
    public class ConstructorBuilder : MethodBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="typeBuilder">Type builder</param>
        /// <param name="attributes">Attributes for the constructor (public, private, etc.)</param>
        /// <param name="parameters">Parameter types for the constructor</param>
        /// <param name="callingConventions">Calling convention for the constructor</param>
        public ConstructorBuilder(TypeBuilder typeBuilder, MethodAttributes attributes,
            IEnumerable<Type> parameters, CallingConventions callingConventions)
        {
            if (typeBuilder.IsNull()) { throw new ArgumentNullException("typeBuilder"); }
            Type = typeBuilder;
            Attributes = attributes;
            Parameters = new List<ParameterBuilder> { new ParameterBuilder(null, 0) };
            var parameterTypes = parameters as Type[] ?? parameters.ToArray();
            if (parameters != null)
            {
                int x = 1;
                foreach (Type parameterType in parameterTypes)
                {
                    Parameters.Add(new ParameterBuilder(parameterType, x));
                    ++x;
                }
            }
            CallingConventions = callingConventions;
            Builder = Type.Builder.DefineConstructor(attributes, callingConventions,
                (parameters != null && parameterTypes.Any()) ? parameterTypes.ToArray() : System.Type.EmptyTypes);
            Generator = Builder.GetILGenerator();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Calling conventions for the constructor
        /// </summary>
        public CallingConventions CallingConventions { get; protected set; }

        /// <summary>
        /// Constructor builder
        /// </summary>
        public System.Reflection.Emit.ConstructorBuilder Builder { get; protected set; }

        /// <summary>
        /// Type builder
        /// </summary>
        private TypeBuilder Type { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// The definition of the constructor as a string
        /// </summary>
        /// <returns>The constructor as a string</returns>
        public override string ToString()
        {
            var output = new StringBuilder();

            output.Append("\n");
            if ((Attributes & MethodAttributes.Public) > 0)
                output.Append("public ");
            else if ((Attributes & MethodAttributes.Private) > 0)
                output.Append("private ");
            if ((Attributes & MethodAttributes.Static) > 0)
                output.Append("static ");
            if ((Attributes & MethodAttributes.Virtual) > 0)
                output.Append("virtual ");
            else if ((Attributes & MethodAttributes.Abstract) > 0)
                output.Append("abstract ");
            else if ((Attributes & MethodAttributes.HideBySig) > 0)
                output.Append("override ");

            string[] splitter = { "." };
            string[] nameParts = Type.Name.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            output.Append(nameParts[nameParts.Length - 1]).Append("(");

            string seperator = "";
            if (Parameters != null)
            {
                foreach (ParameterBuilder parameter in Parameters)
                {
                    if (parameter.Number != 0)
                    {
                        output.Append(seperator).Append(parameter.GetDefinition());
                        seperator = ",";
                    }
                }
            }
            output.Append(")");
            output.Append("\n{\n");
            foreach (ICommand command in Commands)
            {
                output.Append(command);
            }
            output.Append("}\n\n");

            return output.ToString();
        }

        #endregion
    }
}