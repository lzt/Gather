﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Reflection;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.Interfaces;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.Reflection.Emit.BaseClasses
{
    /// <summary>
    /// Variable base class
    /// </summary>
    public abstract class VariableBase
    {
        #region Constructor

        #endregion

        #region Properties

        /// <summary>
        /// Variable name
        /// </summary>
        public virtual string Name { get; protected set; }

        /// <summary>
        /// Variable data type
        /// </summary>
        public virtual Type DataType { get; protected set; }

        #endregion

        #region Functions

        /// <summary>
        /// Assigns the value to the variable
        /// </summary>
        /// <param name="value">Value to assign</param>
        public virtual void Assign(object value)
        {
            MethodBase.CurrentMethod.Assign(this, value);
        }

        /// <summary>
        /// Loads the variable onto the stack
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public abstract void Load(System.Reflection.Emit.ILGenerator generator);

        /// <summary>
        /// Saves the top item from the stack onto the variable
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public abstract void Save(System.Reflection.Emit.ILGenerator generator);

        /// <summary>
        /// Gets the definition of the variable
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public virtual string GetDefinition()
        {
            return DataType.GetName() + " " + Name;
        }

        /// <summary>
        /// Calls a method on this variable
        /// </summary>
        /// <param name="methodName">Method name</param>
        /// <param name="parameters">Parameters sent in</param>
        /// <returns>Variable returned by the function (if one exists, null otherwise)</returns>
        public virtual VariableBase Call(string methodName, object[] parameters = null)
        {
            if (methodName.IsNullOrEmpty()) { throw new ArgumentNullException("methodName"); }
            var parameterTypes = new List<Type>();
            if (parameters != null)
            {
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (object parameter in parameters)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    var tempParameter = parameter as VariableBase;
                    parameterTypes.Add(tempParameter != null ? tempParameter.DataType : parameter.GetType());
                }
            }
            return Call(DataType.GetMethod(methodName, parameterTypes.ToArray()), parameters);
        }

        /// <summary>
        /// Calls a method on this variable
        /// </summary>
        /// <param name="method">Method</param>
        /// <param name="parameters">Parameters sent in</param>
        /// <returns>Variable returned by the function (if one exists, null otherwise)</returns>
        public virtual VariableBase Call(MethodBuilder method, object[] parameters = null)
        {
                       if (method.IsNull()) { throw new ArgumentNullException("method"); }
            return Call(method.Builder, parameters);
        }

        /// <summary>
        /// Calls a method on this variable
        /// </summary>
        /// <param name="method">Method</param>
        /// <param name="parameters">Parameters sent in</param>
        /// <returns>Variable returned by the function (if one exists, null otherwise)</returns>
        public virtual VariableBase Call(System.Reflection.Emit.MethodBuilder method, object[] parameters = null)
        {
                       if (method.IsNull()) { throw new ArgumentNullException("method"); }
            return MethodBase.CurrentMethod.Call(this, method, parameters);
        }

        /// <summary>
        /// Calls a method on this variable
        /// </summary>
        /// <param name="method">Method</param>
        /// <param name="parameters">Parameters sent in</param>
        /// <returns>Variable returned by the function (if one exists, null otherwise)</returns>
        public virtual VariableBase Call(MethodInfo method, object[] parameters = null)
        {
                       if (method.IsNull()) { throw new ArgumentNullException("method"); }
            return MethodBase.CurrentMethod.Call(this, method, parameters);
        }

        /// <summary>
        /// Calls a method on this variable
        /// </summary>
        /// <param name="method">Method</param>
        /// <param name="parameters">Parameters sent in</param>
        /// <returns>Variable returned by the function (if one exists, null otherwise)</returns>
        public virtual void Call(ConstructorInfo method, object[] parameters = null)
        {
                       if (method.IsNull()) { throw new ArgumentNullException("method"); }
            MethodBase.CurrentMethod.Call(this, method, parameters);
        }

        /// <summary>
        /// Calls a method on this variable
        /// </summary>
        /// <param name="method">Method</param>
        /// <param name="parameters">Parameters sent in</param>
        /// <returns>Variable returned by the function (if one exists, null otherwise)</returns>
        public virtual VariableBase Call(IMethodBuilder method, object[] parameters = null)
        {
            if (method.IsNull()) { throw new ArgumentNullException("method"); }
            return Call((MethodBuilder)method, parameters);
        }

        #endregion

        #region Operator Functions

        /// <summary>
        /// Addition operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1013:OverloadOperatorEqualsOnOverloadingAddAndSubtract")]
        public static VariableBase operator +(VariableBase left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Add(left, right);
        }

        /// <summary>
        /// Subtraction operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1013:OverloadOperatorEqualsOnOverloadingAddAndSubtract")]
        public static VariableBase operator -(VariableBase left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Subtract(left, right);
        }

        /// <summary>
        /// Multiplication operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator *(VariableBase left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Multiply(left, right);
        }

        /// <summary>
        /// Division operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator /(VariableBase left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Divide(left, right);
        }

        /// <summary>
        /// Modulo operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator %(VariableBase left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Modulo(left, right);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator +(VariableBase left, object right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Add(left, right);
        }

        /// <summary>
        /// Subtraction operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator -(VariableBase left, object right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Subtract(left, right);
        }

        /// <summary>
        /// Multiplication operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator *(VariableBase left, object right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Multiply(left, right);
        }

        /// <summary>
        /// Division operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator /(VariableBase left, object right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Divide(left, right);
        }

        /// <summary>
        /// Modulo operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator %(VariableBase left, object right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Modulo(left, right);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator +(object left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Add(left, right);
        }

        /// <summary>
        /// Subtraction operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator -(object left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Subtract(left, right);
        }

        /// <summary>
        /// Multiplication operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator *(object left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Multiply(left, right);
        }

        /// <summary>
        /// Division operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator /(object left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Divide(left, right);
        }

        /// <summary>
        /// Modulo operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <param name="right">Right side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator %(object left, VariableBase right)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            return MethodBase.CurrentMethod.Modulo(left, right);
        }

        /// <summary>
        /// Plus one operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator ++(VariableBase left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Add(left, 1));
            return left;
        }

        /// <summary>
        /// Subtract one operator
        /// </summary>
        /// <param name="left">Left side</param>
        /// <returns>The resulting object</returns>
        public static VariableBase operator --(VariableBase left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Subtract(left, 1));
            return left;
        }

        #endregion
    }
}