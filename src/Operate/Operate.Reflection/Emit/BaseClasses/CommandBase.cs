﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit.BaseClasses
{
    /// <summary>
    /// Command base class
    /// </summary>
    public abstract class CommandBase:ICommand
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        protected CommandBase()
        {
            SetupOpCodes();
        }

        #endregion

        #region Functions

        private static void SetupOpCodes()
        {
            if (ConversionOpCodes == null)
            {
                ConversionOpCodes = new Dictionary<Type, OpCode>
                    {
                        {typeof (int), OpCodes.Conv_I4},
                        {typeof (Int64), OpCodes.Conv_I8},
                        {typeof (float), OpCodes.Conv_R4},
                        {typeof (double), OpCodes.Conv_R8},
                        {typeof (uint), OpCodes.Conv_U4},
                        {typeof (UInt64), OpCodes.Conv_U8}
                    };
            }
            if (ComparisonOpCodes == null)
            {
                ComparisonOpCodes = new Dictionary<Enums.Comparison, OpCode>
                    {
                        {Enums.Comparison.Equal, OpCodes.Beq},
                        {Enums.Comparison.GreaterThan, OpCodes.Ble},
                        {Enums.Comparison.GreaterThenOrEqual, OpCodes.Blt},
                        {Enums.Comparison.LessThan, OpCodes.Bge},
                        {Enums.Comparison.LessThanOrEqual, OpCodes.Bgt},
                        {Enums.Comparison.NotEqual, OpCodes.Beq}
                    };
            }
            if (ComparisonTextEquivalent == null)
            {
                ComparisonTextEquivalent = new Dictionary<Enums.Comparison, string>
                    {
                        {Enums.Comparison.Equal, "=="},
                        {Enums.Comparison.GreaterThan, ">"},
                        {Enums.Comparison.GreaterThenOrEqual, ">="},
                        {Enums.Comparison.LessThan, "<"},
                        {Enums.Comparison.LessThanOrEqual, "<="},
                        {Enums.Comparison.NotEqual, "!="}
                    };
            }
        }

        /// <summary>
        /// Sets up the command
        /// </summary>
        public abstract void Setup();

        #endregion

        #region Properties

        /// <summary>
        /// Used to store conversion opcodes
        /// </summary>
        protected static Dictionary<Type, OpCode> ConversionOpCodes { get;private set; }

        /// <summary>
        /// Used to store comparison opcodes
        /// </summary>
        protected static Dictionary<Enums.Comparison, OpCode> ComparisonOpCodes { get;private set; }

        /// <summary>
        /// Used to store text equivalent of comparison types
        /// </summary>
        protected static Dictionary<Enums.Comparison, string> ComparisonTextEquivalent { get;private set; }

        /// <summary>
        /// Return value (set to null if not used by the command)
        /// </summary>
        public VariableBase Result { get; set; }

        #endregion
    }
}
