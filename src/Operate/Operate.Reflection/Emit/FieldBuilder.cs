﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.ExtensionMethods;
using MethodBase = Operate.Reflection.Emit.BaseClasses.MethodBase;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Helper class for defining a field within a type
    /// </summary>
    public class FieldBuilder : VariableBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="typeBuilder">Type builder</param>
        /// <param name="name">Name of the method</param>
        /// <param name="attributes">Attributes for the field (public, private, etc.)</param>
        /// <param name="fieldType">Type for the field</param>
        public FieldBuilder(TypeBuilder typeBuilder, string name, Type fieldType, FieldAttributes attributes)
        {
            if (typeBuilder.IsNull()) { throw new ArgumentNullException("typeBuilder"); }
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Name = name;
            Type = typeBuilder;
            DataType = fieldType;
            Attributes = attributes;
            Builder = Type.Builder.DefineField(name, fieldType, attributes);
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Functions

        /// <summary>
        /// Loads the field
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Load(ILGenerator generator)
        {
            generator.Emit(OpCodes.Ldfld, Builder);
        }

        /// <summary>
        /// Saves the field
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Save(ILGenerator generator)
        {
            generator.Emit(OpCodes.Stfld, Builder);
        }

        /// <summary>
        /// Gets the definition of the field
        /// </summary>
        /// <returns>The field's definition</returns>
        public override string GetDefinition()
        {
            var output = new StringBuilder();

            output.Append("\n");
            if ((Attributes & FieldAttributes.Public) > 0)
                output.Append("public ");
            else if ((Attributes & FieldAttributes.Private) > 0)
                output.Append("private ");
            if ((Attributes & FieldAttributes.Static) > 0)
                output.Append("static ");
            output.Append(DataType.GetName());
            output.Append(" ").Append(Name).Append(";");

            return output.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Field builder
        /// </summary>
        public System.Reflection.Emit.FieldBuilder Builder { get; protected set; }

        /// <summary>
        /// Attributes for the field (private, public, etc.)
        /// </summary>
        public FieldAttributes Attributes { get; protected set; }

        /// <summary>
        /// Type builder
        /// </summary>
        private TypeBuilder Type { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// The field as a string
        /// </summary>
        /// <returns>The field as a string</returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Operator Functions

        /// <summary>
        /// Increments the field by one
        /// </summary>
        /// <param name="left">Field to increment</param>
        /// <returns>The field</returns>
        public static FieldBuilder operator ++(FieldBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Add(left, 1));
            return left;
        }

        /// <summary>
        /// Decrements the field by one
        /// </summary>
        /// <param name="left">Field to decrement</param>
        /// <returns>The field</returns>
        public static FieldBuilder operator --(FieldBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Subtract(left, 1));
            return left;
        }

        #endregion
    }
}