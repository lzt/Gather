﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Globalization;
using System.Reflection.Emit;
using System.Text;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// Boxes an object
    /// </summary>
    public sealed class Box : CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">Value to box</param>
        public Box(object value)
        {
            var tempValue = value as VariableBase;
            Value = tempValue ?? new ConstantBuilder(value);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Value to box
        /// </summary>
        public VariableBase Value { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the command
        /// </summary>
        public override void Setup()
        {
            if (!Value.DataType.IsValueType)
                throw new ArgumentException("Value is not a value type, box operations convert value types to reference types");
            Result = MethodBase.CurrentMethod.CreateLocal("BoxResult" + Value.Name+MethodBase.ObjectCounter.ToString(CultureInfo.InvariantCulture), typeof(object));
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            if (Value is FieldBuilder || Value is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            Value.Load(generator);
            generator.Emit(OpCodes.Box, Value.DataType);
            Result.Save(generator);
        }

        /// <summary>
        /// Converts the command to a string
        /// </summary>
        /// <returns>The string version of the command</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            output.Append(Result).Append("=(").Append(typeof(object).GetName())
                .Append(")").Append(Value).Append(";\n");
            return output.ToString();
        }

        #endregion
    }
}