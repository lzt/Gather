﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Reflection.Emit;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// Throws an exception
    /// </summary>
    public sealed class Throw:CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="exception">Exception to throw</param>
        public Throw(VariableBase exception)
        {
            Exception = exception;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Exception to throw
        /// </summary>
        public VariableBase Exception { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the throw statement
        /// </summary>
        public override void Setup()
        {
            if (Exception is FieldBuilder || Exception is IPropertyBuilder)
                MethodBase.CurrentMethod.Generator.Emit(OpCodes.Ldarg_0);
            Exception.Load(MethodBase.CurrentMethod.Generator);
            MethodBase.CurrentMethod.Generator.Emit(OpCodes.Throw);
        }

        /// <summary>
        /// The throw statement as a string
        /// </summary>
        /// <returns>The throw statement as a string</returns>
        public override string ToString()
        {
            return "throw " + Exception.ToString() + ";\n";
        }

        #endregion
    }
}
