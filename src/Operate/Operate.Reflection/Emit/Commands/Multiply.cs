﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Globalization;
using System.Reflection.Emit;
using System.Text;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// Multiply two variables
    /// </summary>
    public sealed class Multiply : CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="leftHandSide">Left variable</param>
        /// <param name="rightHandSide">Right variable</param>
        public Multiply(object leftHandSide, object rightHandSide)
        {
            var tempLeftHandSide = leftHandSide as VariableBase;
            var tempRightHandSide = rightHandSide as VariableBase;
            LeftHandSide = tempLeftHandSide ?? MethodBase.CurrentMethod.CreateConstant(leftHandSide);
            RightHandSide = tempRightHandSide ?? MethodBase.CurrentMethod.CreateConstant(rightHandSide);
            Result = MethodBase.CurrentMethod.CreateLocal("MultiplyLocalResult" + MethodBase.ObjectCounter.ToString(CultureInfo.InvariantCulture),
                LeftHandSide.DataType);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Left hand side of the multiplication
        /// </summary>
        private VariableBase LeftHandSide { get; set; }

        /// <summary>
        /// Right hand side of the multiplication
        /// </summary>
        private VariableBase RightHandSide { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Set up the command
        /// </summary>
        public override void Setup()
        {
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            if (LeftHandSide is FieldBuilder || LeftHandSide is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            LeftHandSide.Load(generator);
            if (RightHandSide is FieldBuilder || RightHandSide is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            RightHandSide.Load(generator);
            if (LeftHandSide.DataType != RightHandSide.DataType)
            {
                if (ConversionOpCodes.ContainsKey(LeftHandSide.DataType))
                {
                    generator.Emit(ConversionOpCodes[LeftHandSide.DataType]);
                }
            }
            generator.Emit(OpCodes.Mul);
            Result.Save(generator);
        }

        /// <summary>
        /// Converts the command to the string
        /// </summary>
        /// <returns>The string version of the command</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            output.Append(Result).Append("=").Append(LeftHandSide).Append("*").Append(RightHandSide).Append(";\n");
            return output.ToString();
        }

        #endregion
    }
}