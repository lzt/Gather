﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;
using MethodBase = Operate.Reflection.Emit.BaseClasses.MethodBase;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// Call command
    /// </summary>
    public sealed class Call : CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="objectCallingOn">Object calling on</param>
        /// <param name="method">Method builder</param>
        /// <param name="methodCalling">Method calling on the object</param>
        /// <param name="parameters">List of parameters to send in</param>
        public Call(IMethodBuilder method, VariableBase objectCallingOn, MethodInfo methodCalling, object[] parameters)
        {
            ObjectCallingOn = objectCallingOn;
            MethodCalling = methodCalling;
            MethodCallingFrom = method;
            if (methodCalling.ReturnType != typeof(void))
            {
                Result = method.CreateLocal(methodCalling.Name + "ReturnObject" + MethodBase.ObjectCounter.ToString(CultureInfo.InvariantCulture), methodCalling.ReturnType);
            }
            if (parameters != null)
            {
                Parameters = new VariableBase[parameters.Length];
                for (int x = 0; x < parameters.Length; ++x)
                {
                    var @base = parameters[x] as VariableBase;
                    if (@base != null)
                        Parameters[x] = @base;
                    else
                        Parameters[x] = MethodCallingFrom.CreateConstant(parameters[x]);
                }
            }
            else
            {
                Parameters = null;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="objectCallingOn">Object calling on</param>
        /// <param name="method">Method builder</param>
        /// <param name="methodCalling">Method calling on the object</param>
        /// <param name="parameters">List of parameters to send in</param>
        public Call(IMethodBuilder method, VariableBase objectCallingOn, ConstructorInfo methodCalling, object[] parameters)
        {
            ObjectCallingOn = objectCallingOn;
            ConstructorCalling = methodCalling;
            MethodCallingFrom = method;
            if (parameters != null)
            {
                Parameters = new VariableBase[parameters.Length];
                for (int x = 0; x < parameters.Length; ++x)
                {
                    var @base = parameters[x] as VariableBase;
                    if (@base != null)
                        Parameters[x] = @base;
                    else
                        Parameters[x] = MethodCallingFrom.CreateConstant(parameters[x]);
                }
            }
            else
            {
                Parameters = null;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Object calling on
        /// </summary>
        private VariableBase ObjectCallingOn { get; set; }

        /// <summary>
        /// Method calling
        /// </summary>
        private MethodInfo MethodCalling { get; set; }

        /// <summary>
        /// Method calling
        /// </summary>
        private ConstructorInfo ConstructorCalling { get; set; }

        /// <summary>
        /// Parameters sent in
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        private VariableBase[] Parameters { get; set; }

        /// <summary>
        /// Method calling from
        /// </summary>
        private IMethodBuilder MethodCallingFrom { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the command
        /// </summary>
        public override void Setup()
        {
            if (ObjectCallingOn != null)
            {
                if (ObjectCallingOn is FieldBuilder || ObjectCallingOn is IPropertyBuilder)
                    MethodCallingFrom.Generator.Emit(OpCodes.Ldarg_0);
                ObjectCallingOn.Load(MethodCallingFrom.Generator);
            }
            if (Parameters != null)
            {
                foreach (VariableBase parameter in Parameters)
                {
                    if (parameter is FieldBuilder || parameter is IPropertyBuilder)
                        MethodCallingFrom.Generator.Emit(OpCodes.Ldarg_0);
                    parameter.Load(MethodCallingFrom.Generator);
                }
            }
            OpCode opCodeUsing = OpCodes.Callvirt;
            if (MethodCalling != null)
            {
                if (ObjectCallingOn != null && (!MethodCalling.IsVirtual ||
                                                (ObjectCallingOn.Name == "this" && MethodCalling.Name == MethodCallingFrom.Name)))
                    opCodeUsing = OpCodes.Call;
                MethodCallingFrom.Generator.EmitCall(opCodeUsing, MethodCalling, null);
                if (MethodCalling.ReturnType != typeof(void))
                {
                    Result.Save(MethodCallingFrom.Generator);
                }
            }
            else if (ConstructorCalling != null)
            {
                if (MethodCalling != null && (ObjectCallingOn != null && (!ConstructorCalling.IsVirtual ||
                                                                          (ObjectCallingOn.Name == "this" && MethodCalling.Name == MethodCallingFrom.Name))))
                    opCodeUsing = OpCodes.Call;
                MethodCallingFrom.Generator.Emit(opCodeUsing, ConstructorCalling);
            }
        }

        /// <summary>
        /// Converts the command to a string
        /// </summary>
        /// <returns>The string version of the command</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            if (Result != null)
            {
                output.Append(Result).Append(" = ");
            }
            if (ObjectCallingOn != null)
                output.Append(ObjectCallingOn).Append(".");
            if (ObjectCallingOn != null && (ObjectCallingOn.Name == "this" && MethodCallingFrom.Name == MethodCalling.Name))
            {
                output.Append("base").Append("(");
            }
            else
            {
                output.Append(MethodCalling.Name).Append("(");
            }
            string seperator = "";
            if (Parameters != null)
            {
                foreach (VariableBase variable in Parameters)
                {
                    output.Append(seperator).Append(variable);
                    seperator = ",";
                }
            }
            output.Append(");\n");
            return output.ToString();
        }

        #endregion
    }
}