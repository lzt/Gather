﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Reflection.Emit;
using System.Text;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// Assignment command
    /// </summary>
    public sealed class Assign : CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="value">Value to store</param>
        public Assign(VariableBase leftHandSide, object value)
        {
            if (leftHandSide.IsNull()) { throw new ArgumentNullException("leftHandSide"); }
            LeftHandSide = leftHandSide;
            var tempValue = value as VariableBase;
            RightHandSide = tempValue ?? MethodBase.CurrentMethod.CreateConstant(value);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Left hand side of the assignment
        /// </summary>
        private VariableBase LeftHandSide { get; set; }

        /// <summary>
        /// Value to assign
        /// </summary>
        private VariableBase RightHandSide { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the command
        /// </summary>
        public override void Setup()
        {
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            if (RightHandSide.DataType.IsValueType 
                && !LeftHandSide.DataType.IsValueType)
            {
                RightHandSide = MethodBase.CurrentMethod.Box(RightHandSide);
            }
            else if (!RightHandSide.DataType.IsValueType 
                && LeftHandSide.DataType.IsValueType)
            {
                RightHandSide = MethodBase.CurrentMethod.UnBox(RightHandSide, LeftHandSide.DataType);
            }
            else if (!RightHandSide.DataType.IsValueType 
                && !LeftHandSide.DataType.IsValueType
                && RightHandSide.DataType != LeftHandSide.DataType)
            {
                RightHandSide = MethodBase.CurrentMethod.Cast(RightHandSide, LeftHandSide.DataType);
            }
            if (LeftHandSide is FieldBuilder || LeftHandSide is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            if (RightHandSide is FieldBuilder || RightHandSide is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            RightHandSide.Load(generator);
            if (RightHandSide.DataType != LeftHandSide.DataType)
            {
                if (ConversionOpCodes.ContainsKey(LeftHandSide.DataType))
                {
                    generator.Emit(ConversionOpCodes[LeftHandSide.DataType]);
                }
            }
            LeftHandSide.Save(generator);
        }

        /// <summary>
        /// Converts the command to a string
        /// </summary>
        /// <returns>The string version of the command</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            output.Append(LeftHandSide).Append("=").Append(RightHandSide).Append(";\n");
            return output.ToString();
        }

        #endregion
    }
}