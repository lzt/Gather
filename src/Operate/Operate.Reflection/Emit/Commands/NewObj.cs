﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;
using Operate.Reflection.ExtensionMethods;
using MethodBase = Operate.Reflection.Emit.BaseClasses.MethodBase;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// Command for creating a new object
    /// </summary>
    public sealed class NewObj : CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="constructor">Constructor to use</param>
        /// <param name="parameters">Variables sent to the constructor</param>
        public NewObj(ConstructorInfo constructor, object[] parameters)
        {
            Constructor = constructor;
            if (parameters != null)
            {
                Parameters = new VariableBase[parameters.Length];
                for (int x = 0; x < parameters.Length; ++x)
                {
                    var @base = parameters[x] as VariableBase;
                    if (@base != null)
                        Parameters[x] = @base;
                    else
                        Parameters[x] = MethodBase.CurrentMethod.CreateConstant(parameters[x]);
                }
            }
            Result = MethodBase.CurrentMethod.CreateLocal("ObjLocal" + MethodBase.ObjectCounter.ToString(CultureInfo.InvariantCulture),
                constructor.DeclaringType);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Constructor used
        /// </summary>
        private ConstructorInfo Constructor { get; set; }

        /// <summary>
        /// Variables sent to the Constructor
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        private VariableBase[] Parameters { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Sets up the command
        /// </summary>
        public override void Setup()
        {
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            if (Parameters != null)
            {
                foreach (VariableBase parameter in Parameters)
                {
                    if (parameter is FieldBuilder || parameter is IPropertyBuilder)
                        generator.Emit(OpCodes.Ldarg_0);
                    parameter.Load(generator);
                }
            }
            generator.Emit(OpCodes.Newobj, Constructor);
            Result.Save(generator);
        }

        /// <summary>
        /// Converts the command to the string
        /// </summary>
        /// <returns>The string version of the command</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            output.Append(Result).Append(" = new ")
                .Append(Constructor.DeclaringType.GetName())
                .Append("(");
            string seperator = "";
            if (Parameters != null)
            {
                foreach (VariableBase variable in Parameters)
                {
                    output.Append(seperator).Append(variable);
                    seperator = ",";
                }
            }
            output.Append(");\n");
            return output.ToString();
        }

        #endregion
    }
}