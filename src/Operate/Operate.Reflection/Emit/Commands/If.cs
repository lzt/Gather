﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Reflection.Emit;
using System.Text;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit.Commands
{
    /// <summary>
    /// If command
    /// </summary>
    public sealed class If : CommandBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="comparisonType">Comparison type</param>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="rightHandSide">Right hand side</param>
        public If(Enums.Comparison comparisonType, VariableBase leftHandSide, VariableBase rightHandSide)
        {
            var generator = MethodBase.CurrentMethod.Generator;
            EndIfLabel = generator.DefineLabel();
            EndIfFinalLabel = generator.DefineLabel();
            LeftHandSide = leftHandSide ?? MethodBase.CurrentMethod.CreateConstant(null);
            RightHandSide = rightHandSide ?? MethodBase.CurrentMethod.CreateConstant(null);
            ComparisonType = comparisonType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// End if label
        /// </summary>
        public Label EndIfLabel { get; set; }

        /// <summary>
        /// End if label
        /// </summary>
        public Label EndIfFinalLabel { get; set; }

        /// <summary>
        /// Left hand side of the comparison
        /// </summary>
        public VariableBase LeftHandSide { get; set; }

        /// <summary>
        /// Right hand side of the comparison
        /// </summary>
        public VariableBase RightHandSide { get; set; }

        /// <summary>
        /// Comparison type
        /// </summary>
        public Enums.Comparison ComparisonType { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Ends the if statement
        /// </summary>
        public void EndIf()
        {
            MethodBase.CurrentMethod.EndIf(this);
        }

        /// <summary>
        /// Defines an else if statement
        /// </summary>
        /// <param name="comparisonType">Comparison type</param>
        /// <param name="leftHandSide">left hand side value</param>
        /// <param name="rightHandSide">right hand side value</param>
        public void ElseIf(VariableBase leftHandSide, Enums.Comparison comparisonType, VariableBase rightHandSide)
        {
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            generator.Emit(OpCodes.Br, EndIfFinalLabel);
            generator.MarkLabel(EndIfLabel);
            EndIfLabel = generator.DefineLabel();
            MethodBase.CurrentMethod.Commands.Add(new ElseIf(EndIfLabel, comparisonType, leftHandSide, rightHandSide));
        }

        /// <summary>
        /// Defines an else statement
        /// </summary>
        public void Else()
        {
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            generator.Emit(OpCodes.Br, EndIfFinalLabel);
            generator.MarkLabel(EndIfLabel);
            EndIfLabel = generator.DefineLabel();
            MethodBase.CurrentMethod.Commands.Add(new Else());
        }

        /// <summary>
        /// Sets up the command
        /// </summary>
        public override void Setup()
        {
            ILGenerator generator = MethodBase.CurrentMethod.Generator;
            if (LeftHandSide is FieldBuilder || LeftHandSide is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            LeftHandSide.Load(generator);
            if (RightHandSide is FieldBuilder || RightHandSide is IPropertyBuilder)
                generator.Emit(OpCodes.Ldarg_0);
            RightHandSide.Load(generator);
            if (ComparisonType == Enums.Comparison.Equal)
            {
                generator.Emit(OpCodes.Ceq);
                generator.Emit(OpCodes.Ldc_I4_0);
            }
            generator.Emit(ComparisonOpCodes[ComparisonType], EndIfLabel);
        }

        /// <summary>
        /// Converts the command to the string
        /// </summary>
        /// <returns>The string version of the command</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            output.Append("if(").Append(LeftHandSide)
                .Append(ComparisonTextEquivalent[ComparisonType])
                .Append(RightHandSide).Append(")\n}\n");
            return output.ToString();
        }

        #endregion
    }
}