﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Interfaces;
using Operate.Reflection.ExtensionMethods;
using MethodBase = Operate.Reflection.Emit.BaseClasses.MethodBase;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Helper class for defining default properties
    /// </summary>
    public class DefaultPropertyBuilder : VariableBase, IPropertyBuilder
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="typeBuilder">Type builder</param>
        /// <param name="name">Name of the property</param>
        /// <param name="attributes">Attributes for the property (public, private, etc.)</param>
        /// <param name="getMethodAttributes">Get method attributes</param>
        /// <param name="setMethodAttributes">Set method attributes</param>
        /// <param name="propertyType">Property type for the property</param>
        /// <param name="parameters">Parameter types for the property</param>
        public DefaultPropertyBuilder(TypeBuilder typeBuilder, string name,
            PropertyAttributes attributes, MethodAttributes getMethodAttributes,
            MethodAttributes setMethodAttributes,
            Type propertyType, IEnumerable<Type> parameters)
        {
            if (typeBuilder.IsNull()) { throw new ArgumentNullException("typeBuilder"); }
            if (name.IsNull()) { throw new ArgumentNullException("name"); }
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Name = name;
            Type = typeBuilder;
            Attributes = attributes;
            GetMethodAttributes = getMethodAttributes;
            SetMethodAttributes = setMethodAttributes;
            DataType = propertyType;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
            Parameters = new List<ParameterBuilder>();
            var enumerable = parameters as Type[] ?? parameters.ToArray();
            if (parameters != null)
            {
                int x = 1;
                foreach (Type parameter in enumerable)
                {
                    Parameters.Add(new ParameterBuilder(parameter, x));
                    ++x;
                }
            }
            Field = new FieldBuilder(Type, "_" + name + "field", propertyType, FieldAttributes.Private);
            Builder = Type.Builder.DefineProperty(name, attributes, propertyType,
                (parameters != null && enumerable.Any()) ? enumerable.ToArray() : System.Type.EmptyTypes);
            GetMethod = new MethodBuilder(Type, "get_" + name, getMethodAttributes, enumerable, propertyType);
            GetMethod.Generator.Emit(OpCodes.Ldarg_0);
            GetMethod.Generator.Emit(OpCodes.Ldfld, Field.Builder);
            GetMethod.Generator.Emit(OpCodes.Ret);
            var setParameters = new List<Type>();
            if (parameters != null)
            {
                setParameters.AddRange(enumerable);
            }
            setParameters.Add(propertyType);
            SetMethod = new MethodBuilder(Type, "set_" + name, setMethodAttributes, setParameters, typeof(void));
            SetMethod.Generator.Emit(OpCodes.Ldarg_0);
            SetMethod.Generator.Emit(OpCodes.Ldarg_1);
            SetMethod.Generator.Emit(OpCodes.Stfld, Field.Builder);
            SetMethod.Generator.Emit(OpCodes.Ret);
            Builder.SetGetMethod(GetMethod.Builder);
            Builder.SetSetMethod(SetMethod.Builder);
        }

        #endregion

        #region Functions

        /// <summary>
        /// Loads the property
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Load(ILGenerator generator)
        {
            generator.EmitCall(GetMethod.Builder.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, GetMethod.Builder, null);
        }

        /// <summary>
        /// Saves the property
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Save(ILGenerator generator)
        {
            generator.EmitCall(SetMethod.Builder.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, SetMethod.Builder, null);
        }

        /// <summary>
        /// Gets the definition of the property
        /// </summary>
        /// <returns>The definition of the property</returns>
        public override string GetDefinition()
        {
            var output = new StringBuilder();

            output.Append("\n");
            if ((GetMethodAttributes & MethodAttributes.Public) > 0)
                output.Append("public ");
            else if ((GetMethodAttributes & MethodAttributes.Private) > 0)
                output.Append("private ");
            if ((GetMethodAttributes & MethodAttributes.Static) > 0)
                output.Append("static ");
            if ((GetMethodAttributes & MethodAttributes.Virtual) > 0)
                output.Append("virtual ");
            else if ((GetMethodAttributes & MethodAttributes.Abstract) > 0)
                output.Append("abstract ");
            else if ((GetMethodAttributes & MethodAttributes.HideBySig) > 0)
                output.Append("override ");
            output.Append(DataType.GetName());
            output.Append(" ").Append(Name);

            string splitter = "";
            if (Parameters != null && Parameters.Count > 0)
            {
                output.Append("[");
                foreach (ParameterBuilder parameter in Parameters)
                {
                    output.Append(splitter).Append(parameter.GetDefinition());
                    splitter = ",";
                }
                output.Append("]");
            }
            output.Append(" { get; ");
            if ((SetMethodAttributes & GetMethodAttributes) != SetMethodAttributes)
            {
                if ((SetMethodAttributes & MethodAttributes.Public) > 0)
                    output.Append("public ");
                else if ((SetMethodAttributes & MethodAttributes.Private) > 0)
                    output.Append("private ");
            }
            output.Append("set; }\n");

            return output.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Parameter types
        /// </summary>
        public ICollection<ParameterBuilder> Parameters { get; private set; }

        /// <summary>
        /// Method builder
        /// </summary>
        public System.Reflection.Emit.PropertyBuilder Builder { get; private set; }

        /// <summary>
        /// Attributes for the property
        /// </summary>
        public PropertyAttributes Attributes { get; private set; }

        /// <summary>
        /// Attributes for the get method
        /// </summary>
        public MethodAttributes GetMethodAttributes { get; private set; }

        /// <summary>
        /// Attributes for the set method
        /// </summary>
        public MethodAttributes SetMethodAttributes { get; private set; }

        /// <summary>
        /// Method builder for the get method
        /// </summary>
        public MethodBuilder GetMethod { get; private set; }

        /// <summary>
        /// Method builder for the set method
        /// </summary>
        public MethodBuilder SetMethod { get; private set; }

        /// <summary>
        /// Field builder
        /// </summary>
        public FieldBuilder Field { get; protected set; }

        /// <summary>
        /// Type builder
        /// </summary>
        private TypeBuilder Type { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// property as a string
        /// </summary>
        /// <returns>Property as a string</returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Operator Functions

        /// <summary>
        /// Increments the property by one
        /// </summary>
        /// <param name="left">The property to increment</param>
        /// <returns>The property</returns>
        public static DefaultPropertyBuilder operator ++(DefaultPropertyBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Add(left, 1));
            return left;
        }

        /// <summary>
        /// Decrements the property by one
        /// </summary>
        /// <param name="left">The property to decrement</param>
        /// <returns>The property</returns>
        public static DefaultPropertyBuilder operator --(DefaultPropertyBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Subtract(left, 1));
            return left;
        }

        #endregion
    }
}