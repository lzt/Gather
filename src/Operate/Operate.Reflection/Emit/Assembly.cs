﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;

using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.Enums;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Assembly class
    /// </summary>
    public class Assembly
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Assembly name</param>
        /// <param name="directory">Directory to save the assembly (if left blank, the assembly is run only and will not be saved)</param>
        /// <param name="type">Assembly type (exe or dll)</param>
        public Assembly(string name, string directory = "", AssemblyType type = AssemblyType.Dll)
        {
            if (name.IsNullOrEmpty()) { throw new InvalidOperationException("name"); }
            Setup(name, directory, type);
        }

        #endregion

        #region Functions

        #region Setup

        /// <summary>
        /// Sets up the assembly
        /// </summary>
        /// <param name="name">Assembly name</param>
        /// <param name="directory">Directory to save the assembly (if left blank, the assembly is run only and will not be saved)</param>
        /// <param name="type">Assembly type (dll or exe)</param>
        private void Setup(string name, string directory = "", AssemblyType type = AssemblyType.Dll)
        {
            Name = name;
            Directory = directory;
            Type = type;
            var assemblyName = new AssemblyName(name);
            var domain = Thread.GetDomain();
            if (!string.IsNullOrEmpty(directory))
            {
                Builder = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave, directory);
                Module = Builder.DefineDynamicModule(name, name + (type == AssemblyType.Dll ? ".dll" : ".exe"), true);
            }
            else
            {
                Builder = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
                Module = Builder.DefineDynamicModule(name);
            }
            Classes = new List<TypeBuilder>();
            Enums = new List<EnumBuilder>();
        }

        #endregion

        #region CreateType

        /// <summary>
        /// Creates a type builder
        /// </summary>
        /// <param name="name">Name of the type</param>
        /// <param name="attributes">Attributes associated with the type</param>
        /// <param name="baseClass">Base class for this type</param>
        /// <param name="interfaces">Interfaces used by this type</param>
        /// <returns>A TypeBuilder class</returns>
        public virtual TypeBuilder CreateType(string name, TypeAttributes attributes = TypeAttributes.Public,
            Type baseClass = null, IEnumerable<Type> interfaces = null)
        {
            var returnValue = new TypeBuilder(this, name, interfaces, baseClass, attributes);
            Classes.Add(returnValue);
            return returnValue;
        }

        #endregion

        #region CreateEnum

        /// <summary>
        /// Creates an enum builder
        /// </summary>
        /// <param name="name">Name of the enum</param>
        /// <param name="enumBaseType">Base type of the enum (defaults to int)</param>
        /// <param name="attributes">Attributes associated with the type</param>
        /// <returns>An EnumBuilder class</returns>
        public virtual EnumBuilder CreateEnum(string name, Type enumBaseType = null,
            TypeAttributes attributes = TypeAttributes.Public)
        {
            if (enumBaseType == null)
                enumBaseType = typeof(int);
            var returnValue = new EnumBuilder(this, name, enumBaseType, attributes);
            Enums.Add(returnValue);
            return returnValue;
        }

        #endregion

        #region Create

        /// <summary>
        /// Creates all types associated with the assembly and saves the assembly to disk
        /// if a directory is specified.
        /// </summary>
        public virtual void Create()
        {
            foreach (TypeBuilder Class in Classes)
                Class.Create();
            foreach (IType Enum in Enums)
                Enum.Create();
            if (!string.IsNullOrEmpty(Directory))
                Builder.Save(Name + (Type == AssemblyType.Dll ? ".dll" : ".exe"));
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// ModuleBuilder object
        /// </summary>
        public ModuleBuilder Module { get; protected set; }

        /// <summary>
        /// Name of the assembly
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Directory of the assembly
        /// </summary>
        public string Directory { get; protected set; }

        /// <summary>
        /// List of classes associated with this assembly
        /// </summary>
        public ICollection<TypeBuilder> Classes { get; private set; }

        /// <summary>
        /// List of enums associated with this assembly
        /// </summary>
        public ICollection<EnumBuilder> Enums { get; private set; }

        /// <summary>
        /// Assembly type (exe or dll)
        /// </summary>
        public AssemblyType Type { get; protected set; }

        /// <summary>
        /// Assembly builder
        /// </summary>
        protected AssemblyBuilder Builder { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Converts the assembly to a string
        /// </summary>
        /// <returns>The string version of the assembly</returns>
        public override string ToString()
        {
            var output = new StringBuilder();
            Enums.ForEach(x => output.Append(x.ToString()));
            Classes.ForEach(x => output.Append(x.ToString()));
            return output.ToString();
        }

        #endregion
    }
}