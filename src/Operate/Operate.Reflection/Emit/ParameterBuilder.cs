﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

using System.Globalization;
using System.Reflection.Emit;
using Operate.Reflection.Emit.BaseClasses;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Used to define a parameter
    /// </summary>
    public class ParameterBuilder : VariableBase
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parameterType">Parameter type</param>
        /// <param name="number">Position in parameter order</param>
        public ParameterBuilder(Type parameterType, int number)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            if (number == -1)
            {

                Name = "value";
                Number = 1;
                DataType = parameterType;
                return;
            }
            if (number == 0)
            {
                Name = "this";
                Number = 0;
                DataType = null;
                return;
            }
            Name = "Parameter" + number.ToString(CultureInfo.InvariantCulture);
            Number = number;
            DataType = parameterType;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        /// <summary>
        /// Order in the parameter list
        /// </summary>
        public int Number { get; protected set; }

        #endregion

        #region Functions

        /// <summary>
        /// Loads from the parameter
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Load(ILGenerator generator)
        {
            generator.Emit(OpCodes.Ldarg, Number);
        }

        /// <summary>
        /// Saves to the parameter
        /// </summary>
        /// <param name="generator">IL Generator</param>
        public override void Save(ILGenerator generator)
        {
            generator.Emit(OpCodes.Starg, Number);
        }

        #endregion

        #region Overridden Function

        /// <summary>
        /// Outputs the parameter as a string
        /// </summary>
        /// <returns>The parameter</returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Operator Functions

        /// <summary>
        /// Increments by one
        /// </summary>
        /// <param name="left">Parameter to increment</param>
        /// <returns>The parameter</returns>
        public static ParameterBuilder operator ++(ParameterBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Add(left, 1));
            return left;
        }

        /// <summary>
        /// Decrements by one
        /// </summary>
        /// <param name="left">Parameter to decrement</param>
        /// <returns>The parameter</returns>
        public static ParameterBuilder operator --(ParameterBuilder left)
        {
            if (MethodBase.CurrentMethod.IsNull()) { throw new InvalidOperationException("Unsure which method is the current method"); }
            left.Assign(MethodBase.CurrentMethod.Subtract(left, 1));
            return left;
        }

        #endregion
    }
}