﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Operate.Reflection.Emit.BaseClasses;
using Operate.Reflection.Emit.Commands;

#endregion

namespace Operate.Reflection.Emit.Interfaces
{
    /// <summary>
    /// Interface for methods
    /// </summary>
    public interface IMethodBuilder
    {
        #region Functions

        /// <summary>
        /// Sets the method as the current method
        /// </summary>
        void SetCurrentMethod();

        /// <summary>
        /// Defines a local variable
        /// </summary>
        /// <param name="name">Name of the local variable</param>
        /// <param name="localType">The Type of the local variable</param>
        /// <returns>The LocalBuilder associated with the variable</returns>
        VariableBase CreateLocal(string name, Type localType);

        /// <summary>
        /// Constant value
        /// </summary>
        /// <param name="value">Value of the constant</param>
        /// <returns>The ConstantBuilder associated with the variable</returns>
        VariableBase CreateConstant(object value);

        /// <summary>
        /// Creates new object
        /// </summary>
        /// <param name="constructor">Constructor</param>
        /// <param name="variables">Variables to send to the constructor</param>
        VariableBase NewObj(ConstructorInfo constructor, object[] variables=null);

        /// <summary>
        /// Creates new object
        /// </summary>
        /// <param name="objectType">object type</param>
        /// <param name="variables">Variables to send to the constructor</param>
        VariableBase NewObj(Type objectType, object[] variables=null);

        /// <summary>
        /// Assigns the value to the left hand side variable
        /// </summary>
        /// <param name="leftHandSide">Left hand side variable</param>
        /// <param name="value">Value to store (may be constant or VariableBase object)</param>
        void Assign(VariableBase leftHandSide, object value);

        /// <summary>
        /// Returns a specified value
        /// </summary>
        /// <param name="returnValue">Variable to return</param>
        void Return(object returnValue);

        /// <summary>
        /// Returns from the method (used if void is the return type)
        /// </summary>
        void Return();

        /// <summary>
        /// Calls a function on an object
        /// </summary>
        /// <param name="objectCallingOn">Object calling on</param>
        /// <param name="methodCalling">Method calling</param>
        /// <param name="parameters">Parameters sending</param>
        /// <returns>The return value</returns>
        VariableBase Call(VariableBase objectCallingOn, MethodInfo methodCalling, object[] parameters);

        /// <summary>
        /// Calls a function on an object
        /// </summary>
        /// <param name="objectCallingOn">Object calling on</param>
        /// <param name="methodCalling">Method calling</param>
        /// <param name="parameters">Parameters sending</param>
        /// <returns>The return value</returns>
        void Call(VariableBase objectCallingOn, ConstructorInfo methodCalling, object[] parameters);

        /// <summary>
        /// Defines an if statement
        /// </summary>
        /// <param name="comparisonType">Comparison type</param>
        /// <param name="leftHandSide">Left hand side of the if statement</param>
        /// <param name="rightHandSide">Right hand side of the if statement</param>
        /// <returns>The if command</returns>
        If If(VariableBase leftHandSide, Enums.Comparison comparisonType, VariableBase rightHandSide);

        /// <summary>
        /// Defines the end of an if statement
        /// </summary>
        /// <param name="ifCommand">If command</param>
        void EndIf(If ifCommand);

        /// <summary>
        /// Defines a while statement
        /// </summary>
        /// <param name="comparisonType">Comparison type</param>
        /// <param name="leftHandSide">Left hand side of the while statement</param>
        /// <param name="rightHandSide">Right hand side of the while statement</param>
        /// <returns>The while command</returns>
        While While(VariableBase leftHandSide, Enums.Comparison comparisonType, VariableBase rightHandSide);

        /// <summary>
        /// Defines the end of a while statement
        /// </summary>
        /// <param name="whileCommand">While command</param>
        void EndWhile(While whileCommand);

        /// <summary>
        /// Adds two variables and returns the result
        /// </summary>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="rightHandSide">Right hand side</param>
        /// <returns>The result</returns>
        VariableBase Add(object leftHandSide, object rightHandSide);

        /// <summary>
        /// Subtracts two variables and returns the result
        /// </summary>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="rightHandSide">Right hand side</param>
        /// <returns>The result</returns>
        VariableBase Subtract(object leftHandSide, object rightHandSide);

        /// <summary>
        /// Multiplies two variables and returns the result
        /// </summary>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="rightHandSide">Right hand side</param>
        /// <returns>The result</returns>
        VariableBase Multiply(object leftHandSide, object rightHandSide);

        /// <summary>
        /// Divides two variables and returns the result
        /// </summary>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="rightHandSide">Right hand side</param>
        /// <returns>The result</returns>
        VariableBase Divide(object leftHandSide, object rightHandSide);

        /// <summary>
        /// Mods (%) two variables and returns the result
        /// </summary>
        /// <param name="leftHandSide">Left hand side</param>
        /// <param name="rightHandSide">Right hand side</param>
        /// <returns>The result</returns>
        VariableBase Modulo(object leftHandSide, object rightHandSide);

        /// <summary>
        /// Starts a try block
        /// </summary>
        Try Try();

        /// <summary>
        /// Ends a try block and starts a catch block
        /// </summary>
        /// <param name="exceptionType">Exception type</param>
        Catch Catch(Type exceptionType);

        /// <summary>
        /// Ends a try/catch block
        /// </summary>
        void EndTry();

        /// <summary>
        /// Boxes a value
        /// </summary>
        /// <param name="value">Value to box</param>
        /// <returns>The resulting boxed variable</returns>
        VariableBase Box(object value);

        /// <summary>
        /// Unboxes a value
        /// </summary>
        /// <param name="value">Value to unbox</param>
        /// <param name="valueType">Type to unbox to</param>
        /// <returns>The resulting unboxed variable</returns>
        VariableBase UnBox(VariableBase value,Type valueType);

        /// <summary>
        /// Casts an object to another type
        /// </summary>
        /// <param name="value">Value to cast</param>
        /// <param name="valueType">Value type to cast to</param>
        /// <returns>The resulting casted value</returns>
        VariableBase Cast(VariableBase value, Type valueType);

        /// <summary>
        /// Throws an exception
        /// </summary>
        /// <param name="exception">Exception to throw</param>
        void Throw(VariableBase exception);

        #endregion

        #region Properties

        /// <summary>
        /// Method name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Return type
        /// </summary>
        Type ReturnType { get; }

        /// <summary>
        /// Parameters
        /// </summary>
        ICollection<ParameterBuilder> Parameters { get; }

        /// <summary>
        /// Attributes for the method
        /// </summary>
        MethodAttributes Attributes { get; }

        /// <summary>
        /// IL generator for this method
        /// </summary>
        ILGenerator Generator { get; }

        /// <summary>
        /// Returns the this object for this object
        /// </summary>
        VariableBase This { get; }

        #endregion
    }
}