﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Operate.ExtensionMethods;
using Operate.Reflection.Emit.Interfaces;

#endregion

namespace Operate.Reflection.Emit
{
    /// <summary>
    /// Helper class for defining enums
    /// </summary>
    public class EnumBuilder : IType
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assembly">Assembly builder</param>
        /// <param name="name">Name of the enum</param>
        /// <param name="attributes">Attributes for the enum (public, private, etc.)</param>
        /// <param name="enumType">Type for the enum</param>
        public EnumBuilder(Assembly assembly, string name, Type enumType, TypeAttributes attributes)
        {
            if (assembly.IsNull()) { throw new ArgumentNullException("assembly"); }
            if (name.IsNullOrEmpty()) { throw new ArgumentNullException("name"); }
           Name = name;
           Assembly = assembly;
           EnumType = enumType;
           Attributes = attributes;
           Literals = new List<System.Reflection.Emit.FieldBuilder>();
            Builder = assembly.Module.DefineEnum(assembly.Name + "." + name, TypeAttributes.Public, enumType);
        }

        #endregion

        #region Functions

        #region AddLiteral

        /// <summary>
        /// Adds a literal to the enum (an entry)
        /// </summary>
        /// <param name="name">Name of the entry</param>
        /// <param name="value">Value associated with it</param>
        public virtual void AddLiteral(string name, object value)
        {
            Literals.Add(Builder.DefineLiteral(name, value));
        }

        #endregion

        #region Create

        /// <summary>
        /// Creates the enum
        /// </summary>
        /// <returns>The type defined by this EnumBuilder</returns>
        public virtual Type Create()
        {
            if (Builder == null)
                throw new InvalidOperationException("The builder object has not been defined. Ensure that Setup is called prior to Create");
            if (DefinedType != null)
                return DefinedType;
            DefinedType = Builder.CreateType();
            return DefinedType;
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Field name
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Literals defined within the enum
        /// </summary>
        public ICollection<System.Reflection.Emit.FieldBuilder> Literals { get; private set; }

        /// <summary>
        /// Field builder
        /// </summary>
        public System.Reflection.Emit.EnumBuilder Builder { get; protected set; }

        /// <summary>
        /// Base enum type (int32, etc.)
        /// </summary>
        public Type EnumType { get; protected set; }

        /// <summary>
        /// Type defined by this enum
        /// </summary>
        public Type DefinedType { get; protected set; }

        /// <summary>
        /// Attributes for the enum (private, public, etc.)
        /// </summary>
        public TypeAttributes Attributes { get; protected set; }

        /// <summary>
        /// Assembly builder
        /// </summary>
        protected Assembly Assembly { get; set; }

        #endregion

        #region Overridden Functions

        /// <summary>
        /// Enum definition as a string
        /// </summary>
        /// <returns>The enum as a string</returns>
        public override string ToString()
        {
            string[] splitter = { "." };
            string[] nameParts = Name.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            var output = new StringBuilder();
            output.Append("namespace ").Append(Assembly.Name);
            for (int x = 0; x < nameParts.Length - 1; ++x)
                output.Append(".").Append(nameParts[x]);
            output.Append("\n{\n");
            output.Append((Attributes & TypeAttributes.Public) > 0 ? "public " : "private ");
            output.Append("enum ").Append(nameParts[nameParts.Length - 1]).Append("\n{");
            string seperator = "";
            foreach (System.Reflection.Emit.FieldBuilder literal in Literals)
            {
                output.Append(seperator).Append("\n\t").Append(literal.Name);
                seperator = ",";
            }
            output.Append("\n}\n}\n\n");
            return output.ToString();
        }

        #endregion
    }
}