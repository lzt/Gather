﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Operate.ExtensionMethods;
using Operate.Reflection.ExtensionMethods;

#endregion

namespace Operate.Web.ExtensionMethods
{
    /// <summary>
    /// HttpContext extensions
    /// </summary>
    public static class HttpContextExtensions
    {
        #region Functions

        #region HttpCookies

        /// <summary>
        /// 清除指定Cookie
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name">cookiename</param>
        public static void ClearCookie(this HttpContext context, string name)
        {
            var cookie = context.Request.Cookies[name];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddYears(-3);
                context.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// 获取指定Cookie值
        /// </summary>
        /// <param name="context">Current HttpContext</param>
        /// <param name="name">cookiename</param>
        /// <returns></returns>
        public static string GetCookie(this HttpContext context, string name)
        {
            HttpCookie cookie = context.Request.Cookies[name];
            string str = string.Empty;
            if (cookie != null)
            {
                str = cookie.Value;
            }
            return str;
        }

        /// <summary>
        /// 添加一个Cookie（24小时过期）
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void SetCookie(this HttpContext context, string name, string value)
        {
            context.SetCookie(name, value, DateTime.Now.AddDays(1.0));
        }

        /// <summary>
        /// 添加一个Cookie
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name">cookie名</param>
        /// <param name="value">cookie值</param>
        /// <param name="expires">过期时间 DateTime</param>
        public static void SetCookie(this HttpContext context, string name, string value, DateTime expires)
        {
            var cookie = new HttpCookie(name)
            {
                Value = value,
                Expires = expires
            };
            context.Response.Cookies.Add(cookie);
        }

        #endregion

        #region DumpAllInformation

        /// <summary>
        /// Dumps a lot of information about the request to a string (Request, Response, Session, Cookies, Cache, and Application state)
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <param name="htmlOutput">Determines if this should be HTML output or not</param>
        /// <returns>The exported data</returns>
        public static string DumpAllInformation(this HttpContext context, bool htmlOutput = false)
        {
            const string htmlTemplate = "<strong>Request Variables</strong><br />{Request}<br /><br /><strong>Response Variables</strong><br />{Response}<br /><br /><strong>Server Variables</strong><br />{Server}<br /><br /><strong>Session Variables</strong><br />{Session}<br /><br /><strong>Cookie Variables</strong><br />{Cookie}<br /><br /><strong>Cache Variables</strong><br />{Cache}<br /><br /><strong>Application Variables</strong><br />{Application}";
            const string normalTemplate = "Request Variables\r\n{Request}\r\n\r\nResponse Variables\r\n{Response}\r\n\r\nServer Variables\r\n{Server}\r\n\r\nSession Variables\r\n{Session}\r\n\r\nCookie Variables\r\n{Cookie}\r\n\r\nCache Variables\r\n{Cache}\r\n\r\nApplication Variables\r\n{Application}";
            var values = new[]{new KeyValuePair<string,string>("{Request}",context.Request.DumpRequestVariable(htmlOutput)),
                    new KeyValuePair<string,string>("{Response}",context.Response.DumpResponseVariable(htmlOutput)),
                    new KeyValuePair<string,string>("{Server}",context.Request.DumpServerVars(htmlOutput)),
                    new KeyValuePair<string,string>("{Session}",context.Session.DumpSession(htmlOutput)),
                    new KeyValuePair<string,string>("{Cookies}",context.Request.Cookies.DumpCookies(htmlOutput)),
                    new KeyValuePair<string,string>("{Cache}",context.Cache.DumpCache(htmlOutput)),
                    new KeyValuePair<string,string>("{Application}",context.Application.DumpApplicationState(htmlOutput))};
            return htmlOutput ? htmlTemplate.ToString(values) : normalTemplate.ToString(values);
        }

        #endregion

        #region DumpApplicationState

        /// <summary>
        /// Dumps the values found in the Application State
        /// </summary>
        /// <param name="page">Page in which to dump</param>
        /// <param name="htmlOutput">Should html output be used?</param>
        /// <returns>A string containing the application state information</returns>
        public static string DumpApplicationState(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Application.DumpApplicationState(htmlOutput);
        }

        /// <summary>
        /// Dumps the values found in the application state
        /// </summary>
        /// <param name="input">Application state variable</param>
        /// <param name="htmlOutput">Should html output be used?</param>
        /// <returns>A string containing the application state information</returns>
        public static string DumpApplicationState(this HttpApplicationState input, bool htmlOutput = false)
        {
            var String = new StringBuilder();
            foreach (string key in input.Keys)
            {
                String.Append(key).Append(": ")
                    .Append(input[key])
                    .Append(htmlOutput ? "<br />Properties<br />" : "\r\nProperties\r\n")
                    .Append(input[key].ToString(htmlOutput))
                    .Append(htmlOutput ? "<br />" : "\r\n");
            }
            return String.ToString();
        }

        #endregion

        #region DumpCache

        /// <summary>
        /// Dumps the values found in the cache
        /// </summary>
        /// <param name="page">Page in which to dump</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>A string containing the cache information</returns>
        public static string DumpCache(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Cache.DumpCache(htmlOutput);
        }

        /// <summary>
        /// Dumps the values found in the cache
        /// </summary>
        /// <param name="input">Cache variable</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>A string containing the cache information</returns>
        public static string DumpCache(this System.Web.Caching.Cache input, bool htmlOutput = false)
        {
            var String = new StringBuilder();
            foreach (DictionaryEntry entry in input)
            {
                String.Append(entry.Key).Append(": ")
                    .Append(entry.Value)
                    .Append(htmlOutput ? "<br />Properties<br />" : "\r\nProperties\r\n")
                    .Append(entry.Value.ToString(htmlOutput))
                    .Append(htmlOutput ? "<br />" : "\r\n");
            }
            return String.ToString();
        }

        #endregion

        #region DumpCookies

        /// <summary>
        /// Dumps the values found in the cookies sent by the user
        /// </summary>
        /// <param name="page">Page in which to dump</param>
        /// <param name="htmlOutput">Should html output be used</param>
        /// <returns>A string containing the cookie information</returns>
        public static string DumpCookies(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Request.Cookies.DumpCookies(htmlOutput);
        }

        /// <summary>
        /// Dumps the values found in the cookies sent by the user
        /// </summary>
        /// <param name="input">Cookies</param>
        /// <param name="htmlOutput">Should html output be used</param>
        /// <returns>A string containing the cookie information</returns>
        public static string DumpCookies(this HttpCookieCollection input, bool htmlOutput = false)
        {
            var String = new StringBuilder();
            String.Append(htmlOutput ? "<table><thead><tr><th>Name</th><th>Sub Name</th><th>Value</th></tr></thead><tbody>" : "Name\t\tSub Name\t\tValue\r\n");
            foreach (string key in input.Keys)
            {
                var httpCookie = input[key];
                if (httpCookie != null && httpCookie.Values.Count > 1)
                {
                    var cookie = input[key];
                    if (cookie != null)
                        foreach (string subKey in cookie.Values.Keys)
                        {
                            String.Append(htmlOutput ? "<tr><td>" : "").Append(key)
                                  .Append(htmlOutput ? "</td><td>" : "\t\t")
                                  .Append(subKey)
                                  .Append(htmlOutput ? "</td><td>" : "\t\t")
                                  .Append(cookie.Values[subKey])
                                  .Append(htmlOutput ? "</td></tr>" : "\r\n");
                        }
                }
                else
                {
                    var cookie = input[key];
                    if (cookie != null)
                        String.Append(htmlOutput ? "<tr><td>" : "").Append(key)
                              .Append(htmlOutput ? "</td><td>" : "\t\t")
                              .Append(htmlOutput ? "</td><td>" : "\t\t")
                              .Append(cookie.Value)
                              .Append(htmlOutput ? "</td></tr>" : "\r\n");
                }
            }
            String.Append(htmlOutput ? "</tbody></table>" : "\r\n");
            return String.ToString();
        }

        #endregion

        #region DumpRequestVariable

        /// <summary>
        /// Dumps information about the request variable
        /// </summary>
        /// <param name="request">Request to dump the information about</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>a string containing the information</returns>
        public static string DumpRequestVariable(this HttpRequest request, bool htmlOutput = false)
        {
            return request.ToString(htmlOutput);
        }

        /// <summary>
        /// Dumps information about the request variable
        /// </summary>
        /// <param name="page">Page to dump the information about</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>a string containing the information</returns>
        public static string DumpRequestVariable(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Request.ToString(htmlOutput);
        }

        #endregion

        #region DumpResponseVariable

        /// <summary>
        /// Dumps information about the response variable
        /// </summary>
        /// <param name="response">Response to dump the information about</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>a string containing the information</returns>
        public static string DumpResponseVariable(this HttpResponse response, bool htmlOutput = false)
        {
            return response.ToString(htmlOutput);
        }

        /// <summary>
        /// Dumps information about the response variable
        /// </summary>
        /// <param name="page">Page to dump the information about</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>a string containing the information</returns>
        public static string DumpResponseVariable(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Response.DumpResponseVariable(htmlOutput);
        }

        #endregion

        #region DumpServerVars

        /// <summary>
        /// Gets the server variables and dumps them out
        /// </summary>
        /// <param name="request">request to get server variables from</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>a string containing an HTML formatted list of the server variables</returns>
        public static string DumpServerVars(this HttpRequest request, bool htmlOutput = false)
        {
            var String = new StringBuilder();
            String.Append(htmlOutput ? "<table><thead><tr><th>Property Name</th><th>Value</th></tr></thead><tbody>" : "Property Name\t\tValue\r\n");
            foreach (string key in request.ServerVariables.Keys)
                String.Append(htmlOutput ? "<tr><td>" : "")
                        .Append(key)
                        .Append(htmlOutput ? "</td><td>" : "\t\t")
                        .Append(request.ServerVariables[key])
                        .Append(htmlOutput ? "</td></tr>" : "\r\n");
            String.Append(htmlOutput ? "</tbody></table>" : "\r\n");
            return String.ToString();
        }

        /// <summary>
        /// Gets the server variables and dumps them out
        /// </summary>
        /// <param name="page">page to get server variables from</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>A string containing an HTML formatted list of the server variables</returns>
        public static string DumpServerVars(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Request.DumpServerVars(htmlOutput);
        }

        #endregion

        #region DumpSession

        /// <summary>
        /// Dumps the values found in the session
        /// </summary>
        /// <param name="page">Page in which to dump</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>A string containing the session information</returns>
        public static string DumpSession(this System.Web.UI.Page page, bool htmlOutput = false)
        {
            return page.Session.DumpSession(htmlOutput);
        }

        /// <summary>
        /// Dumps the values found in the session
        /// </summary>
        /// <param name="input">Session variable</param>
        /// <param name="htmlOutput">Should HTML output be used</param>
        /// <returns>A string containing the session information</returns>
        public static string DumpSession(this System.Web.SessionState.HttpSessionState input, bool htmlOutput = false)
        {
            var String = new StringBuilder();
            foreach (string key in input.Keys)
            {
                String.Append(key).Append(": ")
                    .Append(input[key])
                    .Append(htmlOutput ? "<br />Properties<br />" : "\r\nProperties\r\n")
                    .Append(input[key].ToString(htmlOutput))
                    .Append(htmlOutput ? "<br />" : "\r\n");
            }
            return String.ToString();
        }

        #endregion

        #endregion
    }
}