﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

#endregion

namespace Operate.Web.ExtensionMethods
{
    /// <summary>
    /// HTTPResponse扩展方法
    /// </summary>
    public static class HttpResponseExtensions
    {
        /// <summary>
        /// 获取数据并转换为字符串(使用UTF8编码)
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static string GetDataToStringByUTF8Encoding(this HttpWebResponse response)
        {
            return response.GetDataToString(Encoding.UTF8);
        }

        /// <summary>
        /// 获取数据并转换为字符串(使用默认编码)
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static string GetDataToStringByDefaultEncoding(this HttpWebResponse response)
        {
            return response.GetDataToString(Encoding.Default);
        }

        /// <summary>
        /// 获取数据并转换为字符串
        /// </summary>
        /// <param name="response"></param>
        /// <param name="encoding">指定编码</param>
        /// <returns></returns>
        public static string GetDataToString(this HttpWebResponse response, Encoding encoding)
        {
            var data = response.GetData();
            var result = encoding.GetString(data);
            return result;
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static byte[] GetData(this HttpWebResponse response)
        {
            const int bufferSize = 15240;
            var responseHeaders = response.Headers;
            //SaveCookiesToDisk();

            var args = new DownloadEventArgs();
            if (responseHeaders[HttpResponseHeader.ContentLength] != null)
                args.TotalBytes = Convert.ToInt32(responseHeaders[HttpResponseHeader.ContentLength]);

            var ms = new MemoryStream();
            int count;
            var buf = new byte[bufferSize];
            using (Stream stream = response.GetResponseStream())
            {
                while (stream != null && (count = stream.Read(buf, 0, buf.Length)) > 0)
                {
                    ms.Write(buf, 0, count);
                }
            }
            //解压    
            if (responseHeaders[HttpResponseHeader.ContentEncoding] != null)
            {
                var msTemp = new MemoryStream();
                buf = new byte[100];
                switch (responseHeaders[HttpResponseHeader.ContentEncoding].ToLower())
                {
                    case "gzip":
                        var gzip = new GZipStream(ms, CompressionMode.Decompress);
                        while ((count = gzip.Read(buf, 0, buf.Length)) > 0)
                        {
                            msTemp.Write(buf, 0, count);
                        }
                        return msTemp.ToArray();
                    case "deflate":
                        var deflate = new DeflateStream(ms, CompressionMode.Decompress);
                        while ((count = deflate.Read(buf, 0, buf.Length)) > 0)
                        {
                            msTemp.Write(buf, 0, count);
                        }
                        return msTemp.ToArray();
                }
            }
            return ms.ToArray();
        }
    }
}
