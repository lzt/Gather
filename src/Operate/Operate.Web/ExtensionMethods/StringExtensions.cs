﻿using Operate.ExtensionMethods;
using Operate.Net.Mime;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Operate.Web.ExtensionMethods
{
    /// <summary>
    /// String扩展类
    /// </summary>
    public static class StringExtensions
    {
        #region Cookie

        /// <summary>
        /// 从字符串源中获取CookieCollection
        /// </summary>
        /// <param name="cookie"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static CookieContainer ConvertToCookieContainer(this string cookie, string domain)
        {
            return CookieOperate.CookieConvert.ToCookieContainer(cookie, domain);
        }

        #endregion Cookie

        #region URL

        /// <summary>
        /// URL编码
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string UrlEncode(this string url)
        {
            return System.Web.HttpUtility.UrlEncode(url);
        }

        /// <summary>
        /// URL解码
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string UrlDecode(this string url)
        {
            return System.Web.HttpUtility.UrlDecode(url);
        }

        /// <summary>
        /// 获取URL指定参数
        /// </summary>
        /// <param name="url">      todo: describe url parameter on GetUrlParam</param>
        /// <param name="paramName">todo: describe paramName parameter on GetUrlParam</param>
        public static string GetUrlParam(this string url, string paramName)
        {
            string baseurl;
            NameValueCollection nv;
            url.ParseUrl(out baseurl, out nv);
            return nv[paramName];
        }

        /// <summary>
        /// SetUrlParam
        /// </summary>
        /// <param name="url">      </param>
        /// <param name="urlParams"></param>
        /// <returns></returns>
        public static string SetUrlParam(this string url, NameValueCollection urlParams)
        {
            var u = new Uri(url);

            var baseUrl = u.Scheme + "://" + u.Host + (u.Port != 80 ? ":" + u.Port : "") + u.AbsolutePath;

            var nv = url.FormatParamToNameValueCollection();

            foreach (string key in urlParams.Keys)
            {
                var existKey = false;
                foreach (string alreadyKey in nv.Keys)
                {
                    if (alreadyKey == key)
                    {
                        nv[alreadyKey] = urlParams[key];
                        existKey = true;
                        break;
                    }
                }

                if (!existKey)
                {
                    nv[key] = urlParams[key];
                }
            }

            var result = baseUrl + "?";
            var builder = new StringBuilder();
            builder.Append(result);
            foreach (string k in nv.Keys)
            {
                var v = nv[k];
                builder.Append(k + "=" + HttpUtility.UrlEncode(v) + "&");
            }
            result = builder.ToString();

            result = result.Substring(0, result.Length - 1);

            return result;
        }

        /// <summary>
        /// 添加URL参数
        /// </summary>
        /// <param name="url">      todo: describe url parameter on AddUrlParam</param>
        /// <param name="urlParams">todo: describe urlParams parameter on AddUrlParam</param>
        public static string AddUrlParam(this string url, NameValueCollection urlParams)
        {
            return SetUrlParam(url, urlParams);
        }

        /// <summary>
        /// 添加URL参数
        /// </summary>
        /// <param name="url">      todo: describe url parameter on AddUrlParam</param>
        /// <param name="paramName">todo: describe paramName parameter on AddUrlParam</param>
        /// <param name="value">    todo: describe value parameter on AddUrlParam</param>
        public static string AddUrlParam(this string url, string paramName, string value)
        {
            var nv = new NameValueCollection();
            nv[paramName] = value;

            return AddUrlParam(url, nv);
        }

        /// <summary>
        /// UpdateUrlParam
        /// </summary>
        /// <param name="url">      </param>
        /// <param name="urlParams"></param>
        /// <returns></returns>
        public static string UpdateUrlParam(this string url, NameValueCollection urlParams)
        {
            return SetUrlParam(url, urlParams);
        }

        /// <summary>
        /// 更新URL参数
        /// </summary>
        /// <param name="url">      todo: describe url parameter on UpdateUrlParam</param>
        /// <param name="paramName">todo: describe paramName parameter on UpdateUrlParam</param>
        /// <param name="value">    todo: describe value parameter on UpdateUrlParam</param>
        public static string UpdateUrlParam(this string url, string paramName, string value)
        {
            var nv = new NameValueCollection();
            nv[paramName] = value;

            return AddUrlParam(url, nv);
        }

        /// <summary>
        /// </summary>
        /// <param name="url">       </param>
        /// <param name="paramNames"></param>
        /// <returns></returns>
        public static string RemoveUrlParam(this string url, ICollection<string> paramNames)
        {
            var u = new Uri(url);

            var baseUrl = u.Scheme + "://" + u.Host + (u.Port != 80 ? ":" + u.Port : "") + u.AbsolutePath;

            var nv = url.FormatParamToNameValueCollection();

            foreach (string p in paramNames)
            {
                foreach (string alreadyKey in nv.Keys)
                {
                    if (alreadyKey == p)
                    {
                        nv.Remove(alreadyKey);
                        break;
                    }
                }
            }

            var result = baseUrl + "?";
            var builder = new StringBuilder();
            builder.Append(result);
            foreach (string k in nv.Keys)
            {
                builder.Append(k + "=" + HttpUtility.UrlEncode(nv[k]) + "&");
            }
            result = builder.ToString();

            result = result.Substring(0, result.Length - 1);

            return result;
        }

        /// <summary>
        ///RemoveUrlParam
        /// </summary>
        /// <param name="url"></param>
        /// <param name="paramName"></param>
        /// <returns></returns>
        public static string RemoveUrlParam(this string url, string paramName)
        {
            var paramList = new List<string>() { paramName };

            return RemoveUrlParam(url, paramList);
        }

        /// <summary>
        /// 删除URL参数
        /// </summary>
        /// <param name="url">      todo: describe url parameter on DeleteUrlParam</param>
        /// <param name="paramName">todo: describe paramName parameter on DeleteUrlParam</param>
        public static string DeleteUrlParam(this string url, string paramName)
        {
            return RemoveUrlParam(url, paramName);
        }

        /// <summary>
        /// 分析URL所属的域
        /// </summary>
        /// <param name="fromUrl">  </param>
        /// <param name="domain">   </param>
        /// <param name="subDomain"></param>
        public static void GetUrlDomain(this string fromUrl, out string domain, out string subDomain)
        {
            try
            {
                var builder = new UriBuilder(fromUrl);
                fromUrl = builder.ToString();

                var u = new Uri(fromUrl);

                if (u.IsWellFormedOriginalString())
                {
                    if (u.IsFile)
                    {
                        subDomain = domain = "客户端本地文件路径";
                    }
                    else
                    {
                        string authority = u.Authority;
                        string[] ss = u.Authority.Split('.');
                        if (ss.Length == 2)
                        {
                            authority = "www." + authority;
                        }
                        int index = authority.IndexOf('.', 0);
                        domain = authority.Substring(index + 1, authority.Length - index - 1).Replace("comhttp", "com");
                        subDomain = authority.Replace("comhttp", "com");
                        if (ss.Length < 2)
                        {
                            domain = "不明路径";
                            subDomain = "不明路径";
                        }
                    }
                }
                else
                {
                    if (u.IsFile)
                    {
                        subDomain = domain = "客户端本地文件路径";
                    }
                    else
                    {
                        subDomain = domain = "不明路径";
                    }
                }
            }
            catch
            {
                subDomain = domain = "不明路径";
            }
        }

        /// <summary>
        /// FormatParamToNameValueCollection
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static NameValueCollection FormatParamToNameValueCollection(this string url)
        {
            NameValueCollection nv;
            string baseUrl;
            url.ParseUrl(out baseUrl, out nv);
            return nv;
        }

        /// <summary>
        /// 分析 url 字符串中的参数信息
        /// </summary>
        /// <param name="url">    输入的 URL</param>
        /// <param name="baseUrl">输出 URL 的基础部分</param>
        /// <param name="nvc">    输出分析后得到的 (参数名,参数值) 的集合</param>
        /// <param name="keyToLowerFirst"></param>
        public static void ParseUrl(this string url, out string baseUrl, out NameValueCollection nvc,bool keyToLowerFirst=true)
        {
            if (url.IsNullOrEmpty())
            {
                throw new ArgumentNullException("url");
            }

            nvc = new NameValueCollection();
            baseUrl = "";

            if (url == "")
            {
                return;
            }

            int questionMarkIndex = url.IndexOf('?');

            if (questionMarkIndex == -1)
            {
                baseUrl = url;
                return;
            }
            baseUrl = url.Substring(0, questionMarkIndex);
            if (questionMarkIndex == url.Length - 1)
            {
                return;
            }

            string ps = url.Substring(questionMarkIndex + 1);

            // 开始分析参数对
            var re = new Regex(@"(^|&)?(\w+)=([^&]+)(&|$)?", RegexOptions.Compiled);
            MatchCollection mc = re.Matches(ps);

            foreach (Match m in mc)
            {
                nvc.Add(keyToLowerFirst?m.Result("$2").ToLowerFirst(): m.Result("$2"), m.Result("$3"));
            }
        }

        #endregion URL

        #region 匹配页面的链接

        /// <summary>
        /// 获取页面的链接正则
        /// </summary>
        public static List<string> GetHref(this string source)
        {
            var result = new List<string>();
            const string reg = @"(h|H)(r|R)(e|E)(f|F) *= *('|"")?((\w|\\|\/|\.|:|-|_)+)[\S]*";
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Match m in Regex.Matches(source, reg))
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                result.Add((m.Value).ToLower().Replace("href=", "").Trim());
            }
            return result;
        }

        #endregion 匹配页面的链接

        #region 抓取远程页面内容

        /// <summary>
        /// 以GET方式抓取远程页面内容
        /// </summary>
        public static string GetHttp(this string url)
        {
            return url.GetHttp(Encoding.Default);
        }

        /// <summary>
        /// 以GET方式抓取远程页面内容
        /// </summary>
        public static string GetHttp(this string url, Encoding encoding)
        {
            if (!url.IsURL())
            {
                return "";
            }

            string strResult = "";
            try
            {
                var hwr = (HttpWebRequest)WebRequest.Create(url);
                hwr.Timeout = 19600;
                var hwrs = (HttpWebResponse)hwr.GetResponse();
                Stream myStream = hwrs.GetResponseStream();
                if (myStream != null)
                {
                    var sr = new StreamReader(myStream, encoding);
                    var sb = new StringBuilder();
                    while (-1 != sr.Peek())
                    {
                        sb.Append(sr.ReadLine() + "\r\n");
                    }
                    strResult = sb.ToString();
                }
                hwrs.Close();
            }
            catch (Exception ee)
            {
                strResult = ee.Message;
            }
            return strResult;
        }

        /// <summary>
        /// 以POST方式提交数据
        /// </summary>
        /// <param name="url">           </param>
        /// <param name="postData">      参数列表</param>
        /// <param name="dataEncodeType"></param>
        public static string PostHttp(this string url, string postData, string dataEncodeType = "utf-8")
        {
            return url.PostHttp(postData, Encoding.Default, dataEncodeType);
        }

        /// <summary>
        /// 以POST方式提交数据
        /// </summary>
        /// <param name="url">           </param>
        /// <param name="postData">      参数列表</param>
        /// <param name="encoding">      </param>
        /// <param name="dataEncodeType"></param>
        public static string PostHttp(this string url, string postData, Encoding encoding, string dataEncodeType = "utf-8")
        {
            return PostHttp(url, postData, encoding, MimeCollection.FormUrlencoded, dataEncodeType);
        }

        /// <summary>
        /// 以POST方式提交数据
        /// </summary>
        /// <param name="url">           </param>
        /// <param name="postData">      </param>
        /// <param name="encoding">      </param>
        /// <param name="mimeModel">     </param>
        /// <param name="dataEncodeType"></param>
        /// <returns></returns>
        public static string PostHttp(this string url, string postData, Encoding encoding, MimeModel mimeModel, string dataEncodeType = "utf-8")
        {
            if (!url.IsURL())
            {
                return "";
            }
            string strResult = null;
            try
            {
                Encoding dataEncoding = Encoding.GetEncoding(dataEncodeType);
                byte[] post = dataEncoding.GetBytes(postData);
                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = "POST";
                //myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentType = mimeModel.ContentType;
                myRequest.ContentLength = post.Length;
                Stream newStream = myRequest.GetRequestStream();
                newStream.Write(post, 0, post.Length); //设置POST
                newStream.Flush();
                newStream.Close();
                var myResponse = (HttpWebResponse)myRequest.GetResponse();
                var stream = myResponse.GetResponseStream();
                if (stream != null)
                {
                    var reader = new StreamReader(stream, encoding);
                    strResult = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }
            return strResult;
        }

        #endregion 抓取远程页面内容

        #region 下载

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="url">           文件URL地址</param>
        /// <param name="cc">            </param>
        /// <param name="requestHeaders"></param>
        /// <param name="proxy">         </param>
        public static MemoryStream Download(this string url, CookieContainer cc = null, WebHeaderCollection requestHeaders = null, WebProxy proxy = null)
        {
            if (!url.IsURL())
            {
                return null;
            }
            HttpWebRequest request = url.CreateRequest("GET", cc, requestHeaders, proxy);
            byte[] data = GetData(request);
            var result = new MemoryStream(data);
            return result;
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="url">           文件URL地址</param>
        /// <param name="savePath">      文件保存完整路径</param>
        /// <param name="cc">            </param>
        /// <param name="requestHeaders"></param>
        /// <param name="proxy">         </param>
        public static void Download(this string url, string savePath, CookieContainer cc = null, WebHeaderCollection requestHeaders = null, WebProxy proxy = null)
        {
            if (!url.IsURL())
            {
                return;
            }

            FileStream fs = null;
            try
            {
                HttpWebRequest request = url.CreateRequest("GET", cc, requestHeaders, proxy);
                byte[] data = GetData(request);
                fs = new FileStream(savePath, FileMode.Create, FileAccess.Write);
                fs.Write(data, 0, data.Length);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        #endregion 下载

        /// <summary>
        /// 创建HTTP请求
        /// </summary>
        /// <param name="url">           URL地址</param>
        /// <param name="method">        </param>
        /// <param name="proxy">         </param>
        /// <param name="cc">            </param>
        /// <param name="requestHeaders"></param>
        /// <returns></returns>
        private static HttpWebRequest CreateRequest(this string url, string method, CookieContainer cc = null, WebHeaderCollection requestHeaders = null, WebProxy proxy = null)
        {
            var uri = new Uri(url);

            if (uri.Scheme == "https")
            {
                ServicePointManager.ServerCertificateValidationCallback = CheckValidationResult;
            }

            // Set a default policy level for the "http:" and "https" schemes.
            var policy = new HttpRequestCachePolicy(HttpRequestCacheLevel.Revalidate);
            HttpWebRequest.DefaultCachePolicy = policy;

            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.AllowAutoRedirect = false;
            request.AllowWriteStreamBuffering = false;
            request.Method = method;
            if (proxy != null)
            {
                request.Proxy = proxy;
            }

            request.CookieContainer = cc;
            if (requestHeaders != null)
            {
                foreach (string key in requestHeaders.Keys)
                {
                    request.Headers.Add(key, requestHeaders[key]);
                }
                requestHeaders.Clear();
            }
            return request;
        }

        /// <summary>
        /// 读取请求返回的数据
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <returns></returns>
        private static byte[] GetData(this HttpWebRequest request)
        {
            return ((HttpWebResponse)request.GetResponse()).GetData();
        }

        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }
    }

    /// <summary>
    /// 下载数据参数
    /// </summary>
    public class DownloadEventArgs : System.EventArgs
    {
        /// <summary>
        /// 已接收的字节数
        /// </summary>
        public int BytesReceived { get; set; }

        /// <summary>
        /// 总字节数
        /// </summary>
        public int TotalBytes { get; set; }

        /// <summary>
        /// 当前缓冲区接收的数据
        /// </summary>
        public byte[] ReceivedData { get; set; }
    }
}