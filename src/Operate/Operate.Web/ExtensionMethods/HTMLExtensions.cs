﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;

using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Operate.Compression.ExtensionMethods.Enums;
using Operate.ExtensionMethods;
using Operate.IO.ExtensionMethods;
using Operate.Web.ExtensionMethods.Streams;
#endregion

namespace Operate.Web.ExtensionMethods
{
    /// <summary>
    /// Set of HTML related extensions (and HTTP related)
    /// HTML 和 HTTP 相关的扩展
    /// </summary>
    public static class HtmlExtensions
    {
        #region AbsoluteRoot

        /// <summary>
        /// Returns the absolute root
        /// 返回绝对路径
        /// </summary>
        public static Uri AbsoluteRoot(this HttpContextBase context)
        {
            if (context.IsNull()) { throw new ArgumentNullException("context"); }
            if (context.Items["absoluteurl"] == null)
                if (context.Request.Url != null)
                    context.Items["absoluteurl"] = new Uri(context.Request.Url.GetLeftPart(UriPartial.Authority) + context.RelativeRoot());
            return context.Items["absoluteurl"] as Uri;
        }

        /// <summary>
        /// Returns the absolute root
        /// 返回绝对路径
        /// </summary>
        public static Uri AbsoluteRoot(this HttpContext context)
        {
             if (context.IsNull()) { throw new ArgumentNullException("context"); }
            if (context.Items["absoluteurl"] == null)
                context.Items["absoluteurl"] = new Uri(context.Request.Url.GetLeftPart(UriPartial.Authority) + context.RelativeRoot());
            return context.Items["absoluteurl"] as Uri;
        }


        #endregion

        #region AddScriptFile

        /// <summary>
        /// Adds a script file to the header of the current page
        /// 向当前页面头部添加脚本文件
        /// </summary>
        /// <param name="file">Script file 脚本文件</param>
        /// <param name="page">Page to add it to 目标页面</param>
        public static void AddScriptFile(this System.Web.UI.Page page, FileInfo file)
        {
           if (file.IsNull()) { throw new ArgumentNullException("file"); }
             if (!file.Exists) { throw new FileNotFoundException("file does not exist"); }
            if (!page.ClientScript.IsClientScriptIncludeRegistered(typeof(System.Web.UI.Page), file.FullName))
                page.ClientScript.RegisterClientScriptInclude(typeof(System.Web.UI.Page), file.FullName, file.FullName);
        }

        #endregion

        #region HTTPCompress

        /// <summary>
        /// Adds HTTP compression to the current context
        /// </summary>
        /// <param name="context">Current context</param>
        /// <param name="removePrettyPrinting">Sets the response filter to a special stream that
        /// removes pretty printing from content</param>
        /// <param name="type">The minification type to use (defaults to HTML if RemovePrettyPrinting 
        /// is set to true, but can also deal with CSS and Javascript)</param>
        public static void HttpCompress(this HttpContextBase context, bool removePrettyPrinting = false, MinificationType type = MinificationType.Html)
        {
             if (context.IsNull()) { throw new ArgumentNullException("context"); }
            if (context.Request.UserAgent != null && context.Request.UserAgent.Contains("MSIE 6"))
                return;
            if (removePrettyPrinting)
            {
                if (context.IsEncodingAccepted(Gzip))
                {
                    context.Response.Filter = new UglyStream(context.Response.Filter, CompressionType.GZip, type);
                    context.SetEncoding(Gzip);
                }
                else if (context.IsEncodingAccepted(Deflate))
                {
                    context.Response.Filter = new UglyStream(context.Response.Filter, CompressionType.Deflate, type);
                    context.SetEncoding(Deflate);
                }
            }
            else
            {
                if (context.IsEncodingAccepted(Gzip))
                {
                    context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
                    context.SetEncoding(Gzip);
                }
                else if (context.IsEncodingAccepted(Deflate))
                {
                    context.Response.Filter = new DeflateStream(context.Response.Filter, CompressionMode.Compress);
                    context.SetEncoding(Deflate);
                }
            }
        }

        /// <summary>
        /// Adds HTTP compression to the current context
        /// </summary>
        /// <param name="context">Current context</param>
        /// <param name="removePrettyPrinting">Sets the response filter to a special stream that
        /// removes pretty printing from content</param>
        /// <param name="type">The minification type to use (defaults to HTML if RemovePrettyPrinting 
        /// is set to true, but can also deal with CSS and Javascript)</param>
        public static void HttpCompress(this HttpContext context, bool removePrettyPrinting = false, MinificationType type = MinificationType.Html)
        {
             if (context.IsNull()) { throw new ArgumentNullException("context"); }
            if (context.Request.UserAgent != null && context.Request.UserAgent.Contains("MSIE 6"))
                return;
            if (removePrettyPrinting)
            {
                if (context.IsEncodingAccepted(Gzip))
                {
                    context.Response.Filter = new UglyStream(context.Response.Filter, CompressionType.GZip, type);
                    context.SetEncoding(Gzip);
                }
                else if (context.IsEncodingAccepted(Deflate))
                {
                    context.Response.Filter = new UglyStream(context.Response.Filter, CompressionType.Deflate, type);
                    context.SetEncoding(Deflate);
                }
            }
            else
            {
                if (context.IsEncodingAccepted(Gzip))
                {
                    context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
                    context.SetEncoding(Gzip);
                }
                else if (context.IsEncodingAccepted(Deflate))
                {
                    context.Response.Filter = new DeflateStream(context.Response.Filter, CompressionMode.Compress);
                    context.SetEncoding(Deflate);
                }
            }
        }

        #endregion

        #region IsEncodingAccepted

        /// <summary>
        /// Checks the request headers to see if the specified
        /// encoding is accepted by the client.
        /// </summary>
        public static bool IsEncodingAccepted(this HttpContextBase context, string encoding)
        {
            if (context == null)
                return false;
            return context.Request.Headers["Accept-encoding"] != null && context.Request.Headers["Accept-encoding"].Contains(encoding);
        }

        /// <summary>
        /// Checks the request headers to see if the specified
        /// encoding is accepted by the client.
        /// </summary>
        public static bool IsEncodingAccepted(this HttpContext context, string encoding)
        {
            if (context == null)
                return false;
            return context.Request.Headers["Accept-encoding"] != null && context.Request.Headers["Accept-encoding"].Contains(encoding);
        }

        #endregion

        #region RelativeRoot

        /// <summary>
        /// Gets the relative root of the web site
        /// </summary>
        /// <param name="context">Current context</param>
        /// <returns>The relative root of the web site</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Context")]
        public static string RelativeRoot(this HttpContextBase context)
        {
            return VirtualPathUtility.ToAbsolute("~/");
        }

        /// <summary>
        /// Gets the relative root of the web site
        /// </summary>
        /// <param name="context">Current context</param>
        /// <returns>The relative root of the web site</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Context")]
        public static string RelativeRoot(this HttpContext context)
        {
            return VirtualPathUtility.ToAbsolute("~/");
        }
        
        #endregion

        #region RemoveURLIllegalCharacters

        /// <summary>
        /// Removes illegal characters (used in uri's, etc.)
        /// </summary>
        /// <param name="input">string to be converted</param>
        /// <returns>A stripped string</returns>
        public static string RemoveURLIllegalCharacters(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            input = input.Replace(":", string.Empty)
                        .Replace("/", string.Empty)
                        .Replace("?", string.Empty)
                        .Replace("#", string.Empty)
                        .Replace("[", string.Empty)
                        .Replace("]", string.Empty)
                        .Replace("@", string.Empty)
                        .Replace(".", string.Empty)
                        .Replace("\"", string.Empty)
                        .Replace("&", string.Empty)
                        .Replace("'", string.Empty)
                        .Replace(" ", "-");
            input = RemoveExtraHyphen(input);
            input = RemoveDiacritics(input);
            return input.URLEncode().Replace("%", string.Empty);
        }

        #endregion

        #region SetEncoding

        /// <summary>
        /// Adds the specified encoding to the response headers.
        /// </summary>
        /// <param name="encoding">Encoding to set</param>
        /// <param name="context">Context to set the encoding on</param>
        public static void SetEncoding(this HttpContextBase context, string encoding)
        {
             if (context.IsNull()) { throw new ArgumentNullException("context"); }
            context.Response.AppendHeader("Content-encoding", encoding);
        }

        /// <summary>
        /// Adds the specified encoding to the response headers.
        /// </summary>
        /// <param name="encoding">Encoding to set</param>
        /// <param name="context">Context to set the encoding on</param>
        public static void SetEncoding(this HttpContext context, string encoding)
        {
             if (context.IsNull()) { throw new ArgumentNullException("context"); }
            context.Response.AppendHeader("Content-encoding", encoding);
        }

        #endregion

        #region ContainsHTML

        /// <summary>
        /// Decides if the file contains HTML
        /// 是否包含HTML字符串
        /// </summary>
        /// <param name="input">Input file to check 检测源</param>
        /// <returns>false if it does not contain HTML, true otherwise</returns>
        public static bool ContainsHtml(this FileInfo input)
        {
            if (input.IsNull()) { throw new ArgumentNullException("input"); }
            return input.Exists && input.Read().ContainsHtml();
        }

        #endregion

        #region StripHTML

        /// <summary>
        /// Removes HTML elements from a string
        /// </summary>
        /// <param name="html">HTML laiden file</param>
        /// <returns>HTML-less string</returns>
        public static string StripHtml(this FileInfo html)
        {
            if (html.IsNull()) { throw new ArgumentNullException("html"); }
            if (!html.Exists) { throw new FileNotFoundException("File does not exist"); }
            return html.Read().StripHtml();
        }

        #endregion

        #region URLDecode

        /// <summary>
        /// URL decodes a string
        /// </summary>
        /// <param name="input">Input to decode</param>
        /// <returns>A decoded string</returns>
        public static string URLDecode(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            return System.Web.HttpUtility.UrlDecode(input);
        }

        #endregion

        #region URLEncode

        /// <summary>
        /// URL encodes a string
        /// </summary>
        /// <param name="input">Input to encode</param>
        /// <returns>An encoded string</returns>
        public static string URLEncode(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            return System.Web.HttpUtility.UrlEncode(input);
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Removes extra hyphens from a string
        /// </summary>
        /// <param name="input">string to be stripped</param>
        /// <returns>Stripped string</returns>
        private static string RemoveExtraHyphen(string input)
        {
            return new Regex(@"[-]{2,}", RegexOptions.None).Replace(input, "-");
        }

        /// <summary>
        /// Removes special characters (Diacritics) from the string
        /// </summary>
        /// <param name="input">String to strip</param>
        /// <returns>Stripped string</returns>
        private static string RemoveDiacritics(string input)
        {
            string normalized = input.Normalize(NormalizationForm.FormD);
            var builder = new StringBuilder();
            foreach (char tempChar in normalized)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(tempChar) != UnicodeCategory.NonSpacingMark)
                    builder.Append(tempChar);
            }
            return builder.ToString();
        }

        #endregion

        #region Constants
        private const string Gzip = "gzip";
        private const string Deflate = "deflate";
        #endregion
    }
}