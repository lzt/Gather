﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Operate.IO.ExtensionMethods;
#endregion

namespace Operate.Web.ExtensionMethods
{
    /// <summary>
    /// Extensions dealing with minification of data
    /// </summary>
    public static class Minification
    {
        #region Combine

        /// <summary>
        /// Combines and minifies various files
        /// </summary>
        /// <param name="input">input strings (file contents)</param>
        /// <param name="type">Type of minification</param>
        /// <returns>A minified/packed string</returns>
        public static string Combine(this IEnumerable<string> input, MinificationType type = MinificationType.Html)
        {
            var output = new StringBuilder();
            foreach (string temp in input)
                output.Append(temp).Append("\n");
            return Minify(output.ToString(), type);
        }

        /// <summary>
        /// Combines and minifies various files
        /// </summary>
        /// <param name="input">input strings (file contents)</param>
        /// <param name="type">Type of minification</param>
        /// <returns>A minified/packed string</returns>
        public static string Combine(this IEnumerable<FileInfo> input, MinificationType type = MinificationType.Html)
        {
            var output = new StringBuilder();
            foreach (FileInfo temp in input.Where(x => x.Exists))
                output.Append(temp.Read()).Append("\n");
            return Minify(output.ToString(), type);
        }

        #endregion

        #region Minify

        /// <summary>
        /// Minifies the file based on the data type specified
        /// </summary>
        /// <param name="input">Input text</param>
        /// <param name="type">Type of minification to run</param>
        /// <returns>A stripped file</returns>
        public static string Minify(this string input, MinificationType type = MinificationType.Html)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            if (type == MinificationType.Css)
                return CssMinify(input);
            if (type == MinificationType.JavaScript)
                return JavaScriptMinify(input);
            return HtmlMinify(input);
        }

        /// <summary>
        /// Minifies the file based on the data type specified
        /// </summary>
        /// <param name="input">Input file</param>
        /// <param name="type">Type of minification to run</param>
        /// <returns>A stripped file</returns>
        public static string Minify(this FileInfo input, MinificationType type = MinificationType.Html)
        {
            if (input.IsNull()) { throw new ArgumentNullException("input"); }
            if (!input.Exists) { throw new FileNotFoundException("Input file does not exist"); }
            return input.Read().Minify(type);
        }

        #endregion

        #region Private Functions

        private static string HtmlMinify(string input)
        {
            input = Regex.Replace(input, "/// <.+>", "");
            if (string.IsNullOrEmpty(input))
                return "";
            input = Regex.Replace(input, @">[\s\S]*?<", Evaluate);
            return input;
        }

        private static string Evaluate(Match matcher)
        {
            string myString = matcher.ToString();
            if (string.IsNullOrEmpty(myString))
                return "";
            myString = Regex.Replace(myString, @"\r\n\s*", "");
            return myString;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        private static string JavaScriptMinify(string input)
        {
            string[] codeLines = input.Split(new[] { Environment.NewLine, "\n" }, StringSplitOptions.RemoveEmptyEntries);
            var builder = new StringBuilder();
            foreach (string line in codeLines)
            {
                string temp = line.Trim();
                if (temp.Length > 0 && !temp.StartsWith("//", StringComparison.InvariantCulture))
                    builder.AppendLine(temp);
            }

            input = builder.ToString();
            input = Regex.Replace(input, @"(/\*\*/)|(/\*[^!][\s\S]*?\*/)", string.Empty);
            input = Regex.Replace(input, @"^[\s]+|[ \f\r\t\v]+$", String.Empty);
            input = Regex.Replace(input, @"^[\s]+|[ \f\r\t\v]+$", String.Empty);
            input = Regex.Replace(input, @"([+-])\n\1", "$1 $1");
            input = Regex.Replace(input, @"([^+-][+-])\n", "$1");
            input = Regex.Replace(input, @"([^+]) ?(\+)", "$1$2");
            input = Regex.Replace(input, @"(\+) ?([^+])", "$1$2");
            input = Regex.Replace(input, @"([^-]) ?(\-)", "$1$2");
            input = Regex.Replace(input, @"(\-) ?([^-])", "$1$2");
            input = Regex.Replace(input, @"\n([{}()[\],<>/*%&|^!~?:=.;+-])", "$1");
            input = Regex.Replace(input, @"(\W(if|while|for)\([^{]*?\))\n", "$1");
            input = Regex.Replace(input, @"(\W(if|while|for)\([^{]*?\))((if|while|for)\([^{]*?\))\n", "$1$3");
            input = Regex.Replace(input, @"([;}]else)\n", "$1 ");
            input = Regex.Replace(input, @"(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,}(?=&nbsp;)|(?<=&nbsp;)\s{2,}(?=[<])", String.Empty);

            return input;
        }

        private static string CssMinify(string input)
        {
            input = Regex.Replace(input, @"(/\*\*/)|(/\*[^!][\s\S]*?\*/)", string.Empty);
            input = Regex.Replace(input, @"\s+", " ");
            input = Regex.Replace(input, @"(\s([\{:,;\}\(\)]))", "$2");
            input = Regex.Replace(input, @"(([\{:,;\}\(\)])\s)", "$2");
            input = Regex.Replace(input, ":0 0 0 0;", ":0;");
            input = Regex.Replace(input, ":0 0 0;", ":0;");
            input = Regex.Replace(input, ":0 0;", ":0;");
            input = Regex.Replace(input, ";}", "}");
            input = Regex.Replace(input, @"(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,}(?=&nbsp;)|(?<=&nbsp;)\s{2,}(?=[<])", string.Empty);
            input = Regex.Replace(input, @"([!{}:;>+([,])\s+", "$1");
            input = Regex.Replace(input, @"([\s:])(0)(px|em|%|in|cm|mm|pc|pt|ex)", "$1$2");
            input = Regex.Replace(input, "background-position:0", "background-position:0 0");
            input = Regex.Replace(input, @"(:|\s)0+\.(\d+)", "$1.$2");
            input = Regex.Replace(input, @"[^\}]+\{;\}", "");
            return input;
        }

        #endregion
    }

    #region Enums

    /// <summary>
    /// Defines the type of data that is being minified
    /// </summary>
    public enum MinificationType
    {
        /// <summary>
        /// CSS
        /// </summary>
        Css,
        /// <summary>
        /// Javascript
        /// </summary>
        JavaScript,
        /// <summary>
        /// HTML
        /// </summary>
        Html
    }

    #endregion
}