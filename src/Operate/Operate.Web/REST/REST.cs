﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using Operate.ExtensionMethods;
using Operate.Web.ExtensionMethods;

#endregion

namespace Operate.Web.REST
{
    /// <summary>
    /// Class designed to help with calling REST based applications
    /// </summary>
    public class REST
    {
        #region Constructor

        #endregion

        #region Private Functions

        private void SetCookie(ref HttpWebRequest request, CookieContainer cookieContainer)
        {
            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }
            else
            {
                if (!Cookies.IsNullOrEmpty()&& !Domain.IsNullOrEmpty())
                {
                    request.CookieContainer = Cookies.ConvertToCookieContainer(Domain);
                }
            }
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Does a GET to the REST service
        /// </summary>
        /// <returns>a string containing the data returned by the service</returns>
        public virtual string GET(CookieContainer cookieContainer = null)
        {
            var request = WebRequest.Create(Url) as HttpWebRequest;
            if (request != null)
            {
                request.Method = "GET";
                request.ContentType = "text/xml";
                SetupData(request);
                SetupCredentials(request);
                //SetCookie(ref request, cookieContainer);
                return SendRequest(request);
            }
            return null;
        }

        /// <summary>
        /// Does a POST to the REST service
        /// </summary>
        /// <returns>a string containing the data returned by the service</returns>
        public virtual string POST(CookieContainer cookieContainer=null)
        {
            var request = WebRequest.Create(Url) as HttpWebRequest;
            if (request != null)
            {
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                SetupData(request);
                SetupCredentials(request);
                //SetCookie(ref request, cookieContainer);
                return SendRequest(request);
            }
            return null;
        }

        /// <summary>
        /// Does a DELETE on the REST service
        /// </summary>
        /// <returns>a string containing the data returned by the service</returns>
        public virtual string DELETE(CookieContainer cookieContainer = null)
        {
            var request = WebRequest.Create(Url) as HttpWebRequest;
            if (request != null)
            {
                request.Method = "DELETE";
                request.ContentType = "application/x-www-form-urlencoded";
                SetupData(request);
                SetupCredentials(request);
                //SetCookie(ref request, cookieContainer);
                return SendRequest(request);
            }
            return null;
        }

        /// <summary>
        /// Does a PUT on the REST service
        /// </summary>
        /// <returns>a string containing the data returned by the service</returns>
        public virtual string PUT(CookieContainer cookieContainer = null)
        {
            var request = WebRequest.Create(Url) as HttpWebRequest;
            if (request != null)
            {
                request.Method = "PUT";
                request.ContentType = "application/x-www-form-urlencoded";
                SetupData(request);
                SetupCredentials(request);
                //SetCookie(ref request, cookieContainer);
                return SendRequest(request);
            }
            return null;
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Sets up any data that needs to be sent
        /// </summary>
        /// <param name="request">The web request object</param>
        private void SetupData(HttpWebRequest request)
        {
            if (string.IsNullOrEmpty(Data))
            {
                request.ContentLength = 0;
                return;
            }
            byte[] byteData = Encoding.UTF8.GetBytes(Data);
            request.ContentLength = byteData.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(byteData, 0, byteData.Length);
            }
        }

        /// <summary>
        /// Sets up any credentials (basic authentication,
        /// for OAuth, please use the OAuth class to create the
        /// URL)
        /// </summary>
        /// <param name="request">The web request object</param>
        private void SetupCredentials(HttpWebRequest request)
        {
            if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
            {
                request.Credentials = new NetworkCredential(UserName, Password);
            }
        }

        /// <summary>
        /// Sends the request to the URL specified
        /// </summary>
        /// <param name="request">The web request object</param>
        /// <returns>The string returned by the service</returns>
        private static string SendRequest(HttpWebRequest request)
        {
            using (var response = request.GetResponse() as HttpWebResponse)
            {
                if (response != null && response.StatusCode != HttpStatusCode.OK)
                    throw new HttpException("The request did not complete successfully and returned status code " + response.StatusCode);
                Stream stream = null;
                if (response != null)
                {
                     stream = response.GetResponseStream();
                }
                if (stream != null)
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                return null;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// URL to send the request to
        /// </summary>
        public virtual string Cookies { get; set; }

        /// <summary>
        /// URL to send the request to
        /// </summary>
        public virtual string Domain { get; set; }

        /// <summary>
        /// URL to send the request to
        /// </summary>
        public virtual Uri Url { get; set; }

        /// <summary>
        /// Any data that needs to be appended to the request
        /// </summary>
        public virtual string Data { get; set; }

        /// <summary>
        /// User name (basic authentication)
        /// </summary>
        public virtual string UserName { get; set; }

        /// <summary>
        /// Password (basic authentication
        /// </summary>
        public virtual string Password { get; set; }

        #endregion
    }
}