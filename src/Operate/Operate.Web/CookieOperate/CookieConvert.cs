﻿using System;
using System.Net;

namespace Operate.Web.CookieOperate
{
    /// <summary>
    /// Cookie转换类
    /// </summary>
    public class CookieConvert
    {
        /// <summary>
        /// 从字符串源中获取CookieCollection
        /// </summary>
        /// <param name="cookie"></param>
        /// <param name="domain"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public static CookieContainer ToCookieContainer(string cookie, string domain, CookieContainer container = null)
        {
            if (container == null)
            {
                container = new CookieContainer();
            }

            string[] tempCookies = cookie.Split(';');
            //qg.gome.com.cn  cookie 
            foreach (string t in tempCookies)
            {
                if (!string.IsNullOrEmpty(t))
                {
                    string tempCookie = t;

                    int equallength = tempCookie.IndexOf("=", StringComparison.Ordinal);//  =的位置 

                    string cookieKey;
                    string cookieValue;
                    if (equallength != -1)       //有可能cookie 无=，就直接一个cookiename；比如:a=3;ck;abc=; 
                    {
                        cookieKey = tempCookie.Substring(0, equallength).Trim();
                        //cookie=

                        cookieValue = equallength == tempCookie.Length - 1 ? "" : tempCookie.Substring(equallength + 1, tempCookie.Length - equallength - 1).Trim();
                    }

                    else
                    {
                        cookieKey = tempCookie.Trim();
                        cookieValue = "";
                    }

                    container.Add(new Cookie(cookieKey, cookieValue, "", domain));

                }
            }

            return container;
        }

        ///// <summary>
        ///// 从字符串源中获取CookieCollection
        ///// </summary>
        ///// <param name="header"></param>
        ///// <param name="host"></param>
        ///// <returns></returns>
        //public static CookieCollection GetAllCookiesFromHeader(string header, string host)
        //{
        //    var cc = new CookieCollection();
        //    if (header != string.Empty)
        //    {
        //        ArrayList al = ConvertCookieHeaderToArrayList(header);
        //        cc = ConvertCookieArraysToCookieCollection(al, host);
        //    }
        //    return cc;
        //}

        //private static ArrayList ConvertCookieHeaderToArrayList(string strCookHeader)
        //{
        //    strCookHeader = strCookHeader.Replace("\r", "");
        //    strCookHeader = strCookHeader.Replace("\n", "");
        //    string[] strCookTemp = strCookHeader.Split(',');
        //    var al = new ArrayList();
        //    int i = 0;
        //    int n = strCookTemp.Length;
        //    while (i < n)
        //    {
        //        if (strCookTemp[i].IndexOf("expires=", StringComparison.OrdinalIgnoreCase) > 0)
        //        {
        //            al.Add(strCookTemp[i] + "," + strCookTemp[i + 1]);
        //            i = i + 1;
        //        }
        //        else
        //        {
        //            al.Add(strCookTemp[i]);
        //        }
        //        i = i + 1;
        //    }
        //    return al;
        //}

        //private static CookieCollection ConvertCookieArraysToCookieCollection(ArrayList al, string strHost)
        //{
        //    var cc = new CookieCollection();

        //    int alcount = al.Count;
        //    for (int i = 0; i < alcount; i++)
        //    {
        //        string eachCook = al[i].ToString();
        //        string[] eachCookParts = eachCook.Split(';');
        //        int intEachCookPartsCount = eachCookParts.Length;
        //        var cookTemp = new Cookie();

        //        for (int j = 0; j < intEachCookPartsCount; j++)
        //        {
        //            if (j == 0)
        //            {
        //                string nameAndCValue = eachCookParts[j];
        //                if (nameAndCValue != string.Empty)
        //                {
        //                    int firstEqual = nameAndCValue.IndexOf("=", StringComparison.Ordinal);
        //                    string firstName = nameAndCValue.Substring(0, firstEqual);
        //                    string allValue = nameAndCValue.Substring(firstEqual + 1, nameAndCValue.Length - (firstEqual + 1));
        //                    cookTemp.Name = firstName;
        //                    cookTemp.Value = allValue;
        //                }
        //                continue;
        //            }
        //            string pNameAndPValue;
        //            string[] nameValuePairTemp;
        //            if (eachCookParts[j].IndexOf("path", StringComparison.OrdinalIgnoreCase) >= 0)
        //            {
        //                pNameAndPValue = eachCookParts[j];
        //                if (pNameAndPValue != string.Empty)
        //                {
        //                    nameValuePairTemp = pNameAndPValue.Split('=');
        //                    cookTemp.Path = nameValuePairTemp[1] != string.Empty ? nameValuePairTemp[1] : "/";
        //                }
        //                continue;
        //            }

        //            if (eachCookParts[j].IndexOf("domain", StringComparison.OrdinalIgnoreCase) >= 0)
        //            {
        //                pNameAndPValue = eachCookParts[j];
        //                if (pNameAndPValue != string.Empty)
        //                {
        //                    nameValuePairTemp = pNameAndPValue.Split('=');

        //                    cookTemp.Domain = nameValuePairTemp[1] != string.Empty ? nameValuePairTemp[1] : strHost;
        //                }
        //            }
        //        }

        //        if (cookTemp.Path == string.Empty)
        //        {
        //            cookTemp.Path = "/";
        //        }
        //        if (cookTemp.Domain == string.Empty)
        //        {
        //            cookTemp.Domain = strHost;
        //        }
        //        cc.Add(cookTemp);
        //    }
        //    return cc;
        //}
    }
}
