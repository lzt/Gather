﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Operate.Web.Client
{
    /// <summary>
    /// 客户端请求模块
    /// </summary>
    public class ClientRequest
    {
        /// <summary>
        /// 私有化构造函数
        /// </summary>
        private ClientRequest() { }

        #region Get方式调用接口

        /// <summary>
        /// 发起Http/Get请求并获取数据
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="headers">请求头</param>
        public static string Get(string url, IDictionary<string, string> headers = null)
        {
            var client = new HttpClient();

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            return client.GetStringAsync(url).Result;
        }

        /// <summary>
        /// get方式调用接口返回字符串
        /// </summary>
        /// <param name="url"></param>
        /// <param name="encodeing">默认UTF8</param>
        /// <returns></returns>
        public static string Get(string url, string encodeing = "utf-8")
        {
            var httpclient = new LiteHttpClient(Encoding.GetEncoding(encodeing));
            HttpWebRequest request = DefaultRequest.FromString(url);

            return httpclient.GetString(request);
        }

        #endregion

        #region Post方式调用接口

        /// <summary>
        /// Post请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameter"></param>
        /// <param name="isjson"></param>
        /// <param name="encodeing"></param>
        /// <returns></returns>
        public static string Post(string url, object parameter, bool isjson = true, string encodeing = "utf-8")
        {
            var httpclient = new LiteHttpClient(Encoding.GetEncoding(encodeing));
            HttpWebRequest request = DefaultRequest.FromString(url);

            string data;
            if (parameter is string)
            {
                data = parameter.ToString();
                request.ContentType = isjson ? "application/json" : "application/x-www-form-urlencoded";
            }
            else
            {
                request.ContentType = "application/json";
                data = JsonConvert.SerializeObject(parameter);
            }

            return httpclient.PostString(request, data);
        }

        /// <summary>
        /// 发起Http/Post请求并获取数据
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="postData">Post数据</param>
        /// <param name="headers">请求头</param>
        public static string Post(string url, IDictionary<string, string> postData, IDictionary<string, string> headers = null)
        {
            var client = new HttpClient();

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }

            }

            return client.PostAsync(url, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
        }

        #endregion
    }
}