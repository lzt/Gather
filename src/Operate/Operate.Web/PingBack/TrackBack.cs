﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;

using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

#endregion

namespace Operate.Web.PingBack
{
    /// <summary>
    /// Handles track backs
    /// </summary>
    public static class TrackBack
    {
        #region Public Static Functions

        /// <summary>
        /// Sends a track back message
        /// </summary>
        /// <param name="message">Message to send</param>
        public static void SendTrackBack(TrackBackMessage message)
        {
            if (message.IsNull()) { throw new ArgumentNullException("message"); }
            GetTrackBackURL(message);
            var request = (HttpWebRequest)WebRequest.Create(message.NotificationURL);
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            request.Timeout = 10000;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = message.ToString().Length;
            request.KeepAlive = false;
            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(message.ToString());
            }
            var response = (HttpWebResponse)request.GetResponse();
            string responseText = null;
            var responseStream = response.GetResponseStream();
            if (responseStream != null)
                using (var reader = new StreamReader(responseStream))
                {
                    responseText = reader.ReadToEnd();
                }
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (responseText != null && !responseText.Contains("<error>0</error>"))
                {
                    throw new InvalidOperationException(responseText);
                }
            }
            else
            {
                throw new HttpException("HTTP Error occurred: " + response.StatusCode.ToString());
            }
        }

        /// <summary>
        /// Gets a trackback message
        /// </summary>
        /// <param name="context">Context object</param>
        /// <returns>A trackback message</returns>
        public static TrackBackMessage GetTrackBack(HttpContext context)
        {
            return GetTrackBack(context.Request);
        }

        /// <summary>
        /// Gets a trackback message
        /// </summary>
        /// <param name="request">Request object</param>
        /// <returns>A trackback message</returns>
        public static TrackBackMessage GetTrackBack(HttpRequest request)
        {
            var message = new TrackBackMessage
                {
                    Title = request.Params["title"],
                    ID = request.Params["id"],
                    Excerpt = request.Params["excerpt"],
                    BlogName = request.Params["blog_name"]
                };
            if (request.Params["url"] != null)
                message.PostUrl = new Uri(request.Params["url"].Split(',')[0]);
            return message;
        }

        /// <summary>
        /// Send a success message
        /// </summary>
        /// <param name="context">Context object</param>
        public static void SendSuccess(HttpContext context)
        {
            SendSuccess(context.Response);
        }

        /// <summary>
        /// Send a success message
        /// </summary>
        /// <param name="response">Response object</param>
        public static void SendSuccess(HttpResponse response)
        {
            response.Write("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><response><error>0</error></response>");
        }

        /// <summary>
        /// Sends an error message
        /// </summary>
        /// <param name="context">Context Object</param>
        /// <param name="errorMessage">Error message to send</param>
        public static void SendError(HttpContext context, string errorMessage)
        {
            SendError(context.Response, errorMessage);
        }

        /// <summary>
        /// Sends an error message
        /// </summary>
        /// <param name="response">Response Object</param>
        /// <param name="errorMessage">Error message to send</param>
        public static void SendError(HttpResponse response, string errorMessage)
        {
            response.Write("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><response><error>" + errorMessage + "</error></response>");
        }

        #endregion

        #region Private Static Functions

        private static readonly Regex TrackBackLink = new Regex("trackback:ping=\"([^\"]+)\"", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static readonly Regex TrackBackID = new Regex("id=([^\"]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        private static void GetTrackBackURL(TrackBackMessage message)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)");
                string tempContent = client.DownloadString(message.NotificationURL);
                string tempURL = TrackBackLink.Match(tempContent).Groups[1].ToString().Trim();
                Uri tempUri;
                Uri.TryCreate(tempURL, UriKind.Absolute, out tempUri);
                message.NotificationURL = tempUri;

                tempURL = TrackBackID.Match(tempURL).Groups[1].ToString().Trim();
                message.ID = tempURL;
            }
        }

        #endregion
    }

    /// <summary>
    /// Track back message class
    /// </summary>
    public class TrackBackMessage
    {
        #region Constructor

        #endregion

        #region Public Properties

        /// <summary>
        /// ID of the item (most likely this is not set)
        /// </summary>
        public object ID { get; set; }

        /// <summary>
        /// Title of the post linking to you
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// URL of the post linking to you
        /// </summary>
        public Uri PostUrl { get; set; }

        /// <summary>
        /// Excerpt from the post linking to you
        /// </summary>
        public string Excerpt { get; set; }

        /// <summary>
        /// Name of the blog linking to you
        /// </summary>
        public string BlogName { get; set; }

        /// <summary>
        /// URL to send the message notification to.
        /// Only used for sending the message and filled in
        /// automatically.
        /// </summary>
        public Uri NotificationURL { get; set; }

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Writes out the message (used for sending)
        /// </summary>
        /// <returns>A string with the message information</returns>
        public override string ToString()
        {
            string first = "&";
            if (string.IsNullOrEmpty(NotificationURL.Query))
                first = "?";
            return first + "title=" + Title + "&url=" + PostUrl + "&excerpt=" + Excerpt + "&blog_name=" + BlogName;
        }

        #endregion
    }
}