﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
#endregion

namespace Operate.Web.PingBack
{
    /// <summary>
    /// Handles ping backs, track backs, and pinging services
    /// </summary>
    public static class Manager
    {
        #region Public Static Functions

        /// <summary>
        /// Pings services such as technorati, etc.
        /// </summary>
        /// <param name="services">List of services</param>
        /// <param name="blog">URI of your blog</param>
        /// <param name="blogName">Name of the blog</param>
        /// <param name="threaded">If true this is done in a seperate thread,
        /// if false it will wait for it to end</param>
        public static void PingServices(IEnumerable<Uri> services, Uri blog, string blogName, bool threaded)
        {
            if (threaded)
            {
                ThreadPool.QueueUserWorkItem(delegate { PingServices(services, blog, blogName); });
            }
            else
            {
                PingServices(services, blog, blogName);
            }
        }

        #endregion

        #region Private Static Functions

        private static void PingServices(IEnumerable<Uri> services, Uri blog, string blogName)
        {
            foreach (Uri service in services)
            {
                var request = (HttpWebRequest)WebRequest.Create(service);
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                request.ContentType = "text/xml";
                request.Method = "POST";
                request.Timeout = 10000;
                using (var xmlWriter = new XmlTextWriter(request.GetRequestStream(), Encoding.ASCII))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement("methodCall");
                    xmlWriter.WriteElementString("methodName", "weblogUpdates.ping");
                    xmlWriter.WriteStartElement("params");
                    xmlWriter.WriteStartElement("param");
                    xmlWriter.WriteElementString("value", blogName);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("param");
                    xmlWriter.WriteElementString("value", blog.ToString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                }
            }
        }

        #endregion
    }
}