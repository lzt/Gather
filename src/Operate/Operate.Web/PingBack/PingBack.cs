﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
#endregion

namespace Operate.Web.PingBack
{
    /// <summary>
    /// Handles ping backs
    /// </summary>
    public static class PingBack
    {
        #region Public Static Functions

        /// <summary>
        /// Sends a ping back
        /// </summary>
        /// <param name="message">Message to send</param>
        public static void SendPingBack(PingBackMessage message)
        {
            if (string.IsNullOrEmpty(message.Source) || string.IsNullOrEmpty(message.Target))
                return;

            var request = (HttpWebRequest)WebRequest.Create(new Uri(message.Target));
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            var response = (HttpWebResponse)request.GetResponse();
            string pingURL = (!string.IsNullOrEmpty(response.Headers["x-pingback"])) ? response.Headers["x-pingback"] : response.Headers["pingback"];
            Uri uriUsing;
            if (!string.IsNullOrEmpty(pingURL) && Uri.TryCreate(pingURL, UriKind.Absolute, out uriUsing))
            {
                request = (HttpWebRequest)WebRequest.Create(uriUsing);
                request.Method = "POST";
                request.Timeout = 10000;
                request.ContentType = "text/xml";
                request.ProtocolVersion = HttpVersion.Version11;
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 6.0)";
                using (var writer = new XmlTextWriter(request.GetRequestStream(), Encoding.ASCII))
                {
                    writer.WriteStartDocument(true);
                    writer.WriteStartElement("methodCall");
                    writer.WriteElementString("methodName", "pingback.ping");
                    writer.WriteStartElement("params");
                    writer.WriteStartElement("param");
                    writer.WriteStartElement("value");
                    writer.WriteElementString("string", message.Source);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteStartElement("param");
                    writer.WriteStartElement("value");
                    writer.WriteElementString("string", message.Target);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                request.GetResponse();
            }
        }

        /// <summary>
        /// Gets a ping back
        /// </summary>
        /// <param name="context">The HttpContext for this item</param>
        /// <returns>The ping back message</returns>
        public static PingBackMessage GetPingBack(HttpContext context)
        {
            return GetPingBack(context.Request);
        }

        /// <summary>
        /// Gets a ping back
        /// </summary>
        /// <param name="request">The HttpRequest for this item</param>
        /// <returns>The ping back message</returns>
        public static PingBackMessage GetPingBack(HttpRequest request)
        {
            var tempMessage = new PingBackMessage {Source = "", Target = ""};
            string requestText = GetRequest(request);
            if (!requestText.Contains("<methodName>pingback.ping</methodName>"))
            {
                return tempMessage;
            }
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(requestText);
            XmlNodeList nodes = xmlDocument.SelectNodes("methodCall/params/param/value/string") ??
                                xmlDocument.SelectNodes("methodCall/params/param/value");
            if (nodes != null)
            {
                tempMessage.Source = nodes[0].InnerText.Trim();
                tempMessage.Target = nodes[1].InnerText.Trim();
            }
            return tempMessage;
        }

        /// <summary>
        /// Sends a success message.
        /// </summary>
        /// <param name="context">HttpContext of the item</param>
        public static void SendSuccess(HttpContext context)
        {
            SendSuccess(context.Response);
        }
        /// <summary>
        /// Sends an error message
        /// </summary>
        /// <param name="context">Context of the item</param>
        /// <param name="code">Error code</param>
        /// <param name="errorMessage">Error Message</param>
        public static void SendError(HttpContext context, int code, string errorMessage)
        {
            SendError(context.Response, code, errorMessage);
        }

        /// <summary>
        /// Sends a success message.
        /// </summary>
        /// <param name="response">Response for the item</param>
        public static void SendSuccess(HttpResponse response)
        {
            response.Write("<methodResponse><params><param><value><string>Success</string></value></param></params></methodResponse>");
        }

        /// <summary>
        /// Sends an error message
        /// </summary>
        /// <param name="response">Response object</param>
        /// <param name="code">Error code</param>
        /// <param name="errorMessage">Error message</param>
        public static void SendError(HttpResponse response, int code, string errorMessage)
        {
            var builder = new StringBuilder();
            builder.Append("<?xml version=\"1.0\"?><methodResponse><fault><value><struct><member><name>faultCode</name>");
            builder.Append("<value><int>").Append(code).Append("</int></value></member><member><name>faultString</name>");
            builder.Append("<value><string>").Append(errorMessage).Append("</string></value></member></struct></value></fault></methodResponse>");
            response.Write(builder.ToString());
        }

        #endregion

        #region Private Functions

        private static string GetRequest(HttpRequest request)
        {
            var tempBuffer = new byte[request.InputStream.Length];
            request.InputStream.Read(tempBuffer, 0, tempBuffer.Length);
            return Encoding.Default.GetString(tempBuffer);
        }

        #endregion
    }

    /// <summary>
    /// Message class used for pingbacks
    /// </summary>
    public class PingBackMessage
    {
        #region Constructor

        #endregion

        #region Public Properties

        /// <summary>
        /// Source Location
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Target location
        /// </summary>
        public string Target { get; set; }

        #endregion
    }
}