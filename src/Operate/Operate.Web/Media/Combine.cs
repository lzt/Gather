﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Operate.Media.Image.ExtensionMethods;
using Operate.Web.ExtensionMethods;

namespace Operate.Web.Media
{
    /// <summary>
    /// SourceAddressType
    /// </summary>
    public enum SourceAddressType
    {
        /// <summary>
        /// 网络图片
        /// </summary>
        WebUrl,

        /// <summary>
        /// 本地图片
        /// </summary>
        FilePath
    }

    /// <summary>
    /// 合并图片与同一张画布上
    /// </summary>
    public class CombineSource
    {
        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// AddressType
        /// </summary>
        public SourceAddressType AddressType { get; set; }

        /// <summary>
        /// CombineSource
        /// </summary>
        public CombineSource()
        {
            Address = "";
            AddressType = SourceAddressType.FilePath;
        }
    }

    internal class BitmapSource
    {
        public Bitmap Bm { get; set; }

        public Point WritePoint { get; set; }
    }

    /// <summary>
    /// 拼接图片
    /// </summary>
    public class Combine
    {
        /// <summary>
        /// CombineImages
        /// </summary>
        /// <param name="css"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="toPath"></param>
        /// <returns></returns>
        public static void CombineImages(CombineSource[] css, int width, int height, string toPath)
        {
            using (var bt = CombineImages(css, width, height))
            {
                bt.Save(toPath);
            }
        }

        /// <summary>
        /// CombineImages
        /// </summary>
        /// <param name="css"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static Bitmap CombineImages(CombineSource[] css, int width, int height)
        {
            var itemImgWidth = 0;
            var itemImgHeight = 0;
            var columnCount = 1;
            decimal borderProportion = 0.1m;
            decimal imgProportion = 1 - borderProportion;

            if (css.Length <= 4)
            {
                itemImgWidth = width / 2;
                itemImgHeight = height / 2;
                columnCount = 2;
            }

            if (css.Length > 4)
            {
                itemImgWidth = width / 3;
                itemImgHeight = height / 3;
                columnCount = 3;
            }

            var imageList = new List<BitmapSource>();
            for (int index = 0; index < css.Length; index++)
            {
                var cs = css[index];
                Bitmap rbm = null;
                if (cs.AddressType == SourceAddressType.WebUrl)
                {
                    var ms = cs.Address.Download();
                    var bm = new Bitmap(ms);
                    rbm = bm.Resize(Convert.ToInt32(itemImgWidth * imgProportion), Convert.ToInt32(itemImgHeight * imgProportion), Quality.High);
                }

                if (cs.AddressType == SourceAddressType.FilePath)
                {
                    var bm = new Bitmap(Image.FromFile(cs.Address));
                    rbm = bm.Resize(Convert.ToInt32(itemImgWidth * imgProportion), Convert.ToInt32(itemImgHeight * imgProportion), Quality.High);
                }

                imageList.Add(new BitmapSource
                {
                    Bm = rbm,
                    //WritePoint = new Point
                    //{
                    //    X = index / columnCount * itemImgWidth + Convert.ToInt32(itemImgWidth * borderProportion / 2),
                    //    Y = index % columnCount * itemImgHeight + Convert.ToInt32(itemImgHeight * borderProportion / 2),
                    //}
                    WritePoint = new Point
                    {
                        Y = index / columnCount * itemImgWidth + Convert.ToInt32(itemImgWidth * borderProportion / 2),
                        X = index % columnCount * itemImgHeight + Convert.ToInt32(itemImgHeight * borderProportion / 2),
                    }
                });

                if (imageList.Count >= 9)
                {
                    break;
                }
            }

            var finalImg = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(finalImg);
            g.Clear(SystemColors.AppWorkspace);

            foreach (BitmapSource source in imageList)
            {
                g.DrawImage(source.Bm, source.WritePoint);
            }
            g.Dispose();

            return finalImg;
        }
    }
}
