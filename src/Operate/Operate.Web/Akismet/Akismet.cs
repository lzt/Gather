﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Globalization;
using System.Web;
#endregion

namespace Operate.Web.Akismet
{
    /// <summary>
    /// Akismet helper class
    /// </summary>
    public class Akismet : REST.REST
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="key">API Key</param>
        /// <param name="site">Site using Akismet</param>
        public Akismet(string key, string site)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Key = key;
            Website = site;
            VerifyKeyData = string.Format(CultureInfo.InvariantCulture, "key={0}&blog={1}", key, System.Web.HttpUtility.UrlEncode(Website));
            CommentCheckUrl = new Uri(string.Format(CultureInfo.InvariantCulture, "http://{0}.rest.akismet.com/1.1/comment-check", key));
            CommentCheckData = "blog=" + System.Web.HttpUtility.UrlEncode(Website) + "&user_ip={0}&user_agent={1}&referrer={2}" +
                 "&permalink={3}&comment_type={4}&comment_author={5}&comment_author_email={6}&" +
                 "comment_author_url={7}&comment_content={8}";
            SubmitSpamUrl = new Uri(string.Format(CultureInfo.InvariantCulture, "http://{0}.rest.akismet.com/1.1/submit-spam", key));
            SubmitHamUrl = new Uri(string.Format(CultureInfo.InvariantCulture, "http://{0}.rest.akismet.com/1.1/submit-ham", key));
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        /// <summary>
        /// API Key
        /// </summary>
        protected virtual string Key { get; set; }

        /// <summary>
        /// Website
        /// </summary>
        protected virtual string Website { get; set; }

        /// <summary>
        /// Data sent to verify the API key
        /// </summary>
        protected virtual string VerifyKeyData { get; set; }

        /// <summary>
        /// Comment check url
        /// </summary>
        protected virtual Uri CommentCheckUrl { get; set; }

        /// <summary>
        /// Submit spam url
        /// </summary>
        protected virtual Uri SubmitSpamUrl { get; set; }

        /// <summary>
        /// Submit ham url
        /// </summary>
        protected virtual Uri SubmitHamUrl { get; set; }

        /// <summary>
        /// Comment check data string
        /// </summary>
        protected virtual string CommentCheckData { get; set; }

        #endregion

        #region Functions

        /// <summary>
        /// Verifies the Key
        /// </summary>
        /// <returns>True if the key is valid, false otherwise</returns>
        public virtual bool VerifyKey()
        {
           Url = new Uri("http://rest.akismet.com/1.1/verify-key");
           Data = VerifyKeyData;
            return POST() == "valid";
        }

        /// <summary>
        /// Checks if a comment is spam or ham
        /// </summary>
        /// <param name="comment">Comment to check</param>
        /// <returns>True if it is spam, false otherwise</returns>
        public virtual bool IsSpam(Comment comment)
        {
            Url = CommentCheckUrl;
            Data = SetupData(comment);
            return bool.Parse(POST());
        }

        /// <summary>
        /// Submits a spam message
        /// </summary>
        /// <param name="comment">Comment to submit</param>
        public virtual void SubmitSpam(Comment comment)
        {
            Url = SubmitSpamUrl;
            Data = SetupData(comment);
            POST();
        }

        /// <summary>
        /// Submits a ham message
        /// </summary>
        /// <param name="comment">Comment to submit</param>
        public virtual void SubmitHam(Comment comment)
        {
            Url = SubmitHamUrl;
            Data = SetupData(comment);
            POST();
        }

        private string SetupData(Comment comment)
        {
            return string.Format(CultureInfo.InvariantCulture, CommentCheckData,
                 System.Web.HttpUtility.UrlEncode(comment.UserIP),
                 System.Web.HttpUtility.UrlEncode(comment.UserAgent),
                 System.Web.HttpUtility.UrlEncode(comment.Referrer),
                 System.Web.HttpUtility.UrlEncode(comment.Permalink),
                 System.Web.HttpUtility.UrlEncode(comment.CommentType),
                 System.Web.HttpUtility.UrlEncode(comment.UserName),
                 System.Web.HttpUtility.UrlEncode(comment.UserEmail),
                 System.Web.HttpUtility.UrlEncode(comment.UserUrl),
                 System.Web.HttpUtility.UrlEncode(comment.Content));
        }

        #endregion
    }
}
