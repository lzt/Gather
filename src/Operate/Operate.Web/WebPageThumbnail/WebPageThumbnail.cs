﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Forms;
#endregion

namespace Operate.Web.WebPageThumbnail
{
    /// <summary>
    /// Class for taking a screen shot of a web page
    /// </summary>
    public class WebPageThumbnail
    {
        #region Constructor

        #endregion

        #region Private Variables
        private string _fileName;
        private string _url;
        private int _width;
        private int _height;
        #endregion

        #region Public Functions

        /// <summary>
        /// Generates a screen shot of a web site
        /// </summary>
        /// <param name="fileName">File name to save as</param>
        /// <param name="url">Url to take the screen shot of</param>
        /// <param name="width">Width of the image (-1 for full size)</param>
        /// <param name="height">Height of the image (-1 for full size)</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "Width"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "Url"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "Height"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "FileName")]
        public void GenerateBitmap(string fileName, string url, int width = -1, int height = -1)
        {
            _url = url;
            _fileName = fileName;
            _width = width;
            _height = height;
            var tempThread = new Thread(CreateBrowser);
            tempThread.SetApartmentState(ApartmentState.STA);
            tempThread.Start();
            tempThread.Join();
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Creates the browser
        /// </summary>
        private void CreateBrowser()
        {
            using (var browser = new WebBrowser())
            {
                browser.ScrollBarsEnabled = false;
                DateTime timeoutStart = DateTime.Now;
                var timeout = new TimeSpan(0, 0, 10);
                browser.Navigate(_url);
                browser.DocumentCompleted += Browser_DocumentCompleted;
                while (browser.ReadyState != WebBrowserReadyState.Complete)
                {
                    if (DateTime.Now - timeoutStart > timeout)
                        break;
                    Application.DoEvents();
                }
            }
        }

        /// <summary>
        /// Called when the browser is completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var browser = (WebBrowser)sender;
            browser.ScriptErrorsSuppressed = true;
            browser.ScrollBarsEnabled = false;
            if (browser.Document != null)
            {
                if (browser.Document.Body != null)
                {
                    browser.Width = _width == -1 ? browser.Document.Body.ScrollRectangle.Width : _width;
                    browser.Height = _height == -1 ? browser.Document.Body.ScrollRectangle.Height : _height;
                }
            }
            using (var image = new Bitmap(browser.Width, browser.Height))
            {
                browser.BringToFront();
                browser.DrawToBitmap(image, new Rectangle(0, 0, browser.Width, browser.Height));
                image.Save(_fileName, ImageFormat.Bmp);
            }
        }

        #endregion
    }
}