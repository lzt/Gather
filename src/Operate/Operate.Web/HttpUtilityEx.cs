﻿using System.Web;

namespace Operate.Web
{
    /// <summary>
    /// Http功能类
    /// </summary>
    public class HttpUtilityEx
    {
        /// <summary>
        ///  获取访问Ip
        /// </summary>
        /// <returns></returns>
        public static string GetIp()
        {
            var ip = "";
            var cx = HttpContext.Current;
            if (cx != null)
            {
                ip = HttpContext.Current.Request.UserHostAddress;
                ip = (ip == "::1" ? "127.0.0.1" : ip);
            }

            return ip;
        }
    }
}