﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Security;
using System.Net.Sockets;
using System.Text.RegularExpressions;

#endregion

namespace Operate.Web.Email.Pop3
{
    /// <summary>
    /// Class for implemented basic Pop3 client
    /// functionality.
    /// </summary>
    public class Pop3Client : TcpClient
    {
        #region Public Functions

        /// <summary>
        /// Connects to a server
        /// </summary>
        /// <param name="userName">Username used to log into the server</param>
        /// <param name="password">Password used to log into the server</param>
        /// <param name="server">Server location</param>
        /// <param name="port">Port on the server to use</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public void Connect(string userName, string password, string server, int port)
        {
            UserName = userName;
            Password = password;
            Server = server;
            Port = port;

            Connect(server, port);
            string responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }

            WriteMessage("USER " + userName + "\r\n");
            responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }

            WriteMessage("PASS " + password + "\r\n");
            responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }
        }

        /// <summary>
        /// Disconnects from the server
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public void Disconnect()
        {
            WriteMessage("QUIT\r\n");
            string responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }
        }

        /// <summary>
        /// Gets a list of messages from the server
        /// </summary>
        /// <returns>A list of messages (only contains message number and size)</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.EndsWith(System.String,System.StringComparison)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public ICollection<Message> GetMessageList()
        {
            var returnArray = new List<Message>();
            WriteMessage("LIST\r\n");
            string responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }
            bool done = false;
            while (!done)
            {
                var tempRegex = new Regex(Regex.Escape("+") + "OK.*\r\n");
                if (!responseString.EndsWith("\r\n.\r\n", StringComparison.InvariantCulture))
                {
                    while (!responseString.EndsWith("\r\n.\r\n", StringComparison.InvariantCulture))
                        responseString += GetResponse();
                }
                responseString = tempRegex.Replace(responseString, "");
                string[] seperator = { "\r\n" };
                string[] values = responseString.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                foreach (string value in values)
                {
                    string[] newSeperator = { " " };
                    string[] pair = value.Split(newSeperator, StringSplitOptions.RemoveEmptyEntries);
                    if (pair.Length > 1)
                    {
                        var tempMessage = new Message
                            {
                                MessageNumber = Int32.Parse(pair[0], CultureInfo.InvariantCulture),
                                MessageSize = Int32.Parse(pair[1], CultureInfo.InvariantCulture),
                                Retrieved = false
                            };
                        returnArray.Add(tempMessage);
                    }
                    else
                    {
                        done = true;
                        break;
                    }
                }
            }
            return returnArray;
        }

        /// <summary>
        /// Gets a specific message from the server
        /// </summary>
        /// <param name="messageWanted">The message that you want to pull down from the server</param>
        /// <returns>A new message containing the content</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public Message GetMessage(Message messageWanted)
        {
            var tempMessage = new Message
                {
                    MessageSize = messageWanted.MessageSize,
                    MessageNumber = messageWanted.MessageNumber
                };

            WriteMessage("RETR " + messageWanted.MessageNumber + "\r\n");
            string responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }
            var tempRegex = new Regex(Regex.Escape("+") + "OK.*\r\n");
            responseString = tempRegex.Replace(responseString, "");
            tempRegex = new Regex("\r\n.\r\n$");
            tempMessage.Retrieved = true;
            string bodyText = "";
            while (true)
            {
                if (tempRegex.Match(responseString).Success || string.IsNullOrEmpty(responseString))
                {
                    bodyText += responseString;
                    bodyText = tempRegex.Replace(bodyText, "");
                    break;
                }
                bodyText += responseString;
                responseString = GetResponse();
            }
            tempMessage.MessageBody = new MIME.MIMEMessage(bodyText);

            return tempMessage;
        }

        /// <summary>
        /// Deletes a message from the server
        /// </summary>
        /// <param name="messageToDelete">Message to delete</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.StartsWith(System.String,System.StringComparison)")]
        public void Delete(Message messageToDelete)
        {
            WriteMessage("DELE " + messageToDelete.MessageNumber + "\r\n");
            string responseString = GetResponse();
            if (!responseString.StartsWith("+OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Pop3Exception(responseString);
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Writes a message to the server
        /// </summary>
        /// <param name="message">Information to send to the server</param>
        private void WriteMessage(string message)
        {
            var encoding = new System.Text.ASCIIEncoding();
            byte[] buffer = encoding.GetBytes(message);
            if (!UseSSL)
            {
                NetworkStream stream = GetStream();
                stream.Write(buffer, 0, buffer.Length);
            }
            else
            {
                if (_sslStreamUsing == null)
                {
                    _sslStreamUsing = new SslStream(GetStream());
                    _sslStreamUsing.AuthenticateAsClient(Server);
                }
                _sslStreamUsing.Write(buffer, 0, buffer.Length);
            }
        }

        /// <summary>
        /// Gets the response from the server
        /// Note that this uses TCP/IP to get the
        /// messages, which means that the entire message
        /// may not be found in the returned string
        /// (it may only be a partial message)
        /// </summary>
        /// <returns>The response from the server</returns>
        private string GetResponse()
        {
            var buffer = new byte[1024];
            var encoding = new System.Text.ASCIIEncoding();
            string response = "";
            if (!UseSSL)
            {
                NetworkStream responseStream = GetStream();
                while (true)
                {
                    int bytes = responseStream.Read(buffer, 0, 1024);
                    response += encoding.GetString(buffer, 0, bytes);
                    if (bytes != 1024)
                    {
                        break;
                    }
                }
            }
            else
            {
                if (_sslStreamUsing == null)
                {
                    _sslStreamUsing = new SslStream(GetStream());
                    _sslStreamUsing.AuthenticateAsClient(Server);
                }
                while (true)
                {
                    int bytes = _sslStreamUsing.Read(buffer, 0, 1024);
                    response += encoding.GetString(buffer, 0, bytes);
                    if (bytes != 1024)
                    {
                        break;
                    }
                }
            }
            return response;
        }

        #endregion

        #region Properties
        private SslStream _sslStreamUsing;

        /// <summary>
        /// Decides whether or not we are using
        /// SSL to connect to the server
        /// </summary>
        public bool UseSSL { get; set; }

        /// <summary>
        /// Server location
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// User name used to log in
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password used to log in
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Port on which to connect
        /// </summary>
        public int Port { get; set; }

        #endregion
    }
}