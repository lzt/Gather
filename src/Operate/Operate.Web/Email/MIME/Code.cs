﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using  Operate.ExtensionMethods;
using System.Text;

#endregion

namespace Operate.Web.Email.MIME
{
    /// <summary>
    /// Base coder class
    /// </summary>
    public class Code
    {
        #region Constructor

        #endregion

        #region Public Properties
        private string _characterSet = "";
        /// <summary>
        /// Character set this coder is using
        /// </summary>
        public string CharacterSet
        {
            get { return _characterSet; }
            set { _characterSet = value.Replace("\"",""); }
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Decodes a string to bytes
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="output">Bytes once decoded</param>
        public virtual void Decode(string input, out byte[] output)
        {
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }

            if (string.IsNullOrEmpty(CharacterSet))
            {
                CharacterSet = Encoding.Default.BodyName;
            }
            output = Encoding.GetEncoding(CharacterSet).GetBytes(input);
        }

        /// <summary>
        /// Decodes a string to another string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="output">Output string</param>
        public virtual void Decode(string input, out string output)
        {
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }
            if (string.IsNullOrEmpty(CharacterSet))
            {
                CharacterSet = Encoding.Default.BodyName;
            }
            byte[] tempBytes;
            Decode(input,out tempBytes);
            output=Encoding.GetEncoding(CharacterSet).GetString(tempBytes);
        }

        /// <summary>
        /// Encodes a byte array
        /// </summary>
        /// <param name="input">Input array</param>
        /// <returns>A string of the bytes encoded</returns>
        public virtual string Encode(byte[] input)
        {
                        if (input.IsNull()) { throw new ArgumentNullException("input"); }

            if (string.IsNullOrEmpty(CharacterSet))
            {
                CharacterSet = Encoding.Default.BodyName;
            }
            return Encoding.GetEncoding(CharacterSet).GetString(input, 0, input.Length);
        }

        /// <summary>
        /// Encodes a string into a string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>An encoded string</returns>
        public virtual string Encode(string input)
        {
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }
            if (string.IsNullOrEmpty(CharacterSet))
            {
                CharacterSet = Encoding.Default.BodyName;
            }
            byte[] tempArray = Encoding.GetEncoding(CharacterSet).GetBytes(input);
            return Encode(tempArray);
        }
        #endregion
    }
}