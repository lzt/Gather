﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Globalization;
using System.Text;
#endregion

namespace Operate.Web.Email.MIME.CodeTypes
{
    /// <summary>
    /// Default base coder
    /// </summary>
    public class CodeBase : Code
    {
        #region Constructor

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Decodes a string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="output">Output string</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.IndexOf(System.String,System.Int32,System.StringComparison)")]
        public override void Decode(string input, out string output)
        {
            output = "";
            int index = 0;
            while (index < input.Length)
            {
                int currentIndex = input.IndexOf("=?", index, StringComparison.InvariantCulture);
                if (currentIndex != -1)
                {
                    output += input.Substring(index, currentIndex - index);
                    int currentIndex2 = input.IndexOf("?=", currentIndex + 2, StringComparison.InvariantCulture);
                    if (currentIndex2 != -1)
                    {
                        currentIndex += 2;
                        int currentIndex3 = input.IndexOf('?', currentIndex);
                        if (currentIndex3 != -1 && input[currentIndex3 + 2] == '?')
                        {
                            CharacterSet = input.Substring(currentIndex, currentIndex3 - currentIndex);
                            string decString = input.Substring(currentIndex3 + 3, currentIndex2 - currentIndex3 - 3);
                            if (input[currentIndex3 + 1] == 'Q')
                            {
                                Code tempCode = CodeManager.Instance["quoted-printable"];
                                tempCode.CharacterSet = CharacterSet;
                                string tempString;
                                tempCode.Decode(decString, out tempString);
                                output += tempString;
                            }
                            else if (input[currentIndex3 + 1] == 'B')
                            {
                                Code tempCode = CodeManager.Instance["base64"];
                                tempCode.CharacterSet = CharacterSet;
                                string tempString;
                                tempCode.Decode(decString, out tempString);
                                output += tempString;
                            }
                            else
                            {
                                output += decString;
                            }
                        }
                        else
                        {
                            output += input.Substring(currentIndex3, currentIndex2 - currentIndex3);
                        }
                        index = currentIndex2 + 2;
                    }
                    else
                    {
                        output += input.Substring(currentIndex, input.Length - currentIndex);
                        break;
                    }
                }
                else
                {
                    output += input.Substring(index, input.Length - index);
                    break;
                }
            }
        }

        /// <summary>
        /// Encodes a string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>encoded string</returns>
        public override string Encode(string input)
        {
            var builder = new StringBuilder();
            builder.Append(DelimeterNeeded ? EncodeDelimeter(input) : EncodeNoDelimeter(input));

            if (IsAutoFold)
            {
                string[] foldCharacters = FoldCharacters;
                foreach (string foldCharacter in foldCharacters)
                {
                    string newFoldString = foldCharacter + "\r\n\t";
                    builder.Replace(foldCharacter, newFoldString);
                }
            }
            return builder.ToString();
        }
        #endregion

        #region Protected Properties
        /// <summary>
        /// Fold characters
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        protected virtual string[] FoldCharacters
        {
            get { return null; }
        }

        /// <summary>
        /// Is folding used
        /// </summary>
        protected virtual bool IsAutoFold
        {
            get { return false; }
        }

        /// <summary>
        /// Are delimeter's needed
        /// </summary>
        protected virtual bool DelimeterNeeded
        {
            get { return false; }
        }

        /// <summary>
        /// Delimeter characters
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        protected virtual char[] DelimeterCharacters
        {
            get { return null; }
        }
        #endregion

        #region Protected Functions
        /// <summary>
        /// Encodes a string based on delimeters specified
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>A string encoded based off of delimeters</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        protected string EncodeDelimeter(string input)
        {
            var builder = new StringBuilder();
            char[] filter = DelimeterCharacters;
            string[] inputArray = input.Split(filter);
            int index = 0;
            foreach (string tempString in inputArray)
            {
                if (tempString != null)
                {
                    index += tempString.Length;
                    if (string.IsNullOrEmpty(CharacterSet))
                    {
                        CharacterSet = Encoding.Default.BodyName;
                    }
                    string encodingUsing = SelectEncoding(input).ToLower(CultureInfo.InvariantCulture);
                    if (encodingUsing.Equals("non", StringComparison.InvariantCultureIgnoreCase))
                    {
                        builder.Append(tempString);
                    }
                    else if (encodingUsing.Equals("base64", StringComparison.InvariantCultureIgnoreCase)
                        || encodingUsing.Equals("quoted-printable", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Code tempCode = CodeManager.Instance[encodingUsing];
                        tempCode.CharacterSet = CharacterSet;
                        builder.AppendFormat("=?{0}?Q?{1}?=", CharacterSet, tempCode.Encode(tempString));
                    }
                    if (index < input.Length)
                        builder.Append(input.Substring(index, 1));
                    ++index;
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Encodes a string without the use of delimeters
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>An encoded string</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        protected string EncodeNoDelimeter(string input)
        {
            var builder = new StringBuilder();
            if (string.IsNullOrEmpty(CharacterSet))
                CharacterSet = Encoding.Default.BodyName;

            string encodingUsing = SelectEncoding(input).ToLower(CultureInfo.InvariantCulture);
            if (encodingUsing.Equals("non", StringComparison.InvariantCultureIgnoreCase))
            {
                builder.Append(input);
            }
            else if (encodingUsing.Equals("base64", StringComparison.InvariantCultureIgnoreCase)
                        || encodingUsing.Equals("quoted-printable", StringComparison.InvariantCultureIgnoreCase))
            {
                Code tempCode = CodeManager.Instance[encodingUsing];
                tempCode.CharacterSet = CharacterSet;
                builder.AppendFormat("=?{0}?Q?{1}?=", CharacterSet, tempCode.Encode(input));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Selects an encoding type
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>A string containing the encoding type that should be used</returns>
        protected static string SelectEncoding(string input)
        {
            int numberOfNonASCII = 0;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (char t in input)
            {
                if (IsNonASCIICharacter(t))
                    ++numberOfNonASCII;
            }
            if (numberOfNonASCII == 0)
                return "non";
            int quotableSize = input.Length + numberOfNonASCII * 2;
            int base64Size = (input.Length + 2) / 3 * 4;
            return (quotableSize <= base64Size || numberOfNonASCII * 5 <= input.Length) ? "quoted-printable" : "base64";
        }

        /// <summary>
        /// Determines if this is a non ASCII character (greater than 255)
        /// </summary>
        /// <param name="input"></param>
        /// <returns>True if it is, false otherwise</returns>
        protected static bool IsNonASCIICharacter(char input)
        {
            return input > 255;
        }
        #endregion
    }
}