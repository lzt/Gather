﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;

using System.Globalization;
using System.IO;
using System.Text;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Web.Email.MIME.CodeTypes
{
    /// <summary>
    /// Quoted-printable coder
    /// </summary>
    public class CodeQP:Code
    {
        #region Constructor

        #endregion

        #region Public Overridden Functions

        /// <summary>
        /// Decodes the string
        /// </summary>
        /// <param name="input">String to decode</param>
        /// <param name="output">Output in bytes</param>
        public override void Decode(string input, out byte[] output)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException("input");
            using (var memoryStream = new MemoryStream())
            {
                var reader = new StringReader(input);
                try
                {
                    string currentLine=reader.ReadLine();
                    while (!string.IsNullOrEmpty(currentLine))
                    {
                        DecodeOneLine(memoryStream, currentLine);
                        currentLine = reader.ReadLine();
                    }
                    output = memoryStream.ToArray();
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Encodes the data
        /// </summary>
        /// <param name="input">Input data</param>
        /// <returns>The encoded string</returns>
        public override string Encode(byte[] input)
        {
            if (input==null)
                throw new ArgumentNullException("input");
            var output = new StringBuilder();
            foreach (byte index in input)
            {
                if ((index < 33 || index > 126 || index == 0x3D)
                    && index != 0x09 && index != 0x20 && index != 0x0D && index != 0x0A)
                {
                    int code = index;
                    output.AppendFormat("={0}", code.ToString("X2",CultureInfo.InvariantCulture));
                }
                else
                {
                    output.Append(Convert.ToChar(index));
                }
            }
            output.Replace(" \r", "=20\r", 0, output.Length);
            output.Replace("\t\r", "=09\r", 0, output.Length);
            output.Replace("\r\n.\r\n", "\r\n=2E\r\n", 0, output.Length);
            output.Replace(" ", "=20", output.Length - 1, 1);
            return FormatEncodedString(output.ToString());
        }
        #endregion

        #region Protected Functions

        /// <summary>
        /// Decodes a single line
        /// </summary>
        /// <param name="stream">Input stream</param>
        /// <param name="currentLine">The current line</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.EndsWith(System.String,System.StringComparison)")]
        protected static void DecodeOneLine(Stream stream,string currentLine)
        {
            if (stream.IsNull()) { throw new ArgumentNullException("stream"); }
            if (currentLine.IsNullOrEmpty()) { throw new ArgumentNullException("currentLine"); }
            for (int x = 0, y = 0; x < currentLine.Length; ++x, ++y)
            {
                byte currentByte;
                if (currentLine[x] == '=')
                {
                    if (x + 2 > currentLine.Length) break;
                    if (IsHex(currentLine[x + 1]) && IsHex(currentLine[x + 2]))
                    {
                        string hexCode = currentLine.Substring(x + 1, 2);
                        currentByte = Convert.ToByte(hexCode, 16);
                        x += 2;
                    }
                    else
                    {
                        currentByte = Convert.ToByte(currentLine[++x]);
                    }
                }
                else
                {
                    currentByte = Convert.ToByte(currentLine[x]);
                }
                stream.WriteByte(currentByte);
            }
            if (!currentLine.EndsWith("=",StringComparison.InvariantCulture))
            {
                stream.WriteByte(0x0D);
                stream.WriteByte(0x0A);
            }
        }

        /// <summary>
        /// Determines if a character is possibly hexidecimal
        /// </summary>
        /// <param name="input">Input character</param>
        /// <returns>true if it is, false otherwise</returns>
		protected static bool IsHex(char input)
        {
            if((input >= '0' && input <= '9') || (input >= 'A' && input <= 'F') || (input >= 'a' && input <= 'f'))
				return true;
            return false;
        }

        /// <summary>
        /// Formats the encoded string
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>An encoded string</returns>
        protected static string FormatEncodedString(string input)
        {
            using (var reader = new StringReader(input))
            {
                var builder = new StringBuilder();
                string currentLine = reader.ReadLine();
                while (!string.IsNullOrEmpty(currentLine))
                {
                    int index = MaxCharLen;
                    int lastIndex = 0;
                    while (index < currentLine.Length)
                    {
                        if (IsHex(currentLine[index]) && IsHex(currentLine[index - 1]) && currentLine[index - 2] == '=')
                        {
                            index -= 2;
                        }
                        builder.Append(currentLine.Substring(lastIndex, index - lastIndex));
                        builder.Append("=\r\n");
                        lastIndex = index;
                        index += MaxCharLen;
                    }
                    builder.Append(currentLine.Substring(lastIndex, currentLine.Length - lastIndex));
                    builder.Append("\r\n");
                }
                return builder.ToString();
            }
        }
        
        /// <summary>
        /// Max char length
        /// </summary>
        protected const int MaxCharLen = 75;

        #endregion
    }
}