﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Text;
#endregion

namespace Operate.Web.Email.MIME.CodeTypes
{
    /// <summary>
    /// Base 64 coder
    /// </summary>
    public class CodeBase64:Code
    {
        #region Constructor

        #endregion

        #region Public Overridden Functions
        
        /// <summary>
        /// Decodes from base64
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="output">output array</param>
        public override void Decode(string input, out byte[] output)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException("input");
            output = Convert.FromBase64String(input);
        }

        /// <summary>
        /// Encodes to Base64
        /// </summary>
        /// <param name="input">Input</param>
        /// <returns>byte array as base64</returns>
        public override string Encode(byte[] input)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            string tempString = Convert.ToBase64String(input);
            const int max = 76;
            int index = 0;
            var tempBuilder = new StringBuilder();
            while ((index + max) < tempString.Length)
            {
                tempBuilder.AppendFormat("{0}\r\n", tempString.Substring(index, max));
                index += max;
            }
            tempBuilder.AppendFormat("{0}", tempString.Substring(index, tempString.Length - index));
            return tempBuilder.ToString();
        }
        #endregion
    }
}