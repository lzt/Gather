﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Web.Email.MIME
{
    /// <summary>
    /// Fields within the header
    /// </summary>
    public class Field
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Field()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fieldText">Field text</param>
        public Field(string fieldText)
        {
            if (fieldText.IsNullOrEmpty()) { throw new ArgumentNullException("fieldText"); }

            int index = fieldText.IndexOf(':');
            if (index != -1)
                Name = fieldText.Substring(0, index);

            ++index;
            fieldText = fieldText.Substring(index, fieldText.Length - index).Trim();
            string[] splitter = { ";" };
            string[] attributes = fieldText.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            foreach (string attributeText in attributes)
            {
                Code tempCode = CodeManager.Instance[Name];
                if (tempCode != null)
                {
                    string tempText;
                    tempCode.Decode(attributeText, out tempText);
                    Attributes.Add(new Attribute(tempText));
                    CharacterSet = tempCode.CharacterSet;
                }
                else
                {
                    Attributes.Add(new Attribute(attributeText));
                }
            }
        }
        #endregion

        #region Public Properties
        private string _name="";
        private List<Attribute> _attributes=new List<Attribute>();
        private string _characterSet="";

        /// <summary>
        /// Name of the field
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Attributes associated with the field
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists"), SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public List<Attribute> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }

        /// <summary>
        /// Character set used by the field
        /// </summary>
        public string CharacterSet
        {
            get { return _characterSet; }
            set { _characterSet = value; }
        }

        /// <summary>
        /// Can be used to get specific attributes' values
        /// </summary>
        /// <param name="key">Name of the attribute</param>
        /// <returns>A string containing the value of the attribute</returns>
        [SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)")]
        public string this[string key]
        {
            get
            {
                foreach (Attribute tempAttribute in Attributes)
                {
                    if (tempAttribute.Name.Equals(key,StringComparison.InvariantCultureIgnoreCase))
                    {
                        return tempAttribute.Value;
                    }
                }
                return "";
            }
        }
        #endregion
    }
}
