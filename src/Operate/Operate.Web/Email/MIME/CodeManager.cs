﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Collections.Generic;
using System.Globalization;

#endregion

namespace Operate.Web.Email.MIME
{
    /// <summary>
    /// Manager in charge of the various decode/encode classes.
    /// Is a singleton.
    /// </summary>
    public class CodeManager
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        private CodeManager()
        {
            Load();
        }
        #endregion

        #region Instance of the class
        private static CodeManager _instance;
        private readonly Dictionary<string, Code> _codes = new Dictionary<string, Code>();

        /// <summary>
        /// Instance of the class
        /// </summary>
        public static CodeManager Instance
        {
            get { return _instance ?? (_instance = new CodeManager()); }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Loads the basic information for the class
        /// </summary>
        private void Load()
        {
            Code field = new CodeTypes.CodeBase();
            this["Subject"]= field;
            this["Comments"]= field;
            this["Content-Description"]= field;

            field = new CodeTypes.CodeAddress();
            this["From"]= field;
            this["To"]= field;
            this["Resent-To"]= field;
            this["Cc"]= field;
            this["Resent-Cc"]= field;
            this["Bcc"]= field;
            this["Resent-Bcc"]= field;
            this["Reply-To"]= field;
            this["Resent-Reply-To"]= field;

            field = new CodeTypes.CodeParameter();
            this["Content-Type"]= field;
            this["Content-Disposition"]= field;

            field = new Code();
            this["7bit"]= field;
            this["8bit"]= field;

            field = new CodeTypes.CodeBase64();
            this["base64"]= field;

            field = new CodeTypes.CodeQP();
            this["quoted-printable"]= field;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the coder assocaited with the type specified
        /// </summary>
        /// <param name="key">Content type</param>
        /// <returns>The coder associated with the type</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public Code this[string key]
        {
            get
            {
                key = key.ToLower(CultureInfo.InvariantCulture);
                if (_codes.ContainsKey(key))
                {
                    return _codes[key];
                }
                return null;
            }
            set
            {
                key = key.ToLower(CultureInfo.InvariantCulture);
                if (_codes.ContainsKey(key))
                {
                    _codes[key] = value;
                }
                else
                {
                    _codes.Add(key, value);
                }
            }
        }
        #endregion
    }
}