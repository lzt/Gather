﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;

#endregion

namespace Operate.Web.Email.MIME
{
    /// <summary>
    /// Base message class for MIME messages
    /// </summary>
    public class MIMEMessage
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public MIMEMessage()
        {
            Body = null;
            Header = null;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="input">string containing the MIME message</param>
        public MIMEMessage(string input)
        {
            Body = null;
            Header = null;
            LoadMessage(input);
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Loads the message
        /// </summary>
        /// <param name="input">string containing the message</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.IndexOf(System.String,System.StringComparison)")]
        public void LoadMessage(string input)
        {
            _content = input;
            int headerEnd = input.IndexOf("\r\n\r\n", StringComparison.InvariantCulture);
            Header = new MIMEHeader(input.Substring(0, headerEnd + 2));
            input = input.Substring(headerEnd + 2);
            Body = new MIMEBody(input, Header);
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Header of the message
        /// </summary>
        public MIMEHeader Header { get; set; }

        /// <summary>
        /// Body of the message (may contain sub messages/boundries)
        /// </summary>
        public MIMEBody Body { get; set; }

        /// <summary>
        /// Subject of the message
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public string Subject
        {
            get { try { return Header[Constants.Subject].Attributes[0].Value; } catch { return ""; } }
        }

        /// <summary>
        /// Whom the message is to
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public string To
        {
            get { try { return Header[Constants.To].Attributes[0].Value; } catch { return ""; } }
        }

        /// <summary>
        /// Whom the message is from
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public string From
        {
            get { try { return Header[Constants.From].Attributes[0].Value; } catch { return ""; } }
        }

        /// <summary>
        /// The text of the message
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public string BodyText
        {
            get
            {
                try
                {
                    if (GetMediaType(Header)==MediaEnum.MediaText)
                    {
                        return Body.Content;
                    }
                    foreach (MIMEMessage tempMessage in Body.Boundries)
                    {
                        if (!string.IsNullOrEmpty(tempMessage.BodyText))
                        {
                            return tempMessage.BodyText;
                        }
                    }
                    return "";
                }
                catch
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Gets the HTML version of the text
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public string HTMLBodyText
        {
            get
            {
                try
                {
                    if (GetMediaType(Header) == MediaEnum.MediaText && GetContentSubType(Header).Equals("html"))
                    {
                        return Body.Content;
                    }
                    foreach (MIMEMessage tempMessage in Body.Boundries)
                    {
                        if (!string.IsNullOrEmpty(tempMessage.BodyText))
                        {
                            return tempMessage.BodyText;
                        }
                    }
                    return "";
                }
                catch
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Gets the plain version of the text
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public string PlainBodyText
        {
            get
            {
                try
                {
                    if (GetMediaType(Header) == MediaEnum.MediaText&&GetContentSubType(Header).Equals("plain"))
                    {
                        return Body.Content;
                    }
                    foreach (MIMEMessage tempMessage in Body.Boundries)
                    {
                        if (!string.IsNullOrEmpty(tempMessage.BodyText))
                        {
                            return tempMessage.BodyText;
                        }
                    }
                    return "";
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Gets the content type
        /// </summary>
        /// <param name="header">Header of the message</param>
        /// <returns>A string stating the content type</returns>
        private static string GetContentType(MIMEHeader header)
        {
            if (header != null && header[Constants.ContentType] != null && header[Constants.ContentType].Attributes.Count > 0)
            {
                string contentType = header[Constants.ContentType].Attributes[0].Value;
                if (null != contentType)
                {
                    int index = contentType.IndexOf('/', 0);
                    if (index != -1)
                    {
                        return contentType.Substring(0, index);
                    }
                    return contentType;
                }
            }
            return "text";
        }

        /// <summary>
        /// Gets the media type for the body
        /// </summary>
        /// <param name="header">The header of the message</param>
        /// <returns>An enum value indicating the media type of the boundary</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)")]
        private static MediaEnum GetMediaType(MIMEHeader header)
        {
            string contentType = GetContentType(header);
            int x = 0;
            foreach (string tempType in MIMEType.TypeTable)
            {
                if (tempType.Equals(contentType, StringComparison.InvariantCultureIgnoreCase))
                {
                    return (MediaEnum)x;
                }
                ++x;
            }
            return (MediaEnum)MIMEType.TypeTable.Length - 1;
        }

        /// <summary>
        /// Gets the sub type (used to determine if the item is HTML or plain text)
        /// </summary>
        /// <param name="header">Header for this boundary/message</param>
        /// <returns>a string indicating the sub type of the boundary/message</returns>
        private static string GetContentSubType(MIMEHeader header)
        {
            if (header != null && header[Constants.ContentType] != null && header[Constants.ContentType].Attributes.Count > 0)
            {
                string contentType = header[Constants.ContentType].Attributes[0].Value;
                if (null != contentType)
                {
                    int index = contentType.IndexOf('/', 0);
                    if (index != -1)
                    {
                        return contentType.Substring(index + 1, contentType.Length - (index + 1));
                    }
                    return contentType;
                }
            }
            return "text";
        }
        #endregion

        #region Public Overriden Functions
        private string _content="";

        /// <summary>
        /// To string function returns the content of the message (including header)
        /// </summary>
        /// <returns>A string containing the initial message</returns>
        public override string ToString()
        {
            return _content;
        }
        #endregion
    }
}