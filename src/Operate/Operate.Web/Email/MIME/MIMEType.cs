﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

#endregion

namespace Operate.Web.Email.MIME
{
    /// <summary>
    /// Defines basic MIME Types
    /// </summary>
    public static class MIMEType
    {
        #region Public Variables
        /// <summary>
        /// Defines the types in string form
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2105:ArrayFieldsShouldNotBeReadOnly")]
        public static readonly string[] TypeTable = { "text", "image", "audio", "vedio", "application", "multipart", "message", null };

        /// <summary>
        /// Defines the sub types, file extensions, and media types
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2105:ArrayFieldsShouldNotBeReadOnly")]
        public static readonly MediaType[] TypeCvtTable =
            new[] {
								   // media-type, sub-type, file extension
								   new MediaType( MediaEnum.MediaApplication, "xml", "xml" ),
								   new MediaType( MediaEnum.MediaApplication, "msword", "doc" ),
								   new MediaType( MediaEnum.MediaApplication, "rtf", "rtf" ),
								   new MediaType( MediaEnum.MediaApplication, "vnd.ms-excel", "xls" ),
								   new MediaType( MediaEnum.MediaApplication, "vnd.ms-powerpoint", "ppt" ),
								   new MediaType( MediaEnum.MediaApplication, "pdf", "pdf" ),
								   new MediaType( MediaEnum.MediaApplication, "zip", "zip" ),

								   new MediaType( MediaEnum.MediaImage, "jpeg", "jpeg" ),
								   new MediaType( MediaEnum.MediaImage, "jpeg", "jpg" ),
								   new MediaType( MediaEnum.MediaImage, "gif", "gif" ),
								   new MediaType( MediaEnum.MediaImage, "tiff", "tif" ),
								   new MediaType( MediaEnum.MediaImage, "tiff", "tiff" ),

								   new MediaType( MediaEnum.MediaAudio, "basic", "wav" ),
								   new MediaType( MediaEnum.MediaAudio, "basic", "mp3" ),

								   new MediaType( MediaEnum.MediaVideo, "mpeg", "mpg" ),
								   new MediaType( MediaEnum.MediaVideo, "mpeg", "mpeg" ),

								   new MediaType( MediaEnum.MediaUnknown, "", "" )		// add new subtypes before this line
							   };
        #endregion
    }
}