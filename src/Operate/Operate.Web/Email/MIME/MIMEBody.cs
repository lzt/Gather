﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Web.Email.MIME
{
    /// <summary>
    /// Body of the MIME message
    /// </summary>
    public class MIMEBody
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MIMEBody()
        {
            Boundries = new List<MIMEMessage>();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="input">Body text</param>
        /// <param name="header">Header of the message</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.IndexOf(System.String,System.Int32,System.StringComparison)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public MIMEBody(string input,MIMEHeader header)
        {
            if (input.IsNullOrEmpty()) { throw new ArgumentNullException("input"); }
            Boundries = new List<MIMEMessage>();
            MediaEnum contentType = GetMediaType(header);
            if(MediaEnum.MediaMultipart==contentType)
            {
                string currentBoundry=GetBoundryMarker(header);
                if (string.IsNullOrEmpty(currentBoundry))
                    return;
                currentBoundry = currentBoundry.Replace("\"", "");
                
                string boundryStart = "--"+currentBoundry;
                string boundryEnd = boundryStart+"--";

                int startIndex = input.IndexOf(boundryStart, 0, StringComparison.InvariantCulture);
                if (startIndex == -1) return;
                int endIndex = input.IndexOf(boundryEnd, 0, StringComparison.InvariantCulture);
                if (endIndex == -1) endIndex = input.Length;

                Content = input.Substring(0, startIndex);
                while (startIndex < endIndex)
                {
                    startIndex += boundryStart.Length + 2;
                    if (startIndex >= endIndex)
                        break;
                    int tempIndex = input.IndexOf(boundryStart, startIndex, StringComparison.InvariantCulture);
                    if (tempIndex != -1)
                    {
                        Boundries.Add(new MIMEMessage(input.Substring(startIndex, tempIndex - startIndex)));
                    }
                    else
                        break;
                    startIndex = tempIndex;
                }
            }
            else
            {
                Content = input;
            }
            string encoding;
            try
            {
                encoding = header[Constants.TransferEncoding].Attributes[0].Value;
            }
            catch
            {
                encoding = Constants.Encoding7Bit;
            }
            Code codeUsing = CodeManager.Instance[encoding];
            codeUsing.CharacterSet = header[Constants.ContentType][Constants.Charset];
            string tempContent;
            codeUsing.Decode(Content, out tempContent);
            Content = tempContent;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Gets the media type of the message
        /// </summary>
        /// <param name="header">Header of the message</param>
        /// <returns>The media type</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)")]
        private static MediaEnum GetMediaType(MIMEHeader header)
        {
            string contentType = GetContentType(header);
            int x=0;
            foreach (string tempType in MIMEType.TypeTable)
            {
                if (tempType.Equals(contentType, StringComparison.InvariantCultureIgnoreCase))
                {
                    return (MediaEnum)x;
                }
                ++x;
            }
            return (MediaEnum)MIMEType.TypeTable.Length - 1;
        }

        /// <summary>
        /// Gets the content type
        /// </summary>
        /// <param name="header">Header of the message</param>
        /// <returns>A string containing the content type</returns>
        private static string GetContentType(MIMEHeader header)
        {
            if (header != null && header[Constants.ContentType] != null && header[Constants.ContentType].Attributes.Count > 0)
            {
                string contentType = header[Constants.ContentType].Attributes[0].Value;
                if (null != contentType)
                {
                    int index = contentType.IndexOf('/', 0);
                    if (index != -1)
                    {
                        return contentType.Substring(0, index);
                    }
                    return contentType;
                }
            }
            return "text";
        }

        /// <summary>
        /// Gets the boundary marker
        /// </summary>
        /// <param name="header">Header of the message</param>
        /// <returns>A string containing the boundary marker</returns>
        private static string GetBoundryMarker(MIMEHeader header)
        {
            return header[Constants.ContentType][Constants.Boundary];
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Boundaries found within this item (files/messages)
        /// </summary>
        public ICollection<MIMEMessage> Boundries { get; private set; }

        /// <summary>
        /// Content of this boundary/message
        /// </summary>
        public string Content{get;set;}

        #endregion
    }
}