﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Web.Email.SMTP
{
    /// <summary>
    /// Utility for sending an email
    /// </summary>
    public class EmailSender : Message
    {
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EmailSender()
        {
            Attachments = new List<Attachment>();
            EmbeddedResources = new List<LinkedResource>();
            Priority = MailPriority.Normal;
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="messageBody">The body of the message</param>
        public virtual void SendMail(string messageBody = "")
        {
            if (!string.IsNullOrEmpty(messageBody))
                Body = messageBody;
            using (var message = new MailMessage())
            {
                char[] splitter = { ',', ';' };
                string[] addressCollection = To.Split(splitter);
                foreach (string t in addressCollection)
                {
                    if (!string.IsNullOrEmpty(t.Trim()))
                        message.To.Add(t);
                }
                if (!string.IsNullOrEmpty(CC))
                {
                    addressCollection = CC.Split(splitter);
                    foreach (string t in addressCollection)
                    {
                        if (!string.IsNullOrEmpty(t.Trim()))
                            message.CC.Add(t);
                    }
                }
                if (!string.IsNullOrEmpty(Bcc))
                {
                    addressCollection = Bcc.Split(splitter);
                    foreach (string t in addressCollection)
                    {
                        if (!string.IsNullOrEmpty(t.Trim()))
                            message.Bcc.Add(t);
                    }
                }
                message.Subject = Subject;
                message.From = new MailAddress((From));
                using (AlternateView bodyView = AlternateView.CreateAlternateViewFromString(Body, null, MediaTypeNames.Text.Html))
                {
                    foreach (LinkedResource resource in EmbeddedResources)
                    {
                        bodyView.LinkedResources.Add(resource);
                    }
                    message.AlternateViews.Add(bodyView);
                    //message.Body = Body;
                    message.Priority = Priority;
                    message.SubjectEncoding = System.Text.Encoding.UTF8;
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.IsBodyHtml = true;
                    foreach (Attachment tempAttachment in Attachments)
                    {
                        message.Attachments.Add(tempAttachment);
                    }
                    using (var smtp = new SmtpClient(Server, Port))
                    {
                        if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
                        {
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential(UserName, Password);
                        }
                        smtp.EnableSsl = UseSSL;
                        smtp.Send(message);
                    }
                }
            }
        }

        /// <summary>
        /// Sends a piece of mail asynchronous
        /// </summary>
        /// <param name="messageBody">The body of the message</param>
        public virtual void SendMailAsync(string messageBody = "")
        {
            if (!string.IsNullOrEmpty(messageBody))
                Body = messageBody;
            ThreadPool.QueueUserWorkItem(delegate { SendMail(); });
        }

        /// <summary>
        /// Disposes of the object
        /// </summary>
        /// <param name="managed">Ignored in this object</param>
        protected override void Dispose(bool managed)
        {
            if (Attachments != null)
            {
                Attachments.ForEach(x => x.Dispose());
                Attachments = null;
            }
            if (EmbeddedResources != null)
            {
                EmbeddedResources.ForEach(x => x.Dispose());
                EmbeddedResources = null;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Any attachments that are included with this
        /// message.
        /// </summary>
        public ICollection<Attachment> Attachments { get; private set; }

        /// <summary>
        /// Any attachment (usually images) that need to be embedded in the message
        /// </summary>
        public ICollection<LinkedResource> EmbeddedResources { get; private set; }

        /// <summary>
        /// The priority of this message
        /// </summary>
        public MailPriority Priority { get; set; }

        /// <summary>
        /// Server Location
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// User Name for the server
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password for the server
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Port to send the information on
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Decides whether we are using STARTTLS (SSL) or not
        /// </summary>
        public bool UseSSL { get; set; }

        /// <summary>
        /// Carbon copy send (seperate email addresses with a comma)
        /// </summary>
        public string CC { get; set; }

        /// <summary>
        /// Blind carbon copy send (seperate email addresses with a comma)
        /// </summary>
        public string Bcc { get; set; }

        #endregion
    }
}