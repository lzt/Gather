﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;

#endregion

namespace Operate.Profiler.ExtensionMethods
{
    /// <summary>
    /// Holds timing/profiling related extensions
    /// </summary>
    public static class TimerExtensions
    {
        #region Functions

        #region Time

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <param name="actionToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        public static void Time(this Action actionToTime, string functionName = "")
        {
            if (actionToTime == null)
                return;
            using (new Profiler(functionName))
                actionToTime();
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <typeparam name="T">Action input type</typeparam>
        /// <param name="actionToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <param name="object1">Object 1</param>
        public static void Time<T>(this Action<T> actionToTime, T object1, string functionName = "")
        {
            if (actionToTime == null)
                return;
            using (new Profiler(functionName))
                actionToTime(object1);
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <typeparam name="T1">Action input type 1</typeparam>
        /// <typeparam name="T2">Action input type 2</typeparam>
        /// <param name="actionToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <param name="object1">Object 1</param>
        /// <param name="object2">Object 2</param>
        public static void Time<T1, T2>(this Action<T1, T2> actionToTime, T1 object1, T2 object2, string functionName = "")
        {
            if (actionToTime == null)
                return;
            using (new Profiler(functionName))
                actionToTime(object1, object2);
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <typeparam name="T1">Action input type 1</typeparam>
        /// <typeparam name="T2">Action input type 2</typeparam>
        /// <typeparam name="T3">Action input type 3</typeparam>
        /// <param name="actionToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <param name="object1">Object 1</param>
        /// <param name="object2">Object 2</param>
        /// <param name="object3">Object 3</param>
        public static void Time<T1, T2, T3>(this Action<T1, T2, T3> actionToTime, T1 object1, T2 object2, T3 object3, string functionName = "")
        {
            if (actionToTime == null)
                return;
            using (new Profiler(functionName))
                actionToTime(object1, object2, object3);
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <param name="funcToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <typeparam name="TR">Type of the value to return</typeparam>
        /// <returns>The value returned by the Func</returns>
        public static TR Time<TR>(this Func<TR> funcToTime, string functionName = "")
        {
            if (funcToTime == null)
                return default(TR);
            using (new Profiler(functionName))
                return funcToTime();
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <param name="funcToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <param name="object1">Object 1</param>
        /// <typeparam name="T1">Object type 1</typeparam>
        /// <typeparam name="TR">Type of the value to return</typeparam>
        /// <returns>The value returned by the Func</returns>
        public static TR Time<T1, TR>(this Func<T1, TR> funcToTime, T1 object1, string functionName = "")
        {
            if (funcToTime == null)
                return default(TR);
            using (new Profiler(functionName))
                return funcToTime(object1);
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <param name="funcToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <param name="object1">Object 1</param>
        /// <param name="object2">Object 2</param>
        /// <typeparam name="T1">Object type 1</typeparam>
        /// <typeparam name="T2">Object type 2</typeparam>
        /// <typeparam name="TR">Type of the value to return</typeparam>
        /// <returns>The value returned by the Func</returns>
        public static TR Time<T1, T2, TR>(this Func<T1, T2, TR> funcToTime, T1 object1, T2 object2, string functionName = "")
        {
            if (funcToTime == null)
                return default(TR);
            using (new Profiler(functionName))
                return funcToTime(object1, object2);
        }

        /// <summary>
        /// Times an action and places 
        /// </summary>
        /// <param name="funcToTime">Action to time</param>
        /// <param name="functionName">Name to associate with the action</param>
        /// <param name="object1">Object 1</param>
        /// <param name="object2">Object 2</param>
        /// <param name="object3">Object 3</param>
        /// <typeparam name="T1">Object type 1</typeparam>
        /// <typeparam name="T2">Object type 2</typeparam>
        /// <typeparam name="T3">Object type 3</typeparam>
        /// <typeparam name="TR">Type of the value to return</typeparam>
        /// <returns>The value returned by the Func</returns>
        public static TR Time<T1, T2, T3, TR>(this Func<T1, T2, T3, TR> funcToTime, T1 object1, T2 object2, T3 object3, string functionName = "")
        {
            if (funcToTime == null)
                return default(TR);
            using (new Profiler(functionName))
                return funcToTime(object1, object2, object3);
        }

        #endregion

        #endregion
    }
}