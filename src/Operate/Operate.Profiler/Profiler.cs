﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using Operate.Caching.ExtensionMethods;
using Operate.Environment.ExtensionMethods;
using Operate.ExtensionMethods;

#endregion

namespace Operate.Profiler
{
    /// <summary>
    /// Object class used to profile a function.
    /// Create at the beginning of a function in a using statement and it will automatically record the time.
    /// Note that this isn't exact and is based on when the object is destroyed
    /// </summary>
    public class Profiler : IDisposable
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        protected Profiler()
        {
            Setup("");
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="functionName">Function/identifier</param>
        public Profiler(string functionName)
        {
            Setup(functionName);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="profiles">Profiles to copy data from</param>
        protected Profiler(IEnumerable<Profiler> profiles)
        {
            Children = new List<Profiler>();
            Times = new List<long>();
            StopWatch = new StopWatch();
            Running = false;
            foreach (Profiler profile in profiles)
            {
               Level = profile.Level;
               Function = profile.Function;
               Times.Add(profile.Times);
               Children.Add(profile.Children);
               CalledFrom = profile.CalledFrom;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Total time that the profiler has taken (in milliseconds)
        /// </summary>
        public ICollection<long> Times { get; private set; }

        /// <summary>
        /// Children profiler items
        /// </summary>
        public ICollection<Profiler> Children { get; private set; }

        /// <summary>
        /// Parent profiler item
        /// </summary>
        protected Profiler Parent { get; set; }

        /// <summary>
        /// Function name
        /// </summary>
        public string Function { get; protected set; }

        /// <summary>
        /// Determines if it is running
        /// </summary>
        protected bool Running { get; set; }

        /// <summary>
        /// Level of the profiler
        /// </summary>
        protected int Level { get; set; }

        /// <summary>
        /// Where the profiler was started at
        /// </summary>
        protected string CalledFrom { get; set; }

        /// <summary>
        /// Stop watch
        /// </summary>
        protected StopWatch StopWatch { get; set; }

        /// <summary>
        /// Contains the root profiler
        /// </summary>
        public static Profiler Root
        {
            get
            {
                var returnValue = "Root_Profiler".GetFromCache<Profiler>(CacheType.Item | CacheType.Internal);
                if (returnValue == null)
                {
                    returnValue = new Profiler("Start");
                    Root = returnValue;
                }
                return returnValue;
            }
            protected set
            {
                value.Cache("Root_Profiler", CacheType.Item | CacheType.Internal);
            }
        }

        /// <summary>
        /// Contains the current profiler
        /// </summary>
        public static Profiler Current
        {
            get
            {
                var returnValue = "Current_Profiler".GetFromCache<Profiler>(CacheType.Item | CacheType.Internal);
                if (returnValue == null)
                {
                    returnValue = "Root_Profiler".GetFromCache<Profiler>(CacheType.Item | CacheType.Internal);
                    Current = returnValue;
                }
                return returnValue;
            }
            protected set
            {
                value.Cache("Current_Profiler", CacheType.Item | CacheType.Internal);
            }
        }

        #endregion

        #region Functions

        #region Dispose

        /// <summary>
        /// Disposes the object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of the objects
        /// </summary>
        /// <param name="disposing">True to dispose of all resources, false only disposes of native resources</param>
        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
                Stop();
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~Profiler()
        {
            Dispose(false);
        }

        #endregion

        #region Stop

        /// <summary>
        /// Stops the timer and registers the information
        /// </summary>
        public virtual void Stop()
        {
            if (Current.Running)
            {
                Current.Running = false;
                Current.StopWatch.Stop();
                Current.Times.Add(StopWatch.ElapsedTime);
                Current = Parent;
            }
        }

        #endregion

        #region Start

        /// <summary>
        /// Starts the timer
        /// </summary>
        public virtual void Start()
        {
            if (Current.Running)
            {
                Current.Running = false;
                Current.StopWatch.Stop();
                Current.Times.Add(Current.StopWatch.ElapsedTime);
            }
            Current.Running = true;
            Current.StopWatch.Start();
        }

        #endregion

        #region Setup

        /// <summary>
        /// Sets up the profiler
        /// </summary>
        /// <param name="function">Function/Identification name</param>
        protected virtual void Setup(string function = "")
        {
            Parent = Current;
            Profiler child = null;
            if (Parent != null)
                child = Parent.Children.FirstOrDefault(x => x == this);
            if (child == null)
            {
                if (Parent != null)
                    Parent.Children.Add(this);
                Function = function;
                Children = new List<Profiler>();
                Times = new List<long>();
                StopWatch = new StopWatch();
                Level = Parent == null ? 0 : Parent.Level + 1;
                CalledFrom = new StackTrace().GetMethods(GetType().Assembly).ToString(x => x.DeclaringType != null ? x.DeclaringType.Name + " > " + x.Name : null, "<br />");
                Running = false;
                Current = this;
            }
            else
            {
                Current = child;
            }
            Start();
        }

        #endregion

        #region StartProfiling

        /// <summary>
        /// Starts profiling
        /// </summary>
        /// <returns>The root profiler</returns>
        public static Profiler StartProfiling()
        {
            return Root;
        }

        #endregion

        #region StopProfiling

        /// <summary>
        /// Stops profiling
        /// </summary>
        /// <returns>The root profiler</returns>
        public static Profiler StopProfiling()
        {
            Root.Stop();
            return Root;
        }

        #endregion

        #region CompileData

        /// <summary>
        /// Compiles data, combining instances where appropriate
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected virtual void CompileData()
        {
            bool Continue = true;
            while (Continue)
            {
                Continue = false;
                for (int x = 0; x < Children.Count; ++x)
                {
                    IEnumerable<Profiler> combinables = Children.Where(y => y == Children.ElementAt(x)).ToList();
                    if (combinables.Count() > 1)
                    {
                        Continue = true;
                        var temp = new Profiler(combinables);
                        combinables.ForEach(y => Children.Remove(y));
                        Children.Add(temp);
                        break;
                    }
                }
            }
        }

        #endregion

        #region ToString

        /// <summary>
        /// Outputs the information to a table
        /// </summary>
        /// <returns>an html string containing the information</returns>
        public override string ToString()
        {
            CompileData();
            var builder = new StringBuilder();
            Level.Times(x => { builder.Append("\t"); });
            builder.AppendLineFormat("{0} ({1} ms)", Function, Times.Sum());
            foreach (Profiler child in Children)
            {
                builder.AppendLineFormat(child.ToString());
            }
            return builder.ToString();
        }

        #endregion

        #region Equals

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public override bool Equals(object obj)
        {
            var temp = obj as Profiler;
            if (temp == null)
                return false;
            return temp == this;
        }

        /// <summary>
        /// Compares the profilers and determines if they are equal
        /// </summary>
        /// <param name="first">First</param>
        /// <param name="second">Second</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public static bool operator ==(Profiler first, Profiler second)
        {
            if ((object)first == null && (object)second == null)
                return true;
            if ((object)first == null)
                return false;
            if ((object)second == null)
                return false;
            return first.Function == second.Function;
        }


        /// <summary>
        /// Compares the profilers and determines if they are not equal
        /// </summary>
        /// <param name="first">First</param>
        /// <param name="second">Second</param>
        /// <returns>True if they are equal, false otherwise</returns>
        public static bool operator !=(Profiler first, Profiler second)
        {
            return !(first == second);
        }

        #endregion

        #region GetHashCode

        /// <summary>
        /// Gets the hash code for the profiler
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return Function.GetHashCode();
        }

        #endregion

        #region ToHTML

        /// <summary>
        /// Outputs the profiler information as an HTML table
        /// </summary>
        /// <returns>Table containing profiler information</returns>
        public virtual string ToHTML()
        {
            CompileData();
            var builder = new StringBuilder();
            if (Level == 0)
                builder.Append("<table><tr><th>Called From</th><th>Function Name</th><th>Total Time</th><th>Max Time</th><th>Min Time</th><th>Average Time</th><th>Times Called</th></tr>");
            builder.AppendFormat(CultureInfo.InvariantCulture, "<tr><td>{0}</td><td>", CalledFrom);
            if (Level == 0)
                builder.AppendFormat(CultureInfo.InvariantCulture, "{0}</td><td>{1}ms</td><td>{2}ms</td><td>{3}ms</td><td>{4}ms</td><td>{5}</td></tr>", Function, 0, 0, 0, 0, Times.Count);
            else
                builder.AppendFormat(CultureInfo.InvariantCulture, "{0}</td><td>{1}ms</td><td>{2}ms</td><td>{3}ms</td><td>{4}ms</td><td>{5}</td></tr>", Function, Times.Sum(), Times.Max(), Times.Min(), string.Format(CultureInfo.InvariantCulture, "{0:0.##}", Times.Average()), Times.Count);
            foreach (Profiler child in Children)
            {
                builder.AppendLineFormat(child.ToHTML());
            }
            if (Level == 0)
                builder.Append("</table>");
            return builder.ToString();
        }

        #endregion

        #endregion
    }
}