﻿using System.Web.Mvc;
using Operate.Net.Mime;

namespace Operate.Web.Mvc.Common
{
    /// <summary>
    /// Controller Extension Class
    /// 控制器扩展类
    /// </summary>
    public class ControllerEx : AsyncController
    {
        #region Properties

        /// <summary>
        /// CurrentActionResultInfo
        /// </summary>
        public ActionResultInfo CurrentActionResultInfo
        {
            get
            {
                var info = new ActionResultInfo();
                var controller = Request.RequestContext.RouteData.Values["controller"] ?? "";
                var action = Request.RequestContext.RouteData.Values["action"] ?? "";
                info.ControllerName = controller.ToString();
                info.ActionResultName = action.ToString();
                return info;
            }
        }

        #endregion

        /// <summary>
        /// ControllerEx
        /// </summary>
        protected ControllerEx() : base()
        {
        }

        #region

        /// <summary>
        /// 创建一个将视图呈现给响应的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 将视图呈现给响应的 <see cref="M:System.Web.Mvc.Controller.View"/> 结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        protected internal ContentTypeViewResult ContentTypeView(MimeModel contentType)
        {
            return this.ContentTypeView(
                contentType,
                (string)null,
                (string)null,
                (object)null
            );
        }

        /// <summary>
        /// 使用模型创建一个将视图呈现给响应的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="model">视图呈现的模型。</param>
        protected internal ContentTypeViewResult ContentTypeView(MimeModel contentType, object model)
        {
            return this.ContentTypeView(
                contentType,
                (string)null,
                (string)null,
                model
            );
        }

        /// <summary>
        /// 使用视图名称创建一个呈现视图的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="viewName">为响应呈现的视图的名称。</param>
        protected internal ContentTypeViewResult ContentTypeView(MimeModel contentType, string viewName)
        {
            return this.ContentTypeView(
                contentType,
                viewName,
                (string)null,
                (object)null
            );
        }

        /// <summary>
        /// 使用视图名称和母版页名称创建一个将视图呈现给响应的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="viewName">为响应呈现的视图的名称。</param><param name="masterName">在呈现视图时要使用的母版页或模板的名称。</param>
        protected internal ContentTypeViewResult ContentTypeView(
            MimeModel contentType,
            string viewName,
            string masterName
        )
        {
            return this.ContentTypeView(
                contentType,
                viewName,
                masterName,
                (object)null
            );
        }

        /// <summary>
        /// 创建一个呈现指定的 IView 对象的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="viewName">为响应呈现的视图。</param><param name="model">视图呈现的模型。</param>
        protected internal ContentTypeViewResult ContentTypeView(MimeModel contentType, string viewName, object model)
        {
            return this.ContentTypeView(
                contentType,
                viewName,
                (string)null,
                model
            );
        }

        /// <summary>
        /// 使用视图名称、母版页名称和模型创建一个呈现视图的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="viewName">为响应呈现的视图的名称。</param><param name="masterName">在呈现视图时要使用的母版页或模板的名称。</param><param name="model">视图呈现的模型。</param>
        protected internal virtual ContentTypeViewResult ContentTypeView(
            MimeModel contentType,
            string viewName,
            string masterName,
            object model
        )
        {
            if (model != null)
                this.ViewData.Model = model;
            ContentTypeViewResult viewResult = new ContentTypeViewResult
            {
                ViewName = viewName,
                MasterName = masterName,
                ViewData = this.ViewData,
                TempData = this.TempData,
                ViewEngineCollection = this.ViewEngineCollection,
                ContentType = contentType
            };
            return viewResult;
        }

        /// <summary>
        /// 创建一个呈现指定的 IView 对象的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="view">为响应呈现的视图。</param>
        protected internal ContentTypeViewResult ContentTypeView(MimeModel contentType, IView view)
        {
            return this.ContentTypeView(
                contentType,
                view,
                (object)null
            );
        }

        /// <summary>
        /// 创建一个呈现指定的 <see cref="T:System.Web.Mvc.IView"/> 对象的 <see cref="T:System.Web.Mvc.ViewResult"/> 对象。
        /// </summary>
        /// 
        /// <returns>
        /// 视图结果。
        /// </returns>
        /// <param name="contentType">MimeType</param>
        /// <param name="view">为响应呈现的视图。</param><param name="model">视图呈现的模型。</param>
        protected internal virtual ContentTypeViewResult ContentTypeView(
            MimeModel contentType,
            IView view,
            object model
        )
        {
            if (model != null)
                this.ViewData.Model = model;
            ContentTypeViewResult viewResult = new ContentTypeViewResult
            {
                View = view,
                ViewData = this.ViewData,
                TempData = this.TempData,
                ContentType = contentType
            };
            return viewResult;
        }

        #endregion

        //Request.RequestContext.RouteData.Values["controller"]
    }
}