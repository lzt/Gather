﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using Operate.Net.Mime;

namespace Operate.Web.Mvc.Common
{
    /// <summary>
    /// ContentTypeViewResult
    /// </summary>
    public class ContentTypeViewResult : ViewResult
    {
        /// <summary>
        /// ContentType
        /// </summary>
        public MimeModel ContentType { get; set; }

        /// <summary>
        /// 在由操作调用程序调用时，向响应呈现视图。
        /// </summary>
        /// <param name="context">用于执行结果的上下文。</param><exception cref="T:System.ArgumentNullException"><paramref name="context"/> 参数为 null。</exception>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (string.IsNullOrEmpty(this.ViewName))
                this.ViewName = context.RouteData.GetRequiredString("action");
            ViewEngineResult viewEngineResult = (ViewEngineResult)null;
            if (this.View == null)
            {
                viewEngineResult = this.FindView(context);
                this.View = viewEngineResult.View;
            }
            //context.HttpContext.Response.Output.NewLine = "";
            TextWriter output = context.HttpContext.Response.Output;
            var viewContext = new ViewContext(context, this.View, this.ViewData, this.TempData, output);

            var razorView = this.View as RazorView;

            razorView.Render(viewContext, output);

            if (viewEngineResult == null)
                return;

            viewEngineResult.ViewEngine.ReleaseView(context, this.View);

            BindingFlags bind = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.GetField;
            //因为HttpWriter._charBuffer这个字符数组的长度是1024，所以推测，一旦某一段字符串超过1024就会创建一个IHttpResponseElement，然后加入到HttpWriter._buffers,HttpWriter._buffers的每一个元素都实现了IHttpResponseElement接口,具体类型可能是HttpResponseUnmanagedBufferElement，HttpSubstBlockResponseElement等类型
            ArrayList arr = (ArrayList)context.HttpContext.Response.Output.GetType().GetField("_buffers", bind).GetValue(context.HttpContext.Response.Output);

            Assembly systemWeb = Assembly.Load("System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
            Type type = systemWeb.GetType("System.Web.IHttpResponseElement");
            MethodInfo method = type.GetMethod("GetBytes");
            StringBuilder sb = new StringBuilder(5000);
            //遍历每一个buffer，获取buffer里存储的字符数组，然后转换为字符串
            for (int i = 0; i < arr.Count; i++)
            {
                byte[] buffer = (byte[])method.Invoke(arr[i], null);
                //使用当前编码得出已经存储到HttpWriter._buffers中的字符串
                sb.Append(context.HttpContext.Response.ContentEncoding.GetString(buffer));
            }
            //获取HttpWriter的字符数组缓冲区
            char[] charBuffer = (char[])context.HttpContext.Response.Output.GetType().GetField("_charBuffer", bind).GetValue(context.HttpContext.Response.Output);
            int charBufferLength = (int)context.HttpContext.Response.Output.GetType().GetField("_charBufferLength", bind).GetValue(context.HttpContext.Response.Output);
            int charBufferFree = (int)context.HttpContext.Response.Output.GetType().GetField("_charBufferFree", bind).GetValue(context.HttpContext.Response.Output);
            //charBufferLength - charBufferFree 等于字符数组缓冲区已经使用的字符数量
            for (int i = 0; i < charBufferLength - charBufferFree; i++)
            {
                sb.Append(charBuffer[i]);
            }

            var content = sb.ToString();
            content = content.TrimStart().TrimEnd();
            context.HttpContext.Response.ClearContent();
            context.HttpContext.Response.Write(content);
            context.HttpContext.Response.ContentType = (ContentType==null ? MimeCollection.Html.ContentType : ContentType.ContentType);
        }
    }
}
