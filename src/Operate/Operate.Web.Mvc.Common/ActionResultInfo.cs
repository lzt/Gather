﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Operate.Web.Mvc.Common
{
    /// <summary>
    /// 当前ActionResult信息
    /// </summary>
    public class ActionResultInfo
    {
        #region

        /// <summary>
        /// ControllerName
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// ActionResultName
        /// </summary>
        public string ActionResultName { get; set; }

        #endregion

    }
}
