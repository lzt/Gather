﻿using Operate.Net.Mime;
using System;
using System.Web.Mvc;

namespace Operate.Web.Mvc.Common.Results
{
    /// <summary>
    /// CustomJsonResult
    /// </summary>
    public class CustomJsonResult : ActionResult
    {
        /// <summary>
        /// application/json
        /// </summary>
        public string ContentType => MimeCollection.Json.ContentType;

        /// <summary>
        /// Data
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// CustomJsonResult
        /// </summary>
        public CustomJsonResult()
        {
        }

        /// <summary>
        /// CustomJsonResult
        /// </summary>
        public CustomJsonResult(object data)
        {
            Data = data;
        }

        /// <summary>
        /// ExecuteResult
        /// </summary>
        /// <param name="context"></param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var content = JsonConvertAssist.SerializeObject(Data);
            content = content.TrimStart().TrimEnd();
            context.HttpContext.Response.ClearContent();
            context.HttpContext.Response.Write(content);
            context.HttpContext.Response.ContentType = ContentType;
        }
    }
}