﻿using Newtonsoft.Json.Linq;
using Operate.Net.Mime;
using System;
using System.Collections;
using System.Dynamic;
using System.Web.Mvc;
using Operate.JsonConverters;

namespace Operate.Web.Mvc.Common.Results
{
    /// <summary>
    /// CustomDataResult
    /// </summary>
    public class CustomDataResult : ActionResult
    {
        /// <summary>
        /// 返回码
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 消息文本
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 主要数据
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 扩展数据
        /// </summary>
        public object ExtraData { get; set; }

        /// <summary>
        /// application/json
        /// </summary>
        public string ContentType => MimeCollection.Json.ContentType;

        //dynamic d = new ExpandoObject();
        //d.code = code;
        //    d.success = success;
        //    d.message = message;

        /// <summary>
        /// CustomDataResult
        /// </summary>
        /// <param name="code"></param>
        /// <param name="success"></param>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <param name="extraData"></param>
        public CustomDataResult(
            int code = 200,
            bool success = true,
            string message = "success",
            object data = null,
            object extraData = null
        )
        {
            Code = code;
            Success = success;
            Message = message;
            Data = data;
            ExtraData = extraData;
        }

        /// <summary>
        /// ExecuteResult
        /// </summary>
        /// <param name="context"></param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            dynamic d = new ExpandoObject();

            d.code = Code;
            d.success = Success;
            d.message = Message;

            if (Data != null)
            {
                if (Data is JObject jData)
                {
                    d.data = jData;
                }
                else
                {
                    IEnumerable list = Data as IList;
                    if (list != null)
                    {
                        d.list = list;
                    }
                    else
                    {
                        d.data = Data;
                    }
                }
            }

            if (ExtraData != null)
            {
                d.extra = ExtraData;
            }

            var content = JsonConvertAssist.SerializeObject(
                d,
                new LongConverter()
            );

            content = content.TrimStart().TrimEnd();

            context.HttpContext.Response.ClearContent();
            context.HttpContext.Response.Write(content);
            context.HttpContext.Response.ContentType = ContentType;
        }
    }
}