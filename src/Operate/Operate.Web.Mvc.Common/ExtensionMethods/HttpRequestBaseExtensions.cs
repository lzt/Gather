﻿using Operate.ExtensionMethods;
using Operate.Net.Mime;
using Operate.Web.ExtensionMethods;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web;

namespace Operate.Web.Mvc.Common.ExtensionMethods
{
    /// <summary>
    /// HttpRequestBaseExtensions
    /// </summary>
    public static class HttpRequestBaseExtensions
    {
        /// <summary>
        /// 获取Url参数并转换为NameValueCollection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static NameValueCollection GetUrlParams(this HttpRequestBase request)
        {
            var result = new NameValueCollection();

            if (request != null)
            {
                if (request.Url != null)
                {
                    var url = request.Url.OriginalString;

                    result = url.FormatParamToNameValueCollection();
                }
            }

            return result;
        }

        /// <summary>
        /// 获取From参数并转换为NameValueCollection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static NameValueCollection GetFromParams(this HttpRequestBase request)
        {
            var result = new NameValueCollection();

            if (request != null)
            {
                result = request.Form;
            }

            return result;
        }

        /// <summary>
        /// 获取Payload参数并转换为NameValueCollection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static NameValueCollection GetPayloadParams(this HttpRequestBase request)
        {
            var result = new NameValueCollection();

            if (request != null)
            {
                var contentType = request.ContentType;

                if (contentType.Contains(MimeCollection.Json.ContentType))
                {
                    var stream = request.InputStream;
                    if (stream.Length != 0)
                    {
                        using (var streamReader = new StreamReader(stream, Encoding.UTF8, true, 1024, true))
                        {
                            var json = streamReader.ReadToEnd();
                            result = ConvertCollection.JsonToNameValueCollection(json);

                            request.InputStream.Position = 0;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 获取整合参数并转换为NameValueCollection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static NameValueCollection GetIntegratedParams(this HttpRequestBase request)
        {
            var result = GetUrlParams(request);

            result = result.Merge(GetPayloadParams(request));
            result = result.Merge(GetFromParams(request));

            return result;
        }
    }
}