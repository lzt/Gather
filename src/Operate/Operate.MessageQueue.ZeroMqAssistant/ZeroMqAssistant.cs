﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Zmq;

namespace Operate.MessageQueue.ZeroMqAssistant
{
    /// <summary>
    /// ZeroMq辅助
    /// </summary>
    public class ZeroMqAssistant : Context
    {
        #region Properties

        /// <summary>
        /// Context
        /// </summary>
        public string Connection { get; private set; }

        ///// <summary>
        ///// Context
        ///// </summary>
        //public Context Context { get; set; }

        /// <summary>
        /// MqSocket
        /// </summary>
        public IZmqSocket MqSocket { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// ZeroMqAssistant
        /// </summary>
        /// <param name="connection"></param>
        public ZeroMqAssistant(string connection)
        {
            Connection = connection;
            MqSocket = CreateSocket(SocketType.Req);
            MqSocket.Connect(Connection);
        }

        #endregion

        #region Methods

        ///// <summary>
        ///// Send
        ///// </summary>
        ///// <param name="message"></param>
        //public void Send(string message)
        //{
        //        mqSocket.Send(message);
        //        var sd = mqSocket.Recv();
        //    }
        //}

        ///// <summary>
        ///// Recv
        ///// </summary>
        ///// <returns></returns>
        //public byte[] Recv(bool noWait = false)
        //{
        //    return MqSocket.Recv(noWait);
        //}

        #endregion
    }
}
