﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.Entity
{
    /// <summary>
    /// SiteUser实体字段名称
    /// </summary>
    public enum SiteUserField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：Host
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Host,  
        
        /// <summary>
        /// 字段：ClientSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ClientSecret,  
        
        /// <summary>
        /// 字段：ClientIdentifier
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ClientIdentifier,  
        
        /// <summary>
        /// 字段：ServerSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(4000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ServerSecret,  
        
        /// <summary>
        /// 字段：SiteType
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SiteType,  
        
        /// <summary>
        /// 字段：AppId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AppId,  
        
        /// <summary>
        /// 字段：AppSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AppSecret,  
        
        /// <summary>
        /// 字段：Salt
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Salt,  
        
        /// <summary>
        /// 字段：Callback
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Callback,  
        
        /// <summary>
        /// 字段：ExpiresTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ExpiresTime,  
        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusEnabled,  
        
        /// <summary>
        /// 字段：StatusDelete
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusDelete,  
        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Ip,  
        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateBy,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyBy,  
        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyTime,  
        
    }

    /// <summary>
    /// SiteUser实体模型
    /// </summary>
    [Serializable]
    public partial class SiteUser : ExBaseEntity<SiteUser>
	{
        #region SiteUser Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual int Id {get; set;} 
                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Name {get; set;} 
                        
        /// <summary>
        /// 字段：Host
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(1000)")]
        [Precision("")]
        [Scale("")]
        public virtual string Host {get; set;} 
                        
        /// <summary>
        /// 字段：ClientSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(2000)")]
        [Precision("")]
        [Scale("")]
        public virtual string ClientSecret {get; set;} 
                        
        /// <summary>
        /// 字段：ClientIdentifier
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(2000)")]
        [Precision("")]
        [Scale("")]
        public virtual string ClientIdentifier {get; set;} 
                        
        /// <summary>
        /// 字段：ServerSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(4000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(4000)")]
        [Precision("")]
        [Scale("")]
        public virtual string ServerSecret {get; set;} 
                        
        /// <summary>
        /// 字段：SiteType
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long SiteType {get; set;} 
                        
        /// <summary>
        /// 字段：AppId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string AppId {get; set;} 
                        
        /// <summary>
        /// 字段：AppSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string AppSecret {get; set;} 
                        
        /// <summary>
        /// 字段：Salt
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Salt {get; set;} 
                        
        /// <summary>
        /// 字段：Callback
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string Callback {get; set;} 
                        
        /// <summary>
        /// 字段：ExpiresTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime ExpiresTime {get; set;} 
                        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long StatusEnabled {get; set;} 
                        
        /// <summary>
        /// 字段：StatusDelete
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long StatusDelete {get; set;} 
                        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Ip {get; set;} 
                        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long CreateBy {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long LastModifyBy {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime LastModifyTime {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static SiteUser()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public SiteUser()
        {
            //属性初始化
            Name = "";
            Host = "";
            ClientSecret = "";
            ClientIdentifier = "";
            ServerSecret = "";
            SiteType = 0;
            AppId = "";
            AppSecret = "";
            Salt = "";
            Callback = "";
            ExpiresTime = ConstantCollection.DbDefaultDateTime;
            StatusEnabled = 0;
            StatusDelete = 0;
            Ip = "";
            CreateBy = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LastModifyBy = 0;
            LastModifyTime = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params SiteUserField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}