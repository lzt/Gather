﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.Entity
{
    /// <summary>
    /// FundFlowLog实体字段名称
    /// </summary>
    public enum FundFlowLogField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Card
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Card,  
        
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UserProfileId,  
        
        /// <summary>
        /// 字段：Money
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Money,  
        
        /// <summary>
        /// 字段：OperateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        OperateTime,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Remark,  
        
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Title,  
        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Ip,  
        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateBy,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyBy,  
        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyTime,  
        
    }

    /// <summary>
    /// FundFlowLog实体模型
    /// </summary>
    [Serializable]
    public partial class FundFlowLog : ExBaseEntity<FundFlowLog>
	{
        #region FundFlowLog Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual int Id {get; set;} 
                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Card
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Card {get; set;} 
                        
        /// <summary>
        /// 字段：UserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int UserProfileId {get; set;} 
                        
        /// <summary>
        /// 字段：Money
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Money {get; set;} 
                        
        /// <summary>
        /// 字段：OperateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime OperateTime {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Type {get; set;} 
                        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string Remark {get; set;} 
                        
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string Title {get; set;} 
                        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Ip {get; set;} 
                        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long CreateBy {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long LastModifyBy {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime LastModifyTime {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static FundFlowLog()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public FundFlowLog()
        {
            //属性初始化
            Card = "";
            UserProfileId = 0;
            Money = 0;
            OperateTime = ConstantCollection.DbDefaultDateTime;
            Type = 0;
            Remark = "";
            Title = "";
            Ip = "";
            CreateBy = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LastModifyBy = 0;
            LastModifyTime = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params FundFlowLogField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}