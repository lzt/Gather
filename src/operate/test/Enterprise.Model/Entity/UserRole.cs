﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.Entity
{
    /// <summary>
    /// UserRole实体字段名称
    /// </summary>
    public enum UserRoleField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Title,  
        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusEnabled,  
        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Remark,  
        
        /// <summary>
        /// 字段：CreatedBy
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreatedBy,  
        
        /// <summary>
        /// 字段：ModifiedBy
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ModifiedBy,  
        
        /// <summary>
        /// 字段：SpecialOption
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SpecialOption,  
        
        /// <summary>
        /// 字段：RestrictIp
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        RestrictIp,  
        
        /// <summary>
        /// 字段：IpTactics
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        IpTactics,  
        
        /// <summary>
        /// 字段：StatusMultiLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusMultiLogin,  
        
        /// <summary>
        /// 字段：TimephasedLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TimephasedLogin,  
        
        /// <summary>
        /// 字段：AllowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AllowLoginTimeBegin,  
        
        /// <summary>
        /// 字段：AllowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AllowLoginTimeEnd,  
        
        /// <summary>
        /// 字段：DisallowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DisallowLoginTimeBegin,  
        
        /// <summary>
        /// 字段：DisallowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        DisallowLoginTimeEnd,  
        
        /// <summary>
        /// 字段：StatusChanged
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusChanged,  
        
        /// <summary>
        /// 字段：Competence
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Competence,  
        
        /// <summary>
        /// 字段：UrlMarkList
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(4000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UrlMarkList,  
        
        /// <summary>
        /// 字段：Purpose
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Purpose,  
        
        /// <summary>
        /// 字段：StatusDelete
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusDelete,  
        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Ip,  
        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateBy,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyBy,  
        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyTime,  
        
    }

    /// <summary>
    /// UserRole实体模型
    /// </summary>
    [Serializable]
    public partial class UserRole : ExBaseEntity<UserRole>
	{
        #region UserRole Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual int Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string Title {get; set;} 
                        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusEnabled {get; set;} 
                        
        /// <summary>
        /// 字段：Remark
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(1000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(1000)")]
        [Precision("")]
        [Scale("")]
        public virtual string Remark {get; set;} 
                        
        /// <summary>
        /// 字段：CreatedBy
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string CreatedBy {get; set;} 
                        
        /// <summary>
        /// 字段：ModifiedBy
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string ModifiedBy {get; set;} 
                        
        /// <summary>
        /// 字段：SpecialOption
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string SpecialOption {get; set;} 
                        
        /// <summary>
        /// 字段：RestrictIp
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int RestrictIp {get; set;} 
                        
        /// <summary>
        /// 字段：IpTactics
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(2000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(2000)")]
        [Precision("")]
        [Scale("")]
        public virtual string IpTactics {get; set;} 
                        
        /// <summary>
        /// 字段：StatusMultiLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusMultiLogin {get; set;} 
                        
        /// <summary>
        /// 字段：TimephasedLogin
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TimephasedLogin {get; set;} 
                        
        /// <summary>
        /// 字段：AllowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime AllowLoginTimeBegin {get; set;} 
                        
        /// <summary>
        /// 字段：AllowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime AllowLoginTimeEnd {get; set;} 
                        
        /// <summary>
        /// 字段：DisallowLoginTimeBegin
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime DisallowLoginTimeBegin {get; set;} 
                        
        /// <summary>
        /// 字段：DisallowLoginTimeEnd
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime DisallowLoginTimeEnd {get; set;} 
                        
        /// <summary>
        /// 字段：StatusChanged
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusChanged {get; set;} 
                        
        /// <summary>
        /// 字段：Competence
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string Competence {get; set;} 
                        
        /// <summary>
        /// 字段：UrlMarkList
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(4000)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(4000)")]
        [Precision("")]
        [Scale("")]
        public virtual string UrlMarkList {get; set;} 
                        
        /// <summary>
        /// 字段：Purpose
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Purpose {get; set;} 
                        
        /// <summary>
        /// 字段：StatusDelete
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long StatusDelete {get; set;} 
                        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Ip {get; set;} 
                        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long CreateBy {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long LastModifyBy {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime LastModifyTime {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static UserRole()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public UserRole()
        {
            //属性初始化
            Title = "";
            StatusEnabled = 0;
            Remark = "";
            CreatedBy = "";
            ModifiedBy = "";
            SpecialOption = "";
            RestrictIp = 0;
            IpTactics = "";
            StatusMultiLogin = 0;
            TimephasedLogin = 0;
            AllowLoginTimeBegin = ConstantCollection.DbDefaultDateTime;
            AllowLoginTimeEnd = ConstantCollection.DbDefaultDateTime;
            DisallowLoginTimeBegin = ConstantCollection.DbDefaultDateTime;
            DisallowLoginTimeEnd = ConstantCollection.DbDefaultDateTime;
            StatusChanged = 0;
            Competence = "";
            UrlMarkList = "";
            Purpose = 0;
            StatusDelete = 0;
            Ip = "";
            CreateBy = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LastModifyBy = 0;
            LastModifyTime = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params UserRoleField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}