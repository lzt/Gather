﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.Entity
{
    /// <summary>
    /// Geographic实体字段名称
    /// </summary>
    public enum GeographicField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：ParentAreaId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ParentAreaId,  
        
        /// <summary>
        /// 字段：Letters
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Letters,  
        
        /// <summary>
        /// 字段：Abbreviation
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Abbreviation,  
        
        /// <summary>
        /// 字段：FirstLetter
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        FirstLetter,  
        
        /// <summary>
        /// 字段：PinYinAbbreviations
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        PinYinAbbreviations,  
        
        /// <summary>
        /// 字段：ZipCode
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ZipCode,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：AreaCode
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AreaCode,  
        
        /// <summary>
        /// 字段：AreaId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AreaId,  
        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Ip,  
        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateBy,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyBy,  
        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyTime,  
        
    }

    /// <summary>
    /// Geographic实体模型
    /// </summary>
    [Serializable]
    public partial class Geographic : ExBaseEntity<Geographic>
	{
        #region Geographic Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual int Id {get; set;} 
                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string Name {get; set;} 
                        
        /// <summary>
        /// 字段：ParentAreaId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int ParentAreaId {get; set;} 
                        
        /// <summary>
        /// 字段：Letters
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Letters {get; set;} 
                        
        /// <summary>
        /// 字段：Abbreviation
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Abbreviation {get; set;} 
                        
        /// <summary>
        /// 字段：FirstLetter
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string FirstLetter {get; set;} 
                        
        /// <summary>
        /// 字段：PinYinAbbreviations
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string PinYinAbbreviations {get; set;} 
                        
        /// <summary>
        /// 字段：ZipCode
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual string ZipCode {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Type {get; set;} 
                        
        /// <summary>
        /// 字段：AreaCode
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string AreaCode {get; set;} 
                        
        /// <summary>
        /// 字段：AreaId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int AreaId {get; set;} 
                        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Ip {get; set;} 
                        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long CreateBy {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long LastModifyBy {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime LastModifyTime {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static Geographic()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public Geographic()
        {
            //属性初始化
            Name = "";
            ParentAreaId = 0;
            Letters = "";
            Abbreviation = "";
            FirstLetter = "";
            PinYinAbbreviations = "";
            ZipCode = "";
            Type = 0;
            AreaCode = "";
            AreaId = 0;
            Ip = "";
            CreateBy = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LastModifyBy = 0;
            LastModifyTime = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params GeographicField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}