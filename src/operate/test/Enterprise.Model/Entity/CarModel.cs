﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.Entity
{
    /// <summary>
    /// CarModel实体字段名称
    /// </summary>
    public enum CarModelField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：ModelId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ModelId,  
        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Name,  
        
        /// <summary>
        /// 字段：SeriesId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SeriesId,  
        
        /// <summary>
        /// 字段：ModelName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        ModelName,  
        
        /// <summary>
        /// 字段：Year
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Year,  
        
        /// <summary>
        /// 字段：Displ
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Displ,  
        
        /// <summary>
        /// 字段：Price
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Price,  
        
        /// <summary>
        /// 字段：Gear
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Gear,  
        
        /// <summary>
        /// 字段：Status
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Status,  
        
        /// <summary>
        /// 字段：SIP_C_102
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_102,  
        
        /// <summary>
        /// 字段：SIP_C_103
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_103,  
        
        /// <summary>
        /// 字段：SIP_C_104
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_104,  
        
        /// <summary>
        /// 字段：SIP_C_105
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_105,  
        
        /// <summary>
        /// 字段：SIP_C_106
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_106,  
        
        /// <summary>
        /// 字段：SIP_C_293
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_293,  
        
        /// <summary>
        /// 字段：SIP_C_107
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_107,  
        
        /// <summary>
        /// 字段：SIP_C_108
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_108,  
        
        /// <summary>
        /// 字段：SIP_C_303
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_303,  
        
        /// <summary>
        /// 字段：SIP_C_112
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_112,  
        
        /// <summary>
        /// 字段：SIP_C_294
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_294,  
        
        /// <summary>
        /// 字段：SIP_C_113
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_113,  
        
        /// <summary>
        /// 字段：SIP_C_114
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_114,  
        
        /// <summary>
        /// 字段：SIP_C_304
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_304,  
        
        /// <summary>
        /// 字段：SIP_C_115
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_115,  
        
        /// <summary>
        /// 字段：SIP_C_116
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_116,  
        
        /// <summary>
        /// 字段：SIP_C_295
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_295,  
        
        /// <summary>
        /// 字段：SIP_C_117
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_117,  
        
        /// <summary>
        /// 字段：SIP_C_118
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_118,  
        
        /// <summary>
        /// 字段：SIP_C_119
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_119,  
        
        /// <summary>
        /// 字段：SIP_C_120
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_120,  
        
        /// <summary>
        /// 字段：SIP_C_121
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_121,  
        
        /// <summary>
        /// 字段：SIP_C_122
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_122,  
        
        /// <summary>
        /// 字段：SIP_C_123
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_123,  
        
        /// <summary>
        /// 字段：SIP_C_124
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_124,  
        
        /// <summary>
        /// 字段：SIP_C_125
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_125,  
        
        /// <summary>
        /// 字段：SIP_C_126
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_126,  
        
        /// <summary>
        /// 字段：SIP_C_127
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_127,  
        
        /// <summary>
        /// 字段：SIP_C_128
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_128,  
        
        /// <summary>
        /// 字段：SIP_C_129
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_129,  
        
        /// <summary>
        /// 字段：SIP_C_130
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_130,  
        
        /// <summary>
        /// 字段：SIP_C_131
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_131,  
        
        /// <summary>
        /// 字段：SIP_C_132
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_132,  
        
        /// <summary>
        /// 字段：SIP_C_133
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_133,  
        
        /// <summary>
        /// 字段：SIP_C_134
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_134,  
        
        /// <summary>
        /// 字段：SIP_C_135
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_135,  
        
        /// <summary>
        /// 字段：SIP_C_136
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_136,  
        
        /// <summary>
        /// 字段：SIP_C_137
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_137,  
        
        /// <summary>
        /// 字段：SIP_C_138
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_138,  
        
        /// <summary>
        /// 字段：SIP_C_139
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_139,  
        
        /// <summary>
        /// 字段：SIP_C_140
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_140,  
        
        /// <summary>
        /// 字段：SIP_C_141
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_141,  
        
        /// <summary>
        /// 字段：SIP_C_142
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_142,  
        
        /// <summary>
        /// 字段：SIP_C_143
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_143,  
        
        /// <summary>
        /// 字段：SIP_C_297
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_297,  
        
        /// <summary>
        /// 字段：SIP_C_298
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_298,  
        
        /// <summary>
        /// 字段：SIP_C_299
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_299,  
        
        /// <summary>
        /// 字段：SIP_C_148
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_148,  
        
        /// <summary>
        /// 字段：SIP_C_305
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_305,  
        
        /// <summary>
        /// 字段：SIP_C_306
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_306,  
        
        /// <summary>
        /// 字段：SIP_C_307
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_307,  
        
        /// <summary>
        /// 字段：SIP_C_308
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_308,  
        
        /// <summary>
        /// 字段：SIP_C_309
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_309,  
        
        /// <summary>
        /// 字段：SIP_C_310
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_310,  
        
        /// <summary>
        /// 字段：SIP_C_311
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_311,  
        
        /// <summary>
        /// 字段：SIP_C_149
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_149,  
        
        /// <summary>
        /// 字段：SIP_C_150
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_150,  
        
        /// <summary>
        /// 字段：SIP_C_151
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_151,  
        
        /// <summary>
        /// 字段：SIP_C_152
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_152,  
        
        /// <summary>
        /// 字段：SIP_C_153
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_153,  
        
        /// <summary>
        /// 字段：SIP_C_154
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_154,  
        
        /// <summary>
        /// 字段：SIP_C_155
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_155,  
        
        /// <summary>
        /// 字段：SIP_C_156
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_156,  
        
        /// <summary>
        /// 字段：SIP_C_157
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_157,  
        
        /// <summary>
        /// 字段：SIP_C_158
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_158,  
        
        /// <summary>
        /// 字段：SIP_C_159
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_159,  
        
        /// <summary>
        /// 字段：SIP_C_160
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_160,  
        
        /// <summary>
        /// 字段：SIP_C_161
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_161,  
        
        /// <summary>
        /// 字段：SIP_C_162
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_162,  
        
        /// <summary>
        /// 字段：SIP_C_163
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_163,  
        
        /// <summary>
        /// 字段：SIP_C_164
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_164,  
        
        /// <summary>
        /// 字段：SIP_C_165
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_165,  
        
        /// <summary>
        /// 字段：SIP_C_166
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_166,  
        
        /// <summary>
        /// 字段：SIP_C_167
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_167,  
        
        /// <summary>
        /// 字段：SIP_C_168
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_168,  
        
        /// <summary>
        /// 字段：SIP_C_169
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_169,  
        
        /// <summary>
        /// 字段：SIP_C_170
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_170,  
        
        /// <summary>
        /// 字段：SIP_C_171
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_171,  
        
        /// <summary>
        /// 字段：SIP_C_172
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_172,  
        
        /// <summary>
        /// 字段：SIP_C_173
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_173,  
        
        /// <summary>
        /// 字段：SIP_C_174
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_174,  
        
        /// <summary>
        /// 字段：SIP_C_177
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_177,  
        
        /// <summary>
        /// 字段：SIP_C_178
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_178,  
        
        /// <summary>
        /// 字段：SIP_C_179
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_179,  
        
        /// <summary>
        /// 字段：SIP_C_180
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_180,  
        
        /// <summary>
        /// 字段：SIP_C_181
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_181,  
        
        /// <summary>
        /// 字段：SIP_C_183
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_183,  
        
        /// <summary>
        /// 字段：SIP_C_184
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_184,  
        
        /// <summary>
        /// 字段：SIP_C_185
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_185,  
        
        /// <summary>
        /// 字段：SIP_C_186
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_186,  
        
        /// <summary>
        /// 字段：SIP_C_187
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_187,  
        
        /// <summary>
        /// 字段：SIP_C_188
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_188,  
        
        /// <summary>
        /// 字段：SIP_C_189
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_189,  
        
        /// <summary>
        /// 字段：SIP_C_190
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_190,  
        
        /// <summary>
        /// 字段：SIP_C_191
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_191,  
        
        /// <summary>
        /// 字段：SIP_C_192
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_192,  
        
        /// <summary>
        /// 字段：SIP_C_193
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_193,  
        
        /// <summary>
        /// 字段：SIP_C_194
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_194,  
        
        /// <summary>
        /// 字段：SIP_C_195
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_195,  
        
        /// <summary>
        /// 字段：SIP_C_196
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_196,  
        
        /// <summary>
        /// 字段：SIP_C_197
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_197,  
        
        /// <summary>
        /// 字段：SIP_C_198
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_198,  
        
        /// <summary>
        /// 字段：SIP_C_199
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_199,  
        
        /// <summary>
        /// 字段：SIP_C_204
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_204,  
        
        /// <summary>
        /// 字段：SIP_C_205
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_205,  
        
        /// <summary>
        /// 字段：SIP_C_312
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_312,  
        
        /// <summary>
        /// 字段：SIP_C_313
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_313,  
        
        /// <summary>
        /// 字段：SIP_C_314
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_314,  
        
        /// <summary>
        /// 字段：SIP_C_315
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_315,  
        
        /// <summary>
        /// 字段：SIP_C_316
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_316,  
        
        /// <summary>
        /// 字段：SIP_C_210
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_210,  
        
        /// <summary>
        /// 字段：SIP_C_211
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_211,  
        
        /// <summary>
        /// 字段：SIP_C_212
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_212,  
        
        /// <summary>
        /// 字段：SIP_C_300
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_300,  
        
        /// <summary>
        /// 字段：SIP_C_213
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_213,  
        
        /// <summary>
        /// 字段：SIP_C_214
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_214,  
        
        /// <summary>
        /// 字段：SIP_C_215
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_215,  
        
        /// <summary>
        /// 字段：SIP_C_216
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_216,  
        
        /// <summary>
        /// 字段：SIP_C_217
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_217,  
        
        /// <summary>
        /// 字段：SIP_C_218
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_218,  
        
        /// <summary>
        /// 字段：SIP_C_175
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_175,  
        
        /// <summary>
        /// 字段：SIP_C_176
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_176,  
        
        /// <summary>
        /// 字段：SIP_C_219
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_219,  
        
        /// <summary>
        /// 字段：SIP_C_220
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_220,  
        
        /// <summary>
        /// 字段：SIP_C_200
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_200,  
        
        /// <summary>
        /// 字段：SIP_C_201
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_201,  
        
        /// <summary>
        /// 字段：SIP_C_202
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_202,  
        
        /// <summary>
        /// 字段：SIP_C_203
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_203,  
        
        /// <summary>
        /// 字段：SIP_C_221
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_221,  
        
        /// <summary>
        /// 字段：SIP_C_222
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_222,  
        
        /// <summary>
        /// 字段：SIP_C_223
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_223,  
        
        /// <summary>
        /// 字段：SIP_C_301
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_301,  
        
        /// <summary>
        /// 字段：SIP_C_224
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_224,  
        
        /// <summary>
        /// 字段：SIP_C_225
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_225,  
        
        /// <summary>
        /// 字段：SIP_C_226
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_226,  
        
        /// <summary>
        /// 字段：SIP_C_227
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_227,  
        
        /// <summary>
        /// 字段：SIP_C_228
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_228,  
        
        /// <summary>
        /// 字段：SIP_C_229
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_229,  
        
        /// <summary>
        /// 字段：SIP_C_230
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_230,  
        
        /// <summary>
        /// 字段：SIP_C_317
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_317,  
        
        /// <summary>
        /// 字段：SIP_C_233
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_233,  
        
        /// <summary>
        /// 字段：SIP_C_234
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_234,  
        
        /// <summary>
        /// 字段：SIP_C_235
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_235,  
        
        /// <summary>
        /// 字段：SIP_C_236
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_236,  
        
        /// <summary>
        /// 字段：SIP_C_237
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_237,  
        
        /// <summary>
        /// 字段：SIP_C_238
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_238,  
        
        /// <summary>
        /// 字段：SIP_C_239
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_239,  
        
        /// <summary>
        /// 字段：SIP_C_240
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_240,  
        
        /// <summary>
        /// 字段：SIP_C_241
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_241,  
        
        /// <summary>
        /// 字段：SIP_C_242
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_242,  
        
        /// <summary>
        /// 字段：SIP_C_321
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_321,  
        
        /// <summary>
        /// 字段：SIP_C_247
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_247,  
        
        /// <summary>
        /// 字段：SIP_C_248
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_248,  
        
        /// <summary>
        /// 字段：SIP_C_249
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_249,  
        
        /// <summary>
        /// 字段：SIP_C_250
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_250,  
        
        /// <summary>
        /// 字段：SIP_C_251
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_251,  
        
        /// <summary>
        /// 字段：SIP_C_252
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_252,  
        
        /// <summary>
        /// 字段：SIP_C_253
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_253,  
        
        /// <summary>
        /// 字段：SIP_C_254
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_254,  
        
        /// <summary>
        /// 字段：SIP_C_255
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_255,  
        
        /// <summary>
        /// 字段：SIP_C_256
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_256,  
        
        /// <summary>
        /// 字段：SIP_C_257
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_257,  
        
        /// <summary>
        /// 字段：SIP_C_258
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_258,  
        
        /// <summary>
        /// 字段：SIP_C_318
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_318,  
        
        /// <summary>
        /// 字段：SIP_C_260
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_260,  
        
        /// <summary>
        /// 字段：SIP_C_261
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_261,  
        
        /// <summary>
        /// 字段：SIP_C_262
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_262,  
        
        /// <summary>
        /// 字段：SIP_C_263
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_263,  
        
        /// <summary>
        /// 字段：SIP_C_264
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_264,  
        
        /// <summary>
        /// 字段：SIP_C_265
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_265,  
        
        /// <summary>
        /// 字段：SIP_C_266
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_266,  
        
        /// <summary>
        /// 字段：SIP_C_267
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_267,  
        
        /// <summary>
        /// 字段：SIP_C_268
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_268,  
        
        /// <summary>
        /// 字段：SIP_C_269
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_269,  
        
        /// <summary>
        /// 字段：SIP_C_270
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_270,  
        
        /// <summary>
        /// 字段：SIP_C_271
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_271,  
        
        /// <summary>
        /// 字段：SIP_C_277
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_277,  
        
        /// <summary>
        /// 字段：SIP_C_278
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_278,  
        
        /// <summary>
        /// 字段：SIP_C_279
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_279,  
        
        /// <summary>
        /// 字段：SIP_C_282
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_282,  
        
        /// <summary>
        /// 字段：SIP_C_319
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_319,  
        
        /// <summary>
        /// 字段：SIP_C_272
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_272,  
        
        /// <summary>
        /// 字段：SIP_C_273
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_273,  
        
        /// <summary>
        /// 字段：SIP_C_274
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_274,  
        
        /// <summary>
        /// 字段：SIP_C_275
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_275,  
        
        /// <summary>
        /// 字段：SIP_C_276
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_276,  
        
        /// <summary>
        /// 字段：SIP_C_320
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_320,  
        
        /// <summary>
        /// 字段：SIP_C_285
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_285,  
        
        /// <summary>
        /// 字段：SIP_C_286
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_286,  
        
        /// <summary>
        /// 字段：SIP_C_287
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_287,  
        
        /// <summary>
        /// 字段：SIP_C_288
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_288,  
        
        /// <summary>
        /// 字段：SIP_C_289
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_289,  
        
        /// <summary>
        /// 字段：SIP_C_290
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_290,  
        
        /// <summary>
        /// 字段：SIP_C_291
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_291,  
        
        /// <summary>
        /// 字段：SIP_C_292
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SIP_C_292,  
        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Ip,  
        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateBy,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyBy,  
        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LastModifyTime,  
        
    }

    /// <summary>
    /// CarModel实体模型
    /// </summary>
    [Serializable]
    public partial class CarModel : ExBaseEntity<CarModel>
	{
        #region CarModel Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
        [AutoIncrement]
        public virtual int Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：ModelId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int ModelId {get; set;} 
                        
        /// <summary>
        /// 字段：Name
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Name {get; set;} 
                        
        /// <summary>
        /// 字段：SeriesId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int SeriesId {get; set;} 
                        
        /// <summary>
        /// 字段：ModelName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string ModelName {get; set;} 
                        
        /// <summary>
        /// 字段：Year
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Year {get; set;} 
                        
        /// <summary>
        /// 字段：Displ
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("float(8)")]
        [Precision("")]
        [Scale("")]
        public virtual double Displ {get; set;} 
                        
        /// <summary>
        /// 字段：Price
        /// 描述：
        /// 映射类型：double
        /// 数据库中字段类型：float(8)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("float(8)")]
        [Precision("")]
        [Scale("")]
        public virtual double Price {get; set;} 
                        
        /// <summary>
        /// 字段：Gear
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(10)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(10)")]
        [Precision("")]
        [Scale("")]
        public virtual string Gear {get; set;} 
                        
        /// <summary>
        /// 字段：Status
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Status {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_102
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_102 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_103
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_103 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_104
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_104 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_105
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_105 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_106
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_106 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_293
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_293 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_107
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_107 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_108
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_108 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_303
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_303 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_112
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_112 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_294
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_294 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_113
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_113 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_114
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_114 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_304
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_304 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_115
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_115 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_116
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_116 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_295
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_295 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_117
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_117 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_118
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_118 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_119
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_119 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_120
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_120 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_121
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_121 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_122
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_122 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_123
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_123 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_124
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_124 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_125
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_125 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_126
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_126 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_127
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_127 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_128
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_128 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_129
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_129 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_130
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_130 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_131
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_131 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_132
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_132 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_133
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_133 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_134
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_134 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_135
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_135 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_136
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_136 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_137
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_137 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_138
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_138 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_139
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_139 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_140
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_140 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_141
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_141 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_142
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_142 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_143
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_143 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_297
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_297 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_298
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_298 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_299
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_299 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_148
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_148 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_305
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_305 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_306
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_306 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_307
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_307 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_308
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_308 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_309
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_309 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_310
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_310 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_311
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_311 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_149
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_149 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_150
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_150 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_151
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_151 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_152
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_152 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_153
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_153 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_154
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_154 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_155
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_155 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_156
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_156 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_157
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_157 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_158
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_158 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_159
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_159 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_160
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_160 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_161
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_161 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_162
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_162 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_163
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_163 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_164
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_164 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_165
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_165 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_166
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_166 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_167
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_167 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_168
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_168 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_169
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_169 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_170
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_170 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_171
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_171 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_172
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_172 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_173
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_173 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_174
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_174 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_177
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_177 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_178
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_178 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_179
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_179 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_180
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_180 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_181
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_181 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_183
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_183 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_184
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_184 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_185
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_185 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_186
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_186 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_187
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_187 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_188
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_188 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_189
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_189 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_190
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_190 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_191
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_191 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_192
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_192 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_193
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_193 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_194
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_194 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_195
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_195 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_196
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_196 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_197
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_197 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_198
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_198 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_199
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_199 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_204
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_204 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_205
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_205 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_312
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_312 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_313
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_313 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_314
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_314 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_315
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_315 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_316
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_316 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_210
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_210 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_211
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_211 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_212
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_212 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_300
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_300 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_213
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_213 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_214
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_214 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_215
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_215 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_216
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_216 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_217
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_217 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_218
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_218 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_175
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_175 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_176
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_176 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_219
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_219 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_220
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_220 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_200
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_200 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_201
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_201 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_202
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_202 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_203
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_203 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_221
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_221 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_222
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_222 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_223
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_223 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_301
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_301 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_224
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_224 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_225
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_225 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_226
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_226 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_227
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_227 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_228
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_228 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_229
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_229 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_230
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_230 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_317
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_317 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_233
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_233 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_234
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_234 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_235
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_235 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_236
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_236 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_237
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_237 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_238
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_238 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_239
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_239 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_240
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_240 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_241
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_241 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_242
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_242 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_321
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_321 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_247
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_247 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_248
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_248 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_249
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_249 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_250
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_250 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_251
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_251 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_252
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_252 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_253
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_253 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_254
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_254 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_255
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_255 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_256
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_256 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_257
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_257 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_258
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_258 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_318
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_318 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_260
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_260 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_261
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_261 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_262
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_262 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_263
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_263 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_264
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_264 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_265
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_265 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_266
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_266 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_267
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_267 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_268
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_268 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_269
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_269 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_270
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_270 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_271
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_271 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_277
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_277 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_278
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_278 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_279
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_279 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_282
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_282 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_319
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_319 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_272
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_272 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_273
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_273 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_274
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_274 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_275
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_275 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_276
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_276 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_320
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_320 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_285
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_285 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_286
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_286 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_287
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_287 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_288
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_288 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_289
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_289 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_290
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_290 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_291
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_291 {get; set;} 
                        
        /// <summary>
        /// 字段：SIP_C_292
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string SIP_C_292 {get; set;} 
                        
        /// <summary>
        /// 字段：Ip
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Ip {get; set;} 
                        
        /// <summary>
        /// 字段：CreateBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long CreateBy {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyBy
        /// 描述：
        /// 映射类型：long
        /// 数据库中字段类型：INTEGER
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("INTEGER")]
        [Precision("")]
        [Scale("")]
        public virtual long LastModifyBy {get; set;} 
                        
        /// <summary>
        /// 字段：LastModifyTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime LastModifyTime {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static CarModel()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public CarModel()
        {
            //属性初始化
            ModelId = 0;
            Name = "";
            SeriesId = 0;
            ModelName = "";
            Year = 0;
            Displ = 0;
            Price = 0;
            Gear = "";
            Status = 0;
            SIP_C_102 = "";
            SIP_C_103 = "";
            SIP_C_104 = "";
            SIP_C_105 = "";
            SIP_C_106 = "";
            SIP_C_293 = "";
            SIP_C_107 = "";
            SIP_C_108 = "";
            SIP_C_303 = "";
            SIP_C_112 = "";
            SIP_C_294 = "";
            SIP_C_113 = "";
            SIP_C_114 = "";
            SIP_C_304 = "";
            SIP_C_115 = "";
            SIP_C_116 = "";
            SIP_C_295 = "";
            SIP_C_117 = "";
            SIP_C_118 = "";
            SIP_C_119 = "";
            SIP_C_120 = "";
            SIP_C_121 = "";
            SIP_C_122 = "";
            SIP_C_123 = "";
            SIP_C_124 = "";
            SIP_C_125 = "";
            SIP_C_126 = "";
            SIP_C_127 = "";
            SIP_C_128 = "";
            SIP_C_129 = "";
            SIP_C_130 = "";
            SIP_C_131 = "";
            SIP_C_132 = "";
            SIP_C_133 = "";
            SIP_C_134 = "";
            SIP_C_135 = "";
            SIP_C_136 = "";
            SIP_C_137 = "";
            SIP_C_138 = "";
            SIP_C_139 = "";
            SIP_C_140 = "";
            SIP_C_141 = "";
            SIP_C_142 = "";
            SIP_C_143 = "";
            SIP_C_297 = "";
            SIP_C_298 = "";
            SIP_C_299 = "";
            SIP_C_148 = "";
            SIP_C_305 = "";
            SIP_C_306 = "";
            SIP_C_307 = "";
            SIP_C_308 = "";
            SIP_C_309 = "";
            SIP_C_310 = "";
            SIP_C_311 = "";
            SIP_C_149 = "";
            SIP_C_150 = "";
            SIP_C_151 = "";
            SIP_C_152 = "";
            SIP_C_153 = "";
            SIP_C_154 = "";
            SIP_C_155 = "";
            SIP_C_156 = "";
            SIP_C_157 = "";
            SIP_C_158 = "";
            SIP_C_159 = "";
            SIP_C_160 = "";
            SIP_C_161 = "";
            SIP_C_162 = "";
            SIP_C_163 = "";
            SIP_C_164 = "";
            SIP_C_165 = "";
            SIP_C_166 = "";
            SIP_C_167 = "";
            SIP_C_168 = "";
            SIP_C_169 = "";
            SIP_C_170 = "";
            SIP_C_171 = "";
            SIP_C_172 = "";
            SIP_C_173 = "";
            SIP_C_174 = "";
            SIP_C_177 = "";
            SIP_C_178 = "";
            SIP_C_179 = "";
            SIP_C_180 = "";
            SIP_C_181 = "";
            SIP_C_183 = "";
            SIP_C_184 = "";
            SIP_C_185 = "";
            SIP_C_186 = "";
            SIP_C_187 = "";
            SIP_C_188 = "";
            SIP_C_189 = "";
            SIP_C_190 = "";
            SIP_C_191 = "";
            SIP_C_192 = "";
            SIP_C_193 = "";
            SIP_C_194 = "";
            SIP_C_195 = "";
            SIP_C_196 = "";
            SIP_C_197 = "";
            SIP_C_198 = "";
            SIP_C_199 = "";
            SIP_C_204 = "";
            SIP_C_205 = "";
            SIP_C_312 = "";
            SIP_C_313 = "";
            SIP_C_314 = "";
            SIP_C_315 = "";
            SIP_C_316 = "";
            SIP_C_210 = "";
            SIP_C_211 = "";
            SIP_C_212 = "";
            SIP_C_300 = "";
            SIP_C_213 = "";
            SIP_C_214 = "";
            SIP_C_215 = "";
            SIP_C_216 = "";
            SIP_C_217 = "";
            SIP_C_218 = "";
            SIP_C_175 = "";
            SIP_C_176 = "";
            SIP_C_219 = "";
            SIP_C_220 = "";
            SIP_C_200 = "";
            SIP_C_201 = "";
            SIP_C_202 = "";
            SIP_C_203 = "";
            SIP_C_221 = "";
            SIP_C_222 = "";
            SIP_C_223 = "";
            SIP_C_301 = "";
            SIP_C_224 = "";
            SIP_C_225 = "";
            SIP_C_226 = "";
            SIP_C_227 = "";
            SIP_C_228 = "";
            SIP_C_229 = "";
            SIP_C_230 = "";
            SIP_C_317 = "";
            SIP_C_233 = "";
            SIP_C_234 = "";
            SIP_C_235 = "";
            SIP_C_236 = "";
            SIP_C_237 = "";
            SIP_C_238 = "";
            SIP_C_239 = "";
            SIP_C_240 = "";
            SIP_C_241 = "";
            SIP_C_242 = "";
            SIP_C_321 = "";
            SIP_C_247 = "";
            SIP_C_248 = "";
            SIP_C_249 = "";
            SIP_C_250 = "";
            SIP_C_251 = "";
            SIP_C_252 = "";
            SIP_C_253 = "";
            SIP_C_254 = "";
            SIP_C_255 = "";
            SIP_C_256 = "";
            SIP_C_257 = "";
            SIP_C_258 = "";
            SIP_C_318 = "";
            SIP_C_260 = "";
            SIP_C_261 = "";
            SIP_C_262 = "";
            SIP_C_263 = "";
            SIP_C_264 = "";
            SIP_C_265 = "";
            SIP_C_266 = "";
            SIP_C_267 = "";
            SIP_C_268 = "";
            SIP_C_269 = "";
            SIP_C_270 = "";
            SIP_C_271 = "";
            SIP_C_277 = "";
            SIP_C_278 = "";
            SIP_C_279 = "";
            SIP_C_282 = "";
            SIP_C_319 = "";
            SIP_C_272 = "";
            SIP_C_273 = "";
            SIP_C_274 = "";
            SIP_C_275 = "";
            SIP_C_276 = "";
            SIP_C_320 = "";
            SIP_C_285 = "";
            SIP_C_286 = "";
            SIP_C_287 = "";
            SIP_C_288 = "";
            SIP_C_289 = "";
            SIP_C_290 = "";
            SIP_C_291 = "";
            SIP_C_292 = "";
            Ip = "";
            CreateBy = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            LastModifyBy = 0;
            LastModifyTime = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params CarModelField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}