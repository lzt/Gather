﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using Operate.DataBase.SqlDatabase.Model;
using Operate.DataBase.SqlDatabase.NHibernateAssistant.Model;

namespace Enterprise.Model.Base
{
    public class ExBaseEntity<TDatatable> : GenericBaseEntity<TDatatable>  where TDatatable : BaseEntity, new() 
    {

    }
}
