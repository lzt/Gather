﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.ViewEntity
{
    /// <summary>
    /// UserMessageView实体字段名称
    /// </summary>
    public enum UserMessageViewField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：SourceUserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SourceUserProfileId,  
        
        /// <summary>
        /// 字段：TargetUserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TargetUserProfileId,  
        
        /// <summary>
        /// 字段：Content
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Content,  
        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Type,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Title,  
        
        /// <summary>
        /// 字段：TargetStatusRead
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TargetStatusRead,  
        
        /// <summary>
        /// 字段：TargetStatusDelete
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TargetStatusDelete,  
        
        /// <summary>
        /// 字段：SourceUserLoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SourceUserLoginId,  
        
        /// <summary>
        /// 字段：SourceUserAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        SourceUserAvatar,  
        
        /// <summary>
        /// 字段：TargetUserLoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TargetUserLoginId,  
        
        /// <summary>
        /// 字段：TargetUserAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TargetUserAvatar,  
        
    }

    /// <summary>
    /// UserMessageView实体模型
    /// </summary>
    [Serializable]
    public partial class UserMessageView : ExBaseEntity<UserMessageView>
	{
        #region UserMessageView Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
                public virtual int Id {get; set;} 
                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：SourceUserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int SourceUserProfileId {get; set;} 
                        
        /// <summary>
        /// 字段：TargetUserProfileId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TargetUserProfileId {get; set;} 
                        
        /// <summary>
        /// 字段：Content
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string Content {get; set;} 
                        
        /// <summary>
        /// 字段：Type
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Type {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：Title
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(500)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(500)")]
        [Precision("")]
        [Scale("")]
        public virtual string Title {get; set;} 
                        
        /// <summary>
        /// 字段：TargetStatusRead
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TargetStatusRead {get; set;} 
                        
        /// <summary>
        /// 字段：TargetStatusDelete
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TargetStatusDelete {get; set;} 
                        
        /// <summary>
        /// 字段：SourceUserLoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string SourceUserLoginId {get; set;} 
                        
        /// <summary>
        /// 字段：SourceUserAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string SourceUserAvatar {get; set;} 
                        
        /// <summary>
        /// 字段：TargetUserLoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string TargetUserLoginId {get; set;} 
                        
        /// <summary>
        /// 字段：TargetUserAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string TargetUserAvatar {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static UserMessageView()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public UserMessageView()
        {
            //属性初始化
            Id = 0;
            SourceUserProfileId = 0;
            TargetUserProfileId = 0;
            Content = "";
            Type = 0;
            CreateTime = ConstantCollection.DbDefaultDateTime;
            Title = "";
            TargetStatusRead = 0;
            TargetStatusDelete = 0;
            SourceUserLoginId = "";
            SourceUserAvatar = "";
            TargetUserLoginId = "";
            TargetUserAvatar = "";
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params UserMessageViewField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}