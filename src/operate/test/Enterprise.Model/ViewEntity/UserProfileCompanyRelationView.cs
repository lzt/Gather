﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.ViewEntity
{
    /// <summary>
    /// UserProfileCompanyRelationView实体字段名称
    /// </summary>
    public enum UserProfileCompanyRelationViewField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：RelationId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        RelationId,  
        
        /// <summary>
        /// 字段：CompanyId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CompanyId,  
        
        /// <summary>
        /// 字段：Position
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Position,  
        
        /// <summary>
        /// 字段：StatusIsGovernor
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusIsGovernor,  
        
        /// <summary>
        /// 字段：LoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LoginId,  
        
        /// <summary>
        /// 字段：NickName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        NickName,  
        
        /// <summary>
        /// 字段：UserPhone
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UserPhone,  
        
        /// <summary>
        /// 字段：Identification
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Identification,  
        
        /// <summary>
        /// 字段：TrueName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TrueName,  
        
        /// <summary>
        /// 字段：UserAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UserAvatar,  
        
        /// <summary>
        /// 字段：Sex
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Sex,  
        
        /// <summary>
        /// 字段：AppId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AppId,  
        
        /// <summary>
        /// 字段：AppSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        AppSecret,  
        
        /// <summary>
        /// 字段：UserAddress
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        UserAddress,  
        
        /// <summary>
        /// 字段：StatusInvalid
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusInvalid,  
        
        /// <summary>
        /// 字段：Birthday
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Birthday,  
        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        StatusEnabled,  
        
        /// <summary>
        /// 字段：CompanyName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CompanyName,  
        
        /// <summary>
        /// 字段：CompanyCorporation
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CompanyCorporation,  
        
        /// <summary>
        /// 字段：CompanyAddress
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CompanyAddress,  
        
        /// <summary>
        /// 字段：CompanyAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CompanyAvatar,  
        
        /// <summary>
        /// 字段：CompanyRegistrationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CompanyRegistrationTime,  
        
    }

    /// <summary>
    /// UserProfileCompanyRelationView实体模型
    /// </summary>
    [Serializable]
    public partial class UserProfileCompanyRelationView : ExBaseEntity<UserProfileCompanyRelationView>
	{
        #region UserProfileCompanyRelationView Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
                public virtual int Id {get; set;} 
                                                                                                                                                                                                                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：RelationId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        public virtual int RelationId {get; set;} 
                        
        /// <summary>
        /// 字段：CompanyId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int CompanyId {get; set;} 
                        
        /// <summary>
        /// 字段：Position
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string Position {get; set;} 
                        
        /// <summary>
        /// 字段：StatusIsGovernor
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusIsGovernor {get; set;} 
                        
        /// <summary>
        /// 字段：LoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string LoginId {get; set;} 
                        
        /// <summary>
        /// 字段：NickName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string NickName {get; set;} 
                        
        /// <summary>
        /// 字段：UserPhone
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string UserPhone {get; set;} 
                        
        /// <summary>
        /// 字段：Identification
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string Identification {get; set;} 
                        
        /// <summary>
        /// 字段：TrueName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(100)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(100)")]
        [Precision("")]
        [Scale("")]
        public virtual string TrueName {get; set;} 
                        
        /// <summary>
        /// 字段：UserAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string UserAvatar {get; set;} 
                        
        /// <summary>
        /// 字段：Sex
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Sex {get; set;} 
                        
        /// <summary>
        /// 字段：AppId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string AppId {get; set;} 
                        
        /// <summary>
        /// 字段：AppSecret
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(300)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(300)")]
        [Precision("")]
        [Scale("")]
        public virtual string AppSecret {get; set;} 
                        
        /// <summary>
        /// 字段：UserAddress
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string UserAddress {get; set;} 
                        
        /// <summary>
        /// 字段：StatusInvalid
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusInvalid {get; set;} 
                        
        /// <summary>
        /// 字段：Birthday
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime Birthday {get; set;} 
                        
        /// <summary>
        /// 字段：StatusEnabled
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int StatusEnabled {get; set;} 
                        
        /// <summary>
        /// 字段：CompanyName
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string CompanyName {get; set;} 
                        
        /// <summary>
        /// 字段：CompanyCorporation
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(200)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(200)")]
        [Precision("")]
        [Scale("")]
        public virtual string CompanyCorporation {get; set;} 
                        
        /// <summary>
        /// 字段：CompanyAddress
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：TEXT
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("TEXT")]
        [Precision("")]
        [Scale("")]
        public virtual string CompanyAddress {get; set;} 
                        
        /// <summary>
        /// 字段：CompanyAvatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string CompanyAvatar {get; set;} 
                        
        /// <summary>
        /// 字段：CompanyRegistrationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CompanyRegistrationTime {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static UserProfileCompanyRelationView()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public UserProfileCompanyRelationView()
        {
            //属性初始化
            Id = 0;
            RelationId = 0;
            CompanyId = 0;
            Position = "";
            StatusIsGovernor = 0;
            LoginId = "";
            NickName = "";
            UserPhone = "";
            Identification = "";
            TrueName = "";
            UserAvatar = "";
            Sex = 0;
            AppId = "";
            AppSecret = "";
            UserAddress = "";
            StatusInvalid = 0;
            Birthday = ConstantCollection.DbDefaultDateTime;
            StatusEnabled = 0;
            CompanyName = "";
            CompanyCorporation = "";
            CompanyAddress = "";
            CompanyAvatar = "";
            CompanyRegistrationTime = ConstantCollection.DbDefaultDateTime;
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params UserProfileCompanyRelationViewField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}