﻿/*
Warning：此文件基于代码生成器生成，请谨慎修改并做好备份。
Developer：卢志涛（kityandhero@126.com）,
Version：1.0.0.0
Company：
Description：
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Operate;
using Operate.DataBase.SqlDatabase.Attribute;
using Enterprise.Model.Base;

namespace Enterprise.Model.ViewEntity
{
    /// <summary>
    /// EvaluationInfoView实体字段名称
    /// </summary>
    public enum EvaluationInfoViewField 
	{
        
        /// <summary>
        /// 字段：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Id,  
        
        /// <summary>
        /// 字段：RaterId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        RaterId,  
        
        /// <summary>
        /// 字段：TargetUserId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        TargetUserId,  
        
        /// <summary>
        /// 字段：Content
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Content,  
        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        CreateTime,  
        
        /// <summary>
        /// 字段：Verification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Verification,  
        
        /// <summary>
        /// 字段：Verifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Verifier,  
        
        /// <summary>
        /// 字段：VerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        VerificationTime,  
        
        /// <summary>
        /// 字段：OrderId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        OrderId,  
        
        /// <summary>
        /// 字段：LoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        LoginId,  
        
        /// <summary>
        /// 字段：Avatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        Avatar,  
        
    }

    /// <summary>
    /// EvaluationInfoView实体模型
    /// </summary>
    [Serializable]
    public partial class EvaluationInfoView : ExBaseEntity<EvaluationInfoView>
	{
        #region EvaluationInfoView Properties        
                
        /// <summary>
        /// 字段(数据表主键)：Id
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：integer
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("integer")]
        [Precision("")]
        [Scale("")]
        [Key]
                public virtual int Id {get; set;} 
                                                                                                                                                                                
                                
        /// <summary>
        /// 字段：RaterId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int RaterId {get; set;} 
                        
        /// <summary>
        /// 字段：TargetUserId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int TargetUserId {get; set;} 
                        
        /// <summary>
        /// 字段：Content
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string Content {get; set;} 
                        
        /// <summary>
        /// 字段：CreateTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime CreateTime {get; set;} 
                        
        /// <summary>
        /// 字段：Verification
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Verification {get; set;} 
                        
        /// <summary>
        /// 字段：Verifier
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int Verifier {get; set;} 
                        
        /// <summary>
        /// 字段：VerificationTime
        /// 描述：
        /// 映射类型：DateTime
        /// 数据库中字段类型：datetime
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("datetime")]
        [Precision("")]
        [Scale("")]
        public virtual DateTime VerificationTime {get; set;} 
                        
        /// <summary>
        /// 字段：OrderId
        /// 描述：
        /// 映射类型：int
        /// 数据库中字段类型：int
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("int")]
        [Precision("")]
        [Scale("")]
        public virtual int OrderId {get; set;} 
                        
        /// <summary>
        /// 字段：LoginId
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(50)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(50)")]
        [Precision("")]
        [Scale("")]
        public virtual string LoginId {get; set;} 
                        
        /// <summary>
        /// 字段：Avatar
        /// 描述：
        /// 映射类型：string
        /// 数据库中字段类型：nvarchar(400)
        /// 实际占用长度：
        /// 是否可空：True
        /// </summary>
        [Length("")]
        [Comment("")]
        [SqlType("nvarchar(400)")]
        [Precision("")]
        [Scale("")]
        public virtual string Avatar {get; set;} 
                
        /// <summary>
        /// 字段保护符号左
        /// </summary>
        protected new static string FieldProtectionLeft { get; set; }

        /// <summary>
        /// 字段保护符号右
        /// </summary>
        protected new static string FieldProtectionRight { get; set; }

        #endregion

        #region Constructors

        static EvaluationInfoView()
        {
            FieldProtectionLeft = "";
            FieldProtectionRight = "";
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public EvaluationInfoView()
        {
            //属性初始化
            Id = 0;
            RaterId = 0;
            TargetUserId = 0;
            Content = "";
            CreateTime = ConstantCollection.DbDefaultDateTime;
            Verification = 0;
            Verifier = 0;
            VerificationTime = ConstantCollection.DbDefaultDateTime;
            OrderId = 0;
            LoginId = "";
            Avatar = "";
        }

        #endregion

		#region Function

        /// <summary>
        /// 构建筛选条件
        /// </summary>
        /// <param name="fieldEnum"></param>
        /// <returns></returns>
        public virtual string GetFilterString(params EvaluationInfoViewField[] fieldEnum)
        {
            var plist = fieldEnum.Select(f => f.ToString()).ToArray();
            var result = GetFilterString(plist);
            return result;
        }

		#endregion
    }
}